﻿using Studio.BC;
using UnityEngine;

public class OnThienThan : MonoBehaviour
{
    bool allowShow = true;
    private void OnTriggerEnter(Collider other)
    {
        if (!allowShow) return;
        allowShow = false;
        if (other.transform.tag == TagManager.api.Player)
        {
            BCPopupManager.api.Show(PopupName.PopupAcQuy);
            Destroy(transform.gameObject);
        }
    }
}
