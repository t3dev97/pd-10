﻿using UnityEngine;

public class SortChilds : MonoBehaviour
{
    private void Awake()
    {
        gameObject.SetActive(false);
    }
    [ContextMenu("Sort")]
    void SortChild()
    {
        Vector3 point = new Vector3(2, 0, 0);
        foreach (Transform item in transform)
        {
            point.x -= 2;
            item.transform.position = point;
        }
    }
}
