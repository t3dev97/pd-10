﻿using TMPro;
using UnityEngine;
using Studio;

public class CustomTextMeshPro : TextMeshProUGUI
{
    public string Localize = "GUI_NAME_DUNGEON";
    // Start is called before the first frame update
    void Start()
    {
        text = LanguageManager.api.GetKey(Localize);
    }
}
