﻿using Studio.BC;
using UnityEngine;

public class OnPopupType : MonoBehaviour
{
    public POPUP_TYPE PopupType;
    public bool IsBoss = false;
    public bool IsStart;

    [Tooltip("Dùng cho vòng quay")]
    bool allowShow = true;
    private void OnTriggerEnter(Collider other)
    {
        if (!allowShow) return;
        allowShow = false;
        if (other.transform.tag == TagManager.api.Player)
        {
            if (PopupType.ToString() != PopupName.PopupVongQuay) // các loại khác
            {
                BCPopupManager.api.ShowReturn(PopupType.ToString(), (go) =>
                {
                    go.GetComponent<BCPopupDetail>().ObjCallBack = gameObject;
                    go.GetComponent<BCPopupDetail>().Anim = transform.GetComponentInChildren<Animator>();
                }, gameObject);
            }
            else // vòng quay
            {
                if (IsBoss)
                {
                    BCPopupManager.api.ShowReturn(PopupName.PopupVongQuay, (go) =>
                    {
                        go.GetComponent<BCPopupVongQuayController>().Init(EnumVongQuayType.Boss);
                        go.GetComponent<BCPopupDetail>().ObjCallBack = gameObject;
                        go.GetComponent<BCPopupDetail>().Anim = transform.GetComponentInChildren<Animator>();
                    }, gameObject);
                }
                else
                {
                    if (IsStart)
                        BCPopupManager.api.ShowReturn(PopupName.PopupVongQuay, (go) =>
                        {
                            go.GetComponent<BCPopupVongQuayController>().Init(EnumVongQuayType.Start);
                            go.GetComponent<BCPopupDetail>().ObjCallBack = gameObject;
                            go.GetComponent<BCPopupDetail>().Anim = transform.GetComponentInChildren<Animator>();
                        }, gameObject);
                    else
                        BCPopupManager.api.ShowReturn(PopupName.PopupVongQuay, (go) =>
                        {
                            go.GetComponent<BCPopupVongQuayController>().Init(EnumVongQuayType.Ads);
                            go.GetComponent<BCPopupDetail>().ObjCallBack = gameObject;
                            go.GetComponent<BCPopupDetail>().Anim = transform.GetComponentInChildren<Animator>();
                        }, gameObject);
                }
            }
        }
    }
}
