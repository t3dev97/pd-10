﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestShader : MonoBehaviour
{
    public SkinnedMeshRenderer mesh;
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
            ChangeNormalMesh();
        if (Input.GetKeyDown(KeyCode.D))
            ChangeHitMesh();
    }
    void ChangeNormalMesh()
    {
        mesh.material.SetFloat("Boolean_14609670", 0f);
    }
    void ChangeNormalAMesh(int a)
    {
        mesh.material.SetFloat("Boolean_14609670", 0f);
    }
    void ChangeHitMesh()
    {
        mesh.material.SetFloat("Boolean_14609670", 1f);
    }
}
