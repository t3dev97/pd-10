﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Studio.BC
{
    public class BCObjectPool : MonoBehaviour
    {
        private static BCObjectPool _api;
        public static BCObjectPool Api
        {
            get
            {
                if (_api == null)
                {
                    var go = new GameObject("_BCPool");
                    _api = go.AddComponent<BCObjectPool>();

                    _api.EffectPool = new Dictionary<string, Queue<GameObject>>();
                    _api.BulletPool = new Dictionary<string, Queue<GameObject>>();

                    //DontDestroyOnLoad(go);
                }

                return _api;
            }
        }


        public Dictionary<string, Queue<GameObject>> EffectPool;
        public int MaxEffect = 5;


        public int MaxBullet = 50;
        public GameObject[] BulletPrefabs;
        public Dictionary<string, Queue<GameObject>> BulletPool;



        public bool IsLoadFishCache;
        public bool IsLoadBulletCache;

        private Transform _effectParent;
        private Transform _bulletParent;
   
        #region EFFECT

        public GameObject GetEffect(string effectName)
        {
            var effectGo = (GameObject)null;
            if (EffectPool.ContainsKey(effectName) && EffectPool[effectName].Count > 0)
            {
                effectGo = EffectPool[effectName].Dequeue();
            }

            if (effectGo == null)
            {
                var prefab = BCCache.Api.GetEffect(effectName);
                if (prefab != null)
                {
                    prefab.SetActive(false);
                    effectGo = Instantiate(prefab);
                    effectGo.name = effectName;
                }
            }

            return effectGo;
        }

        public void GetEffectAsync(string effectName, Action<GameObject> onCompleteAction)
        {
            var effectGo = (GameObject)null;
            if (EffectPool.ContainsKey(effectName) && EffectPool[effectName].Count > 0)
            {
                effectGo = EffectPool[effectName].Dequeue();
            }

            if (effectGo != null)
            {
                onCompleteAction(effectGo);
                return;
            }

            var onCompleteActionTemp = onCompleteAction;
            BCCache.Api.GetEffectPrefabAsync(effectName, (prefab) =>
            {
                prefab.SetActive(false);
                var effectGoNew = Instantiate(prefab);
                effectGoNew.name = prefab.name;

                onCompleteActionTemp(effectGoNew);
            });
        }
        public void GetEffectAsync(string effectName, Action<GameObject, int, long, int, Vector3, float> onCompleteAction, int stationID, long goldReward, int fishKind, Vector3 gunPos, float shareTime = 0)
        {
            var effectGo = (GameObject)null;
            if (EffectPool.ContainsKey(effectName) && EffectPool[effectName].Count > 0)
            {
                effectGo = EffectPool[effectName].Dequeue();
            }

            if (effectGo != null)
            {
                onCompleteAction(effectGo, stationID, goldReward, fishKind, gunPos, shareTime);
                return;
            }

            var onCompleteActionTemp = onCompleteAction;
            BCCache.Api.GetEffectPrefabAsync(effectName, (prefab) =>
            {
                prefab.SetActive(false);
                var effectGoNew = Instantiate(prefab);
                effectGoNew.name = prefab.name;
                onCompleteActionTemp(effectGoNew, stationID, goldReward, fishKind, gunPos, shareTime);
            });
        }
        public void GetEffectAsync(string effectName, int input, Action<int, GameObject> onCompleteAction)
        {
            var effectGo = (GameObject)null;
            if (EffectPool.ContainsKey(effectName) && EffectPool[effectName].Count > 0)
            {
                effectGo = EffectPool[effectName].Dequeue();
            }

            if (effectGo != null)
            {
                onCompleteAction(input, effectGo);
                return;
            }

            var onCompleteActionTemp = onCompleteAction;
            BCCache.Api.GetEffectPrefabAsync(effectName, (prefab) =>
            {
                prefab.SetActive(false);
                var effectGoNew = Instantiate(prefab);
                effectGoNew.name = prefab.name;

                onCompleteActionTemp(input, effectGoNew);
            });
        }
        public IEnumerator DelayPushEffect(float delaytime, string effectName, GameObject effectGo)
        {
            yield return new WaitForSeconds(delaytime);
            PushEffect(effectName, effectGo);
        }

        public void PushEffect(string effectName, GameObject effectGo)
        {
            if (_effectParent == null)
            {
                _effectParent = new GameObject("Effect").transform;
                _effectParent.SetParent(transform, false);
                _effectParent.localPosition = Vector3.one * 5000;
            }

            if (effectGo == null) return;
            if (!EffectPool.ContainsKey(effectName))
            {
                EffectPool.Add(effectName, new Queue<GameObject>());
            }

            if (EffectPool[effectName].Count < MaxEffect)
            {
                effectGo.SetActive(false);
                effectGo.transform.SetParent(_effectParent, false);
                effectGo.transform.localPosition = Vector3.zero;
                effectGo.transform.localEulerAngles = Vector3.zero;
                EffectPool[effectName].Enqueue(effectGo);
            }
            else
            {
                Destroy(effectGo);
            }
        }

        #endregion

        #region BULLET

        public void InitBullet(GameObject prefab, int number)
        {
            if (IsLoadBulletCache)
            {
                return;
            }

            BulletPrefabs = new GameObject[4];

            for (var i = 0; i < 4; i++)
            {
                BulletPrefabs[i] = prefab;
                prefab.SetActive(false);
            }

            IsLoadBulletCache = true;

            if (_bulletParent == null)
            {
                _bulletParent = new GameObject("Bullet").transform;
                _bulletParent.SetParent(transform, false);
                _bulletParent.localPosition = Vector3.one * 5000;
            }

            for (int i = 0; i < number; i++)
            {
                var go = Instantiate(BulletPrefabs[0]);
                go.name = BulletPrefabs[0].name;
                go.transform.SetParent(_bulletParent, false);

                if (!BulletPool.ContainsKey(go.name))
                {
                    BulletPool.Add(go.name, new Queue<GameObject>());
                }
                BulletPool[go.name].Enqueue(go);

            }
        }
        public void UpdateBullet(EnumBulletEffect nameEffect)
        {
            foreach(Transform tg in _bulletParent)
            {
                tg.gameObject.GetComponent<BulletController>().UpdateEffectButllet(nameEffect);
            }
        }
        public void ResetPool()
        {
            foreach(Transform tg in transform)
            {
                foreach(Transform tg1 in tg)
                {
                    Destroy(tg1);
                }
               
            }
        }
        public void PushBullet(GameObject go)
        {
            if (go == null) return;

            var goName = go.name;

            if (BulletPool.ContainsKey(goName) && BulletPool[goName].Count >= MaxBullet)
            {
                Destroy(go);
                return;
            }

            go.SetActive(false);
            go.transform.SetParent(_bulletParent, false);
            go.transform.localPosition = Vector3.zero;

            if (!BulletPool.ContainsKey(goName))
            {
                BulletPool.Add(goName, new Queue<GameObject>());
            }
            BulletPool[goName].Enqueue(go);
        }

        public GameObject GetBullet(int stationId,Vector3 postion, Quaternion quaternion)
        {
            var go = (GameObject)null;
            var goName = BulletPrefabs[stationId].name;

            if (BulletPool.ContainsKey(goName) && BulletPool[goName].Count > 0)
            {
                go = BulletPool[goName].Dequeue();
            }

            if (go == null)
            {
                go = Instantiate(BulletPrefabs[stationId]);
                if (PlayerController.api.isBulletBlast)
                {
                    go.GetComponent<BulletController>().UpdateEffectButllet(EnumBulletEffect.Blast);
                }
                if (PlayerController.api.isBulletBolt)
                {
                    go.GetComponent<BulletController>().UpdateEffectButllet(EnumBulletEffect.Bolt);
                }
                if (PlayerController.api.isBulletIce)
                {
                    go.GetComponent<BulletController>().UpdateEffectButllet(EnumBulletEffect.Ice);
                }
                if (PlayerController.api.isBulletPoison)
                {
                    go.GetComponent<BulletController>().UpdateEffectButllet(EnumBulletEffect.Poision);
                }
                go.name = goName;
            }

            go.transform.SetParent(_bulletParent, false);

            go.transform.position = postion;

            go.transform.rotation = quaternion;

            return go;
        }

        public void ChangeBulletPrefab(int stationId, GameObject bulletPrefab)
        {
            BulletPrefabs[stationId] = bulletPrefab;
        }
        #endregion
    }
}

