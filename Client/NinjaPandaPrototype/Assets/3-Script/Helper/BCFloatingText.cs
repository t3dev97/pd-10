﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BCFloatingText : MonoBehaviour {

    private Text txt;

    public void SetData(long addValue, string mark = "+", string colorString = "#26FF66FF")
    {
        txt = gameObject.GetComponent<Text>();
        if (txt == null)
            return;

        Color color;
        if (ColorUtility.TryParseHtmlString(colorString, out color))
        { txt.color = color; }

        txt.text = mark + addValue;
        gameObject.SetActive(true);
    }
}
