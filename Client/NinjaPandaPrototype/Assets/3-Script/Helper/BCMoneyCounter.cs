﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Studio.BC
{
    public class BCMoneyCounter : MonoBehaviour
    {
        private Text2 txt;

        private bool _beginCounting;
        private float _currentGold;
        private float _targetGold;
        private float _penaltyGold;

        private bool _isTargetGoldMoreThanCurrentGold;

        private Action _onComplete;

        private void Update()
        {
            if (_beginCounting)
            {

                if (_isTargetGoldMoreThanCurrentGold)
                {
                    if (_currentGold < _targetGold)
                    {
                        _currentGold += _penaltyGold;
                        txt.text = ((long)_currentGold).ToMoneyFormat();
                    }
                    else
                    {
                        _currentGold = _targetGold;
                        txt.text = ((long)_currentGold).ToMoneyFormat();

                        _beginCounting = false;
                        if (_onComplete != null)
                            _onComplete();
                    }
                }
                else
                {
                    if (_targetGold < _currentGold)
                    {
                        _currentGold -= _penaltyGold;
                        txt.text = ((long)_currentGold).ToMoneyFormat();
                    }
                    else
                    {
                        _currentGold = _targetGold;
                        txt.text = ((long)_currentGold).ToMoneyFormat();

                        _beginCounting = false;
                        if (_onComplete != null)
                            _onComplete();
                    }
                }
            }
        }

        public void SetGold(long targetGold, long currentGold = 0, float addingPercentPerTick = 0.05f, Action onComplete = null)
        {
            _onComplete = onComplete;

            var txtComponent = gameObject.GetComponent<Text>();
            var txt2Component = gameObject.GetComponent<Text2>();

            //if (txtComponent != null)
            //    txt = txtComponent;

            if (txt2Component != null)
                txt = txt2Component;

            if (txt == null)
                return;

            _targetGold = targetGold;
            if (currentGold > 0 || _currentGold == 0)
                _currentGold = currentGold;

            if (targetGold > currentGold)
            {
                _isTargetGoldMoreThanCurrentGold = true;
                _penaltyGold = (_targetGold - _currentGold) * addingPercentPerTick;
            }
            else
            {
                _isTargetGoldMoreThanCurrentGold = false;
                _penaltyGold = (_currentGold - _targetGold) * addingPercentPerTick;
            }

            _beginCounting = true;
        }
    }
}