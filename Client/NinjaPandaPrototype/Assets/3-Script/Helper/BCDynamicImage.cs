﻿using Studio;
using Studio.BC;
using UnityEngine;
using UnityEngine.UI;

public class BCDynamicImage : MonoBehaviour
{
    public bool IsForceAspecRatio;
    //[HideInInspector]
    public string AssetName;

    public EnumDynamicImageType Type = EnumDynamicImageType.Skill;
    private bool _isloaded;
    private GameObject _loadingObject;
    private void Awake()
    {
        //AddLoadingObject();
    }

    public void SetImage(string assetName, bool isHideOnloading = false, Color? customColor = null, bool _isSetNativeSize = false)
    {
        if (!gameObject.activeSelf)
            return;
        AssetName = assetName;
        //if (BCConfig.Api.M_Language == "en" && BCConfig.Api.DynamicImageTitles.Contains(assetName))
        //{
        //    AssetName += "_eng";
        //}
        EnableLoadingObject();
        var img = GetComponent<Image>();
        if (img != null)
        {
            BCResource.Api.LoadAsset2d(Type, AssetName, sprite =>
            {
                DisableLoadingObject();
                img.color = customColor ?? new Color(1, 1, 1, 1);
                if (img != null)
                {
                    if (sprite == null)
                    {
                        img.enabled = false;
                        DebugX.LogError(this + ":SetImage: not found asset2d <" + AssetName + ">");
                        return;
                    }
                    if (AssetName != sprite.name)
                        return;
                    img.sprite = sprite;
                    if (_isSetNativeSize)
                        img.SetNativeSize();
                }
            });
        }
    }
    public void SetSpriteRenderer(string assetName, bool isHideOnloading = false, Color? customColor = null, bool _isSetNativeSize = false)
    {
        if (!gameObject.activeSelf)
            return;
        AssetName = assetName;
        //if (BCConfig.Api.M_Language == "en" && BCConfig.Api.DynamicImageTitles.Contains(assetName))
        //{
        //    AssetName += "_eng";
        //}
        EnableLoadingObject();
        var img = GetComponent<SpriteRenderer>();
        if (img != null)
        {
            //img.color = new Color(1, 1, 1, 0);
            //var hasAssetName = !string.IsNullOrEmpty(AssetName);
            //img.enabled = hasAssetName;

            //if (!hasAssetName) {
            //    DebugX.LogError(this + ":SetImage: asset2d is null or empty !");
            //    return;
            //}

            BCResource.Api.LoadAsset2d(Type, AssetName, sprite =>
            {
                DisableLoadingObject();
                img.color = customColor ?? new Color(1, 1, 1, 1);
                if (img != null)
                {
                    if (sprite == null)
                    {
                        img.enabled = false;
                        DebugX.LogError(this + ":SetImage: not found asset2d <" + AssetName + ">");
                        return;
                    }
                    if (AssetName != sprite.name)
                        return;
                    img.sprite = sprite;
                }
            });
        }
    }
    public void SetFacebookAvatar(string assetName)
    {
        if (!gameObject.activeSelf)
        {
            return;
        }

        AssetName = assetName;

        var img = GetComponent<Image>();
        if (img != null)
        {
            var hasAssetName = !string.IsNullOrEmpty(AssetName);
            img.enabled = hasAssetName;
            if (!hasAssetName)
            {
                DebugX.LogError(this + ":SetFacebookAvatar: asset2d is null or empty !");
                return;
            }

            img.color = new Color(1, 1, 1, 0);

            var imgtemp = img;
#if !BUILD_EGT
            BCFacebook.Api.GetAvatar(assetName, sprite =>
            {
                if (this == null || imgtemp == null)
                {
                    return;
                }
                imgtemp.color = new Color(1, 1, 1, 1);
                if (sprite == null)
                {
                    imgtemp.enabled = false;
                    DebugX.LogError(this + ":SetImage: not found asset2d <" + AssetName + ">");
                    return;
                }

                imgtemp.sprite = sprite;

            });
#endif
        }
    }

    public void CheckAspectRatio()
    {
        if (!IsForceAspecRatio)
            return;
        Image img = GetComponent<Image>();
        if (img != null && img.sprite != null)
        {
            img.SetNativeSize();
            var aspectRatio = GetComponent<AspectRatioFitter>();
            if (aspectRatio == null)
                aspectRatio = gameObject.AddComponent<AspectRatioFitter>();
            aspectRatio.aspectMode = AspectRatioFitter.AspectMode.HeightControlsWidth;
            aspectRatio.aspectRatio = img.sprite.rect.width / (float)img.sprite.rect.height;
        }
    }
    private void AddLoadingObject()
    {
        _isloaded = true;

        var img = GetComponent<Image>();
        if (img != null)
            img.color = new Color(1, 1, 1, 0);

        if (_loadingObject == null)
        {
            BCObjectPool.Api.GetEffectAsync(PrefabName.LoadingImage, (go) =>
            {
                _loadingObject = go;
                _loadingObject.name = PrefabName.LoadingImage;
                _loadingObject.transform.SetParent(transform, false);
                var rect = _loadingObject.transform.GetComponent<RectTransform>();
                rect.sizeDelta = Vector3.zero;
                rect.anchoredPosition = Vector3.zero;
                rect.localPosition = Vector3.zero;

                if (_isloaded)
                {
                    _loadingObject.SetActive(false);
                    img.color = new Color(1, 1, 1, 1);
                }
                else
                    _loadingObject.SetActive(true);
            });
        }
    }
    private void EnableLoadingObject()
    {
        _isloaded = false;
        if (_loadingObject != null)
            _loadingObject.SetActive(!_isloaded);
    }
    private void DisableLoadingObject()
    {
        _isloaded = true;
        if (_loadingObject != null)
            _loadingObject.SetActive(!_isloaded);
    }
}
