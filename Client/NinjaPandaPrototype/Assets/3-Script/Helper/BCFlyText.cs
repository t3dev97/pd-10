﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BCFlyText : MonoBehaviour {
	Text txtFly;
	private void OnEnable()
	{
		txtFly = GetComponent<Text>();
	}
	public void SetData(string text, Color fontColor, int fontSize)
	{
		txtFly.text = text;
		txtFly.color = fontColor;
		txtFly.fontSize = fontSize;
	}
}
