﻿using Studio.BC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BCObjectPooler : MonoBehaviour
{
    [System.Serializable]
    public class Pool
    {
        public string tag;
        public GameObject prefab;
        public int size;

        public Pool(string tag, GameObject prefab,int size)
        {
            this.tag = tag;
            this.prefab = prefab;
            this.size = size;
        }
    }
    public Dictionary<string,Queue<GameObject>> poolDictionnary;
    public List<Pool> bullets;

    public static BCObjectPooler api;
    private void Awake()
    {
        if (api == null)
            api = this;
        else if (api != this) Destroy(this);

        poolDictionnary = new Dictionary<string, Queue<GameObject>>();
    }

  public void AddBullet(string tag,int size)
  {
        BCCache.Api.GetBulletAsync(tag, (go) => {
            Pool pool = new Pool(tag, go, size);
            bullets.Add(pool);
        });

        InitBullet();
  }

    public void InitBullet()
    {
        foreach(Pool pool in bullets)
        {
            Queue<GameObject> objectPool = new Queue<GameObject>();
            for(int i = 0; i < pool.size; i++)
            {
                GameObject obj = Instantiate(pool.prefab);
                obj.SetActive(false);
                objectPool.Enqueue(obj);
            }
            poolDictionnary.Add(pool.tag, objectPool);
        }
    }
    public GameObject SpawnFromPool(string tag,Vector3 positon,Quaternion rotaion)
    {
        if (!poolDictionnary.ContainsKey(tag))
        {
            Debug.LogWarning("Pool with tag " + tag + " doen't excist");
            return null;
        }

        GameObject objectToSpawn = poolDictionnary[tag].Dequeue();
        objectToSpawn.SetActive(true);
        objectToSpawn.transform.position = positon;
        objectToSpawn.transform.rotation = rotaion;
        poolDictionnary[tag].Enqueue(objectToSpawn);
        return objectToSpawn;
    }
}
