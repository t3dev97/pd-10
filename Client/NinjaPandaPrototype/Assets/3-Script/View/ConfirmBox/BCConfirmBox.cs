﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Studio.BC
{
    public class BCConfirmBox : MonoBehaviour
    {
        public Text MessageText;

        private Action _onConfirmAction;
        private Action _onCLoseAction;

        public void Init(string message, Action onConfirmAction = null, Action onCloseAction = null)
        {
            _onConfirmAction = onConfirmAction;
            _onCLoseAction = onCloseAction;

            MessageText.text = message;
        }

        public void OnConfirmButtonOnClick()
        {
            if (_onConfirmAction != null) _onConfirmAction();
            Destroy(gameObject);
            BCViewHelper.Api.System.HideAll();
        }

        public void OnCloseButtonOnClick()
        {
            if (_onCLoseAction != null) _onCLoseAction();
            Destroy(gameObject);
            BCViewHelper.Api.System.HideAll();
        }

    }
}
