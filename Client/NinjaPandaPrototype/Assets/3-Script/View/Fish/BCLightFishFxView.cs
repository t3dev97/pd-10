﻿using Studio.BC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BCLightFishFxView : MonoBehaviour {
	public float AppearTime = 0.05f;
	public float ExitsTime = 0.05f;
	public float DisappearTime = 0.1f;
	List<int> leanTweenId = new List<int>();
	private void OnDisable()
	{
		foreach (var l in leanTweenId)
		{
			LeanTween.cancel(l);
		}
		leanTweenId.Clear();
	}
	public void Init(GameObject begin, GameObject end)
	{
		leanTweenId.Clear();
		var rectTrans = GetComponent<RectTransform>();
		rectTrans.position = new Vector3(begin.transform.position.x, begin.transform.position.y, 0);
		Vector2 beginPoint = begin.transform.position;
		Vector2 endPoint = end.transform.position;
		float angle = AngleBetweenVector2(endPoint, beginPoint);
		rectTrans.localEulerAngles = new Vector3(0, 0, angle + 90);
		rectTrans.localScale = Vector3.one;
		rectTrans.SetAsLastSibling();
		var length = (endPoint - beginPoint).magnitude;
		var lt1 = LeanTween.value(0, length, AppearTime).setOnUpdate((float value) =>
		{
			rectTrans.sizeDelta = new Vector2(rectTrans.sizeDelta.x, value);
		});
		leanTweenId.Add(lt1.id);
		var lt2 = LeanTween.delayedCall(AppearTime + ExitsTime, () =>
		{
			var lt3 = LeanTween.value(rectTrans.sizeDelta.x, 0, DisappearTime).setOnUpdate((float value) =>
			{
				rectTrans.sizeDelta = new Vector2(value, rectTrans.sizeDelta.y);
			}).setOnComplete(()=>
			{
                BCObjectPool.Api.PushEffect(gameObject.name,gameObject);
				//DestroyObject(gameObject);
			});
			leanTweenId.Add(lt3.id);
		});
		leanTweenId.Add(lt2.id);
	}

    private float AngleBetweenVector2(Vector2 vec1, Vector2 vec2)
	{
		Vector2 diference = vec2 - vec1;
		float sign = (vec2.y < vec1.y) ? -1.0f : 1.0f;
		return Vector2.Angle(Vector2.right, diference) * sign;
	}

	public float TotalTime()
	{
		return AppearTime + ExitsTime + DisappearTime;
	}
}
