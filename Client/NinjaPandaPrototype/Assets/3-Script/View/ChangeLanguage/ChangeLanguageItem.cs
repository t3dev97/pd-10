﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TTBT.DynamicScroller;

namespace Studio.BC
{
    public class ChangeLanguageItem :DynamicScrollerCellView
    {
        public Text2 LanguageName;
        public GameObject TickObject;
        public Button SelectButton;
        public Action<ChangeLanguageItem> OnClickSelectAction;

        public LanguageChangeData Data;

        public void SetData(LanguageChangeData data)
        {
            Data = data;
            LanguageName.SetLocalize(data.LanguageName);
            TickObject.SetActive(data.isSelected);

            SelectButton.AddClickListener("Language Change", OnClickSelectButton);
        }

        private void OnClickSelectButton()
        {
            OnClickSelectAction?.Invoke(this);
        }

        public void Select(bool isSelect)
        {
            Data.isSelected = isSelect;
            TickObject.SetActive(isSelect);
        }

    }
}