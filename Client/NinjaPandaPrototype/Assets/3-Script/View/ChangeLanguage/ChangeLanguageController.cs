﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TTBT.DynamicScroller;

namespace Studio.BC
{
    public class ChangeLanguageController : MonoBehaviour, IDynamicScrollerDelegate
    {
        public DynamicScroller Scroller;
        public ChangeLanguageItem PrefabItem;
        public Button ConfirmButton;

        private string _selectedLanguageID;
        private ChangeLanguageItem _selectedItem;

        private float _cellHeight;

        private List<LanguageChangeData> _languageData;
        public List<LanguageChangeData> LanguageData
        {
            get
            {
                if (_languageData == null)
                    _languageData = new List<LanguageChangeData>();
                return _languageData;
            }
        }

        public int GetCellCount(DynamicScroller scroller)
        {
            return LanguageData.Count;
        }

        public float GetCellSize(DynamicScroller scroller, int dataIndex)
        {
            return _cellHeight;
        }

        public DynamicScrollerCellView GetCellView(DynamicScroller scroller, int dataIndex)
        {
            var cell = scroller.GetCellView(PrefabItem, dataIndex) as ChangeLanguageItem;
            cell.SetData(LanguageData[dataIndex]);
            cell.OnClickSelectAction = OnClickChangeLanguage;
            if (LanguageData[dataIndex].LanguageID == _selectedLanguageID)
                _selectedItem = cell;
            return cell;
        }

        void Awake()
        {
            _cellHeight = PrefabItem.GetComponent<RectTransform>().rect.size.y;
            PrefabItem.gameObject.SetActive(false);
            Scroller.Delegate = this;
            ConfirmButton.AddClickListener( OnConfirmLanguage, "Language Change");
            _selectedLanguageID = BCConfig.Api.M_Language;
        }

        //public void SetData(LanguageData rawLanguageData)
        //{
        //    LanguageData.Clear();
        //    foreach (var i in rawLanguageData.Data)
        //    {
        //        if (i.Value.LanguageID == _selectedLanguageID)
        //            i.Value.isSelected = true;
        //        else
        //            i.Value.isSelected = false;
        //        LanguageData.Add(i.Value);
        //    }

        //    Scroller.ReloadData();
        //}

        public void OnClickChangeLanguage(ChangeLanguageItem selectedItem)
        {
            //Deselect old selected item
            LanguageData.Find(x => x.LanguageID == _selectedLanguageID).isSelected = false;
            _selectedItem.Select(false);

            //Select new selected item
            _selectedItem = selectedItem;
            _selectedItem.Select(true);
            _selectedLanguageID = selectedItem.Data.LanguageID;
        }

        public void OnConfirmLanguage()
        {
   
            //if (!string.IsNullOrEmpty(BCModel.Api.AccountInfo.LoginToken))
            //{
            //    if (BCConfig.Api.M_Language != _selectedLanguageID)
            //    {
            //        BCConfig.Api.M_Language = _selectedLanguageID;

            //        PlayerPrefs.SetString(WebKey.language, BCConfig.Api.M_Language);
            //        PlayerPrefs.Save();

            //        BCGameService.Api.SendChangeLanguage(() =>
            //        {
            //            BCViewHelper.Api.ShowConfirmBox(LanguageManager.api.GetKey(BCLocalize.ID_GUI_LANGUAGE_CHANGE_SUCCESS), () =>
            //            {
            //                BCViewHelper.Api.Popup.Hide(StateName.ChangeLanguage);

            //                if (BCGameService.Api.IsConnected())
            //                    BCGameService.Api.Disconnect();
            //                if (BCBattleService.Api.IsConnected())
            //                    BCBattleService.Api.Disconnect();
            //                if (BCExternalService.Api.IsConnected())
            //                    BCBattleService.Api.Disconnect();
            //                if (BCPKService.Api.IsConnected())
            //                    BCPKService.Api.Disconnect();

            //                Application.LoadLevel(SceneName.Loading);
            //            });
            //        });
            //    }
            //    else
            //        BCViewHelper.Api.Popup.Hide(StateName.ChangeLanguage);
            //}
            //else
            //{
            //    if (BCConfig.Api.M_Language != _selectedLanguageID)
            //    {
            //        BCConfig.Api.M_Language = _selectedLanguageID;

            //        PlayerPrefs.SetString(WebKey.language, BCConfig.Api.M_Language);
            //        PlayerPrefs.Save();

            //        BCViewHelper.Api.ShowConfirmBox(LanguageManager.api.GetKey(BCLocalize.ID_GUI_LANGUAGE_CHANGE_SUCCESS), () =>
            //        {
            //            BCViewHelper.Api.Popup.Hide(StateName.ChangeLanguage);

            //            Application.LoadLevel(SceneName.Loading);
            //        });
            //    }
            //    else
            //        BCViewHelper.Api.Popup.Hide(StateName.ChangeLanguage);
            //}
        }
    }
    public class LanguageChangeData
    {
        public string LanguageID;
        public string LanguageName;

        public bool isSelected;

        public LanguageChangeData Parse(Dictionary<string,object> data)
        {
            LanguageID = data.Get<string>(BCKey.id);
            LanguageName = data.Get<string>(BCKey.name);
            return this;
        }
    }
}