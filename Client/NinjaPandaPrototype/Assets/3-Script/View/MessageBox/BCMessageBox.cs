﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Studio.BC
{
    public class BCMessageBox : MonoBehaviour
    {
        public Text MessageText;
        public Button CloseButton;

        private Action _onCloseClickAction;

        public void Init(string message, Action onCloseClickAction= null)
        {
            _onCloseClickAction = onCloseClickAction;
            MessageText.text = message;
            CloseButton.AddClickListener("Force Update",OnCloseButtonClick);
        }

        public void OnCloseButtonClick()
        {
            if (_onCloseClickAction != null){
                _onCloseClickAction();
            }

            Destroy(gameObject);
            BCViewHelper.Api.System.HideAll();
        }
    }
}

