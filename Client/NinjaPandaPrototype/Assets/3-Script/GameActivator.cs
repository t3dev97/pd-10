﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Studio.BC
{
    public class GameActivator : MonoBehaviour
    {
        public RefPrefabManageUI AtlasPrefferrence;
        public Text VersionText;
        public Transform CanvasTranform;

        public Sprite vnTexture;
        public Sprite enTexture;
        public Slider slider;
        public bool Test;

        [Header("AssetBundle Settings")]
        public bool HasStreamingAsset = true;
        public bool EncryptAssetBundleName;
        [Space(5)]

        [Header("Game Settings")]
        public bool IsShowChangeLanguage;
        public EnumLanguageConfigType DefaultLanguage;

        [Header("Debug Settings")]
        public bool IsUnityLog;

        [Space(5)]

        private int _retry;
        bool _isResetData = false;
        private void Start()
        {

            // load data new
            if (_isResetData)
            {
                PlayerPrefs.DeleteAll();
                _isResetData = false;
                //0 = true
                PlayerPrefs.SetInt(PlayerPrefsKey.IsLoading, 0);
            }
            else
            {
                if (PlayerPrefs.GetString("language", "").Equals(""))
                {
                    PlayerPrefs.SetString("language", "vn");
                }


                if (PlayerPrefs.GetString("language") == "en")
                {
                    DefaultLanguage = EnumLanguageConfigType.en;
                }
                else
                {
                    DefaultLanguage = EnumLanguageConfigType.vn;
                }

                if (PlayerPrefs.GetInt(PlayerPrefsKey.IsLoading, 0) == 0)
                    PlayerPrefs.SetInt(PlayerPrefsKey.IsLoading, 0);
                else
                    PlayerPrefs.SetInt(PlayerPrefsKey.IsLoading, 1);
            }

            StartCoroutine(StartShowNphPlashScreen());


        }

        [ContextMenu("Resetdata")]
        public void resetdata()
        {
            DebugX.Log("Resetdata ");
            _isResetData = true;
            PlayerPrefs.DeleteAll();
        }


        IEnumerator StartShowNphPlashScreen()
        {
            yield return null;
            InitGame();
        }

        void InitGame()
        {
            Debug.unityLogger.logEnabled = IsUnityLog;
            Application.runInBackground = true;
#if !UNITY_WEBGL
            Application.targetFrameRate = 60;
#endif
            Screen.sleepTimeout = SleepTimeout.NeverSleep;
            Debug.unityLogger.logEnabled = IsUnityLog;
            BCConfig.Api.Clear();

            if (!BCConfig.Api.IsFirstInit)
                BCConfig.Api.IsFirstInit = true;

            BCCache.Api.Init();
            BCConfig.Api.IsShowChangeLanguage = IsShowChangeLanguage;
            BCConfig.Api.CurrentScene = CurrentSceneName.BCLoading;

            var savedLanguage = PlayerPrefs.GetString(WebKey.language);
            if (!string.IsNullOrEmpty(savedLanguage))
                BCConfig.Api.M_Language = savedLanguage;
            else
                BCConfig.Api.M_Language = DefaultLanguage.ToString();

            BCResource.Api.RefManageUI = AtlasPrefferrence.gameObject;
            BaseLoader2.HasStreamingAsset = HasStreamingAsset;
            BaseLoader2.EncryptFileName = EncryptAssetBundleName;

            // kiểm tra thiết bị, lỗi sd card -> k save file dc, trường họp đôi khi xảy ra
            if (string.IsNullOrEmpty(InitFirstAssetController.Api.LocalUrlAsset2D))
            {
                string mess = "";

                if (BCConfig.Api.M_Language == "vn")
                    mess = "Thiết bị gặp sự cố lưu trữ dữ liệu. Vui lòng restart lại thiết bị.";
                else
                    mess = "Error while looking for data location. Please restart your device and try again";
                BCViewHelper.ShowMessageBoxStatic(mess, transform, () => { Application.Quit(); });
                return;
            }
            LoadServiceConfig();
        }

        void LoadServiceConfig()
        {
            if (BCConfig.Api.M_Language == "vn")
            {

            }
            else
            {

            }

            var gameOS = Utils.GetGameOS();// "undefined";

            StartCoroutine(InitResource());
        }


        IEnumerator InitResource()
        {
            yield return null;
            //DebugX.LogError("Init assetbundle resource..........");

            StartCoroutine(StartLoadResource());
        }

        private IEnumerator StartLoadResource()
        {
            FirebaseDynamicLinks.Api.CallRefLink();

            yield return StartCoroutine(InitAssetBundle(false));

            var bcLoading = gameObject.AddComponent<BCLoading>();

            yield return StartCoroutine(bcLoading.LoadResources(process =>
            {
                slider.value = process;

            }, () =>
            {
                GameObject gameObject = new GameObject();
                gameObject.name = "_LoadScenes";
                gameObject.AddComponent<LoadScene>();
                if (!Test)
                    SceneManager.LoadScene(NameScene.LayoutDemo, LoadSceneMode.Single);
                else
                    SceneManager.LoadScene("MainArt", LoadSceneMode.Single);
            }
       ));


            var unloadResource = Resources.UnloadUnusedAssets();
            yield return unloadResource;
        }
        IEnumerator InitAssetBundle(bool hasInitFirstAssetBundle)
        {
            //if (Resource != EnumResourceType.Local)
            //{
            //    var startTime = Time.realtimeSinceStartup;
            //    var serviceConfig = BCConfig.Api.ServiceConfigData;

            //    var hasChange = false;

            //    yield return StartCoroutine(BCResource.Api.Init(serviceConfig.AssetBundleUrl,
            //        (change) => { hasChange = change; },
            //        (assetBundle, errorStr) =>
            //        {
            //            DownloadStatus.SetErrorStatus(errorStr);
            //        }));

            //    //check new verion to update assetbundle

            //    if (!hasInitFirstAssetBundle)
            //    {
            //        DownloadStatus.SetDownloadStatus(BCConfig.Api.GetFirstLocalize(BCLocalize.ID_GUI_UPDATING_DATA));
            //        yield return StartCoroutine(BCResource.Api.UpdateAssetBundle(

            //            hasChange,

            //            //download
            //            (assetName, childLoaded, child) => DownloadStatus.SetDownloadProgress((childLoaded / child)),

            //            //error
            //            (assetName, errorStr) =>
            //            {
            //                DownloadStatus.SetErrorStatus(assetName + " :: " + errorStr);
            //            })
            //        );
            //    }

            //    DownloadStatus.SetDownloadStatus(BCConfig.Api.GetFirstLocalize(BCLocalize.ID_GUI_INITING_DATA));

            //}
            //else
            //{
            BCResource.Api.AtlasUI = AtlasPrefferrence.objectReference;
            //}

            var atlasPrefab = (GameObject)null;
            yield return StartCoroutine(BCResource.Api.LoadAtlas("AtlasUI", (prefab) =>
            {
                atlasPrefab = prefab;
            }));

            //reload atlas
            if (atlasPrefab == null)
            {
                ShowDownloadFail();
                yield break;
            }

            var atlasGo = Instantiate(atlasPrefab);
            atlasGo.name = "_AtlasUI";
            atlasGo.transform.localPosition = Vector3.zero;
            atlasGo.transform.localScale = Vector3.one;

            BCResource.Api.AtlasUI = atlasGo;

#if UNITY_EDITOR
            var manageSpriteUiView = atlasGo.GetComponent<ManageSpriteUI>();
            if (manageSpriteUiView != null) manageSpriteUiView.SetObjectPreference(AtlasPrefferrence);
#endif

            AtlasPrefferrence.objectReference = atlasGo;
            DontDestroyOnLoad(atlasGo);

        }

        void ShowDownloadFail()
        {
            string mess = "";
            if (BCConfig.Api.M_Language == "vn")
                mess = "Khởi tạo dữ liệu thất bại. Vui lòng tải lại dữ liệu!";
            else
                mess = "Initiation failed. Please try again later!";
            BCViewHelper.ShowMessageBoxStatic(mess, transform, () =>
            {
                StartCoroutine(InitResource());
            });
        }
        //private void OnDisable()
        //{
        //    resetdata();
        //}
    }
}

