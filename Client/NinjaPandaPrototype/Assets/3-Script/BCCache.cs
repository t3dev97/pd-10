﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Studio.BC
{
    public class BCCache : MonoBehaviour
    {
        private static BCCache _api;


        public static BCCache Api
        {
            get
            {
                if (_api == null)
                {
                    var go = new GameObject("_BCCache");
                    _api = go.AddComponent<BCCache>();
                    DontDestroyOnLoad(go);
                }

                return _api;
            }
        }

        public List<GameObject> Effects;
        public List<GameObject> Buttons;
        public List<GameObject> Bullet;

        private Dictionary<string, GameObject> _effectManager;
        private Dictionary<string, GameObject> _buttonManager;
        private Dictionary<string, GameObject> _bulletManager;

        public void Init()
        {
            Effects = new List<GameObject>();
            Buttons = new List<GameObject>();
            Bullet = new List<GameObject>();

            _effectManager = new Dictionary<string, GameObject>();
            _buttonManager = new Dictionary<string, GameObject>();
            _bulletManager = new Dictionary<string, GameObject>();
        }


        #region Effect

        public void SaveEffect(GameObject prefab)
        {
            var prefabName = prefab.name;
            if (!_effectManager.ContainsKey(prefabName))
            {
                //#if UNITY_EDITOR
                //                if (!BCResource.Api.IsLocalResource)
                //                {
                //                    prefab.ResetShaderAllChild();
                //                }
                //#endif
                _effectManager.Add(prefabName, prefab);

                Effects.Add(prefab);
            }
        }

        public GameObject GetEffect(string effectName)
        {
            if (_effectManager.ContainsKey(effectName) == false)
            {
                return null;
            }

            return _effectManager[effectName];
        }

        public void GetEffectPrefabAsync(string effectName, Action<GameObject> onCompleteAction)
        {
            var effectGo = GetEffect(effectName);
            if (effectGo != null)
            {
                if (onCompleteAction != null) onCompleteAction(effectGo);
                return;
            }

            var onCompleteActionTemp = onCompleteAction;
            StartCoroutine(BCResource.Api.LoadEffect(effectName, (prefab) =>
            {
                SaveEffect(prefab);

                if (onCompleteActionTemp != null)
                {
                    onCompleteActionTemp(prefab);
                }
            }));
        }

        #endregion


        private DataConfigManager _dataConfigManager;
        public DataConfigManager DataConfig
        {
            get
            {
                if (_dataConfigManager == null)
                    _dataConfigManager = new DataConfigManager();
                return _dataConfigManager;
            }
        }

        public void GetUIPrefabAsync(string buttonName, Action<GameObject> onCompleteAction)
        {
            var buttonGo = GetButton(buttonName);
            if (buttonGo != null)
            {
                if (onCompleteAction != null) onCompleteAction(buttonGo);
                return;
            }

            var onCompleteActionTemp = onCompleteAction;
            StartCoroutine(BCResource.Api.LoadButton(buttonName, (prefab) =>
            {
                SaveButton(prefab);

                if (onCompleteActionTemp != null)
                {
                    onCompleteActionTemp(prefab);
                }
            }));
        }
        public void SaveButton(GameObject prefab)
        {
            var prefabName = prefab.name;
            if (_buttonManager == null) return;
            if (!_buttonManager.ContainsKey(prefabName))
            {
#if UNITY_EDITOR
                if (!BCResource.Api.IsLocalResource)
                {
                    //prefab.ResetShaderAllChild();
                }
#endif
                _buttonManager.Add(prefabName, prefab);
                Buttons.Add(prefab);
            }
        }
        public void SaveBullet(GameObject prefab)
        {
            var prefabName = prefab.name;
            if (!_bulletManager.ContainsKey(prefabName))
            {
#if UNITY_EDITOR
                if (!BCResource.Api.IsLocalResource)
                {
                    //prefab.ResetShaderAllChild();
                }
#endif
                _bulletManager.Add(prefabName, prefab);
                Bullet.Add(prefab);
            }
        }

        public GameObject GetButton(string buttonName)
        {
            if (_buttonManager.ContainsKey(buttonName) == false)
            {
                return null;
            }

            return _buttonManager[buttonName];
        }

        public GameObject GetBullet(string buttonName)
        {
            if (_bulletManager == null) return null;

            if (_bulletManager.ContainsKey(buttonName) == false)
            {
                return null;
            }

            return _bulletManager[buttonName];
        }

        public void GetEffectAsync(string buttonName, Action<GameObject> onCompleteAction)
        {
            var buttonGo = GetButton(buttonName);
            if (buttonGo != null)
            {
                if (onCompleteAction != null) onCompleteAction(buttonGo);
                return;
            }

            var onCompleteActionTemp = onCompleteAction;
            StartCoroutine(BCResource.Api.LoadButton(buttonName, (prefab) =>
            {
                SaveButton(prefab);

                if (onCompleteActionTemp != null)
                {
                    onCompleteActionTemp(prefab);
                }
            }));
        }

        public void GetBulletAsync(string buttonName, Action<GameObject> onCompleteAction)
        {
            var buttonGo = GetBullet(buttonName);
            if (buttonGo != null)
            {
                if (onCompleteAction != null) onCompleteAction(buttonGo);
                return;
            }

            var onCompleteActionTemp = onCompleteAction;
            StartCoroutine(BCResource.Api.LoadBullets(buttonName, (prefab) =>
            {
                SaveButton(prefab);

                if (onCompleteActionTemp != null)
                {
                    onCompleteActionTemp(prefab);
                }
            }));
        }
    }
}

