﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace Studio.BC
{
    public class BCToolEditor
    {
        [MenuItem("BCTool/Resources/Export Sound Resource", false, 0)]
        public static void ExportSoundResource()
        {
            var soundDataPath = (Application.dataPath + "/0-AssetBundles/Resources/Config/SoundResource.txt").Replace('/', '\\');

            var soundList = new List<object>();
            var sounds = Resources.LoadAll<AudioClip>("Sound");
            foreach (var sound in sounds)
            {
                soundList.Add(sound.name);
            }

            var dataSave = new Dictionary<string, object>()
            {
                {"data", soundList}
            }.DictionaryToJson();

            File.WriteAllText(soundDataPath, dataSave);
            AssetDatabase.Refresh();

            Debug.Log("EXPORT SOUND DATA DONE.........");
        }

        [MenuItem("BCTool/Resources/Export Effect Resource", false, 1)]
        public static void ExportEffectResource()
        {
            var soundDataPath = (Application.dataPath + "/0-AssetBundles/Resources/Config/Resource_Effect.txt").Replace('/', '\\');

            var effectList = new List<object>();
            var effects = Resources.LoadAll<GameObject>("Effect/Prefabs");
            foreach (var effect in effects)
            {
                effectList.Add(effect.name);
            }

            var dataSave = new Dictionary<string, object>()
            {
                {"data", effectList}
            }.DictionaryToJson();

            File.WriteAllText(soundDataPath, dataSave);
            AssetDatabase.Refresh();

            Debug.Log("EXPORT EFFECT DATA DONE.........");
        }

        [MenuItem("BCTool/Resources/Export Fish Resource", false, 2)]
        public static void ExportFishResource()
        {
            var soundDataPath = (Application.dataPath + "/0-AssetBundles/Resources/Config/FishResource.txt").Replace('/', '\\');

            var effectList = new List<object>();
            var effects = Resources.LoadAll<GameObject>("Fish/Prefabs");
            foreach (var effect in effects)
            {
                effectList.Add(effect.name);
            }

            var dataSave = new Dictionary<string, object>()
            {
                {"data", effectList}
            }.DictionaryToJson();

            File.WriteAllText(soundDataPath, dataSave);
            AssetDatabase.Refresh();

            Debug.Log("EXPORT FISH RESOURCE DONE.........");
        }

        [MenuItem("BCTool/Resources/Export FishPath Resource", false, 3)]
        public static void ExportFishPathResource()
        {
            var soundDataPath = (Application.dataPath + "/0-AssetBundles/Resources/Config/FishPathResource.txt").Replace('/', '\\');

            var effectList = new List<object>();
            var effects = Resources.LoadAll<GameObject>("FishPath/Prefabs");
            foreach (var effect in effects)
            {
                effectList.Add(effect.name);
            }

            var dataSave = new Dictionary<string, object>()
            {
                {"data", effectList}
            }.DictionaryToJson();

            File.WriteAllText(soundDataPath, dataSave);
            AssetDatabase.Refresh();

            Debug.Log("EXPORT FISH_PATH RESOURCE DONE.........");
        }

        [MenuItem("BCTool/Resources/Export Fish Collider", false, 3)]
        public static void ExportFishCollider()
        {
            var fishAnimationPath = (Application.dataPath + "/0-AssetBundles/Resources/Config/FishAnimationData.txt").Replace('/', '\\');
            var dic = File.ReadAllText(fishAnimationPath).JsonToDictionary().Get<List<object>>("data");
            

            var fishList = Resources.LoadAll<GameObject>("Fish/Prefabs");
            foreach (var effect in fishList)
            {
                var n = effect.name;
                foreach (Dictionary<string, object> o in dic)
                {
                    var kind = o.Get<int>("kind");
                    if (n.ToInt() == kind)
                    {
                        if (!o.ContainsKey("col_size")) {
                            o.Add("col_size", "");
                        }

                        if (!o.ContainsKey("col_center")) {
                            o.Add("col_center", "");
                        }

                        var col = effect.GetComponent<BoxCollider>();
                        if (col != null)
                        {
                            o["col_size"] = col.size.Vector3ToString();
                            o["col_center"] = col.center.Vector3ToString();
                        }
                       
                        break;
                    }
                }
            }

            var dicResul = new Dictionary<string, object>()
            {
                {"data", dic}
            };

            File.WriteAllText(fishAnimationPath, dicResul.DictionaryToJson());
        }

        [MenuItem("BCTool/Resources/Import Fish Collider", false, 4)]
        public static void ImportFishCollider()
        {
            var fishAnimationPath = (Application.dataPath + "/0-AssetBundles/Resources/Config/FishAnimationData.txt").Replace('/', '\\');
            var dic = File.ReadAllText(fishAnimationPath).JsonToDictionary().Get<List<object>>("data");

            var fishList = Resources.LoadAll<GameObject>("Fish/Prefabs");
            foreach (var effect in fishList)
            {
                var n = effect.name;
                foreach (Dictionary<string, object> o in dic)
                {
                    var kind = o.Get<int>("kind");

                    var col = effect.GetComponent<BoxCollider>();
                    if (col == null) col = effect.AddComponent<BoxCollider>();
                    col.isTrigger = true;

                    if (n.ToInt() == kind)
                    {
                        if (o.ContainsKey("col_size"))
                        {
                            col.size = o["col_size"].ToString().ToVector3();
                        }

                        if (o.ContainsKey("col_center"))
                        {
                            col.center = o["col_center"].ToString().ToVector3();
                        }
                        break;
                    }
                }
            }
        }

        [MenuItem("BCTool/SoundController/AddSoundControlerEffect", false, 0)]
        public static void AddSoundControlerEffect()
        {
            var effects = Resources.LoadAll<GameObject>("Effect/Prefabs");
            Debug.Log("==========> AddSoundControlerEffect <===========");

            foreach (var effect in effects)
            {
                if (effect == null) continue;

                var audioSourceList = GetAudioSourceList(effect);
                var bcSoundVolumeController = effect.GetComponent<BCSoundVolumeController>();

                if (audioSourceList.Count > 0)
                {
                    if (bcSoundVolumeController == null) {
                        bcSoundVolumeController = effect.AddComponent<BCSoundVolumeController>();
                    }

                    bcSoundVolumeController.AudioSourceList = audioSourceList;
                    Debug.Log("effect Name:: " + effect.name);
                }
                else
                {
                    if (bcSoundVolumeController != null)
                    {
                        Object.DestroyImmediate(bcSoundVolumeController);
                    }
                }
            }
        }

        [MenuItem("BCTool/SoundController/AddSoundControlerSkill", false, 1)]
        public static void AddSoundControlerSkill()
        {
            var skillGos = Resources.LoadAll<GameObject>("Skill");
            Debug.Log("==========> AddSoundControlerSkill <===========");
            foreach (var skillGo in skillGos)
            {
                if (skillGo == null) continue;

                var audioSourceList = GetAudioSourceList(skillGo);
                var bcSoundVolumeController = skillGo.GetComponent<BCSoundVolumeController>();

                if (audioSourceList.Count > 0)
                {
                    if (bcSoundVolumeController == null)
                    {
                        bcSoundVolumeController = skillGo.AddComponent<BCSoundVolumeController>();
                    }

                    bcSoundVolumeController.AudioSourceList = audioSourceList;
                    Debug.Log("skill Name:: " + skillGo.name);
                }
                else
                {
                    if (bcSoundVolumeController != null)
                    {
                        Object.DestroyImmediate(bcSoundVolumeController);
                    }
                }

               
            }
        }

        [MenuItem("BCTool/RemoveAtlasUIEffectPrefab", false, 2)]
        public static void RemoveAtlasUIEffectPrefab()
        {
            var effectGos = Resources.LoadAll<GameObject>("Effect/Prefabs");
            var hasOne = false;

            foreach (var effectGo in effectGos)
            {
                if (effectGo == null) continue;
                var imageUiHelperList = GetEffectPrefabList(effectGo);
                if (imageUiHelperList.Count > 0)
                {
                    hasOne = true;
                    Debug.Log("---- GO: " + effectGo.name + " -----");
                    foreach (var imageUiHelper in imageUiHelperList)
                    {
                        Debug.Log("ResetImg: " + imageUiHelper.gameObject.name);
                        imageUiHelper.refManageUI = null;
                        imageUiHelper.oldSpriteSelected = null;
                        imageUiHelper.spriteSelected = null;

                        var image = imageUiHelper.gameObject.GetComponent<Image>();
                        if (image != null) image.sprite = null;
                    }

                    Debug.Log("-----------------------------------");
                }
            }

            if (!hasOne)
            {
                Debug.Log("---- No Prefab has imageUiHelper not null !!");
            }
        }

        [MenuItem("BCTool/RevertAtlasUIEffectPrefab", false, 2)]
        public static void RevertAtlasUIEffectPrefab()
        {
            var effectGos = Resources.LoadAll<GameObject>("Effect/Prefabs");
            var hasOne = false;

            foreach (var effectGo in effectGos)
            {
                if (effectGo == null) continue;
                var imageUiHelperList = GetEffectPrefabList(effectGo);
                if (imageUiHelperList.Count > 0)
                {
                    hasOne = true;
                    Debug.Log("---- GO: " + effectGo.name + " -----");
                    foreach (var imageUiHelper in imageUiHelperList)
                    {
                        Debug.Log("RevertImg: " + imageUiHelper.gameObject.name);
                        imageUiHelper.Revert();
                    }

                    Debug.Log("-----------------------------------");
                }
            }

            if (!hasOne)
            {
                Debug.Log("---- No Prefab has imageUiHelper not null !!");
            }
        }

        private static List<AudioSource> GetAudioSourceList(GameObject go)
        {
            var listAudioSource = new List<AudioSource>();

            var audioSource = go.GetComponent<AudioSource>();
            if (audioSource != null) {
                listAudioSource.Add(audioSource);
            }

            var childTran = go.transform;
            var childCount = childTran.childCount;

            for (var i = 0; i < childCount; i++)
            {
                var currentChild = childTran.GetChild(i);
                if (currentChild != null) {
                    listAudioSource.AddRange(GetAudioSourceList(currentChild.gameObject));
                }
            }

            return listAudioSource;
        }

        private static List<ImageUIHelper> GetEffectPrefabList(GameObject go)
        {
            var listImageUiHelper = new List<ImageUIHelper>();

            var imageUiHelper = go.GetComponent<ImageUIHelper>();
            if (imageUiHelper != null)// && imageUiHelper.refManageUI != null)
            {
                listImageUiHelper.Add(imageUiHelper);
            }

            var childTran = go.transform;
            var childCount = childTran.childCount;

            for (var i = 0; i < childCount; i++)
            {
                var currentChild = childTran.GetChild(i);
                if (currentChild != null)
                {
                    listImageUiHelper.AddRange(GetEffectPrefabList(currentChild.gameObject));
                }
            }

            return listImageUiHelper;
        }
    }
}

