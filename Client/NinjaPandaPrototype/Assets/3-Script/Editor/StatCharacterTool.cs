﻿using System;
using UnityEditor;
using UnityEngine;

public class StatCharacterTool : EditorWindow
{
    public static StatCharacterTool window;

    CharacterDetail character; // character dùng để chứa các data dùng tạm, xử lý 



    string link = "Assets/Resources/ModifyTool/Player";
    string sNhapSaiER = "Nhập sai kiểu dữ liệu, nếu bạn nhập số THẬP PHÂN thì có thể bỏ qua lỗi này, nếu vẫn bị vui lòng liên hệ anim";

    [MenuItem("BCTool/Clear All PlayerPrefs %t")]
    public static void ClearAllPlayerPrefs() // bật window editor
    {
        PlayerPrefs.DeleteAll();
    }

    [MenuItem("Window/Stat Chacrater Tool %q")]
    public static void Init() // bật window editor
    {
        // cách 1:
        EditorWindow.GetWindow(typeof(StatCharacterTool));

        // cách 2:
        //window = (StatCharacterTool)EditorWindow.GetWindow(typeof(StatCharacterTool));
        //window.Show();
    }

    int func = -1;
    string namePlayer;
    bool isCheckModify = false;
    private void OnGUI()
    {
        //Debug.Log(12);
        GUILayout.BeginVertical();
        //---------------------------------------------------------

        // chọn các tính năng
        GUILayout.BeginHorizontal();
        //if (GUILayout.Button("Load Character Asset"))
        //{
        //    func = 0;
        //}
        if (GUILayout.Button("Create Character Asset"))
        {
            func = 1;
        }
        if (GUILayout.Button("Modify Character Asset"))
        {
            func = 2;
            isCheckModify = false;
        }

        GUILayout.EndHorizontal();

        // Nội dung tính năng
        GUILayout.BeginVertical();
        switch (func)
        {
            //case 0:
            //    {
            //        LoadCharaterAssetToCharacterDetail("Player"); // load data từ asset lên
            //        //func = -1;
            //        break;
            //    }
            case 1:
                {
                    CreateNewCharacterAsset(); // tạo asset mới
                    break;
                }
            case 2:
                {
                    if (isCheckModify == false)
                    {

                        GUILayout.Label("Name Player");
                        namePlayer = GUILayout.TextField(namePlayer);
                        if (GUILayout.Button("Find"))
                        {
                            isCheckModify = true;
                        }
                    }
                    if (isCheckModify)
                        ModifyCharacterAsset(namePlayer); // tạo asset mới
                    break;
                }

        }
        GUILayout.EndVertical();


        //----------------------------------------------------------
        GUILayout.EndVertical();

    }

    CharacterAsset characterAssetTemp = null; // lưu tạm thời dữ liệu nhập vào
    void ModifyCharacterAsset(string name)
    {
        if (characterAssetTemp == null)
        {
            characterAssetTemp = LoadCharaterAsset(name);//load character
            if (characterAssetTemp == null)
            {
                isCheckModify = false; // quay về trạng thái cho phép nhập tìm lại 
                return;
            }
            character = ConverAssetToDetail(characterAssetTemp);
        }

        GUILayout.BeginVertical();

        GUILayout.Label("Modify Character from Asset");

        try
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label("Name");
            characterAssetTemp.Name = GUILayout.TextField(characterAssetTemp.Name, new GUILayoutOption[] { GUILayout.Width(100) });
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("HP");
            characterAssetTemp.BasicHP = float.Parse(GUILayout.TextField(characterAssetTemp.BasicHP.ToString(), new GUILayoutOption[] { GUILayout.Width(100) }));
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("ATK Damage");
            characterAssetTemp.BasicATKDamage = float.Parse(GUILayout.TextField(characterAssetTemp.BasicATKDamage.ToString(), new GUILayoutOption[] { GUILayout.Width(100) }));
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("MS");
            characterAssetTemp.BasicMS = float.Parse(EditorGUILayout.TextField(characterAssetTemp.BasicMS.ToString(), new GUILayoutOption[] { GUILayout.Width(100) }));
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("AS");
            characterAssetTemp.BasicAS = float.Parse(EditorGUILayout.TextField(characterAssetTemp.BasicAS.ToString(), new GUILayoutOption[] { GUILayout.Width(100) }));
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("CR");
            characterAssetTemp.BasicCR = float.Parse(EditorGUILayout.TextField(characterAssetTemp.BasicCR.ToString(), new GUILayoutOption[] { GUILayout.Width(100) }));
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("CD");
            characterAssetTemp.BasicCD = float.Parse(EditorGUILayout.TextField(characterAssetTemp.BasicCD.ToString(), new GUILayoutOption[] { GUILayout.Width(100) }));
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Dodge");
            characterAssetTemp.BasicDodge = float.Parse(EditorGUILayout.TextField(characterAssetTemp.BasicDodge.ToString(), new GUILayoutOption[] { GUILayout.Width(100) }));
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("MR");
            characterAssetTemp.BasicMR = float.Parse(EditorGUILayout.TextField(characterAssetTemp.BasicMR.ToString(), new GUILayoutOption[] { GUILayout.Width(100) }));
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("RR");
            characterAssetTemp.BasicRR = float.Parse(EditorGUILayout.TextField(characterAssetTemp.BasicRR.ToString(), new GUILayoutOption[] { GUILayout.Width(100) }));
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("RL");
            characterAssetTemp.BasicRL = float.Parse(EditorGUILayout.TextField(characterAssetTemp.BasicRL.ToString(), new GUILayoutOption[] { GUILayout.Width(100) }));
            GUILayout.EndHorizontal();
        }
        catch (Exception e)
        {
            Debug.LogError(sNhapSaiER);
        }


        if (GUILayout.Button("Save"))
        {
            func = -1;
            isCheckModify = false;
            //CharacterAsset asset = LoadCharaterAsset(name);
            //asset = ConverDetailToAsset(characterAssetTemp);
            //string[] path = AssetDatabase.FindAssets(name, new string[] { link });

            //CharacterAsset asset = AssetDatabase.LoadAssetAtPath(AssetDatabase.GUIDToAssetPath(path[0]), typeof(CharacterAsset)) as CharacterAsset;
            //asset.Name = characterAssetTemp.Name;
            //asset.BasicHP = characterAssetTemp.BasicHP;
            //asset.BasicATKDamage = characterAssetTemp.BasicATKDamage;
            //asset.BasicMS = characterAssetTemp.BasicMS;
            //asset.BasicAS = characterAssetTemp.BasicAS;
            //asset.BasicCR = characterAssetTemp.BasicCR;
            //asset.BasicCD = characterAssetTemp.BasicCD;
            //asset.BasicDodge = characterAssetTemp.BasicDodge;
            //asset.BasicMR = characterAssetTemp.BasicMR;
            //asset.BasicRR = characterAssetTemp.BasicRR;
            //asset.BasicRL = characterAssetTemp.BasicRL;
            //AssetDatabase.CreateAsset(characterAssetTemp, link + "/" + "PLAYER2" + ".asset");
            //AssetDatabase.SaveAssets();
            EditorUtility.SetDirty(characterAssetTemp);
            Debug.Log("Đã chỉnh sửa: " + characterAssetTemp.ToString());
        }
        GUILayout.EndVertical();
    }

    void CreateNewCharacterAsset()
    {
        if (characterAssetTemp == null || isCheckModify)
            characterAssetTemp = new CharacterAsset(); // tạo 1 character detail 

        GUILayout.BeginVertical();

        try
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label("Name");
            characterAssetTemp.Name = GUILayout.TextField(characterAssetTemp.Name, new GUILayoutOption[] { GUILayout.Width(100) });
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("HP");
            characterAssetTemp.BasicHP = float.Parse(GUILayout.TextField(characterAssetTemp.BasicHP.ToString(), new GUILayoutOption[] { GUILayout.Width(100) }));
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("ATK Damage");
            characterAssetTemp.BasicATKDamage = float.Parse(EditorGUILayout.TextField(characterAssetTemp.BasicATKDamage.ToString(), new GUILayoutOption[] { GUILayout.Width(100) }));
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("MS");
            characterAssetTemp.BasicMS = float.Parse(EditorGUILayout.TextField(characterAssetTemp.BasicMS.ToString(), new GUILayoutOption[] { GUILayout.Width(100) }));
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("AS");
            characterAssetTemp.BasicAS = float.Parse(EditorGUILayout.TextField(characterAssetTemp.BasicAS.ToString(), new GUILayoutOption[] { GUILayout.Width(100) }));
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("CR");
            characterAssetTemp.BasicCR = float.Parse(EditorGUILayout.TextField(characterAssetTemp.BasicCR.ToString(), new GUILayoutOption[] { GUILayout.Width(100) }));
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("CD");
            characterAssetTemp.BasicCD = float.Parse(EditorGUILayout.TextField(characterAssetTemp.BasicCD.ToString(), new GUILayoutOption[] { GUILayout.Width(100) }));
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Dodge");
            characterAssetTemp.BasicDodge = float.Parse(EditorGUILayout.TextField(characterAssetTemp.BasicDodge.ToString(), new GUILayoutOption[] { GUILayout.Width(100) }));
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("MR");
            characterAssetTemp.BasicMR = float.Parse(EditorGUILayout.TextField(characterAssetTemp.BasicMR.ToString(), new GUILayoutOption[] { GUILayout.Width(100) }));
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("RR");
            characterAssetTemp.BasicRR = float.Parse(EditorGUILayout.TextField(characterAssetTemp.BasicRR.ToString(), new GUILayoutOption[] { GUILayout.Width(100) }));
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("RL");
            characterAssetTemp.BasicRL = float.Parse(EditorGUILayout.TextField(characterAssetTemp.BasicRL.ToString(), new GUILayoutOption[] { GUILayout.Width(100) }));
            GUILayout.EndHorizontal();
        }
        catch (Exception e)
        {
            Debug.LogError(sNhapSaiER);
        }


        if (GUILayout.Button("Create"))
        {
            try
            {
                AssetDatabase.CreateAsset(characterAssetTemp, link + "/" + characterAssetTemp.Name.ToUpper() + ".asset"); // nén lại thành asset sao đó luu vào dưới tên trong link
                Debug.Log("Tạo nhân vật:" + characterAssetTemp.Name.ToUpper());
                characterAssetTemp = null;
                func = -1;
            }
            catch (Exception e)
            {
                Debug.Log("Nhân vật đã tồn tại");
            }
        }
        GUILayout.EndVertical();
    }
    void LoadCharaterAssetToCharacterDetail(string str = "characterASS") // lấy dữ liệu từ asset và chuyển vào detail
    {
        string[] path = AssetDatabase.FindAssets(str, new string[] { link });

        CharacterAsset asset = AssetDatabase.LoadAssetAtPath(AssetDatabase.GUIDToAssetPath(path[0]), typeof(CharacterAsset)) as CharacterAsset;

        if (asset == null)
        {
            Debug.Log("Không tìm thấy asset, kiểm tra lại link: " + link);
            return;
        }
        Debug.Log("Load asset thành công: " + asset.name);

        GetInforCharacterASS(asset); // gán giá trị cho Character

        func = -1; // kết thúc tính năng
    }
    CharacterAsset LoadCharaterAsset(string str = "characterASS") // lấy dữ liệu từ asset
    {
        string[] path = AssetDatabase.FindAssets(str, new string[] { link });
        if (path == null || path.Length < 1)
        {
            Debug.Log("Không tìm thấy asset, kiểm tra lại link: " + link);
            return null;
        }

        CharacterAsset characterAsset = AssetDatabase.LoadAssetAtPath(AssetDatabase.GUIDToAssetPath(path[0]), typeof(CharacterAsset)) as CharacterAsset;

        if (characterAsset == null)
        {
            Debug.Log("Không tìm thấy asset, kiểm tra lại link: " + link);
            return null;
        }

        Debug.Log("Load asset thành công: " + characterAsset.name);

        return characterAsset;
        //func = -1; // kết thúc tính năng
    }

    void GetInforCharacterASS(CharacterAsset asset) // load data từ asset
    {
        Debug.Log("Đang thiết lập thuộc tính...");
        character = new CharacterDetail
        {
            Name = asset.Name,
            BasicHP = asset.BasicHP,
            BasicATKDamage = asset.BasicATKDamage,
            BasicMS = asset.BasicMS,
            BasicAS = asset.BasicAS,
            BasicCR = asset.BasicCR,
            BasicCD = asset.BasicCD,
            BasicDodge = asset.BasicDodge,
            BasicMR = asset.BasicMR,
            BasicRR = asset.BasicRR,
            BasicRL = asset.BasicRL
        };
        Debug.Log("Hoàn tất thiết lập thuộc tính của Nhân vật: " + character.Name);
    }

    CharacterDetail ConverAssetToDetail(CharacterAsset asset)
    {
        Debug.Log("Đang thiết lập thuộc tính...");
        CharacterDetail character = new CharacterDetail
        {
            Name = asset.Name,
            BasicHP = asset.BasicHP,
            BasicATKDamage = asset.BasicATKDamage,
            BasicMS = asset.BasicMS,
            BasicAS = asset.BasicAS,
            BasicCR = asset.BasicCR,
            BasicCD = asset.BasicCD,
            BasicDodge = asset.BasicDodge,
            BasicMR = asset.BasicMR,
            BasicRR = asset.BasicRR,
            BasicRL = asset.BasicRL
        };
        Debug.Log("Hoàn tất thiết lập thuộc tính của Nhân vật: " + character.Name);

        return character;
    }

    CharacterAsset ConverDetailToAsset(CharacterDetail detail)
    {
        Debug.Log("Đang thiết lập thuộc tính...");
        CharacterAsset character = new CharacterAsset
        {
            Name = detail.Name,
            BasicHP = detail.BasicHP,
            BasicATKDamage = detail.BasicATKDamage,
            BasicMS = detail.BasicMS,
            BasicAS = detail.BasicAS,
            BasicCR = detail.BasicCR,
            BasicCD = detail.BasicCD,
            BasicDodge = detail.BasicDodge,
            BasicMR = detail.BasicMR,
            BasicRR = detail.BasicRR,
            BasicRL = detail.BasicRL
        };
        Debug.Log("Hoàn tất thiết lập thuộc tính của Nhân vật: " + character.Name);

        return character;
    }


    #region ToolChangeColor
    Color color;
    void ToolChangeColor()
    {
        GUILayout.Label("Tool change color", EditorStyles.boldLabel);

        color = EditorGUILayout.ColorField("Color", color); // hiện màu đang chọn và gán cho color.

        if (GUILayout.Button("Apply"))
        {
            foreach (var item in Selection.gameObjects)
            {
                item.GetComponent<Renderer>().material.color = color;
            }
        }
    }
    #endregion

}