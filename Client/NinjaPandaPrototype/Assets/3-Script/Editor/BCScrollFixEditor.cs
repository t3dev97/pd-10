﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
[CustomEditor(typeof(BCScrollFix), true)]

public class BCScrollFixEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        BCScrollFix scrollFix = (BCScrollFix)target;
        if (EditorApplication.isPlaying)
        {
            Debug.Log("AAA");
            scrollFix.isPlay = true;
        }
        else
        {
            Debug.Log("BBBB");
            scrollFix.isPlay = false;
        }
    }
}
