﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using System.Linq;
using System;

namespace Studio
{
    public class ToolDataconfigEditor : Editor
    {
        //[MenuItem("HVL/Tool/RecognizeGlobalConfig")]
        //static void RecognizeGlobalConfig()
        //{
        //    // Dev
        //    DirectoryInfo dirInfo = new DirectoryInfo(Application.dataPath + "/0-AssetBundles/Resources/Config/Global");
        //    List<string> fileNames = new List<string>();
        //    foreach (FileInfo configName in dirInfo.GetFiles("*.txt", SearchOption.AllDirectories))
        //    {
        //        var fileName = configName.Name.Replace(".txt", "");
        //        if (!configName.Name.Equals("_global"))
        //        {
        //            fileNames.Add(fileName);
        //        }
        //    }
        //    var dict = new Dictionary<string, object>();
        //    dict.Add("data", fileNames);
        //    File.WriteAllText(Application.dataPath + "/0-AssetBundles/Resources/Config/Global/_global.txt", dict.DictionaryToJson());

        //    // QC
        //    DirectoryInfo QCdirInfo = new DirectoryInfo(Application.dataPath + "/0-AssetBundles/Resources/Config/QCGlobal");
        //    List<string> QCfileNames = new List<string>();
        //    foreach (FileInfo configName in QCdirInfo.GetFiles("*.txt", SearchOption.AllDirectories))
        //    {
        //        var QCfileName = configName.Name.Replace(".txt", "");
        //        if (!configName.Name.Equals("_global"))
        //        {
        //            QCfileNames.Add(QCfileName);
        //        }
        //    }
        //    var QCdict = new Dictionary<string, object>();
        //    QCdict.Add("data", QCfileNames);
        //    File.WriteAllText(Application.dataPath + "/0-AssetBundles/Resources/Config/QCGlobal/_global.txt", QCdict.DictionaryToJson());

        //    // Online
        //    DirectoryInfo OnlinedirInfo = new DirectoryInfo(Application.dataPath + "/0-AssetBundles/Resources/Config/OnlineGlobal");
        //    List<string> OnlinefileNames = new List<string>();
        //    foreach (FileInfo configName in OnlinedirInfo.GetFiles("*.txt", SearchOption.AllDirectories))
        //    {
        //        var OnlinefileName = configName.Name.Replace(".txt", "");
        //        if (!configName.Name.Equals("_global"))
        //        {
        //            OnlinefileNames.Add(OnlinefileName);
        //        }
        //    }
        //    var Onlinedict = new Dictionary<string, object>();
        //    Onlinedict.Add("data", OnlinefileNames);
        //    File.WriteAllText(Application.dataPath + "/0-AssetBundles/Resources/Config/OnlineGlobal/_global.txt", Onlinedict.DictionaryToJson());
        //}
        [MenuItem("HVL/Tool/AddAllPrefabResource")]
        public static void AddAllPrefabResource()
        {
            if (!Directory.Exists(Application.dataPath+"/0-AssetBundles/Resources/"))
            {
                DebugX.Log("Resources folder not found!");
                return;
            }
            DebugX.Log("Add Resource from: "+ Application.dataPath + "/0-AssetBundles/Resources/");

            //AddResource("BulletResource", "/0-AssetBundles/Resources/Bullet");
            AddResource("Resource_Button", "/0-AssetBundles/Resources/Buttons/Prefabs");
            //AddResource("ChatEmotionResource", "/0-AssetBundles/Resources/Emotion");
            //AddResource("FishPathResource", "/0-AssetBundles/Resources/FishPath/Prefabs");
            AddResource("Resource_Effect", "/0-AssetBundles/Resources/Effect/Prefabs");
            //AddResource("FishResource", "/0-AssetBundles/Resources/Fish/Prefabs");
            //AddResource("GunResource", "/0-AssetBundles/Resources/Gun/Prefabs");
            //AddResource("NetResource", "/0-AssetBundles/Resources/Web");
            //AddResource("SkillResource", "/0-AssetBundles/Resources/Skill");
            //AddResource("WingResource", "/0-AssetBundles/Resources/Wing/Prefabs");

            //AddSoundResource("SoundResource", "/0-AssetBundles/Resources/Sound");

            AddNinjaMapResource("Resource_BlockBrushes", "/Resources/BlockBrushes/");
            AddNinjaMapResource("Resource_BossBrushes", "/Resources/BossBrushes/");
            AddNinjaMapResource("Resource_GroundBrushes", "/Resources/GroundBrushes/", ".mat");
            AddNinjaMapResource("Resource_DoorBrush", "/Resources/DoorBrush/");
            AddNinjaMapResource("Resource_BoderMap", "/Resources/BoderMap/");
            AddNinjaMapResource("Resource_MapCut", "/Resources/MapCut/");
            AddNinjaMapResource("Resource_NPC", "/Resources/NPC/");
            AddNinjaMapResource("Resource_MonsterBrushes", "/Resources/MonsterBrushes/");

            AddNinjaMapWater("Resource_WaterBrushes", "/Resources/WaterBrushes/");

            DebugX.Log("Complete Add Resource prefabs.");
        }
        static void AddResource(string resourceName, string resourcePath)
        {
            
            var dirInfo = new DirectoryInfo(Application.dataPath + resourcePath);
            List<string> itemNames = new List<string>();
            foreach (FileInfo itemFullName in dirInfo.GetFiles("*.prefab", SearchOption.AllDirectories))
            {
                var itemName = itemFullName.Name.Replace(".prefab", "");
                if (!string.IsNullOrEmpty(itemName))
                    itemNames.Add(itemName);
            }
            var itemDict = new Dictionary<string, object>();
            itemDict.Add(BCKey.data, itemNames);
            File.WriteAllText(Application.dataPath + "/0-AssetBundles/Resources/Config/" + resourceName + ".txt", itemDict.DictionaryToJson());
        }

        static void AddSoundResource(string resourceName, string resourcePath)
        {
            var dirInfo = new DirectoryInfo(Application.dataPath + resourcePath);
            List<string> itemNames = new List<string>();
            foreach (FileInfo itemFullName in dirInfo.GetFiles("*.wav", SearchOption.AllDirectories))
            {
                var itemName = itemFullName.Name.Replace(".wav", "");
                if (!string.IsNullOrEmpty(itemName))
                    itemNames.Add(itemName);
            }
            foreach (FileInfo itemFullName in dirInfo.GetFiles("*.mp3", SearchOption.AllDirectories))
            {
                var itemName = itemFullName.Name.Replace(".mp3", "");
                if (!string.IsNullOrEmpty(itemName))
                    itemNames.Add(itemName);
            }
            var itemDict = new Dictionary<string, object>();
            itemDict.Add(BCKey.data, itemNames);
            File.WriteAllText(Application.dataPath + "/0-AssetBundles/Resources/Config/" + resourceName + ".txt", itemDict.DictionaryToJson());
        }

        // Ninja Panda
        static void AddNinjaMapResource (string resourceName, string resourcePath, string objectExtension = ".prefab")
        {
            Dictionary<string, string> itemNames = new Dictionary<string, string>();
            Dictionary<string, object> removedNames = new Dictionary<string, object>();
            var dirInfo = new DirectoryInfo(Application.dataPath + resourcePath);

            if (File.Exists(Application.dataPath + "/0-AssetBundles/Resources/Config/" + resourceName + ".txt"))
            {
                var oldDir = File.ReadAllText(Application.dataPath + "/0-AssetBundles/Resources/Config/" + resourceName + ".txt");
                var oldDict = oldDir.JsonToDictionary();

                if (oldDict != null && oldDict.Count > 0)
                {
                    var oldData = oldDict.Get<Dictionary<string, object>>(BCKey.data);
                    Dictionary<string, object> removedOldData = new Dictionary<string, object>();
                    if (oldDict.ContainsKey(BCKey.removed_data))
                    {
                        removedNames = oldDict.Get<Dictionary<string, object>>(BCKey.removed_data);
                        removedOldData = new Dictionary<string, object>(removedNames);
                    }
                    if (oldData != null && oldData.Count > 0)
                    {
                        foreach (KeyValuePair<string, object> oldItem in oldData)
                        {
                            if (File.Exists(dirInfo + oldItem.Value.ToString() + objectExtension))
                                itemNames.Add(oldItem.Key, oldItem.Value.ToString());
                            else if (!removedNames.ContainsValue(oldItem.Value.ToString()))
                                removedNames.Add(oldItem.Key, oldItem.Value.ToString());
                        }
                    }
                    if (removedNames != null && removedNames.Count>0)
                    {
                        foreach (KeyValuePair<string,object> removedOldItem in removedOldData)
                        {
                            if (File.Exists (dirInfo+ removedOldItem.Value.ToString() + objectExtension) && !itemNames.ContainsValue(removedOldItem.Value.ToString()))
                            {
                                itemNames.Add(removedOldItem.Key, removedOldItem.Value.ToString());
                                if (removedNames.ContainsKey(removedOldItem.Key))
                                    removedNames.Remove(removedOldItem.Key);
                            }
                        }
                    }
                }
            }
          
            int i = GetHighestKeyInDict(itemNames);
            foreach (FileInfo itemFullName in dirInfo.GetFiles("*"+ objectExtension, SearchOption.AllDirectories))
            {
                var itemName = itemFullName.Name.Replace(objectExtension, "");
                   
                if (!string.IsNullOrEmpty(itemName) && !itemNames.ContainsValue (itemName))
                {
                    itemNames.Add(i.ToString(), itemName);
                    i++;
                }
               
            }
            OrderDictionaryWithIntKey(itemNames);
            var itemDict = new Dictionary<string, object>();

            itemDict.Add(BCKey.data, itemNames);
            if (removedNames!=null && removedNames.Count > 0)
            {
                OrderDictionaryWithIntKey(removedNames);
                itemDict.Add(BCKey.removed_data, removedNames);
            }
            File.WriteAllText(Application.dataPath + "/0-AssetBundles/Resources/Config/" + resourceName +".txt", itemDict.DictionaryToJson());
        }

        static void AddNinjaMapWater(string resourceName, string resourcePath, string objectExtension = ".prefab")
        {
            var dirInfo = new DirectoryInfo(Application.dataPath + resourcePath);
            var folderDict = new Dictionary<string, object>();

            var removedFolderDict = new Dictionary<string, object>();

            if (File.Exists(Application.dataPath + "/0-AssetBundles/Resources/Config/" + resourceName + ".txt"))
            {
                var oldDir = File.ReadAllText(Application.dataPath + "/0-AssetBundles/Resources/Config/" + resourceName + ".txt");
                var oldDict = oldDir.JsonToDictionary();

                if (oldDict != null && oldDict.Count > 0)
                {
                    var oldData = oldDict.Get<Dictionary<string, object>>(BCKey.data);
                    if (oldDict.ContainsKey(BCKey.removed_data))
                        removedFolderDict = oldDict.Get<Dictionary<string,object>>(BCKey.removed_data);
                    if (oldData != null && oldData.Count > 0)
                    {
                        foreach (KeyValuePair<string, object> oldFolderItem in oldData)
                        {
                            if (Directory.Exists(dirInfo + oldFolderItem.Key))
                            {
                                Dictionary<string, object> oldItemDict = (Dictionary<string, object>)oldFolderItem.Value;
                                Dictionary<string, string> itemNames = new Dictionary<string, string>();

                                Dictionary<string, string> removedItemNames = new Dictionary<string, string>();
                                if (removedFolderDict.ContainsKey(oldFolderItem.Key))
                                {
                                    removedItemNames = removedFolderDict.Get<Dictionary<string, string>>(oldFolderItem.Key);
                                }

                                foreach (KeyValuePair<string, object> oldItem in oldItemDict)
                                {
                                    if (File.Exists(dirInfo + oldFolderItem.Key + "/" + oldItem.Value.ToString() + objectExtension))
                                        itemNames.Add(oldItem.Key, oldItem.Value.ToString());
                                    else if (!removedItemNames.ContainsValue(oldItem.Value.ToString()))
                                    {
                                        removedItemNames.Add(oldItem.Key, oldItem.Value.ToString());
                                    }
                                }

                                OrderDictionaryWithIntKey(itemNames);
                                OrderDictionaryWithIntKey(removedItemNames);

                                folderDict.Add(oldFolderItem.Key, itemNames);
                                if (removedItemNames!=null && removedItemNames.Count>0)
                                    removedFolderDict.Add(oldFolderItem.Key, removedItemNames);
                            }
                            else
                            {
                                removedFolderDict.Add(oldFolderItem.Key, null);
                            }
                        }
                    }
                    var removedOldData = oldDict.Get<Dictionary<string, object>>(BCKey.removed_data);
                    var tempFolder = new Dictionary<string,object>();
                    if (removedOldData !=null && removedOldData.Count>0)
                    {
                        foreach (KeyValuePair<string,object> removedOldFolderItem in removedOldData)
                        {
                            if (Directory.Exists (dirInfo+ removedOldFolderItem.Key))
                            {
                                if (!tempFolder.ContainsKey(removedOldFolderItem.Key))
                                    tempFolder.Add(removedOldFolderItem.Key, new Dictionary<string, string>());

                                Dictionary<string, object> removedOldItemDict = (Dictionary<string, object>)removedOldFolderItem.Value;
                                Dictionary<string, string> removedItemNames = (Dictionary<string, string>) folderDict[removedOldFolderItem.Key];
                                if (removedItemNames == null)
                                    removedItemNames = new Dictionary<string, string>();
                                foreach (KeyValuePair<string, object> removedOldItem in removedOldItemDict)
                                {
                                    var tempFiles = new Dictionary<string,string>();
                                    if (File.Exists(dirInfo + removedOldFolderItem.Key + "/" + removedOldItem.Value.ToString() + objectExtension) &&
                                        !removedItemNames.ContainsValue(removedOldItem.Value.ToString()))
                                    {
                                        removedItemNames.Add(removedOldItem.Key, removedOldItem.Value.ToString());
                                        OrderDictionaryWithIntKey(removedItemNames);
                                        tempFiles.Add(removedOldItem.Key, removedOldItem.Value.ToString());
                                        tempFolder[removedOldFolderItem.Key] = tempFiles;
                                    }
                                }
                                if (!folderDict.ContainsKey(removedOldFolderItem.Key)
                                    && removedItemNames != null && removedItemNames.Count > 0)
                                {
                                    OrderDictionaryWithIntKey(removedItemNames);
                                    folderDict.Add(removedOldFolderItem.Key, removedItemNames);
                                }


                            }
                        }
                    }
                    if (tempFolder!=null && tempFolder.Count>0)
                    {
                        foreach (var tempValue in tempFolder)
                        {
                            if (removedOldData.ContainsKey(tempValue.Key))
                            {
                                Dictionary<string, string> tempFiles = (Dictionary<string,string>)tempValue.Value;
                                foreach (var tempFile in tempFiles)
                                {
                                    Dictionary<string, object> removeOldFiles = (Dictionary<string, object>)removedOldData[tempValue.Key];
                                    removeOldFiles.Remove(tempFile.Key);
                                    if (removeOldFiles.Count <= 0)
                                        removedOldData.Remove(tempValue.Key);
                                }
                            }
                        }
                    }
                }
            }


            foreach (DirectoryInfo folderFullName in dirInfo.GetDirectories())
            {
                Dictionary<string, string> itemNames = folderDict.Get<Dictionary<string,string>>(folderFullName.Name);
                if (itemNames == null)
                    itemNames = new Dictionary<string, string>();

                int i = GetHighestKeyInDict(itemNames);
                foreach (FileInfo fileFullName in folderFullName.GetFiles("*"+ objectExtension, SearchOption.AllDirectories))
                {
                    var itemName = fileFullName.Name.Replace(objectExtension, "");
                    if (!string.IsNullOrEmpty(itemName) && !itemNames.ContainsValue(itemName))
                    {
                        itemNames.Add(i.ToString(), itemName);
                        i++;
                    }
                }
                if (folderDict.ContainsKey (folderFullName.Name))
                {
                    var items = folderDict.Get<Dictionary<string, string>>(folderFullName.Name);
                    foreach (var itemName in itemNames)
                    {
                        if (!items.ContainsKey(itemName.Key))
                            items.Add(itemName.Key, itemName.Value.ToString());
                    }
                }
                else
                {
                    OrderDictionaryWithIntKey(itemNames);
                    folderDict.Add(folderFullName.Name, itemNames);
                }
            }
            var itemDict = new Dictionary<string, object>();
            itemDict.Add(BCKey.data, folderDict);
            if (removedFolderDict != null && removedFolderDict.Count > 0)
                itemDict.Add(BCKey.removed_data, removedFolderDict);
            File.WriteAllText(Application.dataPath + "/0-AssetBundles/Resources/Config/" + resourceName + ".txt", itemDict.DictionaryToJson());
        }
        static private int GetHighestKeyInDict(Dictionary<string,string> dict)
        {
            int[] a = new int[dict.Count];
            int j = 0;
            foreach (var item in dict)
            {
                a[j] = int.Parse(item.Key);
                j++;
            }
            return Mathf.Max(a)+1;
        }
        static private void OrderDictionaryWithIntKey(Dictionary<string,string> dict)
        {
            if (dict == null || dict.Count <= 0)
                return;

            var intArray = new int[dict.Count];
            int i = 0;
            foreach (var item in dict)
            {
                intArray[i] = int.Parse(item.Key);
                i++;
            }
            for (int j = 1; j <intArray.Length;j++)
            {
                int temp = intArray[j];
                int k = j - 1;
                for (; k>=0 && intArray[k]>temp;k--)
                {
                    intArray[k + 1] = intArray[k];
                }
                intArray[k + 1] = temp;
            }
            var tempDict = new Dictionary<string, string>(dict);
            dict.Clear();
            foreach (int item in intArray)
                dict.Add(item.ToString(), tempDict[item.ToString()]);
        }
        static private void OrderDictionaryWithIntKey(Dictionary<string, object> dict)
        {
            var intArray = new int[dict.Count];
            int i = 0;
            foreach (var item in dict)
            {
                intArray[i] = int.Parse(item.Key);
                i++;
            }
            for (int j = 1; j < intArray.Length; j++)
            {
                int temp = intArray[j];
                int k = j - 1;
                for (; k >= 0 && intArray[k] > temp; k--)
                {
                    intArray[k + 1] = intArray[k];
                }
                intArray[k + 1] = temp;
            }
            var tempDict = new Dictionary<string, object>(dict);
            dict.Clear();
            foreach (int item in intArray)
                dict.Add(item.ToString(), tempDict[item.ToString()]);
        }
    }
}