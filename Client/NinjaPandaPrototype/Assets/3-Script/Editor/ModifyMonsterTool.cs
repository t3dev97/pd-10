﻿using System;
using UnityEditor;
using UnityEngine;

public class ModifyMonsterTool : EditorWindow
{
    //public static ModifyMonsterTool window;

    Monster _monsterTemp; // chứa các thông tin tạm thời của monster

    //TypeBtn btn;

    string linkMonster = "Assets/Resources/ModifyTool/Monster";
    string linkMap = "Assets/Resources";
    string linkBonus = "Assets/Resources/ModifyTool/Map";
    string nameFileBonus = "BonusStatAsset";
    string sNhapSaiER = "Nhập sai kiểu dữ liệu, nếu bạn nhập số THẬP PHÂN thì có thể bỏ qua lỗi này, nếu vẫn bị vui lòng liên hệ anim";
    enum FUNC
    {
        none, // màn hình chính
        create, // màn hình create
        modify, // màn hình tìm và thay đổi thông số
        bonus // màn hình chỉnh sửa thông tin bonus và tỉ lệ xuât hiện của vòng quay...
    }
    FUNC func = FUNC.none;

    [MenuItem("Window/Modify Monster Tool %w")]
    public static void Init() // bật window editor
    {
        // cách 1:                                       true: sẻ tạo thành cửa sổ tách biệt
        EditorWindow.GetWindow(typeof(ModifyMonsterTool), false, "Modify Monster Tool");

        // cách 2:
        //window = (ModifyMonsterTool)EditorWindow.GetWindow(typeof(ModifyMonsterTool));
        //window.Show();
    }
    private void Awake()
    {
        _monsterTemp = new Monster();
        bonus = new BonusStatAsset();
        //GUI.ScrollTo(new Rect(Screen.w))
    }

    bool btnCreate = false; // trạng thái của btn Create
    bool btnModify = false; // trạng thái của btn Modify
    bool btnBonus = false; // trạng thái của btn Bonus
    private void OnGUI()
    {
        GUILayout.BeginVertical();
        //------------------
        AddControl();
        AddEvent();
        Process();
        //------------------
        GUILayout.EndVertical();
    }

    void AddControl() // Vẽ layout và add và biến bool tương ứng 
    {
        GUILayout.BeginHorizontal();
        btnCreate = GUILayout.Button("Create new Monster");
        btnModify = GUILayout.Button("Modify monter");
        btnBonus = GUILayout.Button("Bonus Stage");
        GUILayout.EndHorizontal();
    }

    void AddEvent()// xử lý sự kiện cho từng button
    {
        if (btnCreate)
        {
            // reset
            _monsterTemp = new Monster();
            //---------------
            func = FUNC.create;
        }
        else
        if (btnModify)
        {
            // reset/ setup 
            nameMonster = "";
            _monsterTemp = new Monster();
            btnFind = false;
            //----------------
            func = FUNC.modify;
        }
        else
        if (btnBonus)
        {
            // reset/ setup
            //maps = new List<MapChapEditor>();
            bonus = new BonusStatAsset();
            //maps = LoadListMapAssetToMap(); // load danh sách map từ map asset
            //bonus = LoadBonusAsset();
            SetupDataToProcess();
            func = FUNC.bonus;
        }
    }


    void SetupDataToProcess() // thiết lập các thông số: số map, số round từ tool thiết lập map qua
    {
        // để tránh trường hợp số map/round bên tool thiết kế map thay đẩy
        // giải phải:
        // trong chương trình, tạo 1 biến tạm(bonus) để xử lý
        // bonus sẻ lấy số map/round trong tool thiết kế và thiết lập trong phiên làm việc này
        // sau đó các thông số thay đổi (hp/atk...) thây đổi sẻ được gán từ đầu->cuối
        // do dó xuất hiện các trường hợp:
        // th1:
        // số map/round trong tool thiết lập map giảm - > không ảnh hưởng đến code 
        // th2:
        // số map/round trong tool thiết lập map tăng - > cần sử lý thêm map/round cho biến tạm, và lỗi quá giới hạn khi đọc asset đã lưu
        //Debug.Log(maps.Count);

        BonusStatAsset fromAssetHasSave = LoadBonusAsset(); // load danh sách map từ bonus asset
                                                            // trộn 2 danh sách map asset và bonus asset lại, số lượng map thì lấy từ map asset, dữ liệu map thì lấy từ bonus asset
        int numberBonusMapAsset = fromAssetHasSave.Maps.Count;
        maps = fromAssetHasSave;
        bonus = maps;

        //Debug.Log("numberBonusMapAsset " + numberBonusMapAsset);
        //if (numberBonusMapAsset < 0)
        //    bonus.Maps.Add(new RoundBonus());

        //for (int i = 0; i < maps.Count; i++)
        //{
        //    bonus.Maps.Add(new RoundBonus());
        //    for (int j = 0; j < maps[i].listRound.Count; j++)
        //    {
        //        if (fromAssetHasSave.Maps.Count > 0 && j < fromAssetHasSave.Maps[i].Rounds.Count && fromAssetHasSave.Maps[i].Rounds.Count > 0)
        //            bonus.Maps[i].Rounds.Add(fromAssetHasSave.Maps[i].Rounds[j]);
        //        else
        //            bonus.Maps[i].Rounds.Add(new InfoBonus());
        //    }
        //    Debug.Log("BONUS Map " + i + " Co " + bonus.Maps[i].Rounds.Count + " Round");
        //}
    }

    BonusStatAsset LoadBonusAsset()
    {
        string[] path = AssetDatabase.FindAssets(nameFileBonus, new string[] { linkBonus });
        return AssetDatabase.LoadAssetAtPath(AssetDatabase.GUIDToAssetPath(path[0]), typeof(BonusStatAsset)) as BonusStatAsset;
    }

    void Process()
    {
        switch (func)
        {
            case FUNC.create:
                {
                    CreateNewMonster();
                    break;
                }
            case FUNC.modify:
                {
                    ModifyMonster();
                    break;
                }
            case FUNC.bonus:
                {
                    Bonus();
                    break;
                }
        }
    }

    #region Bonus
    //List<MapChapEditor> LoadListMapAssetToMap(string nameFileAssset = "DataMapEditor")
    //{
    //    string[] path = AssetDatabase.FindAssets(nameFileAssset, new string[] { linkMap });
    //    BC_DataMapEditor mapEditor = AssetDatabase.LoadAssetAtPath(AssetDatabase.GUIDToAssetPath(path[0]), typeof(BC_DataMapEditor)) as BC_DataMapEditor;
    //    Debug.Log("Load thành công " + mapEditor);
    //    Debug.Log("Số lượng map " + mapEditor.listMap.Count);
    //    return mapEditor.listMap;
    //}

    BonusStatAsset maps; // chứa danh sách các map, trong 1 map sẻ có danh sách các round 
    BonusStatAsset bonus;
    int currentMap = 1;
    int currentRound = 0;
    bool btnNextMap = false;
    bool btnNextRound = false;
    bool btnBackMap = false;
    bool btnBackRound = false;
    void Bonus()
    {
        GUILayout.BeginVertical();
        //---------------------------
        AddControlBonus();
        AddEvenBouns();
        //---------------------------
        GUILayout.EndVertical();
    }

    void AddControlBonus()
    {
        GUILayout.BeginHorizontal();
        GUILayout.Label("Chapter (Map): ");
        btnBackMap = GUILayout.Button("<", new GUILayoutOption[] { GUILayout.Width(30) });// btn lùi
                                                                                          //currentMap = int.SetData(GUILayout.TextField(currentMap.ToString()));
        GUILayout.Box(currentMap.ToString(), new GUILayoutOption[] { GUILayout.Width(30) });
        btnNextMap = GUILayout.Button(">", new GUILayoutOption[] { GUILayout.Width(30) });// btn tới
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label("Stage (Round):");
        btnBackRound = GUILayout.Button("<", new GUILayoutOption[] { GUILayout.Width(30) });// btn lùi
                                                                                            //currentMap = int.SetData(GUILayout.TextField(currentMap.ToString()));
        GUILayout.Box(currentRound.ToString(), new GUILayoutOption[] { GUILayout.Width(30) });
        btnNextRound = GUILayout.Button(">", new GUILayoutOption[] { GUILayout.Width(30) });// btn tới
        GUILayout.EndHorizontal();

        try
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label("Bonus HP:");
            //Debug.Log(bonus.Maps[currentMap].Rounds.Count);
            bonus.Maps[currentMap - 1].Rounds[currentRound].BonusHP = float.Parse(EditorGUILayout.TextField(checkFloat(bonus.Maps[currentMap - 1].Rounds[currentRound].BonusHP, 0, 100).ToString(), new GUILayoutOption[] { GUILayout.Width(30) }));
            GUILayout.Label("%", new GUILayoutOption[] { GUILayout.Width(30) });
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Bonus ATK:");
            bonus.Maps[currentMap - 1].Rounds[currentRound].BonusATK = float.Parse(EditorGUILayout.TextField(checkFloat(bonus.Maps[currentMap - 1].Rounds[currentRound].BonusATK).ToString(), new GUILayoutOption[] { GUILayout.Width(30) }));
            GUILayout.Label("%", new GUILayoutOption[] { GUILayout.Width(30) });
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Tỷ lệ xuất hiện VÒNG QUY MAY MẮN:");
            //Debug.Log(bonus.Maps[currentMap].Rounds.Count);
            bonus.Maps[currentMap - 1].Rounds[currentRound].VongQuayMayMan = float.Parse(EditorGUILayout.TextField(checkFloat(bonus.Maps[currentMap - 1].Rounds[currentRound].VongQuayMayMan).ToString(), new GUILayoutOption[] { GUILayout.Width(30) }));
            GUILayout.Label("%", new GUILayoutOption[] { GUILayout.Width(30) });
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Tỷ lệ xuất hiện THƯƠNG NHÂN THẦN BÍ:");
            bonus.Maps[currentMap - 1].Rounds[currentRound].ThuongNhanThanBi = float.Parse(EditorGUILayout.TextField(checkFloat(bonus.Maps[currentMap - 1].Rounds[currentRound].ThuongNhanThanBi, 0, 100).ToString(), new GUILayoutOption[] { GUILayout.Width(30) }));
            GUILayout.Label("%", new GUILayoutOption[] { GUILayout.Width(30) });
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Tỷ lệ xuất hiện THỎA THUẬN ÁC QUỶ:");
            bonus.Maps[currentMap - 1].Rounds[currentRound].ThoaThuanAcQuy = float.Parse(EditorGUILayout.TextField(checkFloat(bonus.Maps[currentMap - 1].Rounds[currentRound].ThoaThuanAcQuy).ToString(), new GUILayoutOption[] { GUILayout.Width(30) }));
            GUILayout.Label("%", new GUILayoutOption[] { GUILayout.Width(30) });
            GUILayout.EndHorizontal();
        }
        catch (System.Exception e)
        {
            Debug.LogError(sNhapSaiER);
        }
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Save"))
        {
            //string[] path = AssetDatabase.FindAssets(nameFileBonus, new string[] { linkBonus });
            //BonusStatAsset temp = AssetDatabase.LoadAssetAtPath(AssetDatabase.GUIDToAssetPath(path[0]), typeof(BonusStatAsset)) as BonusStatAsset;
            //AssetDatabase.CreateAsset(bonus, linkBonus + "/" + nameFileBonus + ".asset"); // ghi đè lên cái cũ
            //Debug.Log(temp.Maps[0].Rounds[0].BonusHP);
            //AssetDatabase.SaveAssets();
            //EditorUtility.SetDirty(temp);
            EditorUtility.SetDirty(bonus);
            Debug.Log("Thay đổi thiết lập thành công");
            func = FUNC.none;
        }
        GUILayout.EndHorizontal();

        GUILayout.Label("Khi đang chỉnh sửa một thông số, nếu chuyển MAP hoặc ROUND thì ô đang nhập sẻ không được RESET, DI CHUYỂN SANG Ô KHÁC để RESET");
    }
    float checkFloat(float value, float min = 0, float max = 100)
    {
        if (value > max) return max;
        if (value < min) return min;
        return value;
    }
    void AddEvenBouns()
    {
        if (btnNextMap)
        {
            if (currentMap < maps.Maps.Count) // index map phải nhỏ hơn số lượng map
            {

                ++currentMap;
                currentRound = 0;
            }
            else
            {
                maps.Maps.Add(new RoundBonus());
                maps.Maps[maps.Maps.Count - 1].Rounds.Add(new InfoBonus());
                Debug.LogError("Đây là CHAPTER cuối");
            }
        }

        if (btnBackMap)
        {
            if (currentMap > 1)
                --currentMap;
            else
                Debug.LogWarning("Đây CHAPTER đầu");
        }

        if (btnNextRound)
        {
            if (currentRound < maps.Maps[currentMap - 1].Rounds.Count - 1) // index round phải nhỏ hơn số lượng round
                ++currentRound;
            else
            {
                maps.Maps[currentMap - 1].Rounds.Add(new InfoBonus());
                Debug.LogError("Đây là STAGE cuối");
            }
        }

        if (btnBackRound)
        {
            if (currentRound > 0)
                --currentRound;
            else
                Debug.LogWarning("Đây STAGE đầu");
        }
    }

    void ProcessBouns()
    {
        Debug.Log("ProcessBouns");
    }

    #endregion

    #region Modify
    bool btnFind = false;
    string nameMonster;
    private void ModifyMonster()
    {
        GUILayout.BeginVertical();

        if (btnFind == false)
        {
            GUILayout.Label("Find name:");
            nameMonster = GUILayout.TextField(nameMonster, new GUILayoutOption[] { GUILayout.Width(100) });
            btnFind = GUILayout.Button("Find");
            if (btnFind)
            {
                string[] path = AssetDatabase.FindAssets(nameMonster, new string[] { linkMonster });
                if (path == null || path.Length < 1)
                {
                    btnFind = false;
                    Debug.LogError("Không tìm thấy file " + nameMonster);
                    return;
                }
                MonsterAsset asset = AssetDatabase.LoadAssetAtPath(AssetDatabase.GUIDToAssetPath(path[0]), typeof(MonsterAsset)) as MonsterAsset;
                _monsterTemp = ConverAssetToMonster(asset);
                Debug.Log("Load thành công file tìm thấy");
            }
        }
        else
        {
            EnterInfo();
            if (GUILayout.Button("Save"))
            {
                string[] path = AssetDatabase.FindAssets(nameMonster, new string[] { linkMonster });

                MonsterAsset asset = AssetDatabase.LoadAssetAtPath(AssetDatabase.GUIDToAssetPath(path[0]), typeof(MonsterAsset)) as MonsterAsset;
                AssetDatabase.StartAssetEditing();
                asset.NameMonster = _monsterTemp.NameMonster;
                asset.Id = _monsterTemp.Id;
                asset.BasicATKDamge = _monsterTemp.BasicATKDamge;
                asset.BasicHP = _monsterTemp.BasicHP;
                asset.BasicMS = _monsterTemp.BasicMS;
                asset.BasicASMeeleUnit = _monsterTemp.BasicASMeeleUnit;
                asset.BasicASRangeUnit = _monsterTemp.BasicASRangeUnit;
                asset.BasicCoolDownMax = _monsterTemp.BasicCoolDownMax;
                //asset.TypeAtks = monster.TypeATKs;
                //asset.ItemDrops = monster.ItemDrops;
                asset.HeartDropRate = _monsterTemp.HeartDropRate;
                asset.GoldNumberMax = _monsterTemp.GoldNumberMax;
                //asset.MonsterClass = monster.MonsterClass;
                AssetDatabase.StopAssetEditing();
                EditorUtility.SetDirty(asset);
                //AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();
                Debug.Log("Chỉnh sửa thành công");
                func = FUNC.none;
            }
        }
        GUILayout.EndVertical();
    }
    #endregion


    #region Create
    void CreateNewMonster()
    {
        GUILayout.BeginVertical();

        EnterInfo();

        if (GUILayout.Button("Create"))
        {
            AssetDatabase.CreateAsset(ConverMonsterToAsset(_monsterTemp), linkMonster + "/" + _monsterTemp.NameMonster.ToUpper() + ".asset");
            Debug.Log("Hoàn thành tạo đối tượng: " + _monsterTemp.NameMonster.ToUpper());
            _monsterTemp = null;
            func = FUNC.none;
        }

        GUILayout.EndVertical();
    }
    #endregion

    Monster LoadAssetToMonster()
    {
        Monster monster = new Monster();
        string[] path1 = AssetDatabase.FindAssets(nameMonster, new string[] { linkMonster });

        MonsterAsset asset = AssetDatabase.LoadAssetAtPath(path1[0], typeof(CharacterAsset)) as MonsterAsset;
        monster.Id = asset.Id;
        monster.BasicHP = asset.BasicHP;
        monster.BasicMS = asset.BasicMS;
        monster.BasicATKDamge = asset.BasicATKDamge;
        monster.BasicASMeeleUnit = asset.BasicASMeeleUnit;
        monster.BasicASRangeUnit = asset.BasicASRangeUnit;
        monster.BasicCoolDownMax = asset.BasicCoolDownMax;
        //monster.TypeATKs = asset.TypeAtks;
        //monster.ItemDrops = asset.ItemDrops;
        monster.HeartDropRate = asset.HeartDropRate;
        monster.GoldNumberMax = asset.GoldNumberMax;
        //monster.MonsterClass = asset.MonsterClass;
        Debug.Log("Load thành công file tìm thấy");
        return monster;
    }

    int currentItemDrop = 1; // index Type ATK

    void EnterInfo()
    {
        try
        {
            GUILayout.Label("Main");
            GUILayout.BeginHorizontal();
            GUILayout.Space(10);
            GUILayout.BeginVertical();
            #region Main
            GUILayout.BeginHorizontal();
            GUILayout.Label("Name");
            _monsterTemp.NameMonster = EditorGUILayout.TextField(_monsterTemp.NameMonster, new GUILayoutOption[] { GUILayout.Width(100) });
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("ID");
            _monsterTemp.Id = int.Parse(EditorGUILayout.TextField(_monsterTemp.Id.ToString(), new GUILayoutOption[] { GUILayout.Width(100) }));
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Basic HP");
            _monsterTemp.BasicHP = float.Parse(EditorGUILayout.TextField(_monsterTemp.BasicHP.ToString(), new GUILayoutOption[] { GUILayout.Width(100) }));
            GUILayout.EndHorizontal();
            #endregion
            GUILayout.Space(10);
            GUILayout.EndVertical();
            GUILayout.EndHorizontal();
            //------------------- end
            GUILayout.Label("ATK");
            GUILayout.BeginHorizontal();
            GUILayout.Space(10);
            GUILayout.BeginVertical();
            #region ATK
            GUILayout.BeginHorizontal();
            GUILayout.Label("Basic ATK Damage");
            _monsterTemp.BasicATKDamge = float.Parse(EditorGUILayout.TextField(_monsterTemp.BasicATKDamge.ToString(), new GUILayoutOption[] { GUILayout.Width(100) }));
            GUILayout.EndHorizontal();
            #endregion
            GUILayout.Space(10);
            GUILayout.EndVertical();
            GUILayout.EndHorizontal();
            //-------------------- end
            GUILayout.Label("Speeds");
            GUILayout.BeginHorizontal();
            GUILayout.Space(10);
            GUILayout.BeginVertical();
            #region Speeds
            GUILayout.BeginHorizontal();
            GUILayout.Label("Basic Move Speed");
            _monsterTemp.BasicMS = float.Parse(EditorGUILayout.TextField(_monsterTemp.BasicMS.ToString(), new GUILayoutOption[] { GUILayout.Width(100) }));
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Basic AS Meele Unit (for Dev)");
            _monsterTemp.BasicASMeeleUnit = float.Parse(EditorGUILayout.TextField(_monsterTemp.BasicASMeeleUnit.ToString(), new GUILayoutOption[] { GUILayout.Width(100) }));
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Basic AS Range Unit (for Dev)");
            _monsterTemp.BasicASRangeUnit = float.Parse(EditorGUILayout.TextField(_monsterTemp.BasicASRangeUnit.ToString(), new GUILayoutOption[] { GUILayout.Width(100) }));
            GUILayout.EndHorizontal();
            #endregion
            GUILayout.Space(10);
            GUILayout.EndVertical();
            GUILayout.EndHorizontal();
            //-------------------- end
            GUILayout.Label("Cooldown");
            GUILayout.BeginHorizontal();
            GUILayout.Space(10);
            GUILayout.BeginVertical();
            #region CoolDown
            GUILayout.BeginHorizontal();
            GUILayout.Label("Basic Cool Down Min");
            _monsterTemp.BasicCoolDownMin = float.Parse(EditorGUILayout.TextField(_monsterTemp.BasicCoolDownMin.ToString(), new GUILayoutOption[] { GUILayout.Width(100) }));
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Basic Cool Down Max");
            _monsterTemp.BasicCoolDownMax = float.Parse(EditorGUILayout.TextField(_monsterTemp.BasicCoolDownMax.ToString(), new GUILayoutOption[] { GUILayout.Width(100) }));
            GUILayout.EndHorizontal();
            #endregion
            GUILayout.Space(10);
            GUILayout.EndVertical();
            GUILayout.EndHorizontal();
            //----------------------- end
            GUILayout.Label("Monster Type");
            GUILayout.BeginHorizontal();
            GUILayout.Space(10);
            GUILayout.BeginVertical();
            #region Monster Type
            GUILayout.BeginHorizontal();
            GUILayout.Label("Race type");
            _monsterTemp.RaceType = (MONSTER_RACE_TYPE)EditorGUILayout.EnumPopup(_monsterTemp.RaceType, new GUILayoutOption[] { GUILayout.Width(100) });
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("ATK Type");
            _monsterTemp.AtkType = (MONSTER_ATK_TYPE)EditorGUILayout.EnumPopup(_monsterTemp.AtkType, new GUILayoutOption[] { GUILayout.Width(100) });
            GUILayout.EndHorizontal();
            #endregion
            GUILayout.Space(10);
            GUILayout.EndVertical();
            GUILayout.EndHorizontal();
            //-------------------- end
            GUILayout.Label("Healt");
            GUILayout.BeginHorizontal();
            GUILayout.Space(10);
            GUILayout.BeginVertical();
            #region Healt
            GUILayout.BeginHorizontal();
            GUILayout.Label("Healt Drop Rate");
            _monsterTemp.HeartDropRate = float.Parse(EditorGUILayout.TextField(_monsterTemp.HeartDropRate.ToString(), new GUILayoutOption[] { GUILayout.Width(100) }));
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Healt Count Min");
            _monsterTemp.HeartNumberMin = float.Parse(EditorGUILayout.TextField(_monsterTemp.HeartNumberMin.ToString(), new GUILayoutOption[] { GUILayout.Width(100) }));
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Healt Count Max");
            _monsterTemp.HeartNumberMax = float.Parse(EditorGUILayout.TextField(_monsterTemp.HeartNumberMax.ToString(), new GUILayoutOption[] { GUILayout.Width(100) }));
            GUILayout.EndHorizontal();
            #endregion
            GUILayout.Space(10);
            GUILayout.EndVertical();
            GUILayout.EndHorizontal();
            //------------------------ end
            GUILayout.Label("Gold");
            GUILayout.BeginHorizontal();
            GUILayout.Space(10);
            GUILayout.BeginVertical();
            #region Gold
            GUILayout.BeginHorizontal();
            GUILayout.Label("Gold Count Min");
            _monsterTemp.GoldNumberMin = float.Parse(EditorGUILayout.TextField(_monsterTemp.GoldNumberMin.ToString(), new GUILayoutOption[] { GUILayout.Width(100) }));
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Gold Count Max");
            _monsterTemp.GoldNumberMax = float.Parse(EditorGUILayout.TextField(_monsterTemp.GoldNumberMax.ToString(), new GUILayoutOption[] { GUILayout.Width(100) }));
            GUILayout.EndHorizontal();
            #endregion
            GUILayout.Space(10);
            GUILayout.EndVertical();
            GUILayout.EndHorizontal();
            //----------------------- end
            GUILayout.Label("Offset Selection Position");
            GUILayout.BeginHorizontal();
            GUILayout.Space(10);
            GUILayout.BeginVertical();
            #region Offset Selection Position
            GUILayout.BeginHorizontal();
            GUILayout.Label("Offset Selection Top");
            Vector3 offSetSelectionTop_Temp = _monsterTemp.OffSetSelectionTop;
            offSetSelectionTop_Temp.x = float.Parse(EditorGUILayout.TextField(offSetSelectionTop_Temp.x.ToString(), new GUILayoutOption[] { GUILayout.Width(100) }));
            offSetSelectionTop_Temp.y = float.Parse(EditorGUILayout.TextField(offSetSelectionTop_Temp.y.ToString(), new GUILayoutOption[] { GUILayout.Width(100) }));
            offSetSelectionTop_Temp.z = float.Parse(EditorGUILayout.TextField(offSetSelectionTop_Temp.z.ToString(), new GUILayoutOption[] { GUILayout.Width(100) }));
            _monsterTemp.OffSetSelectionTop = offSetSelectionTop_Temp;
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Offset Selection Scale Foot");
            Vector3 offSetSelectionScaleFoot_Temp = _monsterTemp.OffSetSelectionScaleFoot;
            offSetSelectionScaleFoot_Temp.x = float.Parse(EditorGUILayout.TextField(offSetSelectionScaleFoot_Temp.x.ToString(), new GUILayoutOption[] { GUILayout.Width(100) }));
            offSetSelectionScaleFoot_Temp.y = float.Parse(EditorGUILayout.TextField(offSetSelectionScaleFoot_Temp.y.ToString(), new GUILayoutOption[] { GUILayout.Width(100) }));
            offSetSelectionScaleFoot_Temp.z = float.Parse(EditorGUILayout.TextField(offSetSelectionScaleFoot_Temp.z.ToString(), new GUILayoutOption[] { GUILayout.Width(100) }));
            _monsterTemp.OffSetSelectionScaleFoot = offSetSelectionScaleFoot_Temp;
            GUILayout.EndHorizontal();

            #endregion
            //---------------------- end
            GUILayout.Space(10);
            GUILayout.EndVertical();
            GUILayout.EndHorizontal();

        }
        catch (Exception e)
        {
            Debug.LogError(sNhapSaiER);
        }
    }

    MonsterAsset ConverMonsterToAsset(Monster monster)
    {
        MonsterAsset asset = new MonsterAsset
        {
            NameMonster = monster.NameMonster,
            Id = monster.Id,
            BasicHP = monster.BasicHP,
            BasicMS = monster.BasicMS,
            BasicASMeeleUnit = monster.BasicASMeeleUnit,
            BasicASRangeUnit = monster.BasicASRangeUnit,
            BasicCoolDownMax = monster.BasicCoolDownMax,
            //TypeAtks = monster.TypeATKs,
            //ItemDrops = monster.ItemDrops,
            HeartDropRate = monster.HeartDropRate,
            GoldNumberMax = monster.GoldNumberMax,
            //MonsterClass = monster.MonsterClass,
        };
        return asset;
    }
    Monster ConverAssetToMonster(MonsterAsset asset)
    {
        Monster monster = new Monster
        {
            NameMonster = asset.NameMonster,
            Id = asset.Id,
            BasicHP = asset.BasicHP,
            BasicMS = asset.BasicMS,
            BasicATKDamge = asset.BasicATKDamge,
            BasicASMeeleUnit = asset.BasicASMeeleUnit,
            BasicASRangeUnit = asset.BasicASRangeUnit,
            BasicCoolDownMax = asset.BasicCoolDownMax,
            //TypeATKs = asset.TypeAtks,
            //ItemDrops = asset.ItemDrops,
            HeartDropRate = asset.HeartDropRate,
            GoldNumberMax = asset.GoldNumberMax,
            //MonsterClass = asset.MonsterClass,
        };

        return monster;
    }

}
