﻿using System;
using System.Collections;
using System.Collections.Generic;
using Studio;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

namespace Studio.BC
{
    public class BCViewHelper : ViewHelper
	{
		public static BCViewHelper Api;

        public StateView Web;
        public Camera MainCamera;
        public Camera BattleCamera;
        public GameObject LoadingGo;
        public GameObject BattleCanvasGo;

        private GameObject _loadingGo;

        public bool isLoadedIngameResource;

        protected override void Awake()
        {
            base.Awake();

            Api = this;
        }

        protected override void Start()
        {
            base.Start();

            //Main.Show(StateName.Login, (go) =>
            //{
            //    RemoveScene(SceneName.Loading);
            //});
        }

        public override void Reset()
        {
            base.Reset();

            Web.HideAll(true);

            BattleCanvasGo.SetActive(false);
            Battle.HideAll(true);
        }

        public bool CheckWebApiResult(Dictionary<string, object> dataResponse)
        {
            var result = dataResponse.Get<int>(BCKey.Result) == 1;
            if (!result)
            {
                var message = dataResponse.Get<string>(BCKey.Message);

                HideLoading();
                ShowMessageBox(message);                
            }

            return result;
        }

        public BCMessageBox ShowMessageBox(string message, Action onCloseClickAction = null, Transform parent = null)
        {            
            var prefab = Resources.Load<GameObject>("UIPrefab/"+StateName.MessageBox);
            var messageGo = Instantiate(prefab);
            messageGo.name = StateName.MessageBox;
            messageGo.transform.SetParent(System.transform, false);
            messageGo.transform.localPosition = Vector3.zero;
            messageGo.transform.localScale = Vector3.one;

            BCMessageBox view = messageGo.GetComponent<BCMessageBox>();
            view.Init(message, onCloseClickAction);
            return view;
        }


        public BCConfirmBox ShowConfirmBox(string message, Action onConfirmAction = null, Action onCloseAction = null)
        {
            var prefab = Resources.Load<GameObject>("UIPrefab/" + StateName.ConfirmBox);
            var messageGo = Instantiate(prefab);
            messageGo.transform.SetParent(System.transform, false);
            messageGo.transform.localPosition = Vector3.zero;
            messageGo.transform.localScale = Vector3.one;

            var view = messageGo.GetComponent<BCConfirmBox>();
            view.Init(message, onConfirmAction, onCloseAction);
            return view;
        }



        public void ShowMessageBoxCustom(string message, Action onCloseClickAction = null, Transform parent = null)
        {
            Api.System.ShowMessage(StateName.MessageBox, (go) =>
            {

                go.transform.SetParent(System.transform, false);
                go.transform.localPosition = Vector3.zero;
                go.transform.localScale = Vector3.one;

                BCMessageBox view = go.GetComponent<BCMessageBox>();
                view.Init(message, onCloseClickAction);
            });
        }


        public void ShowConfirmBoxCustom(string message, Action onConfirmAction = null, Action onCloseAction = null)
        {
            Api.System.ShowMessage(StateName.ConfirmBox, (go) => {

                //var prefab = go;
                //var messageGo = Instantiate(prefab);
                go.transform.SetParent(System.transform, false);
                go.transform.localPosition = Vector3.zero;
                go.transform.localScale = Vector3.one;

                var view = go.GetComponent<BCConfirmBox>();
                view.Init(message, onConfirmAction, onCloseAction);

            });
        }


        public void ShowLoading()
        {
            if (_loadingGo == null)
            {
                _loadingGo = Instantiate(Resources.Load<GameObject>(StateName.Loading));
                _loadingGo.transform.SetParent(System.transform, false);
                _loadingGo.transform.localScale = Vector3.one;
                _loadingGo.transform.localPosition = Vector3.zero;
                _loadingGo.transform.SetAsLastSibling();

            }

            _loadingGo.SetActive(true);
        }

        public void HideLoading()
        {
            if (_loadingGo != null) _loadingGo.SetActive(false);
        }

      

        public Vector2 GetMainSize()
        {
            var height = 2f*BattleCamera.orthographicSize;
            var width = height*BattleCamera.aspect;
            return new Vector2(width, height);

            //return BattleCanvasGo.AddToggleToList<RectTransform>().sizeDelta;
        }

        public void ResetBattleCamera()
        {
            BattleCamera.ResetAspect();
        }

        IEnumerator LoadPrefab(string prefabName, Action<GameObject> onCompleteAction)
        {
            var request = Resources.LoadAsync<GameObject>(prefabName);
            yield return request;
            onCompleteAction(request.asset as GameObject);
        }

		public void ShowFlyText(string text, Color fontColor, int fontSize = 30, float duration = 2f, float offsetY = 100, Transform parent = null)
		{
			if (parent == null)
				parent = BCViewHelper.Api.System.transform;
			StartCoroutine(LoadPrefab(PrefabName.flyText, goFly =>
			{
				var flyText = Instantiate(goFly, parent).GetComponent<BCFlyText>();
				if (flyText != null)
				{
					flyText.SetData(text, fontColor, fontSize);
					flyText.transform.position = Vector3.zero;
					LeanTween.moveLocalY(flyText.gameObject, flyText.transform.position.y + offsetY, duration).setEase(LeanTweenType.easeInOutCirc).setDestroyOnComplete(true);
				}
			}));
		}

        #region STATIC
        public static void ShowMessageBoxStatic(string message, Transform parent, Action onCloseClickAction = null)
        {
            var prefab = Resources.Load<GameObject>("UIPrefab/" + StateName.MessageBoxStatic);
            var messageGo = Instantiate(prefab);
            messageGo.transform.SetParent(parent, false);
            messageGo.transform.localPosition = Vector3.zero;
            messageGo.transform.localScale = Vector3.one;
            messageGo.transform.SetAsLastSibling();
            messageGo.GetComponent<BCMessageBox>().Init(message, ()=>
			{
				var child = parent.Find(StateName.MessageBoxStatic);
				if (child != null)
					Destroy(child.gameObject);
				if(onCloseClickAction != null)
					onCloseClickAction();
			});            
        }

        public static void ShowConfirmBoxStatic(string message, Transform parent, Action onConfirmAction = null, Action onCloseAction = null)
        {
            var prefab = Resources.Load<GameObject>("UIPrefab/" + StateName.ConfirmBoxStatic);
            var messageGo = Instantiate(prefab);
            messageGo.transform.SetParent(parent, false);
            messageGo.transform.localPosition = Vector3.zero;
            messageGo.transform.localScale = Vector3.one;
            messageGo.transform.SetAsLastSibling();
            messageGo.GetComponent<BCConfirmBox>().Init(message, onConfirmAction, onCloseAction);
        }

        public static void LoadScene(string sceneName)
        {
            SceneManager.LoadScene(sceneName);
        }

        public static void LoadSceneAddtive(string sceneName)
        {
            SceneManager.LoadScene(sceneName, LoadSceneMode.Additive);
        }

        public static void RemoveScene(string sceneName)
        {
            SceneManager.sceneUnloaded += OnSceneUnloaded;
            SceneManager.UnloadSceneAsync(sceneName);
        }

        private static void OnSceneUnloaded(Scene scene)
        {
            SceneManager.sceneUnloaded -= OnSceneUnloaded;

            //reset event
            var eventSystem = FindObjectOfType<EventSystem>();
            if (eventSystem != null) {
                eventSystem.enabled = false;
                eventSystem.enabled = true;
            }
        }
#endregion
    }
}
