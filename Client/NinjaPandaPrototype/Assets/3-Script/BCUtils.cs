﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace Studio.BC
{
	public static class BCUtils
	{
		//Dont use for List/Dictionary
		public static T DeepClone<T>(this T obj)
		{
			var temp = Newtonsoft.Json.JsonConvert.SerializeObject(obj);
			return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(temp);
		}

		public static float AngleTo(this Vector3 from, Vector3 to)
		{
			Vector2 direction = to - from;
			float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
			if (angle < 0f) angle += 360f;
			return angle;
		}
		public static string GetTimeFormat(this int time)
		{
			return ((float)time).GetTimeFormat();
		}

		public static string GetTimeFormat(this float time)
		{
			var timeInt = Mathf.FloorToInt(time);
			if (time <= 0)
				return "00:00";
			var day = timeInt / 3600 / 24;
			var hour = timeInt / 3600 % 24 + day * 24; ;
			var minute = timeInt % 3600 / 60;
			var second = timeInt % 60;
			//return Mathf.Floor(time / 60) + ":" + (Mathf.Floor(time % 60) < 10 ? "0" + Mathf.Floor(time % 60) : "" + Mathf.Floor(time % 60));
			return (hour <= 0 ? "" : (hour < 10 ? "0" + hour + ":" : hour + ":")) +
					(minute <= 0 ? "00:" : (minute < 10 ? "0" + minute + ":" : minute + ":")) +
					(second <= 0 ? "00" : (second < 10 ? "0" + second : "" + second));
		}

        public static string ToDigitStr(this int value)
        {
            if (value < 10)
                return "0" + value;
            return value.ToString();
        }
        public static void AddClickListener(this Button button, string parentName, Action callBackAction)
        {
            if (button == null)
                return;

#if !BUILD_EGT
            var buttonTracking = button.gameObject.GetComponent<FirebaseButtonTracking>();
            if (buttonTracking == null)
            {
                buttonTracking = button.gameObject.AddComponent<FirebaseButtonTracking>();
                buttonTracking.Type = TrackType.Normal;
            }

            buttonTracking.TrackPrefix = parentName + "_" + button.name;
#endif
            button.onClick.RemoveAllListeners();
            button.onClick.AddListener(() =>
            {
                callBackAction();
            });

        }
        public static void AddClickListener(this Button button, Action callBackAction, string parentName = "")
        {
            if (button == null)
                return;

#if !BUILD_EGT
            var buttonTracking = button.gameObject.GetComponent<FirebaseButtonTracking>();
            if (buttonTracking == null)
            {
                buttonTracking = button.gameObject.AddComponent<FirebaseButtonTracking>();
                buttonTracking.Type = TrackType.Normal;
            }

            buttonTracking.TrackPrefix = parentName + "_" + button.name;
#endif
            button.onClick.RemoveAllListeners();
            button.onClick.AddListener(() =>
            {
                callBackAction();
            });

        }

        public static System.DateTime GetDateTime(this long seconds)
		{
			System.DateTime pointOfReference = new System.DateTime(1970, 1, 1, 0, 0, 0);
			pointOfReference = pointOfReference.AddSeconds(seconds).ToLocalTime();
			return pointOfReference;
		}
	}
}

