﻿public enum EnumLanguageConfigType
{
    vn,
    en
}
public enum EnumAudioSource
{
    HitMonster = 1,
    ShotPlayer = 2
}
public enum EnumPopupMenu
{
    none = 0,
    openchest = 1,
    updateTalen = 2,
    UpdateEquipment = 3,
    IsUseEquipment = 4,
    NewEquipment = 5,
    ISUseAchievement = 6

}
public enum EnumEffectDamageType
{
    FXFloatingDamageAnimal,
    FXFloatingDamageGhost,
    FXFloatingDamageRoBot,
    FXFloatingDamageRock
}
public enum EnumNPCPopup
{

    AcQuy = 1,
    ThienThan = 2,
    ThuongNhan = 3,
    VongQuayBoss=4,
    VongQuayStart=5

}
public enum EnumTalentKey
{
    MaxHp = 1,
    ATK = 2,
    Recover = 3,
    Shield = 4,
    Agile = 5,
    Inspire = 6,
    Enhance = 7,
    TimeReward = 8,
    Glory = 9
}

public enum EnumTag
{
    Monster = 1,
    Wall = 2,
    WallBorder = 3
}

public enum StyleLoadDictionary
{
    normal = 1,
    custom = 2
}

public enum EnumSkillType
{
    MutilAttack = 1,
    EffectAttack = 2,
    SkillSummoner = 3,
    AddParameter = 4,
    Passive = 5,
    Special = 6
}


public enum EnumSkillMutilAttackID
{
    MultiShot = 1,
    FrontArrow = 2,
    Branching = 4,
    SideArrow = 5,
    RearArrow = 6,
}

public enum EnumBulletEffect
{
    Blast = 1,

    Ice = 2,
    Poision = 3,
    Bolt = 4,


}

public enum EnumSkillEffectAttackID
{
    BouncyArrow = 3,
    Ricochet = 7,
    Pierced = 8,
    Bolt = 11,
    Poision = 12,
    Blast = 13,
    Ice = 14,
    Absorb = 40,
    Headshot = 42
}
public enum EnumSkillSummon
{
    LightingCharm = 15,
    ToxicCharm = 16,
    BlastCharm = 17,
    IceCharm = 18,
    ThunderStorm = 19,
    PoisonGas = 20,
    FireBall = 21,
    SnowBall = 22,
}

public enum EnumSkillAddParameterID
{
    Attack = 31,
    AttackMini = 32,
    CriticalRate = 33,
    CriticalRateMini = 34,
    CriticalDame = 35,
    CriticalDameMini = 36,
    Speed = 37,
    SpeedMini = 38,
    Endurance = 46,
    Shield = 47,
    StrongHeart = 55,
    Smart = 56,
    Heal = 57,
    HPBoost = 54,
    Wound = 44,
    Sharp = 45,
    ExtraLife = 60
}
public enum StyleSummon
{
    ball = 1,
    area = 2
}

public enum EnumSkillEffect
{
    EffectPosion = 1,
    EffectIce = 2,
    EffectLight = 3,
    EffectBlast = 4
}
public enum EnumSkillPassiveID
{
    Rage = 39,
    Fury = 48,
    Evasion = 49,
    Chance = 50,
    Rampage = 51,
}

public enum EnumSkillSpecical
{
    Blessing = 25,
    ColdArea = 9,
    Amulet = 10,
    Dodge = 26,
    Wind = 43,
    BulletProof = 41,
    Halo = 24,
    Ultimate = 23,
    CollectAttack = 58,
    collectSpped = 59
}

public enum EnumPopupIcon
{
    OpenChest = 1,
    NewEquipment = 2,
    UseEquipment = 3,
    UpdateEquipment = 4,
    UpdateTalent = 5
}
public enum EnumShopItem
{
    Chest = 1,
    Gem = 2,
    Coin = 3
}

public enum EnumHideMaterial
{
    KeyNormal = 5,
    keyEpic = 6
}



public enum EnumTypeItem
{
    WeaponScroll = 1,
    CharmScroll = 2,
    RingScroll = 3,
    PetScroll = 4,
    KeyNormal = 5,
    KeyEpic = 6
}

public enum EnumMoveBasic // di chuyển loại 1
{
    Di,
    Bay,
    Nhay,
    KhongDi
}

public enum EnumMoveSkill
{
    DonTho,
    XuyenKhong,
    LuotNhanh
}
public enum EnumSpecialSkill // ky nang noi tai
{
    None,
    ChetPhanThan, // khi chết phân thân thành con mini
    GoiThemNhanBan, // gọi thêm một con quái giống mình 
    RaiDoc // đẻ lại độc trên đường đi
}
public enum EnumAnimName
{
    Idle,
    Run,
    PreAtk,
    Atk,
    EndAtk,
    Hit,
    Die
}
public enum EnumShootSkill
{
    Khong, // không bắn
    BanThang,
    Boomerang, // đạn dọi về 
    VongCung,
    DanPhao,
    Ziczac,
    XoayTron,
    FullMap
}
public enum EnumBulletType
{
    Thuong, // bay thang
    XuyenTuong, // bay qua vong
    XuyenThau, // laze
    No,
    PhanChia,
    DoiTuong
}
public enum EnumThoiGianHieuLuc
{
    Ngay,
    Cho,
    DuyTri
}
public enum EnumSpecialMonster
{
    DanDiTheo, // 
    PhanThan, // khi chết phân thân ra
    GoiQuai, // ném co quái 
    Doc // 
}
public enum EnumTypeEquipment
{
    None = 0,
    Weapon = 1,
    Armor = 2,
    Ring = 3,
    Pet = 4
}
public enum EnumGradeEquipment
{
    None = 0,
    Common = 1,
    Great = 2,
    Rare = 3,
    Epic = 4
}

public enum ResourceGame
{
    Gem = 1,
    Energy = 2,
    Gold = 3,
    Level = 4
}

public enum EnumDynamicImageType
{
    None = 0,
    Currency = 1,
    Inventory = 2,
    Skill = 3,
    World = 4
}
public enum EnumItemWheelType
{
    Skill = 1,
    Gold = 2,
    Gem = 3
}
public enum EnumVongQuayType
{
    Start,
    Boss,
    Ads
}

public enum EnumItemSubType
{
    Gold = 1,
    Gem = 2,
    Ticket = 3,
    Medal = 4,
    Charm = 5,
    Chest = 6,
    Random_Chest = 7,
    ChangeNickCard = 9,
    Gun = 13,
    Wing = 14,
    Card = 15,
    Hoa_dao = 16,
    Li_xi = 17,
    One_Piece = 18,
    Thuy_tinh = 19
}

public enum Lobby_Position
{
    SHOP = 1,
    INVENTORY = 2,
    WORLD = 3,
    TALENT = 4,
    SETTINGS = 5
}

