﻿public static class BCHardCode
{
    public const float SizeScale = 0.2f;
    public const float HeightBall = 3f;
}

public class BCPrefKey
{
    public const string Login = "login";
    public const string LoginName = "login_name";
    public const string LoginPass = "login_pass";
    public const string LoginType = "login_type";
    public const string WpAutoLogin = "wp_autologin";
    public const string BsSynthesizeNote = "bsSynthNote";
    public const string BsSynthesizeLoaded = "bsSynthLoaded";
    public const string FBToken = "fbToken";
    public const string FBTokenExpiration = "fbTokenExpiration";
    public const string Facebook = "facebook";
    public const string LastLogin = "lastLogin";
    public const string condition_and_agreement = "condition_and_agreement";
}

public static class BCServerCommand
{
    public const string GET_TIME_SERVER = "GET_TIME_SERVER";
    public const string OP_SYNTHESIZE_INFO = "OP_SYNTHESIZE_INFO";
    public const string QUICK_PLAY = "QUICK_PLAY";
    public const string GET_USER_INFO = "GET_USER_INFO";
    public const string UPDATE_REMAIN_FREE = "UPDATE_REMAIN_FREE";
    public const string USER_INFO_FULL_VIEW_NOTIFY = "USER_INFO_FULL_VIEW_NOTIFY";
    public const string CHALLENGE_ROOM_INFO = "CHALLENGE_ROOM_INFO";
    public const string ROOM_INFO = "ROOM_INFO";
    public const string ROOM_QUEST = "ROOM_QUEST";
    public const string BATTLE_USER_QUEST = "BATTLE_USER_QUEST";
    public const string LIST_USER_IN_DESK = "LIST_USER_IN_DESK";
    public const string CLEAR_FISH = "CLEAR_FISH";
    public const string SPAWN_FISH = "SPAWN_FISH";
    public const string SPAWN_PARADE_FISH = "SPAWN_PARADE_FISH";
    public const string CHALLENGE_BATTLE_END = "CHALLENGE_BATTLE_END";
    public const string CHALLENGE_EVENT_END = "CHALLENGE_EVENT_END";
    public const string SPAWN_BOSS_FISH = "SPAWN_BOSS";
    public const string LOGIN_BATTLE = "LOGIN_BATTLE";
    public const string USER_FIRE = "USER_FIRE";
    public const string CHALLENGE_USER_FIRE = "CHALLENGE_USER_FIRE";
    public const string CHALLENGE_USER_FIRE_SKILL = "CHALLENGE_USER_FIRE_SKILL";
    public const string USER_CATCH_FISH = "USER_CATCH_FISH";
    public const string CHALLENGE_USER_CATCH_FISH = "CHALLENGE_USER_CATCH_FISH";
    public const string CHALLENGE_USER_CATCH_SPECIAL_FISH = "CHALLENGE_USER_CATCH_SPECIAL_FISH";
    public const string CHALLENGE_USE_SKILL_CATCH_FISH = "CHALLENGE_USE_SKILL_CATCH_FISH";
    public const string CHALLENGE_UPDATE_RESOURCE = "CHALLENGE_UPDATE_RESOURCE";
    public const string BOSS_DIE = "C_BD";
    public const string USER_JOIN = "USER_JOIN";
    public const string USER_LEFT = "USER_LEFT";
    public const string USER_QUIT_WAITING = "USER_QUIT_WAITING";
    public const string CHALLENGE_USER_QUIT_BATTLE = "CHALLENGE_USER_QUIT_BATTLE";
    public const string CHALLENGE_USER_COUNT = "CHALLENGE_USER_COUNT";
    public const string USER_QUIT_BATTLE = "USER_QUIT_BATTLE";
    public const string UPDATE_OTHER_USER_RESOURCE = "UPDATE_OTHER_USER_RESOURCE";
    public const string GET_OTHER_USER_INFO = "GET_OTHER_USER_INFO";
    public const string USER_SEND_GIFT = "USER_SEND_GIFT";
    public const string UPDATE_USER_RESOURCE = "UPDATE_USER_RESOURCE";
    public const string UPDATE_INVENTORY = "UPDATE_INVENTORY";
    public const string LEVEL_UP_REWARD = "LEVEL_UP_REWARD";
    public const string GET_LUCKY_MATCH_REWARD = "GET_LUCKY_MATCH_REWARD";
    public const string LUCKY_MATCH_SPIN = "LUCKY_MATCH_SPIN";
    public const string USER_FIRE_SKILL = "USER_FIRE_SKILL";
    public const string USE_SKILL_CATCH_FISH = "USE_SKILL_CATCH_FISH";
    public const string BOSS_REWARD = "BOSS_REWARD";
    public const string JACKPOT_BOSS_REWARD = "JACKPOT_BOSS_REWARD";
    public const string JACKPOT_PRICE_UPDATE = "JACKPOT_PRICE_UPDATE";
    public const string CHANGE_ACHIEVEMENT_TITLE = "CHANGE_ACHIEVEMENT_TITLE";
    public const string PING_SERVER = "PING_SERVER";

    public const string BUY_GUNGROUP = "BUY_GUNGROUP";
    public const string BUY_GUN = "BUY_GUN";
    public const string BUY_WING = "BUY_WING";
    public const string BUY_FRAME = "BUY_FRAME";
    public const string BUY_BORDER = "BUY_BORDER";

    public const string CHANGE_GUN_GROUP = "CHANGE_GUN_GROUP";
    public const string CHANGE_GUN = "CHANGE_GUN";
    public const string CHANGE_WING = "CHANGE_WING";
    public const string CHANGE_FRAME = "CHANGE_FRAME";
    public const string CHANGE_BORDER = "CHANGE_BORDER";

    public const string CHALLENGE_USER_CHAT = "CHALLENGE_USER_CHAT";
    public const string USER_CHAT = "USER_CHAT";
    public const string CHALLENGE_QUICK_BUY = "CHALLENGE_QUICK_BUY";
    public const string CHALLENGE_BATTLE_INFO = "CHALLENGE_BATTLE_INFO";
    public const string SEND_GIFT_EFFECT = "SEND_GIFT_EFFECT";
    public const string ACTIVE_ENERGY_SKILL = "ACTIVE_ENERGY_SKILL";
    public const string BATTLE_NOTIFY = "BATTLE_NOTIFY";
    public const string NOTIFY_USER_FINISH_QUEST = "NOTIFY_USER_FINISH_QUEST";
    public const string CHANGE_BATTLE_GUN = "CHANGE_BATTLE_GUN";
    public const string SEND_SYSTEM_CHAT_MESSAGE = "SEND_SYSTEM_CHAT_MESSAGE";
    /* Thông báo từ server về client (chỉ app dung trong battle)
    * S2C: {type, message}
    * type: loại notify 
    * message: thông báo
    */

    public const string FORCE_APP = "FORCE_APP";
    /* Thông báo từ server về client
    * S2C: {type, message}
    * type: loại notify 
    * message: thông báo
    */
    public const string HIT_MONSTER = "HIT_MONSTER";
    public const string START_ATTACK = "START_ATTACK";
    public const string GET_TEAM_ID = "GET_TEAM_ID";
    public const string SHOW_MESSAGE_BOX = "SHOW_MESSAGE_BOX";
    public const string UPDATE_HOME_SCENE = "UPDATE_HOME_SCENE";
    public const string CREATE_CHARACTER = "CREATE_CHARACTER";
    /**
    * random nick for create character
    * C2S: {}
    * S2C: { ErrorCode, Nick }
    */
    public const string RANDOM_NICK = "RANDOM_NICK";
    #region Battle server (Server To Server) ( unet )
    public const string BS_ENEMY_DIE = "BS_ENEMY_DIE";
    public const string BS_GET_USER_INFO = "BS_GET_USER_INFO";
    #endregion

    #region User Data
    public const string USER_LEVEL_UP = "USER_LEVEL_UP";
    #endregion

    #region Hero
    //Đột phá hero
    public const string HERO_AWAKEN = "HERO_DO_AWAKEN";
    //Lấy danh sách skill active
    public const string HERO_SKILL_GET_ACTIVE_LIST = "HERO_SKILL_GET_ACTIVE_LIST";
    //Lay thong tin ve ki nang, kinh mach, trang bị...
    public const string GET_HERO_DETAIL_INFO = "GET_HERO_DETAIL_INFO";

    //Tăng node kinh mạch
    public const string HERO_UPGRADE_ACUPOINT = "HERO_UPGRADE_ACUPOINT";

    //Trang bị chân nguyên
    public const string HERO_SOUL_DO_EQUIP = "HERO_SOUL_DO_EQUIP";

    //Tháo trang bị chân nguyên
    public const string HERO_SOUL_DO_UNEQUIP = "HERO_SOUL_DO_UNEQUIP";

    //Tăng sao chân nguyên
    public const string HERO_SOUL_UPGRADE_STAR = "HERO_SOUL_UPGRADE_STAR";

    //Tăng cấp chân nguyên
    public const string HERO_SOUL_UPGRADE_LEVEL = "HERO_SOUL_UPGRADE_LEVEL";
    #endregion

    //Cập nhật thay đổi thông số hero (chỉ số)
    public const string UPDATE_HERO_INFO = "UPDATE_HERO_INFO";
    //Cập nhật thay đổi vị trí đội hình chính
    public const string UPDATE_MAIN_SQUAD = "UPDATE_MAIN_SQUAD";
    //Cập nhật thay đổi vị trí đội hình phụ
    public const string UPDATE_SUB_SQUAD = "UPDATE_SUB_SQUAD";
    //Nâng cấp đội hình phụ
    public const string SUB_SQUAD_UPGRADE = "SUB_SQUAD_UPGRADE";
    //Chiêu mộ thành công 1 con hero mới
    public const string RECRUIT_NEW_HERO = "RECRUIT_NEW_HERO";
    //Mua thêm slot hành nang
    public const string INVENTORY_BUY_SLOT = "INVENTORY_BUY_SLOT";
    //Mua item Shop
    public const string Shop_BUY_ITEM = "Shop_BUY_ITEM";
    //Dùng item trong hành nang
    public const string INVENTORY_USE_ITEM = "INVENTORY_USE_ITEM";
    //Mở rương lựa chọn
    public const string INVENTORY_GET_ITEM_PACKAGE_DETAIL = "INVENTORY_GET_ITEM_PACKAGE_DETAIL";
    //Chọn item trong rương lựa chọn
    public const string INVENTORY_CHOOSE_ITEM_IN_PACKAGE = "INVENTORY_CHOOSE_ITEM_IN_PACKAGE";
    //Hủy item trong hành nang
    public const string INVENTORY_DESTROY_ITEM = "INVENTORY_DESTROY_ITEM";
    //Bán item trong hành nang
    public const string INVENTORY_SELL_ITEM = "INVENTORY_SELL_ITEM";
    //Trang Bị item equip
    public const string HERO_EQUIPMENT_EQUIP = "HERO_EQUIPMENT_EQUIP";
    //Tháo trang bị
    public const string HERO_EQUIPMENT_UNEQUIP = "HERO_EQUIPMENT_UNEQUIP";
    //Cuong Hoa O Trang Bi
    public const string HERO_EQUIPMENT_ENCHANT_UPGRADE = "HERO_EQUIPMENT_ENCHANT_UPGRADE";
    //Kiểm tra item có đang giảm giá hay ko
    public const string Shop_CHECK_DISCOUNT = "Shop_CHECK_DISCOUNT";
    //Tăng sao thường trang bị
    public const string HERO_EQUIPMENT_UPGRADE_EXP = "HERO_EQUIPMENT_UPGRADE_EXP";
    //Tăng sao max exp trang bị
    public const string HERO_EQUIPMENT_UPGRADE_MAX_EXP = "HERO_EQUIPMENT_UPGRADE_MAX_EXP";
    //Tắng phẩm
    public const string HERO_EQUIPMENT_ENHANCE_UPGRADE = "HERO_EQUIPMENT_ENHANCE_COLOR";
    //Tinh luyện 
    public const string HERO_EQUIPMENT_DO_FINE = "HERO_EQUIPMENT_DO_FINE";
    //Đục lỗ khảm nạm
    public const string EQUIPMENT_UNLOCK_SLOT_JEWEL = "EQUIPMENT_UNLOCK_SLOT_JEWEL";
    //Khảm nạm
    public const string EQUIPMENT_ADD_JEWEL = "EQUIPMENT_ADD_JEWEL";
    //Tháo ngọc khảm nạm
    public const string EQUIPMENT_REMOVE_JEWEL = "EQUIPMENT_REMOVE_JEWEL";
    //Quay chân nguyên thông tin
    public const string HERO_SOUL_SPIN_GET_INFO = "HERO_SOUL_SPIN_GET_INFO";
    //Quay chân nguyên
    public const string HERO_SOUL_SPIN = "HERO_SOUL_SPIN";

    public const string BS_LOGIN_SUCCESS = "BS_LOGIN_SUCCESS";

    public const string JOB_OPEN = "JOB_OPEN";

    public const string JOB_GET_ALL_JOB_INFO = "JOB_GET_ALL_JOB_INFO";

    public const string BLACKSMITH_DISASSEMBLE_ITEM = "BLACKSMITH_DISASSEMBLE_ITEM";

    public const string BLACKSMITH_QUICK_DISASSEMBLE = "BLACKSMITH_QUICK_DISASSEMBLE";

    public const string BLACKSMITH_SYNTHESIZE_ITEM = "BLACKSMITH_SYNTHESIZE_ITEM";

    public const string BLACKSMITH_CRAFT_ITEM = "BLACKSMITH_CRAFT_ITEM";
    public const string JEWEL_ENHANCE_ITEM = "JEWEL_ENHANCE_ITEM";
    public const string JEWEL_DISASSEMBLE_ITEM = "JEWEL_DISASSEMBLE_ITEM";
    public const string JEWEL_QUICK_DISASSEMBLE = "JEWEL_QUICK_DISASSEMBLE";
    public const string JEWEL_GRIND_ITEM = "JEWEL_GRIND_ITEM";
    public const string UPDATE_JOB_INFO = "UPDATE_JOB_INFO";
    public const string JEWEL_CANCEL_GRIND = "JEWEL_CANCEL_GRIND";

    public const string JACKPOT_REWARD = "JACKPOT_REWARD";
    #region Tutorial
    public const string UPDATE_TUTORIAL = "UPDATE_TUTORIAL";
    #endregion
    #region Mail
    public const string MAIL_DELETE_ALL = "MAIL_DELETE_ALL";
    public const string MAIL_DELETE = "MAIL_DELETE";
    public const string MAIL_READ = "MAIL_READ";
    public const string MAIL_READ_ALL = "MAIL_READ_ALL";
    public const string GET_ALL_MAIL = "GET_ALL_MAIL";
    public const string MAIL_RECEIVE_ALL = "MAIL_RECEIVE_ALL";
    public const string MAIL_RECEIVE = "MAIL_RECEIVE";
    public const string GET_INVENTORY = "GET_INVENTORY";
    public const string UPDATE_NEW_MAIL = "UPDATE_NEW_MAIL";
    #endregion

    public const string REGISTER_PUSH_NOTIFY = "REGISTER_PUSH_NOTIFY";


    public const string RECEIPT_IAP = "RECEIPT_IAP";


    #region LifeQuest
    public const string GET_LIST_LIFE_QUEST = "GET_LIST_LIFE_QUEST";
    public const string RECEIVE_LIFE_QUEST_REWAWRD = "RECEIVE_LIFE_QUEST_REWAWRD";
    public const string GET_LIST_ACHIEVEMENT = "GET_LIST_ACHIEVEMENT";
    public const string RECEIVE_ACHIEVEMENT_REWAWRD = "RECEIVE_ACHIEVEMENT_REWAWRD";
    #endregion

    public const string CHANGE_USER_NICK = "CHANGE_USER_NICK";
    public const string USE_CONSUMABLE_ITEM = "USE_CONSUMABLE_ITEM";
    public const string CHANGE_AVATAR = "CHANGE_AVATAR";
    public const string BUY_SHOP_ITEM = "BUY_SHOP_ITEM";
    public const string GET_LEADERBOARD = "GET_LEADERBOARD";
    public const string EVENTS_GET_EVENT_SUMMARY = "GET_EVENT_SUMMARY";
    public const string GET_ALL_NOTIFY_DATA = "GET_ALL_NOTIFY_DATA";
    public const string GET_EVENTS_DETAIL = "GET_EVENTS_DETAIL";
    public const string RECEIVE_REWARD_EVENT = "RECEIVE_REWARD_EVENT";
    public const string GET_TOP_EVENT_LEADERBOARD = "GET_TOP_EVENT_LEADERBOARD";
    public const string RECEIVE_EVENT_PAYMENT_FIRST_TIME_REWARD = "RECEIVE_EVENT_PAYMENT_FIRST_TIME_REWARD";
    public const string OPEN_VIP_INFO = "OPEN_VIP_INFO";
    public const string GET_LIST_CHALLENGE = "GET_LIST_CHALLENGE";
    public const string GET_CHALLENGE_LEADERBOARD = "GET_CHALLENGE_LEADERBOARD";
    public const string CHALLENGE_REGISTER = "CHALLENGE_REGISTER";
    public const string CHALLENGE_QUIT_WAITING = "CHALLENGE_QUIT_WAITING";
    public const string CHALLENGE_EVENT_INFO = "CHALLENGE_EVENT_INFO";
    public const string CHALLENGE_ENTER = "CHALLENGE_ENTER";
    public const string CHALLENGE_GET_LEADERBOARD = "CHALLENGE_GET_LEADERBOARD";
    public const string TOURNAMENT_GET_LEADERBOARD = "TOURNAMENT_GET_LEADERBOARD";

    public const string CHALLENGE_NOTIFY_START = "CHALLENGE_NOTIFY_START";
    public const string TOURNAMENT_NOTIFY_START = "TOURNAMENT_NOTIFY_START";
    public const string TOURNAMENT_ENTER = "TOURNAMENT_ENTER";

    public const string USER_CATCH_SPECIAL_FISH = "USER_CATCH_SPECIAL_FISH";
    public const string JOINED_EXTEND_SERVER = "JOINED_EXTEND_SERVER";
    public const string LEFT_EXTEND_SERVER = "LEFT_EXTEND_SERVER";
    public const string LUCKY_WHEEL_FISH_REWARD = "LUCKY_WHEEL_FISH_REWARD";

    #region Friend
    public const string FRIEND_GET_FRIEND_LIST = "FRIEND_GET_FRIEND_LIST"; //Lay danh sach ban
    public const string FRIEND_GET_REQUEST_LIST = "FRIEND_GET_REQUEST_LIST"; //Lay danh sach yeu cau ket ban
    public const string FRIEND_GET_RECOMMEND_LIST = "FRIEND_GET_RECOMMEND_LIST"; //Lay danh sach goi y
    public const string FRIEND_SEND_REQUEST = "FRIEND_SEND_REQUEST";
    public const string FRIEND_ACCEPT_REQUEST = "FRIEND_ACCEPT_REQUEST";
    public const string FRIEND_DENY_REQUEST = "FRIEND_DENY_REQUEST";
    public const string FRIEND_SEND_GIFT = "FRIEND_SEND_GIFT";
    public const string FRIEND_SEND_ALL_GIFT = "FRIEND_SEND_ALL_GIFT";
    public const string FRIEND_RECEIVE_GIFT = "FRIEND_RECEIVE_GIFT";
    public const string FRIEND_RECEIVE_ALL_GIFT = "FRIEND_RECEIVE_ALL_GIFT";
    public const string FRIEND_SEND_CHAT = "FRIEND_SEND_CHAT";
    public const string FRIEND_ON_RECEIVE_CHAT = "FRIEND_ON_RECEIVE_CHAT";
    public const string FRIEND_SEARCH_FRIEND = "FRIEND_SEARCH_FRIEND";
    public const string FRIEND_ACCEPT_ALL_REQUEST = "FRIEND_ACCEPT_ALL_REQUEST";
    public const string FRIEND_DENY_ALL_REQUEST = "FRIEND_DELETE_ALL_REQUEST";
    public const string FRIEND_REMOVE = "FRIEND_REMOVE";
    #endregion

    #region broadcast
    public const string BROADCAST_MSG = "BROADCAST_MSG";
    public const string BROADCAST_READY = "BROADCAST_READY";
    #endregion

    #region IAP
    public const string GEM_NOTIFY = "GEM_NOTIFY";
    #endregion

    #region IsLand
    //public const string GET_ALL_DATA_CONFIG_ISLAND = "GET_ALL_DATA_CONFIG_ISLAND";
    //public const string OPEN_NODE_ISLAND = "OPEN_NODE_ISLAND";
    //public const string EXPLOITED_NODE_ISLAND = "EXPLOITED_NODE_ISLAND";
    //public const string FUSION_GEM_ITEM_ISLAND = "FUSION_GEM_ITEM_ISLAND";

    public const string OP_GET_UNLOCK_NODE = "OP_GET_UNLOCK_NODE";
    public const string OP_UNLOCK_NODE = "OP_UNLOCK_NODE";
    public const string OP_OPEN_NODE = "OP_OPEN_NODE";
    public const string OP_SYNTHESIZE_ITEM = "OP_SYNTHESIZE_ITEM";
    #endregion
    public const string UPDATE_ITEM_EXP_VALUE = "UPDATE_ITEM_EXP_VALUE";
    /* Cập nhật thời gian sử dụng item (khi nhấn sử dụng item trong Inventory)
        * S2C: {id, type, exp_value}
        * id: itemId
        * type: loại item (1=súng, 2=cánh)
        * exp_value: thời gian còn lại (second)
        */

    public const string ITEM_EXP_VALUE_NOTIFY = "ITEM_EXP_VALUE_NOTIFY";
    /* Thông báo khi item hết hạn sử dụng (chỉ app dung trong battle)
    * S2C: {id, type, cost_original, curency}
    * id: itemId
    * type: loại item (1=súng, 2=cánh)
    * can_buy: (0=k dc mua, 1=dc mua)
    */

    #region PK
    public const string PK_EVENT_START = "PK_EVENT_START";
    /* Hiện nút pk hay ko, và phí tham gia PK.
        * S2C: {value, type}
        value: Tiền
        type: Loại tiền
        */

    public const string PK_JOIN_MATCH_MAKING = "PK_JOIN_MATCH_MAKING";
    public const string PK_ROOM_WAIT_INFO = "PK_ROOM_WAIT_INFO";
    public const string PK_ROOM_WAIT_CANCEL = "PK_ROOM_WAIT_CANCEL";
    public const string PK_ROOM_WAIT_USER_LEFT = "PK_ROOM_WAIT_USER_LEFT";
    public const string PK_ROOM_WAIT_USER_JOIN = "PK_ROOM_WAIT_USER_JOIN";

    public const string PK_ROOM_INFO = "PK_ROOM_INFO";
    public const string PK_USER_FIRE = "PK_USER_FIRE";
    public const string PK_USER_CATCH_FISH = "PK_USER_CATCH_FISH";
    public const string PK_USER_CATCH_SPECIAL_FISH = "PK_USER_CATCH_SPECIAL_FISH";
    public const string PK_UPDATE_RESOURCE = "PK_UPDATE_RESOURCE";
    public const string PK_BATTLE_END = "PK_BATTLE_END";
    public const string PK_EVENT_END = "PK_EVENT_END";
    public const string PK_BATTLE_INFO = "PK_BATTLE_INFO";
    public const string PK_USER_CHAT = "PK_USER_CHAT";
    public const string PK_USER_QUIT_BATTLE = "PK_USER_QUIT_BATTLE";
    public const string PK_ENTER = "PK_ENTER";
    public const string PK_EVENT_INFO = "PK_EVENT_INFO";
    public const string PK_BATTLE_START = "PK_BATTLE_START";
    #endregion

    #region CardPuzzle
    public const string CARD_PUZZLE_SHOW = "CARD_PUZZLE_SHOW";
    public const string CARD_PUZZLE_USE = "CARD_PUZZLE_USE";
    #endregion

    #region FriendConfirm
    public const string FRIEND_CHECK = "FRIEND_CHECK";
    public const string REMOVE_FRIEND = "REMOVE_FRIEND";
    public const string ACCEPT_FRIEND = "ACCEPT_FRIEND";
    #endregion

    #region FishGame
    public const string FISH_GAME_START = "FISH_GAME_START";
    public const string FISH_GAME_PLAY = "FISH_GAME_PLAY";
    public const string FISH_GAME_QUIT = "FISH_GAME_QUIT";
    #endregion

    #region DailyWheel
    public const string GET_EVENT_DAILY_WHEEL = "GET_EVENT_DAILY_WHEEL";
    public const string EVENT_DAILY_WHEEL_SPIN = "EVENT_DAILY_WHEEL_SPIN";
    #endregion

}

public class ServerKey
{
    public const string ErrorCode = "ec"; // (int)
    public const string ErrorString = "es";

    public const string PlayerInfo = "playerinfo"; //(object) {Energy, Gold, Knb}

    public const string Id = "id"; // (int)

    #region for syn mmo
    public const string EntityType = "t"; // (int)
    public const string MMOMessageType = "mt"; // (int)
    public const string MMOMessageTypeList = "mt_list";

    public const string PosX = "x";
    public const string PosY = "y";
    public const string PosZ = "z";

    public const string RotaX = "rx";
    public const string RotaY = "ry";
    public const string RotaZ = "rz";

    public const string runningAnimationType = "rat";
    public const string characterStatus = "cs";
    public const string animName = "an";
    public const string monsterId = "mid";
    public const string affecter = "affecter";
    public const string animType = "at";

    public const string data1 = "d1";
    public const string data2 = "d2";

    public const string currentHp = "chp";
    public const string maxHp = "mhp";
    public const string hpChange = "hc";
    public const string isSpawn = "is";
    public const string teamId = "tid";
    public const string characterType = "ct";

    public const string monsterIds = "mids";
    public const string playerIds = "pids";

    public const string skillKey = "sk";
    public const string skillHitNumber = "shn";

    //public const string hitXoffset = "hxo";
    //public const string hitYoffset = "hyo";
    //public const string hitZoffset = "hzo";

    //public const string hitXrotate = "hxr";
    //public const string hitYrotate = "hyr";
    //public const string hitZrotate = "hzr";

    //public const string targetRangeType = "trt";
    //public const string targetRangeRaidus = "trr";
    //public const string targetRangeAngle = "tra";
    //public const string targetRangeH = "trh";
    //public const string targetRangeW = "trw";
    #endregion

    //public const string Angle = "rot";
    //public const string ResourceName = "res";
    //public const string StopMove = "stopmove";
    //public const string IsCharacterHide = "is_character_hide";

    public const string attacker = "attacker";
    public const string monster = "monster";
    public const string skill = "skill";
    public const string skills = "skills";
    public const string hit_number = "hit_number";

    public const string userId = "uid";                                             //(string) id của player
    public const string ListID = "ListID";
    public const string LUID = "LUID";
    public const string serverId = "gameServerId";

    #region Login - Server
    public const string LocId = "loc_id";
    public const string param = "param";
    public const string LocParam = "loc_param";
    public const string LocDesc = "loc_desc";
    #endregion


    public const string key1 = "key1";
    public const string key2 = "key2";
    public const string key3 = "key3";

    #region CreatePlayer
    public const string Nick = "nick";
    public const string Element = "element"; // (int) ngu hanh; [kim, moc, thuy, hoa, tho]
    public const string Elements = "elements";
    public const string Roles = "roles";
    public const string Asset = "asset";
    public const string Asset2D = "asset2d";
    public const string Asset3D = "asset3d";
    public const string HardLevel = "hard_level";
    public const string Sect = "sect"; // (string) ten mon phai  
    public const string Body = "body"; //(string)
    public const string Hair = "hair"; //(string)
    public const string Face = "face"; //(string)
    public const string Gender = "gender"; // (int)
    public const string Items = "items";
    public const string ArtWork = "artwork";
    #endregion

    #region UserData
    public const string PlayerBase = "playerBase";
    public const string Level = "level";
    public const string vipLevel = "vip_level";
    public const string Power = "power";
    public const string Avatar = "avatar"; // (int)
    public const string Gold = "gold"; // (long)
    public const string Gem = "gem"; // (long)
    public const string Exp = "exp";
    public const string ExpNextLevel = "exp_next_level";
    public const string Hp = "hp";
    public const string MaxHp = "max_hp";
    public const string Star = "star";
    public const string Faction = "faction";
    public const string HeroId = "hero_id";
    public const string AwakenLevel = "awaken_level";
    public const string Skills = "skills";
    public const string TuViPoint = "tu_vi_point";
    public const string UnTradeableGem = " untradeable_gem";
    #endregion

    #region Item
    public const string Type = "type";
    public const string message = "message";
    public const string ItemGroupType = "itemGroupType";
    public const string ItemType = "item_type";
    public const string Color = "color";
    public const string ColorText = "color_text";
    public const string Value = "value";
    public const string MaxValue = "max_value";
    public const string Stats = "stats";
    public const string IsPercent = "is_percent";
    public const string ShopId = "shop_id";
    public const string Tab = "tab";
    public const string SubType = "sub_type";
    public const string Order = "order";
    public const string Sequence = "sequence";
    public const string Quantity = "quantity";
    public const string DefaultSlot = "default_slot";
    public const string SlotPerPurchase = "slot_per_purchase";
    public const string Price = "price";
    public const string MaxSlot = "max_slot";
    public const string Slots = "slots";
    public const string IsLocked = "is_lock";
    public const string EquipmentType = "equipment_type";
    public const string EnchantLevel = "enchant_level";
    public const string MaxEnhance = "max_enhance";
    public const string MaxAugment = "max_augment";
    public const string SectLimit = "sect_limit";
    public const string RequireLevel = "require_level";
    public const string EffectValue = "effect_value";
    public const string SkillLevel = "skill_level";
    public const string CostGemOriginal = "cost_gem_original";
    public const string CostGemDiscount = "cost_gem_discount";
    public const string LimitTime = "limit_time";
    public const string EndTime = "end_time";
    public const string StartTime = "star_time";
    public const string ShopType = "cash_shop_type";
    public const string VipLevel = "vip_level";
    public const string Hidden = "hidden";
    public const string LimitBuy = "limit_buy";
    public const string Salable = "salable";
    public const string Destroyable = "destroyable";
    public const string Usable = "usable";
    public const string Position = "position";
    public const string PriceSell = "price_sell";
    public const string VipExp = "vip_exp";
    public const string ItemId = "item_id";
    public const string Reward = "reward";
    public const string Item = "item";
    public const string Values = "values";
    public const string EffectConsumableGold = "effect_consume_gold";
    public const string MaxExp = "max_exp";
    public const string EnhanceExp = "enhance_exp";
    public const string EvolveExp = "evolve_exp";
    public const string AdditionValue = "addition_value";
    public const string ColorHero = "color_hero";
    public const string Icon = "icon";
    public const string SynthesizeValue = "synthesize_value";
    #endregion

    public const string Ids = "ids";
    public const string Heroes = "heroes";
    public const string ElementAsset = "element_asset";
    public const string Asset3dBody = "asset_3d_body";
    public const string Asset3dFace = "asset_3d_face";
    public const string Asset3dHair = "asset_3d_hair";
    public const string Animator = "animator";
    public const string Name = "name";
    public const string Node = "node";
    public const string Ratio = "ratio";
    public const string RatioFailAdded = "ratio_fail_added";
    public const string MainPoint = "main_point";
    public const string RequiredPoint = "point_required";
    public const string RequiredLevel = "level_required";
    public const string Key = "key";
    public const string Group = "group";
    public const string MainNode = "main_node";
    public const string StarLevel = "star_level";
    public const string Data = "data";
    public const string BaseStat = "base_stat";
    public const string GainStat = "gain_stat";
    public const string Min = "min";
    public const string Max = "max";
    public const string Inventory = "inventory";
    public const string ConsumableData = "consumable_data";
    public const string EquipmentData = "equipment_data";
    public const string HeroSoulData = "hero_soul_data";
    public const string QuestData = "quest_data";
    public const string EventData = "event_data";
    public const string FashionData = "fashion_data";
    public const string Squad = "squad";
    public const string MainSquad = "main_squad";
    public const string SubSquad = "sub_squad";
    public const string Pos = "pos";
    public const string BattlePos = "btl_pos";
    public const string IsLeader = "is_leader";
    public const string UserRequiredLevel = "user_level_required";
    public const string GoldRequired = "gold_required";
    public const string SlotOpen = "slot_open";
    public const string Slot = "slot";
    public const string Properties = "properties";
    public const string HeroSoul = "hero_soul";
    public const string HeroEquipment = "hero_equipment";
    public const string HeroAcupoint = "hero_acupoint";
    public const string HeroSkill = "hero_skill";
    public const string KeepQuality = "keep_color"; //(boolean) 
    public const string KeepFaction = "keep_faction"; //(boolean)
    public const string KeepElement = "keep_element"; //(boolean)
    public const string EquipId = "equiped_id";
    public const string DetailInfo = "detail_info";
    public const string Hero = "hero";
    public const string EquipmentId = "equipment_id";
    public const string OneSpinCostRequired = "money_cost_1";
    public const string OneSpinItemCostRequired = "consumable_item_cost_1";
    public const string TenSpinCostRequired = "money_cost_10";
    public const string TenSpinItemCostRequired = "consumable_item_cost_10";
    public const string RemainSpinForHero = "remain_receive_special";
    public const string SpinOne = "spin_one";
    public const string RequiredRecruitPeice = "required_recruit_peice";
    public const string HeroRecruitPiece = "hero_recruit_piece";
    public const string Remain = "remain";
    public const string remainSecond = "remain_second";
    public const string MaxFreeInDay = "max_free_in_day";
    public const string AlternateConsumableItem = "alternate_consumable_item";
    public const string RequireExp = "require_exp";
    public const string ExpRequiredNextLevel = "exp_required_next_level";
    public const string ExpSacrifice = "exp_sacrifice";
    public const string SubSlot = "sub_slot";
    public const string Texture = "texture";

    #region Awaken
    /*
        * Đột phá cấp 3
Yêu cầu:
        "char_level_required": 490 => nhân vật cấp 490
        "soul_level_required": 70 => chân nguyên nguyên liệu cấp 70
        "soul_star_required": 7 => chân nguyên nguyên lieu 7 sao
        "total_soul_required": 5 =>5 chân nguyên nguyên lieu
Tác dung:
        "total_slot_soul_open": 8,  => mở slot 8 của bang chân nguyên nhân vật
        "soul_max_star": 8,  => chân nguyên đc nâng cấp lên tối đa 8 sao
        */
    public const string RequiredHeroLevel = "char_level_required";
    public const string RequiredHeroSoulLevel = "soul_level_required";
    public const string RequiredHeroSoulStar = "soul_star_required";
    public const string RequiredSlot = "total_soul_required";
    public const string ResultTotalSlotSoulOpen = "total_slot_soul_open_result";
    public const string ResultSoulMaxStar = "soul_max_star_result";

    public const string RequiredAcupointNode = "acupoint_node_require";
    public const string RequiredAwakenLevel = "awaken_level_require";
    #endregion
    public const string TotalExpRequired = "total_exp_require";

    #region Skills
    public const string LearnedSkills = "learned_skills";
    #endregion
    public const string GroupId = "group_id";
    public const string Category = "category";
    public const string LocName = "loc_name";
    public const string Desc = "desc";
    public const string LocDescParam = "loc_desc_param";
    public const string SummonId = "summon_id";
    public const string RootSkillId = "root_skill_id";
    public const string NextSkillId = "next_skill_id";
    public const string HeroStarRequired = "hero_star_required";
    public const string AcupointIdRequired = "acupoint_id_required";
    public const string Pattern = "pattern";
    public const string Chain = "chain";
    public const string AnimationKey = "animation_key";
    public const string ListBuff = "list_buff";
    public const string TriggerProperty = "trigger_property";
    public const string PassiveProperty = "passive_property";
    public const string BattlePosition = "battle_position";
    public const string InfoPosition = "info_position";
    public const string Interuptible = "interuptible";
    public const string IsChanneling = "isChanneling";
    public const string ChannelingTime = "channelingTime";
    public const string Learnable = "learnable";
    public const string CoolDown = "cooldown";
    public const string MaxRecharge = "maxRecharge";
    public const string Enhances = "enhances";
    public const string SkillGroupId = "skill_group_id";
    public const string EnhanceTargetType = "enhance_target_type";
    public const string EnhanceTargetIndex = "enhance_target_index";
    public const string EnhancePro = "enhance_prop";
    public const string EnhanceEffect = "enhance_effect";
    public const string EnhanceTrigger = "enhance_trigger";
    public const string Factor = "factor";
    public const string FactorExp = "factor_exp";
    public const string Attributes = "attributes";

    #region Equipment
    public const string Enchant = "enchant";
    public const string RequiredValue = "require_value";
    public const string MainPropertyInscreateRatio = "pct_increase_equipment_basic";
    public const string SubPropertyInscreateRatio = "pct_increase_equipment_sub";
    public const string RecycleSuiteCommon = "recycle_suite_common";
    public const string RecycleSuite = "recycle_suite";
    public const string MaxRateSuccess = "max_rate_success";
    public const string FailDecreaseEnchant = "fail_decrease_enchant";
    public const string WeaponId = "weapon_id";
    public const string MinPriceTrade = "min_price_trade";
    public const string MaxPriceTrade = "max_price_trade";
    public const string FineRow = "fine_row";
    public const string PropertyData = "property_data";
    public const string ValueExtend = "value_extend";
    public const string ColorCode = "color_code";
    public const string NamePrefix = "name_prefix";
    public const string Active = "active";
    public const string PropertyTypeLocId = "property_type_loc_id";
    public const string PropertyType = "property_type";
    public const string MainProperties = "main_properties";
    public const string FineEquipmentRequired = "fine_equipment_required";
    public const string FineGroupType = "fine_group_type";
    public const string ProperyId = "property_id";
    public const string SlotEnchantRequired = "slot_enchant_required";
    public const string Status = "status";
    public const string Rows = "rows";
    public const string Row = "row";
    public const string CategoryInfo = "category_info";
    public const string SkillPropertyId = "skill_property_id";
    public const string CategoryType = "category_type";
    public const string CategoryName = "category_name";
    public const string NumPiece = "num_piece";
    public const string RequireQuantity = "require_quantity";
    public const string CategoryId = "category_id";
    public const string CurrentGroup = "curr_group";
    public const string LearnedGroup = "learned_group";
    public const string TotalLevel = "total_level";
    public const string PassivePropertyId = "passive_property_id";
    public const string SubMinRow = "sub_min_row";
    public const string SubMaxRow = "sub_max_row";
    #endregion

    #region IAP
    public const string gId = "gId";
    public const string errorKey = "errorKey";

    #endregion

    #region EGT
    public const string errorKey_egt = "errorKey_egt";
    #endregion

    #region hero attribute in battle
    public const string fdam_point = "fdam_point";
    public const string fdam_res_point = "fdam_res_point";
    public const string fdam_rate = "fdam_rate";

    public const string fdam_res_rate = "fdam_res_rate";
    public const string opatk = "opatk";
    public const string pdef = "pdef";
    public const string pdef_pen = "pdef_pen";
    public const string mdef = "mdef";
    public const string mdef_pen = "mdef_pen";
    public const string lucky_hit_dam = "lucky_hit_dam";
    public const string lucky_hit_rate = "lucky_hit_rate";
    public const string crit_dam = "crit_dam";
    public const string crit_res_dam = "crit_res_dam";
    public const string crit_rate = "crit_rate";
    public const string crit_res_rate = "crit_res_rate";
    public const string ptype = "ptype";
    public const string min_atk = "min_atk";
    public const string max_atk = "max_atk";
    public const string skill_amp = "skill_amp";
    public const string p_dam_reduce = "p_dam_reduce";
    public const string element_dam_reduce = "element_dam_reduce";
    public const string mtype = "mtype";
    public const string m_dam_reduce = "m_dam_reduce";
    public const string p_acc = "p_acc";
    public const string p_eva = "p_eva";
    public const string p_eva_pen = "p_eva_pen";
    public const string m_acc = "m_acc";
    public const string m_eva = "m_eva";
    public const string m_eva_pen = "m_eva_pen";

    public const string dodge_rate = "dodge_rate";
    public const string metal_res = "metal_res";
    public const string metal_res_pen = "metal_res_pen";
    public const string metal_debuff_rate = "metal_debuff_rate";
    public const string metal_debuff_res = "metal_debuff_res";
    public const string metal_debuff_inc_time = "metal_debuff_inc_time";
    public const string metal_debuff_reduce_time = "metal_debuff_reduce_time";
    public const string metal_res_pct = "metal_res_pct";
    public const string metal_res_pen_pct = "metal_res_pen_pct";
    public const string metal_debuff_rate_pct = "metal_debuff_rate_pct";
    public const string metal_debuff_res_pct = "metal_debuff_res_pct";
    public const string metal_debuff_inc_time_pct = "metal_debuff_inc_time_pct";
    public const string metal_debuff_reduce_time_pct = "metal_debuff_reduce_time_pct";
    public const string plant_res = "plant_res";
    public const string plant_res_pen = "plant_res_pen";
    public const string plant_debuff_rate = "plant_debuff_rate";
    public const string plant_debuff_res = "plant_debuff_res";
    public const string plant_debuff_inc_time = "plant_debuff_inc_time";
    public const string plant_debuff_reduce_time = "plant_debuff_reduce_time";
    public const string plant_res_pct = "plant_res_pct";
    public const string plant_res_pen_pct = "plant_res_pen_pct";

    public const string plant_debuff_rate_pct = "plant_debuff_rate_pct";
    public const string plant_debuff_res_pct = "plant_debuff_res_pct";
    public const string plant_debuff_inc_time_pct = "plant_debuff_inc_time_pct";
    public const string plant_debuff_reduce_time_pct = "plant_debuff_reduce_time_pct";
    public const string water_res = "water_res";
    public const string water_res_pen = "water_res_pen";
    public const string water_debuff_rate = "water_debuff_rate";
    public const string water_debuff_res = "water_debuff_res";
    public const string water_debuff_inc_time = "water_debuff_inc_time";
    public const string water_debuff_reduce_time = "water_debuff_reduce_time";
    public const string water_res_pct = "water_res_pct";
    public const string water_res_pen_pct = "water_res_pen_pct";
    public const string water_debuff_rate_pct = "water_debuff_rate_pct";
    public const string water_debuff_res_pct = "water_debuff_res_pct";
    public const string water_debuff_inc_time_pct = "water_debuff_inc_time_pct";
    public const string water_debuff_reduce_time_pct = "water_debuff_reduce_time_pct";
    public const string fire_res = "fire_res";
    public const string fire_res_pen = "fire_res_pen";
    public const string fire_debuff_rate = "fire_debuff_rate";
    public const string fire_debuff_res = "fire_debuff_res";
    public const string fire_debuff_inc_time = "fire_debuff_inc_time";
    public const string fire_debuff_reduce_time = "fire_debuff_reduce_time";
    public const string fire_res_pct = "fire_res_pct";
    public const string fire_res_pen_pct = "fire_res_pen_pct";
    public const string fire_debuff_rate_pct = "fire_debuff_rate_pct";

    public const string fire_debuff_res_pct = "fire_debuff_res_pct";
    public const string fire_debuff_inc_time_pct = "fire_debuff_inc_time_pct";
    public const string fire_debuff_reduce_time_pct = "fire_debuff_reduce_time_pct";
    public const string earth_res = "earth_res";
    public const string earth_res_pen = "earth_res_pen";
    public const string earth_debuff_rate = "earth_debuff_rate";
    public const string earth_debuff_res = "earth_debuff_res";
    public const string earth_debuff_inc_time = "earth_debuff_inc_time";
    public const string earth_debuff_reduce_time = "earth_debuff_reduce_time";
    public const string earth_res_pct = "earth_res_pct";
    public const string earth_res_pen_pct = "earth_res_pen_pct";
    public const string earth_debuff_rate_pct = "earth_debuff_rate_pct";
    public const string earth_debuff_res_pct = "earth_debuff_res_pct";
    public const string earth_debuff_inc_time_pct = "earth_debuff_inc_time_pct";
    public const string earth_debuff_reduce_time_pct = "earth_debuff_reduce_time_pct";

    #endregion

    public const string Scene = "scene";
    public const string PositionX = "posX";
    public const string PositionY = "posY";
    public const string PositionZ = "posZ";
    public const string IsMainMap = "is_main_map";
    public const string Width = "width";
    public const string Height = "height";
    public const string IsSuccess = "is_success";
    public const string AffectKey = "affect_key";
    public const string SuiteCommonId = "suite_common_id";
    public const string NPC = "npc";
    public const string CraftId = "craft_id";
    public const string Materials = "materials";
    public const string MaxLevel = "max_level";
    public const string GoldRate = "gold_rate";
    public const string GrindAble = "grindable";
    public const string SubType2 = "sub_type2";
    public const string SubType3 = "sub_type3"; // SubType3 < SubType2 < SubType < ItemGroupType
    public const string NumGrind = "num_grind";
    public const string Quality = "quality";
    public const string quantity = "quantity";
    public const string IncreasePercent = "increase_pct";
    public const string PairType = "pair_type";
    public const string Colors = "colors";
    public const string Levels = "levels";
    public const string TitleLocParam = "title_loc_param";
    public const string DesLocParam = "desc_loc_param";
    public const string TitleLocId = "title_loc_id";
    public const string DescLocId = "desc_loc_id";
    public const string Mails = "mails";
    public const string Date = "date";
    public const string ExpiredDate = "date_expired";

    #region BossReward
    public const string GoldReward = "GoldReward";
    public const string UserID = "UserID";
    public const string BossID = "BossID";
    public const string StationID = "StationID";
    public const string TotalGold = "TotalGold";
    public const string JackpotPrice = "JackpotPrice";
    #endregion
    #region OneSignal
    public const string notifyId = "notifyId";
    public const string pushNotifyToken = "pushNotifyToken";
    public const string deviceId = "deviceId";
    #endregion
}

public static class BCWebCommand
{
    public const string LOGIN = "LOGIN";
    public const string RENEW_ACCESS_TOKEN = "Renew_Access_Token";
    public const string UPDATE_FIRST_OPEN = "UPDATE_FIRST_OPEN";
    public const string REGISTER = "REGISTER";
    public const string GET_CAPTCHA = "GET_CAPTCHA_INFO";
    public const string GET_USER_INFO = "GetUserInfo";
    public const string CHANGE_PASS = "ChangePassword";
    public const string FORGOT_PASSWORD = "FORGOT_PASSWORD";
    public const string CHANGE_PHONE_AND_CMND = "CHANGE_PHONE_AND_CMND";
    public const string LINK_ACCOUNT = "LINK_ACCOUNT";
    public const string CHANGE_PASSWORD = "CHANGE_PASSWORD";
    public const string GET_CHALLENGE_GAME_INFO = "GET_CHALLENGE_GAME_INFO";
    public const string GET_TOURNAMENT_INFO = "GET_TOURNAMENT_INFO";
    public const string GET_PK_INFO = "GET_PK_INFO";
    public const string LOGIN_GAME = "LOGIN_GAME";
    public const string VIEW_ADS = "VIEW_ADS";
    public const string USER_USE_CODE = "USER_USE_CODE";
    public const string GET_MINI_GAME_INFO = "GET_MINI_GAME_INFO";
    public const string TRACKING = "TRACKING";
}

public static class EffectKey
{
    public const string FX_LaTouch = "FX_LaTouch";
    public const string FX_PhiTieuTouch = "FX_PhiTieuTouch";
    public const string FXImpactKnife = "FXImpactKnife";
    public const string FX_DaoTouch = "FX_DaoTouch";
    public const string FXReborn = "FXReborn";
    public const string FXAtkUp = "FXAtkUp";
    public const string EnemyDieFX = "EnemyDieFX";
    public const string FXBlast = "FXBlast";
    public const string FXBolt = "FXBolt";
    public const string FXPoision = "FXPoision";
    public const string FXIce = "FXIce";
    public const string SkillAmulet = "SkillAmulet";
    public const string FXFloatingDamageRoBot = "FXFloatingDamageRoBot";
    public const string FXFloatingDamageCrit = "FXFloatingDamageCrit";
    public const string FXFloatingDamageOneHit = "FXFloatingDamageOneHit";
    public const string FXFloatingDamageSeft = "FXFloatingDamageSeft";
    public const string FXFloatingDamageMiss = "FXFloatingDamageMiss";
    public const string FXCold = "FXCold";
    public const string ColdArea = "ColdArea";
    public const string LevelUp = "LevelUp";
    public const string Halo = "Halo";
    public const string BlastCharm = "BlastCharm";
    public const string IceCharm = "IceCharm";
    public const string LightingCharm = "LightingCharm";
    public const string ToxicCharm = "ToxicCharm";
    public const string Thunderstorm = "Thunderstorm";
    public const string PoisonGas = "PoisonGas";
    public const string SnowBall = "SnowBall";
    public const string FireBall = "FireBall";
    public const string FireBallArea = "FireBallArea";
    public const string FXHealth = "FXHealth";
    public const string SnowBallArea = "SnowBallArea";
    //public const string PoisonGas = "PoisonGas";
}

public static class BCKey
{
    #region skill
    public const string Effect = "Effect";
    public const string ID_Skill = "ID_Skill";
    public const string Icon_Skill = "Icon_Skill";
    public const string Name_Skill = "Name_Skill";
    public const string Limit = "Limit";
    public const string DameAttack = "DameAttack";
    #endregion
    public const string urlFeedBack = "urlFeedback";
    public const string PositionNPC = "PositionNPC";
    public const string urlCredits = "urlCredits";
    public const string urlTerms = "urlTerms";
    public const string List_Skill = "List_Skill";
    public const string removed_data = "removed_data";
    public const string lv = "lv";
    public const string exp_require = "exp_require";
    public const string lv_bonusgem = "lv_bonusgem";

    public const string Radius = "Radius";
    public const string urlPolicy = "urlPolicy";
    public const string Language = "Language";
    public const string DameEffect = "DameEffect";
    public const string EnergyInStage = "EnergyInStage";
    public const string Lv_Equipment = "Lv_Equipment";
    public const string require_material = "require_material";
    public const string require_gold = "require_gold";
    public const string stage_minreward = "stage_minreward";
    public const string stage_maxreward = "stage_maxreward";
    public const string id_item = "id_item";
    public const string stage_userexp = "stage_userexp";
    public const string atk_bonus = "atk_bonus";
    public const string hp_bonus = "hp_bonus";
    public const string item_droprate = "item_droprate";
    public const string wheel_rate = "wheel_rate";
    public const string wheel_suite = "wheel_suite";
    public const string angel_rate = "angel_rate";
    public const string devil_rate = "devil_rate";
    public const string vendor_rate = "vendor_rate";
    public const string vendor_suite = "vendor_suite";
    public const string ads_bonus = "ads_bonus";
    public const string random_skill_list = "random_skill_list";
    public const string monster_suite = "monster_suite";
    public const string boss_wheel_rate = "boss_wheel_rate";
    public const string chapter = "chapter";
    public const string start_wheel_id = "start_wheel_id";
    public const string boss_wheel_id = "boss_wheel_id";
    public const string devil_skill = "devil_skill";
    public const string angel_skill1 = "angel_skill1";
    public const string angel_skill2 = "angel_skill2";
    public const string ads_wheel_id = "ads_wheel_id";

    public const string Talents = "Talents";
    public const string Levels_exp = "levels_exp";
    public const string ChapterLength = "Chapter_Length";
    public const string Wheel_Data = "Wheel_Data";
    public const string Type = "Type";
    public const string ChapterMap = "Chapter";
    public const string StageMap = "Stage";
    public const string ListMapId = "List_ID_Map";
    public const string Coordinate = "coordinate";
    public const string Id = "Id";
    public const string Bounus = "Bounus";
    public const string Default = "Default";
    public const string Desc_Pos_OffSet = "Desc_Pos_OffSet";
    public const string Desc_Point_Pos = "Desc_Point_Pos";
    public const string Name = "Name";
    public const string IconName = "IconName";
    public const string UserID = "UserID";
    public const string Password = "Password";
    public const string password = "password";
    public const string confirmPassword = "confirmPassword";
    public const string username = "username";
    public const string accountType = "accountType";
    public const string TokenOpenID = "TokenOpenId";
    public const string FBTokenId = "FB_TokenId";
    public const string FBTokenIdExpireTime = "FB_TokenId_ExpireTime";
    public const string Message = "Message";
    public const string Data = "Data";
    public const string Captcha = "Captcha";
    public const string UserId = "UserId";
    public const string phone = "phone";
    public const string charm = "charm";
    public const string key = "key";
    public const string Key = "Key";
    public const string IdKey = "IdKey";
    public const string IDEquipmentUse = "IDEquipmentUse";
    public const string Value = "Value";
    public const string Dec_Effect = "Dec_Effect";
    public const string key1 = "key1";
    public const string key2 = "key2";
    public const string key3 = "key3";
    public const string webServiceToken = "webServiceToken";
    public const string serverData = "serverData";

    public const string BuyMores = "BuyMores";
    public const string Token = "Token";
    public const string KindID = "KindID";
    public const string IsError = "IsError";
    public const string IsEnable = "IsEnable";
    public const string LevelLimit = "LevelLimit";
    public const string Server = "Server";
    public const string GoldLimit = "GoldLimit";

    public const string Ticket = "ticket";
    public const string Money = "Money";
    public const string ID = "ID";
    public const string Asset_Name = "Asset_Name";
    public const string asset_name_ = "asset_name";
    public const string Hide = "Hide";
    public const string Price_Gold = "price_gold";
    public const string Type_Item = "Type_Item";
    public const string type_item = "type_item";
    public const string Grade_Item = "Grade_Item";
    public const string grade_item = "grade_item";
    public const string Gem_price = "Gem_price";
    public const string gem_price = "gem_price";
    public const string Bounus_Enhance = "Bounus_Enhance";
    public const string bonus_enhance = "bonus_enhance";
    public const string Main_property = "Main_property";
    public const string main_property = "main_property";
    public const string Sub_property = "Sub_property";
    public const string sub_property = "sub_property";
    public const string Name_Item = "Name_Item";
    public const string name_item = "name_item";
    public const string Dec_Item = "Dec_Item";
    public const string dec_item = "dec_item";

    public const string ItemImage = "ItemImage";
    public const string ItemName = "ItemName";
    public const string Msg = "Msg";
    public const string TimeRecord = "TimeRecord";
    public const string separation = "separation";
    public const string game_scope = "game_scope";

    public const string ChatMessage = "Message";

    public const string MatchID = "MatchID";
    public const string State = "State";
    public const string StateName = "StateName";
    public const string TimeCoolDown = "TimeCoolDown";
    public const string Result = "Result";
    public const string Result_Type = "Result_Type";
    public const string GoldUserBetTai = "GoldUserBetTai";
    public const string GoldUserBetXiu = "GoldUserBetXiu";
    public const string TotalTai = "TotalTai";
    public const string NumberUserBetTai = "NumberUserBetTai";
    public const string TotalXiu = "TotalXiu";
    public const string NumberUserBetXiu = "NumberUserBetXiu";
    public const string BetValueList = "BetValueList";
    public const string BetValueList_Type = "BetValueList_Type";
    public const string MinBet = "MinBet";
    public const string MaxBet = "MaxBet";
    public const string GoldUserWin = "GoldUserWin";
    public const string TotalUserGold = "TotalUserGold";
    public const string BetID = "BetID";
    public const string GoldBet = "GoldBet";
    public const string TotalBet = "TotalBet";
    public const string NumberBet = "NumberBet";
    public const string GoldWin = "GoldWin";
    public const string TopWin = "TopWin";
    public const string WebGLStreamingAssetUrl = "WebGLStreamingAssetUrl";
    public const string show_event_tutorial = "show_event_tutorial";
    public const string next_remaining_time = "next_remaining_time";

    public const string max_battle_chat_message = "max_battle_chat_message";
    public const string max_characters_per_chat = "max_characters_per_chat";
    public const string battle_chat_send_delay = "battle_chat_send_delay";

    public const string Reward = "Reward";
    public const string TimeTaiXiu = "Time";
    public const string Dices = "Dices";
    public const string Dices_Type = "Dices_Type";

    public const string data = "data";
    public const string delay_transitions_popup = "delay_transitions_popup";
    public const string delay_atk_begin_stage = "delay_atk_begin_stage";
    public const string Kind = "kind";
    public const string stage_exp = "stage_exp";
    public const string AssetBundleName = "assetbundle_name";
    public const string AssetBundleSize = "size";
    public const string AssetBundleChildCount = "child_count";
    public const string Property = "property";
    public const string property_list = "property_list";

    public const string Time = "Time";
    public const string StartTime = "start_time";
    public const string EndTime = "end_time";
    public const string AnimName = "anim_name";
    public const string size = "size";
    public const string GoldData = "gold";
    public const string fish_scale_per_unit = "fish_scale_per_unit";
    public const string fish_min_scale = "fish_min_scale";
    public const string fish_max_scale = "fish_max_scale";
    public const string fish_num = "fish_num";

    public const string scale_freeze = "scale_freeze";
    public const string pos_freeze = "pos_freeze";
    public const string pos = "pos";
    public const string scale_slow = "scale_slow";
    public const string pos_slow = "pos_slow";

    public const string Gold = "Gold";
    public const string battle_gold = "battle_gold";
    public const string bullet_left = "bullet_left";
    public const string bullet_delay_origin = "bullet_delay_origin";
    public const string delay_send_catchfish = "delay_send_catchfish";
    public const string max_smart_fox_response_data = "max_smart_fox_response_data";
    public const string max_smart_fox_response_data_total = "max_smart_fox_response_data_total";
    public const string smart_fox_send_ignore_command_list = "smart_fox_send_ignore_command_list";
    public const string smart_fox_response_ignore_command_list = "smart_fox_response_ignore_command_list";

    #region BattleConfigData
    public const string Loc_ID = "Loc_ID";

    public const string TopScreenId = "TopScreenId";
    public const string LeftScreenId = "LeftScreenId";
    public const string RightScreenId = "RightScreenId";
    public const string GoldIcon_LocId = "GoldIcon_LocId";
    public const string GemIcon_LocId = "GemIcon_LocId";

    public const string isShowTaskButton = "isShowTaskButton";
    public const string isShowChangeGun = "isShowChangeGun";
    public const string isShowChangeWing = "isShowChangeWing";
    public const string isShowQuickChat = "isShowQuickChat";
    public const string isShowBattleChatBox = "isShowBattleChatBox";

    public const string isShowSkills = "isShowSkills";
    public const string isShowMultipleShot = "isShowMultipleShot";
    public const string isShowRightScreenLeaderBoard = "isShowRightScreenLeaderBoard";
    public const string isShowChangeMultipleShot = "isShowChangeMultipleShot";
    public const string isUseAmmo = "isUseAmmo";
    public const string isBuyMore = "isBuyMore";

    #endregion

    public const string num_buy = "num_buy";
    public const string Gem_ = "Gem";

    public const string UpdateTalents = "UpdateTalents";
    public const string MapUnlock = "MapUnlock";
    public const string ChapUnlock = "ChapUnlock";
    public const string AchivementUnLock = "AchivementUnLock";
    public const string AchivementReceive = "AchivementReceive";
    public const string ExpLevel = "ExpLevel";

    public const string gem = "gem";
    public const string ads = "ads";
    public const string ads_rate = "ads_rate";
    public const string max_buy_more_battle_gold = "max_buy_more_battle_gold";
    public const string max_bullet_on_screen = "max_bullet_on_screen";
    public const string gem_cost_buy = "gem_cost_buy";
    public const string gold_battle_received = "gold_battle_received";
    public const string Medal = "medal";
    public const string Nickname = "Nickname";
    public const string nick = "nick";
    public const string day = "day";
    public const string month = "month";
    public const string year = "year";
    public const string gameOS = "gameOS";
    public const string is_first_login = "is_first_login";
    public const string gameVersion = "gameVersion";
    public const string captchaId = "cId";
    public const string captcha = "captcha";
    public const string Level = "Level";
    public const string intLevel = "intLevel";
    public const string Exp = "Exp";
    public const string VIPLevel = "VIPLevel";
    public const string VIPExp = "VIPExp";
    public const string StationID = "StationID";
    public const string PosX = "PosX";
    public const string PosY = "PosY";
    public const string PosZ = "PosZ";
    public const string achievement_point = "achievement_point";

    public const string Index = "Index";

    public const string Angle = "Angle";
    public const string BulletID = "BulletID";
    //public const string GunKind = "GunID";
    public const string BulletMultiple = "BulletMultiple";
    public const string LockedFishID = "LockedFishID";
    public const string ListUserInDesk = "ListUserInDesk";

    public const string FishID = "FishID";
    public const string fish_effect_id = "fish_effect_id";
    public const string effect_on_fish = "effect_on_fish";
    public const string skill_effect_list = "skill_effect_list";
    public const string energy = "energy";
    public const string FishId = "FishId";
    public const string catched_fish = "catched_fish";
    public const string BossID = "BossID";
    public const string NextBossKind = "NextBossKind";
    public const string nTSP = "nTSP";
    public const string ListFish = "ListFish";
    public const string ListGun = "ListGun";
    public const string ListID = "ListID";
    public const string ListFishID = "ListFishID";
    public const string FishKind = "FishKind";
    public const string fish_kind = "fish_kind";
    public const string speed = "speed";
    public const string path_kind = "path_kind";
    public const string leaderboard_data = "leaderboard_data";
    public const string iap_data = "iap_data";
    public const string path_start_pos = "path_start_pos";
    public const string path_scale = "path_scale";
    public const string SpecialKind = "SpecialKind";
    public const string ItemID = "ItemID";

    public const string GoldReward = "GoldReward";
    public const string TotalGold = "TotalGold";

    public const string ParadeKind = "ParadeKind";
    public const string BirthTime = "BirthTime";
    public const string SkillID = "SkillID";
    public const string GunID = "GunID";
    public const string PathKind = "PathKind";
    public const string BossPathKind = "BossPathKind";
    public const string Interval = "Interval";
    public const string Priority = "Priority";
    public const string UrlLink = "UrlLink";
    public const string Description = "Description";
    public const string Image = "Image";
    public const string charm_add = "charm_add";
    public const string Currency_h_icon_ = "h_icon_";
    public const string Currency_h_dan = "h_dan";
    public const string Currency_h_diem = "h_diem";
    #region BC68
    public const string ServerId = "serverId";
    public const string gameServerId = "gameServerId";
    public const string mini_game_type = "mini_game_type";
    public const string DeviceId = "deviceId";
    public const string Url = "url";
    public const string Code = "code";
    public const string ErrorCode = "ec";
    public const string ErrorString = "es";
    public const string message = "message";
    public const string isGetGiftUpdateInfo = "isGetGiftUpdateInfo";
    public const string isGetGiftLinkAccount = "isGetGiftLinkAccount";
    public const string achievement_title_id = "achievement_title_id";
    public const string is_fix_screen_size = "is_fix_screen_size";
    public const string is_delete_old_assetbundle = "is_delete_old_assetbundle";

    public const string dataconfigurl_dev = "dataconfigurl_dev";
    public const string dataconfigurl_qc = "dataconfigurl_qc";
    public const string dataconfigurl_online = "dataconfigurl_online";

    public const string FacebookToken = "facebookToken";
    public const string fp = "fp";
    public const string result = "result";
    public const string id = "id";
    //public const string suite_id = "suite_id";
    public const string ads_atk = "ads_atk";
    public const string ads_hp = "ads_hp";
    public const string hp = "hp";
    public const string angel_ads_rate = "angel_ads_rate";
    public const string AssetName = "AssetName";
    public const string NameGrade = "NameGrade";
    public const string MaxUpdate = "MaxUpdate";
    public const string cmnd = "cmnd";
    public const string buy_more_id = "buy_more_id";
    public const string time_buy = "time_buy";
    public const string bullet_receive = "bullet_receive";
    public const string gold_battle_receive = "gold_battle_receive";
    public const string require_vip = "require_vip";
    public const string cost = "cost";
    public const string cost_type = "cost_type";
    public const string tournament_type = "tournament_type";
    public const string Email = "email";
    public const string TimeServer = "time_server";
    public const string PlayerBase = "playerBase";
    public const string UserInfoInDesk = "UserInfoInDesk";
    public const string vip_level = "vip_level";
    public const string vip_unlock = "vip_unlock";
    public const string list_charm_gift = "list_charm_gift";
    public const string consumable_id = "consumable_id";
    public const string KeyCmd = "CMD";
    public const string KeyMethod = "param";
    public const string KeyErrorCode = "code";
    public const string KeyErrorMessage = "message";
    public const string Cmnd = "cmnd";
    public const string sync = "sync";
    public const string NewPassword = "newPassword";
    public const string quest_data = "quest_data";
    public const string quest = "quest";
    public const string reward = "reward";
    public const string reward_bonus = "reward_bonus";
    public const string reward_percent = "reward_percent";
    public const string suite_common_id = "suite_common_id";
    public const string require_suite_common = "require_suite_common";
    public const string reward_suite = "reward_suite";
    public const string suite_common_suite_id = "suite_common_suite_id";
    public const string suite_drop_rate = "suite_drop_rate";
    public const string listQuest = "listQuest";
    public const string quest_target = "quest_target";
    public const string quest_type = "quest_type";
    public const string quest_quantity = "quest_quantity";
    public const string current_quantity = "current_quantity";
    public const string serverQCData = "serverQCData";
    public const string ExpNextLevel = "exp_next_level";
    public const string ExpVipNextLevel = "exp_vip_next_level";
    public const string LevelTotalExp = "level_total_exp";
    public const string VipLevelTotalExp = "vip_level_total_exp";
    public const string AutoId = "auto_id";
    public const string GroupId = "group_id";
    public const string GroupRate = "group_rate";
    public const string skill_function = "skill_function";
    public const string list_skills = "list_skills";
    public const string skill_id = "skill_id";
    public const string QuestId = "quest_id";
    public const string Target = "target";
    public const string NextQuestId = "next_quest_id";
    public const string SuiteCommonId = "suite_common_id";
    public const string Chapter = "chapter";
    public const string Stage = "stage";
    public const string Suite_id = "suite_id";
    public const string asset = "asset";
    public const string value = "value";

    public const string SuiteDropRateID = "suite_DropRate_id";
    public const string Quantity = "quantity";
    public const string rate = "rate";
    public const string Asset = "Asset";
    public const string UpgradeAsset = "upgrade_asset";
    public const string AssetSkill = "asset_skill";
    public const string remain_free = "remain_free";
    public const string remain_free_treasure = "remain_free_treasure";
    public const string remain_free_gw = "remain_free_gw";
    public const string is_register = "is_register";
    public const string gun_radius = "gun_radius";

    public const string is_show_random_tutorial = "is_show_random_tutorial";
    public const string random_tutorial_delay_time = "random_tutorial_delay_time";
    public const string random_tutorial_duration_time = "random_tutorial_duration_time";
    public const string random_tutorial_max_time = "random_tutorial_max_time";

    #region Item
    public const string ItemGroupType = "itemGroupType";
    public const string ItemType = "item_type";
    public const string Color = "color";
    public const string ColorText = "color_text";
    public const string MaxValue = "max_value";
    public const string Stats = "stats";
    public const string IsPercent = "is_percent";
    public const string ShopId = "shop_id";
    public const string idSuiteDropRate = "idSuiteDropRate";
    public const string from = "from";
    public const string to = "to";
    public const string Tab = "tab";
    public const string SubType = "sub_type";
    public const string Order = "order";
    public const string Sequence = "sequence";
    public const string DefaultSlot = "default_slot";
    public const string SlotPerPurchase = "slot_per_purchase";
    public const string Price = "price";
    public const string MaxSlot = "max_slot";
    public const string Slots = "slots";
    public const string IsLocked = "is_lock";
    public const string is_friend = "is_friend";
    public const string EquipmentType = "equipment_type";
    public const string EnchantLevel = "enchant_level";
    public const string MaxEnhance = "max_enhance";
    public const string MaxAugment = "max_augment";
    public const string SectLimit = "sect_limit";
    public const string RequireLevel = "require_level";
    public const string EffectValue = "effect_value";
    public const string SkillLevel = "skill_level";
    public const string CostGemOriginal = "cost_gem_original";
    public const string CostGemDiscount = "cost_gem_discount";
    public const string LimitTime = "limit_time";
    public const string ShopType = "cash_shop_type";
    public const string VipLevel = "vip_level";
    public const string Hidden = "hidden";
    public const string hidden_type = "hidden_type";
    public const string LimitBuy = "limit_buy";
    public const string Salable = "salable";
    public const string Destroyable = "destroyable";
    public const string Usable = "usable";
    public const string Position = "position";
    public const string FeatureID = "FeatureID";
    public const string NotifyIDs = "NotifyIDs";
    public const string limit_function = "limit_function";
    public const string game_condition_url = "game_condition_url";
    public const string max_item_mail_receive_all = "max_item_mail_receive_all";
    public const string LimitFunctionID = "LimitFunctionID";
    public const string center_buttons = "center_buttons";
    public const string CenterButton = "CenterButton";
    public const string bottom_buttons = "bottom_buttons";
    public const string BottomButton = "BottomButton";
    public const string PriceSell = "price_sell";
    public const string VipExp = "vip_exp";
    public const string Item = "item";
    public const string Values = "values";
    public const string EffectConsumableGold = "effect_consume_gold";
    public const string MaxExp = "max_exp";
    public const string EnhanceExp = "enhance_exp";
    public const string EvolveExp = "evolve_exp";
    public const string AdditionValue = "addition_value";
    public const string ColorHero = "color_hero";
    public const string Icon = "icon";
    public const string SynthesizeValue = "synthesize_value";
    public const string MinPriceTrade = "min_price_trade";
    public const string MaxPriceTrade = "max_price_trade";
    public const string Asset2D = "asset2d";
    public const string Asset3D = "asset3d";
    public const string LocId = "loc_id";
    public const string LocParam = "loc_param";
    public const string LocDesc = "loc_desc";
    public const string RequiredPoint = "point_required";
    public const string RequiredLevel = "level_required";
    public const string PairType = "pair_type";
    public const string SubType2 = "sub_type2";
    public const string SubType3 = "sub_type3"; // SubType3 < SubType2 < SubType < ItemGroupType
    public const string Nick = "nick";
    public const string login = "login";
    public const string register = "register";
    public const string TitleLocParam = "title_loc_param";
    public const string DesLocParam = "desc_loc_param";
    public const string TitleLocId = "title_loc_id";
    public const string DescLocId = "desc_loc_id";
    public const string Mails = "mails";
    public const string Date = "date";
    public const string ExpiredDate = "date_expired";
    public const string Status = "status";
    public const string userId = "uid";
    public const string Items = "items";
    public const string Min = "min";
    public const string Max = "max";
    public const string spawn_time = "spawn_time";
    public const string bomb_spawn_time = "bomb_spawn_time";
    public const string min_z = "min_z";
    public const string max_z = "max_z";
    public const string min_y = "min_y";
    public const string max_y = "max_y";
    public const string min_x = "min_x";
    public const string max_x = "max_x";

    public const string Inventory = "inventory";
    public const string Equipment = "Equipment";
    public const string EquipmentUser = "EquipmentUser";
    public const string idInventory = "idInventory";
    public const string Material = "Material";
    public const string material = "material";
    public const string ConsumableData = "consumable_data";
    public const string SkillData = "skill_data";
    #endregion
    public const string IsReceived = "isReceived";
    public const string LocParams = "locParams";
    public const string Current = "current";
    public const string Avatar = "avatar";
    public const string AvatarBorder = "avatar_border";
    public const string ids = "ids";
    public const string fbId = "fbId";
    public const string uaId = "uaId";//id achivement;
    public const string CostOriginal = "cost_original";
    public const string CostDiscount = "cost_discount";
    public const string percent_discount = "percent_discount";
    public const string curency = "curency";
    public const string achievement = "achievement";
    public const string achievement_id = "achivement_id";
    public const string rank = "rank";
    public const string battle_point = "battle_point";
    public const string pk_point = "pk_point";
    public const string battle_point_reward = "battle_point_reward";
    public const string pk_point_reward = "pk_point_reward";
    public const string player = "player";
    public const string playerinfo = "playerinfo";
    public const string max_user = "max_user";
    public const string mine = "mine";
    public const string isExpiredToken = "isExpiredToken";
    public const string rank_list = "rank_list";
    public const string my_rank = "myRank";
    public const string myRank = "myRank";
    public const string event_id = "event_id";
    public const string is_hide = "is_hide";
    public const string is_end = "is_end";
    public const string event_detail_id = "event_detail_id";
    public const string milestone = "milestone";
    public const string draw_type = "draw_type";
    public const string event_type = "event_type";
    public const string startDate = "startDate";                                    //(string) thời gian bắt đầu
    public const string endDate = "endDate";
    public const string details = "details";
    public const string listEvent = "listEvent";
    public const string receive = "receive";
    public const string type = "type";
    public const string eventId = "eventId";
    public const string eventDetailId = "eventDetailId";
    public const string notify_list = "notify_list";
    public const string NotifyType = "NotifyType";
    public const string capture_probability = "capture_probability";
    public const string multiple_min = "multiple_min";
    public const string multiple_max = "multiple_max";
    public const string Width = "width";
    public const string Height = "height";
    public const string level = "level";
    public const string Text_Color = "text_color";
    public const string total_exp_require = "total_exp_require";
    public const string total_player = "total_player";
    public const string timeLeft = "timeLeft";
    public const string exp_next_level = "exp_next_level";
    public const string desc = "desc";
    public const string loc_desc_params = "loc_desc_params";
    public const string loc_id = "loc_id";
    public const string color_id = "color_id";
    public const string require_for_next_level = "require_for_next_level";
    public const string exchange_rate = "exchange_rate";
    public const string gun_unlock = "gun_unlock";
    public const string remain_time = "remain_time";
    public const string battle_id = "battle_id";
    public const string max_join_time = "max_join_time";
    public const string battle_duration = "battle_duration";
    public const string trophy = "trophy";
    public const string gold = "gold";
    public const string rewards = "rewards";
    public const string user_count = "user_count";
    public const string user_position = "user_position";
    public const string user_1 = "user_1";
    public const string user_2 = "user_2";
    public const string user_3 = "user_3";
    public const string user_4 = "user_4";
    public const string appear_time = "appear_time";
    public const string rank_from = "rank_from";
    public const string rank_to = "rank_to";
    public const string name = "name";
    public const string ip = "ip";
    public const string port = "port";
    public const string zone = "zone";
    public const string server_id = "server_id";
    public const string suite_id = "suite_id";
    public const string date_status = "date_status";
    public const string battle_config = "battle_config";
    public const string fees = "fees";
    public const string fee = "fee";
    public const string fees_type = "fees_type";
    public const string fee_type = "fee_type";
    public const string user_per_match = "user_per_match";
    public const string show_in_wiki = "show_in_wiki";
    public const string effect_id = "effect_id";
    public const string wId = "wId";
    public const string crown_pos = "crown_pos";
    public const string crown_rotation = "crown_rotation";
    public const string crown_scale = "crown_scale";
    public const string crown_path = "crown_path";
    public const string stun_pos = "stun_pos";
    public const string stun_rotation = "stun_rotation";
    public const string stun_scale = "stun_scale";
    public const string stun_path = "stun_path";
    public const string fish_kind_child = "fish_kind_child";
    public const string fish_animation_id = "fish_animation_id";
    public const string special_fish_info = "special_fish_info";
    public const string king_fish_id = "king_fish_id";
    public const string child_id = "child_id";
    public const string hp_per_child = "hp_per_child";
    public const string appear_rate = "appear_rate";
    public const string quantity_required = "quantity_required";
    public const string time_between_check = "time_between_check";
    public const string effect_duration = "effect_duration";
    public const string effect_type = "effect_type";
    public const string start_hour = "start_hour";
    public const string start_minute = "start_minute";
    public const string end_hour = "end_hour";
    public const string end_minute = "end_minute";
    public const string ring = "ring";
    public const string remaining_time = "remaining_time";
    public const string isOnline = "isOnline";
    public const string is_sendable = "is_sendable";
    public const string is_receivable = "is_receivable";
    public const string param = "param";
    public const string maxFriend = "maxFriend";
    public const string listUserFriend = "list_user_friend";
    public const string max_chat_tab = "max_chat_tab";
    public const string list_friend_request = "list_friend_request";
    public const string list_friend_recommend = "list_friend_recommend";
    public const string can_send_reward = "can_send_reward";
    public const string is_have_reward = "is_have_reward";
    public const string chat_config = "chat_config";
    public const string room = "room";
    public const string sfsName = "sfsName";
    public const string time_server = "time_server";
    public const string battle_scale = "battle_scale";
    public const string wiki_scale = "wiki_scale";
    public const string medal_type = "medal_type";
    public const string share_time = "share_time";

    public const string unit = "unit";
    public const string friend_status = "friend_status";
    public const string is_have_request = "is_have_request";
    public const string isHaveGiftReceive = "isHaveGiftReceive";
    public const string productId = "productId";
    public const string productid = "productid";
    public const string TransactionID = "TransactionID";
    public const string gioi_han_tinh_nang_data = "gioi_han_tinh_nang_data";
    public const string data_config_url = "data_config_url";
    public const string max_inventory_chosen_quantity = "max_inventory_chosen_quantity";
    public const string has_update_asset = "has_update_asset";
    public const string is_play_pop_stars = "is_play_pop_stars";
    public const string already_pass_popstars = "already_pass_popstars";
    public const string chat_max_message = "chat_max_message";
    public const string chat_max_message_length = "chat_max_message_length";

    public const string adCountDown = "adCountDown";
    public const string totalAdViewToday = "totalAdViewToday";
    public const string goldAddViewAd = "goldAddViewAd";
    public const string currencyViewAd = "currencyViewAd";
    public const string maxGoldAds = "maxGoldAds";
    public const string countDownViewAd = "countDownViewAd";
    public const string is_view_ads = "is_view_ads";

    public const string review_logo_size = "review_logo_size";

    public const string listLowQualityIOSGeneration = "listLowQualityIOSGeneration";

    public const string suiteIdGiftUpdateInfo = "suiteIdGiftUpdateInfo";
    public const string suiteIdGiftLinkAccount = "suiteIdGiftLinkAccount";

    public const string isShowWebIap = "isShowWebIap";
    public const string afk_kick_seconds = "afk_kick_seconds";

    public const string fb_fanpage = "fb_fanpage";
    public const string is_show_review = "is_show_review";
    #endregion

    //station data
    public const string StationRect = "StationRect";
    public const string VIPRect = "VIPRect";
    public const string GemRect = "GemRect";
    public const string GoldRect = "GoldRect";
    public const string AvatarRect = "AvatarRect";
    public const string ChatBoxRect = "ChatBoxRect";
    public const string GunRect = "GunRect";
    public const string GunRootRect = "GunRootRect";
    public const string WingContainerRect = "WingContainerRect";
    public const string GunButtonRect = "GunButtonRect";
    public const string ButtonsRect = "ButtonsRect";
    public const string GunGoldRect = "GunGoldRect";
    public const string MedalPositionRect = "MedalPositionRect";
    public const string FriendIconRect = "FriendIconRect";

    public const string ExpValue = "exp_value";
    public const string total_exp_value = "total_exp_value";
    public const string day_count = "day_count";
    public const string can_buy = "can_buy";

    public const string fb_upload_url = "fb_upload_url";
    public const string is_show_language_change = "is_show_language_change";

    public const string TimeAutoGun = "TimeAutoGun";

    public const string active_evt = "active_evt";
    #region Card_Puzzle
    public const string shop_id = "shop_id";
    //public const string active_cp = "active_cp";

    public const string child_items = "child_items";

    public const string COF = "COF";
    public const string count = "count";
    public const string puzzle_count = "puzzle_count";
    public const string card_type = "card_type";
    public const string loc_id_name = "loc_id_name";
    public const string puzzle_require = "puzzle_require";
    public const string asset2d = "asset2d";
    #endregion

    #region Fish_Game
    public const string fish_game_id = "fish_game_id";
    public const string one_two_three_id = "one_two_three_id";

    public const string times_play = "times_play";
    public const string times_play_left = "times_play_left";

    public const string duration_time = "duration_time";
    public const string time_left = "time_left";

    public const string fish_game_gold = "fish_game_gold";
    public const string current_gold = "current_gold";
    public const string prize_gold = "prize_gold";
    public const string reward_multiplier = "reward_multiplier";
    #endregion

    #region TreasureWheel
    //public const string active_tw = "active_tw";

    public const string vip_bonus = "vip_bonus";
    public const string vipBonus = "vipBonus";
    public const string vipRequire = "vipRequire";
    public const string vip = "vip";
    public const string login_days = "login_days";
    public const string wheel_id = "wheel_id";
    public const string bonus_id = "bonus_id";
    public const string bonus = "bonus";
    #endregion

    #region Gun Multiple
    public const string max_multiple_shot_angle = "max_multiple_shot_angle";
    public const string is_use_multiple_shot_in_battle = "is_use_multiple_shot_in_battle";
    public const string is_use_multiple_shot_in_challenge = "is_use_multiple_shot_in_challenge";
    public const string is_use_multiple_shot_in_tournament = "is_use_multiple_shot_in_tournament";
    public const string is_use_multiple_shot_in_pk = "is_use_multiple_shot_in_pk";
    #endregion

    #region Li Xi
    //public const string active_lx = "active_lx";

    public const string first_item_shop_id = "first_item_shop_id";
    public const string first_item_type = "first_item_type";
    public const string first_item_quantity = "first_item_quantity";
    public const string second_item_shop_id = "second_item_shop_id";
    public const string second_item_type = "second_item_type";
    public const string second_item_quantity = "second_item_quantity";
    public const string result_item_shop_id = "result_item_shop_id";
    public const string result_item_type = "result_item_type";
    public const string result_item_quantity = "result_item_quantity";
    #endregion

    #region GambleWheel
    //public const string active_gw = "active_gw";
    #endregion

    #region OnePiece
    public const string Node_Id = "node_id";
    public const string Loc_Id = "loc_id";
    public const string Node_Asset = "asset";
    public const string Node_Type = "node_type";
    public const string Suite_Unlock = "require_suite_unlock";
    public const string Suite_Open = "require_suite_open";
    public const string Suite_Open_Type = "suite_open_type";
    public const string Node_Hidden = "hidden";
    public const string Node_Index = "node_index";
    public const string node_next_index = "next_node_index";
    public const string coordinate = "coordinate";
    public const string require_suite_unlock = "require_suite_unlock";
    public const string require_suite_open = "require_suite_open";
    #endregion

    #region shop
    public const string list_gems = "ListGems";
    public const string list_coins = "ListCoins";
    public const string list_chest = "ListChest";
    public const string localize_name_id = "Localize_Name_ID";
    public const string localize_dec_id = "Localize_DEC_ID";
    public const string Localize_Dec_id = "Localize_Dec_ID";
    public const string asset_name = "Asset_Name";
    public const string Asset_Name_Close = "Asset_Name_Close";
    public const string price_text = "Price_Text";
    public const string amount = "Amount";
    public const string amount1 = "amount";
    public const string ItemMaterial = "Item_Material";
    public const string Key_IAP_Android = "Key_IAP_Android";
    public const string Key_IAP_IOS = "Key_IAP_IOS";
    #endregion
    public const string Item_ID = "Item_Id";
    public const string Gem = "Gem";
    public const string Energy = "Energy";
    public const string IDItem = "IDItem";

}

public static class BCShopGunKey
{
    public const string data = "data";

    //Gun
    public const string id = "id";
    public const string group_id = "group_id";
    public const string name = "name";
    public const string cost = "cost";
    public const string currency = "currency";
    public const string currency_type = "currency_type";
    public const string grade = "grade";
    public const string bounce = "bounce";
    public const string avatar_id = "avatar_id";
    public const string prefab_id = "prefab_id";
    public const string bullet_id = "bullet_id";
    public const string net_id = "net_id";

    public const string description = "description";
    public const string loc_params = "loc_params";
    public const string loc_desc = "loc_desc";

    public const string desc = "desc";
    public const string guns = "guns";
    public const string wings = "wings";
    public const string frames = "frames";
    public const string borders = "borders";
    public const string skills = "skills";
    public const string list_skill_item = "list_skill_item";
    public const string gun_skin = "gun_skin";
    public const string gun_cost = "gun_cost";
    public const string cost_currency = "cost_currency";
    public const string skill = "skill";
    public const string number_item_require = "number_item_require";
    public const string is_effect_on_boss = "is_effect_on_boss";
    public const string is_effect_on_parade = "is_effect_on_parade";
    public const string require_vip = "require_vip";

    public const string bullet_multiple = "bullet_multiple";
    public const string allowBuy = "allowBuy";


    public const string active_gun_group = "active_gun_group";
    public const string active_gun = "active_gun";
    public const string active_wing = "active_wing";
    public const string active_frame = "active_frame";
    public const string active_border = "active_border";

    public const string gun_group = "gun_group";
    public const string gun = "gun";
    public const string wing = "wing";
    public const string frame = "frame";
    public const string border = "border";

    public const string gun_list = "gun_list"; // [{id, exp_value}
    public const string wing_list = "wing_list"; // [{id, exp_value}

    //Gun Group
    public const string gun_groups = "gun_groups";
    public const string effect_value = "effect_value";
    public const string affect_duration = "affect_duration";
    public const string target_duration = "target_duration";
    public const string max_catch_fish = "max_catch_fish";
    public const string time_between_shoot = "time_between_shoot";

    //Bullet Multiple
    public const string money = "money";
    public const string level_index = "level_index";

    //Gun Multiple
    public const string multi_shot = "multi_shot";
    public const string vip_require = "vip_require";
    public const string auto_gun_requirement = "auto_gun_requirement";

}
public class WebKey
{
    public const string LANGUAGE_CHANGE = "LANGUAGE_CHANGE";
    public const string register = "REGISTER";

    public const string login = "LOGIN";
    public const string renewAccessToken = "RENEW_ACCESS_TOKEN";

    public const string loginGame = "LOGIN_GAME";
    public const string GET_ALL_EVENT_INFO = "GET_ALL_EVENT_INFO";

    public const string UPDATE_DEEPLINK = "UPDATE_DEEPLINK";
    public const string UPDATE_REFLINK = "UPDATE_REFLINK";

    public const string serverChatIp = "serverChatIp";
    public const string serverChatPort = "serverChatPort";
    public const string serverChatZone = "serverChatZone";
    public const string serverChatRoom = "serverChatRoom";
    public const string serverMovingIP = "serverMoveIP";
    public const string serverMovingPort = "serverMovePort";
    public const string serverMovingZone = "serverMoveZone";

    public const string LanguageKey = "language";
    public const string accountType = "accountType";
    public const string username = "username";
    public const string password = "password";
    public const string repassword = "confirmPassword";
    public const string phone = "phone";
    public const string facebookAccessToken = "facebookAccessToken";
    public const string deviceId = "deviceId";
    public const string serverList = "serverList";
    public const string serverData = "serverData";
    public const string groupServerData = "groupServerData";
    public const string group = "group";
    public const string facebookToken = "facebookToken";
    public const string gameVersion = "gameVersion";
    public const string webServiceToken = "webServiceToken";
    public const string serverId = "serverId";
    public const string serverCode = "serverCode";
    public const string serverName = "serverName";
    public const string serverStatus = "serverStatus";
    public const string ServerSignal = "serverSignal";
    public const string serverIP = "serverIP";
    public const string serverPort = "serverPort";
    public const string serverZone = "serverZone";
    public const string WebGLPort = "web_socket_port";

    public const string GameWebGLPort = "game_server_websocket_port";
    public const string GameMoveWebGLPort = "game_server_move_websocket_port";
    public const string GameChatWebGLPort = "game_server_chat_websocket_port";
    public const string TongKimGameWebGLPort = "tong_kim_game_websocket_port";
    public const string TongKimMoveWebGLPort = "tong_kim_moving_websocket_port";
    public const string HoaSonGameWebGLPort = "hoa_son_game_websocket_port";
    public const string HoaSonMoveWebGLPort = "hoa_son_moving_websocket_port";
    public const string HoaSonChatWebGLPort = "hoa_son_chat_websocket_port";

    public const string accountId = "accountId";
    public const string accessToken = "accessToken";

    public const string groupId = "groupId";
    public const string groupName = "groupName";
    public const string order = "order";
    public const string Id = "id";
    public const string Name = "name";
    public const string IsNew = "isnew";
    public const string IsSelected = "is_selected";

    public const string gameConfigFilePath = "gameConfigFilePath";

    public const string cardType = "cardType";
    public const string code = "code";
    public const string cardSerial = "serial";

    public const string url = "url";

    //ErrorString
    public const string IsUsernameError = "isUsernameError";
    public const string IsPasswordError = "isPasswordError";
    public const string IsPhoneError = "isPhoneError";
    public const string IsConfirmPasswordError = "isConfirmPasswordError";

    public const string IsRateGame = "isRateGame";
    public const string IsGemFirstTime = "isPaymentFirstTime";
    public const string IsOnlineFirstTime = "isOnlineFirstTime";

    public const string message = "message";
    public const string gameOS = "gameOS";

    public const string webServiceUrl = "webServiceUrl";

    public const string usernameDisplay = "usernameDisplay";

    public const string tongKimGameIp = "tongKimGameIp";
    public const string tongKimGamePort = "tongKimGamePort";
    public const string tongKimGameZone = "tongKimGameZone";

    public const string tongKimMovingIp = "tongKimMovingIp";
    public const string tongKimMovingPort = "tongKimMovingPort";
    public const string tongKimMovingZone = "tongKimMovingZone";

    public const string hslkGameIp = "hslkGameIp";
    public const string hslkGamePort = "hslkGamePort";
    public const string hslkGameZone = "hslkGameZone";

    public const string hslkMovingIp = "hslkMovingIp";
    public const string hslkMovingPort = "hslkMovingPort";
    public const string hslkMovingZone = "hslkMovingZone";

    public const string hslkChatIp = "hslkChatIp";
    public const string hslkChatPort = "hslkChatPort";
    public const string hslkChatZone = "hslkChatZone";
    public const string hslkChatRoom = "hslkChatRoom";

    public const string lastTimeLogin = "lastTimeLogin";
    public const string Nick = "nick";
    public const string IsStatic = "is_static";
    public const string ip = "ip";
    public const string zone = "zone";
    public const string port = "port";

    public const string IsShowSignUp = "IsShowSignUp";
    public const string IsShowIap = "IsShowIap";
    public const string IsShowLoginFacebook = "IsShowLoginFacebook";
    public const string IsShowQuickPlay = "IsShowQuickPlay";
    public const string IsShow18Warning = "IsShow18Warning";
    public const string ref_url = "ref_url";
    public const string deep_url = "deep_url";

    public const string language = "language";
    public const string emulator = "emulator";
}
public static class BCTournamentKey
{
    public const string ID = "ID";
    public const string Type = "Type";
    public const string Name = "Name";
    public const string NamePos = "NamePos";
    public const string UserPerMatch = "UserPerMatch";
    public const string pos = "pos";

    public const string FeeValue = "FeeValue";
    public const string FeeItemID = "FeeItemID";
    public const string FeeCurrency = "FeeCurrency";

    public const string FeeCancelPercent = "FeeCancelPercent";
    public const string FeeReturnPercent = "FeeReturnPercent";

    public const string StartTime_hour = "StartTime_hour";
    public const string StartTime_minute = "StartTime_minute";
    public const string EndTime_hour = "EndTime_hour";
    public const string EndTime_minute = "EndTime_minute";

    public const string Prices = "Prices";
    public const string FromRank = "FromRank";
    public const string ToRank = "ToRank";
    public const string Asset2d = "Asset2d";
    public const string Description = "Description";
    public const string PriceSuiteID = "PriceSuiteID";
    public const string PriceValue = "PriceValue";
    public const string PriceCurrency = "PriceCurrency";


}

public static class BCSkillConst
{
    public const int SlowSkill_SkillID = 101;
    public const int IceSkill_SkillID = 102;
    public const int SpeedSkill_SkillID = 103;

    public const int LockFish_SkillID = 104;
    public const int LockFish_SkillPosInSkillGroup = 2;

    public const string Skill_1_name = "Skill_1_name";
    public const string Skill_1_cast = "Skill_1_cast";
    public const string Skill_1_fire = "Skill_1_fire";

    public const string Skill_2_name = "Skill_2_name";
    public const string Skill_2_cast = "Skill_2_cast";
    public const string Skill_2_fire = "Skill_2_fire";
    public const string Skill_2_target = "Skill_2_target";
    public const string Skill_2_projectile = "Skill_2_projectile";

    public const string Skill_3_name = "Skill_3_name";
    public const string Skill_3_cast = "Skill_3_cast";
    public const string Skill_3_fire = "Skill_3_fire";
    public const string Skill_3_target = "Skill_3_target";
    public const string Skill_3_projectile = "Skill_3_projectile";

    public const string Skill_4_name = "Skill_4_name";
    public const string Skill_4_cast = "Skill_4_cast";
    public const string Skill_4_projectile = "Skill_4_projectile";

    public const string Skill_5_name = "Skill_5_name";
    public const string Skill_5_cast = "Skill_5_cast";
    public const string Skill_5_fire = "Skill_5_fire";
    public const string Skill_5_target = "Skill_5_target";
    public const string Skill_5_projectile = "Skill_5_projectile";

    public const string Skill_6_name = "Skill_6_name";
    public const string Skill_6_cast = "Skill_6_cast";
    public const string Skill_6_fire = "Skill_6_fire";
    public const string Skill_6_target = "Skill_6_target";
    public const string Skill_6_projectile = "Skill_6_projectile";

    public const string Skill_7_cast = "Skill_7_cast";
    public const string Skill_7_explosion = "Skill_7_explosion";
    public const string Skill_7_fire = "Skill_7_fire";
    public const string Skill_7_name = "Skill_7_name";

    public const string Skill_8_cast = "Skill_8_cast";
    public const string Skill_8_explosion = "Skill_8_explosion";
    public const string Skill_8_fire = "Skill_8_fire";
    public const string Skill_8_name = "Skill_8_name";

    public const string Skill_9_cast = "Skill_9_cast";
    public const string Skill_9_target = "Skill_9_target";
    public const string Skill_9_explosion = "Skill_9_explosion";
    public const string Skill_9_projectile = "Skill_9_projectile";
    public const string Skill_9_fire = "Skill_9_fire";
    public const string Skill_9_name = "Skill_9_name";

    public const string Skill_10_cast = "Skill_10_cast";
    public const string Skill_10_explosion = "Skill_10_explosion";
    public const string Skill_10_fire = "Skill_10_fire";
    public const string Skill_10_name = "Skill_10_name";

    public const string Skill_10_target = "Skill_10_target";
    public const string Skill_10_projectile = "Skill_10_projectile";

    //Effect Skills
    public const string Skill_Ice_fader = "Skill_Ice_fader";
    public const string Skill_Ice_fish = "Skill_Ice_fish";

    public const string Skill_Slow_fader = "Skill_Slow_fader";
    public const string Skill_Slow_fish = "Skill_Slow_fish";
    public const string Skill_Slow_explosion = "Skill_Slow_explosion";

    public const string Skill_Speed_fader = "Skill_Speed_fader";
    public const string Skill_Speed_cast = "Skill_Speed_cast";

    public const string Skill_Stun_cast = "Skill_Stun_cast";
    public const string Skill_Stun_fader = "Skill_Stun_fader";
    public const string Skill_Stun_fish = "Skill_Stun_fish";

}
public static class BCKeyAnimator
{
    public const string Condition = "Condition";
}
public static class NameScene
{
    public const string BCLoading = "BCLoading";
    public const string LayoutDemo = "LayoutDemo";
    public const string LayoutDemov2 = "LayoutDemoV2";
    public const string Main = "Main";
}

public static class ImageName
{
    public const string item_chest_ = "item_chest_";
}
public enum AnimationType
{
    idle = 0,
    run = 1,
    atk = 2,
    hit = 3,
    die = 4
}





