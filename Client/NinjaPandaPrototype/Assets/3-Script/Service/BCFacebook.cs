﻿#if !BUILD_EGT
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Facebook.Unity;
using System;
using Studio;
using System.IO;
using Studio.BC;

public class BCFacebook : MonoBehaviour {
	private static BCFacebook _api;
	public static BCFacebook Api
	{
		get
		{
			if (_api == null)
			{
				var go = new GameObject("_fb");
				_api = go.AddComponent<BCFacebook>();
				DontDestroyOnLoad(go);
			}
			return _api;
		}
	}

	private Dictionary<string, Sprite> _avatar;
	private Dictionary<string, Sprite> Avatar
	{
		get
		{
			if (_avatar == null)
			{
				_avatar = new Dictionary<string, Sprite>();
			}
			return _avatar;
		}
	}

	public bool IsLoggedIn
	{
		get
		{
			return FB.IsLoggedIn;
		}
	}

	private Action<string, DateTime> OnLoginComplete;

    private Dictionary<string, long> _fileTimeManager = new Dictionary<string, long>();

	public void Init()
	{

	}

	void Awake()
	{
		if (!FB.IsInitialized)
		{
			// Initialize the Facebook SDK
			FB.Init(InitCallback, OnHideUnity);
		}
		else
		{
			// Already initialized, signal an app activation App Event
			FB.ActivateApp();
		}
	}

	private void InitCallback()
	{
		if (FB.IsInitialized)
		{
			// Signal an app activation App Event
			FB.ActivateApp();
			// Continue with Facebook SDK
			// ...
		}
		else
		{
			Debug.Log("Failed to Initialize the Facebook SDK");
		}
	}

	private void OnHideUnity(bool isGameShown)
	{
		if (!isGameShown)
		{
			// Pause the game - we will need to hide
			Time.timeScale = 0;
		}
		else
		{
			// Resume the game - we're getting focus again
			Time.timeScale = 1;
		}
	}

	public void Login(Action<string, DateTime> onCompleteAction, params string[] permission)
	{
#if UNITY_WEBGL
        BCViewHelper.Api.HideLoading();
        UnityNativeCommunicationSharpe.Api.LoginFacebook((accessToken) =>
        {
            var dict = accessToken.JsonToDictionary();
            var token = dict.Get<string>(BCKey.Token);
            var rawExpireTime = dict.Get<long>(BCKey.ExpiredDate);
            DateTime start = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            DateTime expireTime = start.AddSeconds(rawExpireTime).ToLocalTime();
            if (string.IsNullOrEmpty(accessToken))
            {
                BCViewHelper.Api.ShowMessageBox(LanguageManager.api.GetKey(BCLocalize.ID_LOGIN_FACEBOOK_ERROR));
                return;
            }

            BCGameService.Api.LoginFacebookViaWebGL(token, expireTime);
        });
#else
        OnLoginComplete = onCompleteAction;
		var perms = new List<string>();
		foreach (var p in permission)
		{
			perms.Add(p.ToString());
		}
        FB.LogInWithReadPermissions(perms, AuthCallback);
#endif
    }

	private void AuthCallback(ILoginResult result)
	{
		if (FB.IsLoggedIn)
		{
			// AccessToken class will have session details
			var aToken = Facebook.Unity.AccessToken.CurrentAccessToken;
			// Print current access token's User ID
			if (OnLoginComplete != null)
				OnLoginComplete(aToken.TokenString, aToken.ExpirationTime);
		}
		else
		{
			if (OnLoginComplete != null)
				OnLoginComplete("", DateTime.Now);
		}
		OnLoginComplete = null;
	}

	public void CheckValidToken(string token, Action<bool> result)
	{
		if (result == null)
			return;
		StartCoroutine(CheckValidTokenWWW(token, result));
	}


    public void ShareOnFacebook(string title, string desc, string contentLink,Action callBack = null)
    {
#if UNITY_WEBGL
        UnityNativeCommunicationSharpe.Api.ShareFacebook(contentLink);
        callBack?.Invoke();
#else
        FB.ShareLink(contentTitle: title,
            contentDescription: desc,
            contentURL: new Uri(contentLink), callback: (result)=>
            {
                if (string.IsNullOrEmpty(result.Error))
                    BCViewHelper.Api.ShowMessageBox(LanguageManager.api.GetKey( BCLocalize.ID_GUI_FISH_SHARE_SUCCESS));
                else
                    BCViewHelper.Api.ShowMessageBox(LanguageManager.api.GetKey(BCLocalize.ID_GUI_FISH_SHARE_ERROR));
                callBack?.Invoke();
            });
#endif
    }    

    private IEnumerator TakeScreenshot(string textToPost)
    {
        yield return new WaitForEndOfFrame();

        var width = Screen.width;
        var height = Screen.height;
        var tex = new Texture2D(width, height, TextureFormat.RGB24, false);
        // Read screen contents into the texture
        tex.ReadPixels(new Rect(0, 0, width, height), 0, 0);
        tex.Apply();
        byte[] screenshot = tex.EncodeToPNG();
        
        var wwwForm = new WWWForm();
        wwwForm.AddBinaryData("image", screenshot, "InteractiveConsole.png");
        wwwForm.AddField("message", textToPost);
        //FacebookDelegate<IGraphResult> result = new FacebookDelegate<IGraphResult>();
        FB.API("me/photos", Facebook.Unity.HttpMethod.POST, (response)=> {
            if (!string.IsNullOrEmpty( response.Error))
                BCViewHelper.Api.ShowMessageBox(response.RawResult);
            else
            {

            }
        }, wwwForm);
    }

    public void GetAvatar(string fbId, Action<Sprite> result)
	{
		if (string.IsNullOrEmpty(fbId)) {
		    result(null);
            return;
		}

		if (Avatar.ContainsKey(fbId)) {
			result(Avatar[fbId]);
			return;
		}

		var localPath = BundleUtils.GetCachePath() + "/avatar/" + fbId + ".jpg";
	    var fileTime = GetTimeFile(fbId);
	    var currentTime = GetCurrentTime();

        //Cache avatar facebook 1 ngày (currentTime - fileTime <= 1)
        if (File.Exists(localPath) && (currentTime - fileTime <= 1))
		{
			var texture = LoadTextureFromPath(localPath);
			if (texture != null)
			{
				var sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
				result(sprite);
				Avatar.Add(fbId, sprite);
				return;
			}
		}

		var url = "https://graph.facebook.com/" + fbId + "/picture?type=large";
		StartCoroutine(GetAvatarFromUrl(fbId, url, result));
	}

    public Texture2D LoadTextureFromPath(string filePath)
    {
        Texture2D tex = null;
        if (File.Exists(filePath))
        {
            var fileData = File.ReadAllBytes(filePath).DecyptFile();
            tex = new Texture2D(2, 2);
            tex.LoadImage(fileData); //..this will auto-resize the texture dimensions.
        }
        return tex;
    }

    IEnumerator GetAvatarFromUrl(string fbId, string url, Action<Sprite> callBack)
	{
		var www = new WWW(url);
		yield return www;

	    if (www.error != null || www.texture == null)
	    {
            if (callBack != null) callBack(null);
            yield break;
        }

        var sprite = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0.5f, 0.5f));
	    sprite.name = fbId;

        if (callBack != null)
            callBack(sprite);

        if (!Avatar.ContainsKey(fbId))
            Avatar.Add(fbId, sprite);

        var dir = BundleUtils.GetCachePath() + "/avatar/";
        if (!File.Exists(dir))
            Directory.CreateDirectory(dir);

        SaveTimeFile(fbId);

        var localPath = dir + fbId + ".jpg";
        sprite.texture.EncodeToPNG().EncryptFile().WriteFile(localPath, true);
    }

	IEnumerator CheckValidTokenWWW(string token, Action<bool> result)
	{
		string url = "https://graph.facebook.com/app?access_token=" + token;
		WWW www = new WWW(url);
		yield return www;
		var data = www.text;
		var dict = data.JsonToDictionary();
		if (dict.ContainsKey("error"))
			result(false);
		else
			result(true);
	}

    private long GetTimeFile(string fbId)
    {
        if (_fileTimeManager.ContainsKey(fbId)) {
            return _fileTimeManager[fbId];
        }
        return PlayerPrefs.GetString("fb_avatar_" + fbId, "0").ToLong();
    }

    private void SaveTimeFile(string fbId)
    {
        if (_fileTimeManager.ContainsKey(fbId)) {
            return;
        }

        var currentTime = GetCurrentTime();
        _fileTimeManager.Add(fbId, currentTime);

        PlayerPrefs.SetString("fb_avatar_" + fbId, currentTime + "");
        PlayerPrefs.Save();
    }

    private long GetCurrentTime()
    {
        return (long)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalDays;
    }
}
#endif