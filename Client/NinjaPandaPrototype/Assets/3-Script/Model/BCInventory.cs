﻿using Studio;
using Studio.BC;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[Serializable]
public class BCInventory
{
    public int ID;
    public BCEquipment IDItem;
    public int  intLevel;

    public BCInventory(Dictionary<string,object> data,StyleLoadDictionary style)
    {
        switch (style)
        {
            case StyleLoadDictionary.normal: Parse(data); break;
            case StyleLoadDictionary.custom: ParseCustom(data); break;
        }
    }
    public BCInventory()
    {
        ID = 0;
    }
 
    public BCInventory(int idKey,int id)
    {
        ID = idKey;
        IDItem = BCCache.Api.DataConfig.EquipmentConfigData[id];
        intLevel = 1;
    }
    public BCInventory(int idKey,int id, int level)
    {
        ID = idKey;
        IDItem = BCCache.Api.DataConfig.EquipmentConfigData[id];
        intLevel = level;
    }
    public void Parse(Dictionary<string,object> data)
    {
 
        ID = data.Get<int>(BCKey.Id);
        IDItem = BCCache.Api.DataConfig.EquipmentConfigData[data.Get<int>(BCKey.Item_ID)];
        intLevel = data.Get<int>(BCKey.Level);
    }
    public void ParseCustom(Dictionary<string, object> data)
    {

        ID = data.Get<int>(BCKey.id);
        intLevel = data.Get<int>(BCKey.intLevel);
        int idEquimet = data.Get<Dictionary<string, object>>(BCKey.IDItem).Get<int>(BCKey.id);
        IDItem = BCCache.Api.DataConfig.EquipmentConfigData[idEquimet];

    }
    public int GetEffectEquipment()
    {
        int result = 0;
        result = (IDItem.bounusEnhance * (intLevel - 1)) + IDItem.mainPropertype.Value;
        return result;
    }
 
   
}
