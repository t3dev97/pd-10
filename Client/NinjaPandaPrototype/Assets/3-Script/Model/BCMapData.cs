﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Studio;

public class BCMapData 
{
    public int ID;
    public string NameMap;
    public string DecMap;
    public int MapLenght;
    public string nameAsset;
    public List<ItemWheelData> WheelData;
    public BCMapData(Dictionary<string, object> data)
    {
        Parse(data);
    }
    public void Parse(Dictionary<string,object> data)
    {
        ID = data.Get<int>(BCKey.ID);
        NameMap = data.Get<string>(BCKey.localize_name_id);
        DecMap = data.Get<string>(BCKey.localize_dec_id);
        nameAsset = data.Get<string>(BCKey.asset_name);
        MapLenght = data.Get<int>(BCKey.ChapterLength);
        var rawWheelData = data.Get<List<object>>(BCKey.Wheel_Data);
        if (rawWheelData!=null && rawWheelData.Count>0)
        {
            WheelData = new List<ItemWheelData>();
            foreach (Dictionary<string, object> wheelData in rawWheelData)
            {
                //WheelData.Add(new SpinWheelData().Parse(wheelData));
            }
        }
    }
}
