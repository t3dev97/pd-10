﻿using System.Collections;
using System.Collections.Generic;
using Studio.Sound;
using UnityEngine;

namespace Studio.BC
{
    public class BCSoundVolumeController : MonoBehaviour
    {
        public List<AudioSource> AudioSourceList;

        void OnEnable()
        {

            var bcSound = BCSound.Api;
            if (bcSound != null && AudioSourceList != null)
            {
                foreach (var audioSource in AudioSourceList)
                {
                    audioSource.mute = bcSound.IsSoundMute;
                    audioSource.volume = bcSound.GetVolume(TypeSoundX.Sound);
                }
            }
        }
    }
}
