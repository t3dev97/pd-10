﻿using Studio.BC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BCShopItem 
{
    public int ID;
    public string dec;
    public EnumTypeEquipment type;
    public QUALITY quality;
    public string name;
    public string assetName;
}
