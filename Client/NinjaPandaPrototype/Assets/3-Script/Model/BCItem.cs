﻿using Studio;
using Studio.BC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BCItem 
{
    public int ID;
    public string Dec;
    public EnumTypeEquipment Type;
    public QUALITY Quality;
    public string Name;
    public string AssetName;
    public int Gem;
    public int Bounus;
    public int MainProperty;
    public List<int> Sub_Property;
    public BCItem(Dictionary<string,object> data)
    {
        Parse(data);
    }
    public void Parse(Dictionary<string,object> data)
    {
        ID = data.Get<int>(BCKey.ID);
        AssetName = data.Get<string>(BCKey.Asset_Name);
        Type = (EnumTypeEquipment)data.Get<int>(BCKey.Type_Item);
        Quality = (QUALITY)data.Get<int>(BCKey.Grade_Item);
        Name = data.Get<string>(BCKey.Name_Item);
        Gem = data.Get<int>(BCKey.Gem_price);
        Bounus = data.Get<int>(BCKey.Bounus_Enhance);
        MainProperty = data.Get<int>(BCKey.Main_property);
        Sub_Property = data.Get<List<int>>(BCKey.Sub_property);
    }
}
