﻿using Studio;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BCSuiteCommon
{
    public int ID;

    public List<ItemRewards> listRewards;
    public BCSuiteCommon(Dictionary<string,object> data)
    {
        listRewards = new List<ItemRewards>();
        Parse(data);
    }
    public void Parse(Dictionary<string,object> data)
    {
        ID = data.Get<int>(BCKey.SuiteCommonId);
        var listObject = data.Get<List<object>>(BCKey.Items);
        foreach(Dictionary<string,object> tg in listObject)
        {
            ItemRewards item = new ItemRewards(tg);
            listRewards.Add(item);
        }
    }

}
public class ItemRewards
{
    public int ID;
    public int IDSuite;
    public string Key;
    public string asset;
    public int value;
    public ItemRewards(Dictionary<string,object> data)
    {
        Parse(data);
    }
    public void Parse(Dictionary<string,object> data)
    {
        ID = data.Get<int>(BCKey.id);
        IDSuite = data.Get<int>(BCKey.Suite_id);
        Key = data.Get<string>(BCKey.key);
        asset = data.Get<string>(BCKey.asset);
        value = data.Get<int>(BCKey.value);
    }
}

