﻿using UnityEngine;

namespace Studio.BC
{
    public static class BCContanst
    {
        public static int PlatformType
        {
            get
            {

#if UNITY_ANDROID
                return 1;
#elif UNITY_IOS
                return 2;
#elif UNITY_WSA
                return 3;
#elif UNITY_WEBGL
                return 4;
#else
                return 0;
#endif
            }
        }
    }

    public static class ScreenSize
    {
        public const int MinWidth = 0;
        public const int MinHeight = 0;


        public const int MiddleWidth = 640;
        public const int MiddleHeight = 360;


        public const int MaxWidth = 800;
        public const int MaxHeight = 1280;


        public static float ScaleX
        {
            get
            {
                return (float)Screen.width / 1280;
            }
        }

        public static float ScaleY
        {
            get
            {
                return (float)Screen.height / 800;
            }
        }
    }
    public static class GetSizeCamera
    {
        public static Camera Cam = Camera.main;
        public static float MaxHeight = 2f * Cam.orthographicSize;
        public static float MaxWidth = MaxHeight * Cam.aspect;

    }

    public static class SceneName
    {
        public const string Loading = "BCLoading";
        public const string Main = "BCMain";
        public const string Login = "BCLogin";
        public const string BC2D = "BC2D";
        public const string PopStarMain = "main";
    }

    public static class UISceneName
    {
        public const string Shop = "Shop";
        public const string Equipment = "Equipment";
        public const string World = "World";
        public const string Talents = "Talents";
        public const string Setting = "Setting";
    }

    public static class UIPrefabName
    {
        public const string ShopCoinButton = "ShopCoinButton";
        public const string AchivementSlot = "AchivementSlot";
        public const string Rewart = "Rewart";
        public const string ShopGemButton = "ShopGemButton";

        public const string PopupBuyEnergy = "PopupBuyEnergy";
        public const string ShopChestButton = "ShopChestButton";
        public const string TalentItemButton = "TalentItemButton";
        public const string PopupTalent = "PopupTalent";
        public const string Item = "Item";
        public const string WorldMapButton = "WorldMapButton";
        public const string ChoseMap = "ChoseMap";
        public const string Achivement = "Achivement";
        public const string Material = "Material";
        public const string EquipmentInfo = "EquipmentInfo";
    }
    public static class StateName
    {
        //MAIN
        public const string Battle = "Battle";
        public const string Fuse = "Fuse";
        public const string PopupBuyEnergy = "PopupBuyEnergy";
        public const string OpenChest = "OpenChest";
        public const string OpenChestFX = "OpenChestFX";
        public const string OpenFuseFX = "OpenFuseFX";
        public const string OpenUpgradeFX = "OpenUpgradeFX";
        public const string BattleTest = "BattleTest";
        public const string BattleShopDemo = "BattleShopDemo";
        //public const string BattleShopDemo = "BattleDemoTest";

        public const string ShopCoinButton = "ShopCoinButton";
        public const string ShopGemButton = "ShopGemButton";

        public const string ShopChestButton = "ShopChestButton";
        public const string TalentItemButton = "TalentItemButton";
        public const string PopupTalent = "PopupTalent";
        public const string Item = "Item";
        public const string Material = "Material";
        public const string EquipmentInfo = "EquipmentInfo";
        public const string Iventory = "Iventory";
        public const string Achivement = "Achivement";
        public const string ChoseMap = "ChoseMap";
        public const string Lobby = "Lobby";
        public const string Lobby2 = "Lobby2";

        //POPUP
        public const string MiniGameView = "MiniGameView";
        public const string FortuneWheel = "FortuneWheel";
        public const string FortuneWheelEvent = "FortuneWheelEvent";
        public const string TaiXiu = "TaiXiu";
        public const string Setting = "SettingView";
        public const string FishRate = "FishRate";
        public const string UserInfoFullView = "UserInfoFullView";
        public const string GetGoogleIAP = "GetGoogleIAP";
        public const string SupportView = "SupportView";
        public const string ChangeUserNameView = "ChangeUserNameView";
        public const string ChangeGunView = "ChangeGunView";
        public const string ChangeSkinItemInfoView = "ChangeSkinItemInfoView";
        public const string OtherUserInfo = "OtherUserInfo";
        public const string TournamentSumaryView = "TournamentSumaryView";
        public const string UserQuestView = "UserQuestView";
        public const string ChangeSkinItemView = "ChangeSkinItemView";
        public const string EndChallengeView = "EndChallengeView";
        public const string ChallengeQuickBuyView = "ChallengeQuickBuyView";
        public const string ChangeWingView = "ChangeWingView";
        public const string BuySkinItemConfirm = "BuySkinItemConfirm";
        public const string TournamentView = "TournamentView";
        public const string TournamentFloat = "TournamentFloat";
        public const string NonBattleView = "NonBattleView";
        public const string ChangeLanguage = "ChangeLanguage";

        public const string MessageBox = "MessageBox";
        public const string MessageBoxStatic = "MessageBoxStatic";

        public const string ConfirmBox = "ConfirmBox";
        public const string ConfirmBoxStatic = "ConfirmBoxStatic";

        public const string Loading = "Loading";

        public const string Events = "Events";

        public const string GemView = "GemView";

        public const string ScreenCaptureView = "ScreenCaptureView";

        public const string BroadCast = "BroadCast";
        #region project 68
        public const string Login = "Login";
        public const string Central = "Central";
        public const string InGameLoading = "InGameLoading";
        public const string MailView = "MailView";
        public const string InventoryView = "InventoryView";
        public const string ChangePasswordView = "ChangePasswordView";
        public const string ChangeNickView = "ChangeNickView";
        public const string InventoryUseItemView = "InventoryUseItemView";
        public const string ChangeAvatarView = "ChangeAvatarView";
        public const string ShopView = "ShopView";
        public const string BuyItemView = "BuyItemView";
        public const string LeaderboardView = "LeaderboardView";
        public const string OnePiece = "OnePiece";
        public const string VipInfoView = "VipInfoView";
        public const string EventView = "EventView";
        public const string EventRewardView = "EventRewardView";
        public const string FishWikiView = "FishWikiView";
        public const string FishWikiView_new = "FishWikiView_new";
        public const string ChallengeView = "ChallengeView";
        public const string ChallengeDetailView = "ChallengeDetailView";
        public const string EmoticonView = "EmoticonView";
        public const string ChallengeNotifyView = "ChallengeNotifyView";
        public const string FriendView = "FriendView";
        public const string PrivateChatView = "PrivateChatView";
        public const string LevelUpView = "LevelUpView";
        #endregion

        public const string EquipmentExpNotify = "EquipmentExpNotify";

        public const string FishPointLeatherBoardView = "FishPointLeatherBoardView";
        public const string FishPointRewardBoardView = "FishPointRewardBoardView";

        public const string ItemDetail = "ItemDetail";
        public const string GameCondition = "GameCondition";

        public const string CardPuzzleView = "CardPuzzleView";
        public const string OneTwoThreeView = "OneTwoThreeView";
        public const string DailyWheelView = "DailyWheelView";
        public const string OnepieceHelp = "OnepieceHelp";
    }

    public static class TagName
    {
        public const string Fish = "Fish";
        public const string Bullet = "Bullet";
        public const string RewardSpin = "RewardSpin";
        public const string Bomb = "Bomb";
    }

    public static class LayerName
    {
        public const string Fish = "Fish";
        public const string FishLobby = "FishLobby";
        public const string UI = "UI";
    }

    public class ColorType
    {
        public const string BUTTON_DISABLE = "#898989FF";
        public const string BUTTON_ENABLE = "#FFFFFFFF";

        public const string FISH_HIT = "#FF4747FF"; //#FF9C9CFF";
        public const string FISH_NORMAL = "#FFFFFFFF";

    }

    public class EquimentProperty
    {
        public const string GUN_SPEED = "gun_speed";
    }

    public static class GameKind
    {
        public const int BATTLE_NORMAL = 11;
        public const int BATTLE_VIP = 13;
        public const int WHEEL = 1001;
        public const int TAI_XIU = 1002;
        public const int EVENT_WHEEL = 2000;
    }

    public class PlayerPrefsKey
    {
        public const string CurrentChapter = "currentChapter";
        public const string BestChapter = "BestChapter";
        public const string CurrentStage = "CurrentStage";
        public const string BestStage = "BestStage";
        public const string IsLoading = "IsLoading";
        public const string Gem = "Gem";
        public const string Energy = "Energy";
        public const string Level = "Level";
        public const string Gold = "Gold";
        public const string UpdateTalents = "UpdateTalents";
        public const string MapUnlock = "MapUnlock";
        public const string ChapUnLock = "ChapUnLock";
        public const string AchivementUnlock = "AchivementUnlock";
        public const string AchivementReceive = "AchivementReceive";
        public const string Exp = "Exp";
        public const string RewardExpInLife = "RewardExpInLife";
        public const string ListInventory = "ListInventory";
        public const string ListInventoryUser = "ListInventoryUser";
        public const string ListMaterial = "ListMaterial";
        public const string ListTalent = "ListTalent";
        //public const string Level = "Level";
    }
    public class PopupName
    {
        public const string PopupThienThan = "PopupThienThan";
        public const string PopupAcQuy = "PopupAcQuy";
        public const string PopupVongQuay = "PopupVongQuay";
        public const string PopupVongQuayBoss = "PopupVongQuayBoss";
        public const string PopupVongQuayAds = "PopupVongQuayAds";
        public const string PopupThuongNhan = "PopupThuongNhan";
        public const string PopupLevelUp = "PopupLevelUp";
        public const string PopupHoiSinh = "PopupHoiSinh";
        public const string PopupPause = "PopupPause";
        public const string PopupHigherLevel = "PopupHigherLevel";
        public const string PopupStageRearch = "PopupStageRearch";
    }

    public enum POPUP_TYPE
    {
        PopupThienThan,
        PopupAcQuy,
        PopupVongQuay,
        PopupThuongNhan,
        PopupLevelUp,
        PopupHoiSinh,
        PopupPause,
        PopupHigherLevel,
        PopupStageRearch
    }

    public enum SKILL_NAME
    {
        Speed = 1, // tắng % di chuyển
        Branching = 2, // 3 tia chéo
        BouncyArrow = 3,// đạn dội tường
        Heal = 4, // hồi phục lượng % máu
        Attack = 5, // tăng % damage
        FrontArrow = 6, // thêm 2 tia phía trước
        Multishot = 7, // 2 phát liên tục
        SideArrow = 8, // 3 tia 90 độ
        RearArrow = 9 // thêm 1 tia trước, 1 tia sau
    }
    public class EffectName
    {
        public const string EffectNum = "EffectNum";

    }
    public enum BetType : int
    {
        Tai = 1,
        Xiu = 0,
    }

    public enum RESOURCETYPE : int
    {
        coin = 1,
        poinskill = 2,
        pointexp = 3,
        energy = 4,
        gem = 5
    }


    public enum QUALITY : int
    {
        common = 0,
        great = 1,
        rare = 2,
        epic = 3
    }


    public class GlobalConfigFileName
    {
        public const string consumable_item = "consumable_item";
        public const string suite_common = "suite_common";
        public const string suite_drop_rate = "suite_drop_rate";
        public const string quest_life = "quest_life";
        public const string achivement = "achivement";
        public const string avatar_info = "avatar_info";
        public const string cash_shop = "cash_shop";
        public const string avatar_boder_info = "avatar_boder_info";
        public const string fish_wiki = "fish_wiki";
        public const string level_vip_info = "level_vip_info";
        public const string user_game_function_info = "user_game_function_info";
        public const string gun_config = "gun_config";
        public const string skill_config = "skill_config";
        public const string chat_config = "chat_config";
        public const string challenge_config = "challenge_config";
        public const string tournament_config = "tournament_config";
        public const string pk_config = "pk_config";
        public const string card_puzzle_config = "card_puzzle_config";
        public const string daily_wheel_config = "daily_wheel_config";
        public const string mgluckywheel = "mgluckywheel";
        public const string mgslot777 = "mgslot777";
        public const string mgtreasurewheel = "mgtreasurewheel";
        public const string mggamblewheel = "mggamblewheel";
        public const string localizevn = "localize.vn";
        public const string localizeen = "localize.en";
        public const string one_piece_node_info = "one_piece_node_info";

    }

    public class PrefabName
    {
        public const string fx_selected = "fx_selected";
        public const string popupLight = "__popupLight";
        public const string notify = "BigNotification";
        public const string limitFunction = "__limitFunctionLocked";
        public const string fx_gift_new = "gift_new";
        public const string fx_gift_open = "gift_open";
        public const string Crown = "Crown";
        public const string ItemBubble = "ItemBubble";
        public const string FishItemDelivery = "FishItemDelivery";
        public const string s_luckywheel = "s_luckywheel";
        public const string LuckyFly = "LuckyFly";
        public const string flyText = "__flyText";
        public const string BigNotification = "BigNotification";
        public const string NormalNotification = "NormalNotification";
        public const string PoppingText = "PoppingText";
        public const string change_bullet = "change_bullet";

        public const string FishPointRewardBoardView = "FishPointRewardBoardView";
        public const string FishPointLeatherBoardView = "FishPointLeatherBoardView";

        public const string PKFindingRoomView = "PKFindingRoomView";
        public const string PKWaitingRoomView = "PKWaitingRoomView";
        public const string PKHelpView = "PKHelpView";
        public const string PKEndView = "PKEndView";
        public const string PKConfirm = "PKConfirm";
        public const string fx_pkButton = "fx_pkButton";
        public const string fx_PkCounter = "fx_PkCounter";
        public const string fx_login = "fx_login";
        public const string fx_levelup = "fx_levelup";
        public const string GunHint = "GunHint";
        public const string GiftCodeView = "GiftCodeView";
        public const string SuiteRewardItemView = "SuiteRewardItemView";
        public const string PopupReward = "PopupReward";
        public const string LoadingImage = "LoadingImage";

        public const string UnlockNodeOnePiece = "UnlockNodeOnePiece";
        public const string OpenNodeOnePiece = "OpenNodeOnePiece";
        public const string FusionOnepieceItem = "FusionOnepieceItem";
    }

    public class SpriteName
    {
        public const string event_4a = "event_4a";
        public const string event_4b = "event_4b";
        public const string event_5 = "event_5";
        public const string event_5b = "event_5b";
        public const string event_3 = "event_3";
        public const string event_3b = "event_3b";
        public const string event_3c = "event_3c";
        public const string chattab_normal = "friend_8";
        public const string chattab_selected = "friend_9";
    }

    public class BCAnimationFieldKey
    {
        public const string delayBegin = "delayBegin";
        public const string offsetBegin = "offsetBegin";
        public const string delay = "delay";
    }


}
