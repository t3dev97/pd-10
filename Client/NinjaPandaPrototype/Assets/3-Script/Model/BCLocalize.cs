﻿namespace Studio.BC
{
    public static class BCLocalize
    {
        #region Battle
        public const string ID_GUI_BATTLE_NOT_ENOUGH_MONEY = "ID_GUI_BATTLE_NOT_ENOUGH_MONEY";
        public const string ID_GUI_SYSTEM_IDLE_TIMEOUT = "ID_GUI_SYSTEM_IDLE_TIMEOUT";
        public const string ID_GUI_BATTLE_NO_MONEY_NOTICE = "ID_GUI_BATTLE_NO_MONEY_NOTICE";
        public const string ID_GUI_BATTLE_SET_LOWEST_GUN = "ID_GUI_BATTLE_SET_LOWEST_GUN";
        public const string ID_GUI_QUIT_CONFIRM = "ID_GUI_QUIT_CONFIRM";
        public const string ID_GUI_CATCH = "ID_GUI_CATCH";
        public const string ID_GUI_LUCKY_MATCH_SPIN = "ID_GUI_LUCKY_MATCH_SPIN";// QUAY THƯỞNG
        public const string ID_GUI_USER_QUEST_TITLE_PREFIX = "ID_GUI_USER_QUEST_TITLE_";

        public const string ID_GUI_CONFIRM = "ID_GUI_CONFIRM";// XÁC NHẬN
        public const string ID_GUI_LUCKY_MATCH_REQUIRE_FISH = "ID_GUI_LUCKY_MATCH_REQUIRE_FISH"; //"Yêu cầu bắt <color=red>{N}</color> "
        public const string ID_GUI_LUCKY_MATCH_COMPLETE_FISH = "ID_GUI_LUCKY_MATCH_COMPLETE_FISH"; //"Đã bắt đủ <color=red>{N}/{N}</color> "
        public const string ID_GUI_GUNGROUP_UNLOCK_GROUP = "ID_GUI_GUNGROUP_UNLOCK_GROUP";
        public const string ID_GUI_GUNGROUP_UNLOCK_GROUP_REQUIRE = "ID_GUI_GUNGROUP_UNLOCK_GROUP_REQUIRE";
        public const string ID_GUI_GUNGROUP_VIP_REQUIRE = "ID_GUI_GUNGROUP_VIP_REQUIRE";
        public const string ID_GUI_GUNGROUP_KIND = "ID_GUI_GUNGROUP_KIND";
        public const string ID_ITEM_GUN_GROUP_DESCRIPTION_PREFIX = "ID_ITEM_GUN_GROUP_DESCRIPTION_";
        #endregion
        #region LOGIN
        public const string ID_GUI_LOGIN_BUTTON = "ID_GUI_LOGIN_BUTTON"; //ĐĂNG NHẬP
        public const string ID_GUI_REGISTER_BUTTON = "ID_GUI_REGISTER_BUTTON"; // ĐĂNG KÝ
        public const string ID_GUI_LOGIN_USERNAME_HOLDER = "ID_GUI_LOGIN_USERNAME_HOLDER"; // Tên tài khoản
        public const string ID_GUI_LOGIN_PASS_HOLDER = "ID_GUI_LOGIN_PASS_HOLDER"; // Mật khẩu
        public const string ID_GUI_LOGIN_REPASS_HOLDER = "ID_GUI_LOGIN_REPASS_HOLDER"; // Nhập lại mật khẩu
        public const string ID_GUI_LOGIN_DISPLAYNAME = "ID_GUI_LOGIN_DISPLAYNAME"; // Tên hiển thị
        public const string ID_GUI_LOGIN_FORGOT_PASS = "ID_GUI_LOGIN_FORGOT_PASS"; // Quên mật khẩu

        public const string ID_GUI_LOGIN_REPASSWORD_INCORRECT = "ID_GUI_LOGIN_REPASSWORD_INCORRECT"; //Xác nhận mật khẩu không hợp lệ.
        public const string ID_GUI_LOGIN_REQUIRE_FILL_DATA = "ID_GUI_LOGIN_REQUIRE_FILL_DATA"; // Vui lòng nhập đầy đủ thông tin.
        public const string ID_GUI_LOGIN_FORGOT_PASS_NOTIFY = "ID_GUI_LOGIN_FORGOT_PASS_NOTIFY"; // Vui lòng liên hệ CSKH số ĐT: 01202.111.554 để được hỗ trợ.
        public const string ID_SERVICE_ERR_INVALID_REQUEST_DATA = "ID_SERVICE_ERR_INVALID_REQUEST_DATA";
        public const string ID_SERVICE_ERR_INVALID_NEW_PASSWORD = "ID_SERVICE_ERR_INVALID_NEW_PASSWORD";
        public const string ID_GUI_REGISTER_ERR_MISSING_DATA = "ID_GUI_REGISTER_ERR_MISSING_DATA";
        #endregion
        #region Project68
        public const string ID_GUI_LOGIN = "ID_GUI_LOGIN";//Đăng nhập
        public const string ID_GUI_REGISTER = "ID_GUI_REGISTER";//Đăng ký
        public const string ID_GUI_TAP_TO_START = "ID_GUI_TAP_TO_START";
        public const string ID_GUI_LOGIN_USERNAME = "ID_GUI_LOGIN_USERNAME";
        public const string ID_GUI_ONE_PIECE_CENTERSCEN_NOTE_BG_TILE = "ID_GUI_ONE_PIECE_CENTERSCEN_NOTE_BG_TILE_";
        public const string ID_GUI_LOGIN_PASS = "ID_GUI_LOGIN_PASS"; // Mật khẩu
        public const string ID_GUI_LOGIN_REPASS = "ID_GUI_LOGIN_REPASS"; // Nhập lại mật khẩu
        public const string ID_GUI_LOGIN_PHONE = "ID_GUI_LOGIN_PHONE"; //Số điện thoại
        public const string ID_GUI_LOGIN_NICK = "ID_GUI_LOGIN_NICK"; //Tên hiển thị
        public const string ID_GUI_LOGIN_EMAIL = "ID_GUI_LOGIN_EMAIL";
        public const string ID_GUI_LOGIN_CAPCHA = "ID_GUI_LOGIN_CAPCHA";
        public const string ID_GUI_LOGIN_FORGOT_PASS_UPPER = "ID_GUI_LOGIN_FORGOT_PASS_UPPER";

        public const string ID_GUI_LOGIN_USERNAME_PLACEHOLDER = "ID_GUI_LOGIN_USERNAME_PLACEHOLDER"; // Tên tài khoản
        public const string ID_GUI_LOGIN_PASS_PLACEHOLDER = "ID_GUI_LOGIN_PASS_PLACEHOLDER"; // Mật khẩu
        public const string ID_GUI_LOGIN_REPASS_PLACEHOLDER = "ID_GUI_LOGIN_REPASS_PLACEHOLDER"; // Nhập lại mật khẩu
        public const string ID_GUI_LOGIN_PHONE_PLACEHOLDER = "ID_GUI_LOGIN_PHONE_PLACEHOLDER"; //Số điện thoại
        public const string ID_GUI_LOGIN_EMAIL_PLACEHOLDER = "ID_GUI_LOGIN_EMAIL_PLACEHOLDER";
        public const string ID_GUI_LOGIN_NICK_PLACEHOLDER = "ID_GUI_LOGIN_NICK_PLACEHOLDER";
        public const string ID_GUI_LOGIN_CAPCHA_PLACEHOLDER = "ID_GUI_LOGIN_CAPCHA_PLACEHOLDER";

        public const string ID_GUI_SEND = "ID_GUI_SEND";//Gửi
        public const string ID_GUI_QUICK_PLAY_UPPER = "ID_GUI_QUICK_PLAY_UPPER";//CHƠI NGAY
        public const string ID_GUI_ANNOUNCE_UPPER = "ID_GUI_ANNOUNCE_UPPER";//THÔNG BÁO
        public const string ID_GUI_OK = "ID_GUI_OK"; //Đồng ý
        public const string ID_LOGIN_FACEBOOK_ERROR = "ID_LOGIN_FACEBOOK_ERROR";
        public const string ID_SERVICE_GET_INFO_ERROR = "ID_SERVICE_GET_INFO_ERROR";//Lấy thông tin bị lỗi

        public const string ID_DONE_SEND_FORGOT_PASSWORD = "ID_DONE_SEND_FORGOT_PASSWORD";//Thông báo sau khi gửi yêu cầu quên mật khẩu
        public const string ID_GUI_FEATURE_NAME_PREFIX = "ID_GUI_FEATURE_NAME_";//ID_GUI_FEATURE_NAME_1: Tui do
        public const string ID_GUI_LOW_CONNECTION_INTERNET = "ID_GUI_LOW_CONNECTION_INTERNET";
        public const string ID_GUI_UPDATING_DATA = "ID_GUI_UPDATING_DATA";
        public const string ID_GUI_LEVEL_UNLOCK = "ID_GUI_LEVEL_UNLOCK";
        public const string ID_GUI_DOWNLOADING_DATA = "ID_GUI_DOWNLOADING_DATA";
        public const string ID_GUI_EXCUTING_DATA = "ID_GUI_EXCUTING_DATA";
        public const string ID_GUI_INITING_DATA = "ID_GUI_INITING_DATA";
        public const string ID_GUI_ALL_UPPER = "ID_GUI_ALL_UPPER";//TAT CA
        public const string ID_GUI_CONSUMABLE_UPPER = "ID_GUI_CONSUMABLE_UPPER";//TIEU HAO
        public const string ID_GUI_SKILL_UPPER = "ID_GUI_SKILL_UPPER";//KY NANG
        public const string ID_GUI_SKILL_HINT_PREFIX = "ID_GUI_SKILL_HINT_";
        public const string ID_GUI_QUEST_UPPER = "ID_GUI_QUEST_UPPER";
        public const string ID_GUI_INFO_UPPER = "ID_GUI_INFO_UPPER";
        public const string ID_GUI_ACHIEVEMENT_UPPER = "ID_GUI_ACHIEVEMENT_UPPER";//THANH THU
        public const string ID_GUI_BORDER_UPPER = "ID_GUI_BORDER_UPPER";//KHUNG
        public const string ID_GUI_LEVEL_N = "ID_GUI_LEVEL_N";//Cấp {N}
        public const string ID_GUI_VIP_N = "ID_GUI_VIP_N";//VIP {N}
        public const string ID_GUI_VIP = "ID_GUI_VIP";
        public const string ID_GUI_REQUIRE_VIP = "ID_GUI_REQUIRE_VIP";//Can {N} VND len VIP {N}
        public const string ID_GUI_NOT_ENOUGH_VIP = "ID_GUI_NOT_ENOUGH_VIP";
        public const string ID_GUI_CMND = "ID_GUI_CMND";
        public const string ID_GUI_UPDATE = "ID_GUI_UPDATE"; //Cập nhật
        public const string ID_GUI_NOT_LOG_FB_YET = "ID_GUI_NOT_LOG_FB_YET"; //Chua ket noi tk facebook
        public const string ID_GUI_CHANGE_PASSWORD = "ID_GUI_CHANGE_PASSWORD";//Đổi mật khẩu
        public const string ID_GUI_MAILBOX_UPPER = "ID_GUI_MAILBOX_UPPER";//HOP THU
        public const string ID_GUI_RECEIVE_ALL = "ID_GUI_RECEIVE_ALL";//Nhan het
        public const string ID_GUI_DELETE_ALL = "ID_GUI_DELETE_ALL";//Xoa het
        public const string ID_GUI_RECEIVE = "ID_GUI_RECEIVE";//Nhan
        public const string ID_GUI_HAVENOT_RECEIVE = "ID_GUI_HAVENOT_RECEIVE";//Chưa nhận
        public const string ID_GUI_ALREADY_RECEIVE = "ID_GUI_ALREADY_RECEIVE";// Đã nhận
        public const string ID_GUI_RECEIVE_REWARD = "ID_GUI_RECEIVE_REWARD";//Nhan thuong
        public const string ID_GUI_CLICK_TO_RECEIVE_REWARD = "ID_GUI_CLICK_TO_RECEIVE_REWARD";//Nhan de nhan thuong
        public const string ID_GUI_ATTACH_ITEM = "ID_GUI_ATTACH_ITEM";//Vat pham dinh kem
        public const string ID_MESSAGE_MAINTAIN_PAYMENT = "ID_MESSAGE_MAINTAIN_PAYMENT";// Chức năng đang bảo trì

        public const string ID_GUI_ACCOUNT_CONNECTED_FB = "ID_GUI_ACCOUNT_CONNECTED_FB";//Đã kết nối facebook
        public const string ID_GUI_ID_ACCOUNT = "ID_GUI_ID_ACCOUNT";// ID Tài Khoản
        public const string ID_GUI_CHANGE_PASSWORD_UPPER = "ID_GUI_CHANGE_PASSWORD_UPPER";//DOI MAT KHAU
        public const string ID_MESSAGE_WRONG_PASSWORD = "ID_MESSAGE_WRONG_PASSWORD";
        public const string ID_GUI_OLD_PASSWORD = "ID_GUI_OLD_PASSWORD";//Mat khau cu
        public const string ID_GUI_NEW_PASSWORD = "ID_GUI_NEW_PASSWORD";//Mau khau moi
        public const string ID_MESSAGE_CHANGE_PHONE_CMND_SUCCESS = "ID_MESSAGE_CHANGE_PHONE_CMND_SUCCESS";
        public const string ID_MESSAGE_SYNC_FB_SUCCESS = "ID_MESSAGE_SYNC_FB_SUCCESS";
        public const string ID_GUI_ACHIEVEMENT_LOBBY_UPPER = "ID_GUI_ACHIEVEMENT_LOBBY_UPPER"; //SANH CHIEN TICH
        public const string ID_GUI_CONNECTION_PREFIX = "ID_GUI_CONNECTION_";

        public const string ID_GUI_ITEM_NAME_PREFIX = "ID_GUI_ITEM_NAME_"; //ID_GUI_ITEM_NAME_ + locId = Name
        public const string ID_GUI_ITEM_DESC_PREFIX = "ID_GUI_ITEM_DESC_";
        public const string ID_GUI_ITEM_SL_N = "ID_GUI_ITEM_SL_N";//SL:{N}
        public const string ID_GUI_USE = "ID_GUI_USE";//Dùng
        public const string ID_GUI_LOCK = "ID_GUI_LOCK";//Khoá
        public const string ID_GUI_EMPTY_MAILBOX = "ID_GUI_EMPTY_MAILBOX";//Khong co thu nao
        public const string ID_GUI_FISH_NAME = "ID_GUI_FISH_NAME";
        public const string ID_GUI_REWARD = "ID_GUI_REWARD";//Phan thuong
        public const string ID_GUI_QUEST_DESC_PREFIX = "ID_GUI_QUEST_DESC_";// + group_id/quest_id
        public const string ID_GUI_ACHIEVEMENT_DESC_PREFIX = "ID_GUI_ACHIEVEMENT_DESC_";// + group_id/quest_id
        public const string ID_GUI_CHANGE_NICK_UPPER = "ID_GUI_CHANGE_NICK_UPPER";
        public const string ID_GUI_CHANGE_NICK_DESC_1 = "ID_GUI_CHANGE_NICK_DESC_1";//Xin vui long nhap Ten moi vao o ben duoi
        public const string ID_GUI_CHANGE_NICK_DESC_2 = "ID_GUI_CHANGE_NICK_DESC_2";//"Lưu ý:...";
        public const string ID_GUI_USE_ITEM_UPPER = "ID_GUI_USE_ITEM_UPPER";//dung vat pham
        public const string ID_GUI_PURCHASE_ITEM_SUCCESS = "ID_GUI_PURCHASE_ITEM_SUCCESS";
        public const string ID_GUI_CHANGE_ITEM_SUCCESS = "ID_GUI_CHANGE_ITEM_SUCCESS";

        public const string ID_GUI_CHANGE_AVATAR_UPPER = "ID_GUI_CHANGE_AVATAR_UPPER";
        public const string ID_GUI_GOLD_UPPER = "ID_GUI_GOLD_UPPER";//VANG
        public const string ID_GUI_DIAMOND_UPPER = "ID_GUI_DIAMOND_UPPER";//KIM CUONG
        public const string ID_GUI_TICKET_UPPER = "ID_GUI_TICKET_UPPER";//THE
        public const string ID_GUI_MEDAL_UPPER = "ID_GUI_MEDAL_UPPER";//HUY CHUONG
        public const string ID_GUI_BUY = "ID_GUI_BUY";
        public const string ID_GUI_BUY_ITEM_UPPER = "ID_GUI_BUY_ITEM_UPPER";//MUA VAT PHAM
        public const string ID_GUI_QUANTITY = "ID_GUI_QUANTITY";
        public const string ID_GUI_SUMMARY = "ID_GUI_SUMMARY";
        public const string ID_GUI_CHOOSE_QUANTITY_UPPER = "ID_GUI_CHOOSE_QUANTITY_UPPER";//CHON SO LUONG
        public const string ID_MESSAGE_BUY_ITEM_NOT_ENOUGH_PREFIX = "ID_MESSAGE_BUY_ITEM_NOT_ENOUGH_";

        public const string ID_MESSAGE_CHANGE_PASSWORD_SUCCESS = "ID_MESSAGE_CHANGE_PASSWORD_SUCCESS";
        public const string ID_LOGIN_FACEBOOK = "ID_LOGIN_FACEBOOK";
        public const string ID_LOGIN_FACEBOOK_REQUIRE = "ID_LOGIN_FACEBOOK_REQUIRE";// Vui lòng đăng nhập bằng facebook
        public const string ID_FACEBOOK_SHARE_COUNTDOWN = "ID_FACEBOOK_SHARE_COUNTDOWN";// Share on facebook({N})
        public const string ID_GUI_RANK = "ID_GUI_RANK";//Hang
        public const string ID_GUI_PLAYER = "ID_GUI_PLAYER"; //Nguoi choi
        public const string ID_GUI_LEVEL = "ID_GUI_LEVEL";//Cap
        public const string ID_GUI_GOLD = "ID_GUI_GOLD"; //Vang
        public const string ID_GUI_MEDAL = "ID_GUI_MEDAL";// Huy Chuong
        public const string ID_GUI_ACHIEVEMENT = "ID_GUI_ACHIEVEMENT";//Thanh Tuu
        public const string ID_GUI_GOLD_NOT_ENOUGH = "ID_GUI_GOLD_NOT_ENOUGH";
        public const string ID_GUI_GEM_NOT_ENOUGH = "ID_GUI_GEM_NOT_ENOUGH";

        public const string ID_GUI_VIP_UPPER = "ID_GUI_VIP_UPPER";
        public const string ID_GUI_CHARGE_CARD = "ID_GUI_CHARGE_CARD";//Nap the
        public const string ID_GUI_USAGE = "ID_GUI_USAGE";//Su dung
        public const string ID_MESSAGE_NOT_ENOUGH_ITEM = "ID_MESSAGE_NOT_ENOUGH_ITEM";//Bạn không đủ {N} để thực hiện thao tác này. Bạn có muốn mua không?
        public const string ID_MESSAGE_NOT_ENOUGH_CURRENCY_PREFIX = "ID_MESSAGE_NOT_ENOUGH_CURRENCY_"; //Không đủ currency

        public const string ID_GUI_EVENT_UPPER = "ID_GUI_EVENT_UPPER";//SU KIEN
        public const string ID_GUI_EVENT_DATE_TITLE = "ID_GUI_EVENT_DATE_TITLE";//Su kien dien ra tu ngay ... toi ngay ...
        public const string ID_GUI_DAY_N = "ID_GUI_DAY_N";//Ngay {N}
        public const string ID_GUI_GIFT_UPPER = "ID_GUI_GIFT_UPPER";//QUÀ
        public const string ID_GUI_LEADERBOARD_VALUE_TITLE_PREFIX = "ID_GUI_LEADERBOARD_VALUE_TITLE_";
        public const string ID_GUI_LEVEL_UPPER = "ID_GUI_LEVEL_UPPER"; //CAP

        public const string ID_GUI_LIST_FISH_UPPER = "ID_GUI_LIST_FISH_UPPER"; //DANH SACH CA
        public const string ID_GUI_REGISTER_SUCCESS = "ID_GUI_REGISTER_SUCCESS"; //Dang nhap thanh cong
        public const string ID_GUI_EVENT_NAME_PREFIX = "ID_GUI_EVENT_NAME_";//Ten su kien (+loc_id)
        public const string ID_GUI_EVENT_MILESTONE_PREFIX = "ID_GUI_EVENT_MILESTONE_";// + loc_id => Cap {N} / Ngay {N}
        public const string ID_GUI_FISH_NAME_PREFIX = "ID_GUI_FISH_NAME_";
        public const string ID_GUI_FISH_WIKI_DESC_PREFIX = "ID_GUI_FISH_WIKI_DESC_";
        public const string ID_GUI_FISH_WIKI_MULTIPLE = "ID_GUI_FISH_WIKI_MULTIPLE";// x{N} - x{N}

        public const string ID_GUI_FISH_SHARE_FACEBOOK_DESC = "ID_GUI_FISH_SHARE_FACEBOOK_DESC";//Chúc mừng {N} đã bắn trúng {N} nhận được {N} vàng.        
        public const string ID_GUI_FISH_SHARE_SUCCESS = "ID_GUI_FISH_SHARE_SUCCESS";// Chia sẻ hình ảnh lên facebook thành công.
        public const string ID_GUI_FISH_SHARE_ERROR = "ID_GUI_FISH_SHARE_ERROR";// Chia sẻ hình ảnh lên facebook thất bại.
        public const string ID_ITEM_GUN_NAME_PREFIX = "ID_ITEM_GUN_NAME_";
        public const string ID_ITEM_GUN_SKILL_NAME_PREFIX = "ID_ITEM_GUN_SKILL_NAME_";
        public const string ID_ITEM_GUN_SKILL_DESC = "ID_ITEM_GUN_SKILL_DESC";//Sở hữu kỹ năng {N}
        public const string ID_ITEM_GUN_SKILL_DESC_NONE = "ID_ITEM_GUN_SKILL_DESC_NONE";// Không có kỹ năng đặc biệt
        public const string ID_GUI_BUY_SKILL_REQUIREMENT = "ID_GUI_BUY_SKILL_REQUIREMENT";// Thi Triển kỹ năng cần có {N} thẻ
        public const string ID_GUI_EVENT_DAILY_LOGIN_HELP = "ID_GUI_EVENT_DAILY_LOGIN_HELP";

        public const string ID_GUI_CHALLENGE_UPPER = "ID_GUI_CHALLENGE_UPPER";//THACH DAU
        public const string ID_GUI_BATTLE = "ID_GUI_BATTLE";//Tran dau
        public const string ID_GUI_TIME = "ID_GUI_TIME";//Thoi gian
        public const string ID_GUI_CONDITION = "ID_GUI_CONDITION";//Dieu kien
        public const string ID_GUI_REWARD_UPPER = "ID_GUI_REWARD_UPPER";//PHAN THUONG
        public const string ID_GUI_CONTENT_UPPER = "ID_GUI_CONTENT_UPPER";//NOI DUNG
        public const string ID_GUI_RANKING_UPPER = "ID_GUI_RANKING_UPPER";//XEP HANG
        public const string ID_GUI_THANHTICH = "ID_GUI_THANHTICH";//Thanh tich
        public const string ID_GUI_BATTLE_TYPE = "ID_GUI_BATTLE_TYPE";//Loai tran dau
        public const string ID_GUI_REGISTER_CONDITION = "ID_GUI_REGISTER_CONDITION";//Dieu kien dang ky
        public const string ID_GUI_BATTLE_TIME = "ID_GUI_BATTLE_TIME";//Thoi gian tran dau
        public const string ID_GUI_MAX_JOIN_TIME = "ID_GUI_MAX_JOIN_TIME";//So lan tham gia toi da
        public const string ID_GUI_CHALLENGE_DESC_PREFIX = "ID_GUI_CHALLENGE_DESC_";//Mo ta tran dau + "loc_id";
        public const string ID_GUI_CHALLENGE_LEADERBOARD_NOT_FOUND = "ID_GUI_CHALLENGE_LEADERBOARD_NOT_FOUND";// Hiện không có người chơi tham gia thách đấu này

        public const string ID_GUI_MINIGAME_UPPER = "ID_GUI_MINIGAME_UPPER";
        public const string ID_GUI_MINIGAME_NAME_PREFIX = "ID_GUI_MINIGAME_NAME_";//

        public const string ID_GUI_UNLIMITED = "ID_GUI_UNLIMITED"; //Khong gioi han

        public const string ID_GUI_REGISTRED = "ID_GUI_REGISTRED";//Da dang ky
        public const string ID_GUI_ENTER_CHALLENGE = "ID_GUI_ENTER_CHALLENGE";//Vao
        public const string ID_GUI_JOIN = "ID_GUI_JOIN";//Tham gia
        public const string ID_GUI_CHALLENGE_ACTIVE = "ID_GUI_CHALLENGE_ACTIVE";//Dang dien ra
        public const string ID_GUI_YESTERDAY = "ID_GUI_YESTERDAY";// Hôm qua
        public const string ID_GUI_TODAY = "ID_GUI_TODAY";//Hôm nay
        public const string ID_GUI_TOMORROW = "ID_GUI_TOMORROW";//Ngay mai
        public const string ID_GUI_ENDED = "ID_GUI_ENDED";//Da ket thuc
        public const string ID_MESSAGE_REGISTER_CHALLENGE_SUCCESS = "ID_MESSAGE_REGISTER_CHALLENGE_SUCCESS"; // Đăng ký thành công.
        public const string ID_GUI_AUTO = "ID_GUI_AUTO";//Tu dong
        public const string ID_GUI_STOP = "ID_GUI_STOP";//Dung
        public const string ID_GUI_INSTRUCTION_UPPER = "ID_GUI_INSTRUCTION_UPPER";
        public const string ID_GUI_CHANGE_SKIN_GUN = "ID_GUI_CHANGE_SKIN_GUN";
        public const string ID_GUI_CHANGE_SKIN_WING = "ID_GUI_CHANGE_SKIN_WING";


        public const string ID_MESSAGE_CHALLENGE_NOTIFY_DESC = "ID_MESSAGE_CHALLENGE_NOTIFY_DESC";//Thach dau bat dau, ban co muon vao ngay?
        public const string ID_MESSAGE_CHALLENGE_REGISTER_NOTIFY_DESC = "ID_MESSAGE_CHALLENGE_REGISTER_NOTIFY_DESC";//Hiện tại Thách Đấu đang diễn ra, bạn có muốn đăng ký không?
        public const string ID_MESSAGE_TOURNAMENT_NOTIFY_DESC = "ID_MESSAGE_TOURNAMENT_NOTIFY_DESC";//Trận Giải Đấu đã bắt đầu, bạn có muốn vào không?
        public const string ID_GUI_ENTER_NOW = "ID_GUI_ENTER_NOW";//Vao ngay
        public const string ID_GUI_REGISTER_NOW = "ID_GUI_REGISTER_NOW";// Đăng ký ngay

        //public const string ID_GUI_CHALLENGE_QUICK_BUY = "ID_GUI_CHALLENGE_QUICK_BUY"; //Đã hết đạn!!!\nCòn có thể mua thêm {0:N0} lần đạn!\nCần {N} để mua thêm {N} đạn.

        public const string ID_GUI_CHALLENGE_QUICK_BUY_BUY_GOLD_LEFT = "ID_GUI_CHALLENGE_QUICK_BUY_BUY_GOLD_LEFT"; //Đã hết vàng!!!\nCòn có thể mua thêm {N} lần vàng!
        public const string ID_GUI_CHALLENGE_QUICK_BUY_BUY_AMMO_LEFT = "ID_GUI_CHALLENGE_QUICK_BUY_BUY_AMMO_LEFT"; //Đã hết đạn!!!\nCòn có thể mua thêm {N} lần đạn!
        public const string ID_GUI_CHALLENGE_QUICK_BUY_COST = "ID_GUI_CHALLENGE_QUICK_BUY_COST";//Cần {N}
        public const string ID_GUI_CHALLENGE_QUICK_BUY_GOLD_RECEIVE = "ID_GUI_CHALLENGE_QUICK_BUY_GOLD_RECEIVE";//để mua thêm {N} vàng.
        public const string ID_GUI_CHALLENGE_QUICK_BUY_AMMO_RECEIVE = "ID_GUI_CHALLENGE_QUICK_BUY_AMMO_RECEIVE";//để mua thêm {N} đạn.
        public const string ID_GUI_OUT_OF_QUICK_BUY_NUMBER = "ID_GUI_OUT_OF_QUICK_BUY_NUMBER";//Bạn đã hết số lần mua đạn!!!
        public const string ID_GUI_CHALLENGE_END = "ID_GUI_CHALLENGE_END";
        public const string UI_GUI_CHALLENGE_QUICK_BUY_GOLD_VIP_REQUIRE = "UI_GUI_CHALLENGE_QUICK_BUY_GOLD_VIP_REQUIRE";//Cần đạt VIP {N} để có thể mua thêm vàng.
        public const string UI_GUI_CHALLENGE_QUICK_BUY_AMMO_VIP_REQUIRE = "UI_GUI_CHALLENGE_QUICK_BUY_AMMO_VIP_REQUIRE";//Cần đạt VIP {N} để có thể mua thêm đạn.

        public const string ID_GUI_FRIEND_UPPER = "ID_GUI_FRIEND_UPPER";//BAN BE
        public const string ID_GUI_REQUEST_UPPER = "ID_GUI_REQUEST_UPPER";//YEU CAU
        public const string ID_GUI_FIND_FRIEND_UPPER = "ID_GUI_FIND_FRIEND_UPPER";//TIM BAN
        public const string ID_GUI_SEND_GIFT_ALL = "ID_GUI_SEND_GIFT_ALL";//Tang het
        public const string ID_GUI_NO_WING = "ID_GUI_NO_WING";
        public const string ID_GUI_CHARM = "ID_GUI_CHARM";//Mi luc
        public const string ID_MESSAGE_USE_ITEM_PREFIX = "ID_MESSAGE_USE_ITEM_";// + SUBtYPE = Bạn đã nhận dduwojx {N} vật phẩm subType tương ứng
        public const string ID_MESSAGE_USE_ITEM = "ID_MESSAGE_USE_ITEM";
        public const string ID_GUI_ONLINE = "ID_GUI_ONLINE";
        public const string ID_GUI_OFFLINE = "ID_GUI_OFFLINE";
        public const string ID_GUI_FIND = "ID_GUI_FIND";//Tim
        public const string ID_GUI_RECOMMEND_MAKE_FRIEND = "ID_GUI_RECOMMEND_MAKE_FRIEND";//Goi y ket ban
        public const string ID_GUI_ENTER_ID_OR_NAME = "ID_GUI_ENTER_ID_OR_NAME";//Nhap ID hoac ten
        public const string ID_GUI_ENTER_CHAT_CONENT = "ID_GUI_ENTER_CHAT_CONENT";//Nhap noi dung tro chuyen...
        public const string ID_GUI_PRIVATE_CHAT_UPPER = "ID_GUI_PRIVATE_CHAT_UPPER";//CGAT RIENG
        public const string ID_GUI_ACCEPT_ALL = "ID_GUI_ACCEPT_ALL";//Chap nhan het
        public const string ID_GUI_MAKE_FRIEND = "ID_GUI_MAKE_FRIEND";//Ket ban
        public const string ID_GUI_FRIEND_DENY = "ID_GUI_FRIEND_DENY";//Tu choi ket ban
        public const string ID_GUI_FRIEND_ACCEPT = "ID_GUI_FRIEND_ACCEPT";//Dong y ket ban
        public const string ID_GUI_FRIEND_SENT_REQUEST_DONE = "ID_GUI_FRIEND_SENT_REQUEST_DONE";//Da gui ket ban
        public const string ID_GUI_FRIEND_SEND_GIFT_MESSAGE = "ID_GUI_FRIEND_SEND_GIFT_MESSAGE";//Ban da nhan duoc {N} vat pham {N} tu nguoi choi {N}
        public const string ID_GUI_FRIEND_RECIEVE_GIFT_MESSAGE = "ID_GUI_FRIEND_RECIEVE_GIFT_MESSAGE";//Ban da gui {N} vat pham {N} toi nguoi choi {N}
        public const string ID_GUI_FRIEND_SEND_ALL_GIFT_MESSAGE = "ID_GUI_FRIEND_SEND_ALL_GIFT_MESSAGE";//Ban da gui tat ca qua toi ...
        public const string ID_GUI_FRIEND_RECEIVE_ALL_GIFT_MESSAGE = "ID_GUI_FRIEND_RECEIVE_ALL_GIFT_MESSAGE";//Ban da nhan het qua...
        public const string ID_MESSAGE_MAX_CHAT_TAB = "ID_MESSAGE_MAX_CHAT_TAB";
        public const string ID_GUI_MAX = "ID_GUI_MAX";
        public const string ID_GUI_CSKH = "ID_GUI_CSKH";

        public const string ID_GUI_MINIGAME_NOHU = "ID_GUI_MINIGAME_NOHU";
        public const string ID_GUI_MINIGAME_SLOT777 = "ID_GUI_MINIGAME_SLOT777";
        public const string ID_GUI_MINIGAME_OTHER = "ID_GUI_MINIGAME_OTHER";

        public const string ID_GUI_NONBATTLE_NAME_CHALLENGE = "ID_GUI_NONBATTLE_NAME_CHALLENGE";
        public const string ID_GUI_NONBATTLE_NAME_TOURNAMENT = "ID_GUI_NONBATTLE_NAME_TOURNAMENT";
        public const string ID_GUI_NONBATTLE_NAME_PK = "ID_GUI_NONBATTLE_NAME_PK";
        public const string ID_GUI_NONBATTLE_NAME_PREFIX = "ID_GUI_NONBATTLE_NAME_";

        public const string ID_GUI_BATTLE_OVERTIME_KICK = "ID_GUI_BATTLE_OVERTIME_KICK";// Bạn đã chơi quá 180 phút, vui lòng nghỉ ngơi và quay lại vào ngày mai nhé
        public const string ID_GUI_BATTLE_OVERTIME_WARNING = "ID_GUI_BATTLE_OVERTIME_WARNING";//Thời gian chơi của bạn tối đa là {N} giờ 
        //ads
        public const string ID_GUI_ADS_FINISHED = "ID_GUI_ADS_FINISHED";//"Bạn đã coi xong quảng cáo, phần thưởng sẽ được gửi vào hộp thư."
        public const string ID_GUI_ADS_FAILED = "ID_GUI_ADS_FAILED"; //"Để có thể nhận thưởng, bạn phải coi hết quảng cáo."
        public const string ID_GUI_ADS_NO_CONNECTION = "ID_GUI_ADS_NO_CONNECTION"; //"Không thể coi quảng cáo, xin hãy kiểm tra lại kết nối mạng."

        //Tournament
        public const string ID_GUI_TOURNAMENT_TITLE = "ID_GUI_TOURNAMENT_TITLE";// "Giải Đấu"
        public const string ID_GUI_TOURNAMENT_RANK_1_NAME = "ID_GUI_TOURNAMENT_RANK_1_NAME";// SƠ CẤP
        public const string ID_GUI_TOURNAMENT_RANK_2_NAME = "ID_GUI_TOURNAMENT_RANK_2_NAME";// TRUNG CẤP
        public const string ID_GUI_TOURNAMENT_RANK_3_NAME = "ID_GUI_TOURNAMENT_RANK_3_NAME";// CAO CẤP
        public const string ID_GUI_TOURNAMENT_RANK_4_NAME = "ID_GUI_TOURNAMENT_RANK_4_NAME";// CAO CẤP

        public const string ID_GUI_TOURNAMENT_RANK_1_DESCRIPTION = "ID_GUI_TOURNAMENT_RANK_1_DESCRIPTION";// PHÒNG ĐẤU SƠ CẤP
        public const string ID_GUI_TOURNAMENT_RANK_2_DESCRIPTION = "ID_GUI_TOURNAMENT_RANK_2_DESCRIPTION";// PHÒNG ĐẤU TRUNG CẤP
        public const string ID_GUI_TOURNAMENT_RANK_3_DESCRIPTION = "ID_GUI_TOURNAMENT_RANK_3_DESCRIPTION";// PHÒNG ĐẤU CAO CẤP
        public const string ID_GUI_TOURNAMENT_RANK_4_DESCRIPTION = "ID_GUI_TOURNAMENT_RANK_4_DESCRIPTION";// PHÒNG ĐẤU CAO CẤP

        public const string ID_GUI_TOURNAMENT_PLAYERCOUNT = "ID_GUI_TOURNAMENT_PLAYERCOUNT";// Số người chơi: <color=green>{0}</color><color=#005FF9FF>/{1}</color>
        public const string ID_GUI_TOURNAMENT_PLAYERCOUNT_NONE = "ID_GUI_TOURNAMENT_PLAYERCOUNT_NONE";// Số người chơi yêu cầu: <color=#005FF9FF>{0}</color>
        public const string ID_GUI_TOURNAMENT_FEE_JOIN = "ID_GUI_TOURNAMENT_FEE_JOIN";// Phí tham gia: <color=#005FF9FF>{0}</color>

        public const string ID_GUI_TOURNAMENT_PRIZES = "ID_GUI_TOURNAMENT_PRIZES";// Phần Thưởng
        public const string ID_GUI_TOURNAMENT_PRIZE_1 = "ID_GUI_TOURNAMENT_PRIZE_1";// Hạng 1
        public const string ID_GUI_TOURNAMENT_PRIZE_2 = "ID_GUI_TOURNAMENT_PRIZE_2";// Hạng 2
        public const string ID_GUI_TOURNAMENT_PRIZE_3 = "ID_GUI_TOURNAMENT_PRIZE_3";// Hạng 3

        public const string ID_GUI_TOURNAMENT_REGISTER = "ID_GUI_TOURNAMENT_REGISTER";// Đăng Ký
        public const string ID_GUI_TOURNAMENT_JOIN = "ID_GUI_TOURNAMENT_JOIN";// Tham Gia
        public const string ID_GUI_TOURNAMENT_CANCEL_REGISTER = "ID_GUI_TOURNAMENT_CANCEL_REGISTER";// Hủy Đăng Ký

        public const string ID_GUI_TOURNAMENT_HELP_INFO = "ID_GUI_TOURNAMENT_HELP_INFO";//Help

        public const string ID_GUI_TOURNAMENT_BLOCK = "ID_GUI_TOURNAMENT_BLOCK";// Bạn đã đăng ký Tournament, không thể thực hiện chức năng này.
        public const string ID_GUI_BATTLE_BLOCK_PREFIX = "ID_GUI_BATTLE_BLOCK_";// Bạn đã đăng ký(tham gia)..., không thể thực hiện chức năng này.

        public const string ID_GUI_TOURNAMENT_EVENT_END = "ID_GUI_TOURNAMENT_EVENT_END";// Đã hết thời gian tham gia Tournament, phí đăng ký của bạn sẽ được gửi về hộp thư.

        //IAP
        public const string ID_GUI_IAP_TITLE_TO_UPPER = "ID_GUI_IAP_TITLE_TO_UPPER";//NẠP THẺ
        public const string ID_GUI_IAP_PURCHASE_FAILED = "ID_GUI_IAP_PURCHASE_FAILED";
        public const string ID_GUI_IAP_FAILED = "ID_GUI_IAP_FAILED";// Giao dịch không thành công.
        public const string ID_GUI_IAP_USER_CANCEL = "ID_GUI_IAP_USER_CANCEL";// Người dùng hủy giao dịch.
        public const string ID_GUI_IAP_FAILED_INIT = "ID_GUI_IAP_FAILED_INIT";// Lỗi khởi tạo cửa hàng, vui lòng kiểm tra lại đường truyền hoặc tài khoản Appstore(Google play).

        //Shop demo
        public const string ID_GUI_SHOPDEMO_GUNDESC_EXP = "ID_GUI_SHOPDEMO_GUNDESC_EXP"; // Điểm EXP + <color=#005FF9FF>{N}</color>%.
        public const string ID_GUI_SHOPDEMO_GUNDESC_FISHPOINT = "ID_GUI_SHOPDEMO_GUNDESC_FISHPOINT";// Điểm Bắn cá + <color=#005FF9FF>{N}</color>%.
        public const string ID_GUI_SHOPDEMO_GUNDESC_GUNSPEED = "ID_GUI_SHOPDEMO_GUNDESC_GUNSPEED";// Tốc độ súng + <color=#005FF9FF>{N}</color>%.
        public const string ID_GUI_SHOPDEMO_GUNDESC_LUCK = "ID_GUI_SHOPDEMO_GUNDESC_LUCK";// Điểm may mắn + <color=#005FF9FF>{N}</color>%.
        public const string ID_GUI_SHOPDEMO_GUNDESC_ENERGY = "ID_GUI_SHOPDEMO_GUNDESC_ENERGY";// Điểm năng lượng + <color=#005FF9FF>{N}</color>%.
        public const string ID_GUI_SHOPDEMO_GUNDESC_GOLDPELNALTY = "ID_GUI_SHOPDEMO_GUNDESC_GOLDPELNALTY";// Tăng vàng + <color=#005FF9FF>{N}</color>%.

        //Weekly Event
        public const string ID_GUI_EVENT_WEEKLY_LOGIN_TIME = "ID_GUI_EVENT_WEEKLY_LOGIN_TIME"; //Hoạt động diễn ra từ {N} ngày {N} đến {N} ngày {N}.

        public const string ID_GUI_EVENT_WEEKLY_LOGIN_PREFIX = "ID_GUI_EVENT_WEEKLY_LOGIN_";
        public const string ID_GUI_EVENT_WEEKLY_LOGIN_CATCH_FISH_ALL = "ID_GUI_EVENT_WEEKLY_LOGIN_CATCH_FISH_ALL"; //Bắn {N} cá <color=red>({N}/{N})</color>
        public const string ID_GUI_EVENT_WEEKLY_LOGIN_CATCH_FISH = "ID_GUI_EVENT_WEEKLY_LOGIN_CATCH_FISH";//Bắn {N} {N} <color=red>({N}/{N})</color>
        public const string ID_GUI_EVENT_WEEKLY_LOGIN_ADD_FRIEND = "ID_GUI_EVENT_WEEKLY_LOGIN_ADD_FRIEND";//Bạn bè đạt {N} người <color=red>({N}/{N})</color>

        public const string ID_GUI_EVENT_WEEKLY_LOGIN_USE_ITEM = "ID_GUI_EVENT_WEEKLY_LOGIN_USE_ITEM";//Sử dụng vật phẩm {N} {N} lần <color=red>({N}/{N})</color>
        public const string ID_GUI_EVENT_WEEKLY_LOGIN_USE_SKILL = "ID_GUI_EVENT_WEEKLY_LOGIN_USE_SKILL";//Sử dụng kỹ năng {N} {N} lần <color=red>({N}/{N})</color>
        public const string ID_GUI_EVENT_WEEKLY_LOGIN_USE_USER_RESOURCE = "ID_GUI_EVENT_WEEKLY_LOGIN_USE_USER_RESOURCE";// Sử dụng {N} {N} <color=red>({N}/{N})</color>
        public const string ID_GUI_EVENT_WEEKLY_LOGIN_ACHIEVE_USER_RESOURCE = "ID_GUI_EVENT_WEEKLY_LOGIN_ACHIEVE_USER_RESOURCE";// Đạt được {N} {N} <color=red>({N}/{N})</color>

        public const string ID_GUI_EVENT_WEEKLY_LOGIN_TITLE = "ID_GUI_EVENT_WEEKLY_LOGIN_TITLE";//HOẠT ĐỘNG TUẦN
        public const string ID_GUI_ITEM_RESOURCE_PREFIX = "ID_GUI_ITEM_RESOURCE_";
        public const string ID_GUI_PREFIX = "ID_GUI_";
        public const string ID_GUI_DESC_PREFIX = "ID_GUI_DESC_";
        public const string ID_GUI_EVENT_RECEIVE_SUCCESS = "ID_GUI_EVENT_RECEIVE_SUCCESS";// Nhận thưởng thành công

        // Event Fish Point
        public const string ID_GUI_EVENT_FISH_POINT_LEATHERBOARD_TITLE = "ID_GUI_EVENT_FISH_POINT_LEATHERBOARD_TITLE"; //BẢNG XẾP HẠNG
        public const string ID_GUI_EVENT_FISH_POINT_REWARD_TITLE = "ID_GUI_EVENT_FISH_POINT_REWARD_TITLE"; // THƯỞNG HẠNG
        public const string ID_GUI_EVENT_FISH_POINT_TIME = "ID_GUI_EVENT_FISH_POINT_TIME";// <color=red>{N}</color> ngày <color=red>{N}</color> sẽ xóa điểm và tổng kết trao thưởng
        public const string ID_GUI_EVENT_FISH_POINT_USER_POINT = "ID_GUI_EVENT_FISH_POINT_USER_POINT";// Điểm: <color=green>{N}</color>
        public const string ID_GUI_EVENT_FISH_POINT_USER_RANK = "ID_GUI_EVENT_FISH_POINT_USER_RANK";//Hạng: <color=blue>{N}</color>
        public const string ID_GUI_EVENT_FISH_POINT_USER_REQUIRE_POINT = "ID_GUI_EVENT_FISH_POINT_USER_REQUIRE_POINT"; // {N}/{N} điểm

        public const string ID_GUI_EVENT_FISH_POINT_LEATHERBOARD_SOLORANK = "ID_GUI_EVENT_FISH_POINT_LEATHERBOARD_SOLORANK"; //Hạng {N}
        public const string ID_GUI_EVENT_FISH_POINT_LEATHERBOARD_TORANK = "ID_GUI_EVENT_FISH_POINT_LEATHERBOARD_TORANK"; //{N} - {N}
        public const string ID_GUI_EVENT_FISH_POINT_LEATHERBOARD_END = "ID_GUI_EVENT_FISH_POINT_LEATHERBOARD_END"; //Quà sẽ được gửi về thư khi xếp hạng kết thúc
        public const string ID_GUI_EVENT_FISH_POINT_LEATHERBOARD_TIME = "ID_GUI_EVENT_FISH_POINT_LEATHERBOARD_TIME";//{N} - {N}

        //PK
        public const string ID_GUI_PK_ROOM_READY = "ID_GUI_PK_ROOM_READY";// Sẵn sàng
        public const string ID_GUI_PK_ROOM_NOT_READY = "ID_GUI_PK_ROOM_NOT_READY";// Chưa sẵn sàng
        public const string ID_GUI_PK_ROOM_WAITING = "ID_GUI_PK_ROOM_WAITING";// Đang chờ
        public const string ID_GUI_PK_ROOM_JOIN_COUNT = "ID_GUI_PK_ROOM_JOIN_COUNT";// Số người chơi {N}/{N}
        public const string ID_GUI_PK_ROOM_JOIN_REQUIRE = "ID_GUI_PK_ROOM_JOIN_REQUIRE";//Phí tham gia: <color=blue>{N}</color>

        public const string ID_GUI_PK_ROOM_HELP_TITLE = "ID_GUI_PK_ROOM_HELP_TITLE";// HƯỚNG DẪN
        public const string ID_GUI_PK_ROOM_HELP = "ID_GUI_PK_ROOM_HELP";//"Go(GD) nhập thông tin, thể lệ trong đây"
        public const string ID_GUI_PK_ROOM_DESCRIPTION_REWARD_PREFIX = "ID_GUI_PK_ROOM_DESCRIPTION_REWARD_";//{Nhất}: {N}% tổng tiền {N}
        public const string ID_GUI_PK_ROOM_QUIT = "ID_GUI_PK_ROOM_QUIT";// Thoát
        public const string ID_GUI_PK_ROOM_START = "ID_GUI_PK_ROOM_START";//Bắt đầu sau {N}s
        public const string ID_GUI_PK_OUT_OF_BULLET = "ID_GUI_PK_OUT_OF_BULLET";// Bạn đã hết đạn!

        public const string ID_GUI_PK_WAITING_NOT_ENOUGH_USER = "ID_GUI_PK_WAITING_NOT_ENOUGH_USER";// Không đủ số người chơi.
        public const string ID_GUI_PK_WAITING_FINDING_ROOM = "ID_GUI_PK_WAITING_FINDING_ROOM";// Tìm phòng
        public const string ID_GUI_PK_WAITING_COUNT_DOWN = "ID_GUI_PK_WAITING_COUNT_DOWN";//{N}:{N}

        #endregion
        public const string GUI_NAME_REACH_STAGE_CHAPTER = "GUI_NAME_REACH_STAGE_CHAPTER";
        public const string GUI_NAME_LANGUAGE_VIETNAM = "GUI_NAME_LANGUAGE_VIETNAM";
        public const string GUI_NAME_LANGUAGE_ENGLISH = "GUI_NAME_LANGUAGE_ENGLISH";
        public const string ID_GUI_ITEM_DAY_DURATION = "ID_GUI_ITEM_DAY_DURATION"; // {N} Ngày
        public const string ID_GUI_ITEM_HOUR_DURATION = "ID_GUI_ITEM_HOUR_DURATION"; // {N} Giờ
        public const string ID_GUI_ITEM_MINUTE_DURATION = "ID_GUI_ITEM_MINUTE_DURATION"; // {N} Phút
        public const string ID_GUI_ITEM_SECOND_DURATION = "ID_GUI_ITEM_SECOND_DURATION"; // {N} Giây
        public const string GUI_NAME_CHAPTER = "GUI_NAME_CHAPTER";
        public const string GUI_NAME_ON = "GUI_NAME_ON";
        public const string GUI_NAME_OFF = "GUI_NAME_OFF";
        public const string COLOR_NAME_BLUE = "COLOR_NAME_BLUE";
        public const string GUI_NAME_NOT_ENOUGH_GEM = "GUI_NAME_NOT_ENOUGH_GEM";
        public const string GUI_NAME_NOT_ENOUGH_GOLD = "GUI_NAME_NOT_ENOUGH_GOLD";
        public const string GUI_NAME_NOT_ENOUGH_ENERGY = "GUI_NAME_NOT_ENOUGH_ENERGY";
        public const string GUI_NAME_NOT_ENOUGH = "GUI_NAME_NOT_ENOUGH";


        #region EQUIMENT
        public const string ID_GUI_EQUIMENT_EFFECT_USE_BUTTON = "ID_GUI_EQUIMENT_EFFECT_USE_BUTTON"; // DÙNG
        public const string ID_GUI_EQUIMENT_NOTIFY_TITLE = "ID_GUI_EQUIMENT_NOTIFY_TITLE"; // HẾT HẠN SỬ DỤNG
        public const string ID_GUI_EQUIMENT_NOTIFY_DESC = "ID_GUI_EQUIMENT_NOTIFY_DESC"; // Đã hết hạn sử dụng, Vui lòng MUA NGAY để tiếp tục sử dụng.
        public const string ID_GUI_EQUIMENT_NOTIFY_DESC_NOTBUY = "ID_GUI_EQUIMENT_NOTIFY_DESC_NOTBUY"; // Đã hết hạn sử dụng, Hãy tiếp tục theo dõi sự kiện để nhận quà.

        #endregion

        public const string ID_GUI_JACKPOT_GUIDE_CONTENT = "ID_GUI_JACKPOT_GUIDE_CONTENT"; // Hướng dẫn jackpot bla bla

        public const string ID_SERVICE_CONGRATULATION_RECEIVE_GIFT_CODE_AWARD = "ID_SERVICE_CONGRATULATION_RECEIVE_GIFT_CODE_AWARD";//Nhập Giftcode thành công, phần thưởng sẽ được gửi vào hộp thư.

        #region Condition and Agreement
        public const string ID_GUI_CONDITION_AGREEMENT = "ID_GUI_CONDITION_AGREEMENT";//Tôi đã đọc và chấp nhận Chính Sách & Điều Khoản Sử Dụng\n của Game Bắn Cá Phát Lộc.
        public const string ID_GUI_CONDITION_INFO = "ID_GUI_CONDITION_INFO";//Thông tin Chính Sách & Điều Khoản Sử Dụng        
        #endregion

        #region Language
        public const string ID_GUI_LANGUAGE_CHANGE = "ID_GUI_LANGUAGE_CHANGE";//Chọn Ngôn Ngữ
        public const string ID_GUI_LANGUAGE_CHANGE_TITLE = "ID_GUI_LANGUAGE_CHANGE_TITLE";// CHỌN NGÔN NGỮ
        public const string ID_GUI_LANGUAGE_CHANGE_SUCCESS = "ID_GUI_LANGUAGE_CHANGE_SUCCESS";// Thay đổi ngôn ngữ thành công.\nXin khởi động lại trò chơi để áp dụng thay đổi.
        public const string ID_GUI_LANGUAGE_CHANGE_FAILED = "ID_GUI_LANGUAGE_CHANGE_FAILED";// Thay đổi thất bại, vui lòng thử lại sau.

        public const string ID_GUI_LANGUAGE_CHANGE_VN = "ID_GUI_LANGUAGE_CHANGE_VN";// Tiếng Việt
        public const string ID_GUI_LANGUAGE_CHANGE_EN = "ID_GUI_LANGUAGE_CHANGE_EN";// English
        #endregion

        #region Card Puzzle
        public const string ID_GUI_CARD_PUZZLE_TITLE = "ID_GUI_CARD_PUZZLE_TITLE";// MẢNH GHÉP THẺ CÀO        
        public const string ID_GUI_CARD_PUZZLE_NAME = "ID_GUI_CARD_PUZZLE_NAME";//{N} {N}K
        public const string ID_GUI_CARD_PUZZLE_DESC = "ID_GUI_CARD_PUZZLE_DESC";//Cần {N} mảnh ghép
        public const string ID_GUI_CARD_PUZZLE_USE = "ID_GUI_CARD_PUZZLE_USE";//Ghép
        public const string ID_GUI_CARD_PUZZLE_NOTE_CARD = "ID_GUI_CARD_PUZZLE_NOTE_CARD";//Thẻ cào sau khi ghép sẽ được chuyển vào hộp thư
        public const string ID_GUI_CARD_PUZZLE_NOTE_PHONE = "ID_GUI_CARD_PUZZLE_NOTE_PHONE";//Thông tin hướng dẫn nhận điện thoại sẽ được gửi vào hộp thư
        public const string ID_GUI_CARD_PUZZLE_CONFIRM = "ID_GUI_CARD_PUZZLE_CONFIRM";//Bạn có đồng ý đổi thẻ {N} {N}K từ {N} mảnh ghép không?
        public const string ID_GUI_CARD_PUZZLE_CONFIRM_ITEM = "ID_GUI_CARD_PUZZLE_CONFIRM_ITEM";//Bạn có đồng ý đổi <color=#F25F0CFF>{N}</color> vật phẩm <color=#F25F0CFF>{N}</color> từ <color=#F25F0CFF>{N}</color> mảnh ghép không?

        public const string ID_GUI_CARD_PUZZLE_SUCCESS = "ID_GUI_CARD_PUZZLE_SUCCESS";//Đổi mảnh ghép thành công
        public const string ID_GUI_CARD_PUZZLE_OUTOFDATE = "ID_GUI_CARD_PUZZLE_OUTOFDATE";//Đã hết thời gian đổi mảnh ghép
        public const string ID_GUI_CARD_PUZZLE_FAILED = "ID_GUI_CARD_PUZZLE_FAILED";//Đổi mảnh ghép thất bại

        public const string ID_GUI_CARD_PUZZLE_FAILED_SHOW = "ID_GUI_CARD_PUZZLE_FAILED_SHOW";// Mở bảng đổi mảnh ghép thất bại
        #endregion

        #region
        public const string ID_GUI_ONE_TWO_THREE_TIME_DURATION = "ID_GUI_ONE_TWO_THREE_TIME_DURATION";// Thời gian chơi: {N}s
        public const string ID_GUI_ONE_TWO_THREE_TIMESPLAY = "ID_GUI_ONE_TWO_THREE_TIMESPLAY";// Số lần: {N}/{N}
        public const string ID_GUI_ONE_TWO_THREE_CATCH_FISH_GOLD = "ID_GUI_ONE_TWO_THREE_CATCH_FISH_GOLD";// Tiền thưởng bắn cá
        public const string ID_GUI_ONE_TWO_THREE_REWARD = "ID_GUI_ONE_TWO_THREE_REWARD";// Phần thưởng nhận được khi thắng
        public const string ID_GUI_ONE_TWO_THREE_RECEIVE = "ID_GUI_ONE_TWO_THREE_RECEIVE";// GOM XU
        #endregion
        public const string GUI_NAME_LENGHT_CHAPTER = "GUI_NAME_LENGHT_CHAPTER";//Giá:
        public const string ID_GUI_WEAPON_SHOP_COST = "ID_GUI_WEAPON_SHOP_COST";//Giá:
        public const string ID_GUI_OTHER_USER_INFO = "ID_GUI_OTHER_USER_INFO";//THÔNG TIN
        public const string ID_GUI_OTHER_USER_INFO_GIFT = "ID_GUI_OTHER_USER_INFO_GIFT";// Tặng Vật Phẩm

        public const string ID_GUI_CHALLENGE_LEADERBOARD = "ID_GUI_CHALLENGE_LEADERBOARD";//XẾP HẠNG
        public const string ID_GUI_CHALLENGE_QUICKBUY_GIVEUP = "ID_GUI_CHALLENGE_QUICKBUY_GIVEUP";//Bỏ Cuộc
        public const string ID_GUI_CHALLENGE_QUICKBUY_BUYNOW = "ID_GUI_CHALLENGE_QUICKBUY_BUYNOW";//Mua Ngay
        public const string ID_GUI_BUY_ITEM_VIP_REQUIRE = "ID_GUI_BUY_ITEM_VIP_REQUIRE";// Yêu cầu VIP {N}

        public const string ID_GUI_MULTI_SHOT_VIP_REQUIRE = "ID_GUI_MULTI_SHOT_VIP_REQUIRE";//Yêu cấp VIP {N} để đổi đạn
        public const string ID_GUI_MULTI_SHOT_GET_INFO_ERROR = "ID_GUI_MULTI_SHOT_GET_INFO_ERROR";//Có lỗi xảy ra khi lấy thông tin nhân vật

        //Exchange BCEquipmentInfo
        public const string ID_GUI_EXCHANGE_ITEM_SUCCESS = "ID_GUI_EXCHANGE_ITEM_SUCCESS";//Đổi vật phẩm thành công
        public const string ID_GUI_EXCHANGE_ITEM_FAILED = "ID_GUI_EXCHANGE_ITEM_FAILED";//Đổi vật phẩm thất bại

        #region FishHook
        public const string UI_GUI_QUIT_CONFIRM = "UI_GUI_QUIT_CONFIRM";//Bạn có chắc muốn thoát khỏi game?
        public const string ID_GUI_CANCEL_LOWER = "ID_GUI_CANCEL_LOWER";//Hủy
        public const string ID_GUI_REPLAY = "ID_GUI_REPLAY";//Chơi lại
        public const string ID_GUI_FISHHOOK_END = "ID_GUI_FISHHOOK_END";//Số điểm của bạn là:{N}\nBạn có muốn chơi lại không?
        #endregion
        public const string ID_GUI_hoa_dao_QUANTITY = "ID_GUI_hoa_dao_QUANTITY";//{N}: <color=#067BC4FF></color>

        public const string ID_GUI_POPUP_UNLOCK_AUTO = "ID_GUI_POPUP_UNLOCK_AUTO";// yêu cầu vip 2
        public const string GUI_DEC_TALENT1 = "GUI_DEC_TALENT1";
        public const string GUI_DEC_TALENT2 = "GUI_DEC_TALENT2";

        public const string GUI_NAME_BYSLOT = "GUI_NAME_BYSLOT";
        public const string GUI_NAME_BYRARITY = "GUI_NAME_BYRARITY";

        public const string GUI_DEC_TALENT3 = "GUI_DEC_TALENT3";
        public const string GUI_DEC_TALENT4 = "GUI_DEC_TALENT4";
        public const string GUI_DEC_TALENT5 = "GUI_DEC_TALENT5";
        public const string GUI_DEC_TALENT6 = "GUI_DEC_TALENT6";
        public const string GUI_DEC_TALENT7 = "GUI_DEC_TALENT7";
        public const string GUI_DEC_TALENT8 = "GUI_DEC_TALENT8";
        public const string GUI_DEC_TALENT9 = "GUI_DEC_TALENT9";
        public const string ID_GUI_DUNG_HOP_KetQua = "ID_GUI_DUNG_HOP_KetQua";// yêu cầu vip 2
        public const string ID_GUI_USE_ITEM_LIXI_DESC = "ID_GUI_USE_ITEM_LIXI_DESC";
        public const string ID_GUI_USE_ITEM_CHEST_DESC = "ID_GUI_USE_ITEM_CHEST_DESC";
        public const string ID_GUI_BRN = "ID_GUI_BRN";
        public const string TEXT_NAME_LEVEL = "TEXT_NAME_LEVEL";
        public const string ID_GUI_EGT = "ID_GUI_EGT";
        public const string TEXT_NAME_CHAMHOI = "TEXT_NAME_CHAMHOI";
        public const string TEXT_NAME_LOCKED = "TEXT_NAME_LOCKED";
        public const string TEXT_NAME_LEVEL_MAX = "TEXT_NAME_LEVEL_MAX";
        public const string GUI_NAME_EQUIP = "GUI_NAME_EQUIP";
        public const string GUI_NAME_UNEQUIP = "GUI_NAME_UNEQUIP";
        #region Gun_Properties
        public const string ID_GUI_GUN_PROPERTIES_PREFIX = "ID_GUI_GUN_PROPERTIES_";
        public const string ID_GUI_GUN_PROPERTIES_vip_require = "ID_GUI_GUN_PROPERTIES_vip_require";//<color=#007DBFC8>Vip {N}</color> mới có thể mua và sử dụng
        public const string ID_GUI_CHAT_PLACEHOLDER = "ID_GUI_CHAT_PLACEHOLDER";//Nhấp để trò chuyện...
        public const string ID_GUI_EVENT_TOP_CHARM_END = "ID_GUI_EVENT_TOP_CHARM_END";//Quà sẽ được gửi về thư sau 00:00 mỗi ngày
        #endregion

        #region Panda UI Popup
        public const string POPUP_ACQUY_CONTENT = "POPUP_ACQUY_CONTENT";
        public const string POPUP_THIENTHAN_BTN_USE_ITEM = "POPUP_THIENTHAN_BTN_USE_ITEM";
        public const string POPUP_STAGE_REARCH_BOTTOM_CONTENT = "POPUP_STAGE_REARCH_BOTTOM_CONTENT";
        public const string POPUP_LEVEL_UP_GEM = "POPUP_LEVEL_UP_GEM";
        public const string POPUP_THIEN_THAN_GEM = "POPUP_THIEN_THAN_GEM";
        #endregion
        public const string POPUP_NOT_CONNET_NETWORK = "POPUP_NOT_CONNET_NETWORK";
        public const string POPUP_NOT_GEM = "POPUP_NOT_GEM";
        public const string GUI_NAME_CRITICAL = "GUI_NAME_CRITICAL";
        public const string MAIN_LEVEL_MAX = "MAIN_LEVEL_MAX";
        public const string GUI_NAME_ONEHIT = "GUI_NAME_ONEHIT";
        public const string GUI_NAME_MISS = "GUI_NAME_MISS";
        public const string COLOR_NAME_WHITE = "COLOR_NAME_WHITE";
        public const string UIBASIC_LEVEL_PLAYER = "UIBASIC_LEVEL_PLAYER";
        public const string UIBASIC_LEVEL_STAGE = "UIBASIC_LEVEL_STAGE";
    }
}