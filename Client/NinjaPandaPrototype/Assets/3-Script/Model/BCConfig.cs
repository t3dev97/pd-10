﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Studio.BC
{
    public class BCConfig
    {

        private static BCConfig _api;

        public static BCConfig Api
        {
            get { return _api ?? (_api = new BCConfig()); }
        }

        public void Clear()
        {
            _api = null;
        }
        public bool IsShowChangeLanguage;
        public string M_Language;
        public LanguageData M_LanguageData;
        public bool M_ShowFps = true;
        public bool IsFirstInit;
        public bool IsDebug;
        public bool IsShowAssetBundleDebug;
        public CurrentSceneName CurrentScene;
    }
 
    public class LanguageData
    {
        public Dictionary<string, LanguageChangeData> Data;

        public LanguageData Parse(Dictionary<string, object> dict)
        {
            if (dict.IsDicNullOrEmpty()) return null;

            Data = new Dictionary<string, LanguageChangeData>();

            var languageRawListData = dict.Get<List<object>>(BCKey.data);
            foreach (Dictionary<string, object> languageRawData in languageRawListData)
            {
                var id = languageRawData.Get<string>(BCKey.id);
                var languageData = new LanguageChangeData().Parse(languageRawData);
                if (!Data.ContainsKey(id))
                    Data.Add(id, languageData);
            }
            return this;
        }
    }
}
public enum CurrentSceneName
{
    BCLoading = 0,
    LayoutDemo = 1,
    Main = 2
}