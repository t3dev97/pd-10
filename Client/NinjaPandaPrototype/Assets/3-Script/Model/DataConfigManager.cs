﻿using Studio;
using Studio.BC;
using System.Collections.Generic;

public class DataConfigManager
{
    #region Init Method
    public void LoadData(string configName, string data)
    {
        var dict = data.JsonToDictionary();
        switch (configName)
        {

            case GlobalConfigFileName.localizevn:
                {
                    if (BCConfig.Api.M_Language == "vn")
                        LanguageManager.api.AddLocale(GlobalConfigFileName.localizevn, data, true);
                    break;
                }
            case GlobalConfigFileName.localizeen:
                {
                    if (BCConfig.Api.M_Language == "en")
                        LanguageManager.api.AddLocale(GlobalConfigFileName.localizeen, data, true);
                    break;
                }

        }
    }

    private ShopConfigData _shopConfigData;
    public ShopConfigData ShopConfigData;

    private BCResourceGame __ResourceGame;
    public BCResourceGame ResourceGame;

    private Dictionary<int, TalentConfigData> _talentConfigData;
    public Dictionary<int, TalentConfigData> TalenConfigData
    {
        get
        {
            if (_talentConfigData == null)
                _talentConfigData = new Dictionary<int, TalentConfigData>();
            return _talentConfigData;
        }
    }

    private Dictionary<int, BCUserLevelConfig> _userLevelConfigData;
    public Dictionary<int, BCUserLevelConfig> UserLevelConfigData
    {
        get
        {
            if (_userLevelConfigData == null)
                _userLevelConfigData = new Dictionary<int, BCUserLevelConfig>();
            return _userLevelConfigData;
        }
    }

    private Dictionary<int, BCMaterial> _materialConfigData;
    public Dictionary<int, BCMaterial> MaterialConfigData
    {
        get
        {
            if (_materialConfigData == null)
                _materialConfigData = new Dictionary<int, BCMaterial>();
            return _materialConfigData;
        }
    }

    private Dictionary<int, BCEquipmentGrade> _equipmentGradeConfigData;
    public Dictionary<int, BCEquipmentGrade> EquipmentGradelConfigData
    {
        get
        {
            if (_equipmentGradeConfigData == null)
                _equipmentGradeConfigData = new Dictionary<int, BCEquipmentGrade>();
            return _equipmentGradeConfigData;
        }
    }

    private Dictionary<int, BCEquipmentStyle> _equipmentStyleConfigData;
    public Dictionary<int, BCEquipmentStyle> EquipmentStyleConfigData
    {
        get
        {
            if (_equipmentStyleConfigData == null)
                _equipmentStyleConfigData = new Dictionary<int, BCEquipmentStyle>();
            return _equipmentStyleConfigData;
        }
    }

    private Dictionary<int, BCMapData> _mapConfigData;
    public Dictionary<int, BCMapData> MapConfigData
    {
        get
        {
            if (_mapConfigData == null)
                _mapConfigData = new Dictionary<int, BCMapData>();
            return _mapConfigData;
        }
    }
    private Dictionary<int, BCSuiteCommon> _sutieCommonConfigData;
    public Dictionary<int, BCSuiteCommon> SutieCommonConfigData
    {
        get
        {
            if (_sutieCommonConfigData == null)
                _sutieCommonConfigData = new Dictionary<int, BCSuiteCommon>();
            return _sutieCommonConfigData;
        }
    }
    private Dictionary<int, BCMaterialUpdate> _materialUpdateConfigData;
    public Dictionary<int, BCMaterialUpdate> materialUpdateConfigData
    {
        get
        {
            if (_materialUpdateConfigData == null)
                _materialUpdateConfigData = new Dictionary<int, BCMaterialUpdate>();
            return _materialUpdateConfigData;
        }
    }

    private BCConfigGame _ConfigGameData;
    public BCConfigGame ConfigGameData;


    private Dictionary<int, BCAchivement> _achivementConfigData;
    public Dictionary<int, BCAchivement> AchivementConfigData
    {
        get
        {
            if (_achivementConfigData == null)
                _achivementConfigData = new Dictionary<int, BCAchivement>();
            return _achivementConfigData;
        }
    }
    private Dictionary<int, BCEquipment> _equimentConfigData;
    public Dictionary<int, BCEquipment> EquipmentConfigData
    {
        get
        {
            if (_equimentConfigData == null)
                _equimentConfigData = new Dictionary<int, BCEquipment>();
            return _equimentConfigData;
        }
    }
    private Dictionary<int, BCEffectEquipment> _effectEquimentConfigData;
    public Dictionary<int, BCEffectEquipment> EffectEquipmentConfigData
    {
        get
        {
            if (_effectEquimentConfigData == null)
                _effectEquimentConfigData = new Dictionary<int, BCEffectEquipment>();
            return _effectEquimentConfigData;
        }
    }

    //private Dictionary<int, Skill> _skills;
    //    public Dictionary<int, Skill> Skills
    //    {
    //        get
    //        {
    //            if (_skills == null)
    //                _skills = new Dictionary<int, Skill>();
    //            return _skills;
    //        }
    //    }

    private Dictionary<int, Studio.BC.BCItem> _items;
    public Dictionary<int, Studio.BC.BCItem> Items
    {
        get
        {
            if (_items == null)
                _items = new Dictionary<int, Studio.BC.BCItem>();
            return _items;
        }
    }
    private Dictionary<int, BCSkillBaseData> _skillBases;
    public Dictionary<int, BCSkillBaseData> SkillBases
    {
        get
        {
            if (_skillBases == null)
                _skillBases = new Dictionary<int, BCSkillBaseData>();
            return _skillBases;
        }
    }
    private Dictionary<int, BCMapInfo> _mapInfo;
    public Dictionary<int, BCMapInfo> MapInfo
    {
        get
        {
            if (_mapInfo == null)
                _mapInfo = new Dictionary<int, BCMapInfo>();
            return _mapInfo;
        }
    }
    private Dictionary<int, LevelChapter> _lvChapter;
    public Dictionary<int, LevelChapter> LevelChapters
    {
        get
        {
            if (_lvChapter == null)
                _lvChapter = new Dictionary<int, LevelChapter>();
            return _lvChapter;
        }
    }
    private Dictionary<int, StageInfo> _stageInfos;
    public Dictionary<int, StageInfo> StageInfos
    {
        get
        {
            if (_stageInfos == null)
                _stageInfos = new Dictionary<int, StageInfo>();
            return _stageInfos;
        }
    }

    private Dictionary<int, SuiteDropRateSimple> _suiteDropRateInfo;
    public Dictionary<int, SuiteDropRateSimple> SuiteDropRateInfos
    {
        get
        {
            if (_suiteDropRateInfo == null)
                _suiteDropRateInfo = new Dictionary<int, SuiteDropRateSimple>();
            return _suiteDropRateInfo;
        }
    }
    private Dictionary<int, List<ItemWheel>> _wheelContainers;
    public Dictionary<int, List<ItemWheel>> WheelContainers
    {
        get
        {
            if (_wheelContainers == null)
                _wheelContainers = new Dictionary<int, List<ItemWheel>>();
            return _wheelContainers;
        }
    }

    private Dictionary<int, PopupLevelUpInfo> _popupLevelUp;
    public Dictionary<int, PopupLevelUpInfo> PopupLevelUpConfig
    {
        get
        {
            if (_popupLevelUp == null)
                _popupLevelUp = new Dictionary<int, PopupLevelUpInfo>();
            return _popupLevelUp;
        }
    }

    private Dictionary<int, PopupThienThanInfo> _popupThienThan;
    public Dictionary<int, PopupThienThanInfo> PopupThienThanConfig
    {
        get
        {
            if (_popupThienThan == null)
                _popupThienThan = new Dictionary<int, PopupThienThanInfo>();
            return _popupThienThan;
        }
    }

    private Dictionary<int, PopupDevilInfo> _popupDevil;
    public Dictionary<int, PopupDevilInfo> PopupDevilConfig
    {
        get
        {
            if (_popupDevil == null)
                _popupDevil = new Dictionary<int, PopupDevilInfo>();
            return _popupDevil;
        }
    }



    #endregion

}
