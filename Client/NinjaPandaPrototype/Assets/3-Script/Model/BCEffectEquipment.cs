﻿using Studio;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BCEffectEquipment 
{
    public int ID;
    public string Key;
    public int Value;
    public string Dec_Effect;
    public BCEffectEquipment(Dictionary<string, object> data)
    {
        Parse(data);
    }
    public void Parse(Dictionary<string,object> data)
    {
        ID = data.Get<int>(BCKey.ID);
        Key = data.Get<string>(BCKey.Key);
        Value = data.Get<int>(BCKey.Value);
        Dec_Effect = data.Get<string>(BCKey.Dec_Effect);
    }
}
