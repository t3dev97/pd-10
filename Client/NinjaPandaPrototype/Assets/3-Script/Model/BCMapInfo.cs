﻿using Studio;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BCMapInfo
{
    public int chapter;
    public Dictionary<int, BCStageInfo> listStage;
    public BCMapInfo(Dictionary<string,object> data)
    {

        listStage = new Dictionary<int, BCStageInfo>();
        Parse(data);
    }
    public void Parse(Dictionary<string,object> data)
    {
        chapter = int.Parse( data.Get<string>(BCKey.ChapterMap));
        var objects = data.Get<List<object>>(BCKey.StageMap);
        foreach(Dictionary<string,object> tg in objects)
        {
            BCStageInfo stage = new BCStageInfo(tg);
            listStage.Add(stage.stage,stage);
        }

    }
}
public class BCStageInfo
{
    public int stage;
    public List<int> listIDMap;
    public BCStageInfo(Dictionary<string,object> data)
    {
        listIDMap = new List<int>();
        Parse(data);
    }
    public void Parse(Dictionary<string,object> data)
    {
        stage = data.Get<int>(BCKey.Stage);
        var objects = data.Get<List<object>>(BCKey.ListMapId);
        foreach (var tg in objects)
        {
            Int64 i = (Int64)tg;
            listIDMap.Add((int)i);
        }
    }
}
