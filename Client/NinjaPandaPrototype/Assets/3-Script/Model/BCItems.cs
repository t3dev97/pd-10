﻿using Studio;
using Studio.BC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BCItems : MonoBehaviour
{
    public BCItem item;
    public int amount;
    public BCItems(Dictionary<string,object> data)
    {
        Parse(data);
    }
    public void Parse(Dictionary<string,object> data)
    {
        //item = BCCache.Api.DataConfig.Items[data.Get<int>(BCKey.ItemMaterial)];
        amount = data.Get<int>(BCKey.amount);
    }
  
}
