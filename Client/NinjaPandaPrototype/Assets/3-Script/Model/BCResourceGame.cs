﻿using Newtonsoft.Json;
using Studio;
using Studio.BC;
//using Studio.BC;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class BCResourceGame
{
    private int _gem;
    private int _energy;
    private int _level;
    private int _gold;
    private int _updateTalents;
    private int _mapUnlock;
    private int _chapUnLock;
    private int _achivementUnlock;
    private int _achivementReceive;
    private int _exp;

    public DataTemp dataTemp;
    public Dictionary<int, EquipmentSlot> listInventoryUse;
    public Dictionary<int, BCInventory> listIventory;
    public Dictionary<int, BCMaterialResource> listMaterials;
    public Dictionary<int, BCTalents> listTalents;
    public CharacterAsset asset;

    public Dictionary<int, int> ListEquipmentsInStage = new Dictionary<int, int>();

    public int Gem { get => _gem; set { _gem = value; PlayerPrefs.SetInt(PlayerPrefsKey.Gem, _gem); } }
    public int Energy { get => _energy; set { _energy = value; PlayerPrefs.SetInt(PlayerPrefsKey.Energy, _energy); } }
    public int Level { get => _level; set { _level = value; PlayerPrefs.SetInt(PlayerPrefsKey.Level, _level); } }
    public int Gold { get => _gold; set { _gold = value; PlayerPrefs.SetInt(PlayerPrefsKey.Gold, _gold);  } }
    public int UpdateTalents { get => _updateTalents; set { _updateTalents = value; PlayerPrefs.SetInt(PlayerPrefsKey.UpdateTalents, _updateTalents); } }
    public int MapUnlock { get => _mapUnlock; set { _mapUnlock = value; PlayerPrefs.SetInt(PlayerPrefsKey.MapUnlock, _mapUnlock);  } }
    public int ChapUnLock { get => _chapUnLock; set { _chapUnLock = value; PlayerPrefs.SetInt(PlayerPrefsKey.ChapUnLock, _chapUnLock);  } }
    public int AchivementUnlock { get => _achivementUnlock; set { _achivementUnlock = value; PlayerPrefs.SetInt(PlayerPrefsKey.AchivementUnlock, _achivementUnlock); } }
    public int AchivementReceive { get => _achivementReceive; set { _achivementReceive = value; PlayerPrefs.SetInt(PlayerPrefsKey.AchivementReceive, _achivementReceive); }  }
    public int Exp { get => _exp; set { _exp = value; PlayerPrefs.SetInt(PlayerPrefsKey.Exp, _exp); } }

    public void ResetRewardExpInLife()
    {
        dataTemp.RewardExpInLife = 0;
    }

   
    public BCResourceGame(Dictionary<string, object> data)
    {
        asset = Resources.Load<CharacterAsset>("ModifyTool/Player/PLAYEREQUIPMENT");
        listInventoryUse = new Dictionary<int, EquipmentSlot>();
        listIventory = new Dictionary<int, BCInventory>();
        listMaterials = new Dictionary<int, BCMaterialResource>();
        listTalents = new Dictionary<int, BCTalents>();
        dataTemp = new DataTemp();
        Parse(data);
    }
    public void Parse(Dictionary<string, object> data)
    {
        if (PlayerPrefs.GetInt(PlayerPrefsKey.IsLoading) == 0)
        {
            LoadDataNew(data);
        }
        else
        {
            LoadData();
        }

      
    }

    public void LoadData()
    {
        //DebugX.Log("Load data resource game");

        Gem = PlayerPrefs.GetInt(PlayerPrefsKey.Gem);
        Energy = PlayerPrefs.GetInt(PlayerPrefsKey.Energy);
        Level = PlayerPrefs.GetInt(PlayerPrefsKey.Level);
        Gold = PlayerPrefs.GetInt(PlayerPrefsKey.Gold);

        UpdateTalents = PlayerPrefs.GetInt(PlayerPrefsKey.UpdateTalents);
        MapUnlock = PlayerPrefs.GetInt(PlayerPrefsKey.MapUnlock);
        ChapUnLock = PlayerPrefs.GetInt(PlayerPrefsKey.ChapUnLock);
        AchivementUnlock = PlayerPrefs.GetInt(PlayerPrefsKey.AchivementUnlock);
        AchivementReceive = PlayerPrefs.GetInt(PlayerPrefsKey.AchivementReceive);
        Exp = PlayerPrefs.GetInt(PlayerPrefsKey.Exp);

        Dictionary<string,object> data = PlayerPrefs.GetString(PlayerPrefsKey.ListInventory).JsonToDictionary();
        foreach(var result in data)
        {
            Dictionary<string, object> dataInventory = (result.Value as Dictionary<string, object>);
            BCInventory inventory = new BCInventory(dataInventory,StyleLoadDictionary.custom);

            listIventory.Add(inventory.ID, inventory);
        }

        data = PlayerPrefs.GetString(PlayerPrefsKey.ListInventoryUser).JsonToDictionary();
        foreach (var result in data)
        {
            Dictionary<string, object> dataInventory = (result.Value as Dictionary<string, object>);
            EquipmentSlot inventory = new EquipmentSlot(dataInventory, StyleLoadDictionary.custom);
            if (inventory.idInventory != 0)
                inventory.SetData(listIventory[inventory.idInventory]);
            //listInventoryUse.Add(inventory.ID, inventory);
            //listInventoryUse.Add(inventory.idInventory, inventory);
            listInventoryUse.Add(inventory.ID, inventory);
        }

        data = PlayerPrefs.GetString(PlayerPrefsKey.ListMaterial).JsonToDictionary();
        foreach (var result in data)
        {
            Dictionary<string, object> dataInventory = (result.Value as Dictionary<string, object>);
            BCMaterialResource inventory = new BCMaterialResource(dataInventory, StyleLoadDictionary.custom);

            listMaterials.Add(inventory.material.id, inventory);
        }


        data = PlayerPrefs.GetString(PlayerPrefsKey.ListTalent).JsonToDictionary();
        foreach (var result in data)
        {
            Dictionary<string, object> dataInventory = (result.Value as Dictionary<string, object>);
            BCTalents inventory = new BCTalents(dataInventory, StyleLoadDictionary.custom);

            listTalents.Add(inventory.id, inventory);
        }
    }


    public void LoadDataNew(Dictionary<string, object> data)
    {
        asset.Reset();
        Gem = data.Get<int>(BCKey.Gem_);
        Energy = data.Get<int>(BCKey.Energy);
        Level = data.Get<int>(BCKey.Level);
        Gold = data.Get<int>(BCKey.Gold);

        UpdateTalents = data.Get<int>(BCKey.UpdateTalents);
        MapUnlock = data.Get<int>(BCKey.MapUnlock);
        ChapUnLock = data.Get<int>(BCKey.ChapUnlock);
        AchivementUnlock = data.Get<int>(BCKey.AchivementUnLock);
        AchivementReceive = data.Get<int>(BCKey.AchivementReceive);
        Exp = data.Get<int>(BCKey.ExpLevel);


        var listInventoryTG = data.Get<List<object>>(BCKey.Equipment);
        foreach (Dictionary<string, object> tg in listInventoryTG)
        {
            BCInventory inventory = new BCInventory(tg,StyleLoadDictionary.normal);
            AddIventory(inventory);
        }

        var listInventoryUser = data.Get<List<object>>(BCKey.EquipmentUser);
        foreach (Dictionary<string, object> tg in listInventoryUser)
        {
            EquipmentSlot inventory = new EquipmentSlot(tg, StyleLoadDictionary.normal);
            if (inventory.idInventory != 0)
            {
                inventory.SetData(listIventory[inventory.idInventory]);
           
            }
            else
            {
                inventory.inventory = null;
            }
            AddIventoryUse(inventory);


        }


        var lisItemMaterial = data.Get<List<object>>(BCKey.Material);
        foreach (Dictionary<string, object> tg in lisItemMaterial)
        {
            BCMaterialResource item = new BCMaterialResource(tg,StyleLoadDictionary.normal);
            //listMaterials.Add(item.material.id, item);
            AddMaterial(item);
        }

        var listTalentTG = data.Get<List<object>>(BCKey.Talents);
        foreach (Dictionary<string, object> tg in listTalentTG)
        {
            BCTalents item = new BCTalents(tg,StyleLoadDictionary.normal);
            AddTalent(item);
        }
        PlayerPrefs.SetInt(PlayerPrefsKey.IsLoading, 1);
    }

    public void AddIventory(BCInventory inventory)
    {
        listIventory.Add(inventory.ID, inventory);
        PlayerPrefs.SetString(PlayerPrefsKey.ListInventory, JsonConvert.SerializeObject(listIventory));
    }
    public void AddMaterial(BCMaterialResource material)
    {
        listMaterials.Add(material.material.id, material);
       
        PlayerPrefs.SetString(PlayerPrefsKey.ListMaterial, JsonConvert.SerializeObject(listMaterials));
    }
    public void AddTalent(BCTalents talent)
    {
        listTalents.Add(talent.id, talent);
        PlayerPrefs.SetString(PlayerPrefsKey.ListTalent, JsonConvert.SerializeObject(listTalents));
    }
    public void AddIventoryUse(EquipmentSlot inventory)
    {
        //listInventoryUse.Add(inventory.ID, inventory);
        //listInventoryUse.Add(inventory.idInventory, inventory);
        listInventoryUse.Add(inventory.ID, inventory);
        PlayerPrefs.SetString(PlayerPrefsKey.ListInventoryUser, JsonConvert.SerializeObject(listInventoryUse));
    }

    public void updateInventoryUse()
    {
        PlayerPrefs.SetString(PlayerPrefsKey.ListInventoryUser, JsonConvert.SerializeObject(listInventoryUse));
    }

    public void UpdateDataInvertyory(BCInventory inventory,int vt,bool isEquipment)
    {

        if (isEquipment)
        {
            listInventoryUse[vt].inventory = inventory;
            listInventoryUse[vt].idInventory = inventory.ID;
        }
        else
        {
            listInventoryUse[vt].inventory = null;
            listInventoryUse[vt].idInventory = 0;
        }
      
        //idInventory
        updateInventoryUse();
    }

}
public class EquipmentSlot
{
    public int ID;
    public string Key;
    public int idInventory;
    public BCInventory inventory;
    public EquipmentSlot(Dictionary<string,object> data,StyleLoadDictionary style)
    {
        switch (style)
        {
            case StyleLoadDictionary.normal: Parse(data); break;
            case StyleLoadDictionary.custom: ParseCustom(data); break;
        }
    }

    public void Parse(Dictionary<string,object> data)
    {
        ID = data.Get<int>(BCKey.ID);
        Key = data.Get<string>(BCKey.Key);
        idInventory = data.Get<int>(BCKey.IDEquipmentUse);
     
    }
    public void SetData(BCInventory inventory)
    {
        this.inventory = inventory;
    }
    public void ParseCustom(Dictionary<string, object> data)
    {
        ID = data.Get<int>(BCKey.ID);
        Key = data.Get<string>(BCKey.Key);
        idInventory = data.Get<int>(BCKey.idInventory);
        
    }
}

