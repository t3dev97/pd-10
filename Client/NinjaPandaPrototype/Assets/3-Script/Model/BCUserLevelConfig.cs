﻿using Studio;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BCUserLevelConfig 
{
    public int id;
    public int level;
    public int expRequire;
    public int lvBonusGem;

    public BCUserLevelConfig(Dictionary<string,object> data)
    {
        Parse(data);
    }
    public void Parse(Dictionary<string, object> data)
    {
        id = int.Parse(data.Get<string>(BCKey.id));
        level = int.Parse(data.Get<string>(BCKey.lv));
        expRequire = int.Parse(data.Get<string>(BCKey.exp_require));
        lvBonusGem = int.Parse(data.Get<string>(BCKey.lv_bonusgem));
    }
}
