﻿
using Studio;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BCMaterial
{
    public int id;
    public string nameId;
    public string description;
    public string assetName;
    public int hide;
    public int price_gold;
    public BCMaterial(Dictionary<string,object> data)
    {
        Parse(data);
    }
    public void Parse(Dictionary<string,object> data)
    {
        id = data.Get<int>(BCKey.ItemID);
        nameId = data.Get<string>(BCKey.localize_name_id);
        description = data.Get<string>(BCKey.Description);
        assetName = data.Get<string>(BCKey.Asset_Name);
        hide = data.Get<int>(BCKey.Hide);
        price_gold = int.Parse(data.Get<string>(BCKey.Price_Gold));
    }
}
