﻿using Studio;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BCConfigGame 
{
    public string urlFeedBack;
    public string urlCredits;
    public string urlTerms;
    public string urlPolicy;
    public int EnergyInStage;
    public Dictionary<int,string> listLanguage;
    public Dictionary<int, NPC> listNPC;
    public List<int> tgListSkills;
    public List<int> listSkills;
    public float DelayTransitionPopup;
    public float DelayAtkBeginStage;
    public BCConfigGame(Dictionary<string,object> data)
    {
        listLanguage = new Dictionary<int, string>();
        listNPC = new Dictionary<int, NPC>();
        listSkills = new List<int>();
        tgListSkills = new List<int>();
        Parse(data);
    }
    public void Parse(Dictionary<string, object> data)
    {
        urlFeedBack = data.Get<string>(BCKey.urlFeedBack);
        urlCredits = data.Get<string>(BCKey.urlCredits);
        urlTerms = data.Get<string>(BCKey.urlTerms);
        urlPolicy = data.Get<string>(BCKey.urlPolicy);

        var listLanguageData = data.Get<List<object>>(BCKey.Language);
        if (listLanguageData != null)
        {
            int count = 1;
            foreach(var tg in listLanguageData)
            {
                listLanguage.Add(count,(string)tg);
                count++;
            }
        }
       

        listSkills = new List<int>(tgListSkills);
        EnergyInStage = data.Get<int>(BCKey.EnergyInStage);

        DelayTransitionPopup = float.Parse(data.Get<string>(BCKey.delay_transitions_popup));
        DelayAtkBeginStage = float.Parse(data.Get<string>(BCKey.delay_atk_begin_stage));

        List<object> listNPCData = data.Get<List<object>>(BCKey.PositionNPC);
        foreach(Dictionary<string,object> game in listNPCData)
        {
            NPC npc = new NPC(game);
            listNPC.Add(npc.id, npc);
        }
    }

    public void ResetListSkill()
    {
        listSkills = new List<int>(tgListSkills);
    }

    public void RemoveSkill(int id)
    {
        listSkills.Remove(id);
    }
}
public class NPC
{
    public int id;
    public string name;
    public int posion;

    public NPC(Dictionary<string, object> data)
    {
        Parse(data);
    }

    public void Parse(Dictionary<string, object> data)
    {
        id = data.Get<int>(BCKey.ID);
        name = data.Get<string>(BCKey.Name);
        posion = data.Get<int>(BCKey.Position);
    }
}