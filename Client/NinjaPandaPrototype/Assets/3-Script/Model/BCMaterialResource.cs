﻿using Studio;
using Studio.BC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BCMaterialResource 
{
    public int amount;
    public BCMaterial material;
    public BCMaterialResource(Dictionary<string,object> data,StyleLoadDictionary style)
    {
        switch (style)
        {
            case StyleLoadDictionary.normal: parse(data);break;
            case StyleLoadDictionary.custom:parseCustom(data);break;
        }
    }
    public void parse(Dictionary<string,object> data)
    {
        amount = data.Get<int>(BCKey.value);
        int id = data.Get<int>(BCKey.Item_ID);
        material = BCCache.Api.DataConfig.MaterialConfigData[id];
    }
    public void parseCustom(Dictionary<string, object> data)
    {
        amount = data.Get<int>(BCKey.amount1);
        int id = data.Get< Dictionary<string, object> > (BCKey.material).Get<int>(BCKey.id);
        material = BCCache.Api.DataConfig.MaterialConfigData[id];
    }
}
