﻿using Studio;
using Studio.BC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BCTalents 
{
    public int id;
    public int Level;
    public TalentConfigData talent;

    public BCTalents(Dictionary<string,object> data, StyleLoadDictionary style)
    {
        
        switch (style)
        {
            case StyleLoadDictionary.normal: Parse(data); break;
            case StyleLoadDictionary.custom: ParseCustom(data); break;
        }
        talent = BCCache.Api.DataConfig.TalenConfigData[id];
    }
    public void Parse(Dictionary<string,object> data)
    {
        id = data.Get<int>(BCKey.ID);
        Level = data.Get<int>(BCKey.Level);
    }
    public void ParseCustom(Dictionary<string, object> data)
    {
        id = data.Get<int>(BCKey.id);
        Level = data.Get<int>(BCKey.Level);
    }
}
