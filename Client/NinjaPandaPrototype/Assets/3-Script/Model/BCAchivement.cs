﻿using Studio;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BCAchivement 
{
    public int ID;
    public int Suite_Common_ID;
    public int Chapter;
    public int Stage;
    public BCAchivement(Dictionary<string,object> data)
    {
        Parse(data);
    }
    public void Parse(Dictionary<string,object> data)
    {

        ID = data.Get<int>(BCKey.id);
        Suite_Common_ID = data.Get<int>(BCKey.SuiteCommonId);
        Chapter = data.Get<int>(BCKey.Chapter);
        Stage = data.Get<int>(BCKey.Stage);
    }
}
