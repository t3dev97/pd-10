﻿using Studio;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BCMaterialUpdate 
{
    public int level;
    public int requireMaterial;
    public int requireGold;
    public BCMaterialUpdate(Dictionary<string,object> data)
    {
        Parse(data);
    }
    public void Parse(Dictionary<string,object> data)
    {
        level = data.Get<int>(BCKey.Lv_Equipment);
        requireMaterial = data.Get<int>(BCKey.require_material);
        requireGold = data.Get<int>(BCKey.require_gold);
    }
}
