﻿using Studio;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BCEquipmentStyle 
{
    public int id;
    public string AssetName;
    public string NameStyle;
    public BCEquipmentStyle(Dictionary<string,object> data)
    {
        Parse(data);
    }
    public void Parse(Dictionary<string,object> data)
    {
        id = data.Get<int>(BCKey.id);
        AssetName = data.Get<string>(BCKey.AssetName);
        NameStyle = data.Get<string>(BCKey.NameGrade);
    }
}
