﻿using System.Collections.Generic;
using UnityEngine;

namespace Studio.BC
{
    public class BCModel
    {
        private static BCModel _api;

        public static BCModel Api
        {
            get { return _api ?? (_api = new BCModel()); }
        }

        public string MasterHostIp;
        public int MasterPort;
        public string MasterToken;

        public string GameHostIp;
        public int GamePort;
        public string GameToken;

        public void Logout()
        {
            _api = null;
        }
        //===================== PUBLIC ===============

        public string AvatarId;
        public bool IsFullScreen;

        public float? StartTimeClient;
        public long? TimeServer;

        public float GetLatency(long timeServer)
        {
            if (TimeServer == null || StartTimeClient == null)
            {
                return 0;
            }

            var currentTime = Time.realtimeSinceStartup;
            var distanceTimeServer = (long)((currentTime - StartTimeClient.Value) * 1000);
            var latency = ((TimeServer.Value + distanceTimeServer) - timeServer) / 1000f;

            if (latency < 0) latency = 0;
            return latency;
        }
    }

    public class ShopConfigData
    {
        public Dictionary<int, ChestShopConfigData> listChestData;
        public Dictionary<int, GemShopConfigData> listGemData;
        public Dictionary<int, CoinShopConfigData> listCoinData;

        public ShopConfigData Parse(List<object> data)
        {
            listChestData = new Dictionary<int, ChestShopConfigData>();
            listGemData = new Dictionary<int, GemShopConfigData>();
            listCoinData = new Dictionary<int, CoinShopConfigData>();
            var chestData = (data[0] as Dictionary<string, object>).Get<List<object>>(BCKey.list_chest);
            var coninData = (data[0] as Dictionary<string, object>).Get<List<object>>(BCKey.list_coins);
            var gemData = (data[0] as Dictionary<string, object>).Get<List<object>>(BCKey.list_gems);

            foreach (Dictionary<string, object> obj in chestData)
            {
                var chest = new ChestShopConfigData(obj);
                listChestData.Add(chest.ID, chest);
            }
            foreach (Dictionary<string, object> obj in coninData)
            {
                var coin = new CoinShopConfigData(obj);
                listCoinData.Add(coin.ID, coin);
            }
            foreach (Dictionary<string, object> obj in gemData)
            {
                var gem = new GemShopConfigData(obj);
                listGemData.Add(gem.ID, gem);
            }
            return this;
        }
    }

    public class ChestShopConfigData
    {
        public int ID;
        public string NameID;
        public string DecID;
        public string AssetName;
        public string AssetNameClose;
        public string PriceText;
        public ChestShopConfigData(Dictionary<string, object> data)
        {
            Parse(data);
        }
        public void Parse(Dictionary<string, object> data)
        {
            ID = data.Get<int>(BCKey.ID);
            NameID = data.Get<string>(BCKey.localize_name_id);
            DecID = data.Get<string>(BCKey.localize_dec_id);
            AssetName = data.Get<string>(BCKey.asset_name);
            AssetNameClose = data.Get<string>(BCKey.Asset_Name_Close);
            PriceText = data.Get<string>(BCKey.price_text);
        }
    }
    public class GemShopConfigData
    {
        public int ID;
        public string NameID;
        public string AssetName;
        public string Amount;
        public string PriceText;
        public string KeyIAPAndroid;
        public string KeyIAPIOS;
        public GemShopConfigData(Dictionary<string, object> data)
        {
            Parse(data);
        }
        public void Parse(Dictionary<string, object> data)
        {
            ID = data.Get<int>(BCKey.ID);
            NameID = data.Get<string>(BCKey.localize_name_id);
            AssetName = data.Get<string>(BCKey.asset_name);
            Amount = data.Get<string>(BCKey.amount);
            PriceText = data.Get<string>(BCKey.price_text);
            KeyIAPAndroid = data.Get<string>(BCKey.Key_IAP_Android);
            KeyIAPIOS = data.Get<string>(BCKey.Key_IAP_IOS);
        }
    }
    public class CoinShopConfigData
    {
        public int ID;
        public string NameID;
        public string AssetName;
        public string Amount;
        public string PriceText;
        public CoinShopConfigData(Dictionary<string, object> data)
        {
            Parse(data);
        }
        public void Parse(Dictionary<string, object> data)
        {
            ID = data.Get<int>(BCKey.ID);
            NameID = data.Get<string>(BCKey.localize_name_id);
            AssetName = data.Get<string>(BCKey.asset_name);
            Amount = data.Get<string>(BCKey.amount);
            PriceText = data.Get<string>(BCKey.price_text);
        }
    }

    public class TalentConfigData
    {
        public int ID;
        public string NameID;
        public string AssetName;
        public string DecID;
        public int Bounus;
        public int Default;
        public Vector3 DescPosOffset;
        public Vector3 DescPointPos;

        public TalentConfigData(Dictionary<string, object> data)
        {
            Parse(data);
        }
        public void Parse(Dictionary<string, object> data)
        {
            ID = data.Get<int>(BCKey.ID);
            NameID = data.Get<string>(BCKey.localize_name_id);
            AssetName = data.Get<string>(BCKey.asset_name);
            DecID = data.Get<string>(BCKey.Localize_Dec_id);
            Bounus = data.Get<int>(BCKey.Bounus);
            Default = data.Get<int>(BCKey.Default);
            DescPosOffset = data.Get<string>(BCKey.Desc_Pos_OffSet).ToVector3();
            DescPointPos = data.Get<string>(BCKey.Desc_Point_Pos).ToVector3();
        }
    }

    public class Skill
    {
        public int Id;
        public string Icon;
        public string Name;
        public string Description;
        public int Limit;

        public Skill(Dictionary<string, object> data)
        {
            Id = data.Get<int>(BCKey.ID);
            Icon = data.Get<string>(BCKey.asset_name);
            Name = data.Get<string>(BCKey.localize_name_id);
            Limit = data.Get<int>(BCKey.Limit);
        }
    }

    public class BCItem
    {
        public int Id;
        public string Name;
        public EnumTypeItem Type;
        public string Icon;

        public BCItem(Dictionary<string, object> data)
        {
            Id = data.Get<int>(BCKey.ItemID);
            Name = data.Get<string>(BCKey.localize_name_id);
            Icon = data.Get<string>(BCKey.Asset_Name);
            Type = (EnumTypeItem)data.Get<int>(BCKey.ItemType);
        }

    }
    public class SuiteDropRateSimple
    {
        public Dictionary<int,SuiteDropRateInfo> listSuiteDropRate = new Dictionary<int, SuiteDropRateInfo>();
    }
    public class SuiteDropRateInfo // 1 hang ngang 
    {
        public int Id;
        public int SuiteID;
        public string Key;
        public int Value;
        public int Quantity;
        public int Rate;
        public string Desc;
        public int GroupRate;
        public int GroupID;

        public SuiteDropRateInfo(Dictionary<string, object> data)
        {
            Id = int.Parse(data.Get<string>(BCKey.id));
            SuiteID = int.Parse(data.Get<string>(BCKey.suite_id));
            Key = data.Get<string>(BCKey.key);
            Value = int.Parse(data.Get<string>(BCKey.value));
            Quantity = int.Parse(data.Get<string>(BCKey.Quantity));
            Rate = int.Parse(data.Get<string>(BCKey.rate));
            Desc = data.Get<string>(BCKey.desc);
            GroupRate = int.Parse(data.Get<string>(BCKey.GroupRate));
            GroupID = int.Parse(data.Get<string>(BCKey.GroupId));
        }
    }
    public class StageInfo
    {
        public int Id;
        public int ExpMin;
        public int ExpMax;
        public int ExpUser;
        public int MonsterSuite; // item rot ra
        public int StartWheelId; // id vong quay bat dau

        //Boss
        public float BossWheelRate;
        public int BossWheelID;

        // Devil
        public float DevilRate;
        public List<int> DevilSkills;

        // Angle
        public float AngleRate;
        public List<int> AngleSkill_1;
        public List<int> AngleSkill_2;

        // Vendor
        public float VendorRate;
        public int VendorSuite;

        public int AdsWheelID;

        // Skill Level Up va Skill Vong Quay
        public List<int> RandomSkills;
        public int Chapter;

        public float ItemDrop;
        public List<int> ListIDItems;

        // bouns
        private float aTKBouns;
        private float hPBouns;
        public float ATKBouns
        {
            get
            {
                if (aTKBouns == 0)
                    return 1;
                return aTKBouns;
            }
            set => aTKBouns = value;
        }
        public float HPBouns
        {
            get
            {
                if (hPBouns == 0)
                    return 1;
                return hPBouns;
            }
            set => hPBouns = value;
        }

        public StageInfo(Dictionary<string, object> data)
        {
            //id
            Id = int.Parse(data.Get<string>(BCKey.id));

            //exp
            ExpMin = int.Parse(data.Get<string>(BCKey.stage_minreward));
            ExpMax = int.Parse(data.Get<string>(BCKey.stage_maxreward));
            ExpUser = int.Parse(data.Get<string>(BCKey.stage_userexp));

            // Bonus
            ATKBouns = float.Parse(data.Get<string>(BCKey.atk_bonus));
            HPBouns = float.Parse(data.Get<string>(BCKey.hp_bonus));

            // Item rate
            MonsterSuite = int.Parse(data.Get<string>(BCKey.monster_suite));

            // Wheel
            if (data.Get<string>(BCKey.start_wheel_id) != null)
                StartWheelId = int.Parse(data.Get<string>(BCKey.start_wheel_id));

            if (data.Get<string>(BCKey.boss_wheel_rate) != null)
                BossWheelRate = float.Parse(data.Get<string>(BCKey.boss_wheel_rate));

            if (data.Get<string>(BCKey.boss_wheel_id) != null)
                BossWheelID = int.Parse(data.Get<string>(BCKey.boss_wheel_id));

            // Devil
            DevilRate = float.Parse(data.Get<string>(BCKey.devil_rate));
            if (data.Get<string>(BCKey.devil_skill) != null)
            {
                DevilSkills = new List<int>();
                DevilSkills = GetListInt(data, BCKey.devil_skill);
            }

            // Vendor
            if (data.Get<string>(BCKey.vendor_rate) != null)
                VendorRate = float.Parse(data.Get<string>(BCKey.vendor_rate));
            if (data.Get<string>(BCKey.vendor_suite) != null)
                VendorSuite = int.Parse(data.Get<string>(BCKey.vendor_suite));

            // Angle
            if (data.Get<string>(BCKey.angel_rate) != null)
                AngleRate = float.Parse(data.Get<string>(BCKey.angel_rate));
            if (data.Get<string>(BCKey.angel_skill1) != null)
            {
                AngleSkill_1 = new List<int>();
                AngleSkill_1 = GetListInt(data, BCKey.angel_skill1);
            }
            if (data.Get<string>(BCKey.angel_skill2) != null)
            {
                AngleSkill_2 = new List<int>();
                AngleSkill_2 = GetListInt(data, BCKey.angel_skill2);
            }

            // Ads
            if (data.Get<string>(BCKey.ads_wheel_id) != null)
                AdsWheelID = int.Parse(data.Get<string>(BCKey.ads_wheel_id));

            // Skill Level Up
            if (data.Get<string>(BCKey.random_skill_list) != null)
            {
                RandomSkills = new List<int>();
                RandomSkills = GetListInt(data, BCKey.random_skill_list);
            }
            // Chapter
            if (data.Get<string>(BCKey.chapter) != null)
                Chapter = int.Parse(data.Get<string>(BCKey.chapter));

            // Item demo: bản mới đã dời sang bảng dữ liệu khác
            ItemDrop = 0.2f;
            ListIDItems = new List<int>();
            ListIDItems.Add(2);
            ListIDItems.Add(17);
        }

        List<int> GetListInt(Dictionary<string, object> data, string name)
        {
            List<int> listInt = new List<int>();
            string listSkillStr = data.Get<string>(name);
            string[] strF_E = listSkillStr.Split('"');
            string strTemp = strF_E[0].TrimStart('[');
            strTemp = strTemp.TrimEnd(']');

            string[] list = strTemp.Split(',');
            foreach (var item in list)
            {
                listInt.Add(int.Parse(item));
            }
            return listInt;
        }
    }


    public class LevelChapter
    {
        public int Id;
        public string Name;
        public List<int> level_exps; // danh exp trong trong chapter

        public LevelChapter(Dictionary<string, object> data)
        {
            Id = int.Parse(data.Get<string>(BCKey.ID));
            Name = data.Get<string>(BCKey.Name);
            level_exps = new List<int>();
            if (data.Get<string>(BCKey.Levels_exp) != null)
            {
                level_exps = new List<int>();
                level_exps = GetListInt(data, BCKey.Levels_exp);
            }
        }
        List<int> GetListInt(Dictionary<string, object> data, string name)
        {
            List<int> listInt = new List<int>();
            string listSkillStr = data.Get<string>(name);

            string[] strF_E = listSkillStr.Split('"');
            string strTemp = strF_E[0].TrimStart('[');
            strTemp = strTemp.TrimEnd(']');

            string[] list = strTemp.Split(',');
            foreach (var item in list)
            {
                listInt.Add(int.Parse(item));
            }
            return listInt;
        }
    }
    public class PopupLevelUpInfo
    {
        public int Level { get; set; }
        public int Gem { get; set; }
        public int Ads { get; set; }

        public PopupLevelUpInfo(Dictionary<string, object> data)
        {
            Level = int.Parse(data.Get<string>(BCKey.level));
            Gem = int.Parse(data.Get<string>(BCKey.gem));
            Ads = int.Parse(data.Get<string>(BCKey.ads));
        }
    }
    public class PopupThienThanInfo
    {
        public int Id;
        public float Gem { get; set; }
        public float Ads_rate { get; set; }

        public PopupThienThanInfo(Dictionary<string, object> data)
        {
            Id = int.Parse(data.Get<string>(BCKey.id));
            Gem = float.Parse(data.Get<string>(BCKey.gem));
            Ads_rate = float.Parse(data.Get<string>(BCKey.ads_rate));
        }
    }
    public class PopupDevilInfo
    {
        public int Id;
        public int Gem { get; set; }
        public float Ads_rate { get; set; }
        public float Hp { get; set; }

        public PopupDevilInfo(Dictionary<string, object> data)
        {
            Id = int.Parse(data.Get<string>(BCKey.id));
            Gem = int.Parse(data.Get<string>(BCKey.gem));
            Ads_rate = float.Parse(data.Get<string>(BCKey.ads_rate));
            Hp = float.Parse(data.Get<string>(BCKey.hp));
        }
    }
    //public class WheelContainer
    //{
    //    public int ID;
    //    public List<ItemWheel> Items;

    //    public WheelContainer(int iD, List<ItemWheel> items)
    //    {
    //        ID = iD;
    //        Items = items;
    //    }
    //}
    public class ItemWheel
    {
        public int IDWheel;
        public EnumItemWheelType ItemType;
        public List<int> Values;
        public string AssetName;
        public string ColorCode;
        public string Description;

        public ItemWheel(Dictionary<string, object> data)
        {
            IDWheel = int.Parse(data.Get<string>(BCKey.ID));
            int number = int.Parse(data.Get<string>(BCKey.Type));

            switch (number)
            {
                case 1:
                    {
                        ItemType = EnumItemWheelType.Skill;
                        break;
                    }
                case 2:
                    {
                        ItemType = EnumItemWheelType.Gold;
                        break;
                    }
                case 3:
                    {
                        ItemType = EnumItemWheelType.Gem;
                        break;
                    }
            }
            Values = GetListInt(data, BCKey.Value);
            AssetName = data.Get<string>(BCKey.asset);
            ColorCode = data.Get<string>(BCKey.Color);
            Description = data.Get<string>(BCKey.desc);
        }
        List<int> GetListInt(Dictionary<string, object> data, string name)
        {
            List<int> listInt = new List<int>();
            string listSkillStr = data.Get<string>(name);

            string[] strF_E = listSkillStr.Split('"');
            string strTemp = strF_E[0].TrimStart('[');
            strTemp = strTemp.TrimEnd(']');

            string[] list = strTemp.Split(',');
            foreach (var item in list)
            {
                listInt.Add(int.Parse(item));
            }
            return listInt;
        }
    }



}

