﻿using Studio;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BCEquipmentGrade 
{
    public int id;
    public string assetName;
    public string nameGrade;
    public int maxUpdate;

    public BCEquipmentGrade(Dictionary<string,object> data)
    {
        Parse(data);
    }
    public void Parse(Dictionary<string,object> data)
    {
        id = data.Get<int>(BCKey.id);
        assetName = data.Get<string>(BCKey.AssetName);
        nameGrade = data.Get<string>(BCKey.NameGrade);
        maxUpdate = data.Get<int>(BCKey.MaxUpdate);

    }
}
