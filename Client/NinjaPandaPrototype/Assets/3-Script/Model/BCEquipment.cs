﻿using Studio;
using Studio.BC;
using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class BCEquipment
{
    public int ID;
    public string assetName;
    //public EnumTypeEquipment typeEquipment;
    //public EnumGradeEquipment gradeEquipment;
    public BCEquipmentStyle typeEquipment;
    public BCEquipmentGrade gradeEquipment;
    public int gemPrice;
    public int bounusEnhance;
    public BCEffectEquipment mainPropertype;
    public List<BCEffectEquipment> subPropertype;
    public string nameEquipment;
    public string decEquipment;
    public BCEquipment(Dictionary<string, object> data)
    {
        subPropertype = new List<BCEffectEquipment>();
        Parse(data);
    }
    public void Parse(Dictionary<string, object> data)
    {
        ID = int.Parse(data.Get<string>(BCKey.id));
        assetName = data.Get<string>(BCKey.asset_name_);
        //typeEquipment = (EnumTypeEquipment)data.Get<int>(BCKey.Type_Item);
        //gradeEquipment = (EnumGradeEquipment)data.Get<int>(BCKey.Grade_Item);
        int TypeEquipment = int.Parse(data.Get<string>(BCKey.type_item));
        int GradeEquipment = int.Parse(data.Get<string>(BCKey.grade_item));
        typeEquipment = BCCache.Api.DataConfig.EquipmentStyleConfigData[TypeEquipment];
        gradeEquipment = BCCache.Api.DataConfig.EquipmentGradelConfigData[GradeEquipment];

        //gemPrice = int.Parse(data.Get<string>(BCKey.gem_price));
        bounusEnhance = int.Parse(data.Get<string>(BCKey.bonus_enhance));
        int mainPor = int.Parse(data.Get<string>(BCKey.main_property));
        mainPropertype = BCCache.Api.DataConfig.EffectEquipmentConfigData[mainPor];
        var a = data.Get<List<object>>(BCKey.sub_property);
        int t = int.Parse(data.Get<string>(BCKey.sub_property));
        //Debug.Log(t);
        if (t > 0)
        {
            BCEffectEquipment effect = BCCache.Api.DataConfig.EffectEquipmentConfigData[(int)t];
            subPropertype.Add(effect);
        }
        //foreach (var tg in a)
        //{

        //    Int64 i = (Int64)tg;
        //    BCEffectEquipment effect = BCCache.Api.DataConfig.EffectEquipmentConfigData[(int)i];
        //    subPropertype.Add(effect);
        //}
        nameEquipment = data.Get<string>(BCKey.name_item);
        decEquipment = data.Get<string>(BCKey.dec_item);
    }

}
