﻿using System.Collections;
using System.Collections.Generic;
using Studio.Sound;
using UnityEngine;

namespace Studio.BC
{
    public class BCSound : SSoundManager
    {
        private static BCSound _api;
        public static BCSound Api
        {
            get
            {
                if (_api == null)
                {
                    var soundGo = new GameObject("_SoundManager");
                    _api = soundGo.AddComponent<BCSound>();
                    DontDestroyOnLoad(soundGo);
                }

                return _api;
            }
        }
    }
 
    public static class ClipName
    {

        public const string BG_BATTLE = "bg_battle";
        public const string MONEY_SMALL = "money_small";

        public const string GUN_SHOT = "gun_shot";
        public const string change = "change";

        public const string WHEEL_WIN = "wheel_win";
        public const string BUTTON3 = "button3";
        public const string BUTTON6 = "button6";
        public const string BUTTON7 = "button7";
        public const string LOBBY = "Lobby";
        public const string skill_8_fire = "skill_8_fire";
        public const string skill_active = "skill_active";
        public const string boss = "boss";
    }
}

