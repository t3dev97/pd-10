﻿
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Studio.BC
{
    public class BCLoading : MonoBehaviour
    {
        Action<float> processing;
        int maxProcess = 12;
        int curProcess;


        public IEnumerator LoadResources(Action<float> processing, Action finalAction)
        {
            curProcess = 0;
            this.processing = processing;
            yield return StartCoroutine(LoadResourceCache(finalAction));
        }

        private IEnumerator LoadResourceCache(Action finalAction)
        {
            BCResource.Api.IsCacheAsset = true;


            yield return StartCoroutine(LoadGlobalConfig());
            processing?.Invoke(curProcess / (float)maxProcess);
            curProcess++;

            processing?.Invoke(curProcess / (float)maxProcess);
            yield return StartCoroutine(LoadEffect());
            curProcess++;

            yield return StartCoroutine(BCResource.Api.LoadConfig("LanguageData", (dataConfig) =>
            {
                if (!string.IsNullOrEmpty(dataConfig))
                {
                    BCConfig.Api.M_LanguageData = new LanguageData().Parse(dataConfig.JsonToDictionary());
                }
            }));
            curProcess++;
            yield return StartCoroutine(BCResource.Api.LoadConfig("shop_config_data", (dataConfig) =>
            {
                if (!string.IsNullOrEmpty(dataConfig))
                {
                    var dict = dataConfig.JsonToDictionary();
                    var items = dict.Get<List<object>>(BCKey.data);
                    {

                        if (items != null)
                        {
                            var shopdata = new ShopConfigData().Parse(items);

                            BCCache.Api.DataConfig.ShopConfigData = shopdata;
                        }
                    }
                }
            }));
            curProcess++;
            yield return StartCoroutine(BCResource.Api.LoadConfig("effect_equipment_data", (dataConfig) =>
            {
                if (!string.IsNullOrEmpty(dataConfig))
                {
                    var dict = dataConfig.JsonToDictionary();
                    var items = dict.Get<List<object>>(BCKey.data);
                    {

                        if (items != null)
                        {

                            BCCache.Api.DataConfig.EffectEquipmentConfigData.Clear();

                            foreach (Dictionary<string, object> obj in items)
                            {
                                BCEffectEquipment talentdata = new BCEffectEquipment(obj);

                                BCCache.Api.DataConfig.EffectEquipmentConfigData.Add(talentdata.ID, talentdata);
                            }
                        }
                    }
                }
            }));
            curProcess++;
            yield return StartCoroutine(BCResource.Api.LoadConfig("suite_common_data", (dataConfig) =>
            {
                if (!string.IsNullOrEmpty(dataConfig))
                {
                    var dict = dataConfig.JsonToDictionary();
                    var items = dict.Get<List<object>>(BCKey.data);
                    {

                        BCCache.Api.DataConfig.SutieCommonConfigData.Clear();
                        foreach (Dictionary<string, object> obj in items)
                        {
                            BCSuiteCommon talentdata = new BCSuiteCommon(obj);

                            BCCache.Api.DataConfig.SutieCommonConfigData.Add(talentdata.ID, talentdata);
                        }
                    }
                }
            }));

            yield return StartCoroutine(BCResource.Api.LoadConfig("talent_data", (dataConfig) =>
            {
                if (!string.IsNullOrEmpty(dataConfig))
                {
                    var dict = dataConfig.JsonToDictionary();
                    var items = dict.Get<List<object>>(BCKey.data);
                    {

                        if (items != null)
                        {

                            BCCache.Api.DataConfig.TalenConfigData.Clear();
                            foreach (Dictionary<string, object> obj in items)
                            {
                                TalentConfigData talentdata = new TalentConfigData(obj);

                                BCCache.Api.DataConfig.TalenConfigData.Add(talentdata.ID, talentdata);
                            }
                        }
                    }
                }
            }));

            yield return StartCoroutine(BCResource.Api.LoadConfig("userlv_config_data", (dataConfig) =>
            {
                if (!string.IsNullOrEmpty(dataConfig))
                {
                    var dict = dataConfig.JsonToDictionary();
                    var items = dict.Get<List<object>>(BCKey.data);
                    {

                        if (items != null)
                        {

                            BCCache.Api.DataConfig.UserLevelConfigData.Clear();
                            foreach (Dictionary<string, object> obj in items)
                            {
                                BCUserLevelConfig talentdata = new BCUserLevelConfig(obj);

                                BCCache.Api.DataConfig.UserLevelConfigData.Add(talentdata.id, talentdata);
                            }
                        }
                    }
                }
            }));

            curProcess++;
            yield return StartCoroutine(BCResource.Api.LoadConfig("achivement_config_data", (dataConfig) =>
            {
                if (!string.IsNullOrEmpty(dataConfig))
                {
                    var dict = dataConfig.JsonToDictionary();
                    var items = dict.Get<List<object>>(BCKey.Items);
                    {

                        if (items != null)
                        {
                            BCCache.Api.DataConfig.AchivementConfigData.Clear();
                            foreach (Dictionary<string, object> obj in items)
                            {
                                var shopdata = new BCAchivement(obj);
                                BCCache.Api.DataConfig.AchivementConfigData.Add(shopdata.ID, shopdata);
                            }


                            //BCCache.Api.DataConfig.ShopConfigData = shopdata;
                        }
                    }
                }
            }));
            yield return StartCoroutine(BCResource.Api.LoadConfig("grade_equipment_data", (dataConfig) =>
            {
                if (!string.IsNullOrEmpty(dataConfig))
                {
                    var dict = dataConfig.JsonToDictionary();
                    var items = dict.Get<List<object>>(BCKey.data);
                    {

                        BCCache.Api.DataConfig.EquipmentGradelConfigData.Clear();
                        foreach (Dictionary<string, object> obj in items)
                        {
                            BCEquipmentGrade talentdata = new BCEquipmentGrade(obj);
                            BCCache.Api.DataConfig.EquipmentGradelConfigData.Add(talentdata.id, talentdata);
                        }
                    }
                }
            }));

            yield return StartCoroutine(BCResource.Api.LoadConfig("style_equipment_data", (dataConfig) =>
            {
                if (!string.IsNullOrEmpty(dataConfig))
                {
                    var dict = dataConfig.JsonToDictionary();
                    var items = dict.Get<List<object>>(BCKey.data);
                    {

                        BCCache.Api.DataConfig.EquipmentStyleConfigData.Clear();
                        foreach (Dictionary<string, object> obj in items)
                        {
                            BCEquipmentStyle talentdata = new BCEquipmentStyle(obj);
                            BCCache.Api.DataConfig.EquipmentStyleConfigData.Add(talentdata.id, talentdata);
                        }
                    }
                }
            }));



            curProcess++;
            yield return StartCoroutine(BCResource.Api.LoadConfig("equipment_data", (dataconfig) =>
            {
                if (!string.IsNullOrEmpty(dataconfig))
                {
                    var dict = dataconfig.JsonToDictionary();
                    var items = dict.Get<List<object>>(BCKey.data);
                    {

                        BCCache.Api.DataConfig.EquipmentConfigData.Clear();
                        foreach (Dictionary<string, object> obj in items)
                        {
                            BCEquipment talentdata = new BCEquipment(obj);
                            BCCache.Api.DataConfig.EquipmentConfigData.Add(talentdata.ID, talentdata);
                        }
                    }
                }
            }));


            curProcess++;
            yield return StartCoroutine(BCResource.Api.LoadConfig("chap_config_data", (dataConfig) =>
            {
                if (!string.IsNullOrEmpty(dataConfig))
                {
                    var dict = dataConfig.JsonToDictionary();
                    var items = dict.Get<List<object>>(BCKey.Items);
                    {
                        if (items != null)
                        {

                            BCCache.Api.DataConfig.MapConfigData.Clear();

                            foreach (Dictionary<string, object> obj in items)
                            {
                                BCMapData talentdata = new BCMapData(obj);

                                BCCache.Api.DataConfig.MapConfigData.Add(talentdata.ID, talentdata);
                            }
                        }
                    }
                }
            }));
            curProcess++;




            yield return StartCoroutine(BCResource.Api.LoadConfig("material_data", (dataConfig) =>
            {
                if (!string.IsNullOrEmpty(dataConfig))
                {
                    var dict = dataConfig.JsonToDictionary();
                    var items = dict.Get<List<object>>(BCKey.data);
                    {

                        BCCache.Api.DataConfig.MaterialConfigData.Clear();
                        foreach (Dictionary<string, object> obj in items)
                        {
                            BCMaterial talentdata = new BCMaterial(obj);
                            BCCache.Api.DataConfig.MaterialConfigData.Add(talentdata.id, talentdata);
                        }
                    }
                }
            }));

            yield return StartCoroutine(BCResource.Api.LoadConfig("update_equipment_data", (dataConfig) =>
            {
                if (!string.IsNullOrEmpty(dataConfig))
                {
                    var dict = dataConfig.JsonToDictionary();
                    var items = dict.Get<List<object>>(BCKey.data);
                    {

                        BCCache.Api.DataConfig.materialUpdateConfigData.Clear();
                        foreach (Dictionary<string, object> obj in items)
                        {
                            BCMaterialUpdate talentdata = new BCMaterialUpdate(obj);
                            BCCache.Api.DataConfig.materialUpdateConfigData.Add(talentdata.level, talentdata);
                        }
                    }
                }
            }));
            yield return StartCoroutine(BCResource.Api.LoadConfig("game_config_data", (dataConfig) =>
            {
                if (!string.IsNullOrEmpty(dataConfig))
                {
                    var dict = dataConfig.JsonToDictionary();

                    var items = dict.Get<Dictionary<string, object>>(BCKey.data);
                    {

                        if (items != null)
                        {
                            var shopdata = new BCConfigGame(items);

                            BCCache.Api.DataConfig.ConfigGameData = shopdata;
                        }
                    }
                }
            }));


            yield return StartCoroutine(BCResource.Api.LoadConfig("dataTest", (dataConfig) =>
            {
                if (!string.IsNullOrEmpty(dataConfig))
                {
                    var dict = dataConfig.JsonToDictionary();
                    var items = dict.Get<Dictionary<string, object>>(BCKey.data);
                    {

                        if (items != null)
                        {

                            BCCache.Api.DataConfig.ResourceGame = new BCResourceGame(items);

                        }
                    }
                }
            }));

            //curProcess++;
            //yield return StartCoroutine(BCResource.Api.LoadConfig("skill_config_data", (dataConfig) =>
            //{
            //    if (!string.IsNullOrEmpty(dataConfig))
            //    {
            //        var dict = dataConfig.JsonToDictionary();
            //        var items = dict.Get<List<object>>(BCKey.data);
            //        {
            //            if (items != null)
            //            {

            //                BCCache.Api.DataConfig.Skills.Clear();

            //                foreach (Dictionary<string, object> obj in items)
            //                {
            //                    BCSkillBaseData skill = new BCSkillBaseData(obj);

            //                    BCCache.Api.DataConfig.Skills.Add(skill.Id, skill);
            //                }
            //            }
            //        }
            //    }
            //}));

            curProcess++;
            yield return StartCoroutine(BCResource.Api.LoadConfig("level_stage_config_data", (dataConfig) =>
            {
                if (!string.IsNullOrEmpty(dataConfig))
                {
                    var dict = dataConfig.JsonToDictionary();
                    var items = dict.Get<List<object>>(BCKey.data);
                    {
                        if (items != null)
                        {
                            BCCache.Api.DataConfig.LevelChapters.Clear();
                            foreach (Dictionary<string, object> obj in items)
                            {
                                LevelChapter item = new LevelChapter(obj);
                                BCCache.Api.DataConfig.LevelChapters.Add(item.Id, item);
                            }
                        }
                    }
                }
            }));

            curProcess++;
            yield return StartCoroutine(BCResource.Api.LoadConfig("stageexp", (dataConfig) =>
            {
                if (!string.IsNullOrEmpty(dataConfig))
                {
                    var dict = dataConfig.JsonToDictionary(); // đọc toàn bộ file
                    var items = dict.Get<List<object>>(BCKey.stage_exp);

                    if (items != null)
                    {
                        BCCache.Api.DataConfig.StageInfos.Clear();

                        foreach (Dictionary<string, object> obj in items)
                        {
                            StageInfo item = new StageInfo(obj);

                            BCCache.Api.DataConfig.StageInfos.Add(item.Id, item);
                        }
                    }
                }
            }));
            yield return StartCoroutine(BCResource.Api.LoadConfig("wheel_config", (dataConfig) =>
            {
                if (!string.IsNullOrEmpty(dataConfig))
                {
                    var document = dataConfig.JsonToDictionary();
                    var listItem = document.Get<List<object>>(BCKey.data);
                    foreach (Dictionary<string, object> row in listItem)
                    {
                        ItemWheel item = new ItemWheel(row);
                        if (BCCache.Api.DataConfig.WheelContainers.ContainsKey(item.IDWheel))
                        {
                            BCCache.Api.DataConfig.WheelContainers[item.IDWheel].Add(item);
                        }
                        else
                        {
                            BCCache.Api.DataConfig.WheelContainers.Add(item.IDWheel, new List<ItemWheel>() { item });
                        }
                    }
                }
            }));
            curProcess++;
            yield return StartCoroutine(BCResource.Api.LoadConfig("suite_drop_rate", (dataConfig) =>
            {
                if (!string.IsNullOrEmpty(dataConfig))
                {
                    var document = dataConfig.JsonToDictionary();
                    var listItem = document.Get<List<object>>(BCKey.data);
                    foreach (Dictionary<string, object> row in listItem)
                    {
                        SuiteDropRateInfo item = new SuiteDropRateInfo(row);
                        if (BCCache.Api.DataConfig.SuiteDropRateInfos.ContainsKey(item.SuiteID))
                        {
                            BCCache.Api.DataConfig.SuiteDropRateInfos[item.SuiteID].listSuiteDropRate.Add(item.Id, item);
                        }
                        else
                        {
                            SuiteDropRateSimple t = new SuiteDropRateSimple();
                            t.listSuiteDropRate.Add(item.Id, item);
                            BCCache.Api.DataConfig.SuiteDropRateInfos.Add(item.SuiteID, t);
                        }
                        //BCCache.Api.DataConfig.SuiteDropRateInfos.Add(item.IDWheel, new List<ItemWheel>() { item });
                    }
                }
            }));
            curProcess++;
            yield return StartCoroutine(BCResource.Api.LoadConfig("map_config_data", (dataConfig) =>
            {
                if (!string.IsNullOrEmpty(dataConfig))
                {
                    var dict = dataConfig.JsonToDictionary();
                    var items = dict.Get<List<object>>(BCKey.data);

                    if (items != null)
                    {

                        BCCache.Api.DataConfig.MapInfo.Clear();

                        foreach (Dictionary<string, object> obj in items)
                        {
                            BCMapInfo skill = new BCMapInfo(obj);

                            BCCache.Api.DataConfig.MapInfo.Add(skill.chapter, skill);
                        }


                    }
                }

            }));

            curProcess++;

            curProcess++;
            yield return StartCoroutine(BCResource.Api.LoadConfig("item_config_data", (dataConfig) =>
            {
                if (!string.IsNullOrEmpty(dataConfig))
                {
                    var dict = dataConfig.JsonToDictionary();
                    var items = dict.Get<List<object>>(BCKey.data);
                    {

                        if (items != null)
                        {

                            BCCache.Api.DataConfig.Items.Clear();

                            foreach (Dictionary<string, object> obj in items)
                            {
                                BCItem item = new BCItem(obj);


                                BCCache.Api.DataConfig.Items.Add(item.Id, item);
                            }
                        }
                    }
                }
            }));

            yield return StartCoroutine(BCResource.Api.LoadConfig("skill_config_data_v2", (dataConfig) =>
            {
                if (!string.IsNullOrEmpty(dataConfig))
                {
                    var dict = dataConfig.JsonToDictionary();
                    var items = dict.Get<List<object>>(BCKey.data);
                    {

                        if (items != null)
                        {

                            BCCache.Api.DataConfig.SkillBases.Clear();
                            int id = 1;
                            foreach (Dictionary<string, object> obj in items)
                            {
                                BCSkillBaseData skill = BCSkillBaseData.GetStyleSkill(obj);


                                BCCache.Api.DataConfig.SkillBases.Add(skill.id, skill);
                                id++;
                            }
                        }
                    }
                }
            }));



            yield return StartCoroutine(BCResource.Api.LoadConfig("popup_level_up_config", (dataConfig) =>
            {
                if (!string.IsNullOrEmpty(dataConfig))
                {
                    var dict = dataConfig.JsonToDictionary(); // đọc toàn bộ file

                    var items = dict.Get<List<object>>(BCKey.data);

                    if (items != null)
                    {

                        BCCache.Api.DataConfig.PopupLevelUpConfig.Clear();

                        foreach (Dictionary<string, object> obj in items)
                        {
                            PopupLevelUpInfo item = new PopupLevelUpInfo(obj);

                            BCCache.Api.DataConfig.PopupLevelUpConfig.Add(item.Level, item);
                        }
                    }
                }

            }));

            yield return StartCoroutine(BCResource.Api.LoadConfig("popup_angle_config", (dataConfig) =>
            {
                if (!string.IsNullOrEmpty(dataConfig))
                {
                    var dict = dataConfig.JsonToDictionary(); // đọc toàn bộ file
                    var items = dict.Get<List<object>>(BCKey.data);

                    if (items != null)
                    {
                        BCCache.Api.DataConfig.PopupThienThanConfig.Clear();

                        foreach (Dictionary<string, object> obj in items)
                        {
                            PopupThienThanInfo item = new PopupThienThanInfo(obj);

                            BCCache.Api.DataConfig.PopupThienThanConfig.Add(item.Id, item);
                        }
                    }
                }

            }));

            yield return StartCoroutine(BCResource.Api.LoadConfig("popup_devil_config", (dataConfig) =>
            {
                if (!string.IsNullOrEmpty(dataConfig))
                {
                    var dict = dataConfig.JsonToDictionary(); // đọc toàn bộ file
                    var items = dict.Get<List<object>>(BCKey.data);

                    if (items != null)
                    {
                        BCCache.Api.DataConfig.PopupDevilConfig.Clear();

                        foreach (Dictionary<string, object> obj in items)
                        {
                            PopupDevilInfo item = new PopupDevilInfo(obj);

                            BCCache.Api.DataConfig.PopupDevilConfig.Add(item.Id, item);
                        }
                    }
                }

            }));



            if (processing != null)
                processing(curProcess / (float)maxProcess);
            yield return StartCoroutine(LoadSound());
            curProcess++;

            BCResource.Api.ClearAssetBundleCache(false);
            yield return Resources.UnloadUnusedAssets();
            finalAction.Invoke();
            System.GC.Collect();

        }

        //Load nhung config dung xuyen suot game
        private IEnumerator LoadGlobalConfig()
        {
            if (BCResource.Api.IsLocalResource)
            {
                BCResource.Api.LoadLocalGlobalConfig((result) =>
                {
                    foreach (var item in result)
                        BCCache.Api.DataConfig.LoadData(item.Key, item.Value);
                });
            }
            yield return null;
            //else
            //{
            //    yield return StartCoroutine(BCResource.Api.LoadOnlineGlobalConfig("_global", (globalName, globalText) =>
            //    {
            //        var dict = globalText.JsonToDictionary();
            //        var list = dict.Get<List<object>>(BCKey.data);

            //        StartCoroutine(LoadGlobalFiles(list));
            //    }));
            //}
        }

        //private IEnumerator LoadGlobalFiles(List<object> list)
        //{
        //    foreach (var o in list)
        //    {
        //        yield return StartCoroutine(BCResource.Api.LoadOnlineGlobalConfig(o.ToString(), (name, text) =>
        //        {
        //            BCCache.Api.DataConfig.LoadData(name, text);
        //        }));
        //    }
        //}

        private IEnumerator LoadEffect()
        {
            var str = "";
            yield return StartCoroutine(BCResource.Api.LoadConfig("EffectResource", (dataConfig) =>
            {
                str = dataConfig;
            }));

            if (!string.IsNullOrEmpty(str))
            {
                var list = str.JsonToDictionary().Get<List<object>>("data");
                var count = list.Count;
                foreach (var o in list)
                {
                    yield return StartCoroutine(BCResource.Api.LoadEffect(o.ToString(), (effectPrefab) =>
                    {
                        BCCache.Api.SaveEffect(effectPrefab);
                    }));
                }
            }
        }

        private IEnumerator LoadSound()
        {
            var audioClipData = new List<string>();
            yield return StartCoroutine(BCResource.Api.LoadConfig("SoundResource", (dataConfig) =>
            {
                if (!string.IsNullOrEmpty(dataConfig))
                {
                    var list = dataConfig.JsonToDictionary().Get<List<object>>("data");
                    foreach (var o in list)
                    {
                        audioClipData.Add(o.ToString());
                    }
                }
            }));

            var soundManager = BCSound.Api.Init();
            var count = audioClipData.Count;

            foreach (var clipName in audioClipData)
            {
                yield return StartCoroutine(BCResource.Api.LoadAudioClip(clipName, (audioClip) =>
                {
                    soundManager.AddAudioClip(audioClip);
                }));
            }
        }

        private IEnumerator LoadLocalize()
        {
            var localizeName = "localize." + BCConfig.Api.M_Language;
            yield return StartCoroutine(BCResource.Api.LoadLocalize(localizeName, (localizeStr) =>
            {
                if (!string.IsNullOrEmpty(localizeStr))
                {
                    DebugX.Log("LoadLocalize from BCLoading: " + localizeStr);
                    LanguageManager.api.AddLocale(localizeName, localizeStr, true);
                }
            }));
        }
    }
}