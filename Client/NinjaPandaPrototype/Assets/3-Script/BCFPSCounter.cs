﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Studio.BC
{
    public class BCFPSCounter : MonoBehaviour
    {
        public Text FpsText;

        private float _startTime;
        private float _counter;
        private bool _isReset = true;

        void Awake()
        {
            gameObject.SetActive(BCConfig.Api.M_ShowFps);
        }

        void Update()
        {
            if (_isReset)
            {
                if (FpsText != null)
                    FpsText.text = _counter + " FPS "/* + QualitySettings.GetQualityLevel()*/;

                _startTime = Time.time;
                _counter = 0;
                _isReset = false;                
            }
            _counter++;
            if ((Time.time - _startTime) >= 1.0f)
            {
                _isReset = true;
            }
        }
    }
}

