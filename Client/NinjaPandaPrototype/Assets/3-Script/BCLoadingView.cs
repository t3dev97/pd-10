﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Studio.BC
{
    public class BCLoadingView : MonoBehaviour
    {
        public Transform Loading;
        public float Timeout;

        private const int Speed = -200;
        private Vector3 _localAngle;

        void Start()
        {
            _localAngle = Loading.localEulerAngles;
        }

        void Update()
        {
            _localAngle.z -= Speed*Time.deltaTime;
            Loading.localEulerAngles = _localAngle;
        }

        public void Init(float timeout = 0)
        {
            Timeout = timeout;
            
            if (Timeout > 0)
                StartCoroutine(StartLoading());
        }

        IEnumerator StartLoading()
        {
            yield return new WaitForSeconds(Timeout);
            Destroy(gameObject);
        }
    }
}

