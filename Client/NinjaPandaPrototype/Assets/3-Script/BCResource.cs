﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Studio.BC
{
    public class BCResource : BaseLoader2
    {
        private static BCResource _api;
        public new static BCResource Api
        {
            get
            {
                if (_api == null)
                {
                    var go = new GameObject("_BCResource", typeof(BCResource));
                    _api = go.GetComponent<BCResource>();
                    DontDestroyOnLoad(go);
                }

                return _api;
            }
        }

        public GameObject RefManageUI;
        public GameObject AtlasUI;

        public bool IsLocalResource = true;

        private Dictionary<string, Sprite> _spriteCacheManager;

        public Dictionary<string, Sprite> _asset2dCacheManager;

        public void InitLocal()
        {
            _spriteCacheManager = new Dictionary<string, Sprite>();
            _asset2dCacheManager = new Dictionary<string, Sprite>();
        }

        public IEnumerator Init(string url, Action<bool> onCompleteAction, Action<string, string> onErrorAction)
        {
            InitLocal();

            yield return StartCoroutine(Initialize(url, "", onCompleteAction, onErrorAction));
        }
        //public IEnumerator InitZip(string url, Action<bool> onCompleteAction, Action<string, string> onErrorAction)
        //{
        //    InitLocal();

        //    yield return StartCoroutine(InitializeZip(url, "", onCompleteAction, onErrorAction));
        //}

        public IEnumerator LoadFish(string fishName, Action<GameObject> onCompleteAction)
        {
            var go = (GameObject)null;
            if (IsLocalResource)
            {
                var request = Resources.LoadAsync<GameObject>("Fish/Prefabs/" + fishName);
                while (!request.isDone)
                {
                    yield return null;
                }
                //yield return request;

                go = request.asset as GameObject;
            }
            else
            {
                yield return StartCoroutine(LoadAssetBundle<GameObject>("fish/prefabs/" + fishName + ".unity3d", fishName, (prefab) =>
                {
                    go = prefab;
                }));
            }

            if (go != null && onCompleteAction != null) onCompleteAction(go);
        }

        public IEnumerator LoadFishPath(string pathName, Action<GameObject> onCompleteAction)
        {
            var go = (GameObject)null;
            if (IsLocalResource)
            {
                var request = Resources.LoadAsync<GameObject>("FishPath/Prefabs/" + pathName);
                while (!request.isDone)
                {
                    yield return null;
                }

                //yield return request;

                go = request.asset as GameObject;
            }
            else
            {
                yield return StartCoroutine(LoadAssetBundle<GameObject>("fishpath/prefabs/" + pathName + ".unity3d", pathName, (prefab) =>
                {
                    go = prefab;
                }));
            }

            if (onCompleteAction != null) onCompleteAction(go);
        }

        public IEnumerator LoadGuns(string pathName, Action<GameObject> onCompleteAction)
        {
            var go = (GameObject)null;
            if (IsLocalResource)
            {
                var request = Resources.LoadAsync<GameObject>("Gun/Prefabs/" + pathName);
                while (!request.isDone)
                {
                    yield return null;
                }

                //yield return request;

                go = request.asset as GameObject;
            }
            else
            {
                yield return StartCoroutine(LoadAssetBundle<GameObject>("gun/prefabs/" + pathName + ".unity3d", pathName, (prefab) =>
                {
                    go = prefab;
                }));
                //DebugX.Log("Has not yet config the AssetBundle!");
            }

            if (onCompleteAction != null) onCompleteAction(go);
        }

        public IEnumerator LoadWings(string pathName, Action<GameObject> onCompleteAction)
        {
            var go = (GameObject)null;
            if (IsLocalResource)
            {
                var request = Resources.LoadAsync<GameObject>("Wing/Prefabs/" + pathName);
                while (!request.isDone)
                {
                    yield return null;
                }

                //yield return request;

                go = request.asset as GameObject;
            }
            else
            {
                yield return StartCoroutine(LoadAssetBundle<GameObject>("wing/prefabs/" + pathName + ".unity3d", pathName, (prefab) =>
                {
                    go = prefab;
                }));
                //DebugX.Log("Has not yet config the AssetBundle!");
            }

            if (onCompleteAction != null) onCompleteAction(go);
        }

        public IEnumerator LoadFrame(string frameName, Action<GameObject> onCompleteAction)
        {
            var go = (GameObject)null;
            if (IsLocalResource)
            {
                var request = Resources.LoadAsync<GameObject>("Frame/Prefabs/" + frameName);
                while (!request.isDone)
                {
                    yield return null;
                }

                //yield return request;

                go = request.asset as GameObject;
            }
            else
            {
                yield return StartCoroutine(LoadAssetBundle<GameObject>("frame/prefabs/" + frameName + ".unity3d", frameName, (prefab) =>
                {
                    go = prefab;
                }));
                //DebugX.Log("Has not yet config the AssetBundle!");
            }

            if (onCompleteAction != null) onCompleteAction(go);
        }

        public IEnumerator LoadEmotions(string pathName, Action<GameObject> onCompleteAction)
        {
            var go = (GameObject)null;
            if (IsLocalResource)
            {
                var request = Resources.LoadAsync<GameObject>("Emotion/Prefabs/" + pathName);
                while (!request.isDone)
                {
                    yield return null;
                }

                //yield return request;

                go = request.asset as GameObject;
            }
            else
            {
                yield return StartCoroutine(LoadAssetBundle<GameObject>("emotion/prefabs/" + pathName + ".unity3d", pathName, (prefab) =>
                {
                    go = prefab;
                }));
                //DebugX.Log("Has not yet config the AssetBundle!");
            }

            if (onCompleteAction != null) onCompleteAction(go);
        }

        public IEnumerator LoadBullets(string pathName, Action<GameObject> onCompleteAction)
        {
            var go = (GameObject)null;
            if (IsLocalResource)
            {
                var request = Resources.LoadAsync<GameObject>("Bullet/" + pathName);
                while (!request.isDone)
                {
                    yield return null;
                }

                //yield return request;

                go = request.asset as GameObject;
            }
            else
            {
                yield return StartCoroutine(LoadAssetBundle<GameObject>("bullet/" + pathName + ".unity3d", pathName, (prefab) =>
                {
                    go = prefab;
                }));
                //DebugX.Log("Has not yet config the AssetBundle!");
            }

            if (onCompleteAction != null) onCompleteAction(go);
        }

        public IEnumerator LoadBulletNet(string pathName, Action<GameObject> onCompleteAction)
        {
            var go = (GameObject)null;
            if (IsLocalResource)
            {
                var request = Resources.LoadAsync<GameObject>("Web/" + pathName);
                while (!request.isDone)
                {
                    yield return null;
                }

                //yield return request;

                go = request.asset as GameObject;
            }
            else
            {
                yield return StartCoroutine(LoadAssetBundle<GameObject>("web/" + pathName + ".unity3d", pathName, (prefab) =>
                {
                    go = prefab;
                }));
                // DebugX.Log("Has not yet config the AssetBundle!");
            }

            if (onCompleteAction != null) onCompleteAction(go);
        }

        public IEnumerator LoadConfig(string configName, Action<string> onCompleteAction)
        {
            var go = (TextAsset)null;
            if (IsLocalResource)
            {
                var request = Resources.LoadAsync<TextAsset>("Config/" + configName);
                while (!request.isDone)
                {
                    yield return null;
                }

                //yield return request;

                go = request.asset as TextAsset;
            }
            else
            {
                yield return StartCoroutine(LoadAssetBundle<TextAsset>("config/" + configName + ".unity3d", configName, (prefab) =>
                {
                    go = prefab;
                }));
            }

            if (go != null && onCompleteAction != null)
            {
                onCompleteAction(go != null ? go.text : "");
            }
        }

        public IEnumerator LoadGlobalConfig(string configName, Action<string> onCompleteAction)
        {
            var configPath = "Config/Global/";
            //switch (BCConfig.Api.ResourceConfigType)
            //{
            //    case EnumResourceConfigType.Dev:
            //        {
            //            configPath = "Config/Global/";
            //            break;
            //        }
            //    case EnumResourceConfigType.QC:
            //        {
            //            configPath = "Config/QCGlobal/";
            //            break;
            //        }
            //    case EnumResourceConfigType.Online:
            //        {
            //            configPath = "Config/OnlineGlobal/";
            //            break;
            //        }
            //}
            var go = (TextAsset)null;
            if (IsLocalResource)
            {
                //var request = Resources.LoadAsync<TextAsset>("Config/Global/" + configName);
                var request = Resources.LoadAsync<TextAsset>(configPath + configName);
                while (!request.isDone)
                {
                    yield return null;
                }

                //yield return request;

                go = request.asset as TextAsset;
            }
            else
            {
                //yield return StartCoroutine(LoadAssetBundle<TextAsset>("config/global/" + configName + ".unity3d", configName, (prefab) =>
                yield return StartCoroutine(LoadAssetBundle<TextAsset>(configPath.ToLower() + configName + ".unity3d", configName, (prefab) =>
                 {
                     go = prefab;
                 }));
            }

            if (go != null && onCompleteAction != null)
            {
                onCompleteAction(go != null ? go.text : "");
            }

        }
        public void LoadLocalGlobalConfig(Action<Dictionary<string, string>> onCompleteAction)
        {
            var configPath = "Config/Global/";
            //switch (BCConfig.Api.ResourceConfigType)
            //{
            //    case EnumResourceConfigType.Dev:
            //        {
            //            configPath = "Config/Global/";
            //            break;
            //        }
            //    case EnumResourceConfigType.QC:
            //        {
            //            configPath = "Config/QCGlobal/";
            //            break;
            //        }
            //    case EnumResourceConfigType.Online:
            //        {
            //            configPath = "Config/OnlineGlobal/";
            //            break;
            //        }
            //}
            var TextAssetdict = new Dictionary<string, string>();
            var request = Resources.LoadAll<TextAsset>(configPath);
            foreach (TextAsset item in request)
            {
                var txt = item.text;
                var encode = Utils.SimpleEncryptDecrypt.HasEncrypt(item.text);
                if (encode)
                    txt = Utils.SimpleEncryptDecrypt.Decrypt(item.text);
                TextAssetdict.Add(item.name, txt);
            }

            if (onCompleteAction != null)
                onCompleteAction(TextAssetdict);

        }
        //public IEnumerator LoadOnlineGlobalConfig(string configName, Action<string, string> onCompleteAction)
        //{
        //    var configPath = BundleUtils.GetCachePath();
        //    var globalFolder = "";
        //    //switch (BCConfig.Api.ResourceConfigType)
        //    //{
        //    //    case EnumResourceConfigType.Dev:
        //    //        {
        //    //            globalFolder = "/Config/Global/";
        //    //            break;
        //    //        }
        //    //    case EnumResourceConfigType.QC:
        //    //        {
        //    //            globalFolder = "/Config/QCGlobal/";
        //    //            break;
        //    //        }
        //    //    case EnumResourceConfigType.Online:
        //    //        {
        //    //            globalFolder = "/Config/OnlineGlobal/";
        //    //            break;
        //    //        }
        //    //}
        //    configPath += globalFolder.ToLower();
        //    yield return StartCoroutine(DownloadGlobalConfigFile(BCConfig.Api.DataConfigUrl, configName, configPath,(text)=>
        //    {
        //        if (onCompleteAction != null)
        //            onCompleteAction(configName, text);
        //    }));
        //}

        public IEnumerator LoadAtlas(string atlasName, Action<GameObject> onCompleteAction)
        {
            var go = (GameObject)null;
            if (IsLocalResource)
            {
                var request = Resources.LoadAsync<GameObject>("UI/Atlas/" + atlasName);
                yield return request;

                go = request.asset as GameObject;
            }
            else
            {
                atlasName = atlasName.ToLower();
                yield return StartCoroutine(LoadAssetBundle<GameObject>("atlas/" + atlasName + ".unity3d", atlasName, (prefab) =>
                {
                    go = prefab;
                }));
            }

            if (go != null && onCompleteAction != null) onCompleteAction(go);
        }
        public IEnumerator LoadMessage(string atlasName, Action<GameObject> onCompleteAction)
        {
            var go = (GameObject)null;
            if (IsLocalResource)
            {
                var request = Resources.LoadAsync<GameObject>("Message/Prefabs/" + atlasName);
                yield return request;

                go = request.asset as GameObject;
            }
            else
            {
                atlasName = atlasName.ToLower();
                yield return StartCoroutine(LoadAssetBundle<GameObject>("message/prefabs/" + atlasName + ".unity3d", atlasName, (prefab) =>
                {
                    go = prefab;
                }));
            }

            if (go != null && onCompleteAction != null) onCompleteAction(go);
        }
        public IEnumerator LoadEffect(string effectName, Action<GameObject> onCompleteAction)
        {
            var go = (GameObject)null;
            if (IsLocalResource)
            {
                var request = Resources.LoadAsync<GameObject>("Effect/Prefabs/" + effectName);
                while (!request.isDone)
                {
                    yield return new WaitForSeconds(Time.deltaTime);
                }
                go = request.asset as GameObject;
            }
            else
            {
                yield return StartCoroutine(LoadAssetBundle<GameObject>("effect/prefabs/" + effectName + ".unity3d", effectName, (prefab) =>
                {
                    if (prefab != null && onCompleteAction != null)
                    {
                        onCompleteAction(prefab);
                    }
                }));
            }

            if (go != null && onCompleteAction != null)
            {
                onCompleteAction(go);
            }
        }

        public IEnumerator LoadButton(string buttonName, Action<GameObject> onCompleteAction)
        {
            var go = (GameObject)null;
            if (IsLocalResource)
            {
                var request = Resources.LoadAsync<GameObject>("Buttons/Prefabs/" + buttonName);
                while (!request.isDone)
                {
                    yield return new WaitForSeconds(Time.deltaTime);
                }
                go = request.asset as GameObject;
            }
            else
            {
                yield return StartCoroutine(LoadAssetBundle<GameObject>("buttons/prefabs/" + buttonName + ".unity3d", buttonName, (prefab) =>
                {
                    go = prefab;
                }));
            }

            if (go != null && onCompleteAction != null)
            {
                onCompleteAction(go);
            }
        }



        public IEnumerator LoadBullet(string buttonName, Action<GameObject> onCompleteAction)
        {
            var go = (GameObject)null;
            if (IsLocalResource)
            {
                var request = Resources.LoadAsync<GameObject>("Bullets/Prefabs/" + buttonName);
                while (!request.isDone)
                {
                    yield return new WaitForSeconds(Time.deltaTime);
                }
                go = request.asset as GameObject;
            }
            else
            {
                yield return StartCoroutine(LoadAssetBundle<GameObject>("bullets/prefabs/" + buttonName + ".unity3d", buttonName, (prefab) =>
                {
                    go = prefab;
                }));
            }

            if (go != null && onCompleteAction != null)
            {
                onCompleteAction(go);
            }
        }

        public IEnumerator LoadSkill(string skillName, Action<GameObject> onCompleteAction)
        {
            var go = (GameObject)null;
            if (IsLocalResource)
            {
                var request = Resources.LoadAsync<GameObject>("Skill/" + skillName);
                while (!request.isDone)
                {
                    yield return new WaitForSeconds(Time.deltaTime);
                }
                go = request.asset as GameObject;
            }
            else
            {
                yield return StartCoroutine(LoadAssetBundle<GameObject>("skill/" + skillName + ".unity3d", skillName, (prefab) =>
                {
                    go = prefab;
                }));
            }

            if (go != null && onCompleteAction != null)
            {
                onCompleteAction(go);
            }
        }

        public IEnumerator LoadEffectAsync(string effectName, Action<GameObject> onCompleteAction)
        {
            var go = (GameObject)null;
            if (IsLocalResource)
            {
                var request = Resources.LoadAsync<GameObject>("Effect/PrefabAsync/" + effectName);
                while (!request.isDone)
                {
                    yield return new WaitForSeconds(Time.deltaTime);
                }
                go = request.asset as GameObject;
            }
            else
            {
                yield return StartCoroutine(LoadAssetBundle<GameObject>("effect/" + effectName + ".unity3d", effectName, (prefab) =>
                {
                    go = prefab;
                }));
            }

            if (go != null && onCompleteAction != null) onCompleteAction(go);
        }

        public IEnumerator LoadAudioClip(string clipName, Action<AudioClip> onCompleteAction)
        {
            var audioClip = (AudioClip)null;
            if (IsLocalResource)
            {
                var request = Resources.LoadAsync<AudioClip>("Sound/" + clipName);
                yield return request;
                audioClip = request.asset as AudioClip;
            }
            else
            {
                yield return StartCoroutine(LoadAssetBundle<AudioClip>("sound/" + clipName + ".unity3d", clipName, (prefab) =>
                {
                    audioClip = prefab;
                }));
            }

            if (audioClip != null && onCompleteAction != null) onCompleteAction(audioClip);
        }

        public IEnumerator LoadAsset2D(string spriteName, Action<Sprite> onCompleteAction)
        {
            //check cache
            if (_spriteCacheManager.ContainsKey(spriteName))
            {
                if (onCompleteAction != null) onCompleteAction(_spriteCacheManager[spriteName]);
                yield break;
            }

            var sprite = (Sprite)null;
            if (IsLocalResource)
            {
                var request = Resources.LoadAsync<Sprite>("Asset2d/" + spriteName);
                yield return request;
                sprite = request.asset as Sprite;
            }
            else
            {
                yield return StartCoroutine(LoadAssetBundle<Sprite>("asset2d/" + spriteName + ".unity3d", spriteName, (prefab) =>
                {
                    sprite = prefab;
                }));
            }

            if (onCompleteAction != null) onCompleteAction(sprite);
        }

        public IEnumerator CacheAsset2D(List<string> spriteNameList)
        {
            foreach (var spriteName in spriteNameList)
            {
                var spriteTemp = (Sprite)null;
                yield return StartCoroutine(LoadAsset2D(spriteName, (sprite) =>
                {
                    spriteTemp = sprite;
                }));

                if (spriteTemp != null && !_spriteCacheManager.ContainsKey(spriteName))
                {
                    _spriteCacheManager.Add(spriteName, spriteTemp);
                }
            }

        }

        public IEnumerator LoadAvatar(string avatarName, Action<Sprite> onCompleteAction)
        {
            var sprite = (Sprite)null;
            if (IsLocalResource)
            {
                var request = Resources.LoadAsync<Sprite>("Asset2d/Avatar/" + avatarName);
                yield return request;
                sprite = request.asset as Sprite;
            }
            else
            {
                yield return StartCoroutine(LoadAssetBundle<Sprite>("asset2d/avatar/" + avatarName + ".unity3d", avatarName, (prefab) =>
                {
                    sprite = prefab;
                }));
            }

            if (onCompleteAction != null) onCompleteAction(sprite);
        }

        public IEnumerator LoadBGImage(string resName, Action<Sprite> onCompleteAction)
        {
            var sprite = (Sprite)null;
            if (IsLocalResource)
            {
                var request = Resources.LoadAsync<Sprite>("Asset2d/Background/" + resName);
                yield return request;
                sprite = request.asset as Sprite;
            }
            else
            {
                yield return StartCoroutine(LoadAssetBundle<Sprite>("asset2d/background/" + resName + ".unity3d", resName, (prefab) =>
                {
                    sprite = prefab;
                }));
            }

            if (onCompleteAction != null) onCompleteAction(sprite);
        }

        IEnumerator LoadAsset2d_2(EnumDynamicImageType type, string resName, Action<Sprite> onCompleteAction)
        {
            string path = "Asset2d/";
            //switch (type)
            //{
            //             case EnumDynamicImageType.World:
            //                 path += "World/";
            //                 break;
            //             case EnumDynamicImageType.Skill:
            //                 path += "Skill/";
            //                 break;
            //             case EnumDynamicImageType.Currency:
            //                 path += "Currency/";
            //                 break;
            //             case EnumDynamicImageType.Inventory:
            //                 path += "Inventory/";
            //                 break;
            //         }
            path += type + "/";
            var sprite = (Sprite)null;
            var resPath = path + resName;
            if (IsLocalResource)
            {
                //var request = Resources.LoadAsync<Sprite>(resPath);
                //yield return request;
                //sprite = request.asset as Sprite; 
                sprite = Resources.Load<Sprite>(resPath);
            }
            else
            {
                if (_asset2dCacheManager.ContainsKey(resPath))
                    sprite = _asset2dCacheManager[resPath];
                else
                {
                    yield return StartCoroutine(LoadAssetBundle<Sprite>(path.ToLower() + resName + ".unity3d", resName, (prefab) =>
                    {
                        sprite = prefab;
                        if (!_asset2dCacheManager.ContainsKey(resPath))
                            _asset2dCacheManager.Add(resPath, prefab);
                    }));
                }
            }

            if (onCompleteAction != null) onCompleteAction(sprite);
        }


        public IEnumerator LoadEventImage(string resName, Action<Sprite> onCompleteAction)
        {
            var sprite = (Sprite)null;
            if (IsLocalResource)
            {
                var request = Resources.LoadAsync<Sprite>("Asset2d/Event/" + resName);
                yield return request;
                sprite = request.asset as Sprite;
            }
            else
            {
                yield return StartCoroutine(LoadAssetBundle<Sprite>("asset2d/event/" + resName + ".unity3d", resName, (prefab) =>
                {
                    sprite = prefab;
                }));
            }

            if (onCompleteAction != null) onCompleteAction(sprite);
        }

        public IEnumerator LoadLocalize(string resName, Action<string> onCompleteAction)
        {
            var textAsset = (TextAsset)null;
            if (IsLocalResource)
            {
                var request = Resources.LoadAsync<TextAsset>("Localize/" + resName);
                yield return request;
                textAsset = request.asset as TextAsset;
            }
            else
            {
                yield return StartCoroutine(LoadAssetBundle<TextAsset>("localize/" + resName + ".unity3d", resName, (prefab) =>
                {
                    textAsset = prefab;
                }));

            }

            if (onCompleteAction != null) onCompleteAction(textAsset.text);
        }

        public void LoadAsset2d(EnumDynamicImageType type, string resName, Action<Sprite> onCompleteAction)
        {
            StartCoroutine(LoadAsset2d_2(type, resName, onCompleteAction));
        }

        public IEnumerator LoadMGPrefab(string resFullName, string resName, Action<GameObject> onCompleteAction)
        {
            resName = resName.ToLower();
            resFullName = resFullName.ToLower();
            var go = (GameObject)null;
            if (IsLocalResource)
            {
                var request = Resources.LoadAsync<GameObject>(resFullName);
                while (!request.isDone)
                {
                    yield return new WaitForSeconds(Time.deltaTime);
                }
                go = request.asset as GameObject;
            }
            else
            {
                yield return StartCoroutine(LoadAssetBundle<GameObject>(resFullName + ".unity3d", resName, (prefab) =>
                {
                    go = prefab;
                }));
            }
            if (go != null && onCompleteAction != null) onCompleteAction(go);
        }

        public IEnumerator LoadMGS(string resFullName, string resName, Action<TextAsset> onCompleteAction)
        {
            resName = resName.ToLower();
            resFullName = resFullName.ToLower();
            var go = (TextAsset)null;
            if (IsLocalResource)
            {
                var request = Resources.LoadAsync<TextAsset>(resFullName);
                while (!request.isDone)
                {
                    yield return null;
                }

                //yield return request;

                go = request.asset as TextAsset;
            }
            else
            {
                yield return StartCoroutine(LoadAssetBundle<TextAsset>(resFullName + ".unity3d", resName, (prefab) =>
                {
                    go = prefab;
                }));
            }

            if (go != null && onCompleteAction != null)
            {
                onCompleteAction(go);
            }
        }

        public IEnumerator LoadMGA(string resFullName, string spriteName, Action<Sprite> onCompleteAction)
        {
            //check cache
            if (_spriteCacheManager == null)
                _spriteCacheManager = new Dictionary<string, Sprite>();
            if (_spriteCacheManager.ContainsKey(spriteName))
            {
                if (onCompleteAction != null) onCompleteAction(_spriteCacheManager[spriteName]);
                yield break;
            }

            var sprite = (Sprite)null;
            if (IsLocalResource)
            {
                var request = Resources.LoadAsync<Sprite>(resFullName);
                yield return request;
                sprite = request.asset as Sprite;
            }
            else
            {
                yield return StartCoroutine(LoadAssetBundle<Sprite>(resFullName + ".unity3d", spriteName, (prefab) =>
                {
                    sprite = prefab;
                }));
            }

            if (onCompleteAction != null) onCompleteAction(sprite);
        }
    }

}

