﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BCGameController : MonoBehaviour
{
    public static BCGameController api;
    public bool isLoadComplete=false;
    public void Awake()
    {
        if (api == null)
            api = this;
        else if (api != this)
        {
            Destroy(this);
        }
        UpdateResource();
    }
    public void UpdateResource()
    {
        isLoadComplete = true;
    
    }
}
