﻿using System.Collections.Generic;
using UnityEngine;
using Debug = UnityEngine.Debug;

public class Pathfinding : MonoBehaviour
{

    //private Grid Grid.api;
    [HideInInspector]
    public Transform CharacterPosition;
    public List<Node> PathFinal = new List<Node>();
    public bool MoveLine = true;
    private void Start()
    {
        CharacterPosition = GameObject.FindGameObjectWithTag("Player").transform;
        //Grid.api = GameObject.FindGameObjectWithTag("AStar").AddToggleToList<Grid>();
        //StartCoroutine(UpdateLine());
    }
    private void FixedUpdate()
    {
        if (CharacterPosition != null)
        {
            if (Grid.api.NodeFromWorldPoint(CharacterPosition.position) != null && Grid.api.NodeFromWorldPoint(CharacterPosition.position).IsWall == false)
            {
                FindPath(transform.position, CharacterPosition.position);//Find a path to the goal
            }
            else
            {
                //Debug.Log("In Wall");

                //Node temp = FindNodeDistanceLow(getGrid.NodeFromWorldPoint(CharacterPosition.position), getGrid.NodeFromWorldPoint(transform.position));
                //while (temp.IsWall)
                //{
                //    temp = FindNodeDistanceLow(getGrid.NodeFromWorldPoint(temp.Position), getGrid.NodeFromWorldPoint(transform.position));
                //}
                //Debug.Log(temp.PosX + "_Temp_" + temp.PosY + "__" + temp.IsWall);
                //FindPath(transform.position, temp.Position);//Find a path to the goal

                Node posNew = FindFindNodeDistanceLowV2(Grid.api.NodeFromWorldPoint(CharacterPosition.position), Grid.api.NodeFromWorldPoint(transform.position));
                FindPath(transform.position, posNew.Position);
            }
        }
    }

    //IEnumerator UpdateLine()
    //{
    //    yield return new WaitForSeconds(0.01f);

    //    if (Grid.api.NodeFromWorldPoint(CharacterPosition.position).IsWall == false)
    //    {
    //        FindPath(transform.position, CharacterPosition.position);//Find a path to the goal
    //    }
    //    else
    //    {
    //        Debug.Log("In Wall");
    //        Node posNew = FindFindNodeDistanceLowV2(Grid.api.NodeFromWorldPoint(CharacterPosition.position), Grid.api.NodeFromWorldPoint(transform.position));
    //        FindPath(transform.position, posNew.Position);
    //    }
    //    StartCoroutine(UpdateLine());
    //}

    Node FindFindNodeDistanceLowV2(Node characterPos, Node monsterPosition)
    {
        if (characterPos.IsWall == false) return characterPos;
        // top/bottom/right/left
        List<Node> points = new List<Node>();
        List<int> indexs = new List<int>();
        indexs.Add(0);
        indexs.Add(0);
        indexs.Add(0);
        indexs.Add(0);

        if (Grid.api.CheckNodeInGrid(characterPos.PosX, characterPos.PosY + 1))
        {
            points.Add(Grid.api.NodeArray[characterPos.PosX, characterPos.PosY + 1]);
            //Top 
            do
            {
                ++indexs[0];
                if (Grid.api.CheckNodeInGrid(characterPos.PosX, characterPos.PosY + indexs[0]))
                    points[0] = Grid.api.NodeArray[characterPos.PosX, characterPos.PosY + indexs[0]];
                else break;
            } while (points[0].IsWall);
        }

        if (Grid.api.CheckNodeInGrid(characterPos.PosX, characterPos.PosY - 1))
        {
            points.Add(Grid.api.NodeArray[characterPos.PosX, characterPos.PosY - 1]);
            //Bottom
            do
            {
                ++indexs[1];
                if (Grid.api.CheckNodeInGrid(characterPos.PosX, characterPos.PosY - indexs[1]))
                    points[1] = Grid.api.NodeArray[characterPos.PosX, characterPos.PosY - indexs[1]];
                else break;
            } while (points[1].IsWall);
        }

        if (Grid.api.CheckNodeInGrid(characterPos.PosX + 1, characterPos.PosY))
        {
            points.Add(Grid.api.NodeArray[characterPos.PosX + 1, characterPos.PosY]);
            //Right
            do
            {
                ++indexs[2];
                if (Grid.api.CheckNodeInGrid(characterPos.PosX + indexs[2], characterPos.PosY))
                    points[2] = Grid.api.NodeArray[characterPos.PosX + indexs[2], characterPos.PosY];
                else break;
            } while (points[2].IsWall);
        }

        if (Grid.api.CheckNodeInGrid(characterPos.PosX - 1, characterPos.PosY))
        {
            points.Add(Grid.api.NodeArray[characterPos.PosX - 1, characterPos.PosY]);
            //Left
            do
            {
                ++indexs[3];
                if (Grid.api.CheckNodeInGrid(characterPos.PosX - indexs[3], characterPos.PosY))
                    points[3] = Grid.api.NodeArray[characterPos.PosX - indexs[3], characterPos.PosY];
                else break;
            } while (points[3].IsWall);
        }

        int min = 10000;
        int index = 0;
        foreach (var item in indexs)
        {
            if (!points[indexs.IndexOf(item)].IsWall)
            {
                if (item < min)
                {
                    min = item;
                    index = indexs.IndexOf(item);
                }
            }
        }

        //Debug.Log(points[index].PosX + "_" + points[index].PosY + "Is Wall: " + points[index].IsWall);
        return points[index];
    }


    Node FindNodeDistanceLow(Node characterPos, Node monsterPosition) // tìm node gần nhất trong 8 node xung quanh character/node lân cận character
    {
        if (characterPos.IsWall == false) return characterPos;
        Node result = characterPos;
        float disMin = 100;
        float distance = 0;
        List<Node> NeighborNode = new List<Node>();
        if (Grid.api.CheckNodeInGrid(characterPos.PosX - 1, characterPos.PosY + 1))
            NeighborNode.Add(Grid.api.NodeArray[characterPos.PosX - 1, characterPos.PosY + 1]);// trên bên trái

        if (Grid.api.CheckNodeInGrid(characterPos.PosX, characterPos.PosY + 1))
            NeighborNode.Add(Grid.api.NodeArray[characterPos.PosX, characterPos.PosY + 1]);// trên 

        if (Grid.api.CheckNodeInGrid(characterPos.PosX + 1, characterPos.PosY + 1))
            NeighborNode.Add(Grid.api.NodeArray[characterPos.PosX + 1, characterPos.PosY + 1]);// trên bên phải

        if (Grid.api.CheckNodeInGrid(characterPos.PosX - 1, characterPos.PosY))
            NeighborNode.Add(Grid.api.NodeArray[characterPos.PosX - 1, characterPos.PosY]);// trái

        if (Grid.api.CheckNodeInGrid(characterPos.PosX + 1, characterPos.PosY))
            NeighborNode.Add(Grid.api.NodeArray[characterPos.PosX + 1, characterPos.PosY]);// phải

        if (Grid.api.CheckNodeInGrid(characterPos.PosX - 1, characterPos.PosY - 1))
            NeighborNode.Add(Grid.api.NodeArray[characterPos.PosX - 1, characterPos.PosY - 1]);// dưới bên trái

        if (Grid.api.CheckNodeInGrid(characterPos.PosX, characterPos.PosY - 1))
            NeighborNode.Add(Grid.api.NodeArray[characterPos.PosX, characterPos.PosY - 1]);// dưới

        if (Grid.api.CheckNodeInGrid(characterPos.PosX + 1, characterPos.PosY - 1))
            NeighborNode.Add(Grid.api.NodeArray[characterPos.PosX + 1, characterPos.PosY - 1]);// dưới

        //Debug.Log(NeighborNode.Count);
        foreach (var item in NeighborNode)
        {
            distance = Vector3.Distance(monsterPosition.Position, item.Position);
            if (disMin > distance)
            {
                disMin = distance;
                result = item;
            }
        }
        return result;
    }


    void FindPath(Vector3 monsterPosition, Vector3 characterPosition)
    {

        Node StartNode = Grid.api.NodeFromWorldPoint(monsterPosition);//Gets the node closest to the starting position
        Node TargetNode = Grid.api.NodeFromWorldPoint(characterPosition);//Gets the node closest to the target position

        List<Node> OpenList = new List<Node>();//List of nodes for the open list// Các node mở
        HashSet<Node> ClosedList = new HashSet<Node>();//Hashset of nodes for the closed list// Các node đóng

        OpenList.Add(StartNode);//Add the starting node to the open list to begin the program

        while (OpenList.Count > 0)//Whilst there is something in the open list
        {
            Node CurrentNode = OpenList[0];//Create a node and set it to the first item in the open list
            for (int i = 1; i < OpenList.Count; i++)//Loop through the open list starting from the second object
            {
                if (OpenList[i].FCost < CurrentNode.FCost || OpenList[i].FCost == CurrentNode.FCost && OpenList[i].HCost < CurrentNode.HCost)//If the f cost of that object is less than or equal to the f cost of the current node
                {
                    CurrentNode = OpenList[i];//Set the current node to that object
                }
            }
            OpenList.Remove(CurrentNode);//Remove that from the open list
            ClosedList.Add(CurrentNode);//And add it to the closed list

            if (CurrentNode == TargetNode)//If the current node is the same as the target node
            {
                //Debug.Log("Final Path");
                GetFinalPath(StartNode, TargetNode);//Calculate the final path 
                return;
            }

            List<Node> listNodeProces = new List<Node>();
            if (MoveLine)
                listNodeProces = Grid.api.GetNeighboringNodes(CurrentNode);// đi vuông
            else
                listNodeProces = Grid.api.GetNodeNeighbours(CurrentNode);// đi chéo

            foreach (Node NeighborNode in listNodeProces)//Loop through each neighbor of the current node
            {
                if (NeighborNode.IsWall || ClosedList.Contains(NeighborNode) || NeighborNode.IsWallMove)//If the neighbor is a wall or has already been checked
                {
                    continue;//Skip it
                }
                int MoveCost = CurrentNode.GCost + GetManhattenDistance(CurrentNode, NeighborNode);//Get the F cost of that neighbor

                if (MoveCost < NeighborNode.GCost || !OpenList.Contains(NeighborNode))//If the f cost is greater than the g cost or it is not in the open list
                {
                    NeighborNode.GCost = MoveCost;//Set the g cost to the f cost
                    NeighborNode.HCost = GetManhattenDistance(NeighborNode, TargetNode);//Set the h cost
                    NeighborNode.ParentNode = CurrentNode;//Set the parent of the node for retracing steps

                    if (!OpenList.Contains(NeighborNode))//If the neighbor is not in the openlist
                    {
                        OpenList.Add(NeighborNode);//Add it to the list
                    }
                }
            }
        }
    }



    void GetFinalPath(Node a_StartingNode, Node a_EndNode)
    {
        List<Node> FinalPath = new List<Node>();//List to hold the path sequentially 
        Node CurrentNode = a_EndNode;//Node to store the current node being checked

        while (CurrentNode != a_StartingNode)//While loop to work through each node going through the parents to the beginning of the path
        {
            FinalPath.Add(CurrentNode);//Add that node to the final path
            CurrentNode = CurrentNode.ParentNode;//Move onto its parent node
        }

        FinalPath.Reverse();// đảo danh sách lại cho đúng thứ tự

        PathFinal = FinalPath;
        Grid.api.FinalPath = FinalPath;//Set the final path
    }

    int GetManhattenDistance(Node a_nodeA, Node a_nodeB)
    {
        int ix = Mathf.Abs(a_nodeA.PosX - a_nodeB.PosX);//x1-x2
        int iy = Mathf.Abs(a_nodeA.PosY - a_nodeB.PosY);//y1-y2

        return ix + iy;//Return the sum
    }
    int GetDistance(Node nodeA, Node nodeB)
    {
        int disX = Mathf.Abs(nodeA.PosX - nodeB.PosX);
        int disY = Mathf.Abs(nodeA.PosY - nodeB.PosY);

        if (disX > disY)
            return 14 * disY + 10 * (disX - disY);
        return 14 * disX + 10 * (disY - disX);

    }
}
