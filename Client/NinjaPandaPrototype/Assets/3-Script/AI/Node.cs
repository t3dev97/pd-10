﻿using UnityEngine;

public class Node : IHeapItem<Node>
{

    public int PosX;//X Vị trí trong Node Array
    public int PosY;//Y 

    public bool IsWall;// Node này là tường.
    public bool IsWallMove;// Node này là tường, xuất hiện ở dưới chân của Monster, nhầm để các monster khác tránh.
    public Vector3 Position;//Pos trong thể giới.

    public Node ParentNode;//For the AStar algoritm, will store what node it previously came from so it cn trace the shortest path.

    public int GCost;//The cost of moving to the next square.
    public int HCost;//The distance to the goal from this node..

    int heapIndex;

    public int FCost { get { return GCost + HCost; } }//Quick get function to add G cost and H Cost, and since we'll never need to edit FCost, we dont need a set function.

    public int HeapIndex
    {
        get
        {
            return heapIndex;
        }

        set
        {
            heapIndex = value; 
        }
    }

    public Node(bool isWall, Vector3 pos, int posXInGrid, int posYGrid)//Constructor
    {
        IsWall = isWall;
        Position = pos;
        PosX = posXInGrid;
        PosY = posYGrid;
    }

    public int CompareTo(Node other)
    {
        int compare = FCost.CompareTo(other.FCost);
        if(compare == 0)
        {
            compare = HCost.CompareTo(other.HCost);
        }
        return compare;
    }
}
