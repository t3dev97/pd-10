﻿using System.Collections.Generic;
using UnityEngine;
// for monster
public class BCMoveToCharacter : MonoBehaviour
{
    public float MoveSpeed;
    public float RotateSpeed;
    private bool isWalk = true;
    //public float distance;


    //public Grid GetGrid;
    private Pathfinding pathfinding;
    private List<Node> Path = new List<Node>();
    private float countPath = 0;
    private Vector3 oldTarget = Vector3.zero;
    private int indexCurrent;
    private Rigidbody rigi;
    private Animator anim;
    private BCMonsterDetail detail;
    private GameObject player;
    public bool test = false;
    // Use this for initialization
    private void Awake()
    {
        anim = GetComponent<Animator>();
    }
    public bool IsWalk
    {
        get { return isWalk; }
        set
        {
            isWalk = value;
            if (value == true)
            {
                anim.SetBool("Running", true);
                if (test == false)
                    anim.SetInteger(BCKeyAnimator.Condition, (int)AnimationType.run);
            }
            else
            {
                anim.SetBool("Running", false);
            }
        }
    }


    private Vector3 TargetPos;
    void Start()
    {
        //GetGrid = GameObject.Find("GameManager").gameObject.AddToggleToList<Grid>();
        pathfinding = GetComponent<Pathfinding>();
        indexCurrent = 0;
        rigi = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
        detail = GetComponent<BCMonsterDetail>();
        player = GameObject.FindGameObjectWithTag("Player");
    }
    void FixedUpdate()
    {
        if (IsWalk)
        {
            if (player != null && player.GetComponent<BCCharacterDetail>().GetStatus == Status.die)
            {
                anim.SetInteger(BCKeyAnimator.Condition, (int)AnimationType.idle);
                return;
            }
            //Grid.api.NodeFromWorldPoint(transform.position).IsWall = true;
            Path = pathfinding.PathFinal;
            //Debug.Log(Path.Count);
            if (Path != null)
            {
                if (indexCurrent > Path.Count - 1)
                {
                    if (player != null)
                    {
                        //Debug.Log("In range Attack");
                        Vector3 temp = player.transform.position;
                        temp = new Vector3(temp.x, transform.position.y, temp.z);
                        transform.LookAt(temp);
                        indexCurrent = 0;
                        TargetPos = Vector3.zero;
                    }
                }
                else
                {
                    if (Vector3.Distance(Path[indexCurrent].Position, transform.position) > 0.5f /*&& GetGrid.NodeFromWorldPoint(transform.position).IsWall == false*/)
                    {
                        //Debug.Log("Moving");
                        Vector3 temp = Path[indexCurrent].Position;
                        temp.y = transform.position.y;

                        // Rotate object
                        // Case 1:
                        //temp = new Vector3(temp.x, transform.position.y, temp.z);
                        //transform.LookAt(temp);

                        // Case 2:
                        //float angle = Mathf.Atan2(-temp.z, temp.x) * Mathf.Rad2Deg;
                        //Quaternion rotation = Quaternion.AngleAxis(angle, Vector3.up);
                        //transform.rotation = Quaternion.Lerp(transform.rotation, rotation, Time.deltaTime * 10);

                        // Case 3:

                        var rotation = Quaternion.LookRotation(temp - transform.position);
                        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.fixedDeltaTime * RotateSpeed);
                        if (anim.GetBool("Running") == true)
                        {
                            anim.SetInteger(BCKeyAnimator.Condition, (int)AnimationType.run);
                            transform.position = Vector3.Lerp(transform.position, temp, MoveSpeed * Time.fixedDeltaTime);
                            //rigi.velocity = (temp - transform.position) * MoveSpeed;
                        }
                        //else
                        //{
                        //    anim.SetInteger(BCKeyAnimator.Condition, (int)AnimationType.run);
                        //    transform.position = Vector3.Lerp(transform.position, temp, MoveSpeed / 3 * Time.fixedDeltaTime);
                        //}
                        //transform.position = temp;
                        //rigi.AddForce((temp - transform.position) * Speed);
                    }
                    else
                    {
                        //Debug.Log("qwe");
                        indexCurrent += 1;
                    }

                }
            }
            else
            {
                indexCurrent = 0;
            }
        }
    }
}
