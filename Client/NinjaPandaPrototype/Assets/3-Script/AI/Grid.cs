﻿using System.Collections.Generic;
using UnityEngine;

public class Grid : MonoBehaviour
{
    static public Grid api;

    public bool ShowGrid = true;

    //public Transform StartPosition;//This is where the program will start the pathfinding from.
    public LayerMask WallMask;//This is the mask that the program will look for when trying to find obstructions to the path.
    public LayerMask WallMoveMask;//This is the mask that the program will look for when trying to find obstructions to the path.
    public Vector2 GridWorldSize;//A vector2 to store the width and height of the graph in world units.
    public float NodeRadius;//This stores how big each square on the graph will be
    public float DistanceBetweenNodes;//The distance that the squares will spawn from eachother.

    public Node[,] NodeArray;//The array of nodes that the A Star algorithm uses.
    public List<Node> FinalPath;//The completed path that the red line will be drawn along


    float nodeDiameter;//Twice the amount of the radius (Set in the start function) -> đường kính
    int iGridSizeX, iGridSizeY;//Size of the Grid in Array units.
    private GameObject plane;

    private void Awake()
    {
        if (api == null)
            api = this;
        else
            Destroy(this);
    }
    public void setDataDetail(Vector2 girdsize)
    {
        WallMask = LayerMask.GetMask("Wall");
        GridWorldSize = girdsize;
        NodeRadius = 0.25f;

    }
    private void Start()//Ran once the program starts
    {
        nodeDiameter = NodeRadius * 2;//Double the radius to get diameter
        iGridSizeX = Mathf.RoundToInt(GridWorldSize.x / nodeDiameter);//Divide the grids world co-ordinates by the diameter to get the size of the graph in array units. // so o
        iGridSizeY = Mathf.RoundToInt(GridWorldSize.y / nodeDiameter);//Divide the grids world co-ordinates by the diameter to get the size of the graph in array units.
        //plane = transform.GetChild(0).gameObject;

        CreateGrid();//Draw the grid

    }
    private void Update()
    {
        //CreateGrid();
    }

    private void UpdateGrid()
    {

        Vector3 bottomLeft = transform.position - Vector3.right * GridWorldSize.x / 2 - Vector3.forward * GridWorldSize.y / 2;//Get the real world position of the bottom left of the grid.
        for (int x = 0; x < iGridSizeX; x++)//Loop through the array of nodes.
        {
            for (int y = 0; y < iGridSizeY; y++)//Loop through the array of nodes
            {
                Vector3 worldPoint = bottomLeft + Vector3.right * (x * nodeDiameter + NodeRadius) + Vector3.forward * (y * nodeDiameter + NodeRadius);//Get the world co ordinates of the bottom left of the graph
                bool WallMove = false;//Make the node a wall

                //If the node is not being obstructed
                //Quick collision check against the current node and anything in the world at its position. If it is colliding with an object with a WallMask,
                //The if statement will return false.
                //Debug.Log("i :"+x + "y: "+y+ worldPoint);
                if (Physics.CheckSphere(worldPoint, NodeRadius, WallMask))
                {
                    WallMove = true;//Object is not a wall
                   //Debug.LogError(worldPoint);
                }
                NodeArray[x, y].IsWallMove = WallMove;
            }
        }
    }

    public void CreateGrid()
    {
        NodeArray = new Node[iGridSizeX, iGridSizeY];//Declare the array of nodes.
        Vector3 bottomLeft = transform.position - Vector3.right * GridWorldSize.x / 2 - Vector3.forward * GridWorldSize.y / 2;//Get the real world position of the bottom left of the grid.
        for (int x = 0; x < iGridSizeX; x++)//Loop through the array of nodes.
        {
            for (int y = 0; y < iGridSizeY; y++)//Loop through the array of nodes
            {
                Vector3 worldPoint = (bottomLeft + Vector3.right * (x * nodeDiameter + NodeRadius) + Vector3.forward * (y * nodeDiameter + NodeRadius));//Get the world co ordinates of the bottom left of the graph
                bool Wall = false;//Make the node a wall

                //If the node is not being obstructed
                //Quick collision check against the current node and anything in the world at its position. If it is colliding with an object with a WallMask,
                //The if statement will return false.
                //Debug.Log("i :"+x + "y: "+y+ worldPoint);
                if (Physics.CheckSphere(worldPoint, NodeRadius, WallMask))
                {
                    Wall = true;//Object is not a wall
                    //Debug.LogError(worldPoint);
                }
                NodeArray[x, y] = new Node(Wall, worldPoint, x, y);//Create a new node in the array.
            }
        }
    }

    //Function that gets the neighboring nodes of the given node.
    public List<Node> GetNeighboringNodes(Node a_NeighborNode) // đi vuông
    {
        List<Node> NeighborList = new List<Node>();//Make a new list of all available neighbors.
        int icheckX;//Variable to check if the XPosition is within range of the node array to avoid out of range errors.
        int icheckY;//Variable to check if the YPosition is within range of the node array to avoid out of range errors.

        //Check the right side of the current node.
        icheckX = a_NeighborNode.PosX + 1;
        icheckY = a_NeighborNode.PosY;
        if (icheckX >= 0 && icheckX < iGridSizeX)//If the XPosition is in range of the array
        {
            if (icheckY >= 0 && icheckY < iGridSizeY)//If the YPosition is in range of the array
            {
                NeighborList.Add(NodeArray[icheckX, icheckY]);//Add the grid to the available neighbors list
            }
        }
        //Check the Left side of the current node.
        icheckX = a_NeighborNode.PosX - 1;
        icheckY = a_NeighborNode.PosY;
        if (icheckX >= 0 && icheckX < iGridSizeX)//If the XPosition is in range of the array
        {
            if (icheckY >= 0 && icheckY < iGridSizeY)//If the YPosition is in range of the array
            {
                NeighborList.Add(NodeArray[icheckX, icheckY]);//Add the grid to the available neighbors list
            }
        }
        //Check the Top side of the current node.
        icheckX = a_NeighborNode.PosX;
        icheckY = a_NeighborNode.PosY + 1;
        if (icheckX >= 0 && icheckX < iGridSizeX)//If the XPosition is in range of the array
        {
            if (icheckY >= 0 && icheckY < iGridSizeY)//If the YPosition is in range of the array
            {
                NeighborList.Add(NodeArray[icheckX, icheckY]);//Add the grid to the available neighbors list
            }
        }
        //Check the Bottom side of the current node.
        icheckX = a_NeighborNode.PosX;
        icheckY = a_NeighborNode.PosY - 1;
        if (icheckX >= 0 && icheckX < iGridSizeX)//If the XPosition is in range of the array
        {
            if (icheckY >= 0 && icheckY < iGridSizeY)//If the YPosition is in range of the array
            {
                NeighborList.Add(NodeArray[icheckX, icheckY]);//Add the grid to the available neighbors list
            }
        }
        return NeighborList;//Return the neighbors list.
    }

    public List<Node> GetNodeNeighbours(Node node) // đi cheo
    {
        List<Node> listNodeNeighbours = new List<Node>();
        for (int x = -1; x <= 1; x++)
        {
            for (int y = -1; y <= 1; y++)
            {
                if (x == 0 && y == 0)
                    continue;
                int checkX = node.PosX + x;
                int checkY = node.PosY + y;

                if (checkX >= 0 && checkX < iGridSizeX && checkY >= 0 && checkY < iGridSizeY)
                {
                    listNodeNeighbours.Add(NodeArray[checkX, checkY]);
                }
            }
        }
        return listNodeNeighbours;
    }

    //Gets the closest node to the given world position.
    public Node NodeFromWorldPoint(Vector3 a_vWorldPos)
    {
        float ixPos = ((a_vWorldPos.x + GridWorldSize.x / 2) / GridWorldSize.x);
        float iyPos = ((a_vWorldPos.z + GridWorldSize.y / 2) / GridWorldSize.y);

        ixPos = Mathf.Clamp01(ixPos);
        iyPos = Mathf.Clamp01(iyPos);

        int ix = Mathf.RoundToInt((iGridSizeX - 1) * ixPos);
        int iy = Mathf.RoundToInt((iGridSizeY - 1) * iyPos);

        return NodeArray[ix, iy];
    }
    public bool CheckNodeInGrid(int x, int y, int temp = 0)// temp là hệ số lệch vào trong 
    {
        if (x < temp || x >= iGridSizeX - temp || y < temp || y >= iGridSizeY - temp) return false;
        return true;
    }


    private void OnDrawGizmos()
    {

        Gizmos.DrawWireCube(transform.position, new Vector3(GridWorldSize.x, 1, GridWorldSize.y));
        if (ShowGrid)
        {

            //UpdateGrid();

            if (NodeArray != null)//If the grid is not empty
            {

                foreach (Node n in NodeArray)//Loop through every node in the grid
                {

                    if (n.IsWall)//If the current node is a wall node
                    {
                        Gizmos.color = Color.yellow;//Set the color of the node
                    }
                    else
                    {
                        Gizmos.color = Color.white;//Set the color of the node
                    }
                    if (FinalPath != null)//If the final path is not empty
                    {
                        if (FinalPath.Contains(n))//If the current node is in the final path
                        {
                            Gizmos.color = Color.red;//Set the color of that node
                        }
                    }
                    Gizmos.DrawCube(n.Position, Vector3.one * (nodeDiameter - DistanceBetweenNodes));//Draw the node at the position of the node.
                }
            }
        }
    }
}
