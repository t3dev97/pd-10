﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class BCScrollFix : ScrollRect
{
    public enum TypeScrollCustom
    {
        scrollScale = 1,
        scrollSize = 2
    }
    public float fCurrentLayot;
    public int CurrentLayout;
    public TypeScrollCustom stype;
    public Vector3 scale;
    public float speed;
    public bool isEdit;
    [SerializeField]
    public List<GameObject> listGameObject;
    public bool isPlay = true;
    private int LastCurrentLayout;
    public float offet;
    private int childCountContent=0;
    public float delay=1;
    private bool isDrag;

    private Vector3 posContent;
    private Vector3 vec;
    private Vector3 posContentUpdate;
    private Vector3 lastPosContentUpdate;
    private Vector3 scaleDefault;
    private bool isLoad = false;

    public void Start()
    {
        listGameObject = new List<GameObject>();
       StartCoroutine( AddChildFromListObject(()=> 
        {
            offet = listGameObject[0].GetComponent<RectTransform>().rect.width + content.GetComponent<GridLayoutGroup>().spacing.x;
            scaleDefault = listGameObject[0].transform.localScale;
            isLoad = true;
        }));
        posContent = content.transform.localPosition;

        LastCurrentLayout = CurrentLayout;
        posContentUpdate = lastPosContentUpdate = content.position;
        isPlay = true;

    }
    public void Update()
    {
        if (isPlay)
        {
            fCurrentLayot = ((((posContent.x - content.transform.localPosition.x)) / offet) + 1);
            posContentUpdate = content.transform.position;

            if (isEdit)
            {
                LastCurrentLayout = CurrentLayout;
            }
            else
            {
                if (LastCurrentLayout > CurrentLayout)
                {
                    if (posContentUpdate.x > lastPosContentUpdate.x)
                    {
                        CurrentLayout = Mathf.RoundToInt(fCurrentLayot);
                    }
                }
                else if (LastCurrentLayout < CurrentLayout)
                {
                    if (posContentUpdate.x < lastPosContentUpdate.x)
                    {
                        CurrentLayout = Mathf.RoundToInt(fCurrentLayot);
                    }
                }
                else
                {

                }
            }

            if (childCountContent != content.childCount)
            {
                childCountContent = content.childCount;
            }

            lastPosContentUpdate = posContentUpdate;
        }
    }
    public void LateUpdate()
    {
        if (isPlay)
        {
        base.LateUpdate();

        if (!isDrag)
        {
            Vector3 vec = new Vector3(posContent.x - ((CurrentLayout*1.0f - 1) * offet), posContent.y, posContent.z);
            content.localPosition = Vector3.MoveTowards(content.localPosition, vec, speed * Time.deltaTime);
        }
      
        if (isLoad)
        {
            float ScaleNew = (fCurrentLayot % 1);

            if (fCurrentLayot > CurrentLayout)
            {
                float scaleNext = (scale.x * ScaleNew) / delay ;
               
                if (scaleDefault.x + scaleNext <= scale.x)
                {
                    if (CurrentLayout<childCountContent && CurrentLayout >= 0)
                        listGameObject[CurrentLayout].transform.localScale = new Vector3(scaleDefault.x + scaleNext, scaleDefault.y + scaleNext, scaleDefault.z);
                    if (CurrentLayout >0)
                        listGameObject[CurrentLayout-1].transform.localScale = new Vector3(scale.x - scaleNext, scale.y - scaleNext, scaleDefault.z);

                }
            }
            else if (fCurrentLayot < CurrentLayout)
            {
                float scaleNext = (scale.x * (1- ScaleNew))/delay;
                if (scaleDefault.x + scaleNext <= scale.x)
                {
                    if (CurrentLayout > 0 && CurrentLayout <= childCountContent)
                        listGameObject[CurrentLayout - 1].transform.localScale = new Vector3(scale.x - scaleNext, scale.y - scaleNext, scaleDefault.z);
                    if (CurrentLayout > 1)
                        listGameObject[CurrentLayout - 2].transform.localScale = new Vector3(scaleDefault.x + scaleNext, scaleDefault.y + scaleNext, scaleDefault.z);

                }

            }
            
            else
            {
                if(CurrentLayout < childCountContent && CurrentLayout > 0)
                   listGameObject[CurrentLayout-1].transform.localScale = new Vector3(scale.x , scale.y , scaleDefault.z);
                if(CurrentLayout<childCountContent)
                  listGameObject[CurrentLayout].transform.localScale = new Vector3(scaleDefault.x, scaleDefault.y, scaleDefault.z);
                if(CurrentLayout>1)
                  listGameObject[CurrentLayout-2].transform.localScale = new Vector3(scaleDefault.x, scaleDefault.y, scaleDefault.z);
            }
            }
        }
    }


    public override void OnBeginDrag(PointerEventData eventData)
    {
        base.OnBeginDrag(eventData);
        LastCurrentLayout = CurrentLayout;
  
    }
    public override void OnDrag(PointerEventData eventData)
    {
        CurrentLayout = Mathf.RoundToInt(fCurrentLayot);
        base.OnDrag(eventData);
        isDrag = true;
    }
    public override void OnEndDrag(PointerEventData eventData)
    {
        base.OnEndDrag(eventData);
        CurrentLayout = Mathf.RoundToInt(fCurrentLayot);
        isDrag = false;
    }

    IEnumerator AddChildFromListObject(Action action)
    {
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();
        while (content.childCount == 0)
        {
            yield return new WaitForEndOfFrame();
        }
        foreach (Transform tg in content)
        {
            listGameObject.Add(tg.gameObject);
        }

        yield return new WaitForEndOfFrame();

        //Debug.Log(listGameObject.Count);
        action.Invoke();
    }
}
