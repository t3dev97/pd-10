﻿using Studio.BC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



public class ScrollRecFix : ScrollRect
{
    enum StypeScroll
    {
        ScrollMain=1
    }
    public int size;
   

    override protected void LateUpdate()
    {

        base.LateUpdate();

        float size = (((ScreenSize.MaxWidth * 1.0f) / this.content.childCount) + (((100 * 1.0f) / this.content.childCount) * (this.content.childCount - 1))) / ScreenSize.MaxWidth;

        if (this.horizontalScrollbar != null)
        {
            this.horizontalScrollbar.size = size;
        }
      
    }

    override public void Rebuild(CanvasUpdate executing)
    {
        base.Rebuild(executing);
        float size = (((ScreenSize.MaxWidth * 1.0f) / this.content.childCount) + (((100 * 1.0f) / this.content.childCount) * (this.content.childCount - 1))) / ScreenSize.MaxWidth;

        if (this.horizontalScrollbar != null)
        {
            this.horizontalScrollbar.size = size;
        }
    }

}
