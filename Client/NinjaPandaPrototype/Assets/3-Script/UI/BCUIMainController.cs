﻿using Studio;
using Studio.BC;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class BCUIMainController : MonoBehaviour
{
  
    public static BCUIMainController api;

    public GameObject Popupopentchest;
    public GameObject PopupupdateTalen;
    public GameObject PopupUpdateEquipment;
    public GameObject PopupIsUseEquipment;
    public GameObject PopupNewEquipment;
    public GameObject PopupAchievement;

    public GridLayoutGroup gridLayoutCenter;

    public Transform parentContaintPopup;
    public Transform transformUIBot;
    public Transform transformEffect;

    [SerializeField]
    private List<Toggle> ListToggle;
    [SerializeField]
    private List<Transform> ListCenterUI;

    public List<int> ListLock;
    
    public BCScrollCustom bcScrollCustom;
    public BCUITopView uiTop;

    public BCEquipmentController equipment;
    public BCShopController shop;
    public BCTalentController talent;
    public BCWorldController world;

    public float scaleHeight;
    public int intChose = 3;
    public int PaddingChose = 50;

    public void Awake()
    {

        if (!BCConfig.Api.IsFirstInit)
        {
            SceneManager.LoadScene(NameScene.BCLoading, LoadSceneMode.Single);
            return;
        }

        if (api == null)
            api = this;
        else if (api != this)
            Destroy(this);

        BCDataControler.api.UpdateData();
        AddToggleToList();
        InitToggle();
        ScaleSizeLayout();
        UpdateAllUI();
        BCConfig.Api.CurrentScene = CurrentSceneName.LayoutDemo;

    }

    public void ShowPopupNotify(EnumPopupMenu type)
    {
        //HidePopupNotify();
        switch (type)
        {
            case EnumPopupMenu.openchest:      Popupopentchest.SetActive(true);break;
            case EnumPopupMenu.updateTalen:    PopupupdateTalen.SetActive(true); break;
            case EnumPopupMenu.IsUseEquipment: PopupIsUseEquipment.SetActive(true); break;
            case EnumPopupMenu.NewEquipment:   PopupNewEquipment.SetActive(true); break;
            case EnumPopupMenu.UpdateEquipment: PopupUpdateEquipment.SetActive(true); break;
            case EnumPopupMenu.ISUseAchievement: PopupAchievement.SetActive(true); break;
        }
    }

    public IEnumerator DelayOffLoading()
    {
        yield return new WaitForSeconds(0.5f);
        //LoadingObject.SetActive(false);
    }

    public void ShowPopup(string txt)
    {

    }

    

    public void TurnOffPopupNotify(EnumPopupMenu type)
    {
        //HidePopupNotify();
        switch (type)
        {
            case EnumPopupMenu.openchest:       Popupopentchest.SetActive(false); break;
            case EnumPopupMenu.updateTalen:     PopupupdateTalen.SetActive(false); break;
            case EnumPopupMenu.IsUseEquipment:  PopupIsUseEquipment.SetActive(false); break;
            case EnumPopupMenu.NewEquipment:    PopupNewEquipment.SetActive(false); break;
            case EnumPopupMenu.UpdateEquipment: PopupUpdateEquipment.SetActive(false); break;
            case EnumPopupMenu.ISUseAchievement: PopupAchievement.SetActive(true); break;
        }
    }
    public void HidePopupNotify()
    {
      Popupopentchest.SetActive(false);
      PopupupdateTalen.SetActive(false);
      PopupUpdateEquipment.SetActive(false);
      PopupIsUseEquipment.SetActive(false);
      PopupNewEquipment.SetActive(false);
      PopupAchievement.SetActive(false);
    }
    public void ScaleSizeLayout()
    {
        int width = Camera.main.pixelWidth;
        int height = Camera.main.pixelHeight;
        float fill = (width * 1.0f) / height;
        scaleHeight = ((ScreenSize.MaxWidth * 1.0f) / fill) / ScreenSize.MaxHeight;
        gridLayoutCenter.cellSize = new Vector2(ScreenSize.MaxWidth, (ScreenSize.MaxWidth * 1.0f) / fill);
    }
 
    public void AddToggleToList()
    {
        foreach (Transform tg in transformUIBot)
        {
            ListToggle.Add(tg.GetComponent<Toggle>());
            //ListObjectChose.Add(tg.GetComponent<Toggle>().graphic.gameObject);
        }
        if (ListCenterUI == null)
            ListCenterUI = new List<Transform>();
        foreach (Transform centerUI in gridLayoutCenter.transform)
        {
            ListCenterUI.Add(centerUI);
        }
    }
  
    public void InitToggle()
    {
        TurnOffAllToggle();
        setChoseToggen(ListToggle[intChose-1]);
        ToggleValueChanged(ListToggle[intChose - 1]);
        int index = 1;
        foreach (Toggle tg in ListToggle)
        {
            if (!ListLock.Contains(index))
            {
                tg.onValueChanged.RemoveAllListeners();
                tg.onValueChanged.AddListener((value) => ToggleValueChanged(tg));
           
                tg.transform.Find("Fader").gameObject.SetActive(false);
            }
            else
                tg.transform.Find("Fader").gameObject.SetActive(true);

            index++;
        }
        for (int i = 0; i < ListCenterUI.Count;i++)
        {
            if (ListLock != null && ListLock.Contains (i+1))
            {
                foreach (Transform child in ListCenterUI[i])
                    child.gameObject.SetActive(false);
            }
            else
            {
                foreach (Transform child in ListCenterUI[i])
                    child.gameObject.SetActive(true);
            }
        }
     
    }

    public void UpdateAllUI()
    {
        gameObject.SetActive(true);

        uiTop.updataUITop();
        equipment.UpdateEquipment();
        shop.updateShop();
        talent.UpdateViewTalent();
        world.UpdateViewWorld();
        //choseMap.UpdateViewChoseMap();
    }
    public void Update()
    {
        if (!bcScrollCustom.isDrag)
        {
            if (ListLock.Contains(bcScrollCustom.currenChap))
            {

                if (intChose - bcScrollCustom.currenChap > 0)
                {
                    intChose = bcScrollCustom.currenChap;
                    if (intChose == 0)
                        intChose++;
                    else
                        intChose--;
                }
                else
                {
                    intChose = bcScrollCustom.currenChap;
                    if (intChose == ListToggle.Count)
                    {
                        intChose--;
                    }

                    else
                        intChose++;
                }
                ToggleValueChanged(ListToggle[intChose-1]);
            }
            else
            {
                intChose = bcScrollCustom.currenChap;
            }
            setChoseToggen(ListToggle[intChose - 1]);
        }
    }

    public void setChoseToggen(Toggle toggle)
    {

        toggle.isOn = true;
        toggle.gameObject.GetComponent<LayoutElement>().preferredWidth = PaddingChose;


    }



    public void ToggleValueChanged(Toggle isChose)
    {
        int currenchap = bcScrollCustom.currenChap;

            if (isChose.isOn)
            {
                TurnOffAllToggle();
                switch (isChose.name)
                {
                    case UISceneName.Shop: /*DebugX.Log("Chose : " + isChose.name);*/ bcScrollCustom.currenChap = 1; break;
                    case UISceneName.Equipment: /*DebugX.Log("Chose : " + isChose.name);*/ bcScrollCustom.currenChap = 2; break;
                    case UISceneName.World: /*DebugX.Log("Chose : " + isChose.name);*/ bcScrollCustom.currenChap = 3; break;
                    case UISceneName.Talents: /*DebugX.Log("Chose : " + isChose.name);*/ bcScrollCustom.currenChap = 4; break;
                    case UISceneName.Setting: /*DebugX.Log("Chose : " + isChose.name);*/ bcScrollCustom.currenChap = 5; break;
                }
                if (currenchap != bcScrollCustom.currenChap)
                    bcScrollCustom.speed = bcScrollCustom.currentspeed * (Mathf.Abs(currenchap - bcScrollCustom.currenChap));

                isChose.gameObject.GetComponent<BCUIBotItemView>().AniTurnOn();
                setChoseToggen(isChose);
            isChose.interactable = false;
            }
            else
            {
            isChose.interactable = true;
            isChose.gameObject.GetComponent<BCUIBotItemView>().AniTurnOFF();
            }
       
      

    }
    public void ToggleValueChanged(Lobby_Position pos)
    {
        switch (pos)
        {
            case Lobby_Position.SHOP: bcScrollCustom.currenChap = 1; break;
            case Lobby_Position.INVENTORY: bcScrollCustom.currenChap = 2; break;

        }
    }

    public void UpdateSizeToggle(Toggle toggle)
    {
        ResizeAllToggle();
    }
    public void ResizeAllToggle()
    {
        foreach (Toggle tg in ListToggle)
        {
            Vector2 width = (tg.gameObject.GetComponent<RectTransform>()).sizeDelta;
          
            float width1 = width.x - PaddingChose / (ListToggle.Count - 1);
            tg.gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(width1, width.y);
        }
    }
    public void TurnOffAllToggle()
    {
        foreach (Toggle tg in ListToggle)
        {
            tg.gameObject.GetComponent<LayoutElement>().preferredWidth = 0;
        }
    }

 
    public void ChoseMap(int vt)
    {
        world.chapCurrent = vt;
        world.UpdateViewWorld();
    }
    public void HideChoseMap(string name)
    {
        GameObject.Find(name).SetActive(false);
    }
}
