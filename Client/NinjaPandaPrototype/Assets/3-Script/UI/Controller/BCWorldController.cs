﻿using Studio.BC;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class BCWorldController : MonoBehaviour
{
    public int chapCurrent;
    public Button btnPlay;
    public BCMapController map;
    public Button btnShowChoseMap;
    public Button btnShowAchivement;

    public void UpdateViewWorld()
    {
        btnPlay.onClick.AddListener(() => OnButtonClickPlay());
        btnShowChoseMap.onClick.AddListener(() => OnButtonClickChose());
        btnShowAchivement.onClick.AddListener(() => OnButtonClickAchivement());

        map.parse(BCCache.Api.DataConfig.MapConfigData[chapCurrent]);
    }
    public void OnButtonClickPlay()
    {
        BCDataControler.api.MinusEnergy(BCCache.Api.DataConfig.ConfigGameData.EnergyInStage, () =>
        {
            GameManager.api.GameState = GAME_STATE.Playing;
            SceneManager.LoadScene(NameScene.Main, LoadSceneMode.Single);
        });

    }

    public void OnButtonClickChose()
    {
        BCViewHelper.Api.Popup.Show(StateName.ChoseMap, (go) =>
        {
            //BCChonseMapController mapController = go.GetComponent<BCChonseMapController>();
            //mapController.UpdateViewChoseMap();
        }, true, null, true, false);
    }

    public void OnButtonClickAchivement()
    {
        BCViewHelper.Api.Popup.Show(StateName.Achivement, (go) =>
        {
            BCAchimentController mapController = go.GetComponent<BCAchimentController>();
            //mapController.();

        }, true, null, true, false);
    }
}
