﻿using Studio;
using Studio.BC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BCAchimentController : BaseView
{
    // Start is called before the first frame update
    public GameObject AchivementPrefab;
    public BCScrollFix scroll;
    public GridLayoutGroup group; 
    public Transform trContain;
    public Button btnClaim;
    public Button btnBack;
    public Text2 nameAchiment;
    public Transform trListReward;
     
    public void Awake()
    {
        DestroyAll();
        NextAchivment();
        group.constraintCount = BCCache.Api.DataConfig.AchivementConfigData.Count;
        BCCache.Api.GetUIPrefabAsync(UIPrefabName.AchivementSlot, (go) =>
        {
            foreach (var tg in BCCache.Api.DataConfig.AchivementConfigData)
            {
                GameObject game = Instantiate(go, trContain, false);
                game.GetComponent<BCAchimentView>().setData(tg.Value);

            }
        });


        btnClaim.onClick.AddListener(onClickClaim);
        btnBack.onClick.AddListener(onClickBack);

    }

    public void onClickClaim()
    {
        BCAchivement item = BCCache.Api.DataConfig.AchivementConfigData[scroll.CurrentLayout];

        foreach (var tg in BCCache.Api.DataConfig.SutieCommonConfigData[item.Suite_Common_ID].listRewards)
        {
           BCDataControler.api.AddResource(tg.Key, tg.value);
        }

        NextAchivment();
    }


    public void onClickBack()
    {
        BCViewHelper.Api.Popup.Hide(StateName.Achivement);
    }

    public void NextAchivment()
    {
        scroll.CurrentLayout++;
        if (scroll.CurrentLayout <= BCCache.Api.DataConfig.AchivementConfigData.Count) { 
             destroyReward();
            
       
            BCAchivement item = BCCache.Api.DataConfig.AchivementConfigData[scroll.CurrentLayout];
            nameAchiment.text = LanguageManager.api.GetKey(BCLocalize.GUI_NAME_REACH_STAGE_CHAPTER
                ,new string[] { item.Stage.ToString(),item.Chapter.ToString()});


            BCCache.Api.GetUIPrefabAsync(UIPrefabName.Rewart, (go) =>
            {
                foreach (var tg in BCCache.Api.DataConfig.SutieCommonConfigData[item.Suite_Common_ID].listRewards)
                {
                    GameObject game = Instantiate(go, trListReward, false);
                    game.GetComponent<BCItemRewardView>().setData(tg.asset,tg.value.ToString());

                }
            });
        }
        else
        {
            btnClaim.interactable = false;
        }

    }
    public void destroyReward()
    {
        foreach (Transform tg in trListReward)
        {
            Destroy(tg.gameObject);
        }
    }
    public void DestroyAll()
    {
        foreach(Transform tg in trContain)
        {
            Destroy(tg.gameObject);
        }
        destroyReward();
    }
}
