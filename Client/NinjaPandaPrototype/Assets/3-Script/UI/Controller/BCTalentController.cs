﻿using Studio.BC;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class BCTalentController : MonoBehaviour
{
    public int currentTalent = 0;
    public float speedAnimation;

    public Dictionary<int, BCUITalentView> listTalent;
    public CharacterAsset assetTalent;
    public List<GameObject> talentObjects;
    public int IDMin = 0;
    public int IDMax = 0;
    public List<int> listIDRandom;
    public Transform trListTalents;
    public GameObject popupTalentCurrent;
    public BCPopupTalentView TalentDesc;
    public Button btnUpdate;
    public Button BackGroundBtn;

    public void UpdateViewTalent()
    {

        listTalent = new Dictionary<int, BCUITalentView>();
        listIDRandom = new List<int>();
        btnUpdate.onClick.RemoveAllListeners();
        btnUpdate.onClick.AddListener(OnClickButtonUpdate);
        assetTalent = Resources.Load<CharacterAsset>("ModifyTool/Player/PLAYERTALENT");
        popupTalentCurrent = null;
        talentObjects = new List<GameObject>();
        DestroyAll();
        ADDViewTalent();
        SetData();
    }

    public void SetData()
    {
        BackGroundBtn.AddClickListener(OnBackGroundBtnClick);
        for (int i = 1; i < BCCache.Api.DataConfig.TalenConfigData.Count; i++)
        {
            listIDRandom.Add(i);
        }
    }
    private void OnBackGroundBtnClick()
    {
        TalentDesc.gameObject.SetActive(false);
    }

    public void OnClickButtonUpdate()
    {
        HidePopupTalent();
        btnUpdate.interactable = false;
        if (currentTalent == 0)
        {
            currentTalent = 9;
        }
        else
        {
            currentTalent = UnityEngine.Random.Range(1, trListTalents.childCount);
            Debug.Log("trListTalents.childCount " + trListTalents.childCount);
            Debug.Log("currentTalent " + currentTalent);
        }
        StartCoroutine(DelayAnimation(speedAnimation, () =>
        {
            btnUpdate.interactable = true;
            //OpenUpgradeFX
            BCViewHelper.Api.PopupNotify.Show(StateName.OpenUpgradeFX);

        }));
    }

    public void ShowPopupTalent(BCUITalentView talent)
    {
        TalentDesc.Parse(talent);
        TalentDesc.gameObject.SetActive(true);
    }

    IEnumerator DelayAnimation(float time, Action action)
    {
        Dictionary<int, BCUITalentView> dic = listTalent;
        List<int> listIndex = new List<int>();
        for (int i = 1; i < trListTalents.childCount; i++)
        {
            listIndex.Add(i);
        }

        while (listIndex.Count > 0)
        {
            int index = UnityEngine.Random.Range(1, listIndex.Count);
            int vt = listIndex[index - 1];

            StartCoroutine(dic[vt].AnimationTalentChose(time));
            listIndex.RemoveAt(index - 1);

            yield return new WaitForSeconds(time);
        }

        for (int i = 0; i < 3; i++)
        {
            StartCoroutine(listTalent[currentTalent].AnimationTalentChose(time));
            yield return new WaitForSeconds(time);
        }

        BCCache.Api.DataConfig.ResourceGame.listTalents[currentTalent].Level++;
        BCTalents talent = BCCache.Api.DataConfig.ResourceGame.listTalents[currentTalent];
        switch (currentTalent)
        {
            case (int)EnumTalentKey.MaxHp:
                assetTalent.BasicHP = talent.talent.Default + ((talent.Level - 1) * talent.talent.Bounus);
                break;
            case (int)EnumTalentKey.ATK:
                assetTalent.BasicATKDamage = talent.talent.Default + ((talent.Level - 1) * talent.talent.Bounus);
                break;
            case (int)EnumTalentKey.Recover:
                assetTalent.BasicIncreaseHPFromHealt = talent.talent.Default + ((talent.Level - 1) * talent.talent.Bounus);
                break;
            case (int)EnumTalentKey.Shield:
                assetTalent.BasicDamageResistance = talent.talent.Default + ((talent.Level - 1) * talent.talent.Bounus);
                break;
            case (int)EnumTalentKey.Agile:
                assetTalent.BasicAS = talent.talent.Default + ((talent.Level - 1) * talent.talent.Bounus);
                break;
            case (int)EnumTalentKey.Inspire:
                assetTalent.BasicIncreaseHPFromUpLevel = talent.talent.Default + ((talent.Level - 1) * talent.talent.Bounus);
                break;
            case (int)EnumTalentKey.Enhance:
                assetTalent.BasicEnhance = talent.talent.Default + ((talent.Level - 1) * talent.talent.Bounus);
                break;
            case (int)EnumTalentKey.TimeReward:
                assetTalent.BasicBounusExp = talent.talent.Default + ((talent.Level - 1) * talent.talent.Bounus);
                break;
            case (int)EnumTalentKey.Glory:
                assetTalent.BasicGlory = true;
                break;
        }

        if (BCCache.Api.DataConfig.ResourceGame.listTalents[currentTalent].Level > IDMax)
        {
            IDMax = BCCache.Api.DataConfig.ResourceGame.listTalents[currentTalent].Level;
        }
      



        UpdateViewTalent();
        action.Invoke();
    }

    public void DestroyAll()
    {
        foreach (Transform tr in trListTalents)
        {
            //talentObjects.Add(tr.gameObject);

            Destroy(tr.gameObject);
        }
    }
    public void ADDViewTalent()
    {
        Dictionary<int, TalentConfigData> listChest = BCCache.Api.DataConfig.TalenConfigData;
        int index = 1;
        listTalent.Clear();
        foreach (var dataChest in listChest)
        {
            BCCache.Api.GetUIPrefabAsync(UIPrefabName.TalentItemButton, (go) =>
            {
                var game = Instantiate(go, trListTalents);
                BCUITalentView view = game.GetComponent<BCUITalentView>();
                view.Parse(dataChest.Value, this);
                listTalent.Add(index, view);
                index++;
            });
        }
        //foreach (var dataChest in listChest)
        //{

        //        var game = talentObjects[index-1];
        //        game.GetComponent<BCUITalentView>().Parse(dataChest.Value);
        //        listTalent.Add(index, game.GetComponent<BCUITalentView>());
        //        index++;

        //}
    }

    public void HidePopupTalent()
    {
        if (popupTalentCurrent != null)
        {
            popupTalentCurrent.SetActive(false);
        }
    }
}
