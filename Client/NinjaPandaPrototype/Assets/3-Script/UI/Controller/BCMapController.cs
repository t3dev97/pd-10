﻿using Studio;
using Studio.BC;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BCMapController : MonoBehaviour
{
    public Text2 numStage;
    public Text2 dec;
    public BCDynamicImage asset;
    public float timeDelayShow;

    public void parse(BCMapData data)
    {
        //asset.gameObject.GetComponent<ImageUIHelper>().SetSprite(data.nameAsset);
        asset.gameObject.GetComponent<BCDynamicImage>().Type = EnumDynamicImageType.World;
        asset.gameObject.GetComponent<BCDynamicImage>().SetImage(data.nameAsset);
        numStage.text = LanguageManager.api.GetKey(BCLocalize.GUI_NAME_LENGHT_CHAPTER) +" " + data.MapLenght.ToString();
        if (dec!=null)
              dec.text= LanguageManager.api.GetKey(data.DecMap);

    }
}
