﻿using Studio;
using Studio.BC;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BCChonseMapController : BaseView
{
    public int countFrame = 0;
    public int maxFrame = 35;
    public bool Showpopup;
    public float TimeShowNameMap;
    public Text2 nameMap;
    public Transform trfListMap;
    public BCScrollFix scrollRec;
    public GameObject TileMap;
    public Button btnChose;
    public Button btnExit;
    public List<GameObject> listObject;
  
    public void Awake()
    {
        DestroyAll();
        setEvent();
        StartCoroutine(LoadListMap());
        TileMap.transform.localScale = new Vector3(0,0,0);
        listObject = new List<GameObject>();

    }

    public void DestroyAll()
    {
        foreach (Transform tg in trfListMap)
        {
            Destroy(tg.gameObject);
        }
    }

    public void setEvent()
    {
        btnChose.onClick.AddListener(OnClickChose);
        btnExit.onClick.AddListener(OnClickExit);
    }

    public void OnClickChose()
    {
        BCUIMainController.api.world.chapCurrent = scrollRec.CurrentLayout;
        BCUIMainController.api.world.UpdateViewWorld();
        BCViewHelper.Api.Popup.Hide(StateName.ChoseMap);
    }

    IEnumerator LoadListMap()
    {
          yield return new WaitForEndOfFrame();
        Dictionary<int, BCMapData> list = BCCache.Api.DataConfig.MapConfigData;
        BCCache.Api.GetUIPrefabAsync(UIPrefabName.WorldMapButton, (go) =>
        {
            foreach (var tg in list)
            {
                GameObject game = Instantiate(go, trfListMap);
                game.GetComponent<BCMapController>().parse(tg.Value);
                //game.GetComponent<BCMapController>().Hide();
                listObject.Add(game);
            }
        });
        yield return new WaitForEndOfFrame();
    }
    public void Update()
    {
        base.Update();
        if (scrollRec.fCurrentLayot.ToString().Equals(scrollRec.CurrentLayout.ToString()))
        {
            if (listObject.Count > 0)
            {
                //StartCoroutine( DelayTime(() => {

                if (countFrame > maxFrame)
                {
                    if (scrollRec.fCurrentLayot.ToString().Equals(scrollRec.CurrentLayout.ToString()))
                    {
                        if (!Showpopup)
                        {
                            Show();
                            //listObject[scrollRec.CurrentLayout - 1].GetComponent<BCMapController>().Show();
                            countFrame = 0;
                            Showpopup = true;
                        }
                       
                    }
                }

                countFrame++;
                //}));
            }
        }
        else
        {
            if (listObject.Count > 0)
            {
                //StopCoroutine(DelayTime(() => { }));
                if (Showpopup)
                {
                    //listObject[scrollRec.CurrentLayout - 1].GetComponent<BCMapController>().Hide();
                    Hide();
                    Showpopup = false;
                }
            }
        }
    }
    

    public void Show()
    {
        Debug.Log("Show");
        TileMap.SetActive(true);
        nameMap.text = LanguageManager.api.GetKey(BCCache.Api.DataConfig.MapConfigData[scrollRec.CurrentLayout].NameMap);
        LeanTween.scale(TileMap, new Vector3(1, 1, 1), TimeShowNameMap);
    }
    public void Hide()
    {
        Debug.Log("Hide");
        
        LeanTween.scale(TileMap, new Vector3(0, 0, 0), TimeShowNameMap).setOnComplete(()=> { TileMap.SetActive(false); });
    }

    public void OnClickExit()
    {
        BCViewHelper.Api.Popup.Hide(StateName.ChoseMap);
    }
}
