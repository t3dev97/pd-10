﻿using Studio;
using Studio.BC;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using static BCEquipmentView;

public class BCEquipmentController : MonoBehaviour
{
    public static BCEquipmentController api;

    public bool isBySortType = false;

    public Transform tfIventory;
    public Transform tfMaterial;
    public Transform effect;

    public GameObject objectMaterial;
    public GameObject gameCenter;
    public GameObject inventory;

    public BCEquipmentView slotRingLeft;
    public BCEquipmentView slotRingRight;
    public BCEquipmentView slotWeapon;
    public BCEquipmentView slotArmor;
    public BCEquipmentView slotPetLeft;
    public BCEquipmentView slotPetRight;
    public BCEquipmentView equiment;

    public CharacterAsset asset;
    public CharacterAsset characterAsset;

    public Text2 txtHP;
    public Text2 txtDame;
    private Text2 _sortBySlot;

    public Button btnSort;
    public Button btnFuse;

    public List<BCSlotEquipmentView> slotRing;
    public List<BCSlotEquipmentView> slotPet;

    public Dictionary<int, GameObject> listEquimentObject;
    public Dictionary<int, BCInventory> listEquipmentData;
    public Dictionary<int, BCEquipmentView> listEquimentView;

    public Dictionary<int, GameObject> listMaterialObject;
    public Dictionary<int, BCMaterialResource> listMaterialData;



    public void UpdateEquipment()
    {
        if (api == null) api = this;
        else if (api != this) Destroy(this);



        _sortBySlot = btnSort.transform.GetChild(0).GetComponent<Text2>();

        listEquimentObject = new Dictionary<int, GameObject>();
        listEquipmentData = new Dictionary<int, BCInventory>();
        listEquimentView = new Dictionary<int, BCEquipmentView>();
        listMaterialObject = new Dictionary<int, GameObject>();
        listMaterialData = new Dictionary<int, BCMaterialResource>();

        asset = Resources.Load<CharacterAsset>("ModifyTool/Player/PLAYEREQUIPMENT");
        characterAsset= Resources.Load<CharacterAsset>("ModifyTool/Player/PLAYER");

        asset.Reset();
        updateTextCharacter();

        inventory.SetActive(false);

        listEquipmentData = BCCache.Api.DataConfig.ResourceGame.listIventory;

        listMaterialData = BCCache.Api.DataConfig.ResourceGame.listMaterials;

        _sortBySlot.SetLocalize(BCLocalize.GUI_NAME_BYSLOT);

        DestroyEquipment();
        

        CreateEquipment(listEquipmentData);
        //HideInventoryUse(listEquipmentData);
        CreateMaterial();

        btnSort.onClick.AddListener(onClickBySort);
        btnFuse.onClick.AddListener(onClickFuse);
        inventory.SetActive(true);
    }

    public void UpdateViewEquipment()
    {
        DestroyEquipment();


        CreateEquipment(listEquipmentData);
    }
    public void onClickFuse()
    {
        BCViewHelper.Api.Popup.Show(StateName.Fuse);

    }
    public void AniUnEquip(int id, GameObject slot = null)
    {
        Debug.Log("AniUnEquip");
        BCCache.Api.GetUIPrefabAsync(UIPrefabName.Item, (go) =>
        {
            GameObject game = Instantiate(go, effect, false);
            game.GetComponent<BCEquipmentView>().SetData(listEquipmentData[id]);
            GameObject pos = null;
            if (slot != null)
                pos = slot;
            else
            {
                pos = GetSlotEquipment(listEquimentObject[id].GetComponent<BCEquipmentView>().slot);
            }

            game.transform.position = pos.transform.position;
            LeanTween.move(game, listEquimentObject[id].transform.position, 0.5f).setOnComplete(() =>
            {
                Debug.Log("Ani un equip ");
                pos.GetComponent<BCEquipmentView>().SetData(listEquipmentData[id]);

                listEquimentObject[id].SetActive(true);

                Debug.Log(pos.name);
                Debug.Log(pos.transform.parent.name);
                pos.SetActive(false);

                UpdateEquipmentData(listEquipmentData[id], -1);

                UpdateEquipmentSlot(listEquimentObject[id].GetComponent<BCEquipmentView>(), false);
                listEquimentObject[id].GetComponent<BCEquipmentView>().isEquip = false;
                listEquimentObject[id].GetComponent<BCEquipmentView>().slot = SlotEquipment.none;
                listEquimentObject[id].GetComponent<BCEquipmentView>().HideEquipped();

                

                //UpdateEquipmentSlot(listEquimentObject[id].GetComponent<BCEquipmentView>(), false);
                Destroy(game);
            });

        });
        ShoweAllEquiment();
        //listEquipmentInvetory.Add(id,listEquipmentData[id]);
        TurnOffTip(id);
    }
        public void AniEquip(int id, GameObject slot = null)
        {
            Debug.Log("AniEquip");
            BCCache.Api.GetUIPrefabAsync(UIPrefabName.Item, (go) =>
            {
                GameObject game = Instantiate(go, effect, false);
                game.GetComponent<BCEquipmentView>().SetData(listEquipmentData[id]);
                game.transform.position = listEquimentObject[id].transform.position;

                GameObject pos = null;

                if (slot != null)
                    pos = slot;
                else
                    pos = GetSlotEquipment(id);

                if (pos == null)
                {
                    SwapEquipment(id);
                    Destroy(game);
                }
                else
                {
                    if (pos.active)
                    {
                        string anme = pos.name;
                        AniUnEquip(pos.GetComponent<BCEquipmentView>().IdView, slot);
                    }
                    LeanTween.move(game, pos.transform.position, 0.5f).setOnComplete(() => {


                        pos.GetComponent<BCEquipmentView>().SetData(listEquipmentData[id], true);

                        listEquimentObject[id].SetActive(false);
                        listEquimentObject[id].GetComponent<BCEquipmentView>().isEquip = true;
                        pos.SetActive(true);
                        listEquimentObject[id].GetComponent<BCEquipmentView>().slot = pos.GetComponent<BCEquipmentView>().slot;

                        UpdateEquipmentData(listEquipmentData[id]);

                        UpdateEquipmentSlot(listEquimentObject[id].GetComponent<BCEquipmentView>(),true);
                        Destroy(game);

                    });
                }

            });
        }
    public void UpdateEquipmentData(BCInventory data,int i=1)
    {
        float hp = 0;
        float dame = 0;
        switch (data.IDItem.mainPropertype.Key)
        {
            case "ATK":dame += (data.IDItem.mainPropertype.Value* i);
                dame += (data.intLevel-1) * data.IDItem.bounusEnhance* i;
                break;
            case "HP": hp += data.IDItem.mainPropertype.Value* i;
                hp += (data.intLevel-1) * data.IDItem.bounusEnhance* i;
                break;
        }
        
        foreach(var tg in data.IDItem.subPropertype)
        {
            switch (tg.Key)
            {
                case "ATK": dame += tg.Value* i; break;
                case "HP": hp += tg.Value* i; break;
            }
        }

        asset.BasicATKDamage += dame;
        asset.BasicHP += hp;
        updateTextCharacter();
    }
    public void updateTextCharacter()
    {
        txtHP.text = (characterAsset.BasicHP + asset.BasicHP).ToString();
        txtDame.text = (characterAsset.BasicATKDamage + asset.BasicATKDamage).ToString();

    }
  
    public GameObject GetSlotEquipment(SlotEquipment style)
    {
        GameObject vec = null;
        switch (style)
        {
            case SlotEquipment.weapon:
                vec = slotWeapon.gameObject;
                break;
            case SlotEquipment.armor:
                vec = slotArmor.gameObject;
                break;
            case SlotEquipment.ringLeft:
                vec = slotRingLeft.gameObject;
                break;
            case SlotEquipment.ringRight:
                vec = slotRingRight.gameObject;
                break;
            case SlotEquipment.petLeft:
                vec = slotPetLeft.gameObject;
                break;
            case SlotEquipment.petRight:
                vec = slotPetRight.gameObject;
                break;
        }
        return vec;
    }

    public void UpdateEquipmentSlot(BCEquipmentView inventory, bool isEquipment)
    {
         SlotEquipment slot= inventory.slot;
        int vt = 0;
        switch (slot)
        {
            case SlotEquipment.weapon: vt = 1;break;
            case SlotEquipment.armor: vt = 2;break;
            case SlotEquipment.ringLeft: vt = 3; break;
            case SlotEquipment.ringRight: vt = 4; break;
            case SlotEquipment.petLeft: vt = 5; break;
            case SlotEquipment.petRight: vt = 6; break;
        }
         BCDataControler.api.data.UpdateDataInvertyory(listEquipmentData[inventory.IdView], vt, isEquipment);
    }

    public GameObject GetSlotEquipment(int id)
    {
        BCInventory inventory = listEquipmentData[id];
        GameObject vec = null;
        switch (inventory.IDItem.typeEquipment.id)
        {
            case (int)EnumTypeEquipment.Weapon:
                vec = slotWeapon.gameObject;
                break;
            case (int)EnumTypeEquipment.Armor:
                vec = slotArmor.gameObject; break;
            case (int)EnumTypeEquipment.Ring:
                if (slotRingLeft.gameObject.active)
                {
                    if (slotRingRight.gameObject.active)
                        vec = null;
                    else
                        vec = slotRingRight.gameObject;
                }
                else
                {
                    vec = slotRingLeft.gameObject;
                }
                break;
            case (int)EnumTypeEquipment.Pet:
                if (slotPetLeft.gameObject.active)
                {
                    if (slotPetRight.gameObject.active)
                        vec = null;
                    else
                        vec = slotPetRight.gameObject;
                }
                else
                {
                    vec = slotPetLeft.gameObject;
                }
                break;
        }

        return vec;
    }
    public void onClickBySort()
    {
        DestroyEquipment();
        Dictionary<int, BCEquipmentView> tg = new Dictionary<int, BCEquipmentView>();

        Dictionary<int, BCEquipmentView> listEquimentSort = new Dictionary<int, BCEquipmentView>();

        foreach (KeyValuePair<int, BCEquipmentView> author in listEquimentView.OrderBy(x => listEquipmentData[x.Value.IdView].IDItem.ID))
        {
            listEquimentSort.Add(author.Key, author.Value);
        }

        if (isBySortType)
        {
            _sortBySlot.SetLocalize(BCLocalize.GUI_NAME_BYSLOT);
            foreach (KeyValuePair<int, BCEquipmentView> author in listEquimentSort.OrderBy(x => listEquipmentData[x.Value.IdView].IDItem.gradeEquipment.id))
            {
                tg.Add(author.Key, author.Value);
            }
        }
        else
        {
            _sortBySlot.SetLocalize(BCLocalize.GUI_NAME_BYRARITY);
            foreach (KeyValuePair<int, BCEquipmentView> author in listEquimentSort.OrderBy(x => listEquipmentData[x.Value.IdView].IDItem.typeEquipment.id))
            {
                tg.Add(author.Key, author.Value);
            }
        }

        CreateEquipment(tg);
        isBySortType = !isBySortType;
    }
    public void CreateEquipment(Dictionary<int, BCInventory> data)
    {
        listEquimentObject.Clear();
        listEquimentView.Clear();
        BCCache.Api.GetUIPrefabAsync(UIPrefabName.Item, (go) =>
        {
            foreach (var tg in data)
            {
                GameObject game = Instantiate(go, tfIventory, false);
                game.GetComponent<BCEquipmentView>().SetData(tg.Value);
                listEquimentView.Add(tg.Value.ID, game.GetComponent<BCEquipmentView>());
                listEquimentObject.Add(tg.Value.ID, game);
            }
            HideInventoryUse(data);
            AddIventorySlot();
        });
    }
    public void AddIventorySlot()
    {
        Dictionary<int, EquipmentSlot> listEquipmentSlot = BCDataControler.api.data.listInventoryUse;
        asset.Reset();
        foreach(var equipmentSlot in listEquipmentSlot)
        {
            if (equipmentSlot.Value.inventory != null)
            {
                int id = equipmentSlot.Value.inventory.ID;
           
                GameObject pos = GetSlotEquipment(id);

                pos.GetComponent<BCEquipmentView>().SetData(listEquipmentData[id], true);

                listEquimentObject[id].SetActive(false);
                //listEquimentObject[id].GetComponent<BCEquipmentView>().isEquip = true;
                pos.SetActive(true);
                listEquimentObject[id].GetComponent<BCEquipmentView>().slot = pos.GetComponent<BCEquipmentView>().slot;

                UpdateEquipmentData(listEquipmentData[id]);

                    // pos.GetComponent<BCEquipmentView>().SetData(listEquipmentData[id], true);

                    //listEquimentObject[id].SetActive(false);
                    //listEquimentObject[id].GetComponent<BCEquipmentView>().isEquip = true;
                    //pos.SetActive(true);
                    //listEquimentObject[id].GetComponent<BCEquipmentView>().slot = pos.GetComponent<BCEquipmentView>().slot;

                    //UpdateEquipmentData(listEquipmentData[id]);
            }
          

        }
    }
    public void HideInventoryUse(Dictionary<int, BCInventory> data)
    {
        
        foreach(var result in listEquimentObject)
        {
            BCEquipmentView view = result.Value.gameObject.GetComponent<BCEquipmentView>();

            if (view.IdView != 0)
            {
                foreach (var slot in BCDataControler.api.data.listInventoryUse)
                {
                    if (slot.Value.idInventory== view.IdView)
                    {
                        result.Value.gameObject.SetActive(false);
                        view.isEquip = true;
                        //UpdateEquipmentData(data[view.IdView]);
                    }
                }
                
            }
           
        }
    }
    public void CreateEquipment(Dictionary<int, BCEquipmentView> data)
    {
        listEquimentObject.Clear();
        listEquimentView.Clear();
        DestroyEquipment();
        BCCache.Api.GetUIPrefabAsync(UIPrefabName.Item, (go) =>
        {
            foreach (var tg in data)
            {
                GameObject game = Instantiate(go, tfIventory, false);
                BCEquipmentView equipment = game.GetComponent<BCEquipmentView>();
              


                if (tg.Value.slot != SlotEquipment.none)
                {
                    game.SetActive(false);
                    equipment.SetData(tg.Value, true, false);
                }
                else {
                    equipment.SetData(tg.Value, false, false);
                }
                listEquimentView.Add(equipment.IdView, equipment);
                listEquimentObject.Add(equipment.IdView, game);
            }
        });
    }
    public void HideAllEquiment()
    {
        gameCenter.SetActive(true);

        tfIventory.gameObject.SetActive(false);
        tfMaterial.parent.gameObject.SetActive(false);

        slotWeapon.GetComponent<Button>().interactable = false;
        slotArmor.GetComponent<Button>().interactable = false;
        slotRingLeft.GetComponent<Button>().interactable = false;
        slotRingRight.GetComponent<Button>().interactable = false;
        slotPetLeft.GetComponent<Button>().interactable = false;
        slotPetRight.GetComponent<Button>().interactable = false;
    }
    public void ShoweAllEquiment()
    {
        gameCenter.SetActive(false);

        tfIventory.gameObject.SetActive(true);
        tfMaterial.parent.gameObject.SetActive(true);

        slotWeapon.GetComponent<Button>().interactable = true;
        slotArmor.GetComponent<Button>().interactable = true;
        slotRingLeft.GetComponent<Button>().interactable = true;
        slotRingRight.GetComponent<Button>().interactable = true;
        slotPetLeft.GetComponent<Button>().interactable = true;
        slotPetRight.GetComponent<Button>().interactable = true;


    }
    public void TurnOnTip(int id, int type)
    {

        switch (type)
        {
            case 3:
                foreach (var tg in slotRing)
                {
                    tg.EnableTip(id);
                }
                break;
            case 4:
                foreach (var tg in slotPet)
                {
                    tg.EnableTip(id);
                }
                break;
        }
    }
    public void TurnOffTip(int id)
    {

        foreach (var tg in slotRing)
        {
            tg.VisibleTip();
        }
        foreach (var tg in slotPet)
        {
            tg.VisibleTip();
        }

    }
    public void SwapEquipment(int id)
    {
        int type = listEquipmentData[id].IDItem.typeEquipment.id;
        HideAllEquiment();
        TurnOnTip(id, type);
        equiment.SetData(listEquipmentData[id]);


    }
    public void CreateMaterial()
    {
        DestroyMaterial();
        listMaterialObject.Clear();
        if (listMaterialData.Count == 0)
        {
            objectMaterial.SetActive(false);
            return;
        }
        foreach (var tg in listMaterialData)
        {
            if (tg.Value.material.hide != 0)
            {
                BCCache.Api.GetUIPrefabAsync(UIPrefabName.Material, (go) =>
                {
                    GameObject game = Instantiate(go, tfMaterial, false);
                    game.GetComponent<BCUIMaterialView>().setdata(tg.Value);
                    listMaterialObject.Add(tg.Value.material.id, game);
                });
            }

        }
    }
    public void UpdateData()
    {
        foreach (var tg in listEquimentObject)
        {
            BCEquipmentView view = tg.Value.GetComponent<BCEquipmentView>();
            view.SetData(listEquipmentData[view.IdView]);
        }


        foreach (var tg in BCDataControler.api.data.listInventoryUse)
        {
            if (tg.Value.idInventory != 0)
            {
                switch (tg.Value.ID)
                {
                    case (int)SlotEquipment.weapon: slotWeapon.SetData(tg.Value.inventory, true); break;
                    case (int)SlotEquipment.armor: slotArmor.SetData(tg.Value.inventory, true); break;
                    case (int)SlotEquipment.ringLeft: slotRingLeft.SetData(tg.Value.inventory, true); break;

                    case (int)SlotEquipment.ringRight: slotRingRight.SetData(tg.Value.inventory, true); break;
                    case (int)SlotEquipment.petLeft: slotPetLeft.SetData(tg.Value.inventory, true); break;
                    case (int)SlotEquipment.petRight: slotPetRight.SetData(tg.Value.inventory, true); break;
                }
                //UpdateEquipmentData(tg.Value.inventory);
            }
        }
        CreateMaterial();


    }
    public void DestroyEquipment()
    {
        foreach (Transform tg in tfIventory)
        {
            Destroy(tg.gameObject);
        }

    }
    public void DestroyMaterial()
    {
        foreach (Transform tg in tfMaterial)
        {
            Destroy(tg.gameObject);
        }
    }
}
