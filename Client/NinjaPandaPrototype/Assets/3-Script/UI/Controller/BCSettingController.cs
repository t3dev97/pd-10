﻿using Studio;
using Studio.BC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class BCSettingController : MonoBehaviour
{
    public Button btnMusic;
    public Button btnSFX;
    public Button btnLanguage;
    public Button btnSupport;
    public Button btnCredit;
    public Button btnTemsOfService;
    public Button btnPolicy;

    public Text2 textMusic;
    public Text2 textSFX;
    public Text2 textLangue;

    public bool isClickMusic=false;
    public bool isClickSFX=false;
    public bool isClickLanguage = false;
    public void Awake()
    {
        btnMusic.onClick.AddListener(onClickMusic);
        btnSFX.onClick.AddListener(onClickSFX);
        btnLanguage.onClick.AddListener(onClickLanguage);
        btnSupport.onClick.AddListener(onClickSupport);
        btnCredit.onClick.AddListener(onClickCredit);
        btnTemsOfService.onClick.AddListener(onClickService);
        btnPolicy.onClick.AddListener(onClickPolicy);
    }

    public void onClickMusic()
    {
        Debug.Log("onClickMusic");
        if (isClickMusic)
        {
            //audioSource.mute
            BCAudioController.api.audioSource.mute = false;
            isClickMusic = false;
            textMusic.text = LanguageManager.api.GetKey(BCLocalize.GUI_NAME_ON);
        }
        else
        {
            BCAudioController.api.audioSource.mute = true;
            isClickMusic = true;
            textMusic.text = LanguageManager.api.GetKey(BCLocalize.GUI_NAME_OFF);
        }
    }

    public void onClickSFX()
    {
        Debug.Log("onClickSFX");
        if (isClickSFX)
        {
            isClickSFX = false;
            textSFX.text = LanguageManager.api.GetKey(BCLocalize.GUI_NAME_ON);
        }
        else
        {
            isClickSFX = true;
            textSFX.text = LanguageManager.api.GetKey(BCLocalize.GUI_NAME_OFF);
        }
    }

    public void onClickLanguage()
    {
        //if (isClickLanguage)
        //{
        if (PlayerPrefs.GetString("language").Equals("en")){ 
            isClickLanguage = false;
            textLangue.text = LanguageManager.api.GetKey(BCLocalize.GUI_NAME_LANGUAGE_ENGLISH);
            
            SceneManager.LoadScene(NameScene.BCLoading, LoadSceneMode.Single);
            PlayerPrefs.SetString("language", "vn");
        }
        else
        {
            isClickLanguage = true;
            textLangue.text = LanguageManager.api.GetKey(BCLocalize.GUI_NAME_LANGUAGE_VIETNAM);
            PlayerPrefs.SetString("language", "en");
            SceneManager.LoadScene(NameScene.BCLoading, LoadSceneMode.Single);
        }
    }

    public void onClickSupport()
    {
        Application.OpenURL(BCCache.Api.DataConfig.ConfigGameData.urlFeedBack);
        Debug.Log("onClickSupport");
    }

    public void onClickCredit()
    {
        Application.OpenURL(BCCache.Api.DataConfig.ConfigGameData.urlCredits);
        Debug.Log("onClickCredit");
    }

    public void onClickService()
    {
        Application.OpenURL(BCCache.Api.DataConfig.ConfigGameData.urlTerms);

       Debug.Log("onClickService");
    }

    public void onClickPolicy()
    {
        Application.OpenURL(BCCache.Api.DataConfig.ConfigGameData.urlPolicy);
        Debug.Log("onClickPolicy");
    }
}
