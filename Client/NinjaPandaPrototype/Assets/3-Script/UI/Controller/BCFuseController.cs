﻿using Studio;
using Studio.BC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BCFuseController : BaseView
{
    public static BCFuseController api;

    public Transform contentListEquipment;
    public Transform effect;

    public Button btnBack;
    public Button btnFuse;

    public List<BCEquipmentView> listEquipmentFuse;
    public Dictionary<int, BCEquipmentView> listEquimentView;

    public void Awake()
    {
        if (api == null)
        {
            api = this;
        }else if (api != this)
        {
            Destroy(this);
        }
        listEquimentView = new Dictionary<int, BCEquipmentView>();
        //DestroyContentList();
        ////UpdateEquipmentView();
        //AddEquipemtView();
        //btnBack.onClick.AddListener(() => HideView());
        //btnFuse.onClick.AddListener(() => FuseController());
        //HideBtnFuse();
    }
    public void OnEnable()
    {
        DestroyContentList();
        //UpdateEquipmentView();
        AddEquipemtView();
        btnBack.onClick.RemoveAllListeners();
        btnFuse.onClick.RemoveAllListeners();

        btnBack.onClick.AddListener(() => HideView());
        btnFuse.onClick.AddListener(() => FuseController());
        HideBtnFuse();
    }
    public void UpdateEquipmentView()
    {
        foreach(var data in BCEquipmentController.api.listEquimentObject)
        {
            BCEquipmentView info = data.Value.GetComponent<BCEquipmentView>();
            listEquimentView.Add(info.IdView, info);
        }
    }
    public void FuseController()
    {
        DebugX.Log("Click Fuse");
        BCViewHelper.Api.PopupNotify.Show(StateName.OpenFuseFX,(go)=> 
        {
            BCOpenFuseFXView fuse = go.GetComponent<BCOpenFuseFXView>();
            fuse.SetData(listEquipmentFuse);
        });
        ResetSlotEquimentFuse();

    }
    public void HideView()
    {
        DebugX.Log("Click Back");
        BCViewHelper.Api.Popup.Hide(StateName.Fuse);
    }
    public void OffClickAllItemView()
    {
        foreach(var tg in listEquimentView)
        {
            tg.Value.gameObject.GetComponent<Button>().interactable = false;
        }

    }
    public void onClickAllItemView(int id)
    {
        foreach (var tg in listEquimentView)
        {
            if (tg.Value.IDInventory == id)
            {
            tg.Value.gameObject.GetComponent<Button>().interactable = true;
            }
        }
    }

    public void AddEquipemtView()
    {
       
            BCCache.Api.GetUIPrefabAsync(UIPrefabName.Item, (go) =>
            {
                foreach (var data in BCEquipmentController.api.listEquimentObject)
                {
                    GameObject game = Instantiate(go, contentListEquipment, false);
                    BCEquipmentView info = data.Value.GetComponent<BCEquipmentView>();
                    BCEquipmentView infoNew = game.GetComponent<BCEquipmentView>();
                    infoNew.SetData(info);
                    listEquimentView.Add(infoNew.IdView, infoNew);
                }
            });
    }
    public void DestroyContentList()
    {
        listEquimentView.Clear();
        foreach (Transform tg in contentListEquipment)
        {
            Destroy(tg.gameObject);
        }
    }
   public void ResetSlotEquimentFuse()
    {
        ResetEquipemtFuse();
        ResetEquipment();
    }
    public void AniEquip(BCEquipmentView view, GameObject slot = null)
    {

        BCCache.Api.GetUIPrefabAsync(UIPrefabName.Item, (go) =>
        {
           

            GameObject vec = GetGameObjectSlotFuse();
            if (vec == null)
            {
                ResetSlotEquimentFuse();
                return ;
            }
            GameObject game = Instantiate(go, effect, false);
            game.GetComponent<BCEquipmentView>().SetData(view);
            game.transform.position = listEquimentView[view.IdView].gameObject.transform.position;

            OffClickAllItemView();

            LeanTween.move(game, vec.transform.position, 0.2f).setOnComplete(() =>
            {

                BCEquipmentView viewEquipment = vec.GetComponent<BCEquipmentView>();
                viewEquipment.SetData(game.GetComponent<BCEquipmentView>());
                viewEquipment.gameObject.SetActive(true);
                viewEquipment.EventReset();

                view.EnableCheck();
                Destroy(game);

                LockEquipments(view.IDInventory);
          
                onClickAllItemView(view.IDInventory);
            });


        });
    }
    public void HideBtnFuse()
    {
        btnFuse.gameObject.SetActive(false);
    }
    public void ShowBtnFuse()
    {
        btnFuse.gameObject.SetActive(true);
    }
    public GameObject GetGameObjectSlotFuse()
    {
        GameObject vec = null;
        int count = 0;
        foreach(BCEquipmentView data in listEquipmentFuse)
        {
            count++;
            if (!data.gameObject.active)
            {
                if (count == listEquipmentFuse.Count)
                    ShowBtnFuse();
                  vec = data.gameObject;
                break;
            }
        }
        return vec;
    }
    
    public void ResetEquipemtFuse()
    {
        foreach(BCEquipmentView view in listEquipmentFuse)
        {
            view.Hide();
        }
    }
    public void ResetEquipment()
    {
        HideBtnFuse();
        foreach (var view in listEquimentView)
        {
            view.Value.EnableNone();
        }
    }

    public void LockEquipments(int idequipment)
    {
        foreach (var view in listEquimentView)
        {
            if (view.Value.IDInventory != idequipment)
                view.Value.EnableLock();
        }
    }

}
