﻿using Studio.BC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BCShopController : MonoBehaviour
{
    public static BCShopController api;
    public Transform trChests;
    public Transform trGems;
    public Transform trCoins;
    public Scrollbar scrollbar;

    public  void updateShop()
    {
        if (api == null)
            api = this;
        else if (api != this)
            Destroy(this);

        DestroyAll();

        SetChest();
        SetGem();
        SetCoint();

    }

    public void SetChest()
    {
        Dictionary<int, ChestShopConfigData> listChest = BCCache.Api.DataConfig.ShopConfigData.listChestData;

        foreach(var dataChest in listChest)
        {
            BCCache.Api.GetUIPrefabAsync(UIPrefabName.ShopChestButton, (go) =>
            {
                var game = Instantiate(go, trChests);
                game.GetComponent<BCShopChestView>().Parse(dataChest.Value);
            });
          
        }
    }

    public void SetGem()
    {
        Dictionary<int, GemShopConfigData> listGem = BCCache.Api.DataConfig.ShopConfigData.listGemData;

        foreach (var dataChest in listGem)
        {

            BCCache.Api.GetUIPrefabAsync(UIPrefabName.ShopGemButton, (go) =>
            {
                var game = Instantiate(go, trGems);
                game.GetComponent<BCShopGemView>().Parse(dataChest.Value);
            });

        
        }
    }

    public void SetCoint()
    {
        Dictionary<int, CoinShopConfigData> listCoint = BCCache.Api.DataConfig.ShopConfigData.listCoinData;

        foreach (var dataChest in listCoint)
        {
            BCCache.Api.GetUIPrefabAsync(UIPrefabName.ShopCoinButton, (go) =>
            {
                var game = Instantiate(go, trCoins);
                game.GetComponent<BCShopCoinView>().Parse(dataChest.Value);
            });
        }
    }

    public void DestroyAll()
    {
        foreach(Transform tg in trChests)
        {
            Destroy(tg.gameObject);
        }

        foreach (Transform tg in trGems)
        {
            Destroy(tg.gameObject);
        }

        foreach (Transform tg in trCoins)
        {
            Destroy(tg.gameObject);
        }
    }

}
