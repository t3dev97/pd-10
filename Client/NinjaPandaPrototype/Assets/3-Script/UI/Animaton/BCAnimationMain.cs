﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class BCAnimationMain : MonoBehaviour
{
    public enum StyleAnimation
    {
        normal=1,
        snake=2
    }
    public StyleAnimation style = StyleAnimation.normal;
    public float scale = 0.4f;
    public float timeAnimationStart = 0.30f;
    public float timeAnimationEnd = 0.15f;
    public float snake = 25f;
    private Vector3 scaleCurren;
    public Vector3 posCurren;
    public void Awake()
    {
        scaleCurren = gameObject.transform.localScale;
        posCurren = gameObject.transform.localPosition;
    }
    public void Play(Action action)
    {
        AniStart(action);
    }
    void AniStart(Action action)
    {
        Vector3 scaleNew = Vector3.zero;

        switch (style)
        {
            case StyleAnimation.normal:
                scaleNew = scaleCurren + new Vector3(scale, scale, scale);
                LeanTween.scale(gameObject, scaleNew, timeAnimationStart).setOnComplete(() => { AniEnd(action); });
                break;
            case StyleAnimation.snake:
                scaleNew = posCurren;
                Vector3[] vec = new Vector3[20];
                for(int i = 0; i < vec.Length; i++)
                {
                    float x = 0;
                    float y = 0;
                    //if(i/2==0)
                     x = Random.Range(-snake, snake);
                    //else
                     y = Random.Range(-snake, snake);
                    vec[i] = new Vector3(posCurren.x+x, posCurren.y+ y,posCurren.z);
                }
                LeanTween.moveSplineLocal(gameObject, vec, timeAnimationStart).
                    setOnComplete(() => 
                    {
                        AniEnd(action);
                    });
                break;
        }
        
  

    }
    void AniEnd(Action action)
    {
     
        switch (style)
        {
            case StyleAnimation.normal:
                LeanTween.scale(gameObject, scaleCurren, timeAnimationEnd)
                    .setOnComplete(() => 
                    {
                        action.Invoke();
                    });
                break;

            case StyleAnimation.snake:
                Debug.Log(posCurren);
                LeanTween.moveLocal(gameObject, posCurren, timeAnimationEnd)
                    .setOnComplete(() => 
                    {
                        action.Invoke();
                    });
                break;
        }
      
        
    }

}
