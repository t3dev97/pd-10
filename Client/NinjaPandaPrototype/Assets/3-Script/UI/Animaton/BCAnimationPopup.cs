﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class BCAnimationPopup : MonoBehaviour
{
    // Start is called before the first frame update
    public enum StylePopup
    {
        moveY=1,
        scale=2
    }
    public StylePopup style = StylePopup.moveY;
    public float scale = 0.5f;
    public float posY=25;
    public float timeAnimation = 0.5f;
    public Vector3 posCurren;
    private float _time;

    public void Awake()
    {
        posCurren = gameObject.transform.localPosition; 
        Init();
    }
    public void SetPosCurren(Vector3 posCurrent)
    {
        posCurren = posCurrent;
    }
    public void Init()
    {
        Vector3 vec = gameObject.transform.localPosition;

        switch (style)
        {
            case StylePopup.moveY: vec.y = posCurren.y + posY;break;
            case StylePopup.scale:break;
        }
        
        gameObject.transform.localPosition = vec;
        gameObject.transform.localScale = new Vector3(1 - scale, 1 - scale, 1);
    }

    public void OnDisable()
    {
        StopCoroutine(DelayTime(() => { }));
           Init();
    }
    public void OnEnable()
    {
        switch (style)
        {
            case StylePopup.moveY:
                LeanTween.moveLocalY(gameObject, posCurren.y, timeAnimation);
                LeanTween.scaleX(gameObject, 1, timeAnimation);
                LeanTween.scaleY(gameObject, 1, timeAnimation);
                break;
            case StylePopup.scale:
                StartCoroutine(DelayTime(() =>
                {
                    LeanTween.scaleX(gameObject, 1, timeAnimation);
                    LeanTween.scaleY(gameObject, 1, timeAnimation);
                }));

                break;

        }
       
    }
    public IEnumerator DelayTime(Action action)
    {
        yield return new WaitForSeconds(timeAnimation);
        action.Invoke();
    }
    

}
