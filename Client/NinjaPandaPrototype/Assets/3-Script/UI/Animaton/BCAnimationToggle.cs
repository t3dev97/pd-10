﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(Toggle))]
public class BCAnimationToggle : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    // Start is called before the first frame update
    public float scale = 0.08f;
    public float timeAnimation = 0.5f;
    public Vector3 scaleCurren;
    private float _time;

    public void Awake()
    {
        scaleCurren = gameObject.transform.localScale;
        _time = timeAnimation / 2;
        GetComponent<Button>().onClick.AddListener(onClickButton);
    }
    public void onClickButton()
    {
        //StartCoroutine(AniButton());
    }
    public void OnPointerDown(PointerEventData eventData)
    {
        LeanTween.scaleX(gameObject, scaleCurren.x - scale, _time);
        LeanTween.scaleY(gameObject, scaleCurren.y - scale, _time);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        LeanTween.scaleX(gameObject, scaleCurren.x, _time);
        LeanTween.scaleY(gameObject, scaleCurren.y, _time);
    }
}
