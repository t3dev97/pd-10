﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class BCAnimationButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
   public  enum StyleAnimation
    {
        normal=1,
        ignore =2,
        parent
    }
    // Start is called before the first frame update
    public float scale = 0.08f;
    public float timeAnimation = 0.5f;
    public Vector3 scaleCurren;
    public StyleAnimation style = StyleAnimation.normal;
    private float _time;
    public List<GameObject> listIgnore;

    public void Awake()
    {
        scaleCurren = gameObject.transform.localScale;
        _time = timeAnimation / 2;
      
    }
  
    public void OnPointerDown(PointerEventData eventData)
    {
        switch (style)
        {
            case StyleAnimation.normal:
                LeanTween.scaleX(gameObject, scaleCurren.x - scale, _time);
                LeanTween.scaleY(gameObject, scaleCurren.y - scale, _time);
                break;
            case StyleAnimation.ignore:
                foreach(GameObject game in listIgnore)
                {
                    LeanTween.scaleX(game, scaleCurren.x - scale, _time);
                    LeanTween.scaleY(game, scaleCurren.y - scale, _time);
                }
                break;
            case StyleAnimation.parent:
                LeanTween.scaleX(gameObject.transform.parent.gameObject, scaleCurren.x - scale, _time);
                LeanTween.scaleY(gameObject.transform.parent.gameObject, scaleCurren.y - scale, _time);
                break;
        }
       
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        switch (style)
        {
            case StyleAnimation.normal:
                LeanTween.scaleX(gameObject, scaleCurren.x, _time);
                LeanTween.scaleY(gameObject, scaleCurren.y, _time);
                break;
            case StyleAnimation.ignore:
                foreach(GameObject game in listIgnore)
                {
                    LeanTween.scaleX(game, scaleCurren.x, _time);
                    LeanTween.scaleY(game, scaleCurren.y, _time);
                }
                break;
            case StyleAnimation.parent:
                LeanTween.scaleX(gameObject.transform.parent.gameObject, scaleCurren.x, _time);
                LeanTween.scaleY(gameObject.transform.parent.gameObject, scaleCurren.y, _time);
                break;
        }
    }
}
