﻿using Studio;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BCEffectNum : MonoBehaviour
{
    // Start is called before the first frame update
    public Text2 txtNum;
    public int num;
    public float timeAni;
    public float moveY;
    public void play(Action action)
    {
        txtNum.text = num.ToString();
        LeanTween.moveY(gameObject, moveY, timeAni).setOnComplete(action);

    }


}
