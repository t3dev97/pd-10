﻿using Studio;
using Studio.BC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class BCShopGemView : MonoBehaviour
{
    // Start is called before the first frame update
    public BCDynamicImage img;
    public Text2 txtNameChest;
    public Text txtAmountChest;
    public Text txtPriceChest;

    public Button btnOpen;

    public void Parse(GemShopConfigData data)
    {
        img.Type = EnumDynamicImageType.Currency;
        img.SetImage(data.AssetName, _isSetNativeSize: true);
        txtNameChest.text = LanguageManager.api.GetKey(data.NameID);
        txtAmountChest.text = data.Amount;
        txtPriceChest.text = data.PriceText;

        btnOpen.onClick.RemoveAllListeners();
        btnOpen.onClick.AddListener(()=>ButtonClick(data));
    }
    void ButtonClick(GemShopConfigData data)
    {
        string amount = data.Amount.Replace(".", "");
        BCDataControler.waitedPurchaseGems = int.Parse(amount);
        //#if UNITY_INAPPS
        //        UnityInAppsIntegration.THIS.BuyProductID(data.KeyIAPAndroid);
        //            //StartCoroutine(InitScript.Instance.DelayOffLoadingBug());
        //#endif

        string key = "";
        #if UNITY_ANDROID
                key = data.KeyIAPAndroid;
        #endif
        #if UNITY_IOS
                          key = data.KeyIAPIOS;
        #endif

        IapManager.Api.BuyProductID(key, (isSuccess, receipID) => {

            if (isSuccess) BCDataControler.api.PlusGem(int.Parse(amount));
        });
    }
}
