﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Studio;
using Studio.BC;

public class BCUITopView : MonoBehaviour
{
    public BCViewResource ExpView;
    public BCViewResource EnergyView;
    public BCViewResource GoldView;
    public BCViewResource GemView;

    public void updataUITop()
    {
        ExpView.Button.AddClickListener(OnClickExpButton);
        EnergyView.Button.AddClickListener(OnClickEnergyButton);
        GoldView.Button.AddClickListener(OnClickGoldButton);
        GemView.Button.AddClickListener(OnClickGemButton);

        ExpView.UpdateResource();
        EnergyView.UpdateResource();
        GoldView.UpdateResource();
        GemView.UpdateResource();
    }

    private void OnClickExpButton()
    {
        DebugX.Log("EXp");
        BCUIMainController.api.ToggleValueChanged(Lobby_Position.INVENTORY);
    }
    private void OnClickEnergyButton()
    {
        Debug.Log("Energy");
        BCViewHelper.Api.PopupNotify.Show(StateName.PopupBuyEnergy);

    }
    private void OnClickGoldButton()
    {
        DebugX.Log("Gold");
        BCUIMainController.api.ToggleValueChanged(Lobby_Position.SHOP);
    }
    private void OnClickGemButton()
    {
        DebugX.Log("Gem");
        BCUIMainController.api.ToggleValueChanged(Lobby_Position.SHOP);
    }
}
