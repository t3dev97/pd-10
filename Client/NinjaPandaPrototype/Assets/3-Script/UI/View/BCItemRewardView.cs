﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BCItemRewardView : MonoBehaviour
{
    // Start is called before the first frame update
    public ImageUIHelper imgRewawrds;
    public Text txtValue;
   public void setData(string img,string value)
    {
        imgRewawrds.SetSprite(img);
        imgRewawrds.GetComponent<Image>().SetNativeSize();
        GetComponent<Image>().SetNativeSize();
        txtValue.text = value;
    }
}
