﻿using Studio;
using Studio.BC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BCOpenChestView : BaseView
{
    public Button btnBack;
    public Animation animation;

    public Text2 nameItem;
    public Text2 gradeItem;
    public Text2 desItem;

    public Text2 nameItemShadow;
    public Text2 gradeItemShadow;
    public Text2 desItemShadow;

    public ImageUIHelper imgChestClose;
    public ImageUIHelper imgChestOpen;
    public BCDynamicImage imgItem;
    public void Awake()
    {
        btnBack.onClick.AddListener(onClickBack);
        animation = gameObject.GetComponent<Animation>();
        animation.Play("AnimOpenChestFX");
    }
    public void Init(BCEquipment equipment,int chestID,ChestShopConfigData data)
    {
        nameItem.text = LanguageManager.api.GetKey(equipment.nameEquipment);
        gradeItem.text = LanguageManager.api.GetKey(equipment.gradeEquipment.nameGrade);
        desItem.text = LanguageManager.api.GetKey(equipment.decEquipment);

        nameItemShadow.text = LanguageManager.api.GetKey(equipment.nameEquipment);
        gradeItemShadow.text = LanguageManager.api.GetKey(equipment.gradeEquipment.nameGrade);
        desItemShadow.text = LanguageManager.api.GetKey(equipment.decEquipment);



        imgItem.Type = EnumDynamicImageType.Inventory;
        imgItem.SetImage(equipment.assetName);

        //imgChestClose.SetSprite(ImageName.item_chest_ + chestID );
        //imgChestOpen.SetSprite(ImageName.item_chest_ + chestID +"b");
        imgChestClose.SetSprite(data.AssetName);
        imgChestOpen.SetSprite(data.AssetNameClose);

    }
    public void onClickBack()
    {
        BCViewHelper.Api.PopupNotify.Hide(StateName.OpenChestFX);
    }
  

}
