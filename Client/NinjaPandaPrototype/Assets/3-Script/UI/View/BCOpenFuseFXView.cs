﻿using Studio;
using Studio.BC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BCOpenFuseFXView : BaseView
{
    public Button btnBack;
    public Button btnAgain;
    public Animation animation;

    public BCEquipmentView LeftEquipment;
    public BCEquipmentView RightEquipment;
    public BCEquipmentView CenterEquipment;

    public BCEquipmentView LeftEquipmentAfter;
    public BCEquipmentView RightEquipmentAfter;
    public BCEquipmentView CenterEquipmentAfter;
    public void Awake()
    {
        btnBack.onClick.AddListener(onClickBack);
        animation = gameObject.GetComponent<Animation>();
        //animation.Play("AnimOpenChestFX");
        animation.Play();
    }
    public void SetData(List<BCEquipmentView> listEquipment)
    {
        LeftEquipment.SetData(listEquipment[0],false);
        RightEquipment.SetData(listEquipment[1], false);
        CenterEquipment.SetData(listEquipment[2], false);
        LeftEquipmentAfter.SetData(listEquipment[0], false);
        RightEquipmentAfter.SetData(listEquipment[1], false);
        CenterEquipmentAfter.SetData(listEquipment[2], false);
    }

    public void onClickBack()
    {
        DebugX.Log("onClickBack");
        BCViewHelper.Api.PopupNotify.Hide(StateName.OpenFuseFX);
    }
    public void onClickAgain()
    {
        DebugX.Log("onClickAgain");
        BCViewHelper.Api.PopupNotify.Hide(StateName.OpenFuseFX);
        BCViewHelper.Api.PopupNotify.Show(StateName.OpenFuseFX);
    }
}
