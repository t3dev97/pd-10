﻿using Studio;
using Studio.BC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class BCShopCoinView : MonoBehaviour
{
    // Start is called before the first frame update
    public BCDynamicImage img;
    public Text2 txtNameChest;
    public Text txtAmountChest;
    public Text txtPriceChest;

    public Button btnOpen;

    public void Parse(CoinShopConfigData data)
    {
        img.Type = EnumDynamicImageType.Currency;
        img.SetImage(data.AssetName,_isSetNativeSize: true);
        txtNameChest.text = LanguageManager.api.GetKey(data.NameID);
        txtAmountChest.text = data.Amount;
        txtPriceChest.text = data.PriceText;
        btnOpen.onClick.AddListener(()=>ButtonClick(data.PriceText,data.Amount));
    }
    void ButtonClick(string id,string amount)
    {
        BCDataControler.api.converGemToCoint(int.Parse(id), int.Parse(amount));
        DebugX.Log("Click Coin id = " + id);
    }
}
