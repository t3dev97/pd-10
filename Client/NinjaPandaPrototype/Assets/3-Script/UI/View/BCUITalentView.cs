﻿using Studio;
using Studio.BC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class BCUITalentView : MonoBehaviour
{
    // Start is called before the first frame update
    public Image asset;
    public Text2 nameTalent;
    public bool isChose;
    public Image imgChose;
    public Text2 level;
    public Button btnTalent;
    public bool isLock;
    public Image imgLock;
    public int levelNum;
    public BCPopupTalentView popupTalentView;
    public TalentConfigData Data;

    private BCTalentController _talentController;

    public void Awake()
    {
        asset.gameObject.SetActive(false);
        popupTalentView.gameObject.SetActive(false);
        imgChose.gameObject.SetActive(false);
        btnTalent.onClick.AddListener(ShowPopupTalent);
        imgLock.gameObject.SetActive(true);
        level.gameObject.SetActive(false);
    }
    public void ShowPopupTalent()
    {
        //if(_talentController!=null && _talentController.popupTalentCurrent != null)
        //    _talentController.popupTalentCurrent.SetActive(false);

        //_talentController.popupTalentCurrent = popupTalentView.gameObject;

        //popupTalentView.gameObject.SetActive(false);

        //popupTalentView.gameObject.SetActive(true);

        _talentController.ShowPopupTalent(this);

    }

    public IEnumerator AnimationTalentChose(float time)
    {
        imgChose.gameObject.SetActive(true);
        yield return new WaitForSeconds(time);
        imgChose.gameObject.SetActive(false);
        yield return null;
    }


    public void Parse(TalentConfigData data, BCTalentController talentController)
    {
        Data = data;
        _talentController = talentController;
        int Level = BCCache.Api.DataConfig.ResourceGame.listTalents[data.ID].Level;
        asset.gameObject.GetComponent<ImageUIHelper>().SetSprite(data.AssetName);
      
        nameTalent.text = LanguageManager.api.GetKey(data.NameID);
        if (Level < 1)
        {
            nameTalent.text = LanguageManager.api.GetKey(BCLocalize.TEXT_NAME_LOCKED);
        }
        else
        {
            imgLock.gameObject.SetActive(false);
            asset.gameObject.SetActive(true);
            level.gameObject.SetActive(true);
            //popupTalentView.Parse(data);
        }
        if (data.Bounus == 0)
        {
            level.SetLocalize(BCLocalize.TEXT_NAME_LEVEL_MAX);
        }
        else
        {
            level.text = LanguageManager.api.GetKey(BCLocalize.TEXT_NAME_LEVEL, new string[] { Level.ToString() });
        }

        levelNum = Level;
    }

}
