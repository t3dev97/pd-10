﻿using Studio;
using Studio.BC;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
[Serializable]
public class BCEquipmentView : MonoBehaviour
{
    public enum SlotEquipment
    {
        none=0,
        weapon=1,
        armor=2,
        ringLeft=3,
        ringRight=4,
        petLeft=5,
        petRight=6
    }

    public SlotEquipment slot = SlotEquipment.none;

    public int IdView;
    public int IDInventory;
    public bool isEquip = false;

    public GameObject objectEquipment;
    public GameObject objectLock;
    public GameObject objectCheck;

    public ImageUIHelper imgType;
    public ImageUIHelper imgGrade;
    //public ImageUIHelper imgEquipment;
    public BCDynamicImage imgDynamicImage;
    public Text2 txtLevel;
    public Button btnEquipment;

    public void SetData(BCEquipmentView view,bool isEvent)
    {
        slot = SlotEquipment.none;
        IdView = view.IdView;
        IDInventory=view.IDInventory;
        isEquip = view.isEquip;
        objectEquipment=view.objectEquipment;
        objectLock=view.objectLock;
        objectCheck=view.objectCheck;
        //imgType=view.imgType;
        //imgGrade=view.imgGrade;
        //imgDynamicImage=view.imgDynamicImage;
        txtLevel=view.txtLevel;
        btnEquipment=view.btnEquipment;


        imgType.SetSprite(view.imgType.nameTexUI);
      
        imgGrade.SetSprite(view.imgGrade.nameTexUI);


        imgDynamicImage.Type = EnumDynamicImageType.Inventory;
        imgDynamicImage.SetImage(view.imgDynamicImage.AssetName);
        imgDynamicImage.gameObject.GetComponent<Image>().SetNativeSize();
    }

    public void SetData(BCInventory inventory,bool Equip=false)
    {
        IdView = inventory.ID;
        isEquip = Equip;
        IDInventory = inventory.IDItem.ID;
        txtLevel.SetLocalize(BCLocalize.TEXT_NAME_LEVEL, new string[] { inventory.intLevel.ToString() });

        BCEquipment equipment = inventory.IDItem;
        imgType.SetSprite(equipment.typeEquipment.AssetName);
        if(!Equip)
            imgType.gameObject.SetActive(false);
        imgGrade.SetSprite(equipment.gradeEquipment.assetName);

        //imgEquipment.SetSprite(equipment.assetName);
        //imgEquipment.gameObject.GetComponent<Image>().SetNativeSize();

        //imgDynamicImage.AssetName = equipment.assetName;
        imgDynamicImage.Type = EnumDynamicImageType.Inventory;
        imgDynamicImage.SetImage(equipment.assetName);
        imgDynamicImage.gameObject.GetComponent<Image>().SetNativeSize();

        btnEquipment.onClick.RemoveAllListeners();
        btnEquipment.onClick.AddListener(() => OnClickEquipment(inventory));
    }


    public void SetData(BCEquipmentView equimenview, bool Equip = false,bool isViewFuse=true)
    {
        IdView = equimenview.IdView;
        slot = equimenview.slot;
        IDInventory = equimenview.IDInventory;
        if (slot != SlotEquipment.none)
        {
            objectEquipment.SetActive(true);
        }
        isEquip = equimenview.isEquip;
        Debug.Log("isEquip" + isEquip);

        BCInventory inventory = BCCache.Api.DataConfig.ResourceGame.listIventory[equimenview.IdView];
        txtLevel.SetLocalize(BCLocalize.TEXT_NAME_LEVEL, new string[] { inventory.intLevel.ToString() });

        BCEquipment equipment = inventory.IDItem;
        imgType.SetSprite(equipment.typeEquipment.AssetName);
        imgGrade.SetSprite(equipment.gradeEquipment.assetName);
       
        btnEquipment.onClick.RemoveAllListeners();
        if (!Equip)
            imgType.gameObject.SetActive(false);

        if (isViewFuse)
          btnEquipment.onClick.AddListener(() => OnClickEquipmentFuse());
        else
        btnEquipment.onClick.AddListener(() => OnClickEquipment(inventory));

        //imgEquipment.SetSprite(equipment.assetName);
        //imgEquipment.gameObject.GetComponent<Image>().SetNativeSize();

        //imgDynamicImage.AssetName = equipment.assetName;
        imgDynamicImage.Type = EnumDynamicImageType.Inventory;
        imgDynamicImage.SetImage(equipment.assetName);
        imgDynamicImage.gameObject.GetComponent<Image>().SetNativeSize();

    }

    public void OnClickEquipment(BCInventory inventory)
    {
        BCViewHelper.Api.Popup.Show(StateName.EquipmentInfo, (go) =>
        {
            BCEquipmentInfoView view = go.GetComponent<BCEquipmentInfoView>();
            if (view != null)
            {
                view.SetData(inventory, isEquip);
            }
        });
    }
    public void OnClickEquipmentFuse()
    {
        Debug.Log("OnClickEquipmentFuse");
        BCFuseController.api.AniEquip(this);
    }
    public void ResetListEquipmentFuse()
    {
        BCFuseController.api.ResetSlotEquimentFuse();
    }
    public void EnableCheck()
    {
        btnEquipment.onClick.RemoveAllListeners();
        btnEquipment.onClick.AddListener(() => ResetListEquipmentFuse());
        objectLock.SetActive(false);
        objectCheck.SetActive(true);
        
    }
    
    public void EventReset()
    {
        btnEquipment.onClick.RemoveAllListeners();
        btnEquipment.onClick.AddListener(() => ResetListEquipmentFuse());
    }
    public void EnableLock()
    {
        objectLock.SetActive(true);
        objectCheck.SetActive(false);
        btnEquipment.interactable = false;
    }
    public void EnableNone()
    {
        objectLock.SetActive(false);
        objectCheck.SetActive(false);
        btnEquipment.interactable = true;
        btnEquipment.onClick.RemoveAllListeners();
        btnEquipment.onClick.AddListener(() => OnClickEquipmentFuse());
    }

   
    public void Hide()
    {
        gameObject.SetActive(false);
    }
    public void HideEquipped()
    {
        objectEquipment.SetActive(false);
    }
}
