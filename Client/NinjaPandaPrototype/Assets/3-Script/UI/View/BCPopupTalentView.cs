﻿using Studio;
using Studio.BC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BCPopupTalentView : MonoBehaviour
{
    public Text2 txtNameTalent;
    public Text2 txtDecTalent;
    public RectTransform Pointer;
    public Button TalentDescButton;
    private RectTransform _rect;

    //public BCPopupTalentView(TalentConfigData data) {
    //    Parse(data);
    //}
    private void Start()
    {
        TalentDescButton.AddClickListener(OnTalentDescButtonClick);
    }
    private void OnTalentDescButtonClick()
    {
        gameObject.SetActive(false);
    }

    public void Parse(BCUITalentView talent)
    {
   
        _rect = GetComponent<RectTransform>();

        txtNameTalent.SetLocalize(talent.Data.NameID);
        if (talent.Data.Bounus == 0)
            txtDecTalent.SetLocalize(talent.Data.DecID);
        else
            txtDecTalent.SetLocalize(talent.Data.DecID,new string[] { (talent.Data.Bounus*BCCache.Api.DataConfig.ResourceGame.listTalents[talent.Data.ID].Level).ToString()});

        _rect.anchoredPosition3D = talent.GetComponent<RectTransform>().anchoredPosition3D + talent.Data.DescPosOffset;
        Debug.Log("talent.Data.DescPosOffset: " + talent.Data.DescPosOffset);
        Pointer.anchoredPosition3D = talent.Data.DescPointPos;
        var moveAnim = GetComponent<BCAnimationPopup>();
        if (moveAnim != null)
        {
            moveAnim.SetPosCurren(_rect.transform.localPosition);
        }
    }
}
