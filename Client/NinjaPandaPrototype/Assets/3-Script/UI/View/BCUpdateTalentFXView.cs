﻿using Studio;
using Studio.BC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BCUpdateTalentFXView : BaseView
{
    public Button btnBack;
    public Button btnAgain;
    public Animation animation;
    public void Awake()
    {
        btnBack.onClick.AddListener(onClickBack);
        //btnAgain.onClick.AddListener(onClickAgain);
        animation = gameObject.GetComponent<Animation>();
        //animation.Play("AnimOpenChestFX");
        animation.Play();
    }
    public void onClickBack()
    {
        DebugX.Log("onClickBack");
        BCViewHelper.Api.PopupNotify.Hide(StateName.OpenUpgradeFX);
    }
    public void onClickAgain()
    {
        DebugX.Log("onClickAgain");
        BCViewHelper.Api.PopupNotify.Hide(StateName.OpenUpgradeFX);
        BCViewHelper.Api.PopupNotify.Show(StateName.OpenUpgradeFX);
    }
}
