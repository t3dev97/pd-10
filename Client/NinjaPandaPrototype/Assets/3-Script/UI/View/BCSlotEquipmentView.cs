﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BCSlotEquipmentView : MonoBehaviour
{
    public BCEquipmentView equipmenView;
    public bool isEquipment;
    public GameObject objectTip;
     
    public void Awake()
    {
        equipmenView.gameObject.SetActive(false);
        objectTip.gameObject.SetActive(false);
    }
    public void EnableTip(int id)
    {
        equipmenView.gameObject.SetActive(true);
        objectTip.gameObject.SetActive(true);
        Button button = objectTip.GetComponent<Button>();
        button.onClick.RemoveAllListeners();
        button.onClick.AddListener(() => SwapEquipment(id));
    }
    public void VisibleTip()
    {
        //equipmenView.gameObject.SetActive(false);
        objectTip.gameObject.SetActive(false);
        Button button = objectTip.GetComponent<Button>();
        button.onClick.RemoveAllListeners();
        //button.onClick.AddListener(() => SwapEquipment(id));
    }

    public void SwapEquipment(int id)
    {
        BCEquipmentController.api.AniEquip(id, equipmenView.gameObject);
    }

}
