﻿using Studio;
using Studio.BC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BCAchimentView : MonoBehaviour
{
    public Text2 txtStage;
    public Text2 txtChapter;
  
   public void setData(BCAchivement data)
    {
        txtStage.text = data.Stage.ToString();
        txtChapter.text = LanguageManager.api.GetKey(BCLocalize.GUI_NAME_CHAPTER, new string[] { data.Chapter.ToString() } );
    }
}
