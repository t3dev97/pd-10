﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BCUIBotItemView : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject nameButton;
    public GameObject imgButton;
    public int moveYTop=15;
    public Vector3 scale = new Vector3(1.1f,1.1f,1.1f);
    public float time =0.3f;
    private float tgMoveYTop;
    private Vector3 tgScale;
    public void Awake()
    {
        nameButton.SetActive(false);
        tgMoveYTop = gameObject.transform.position.y;
        tgScale = gameObject.transform.localScale;
    }
    public void AniTurnOn()
    {
        nameButton.SetActive(true);
        LeanTween.moveLocalY(imgButton, moveYTop, time);
        LeanTween.scale(imgButton, scale, time);
    }
    public void AniTurnOFF()
    {
        nameButton.SetActive(false);
        LeanTween.moveLocalY(imgButton, tgMoveYTop, time);
        LeanTween.scale(imgButton, tgScale, time);
    }

}
