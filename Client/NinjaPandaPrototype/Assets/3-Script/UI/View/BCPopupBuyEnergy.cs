﻿using Studio;
using Studio.BC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BCPopupBuyEnergy : BaseView
{
    public Button btnBuyEnergy;
    public Button btnViewAds;
    public Button btnClose;
    public void Awake()
    {
        btnBuyEnergy.onClick.AddListener(onClickBuyEnergy);
        btnViewAds.onClick.AddListener(OnClickViewAds);
        btnClose.onClick.AddListener(OnClose);
    }

    public void OnClose()
    {
        BCViewHelper.Api.PopupNotify.Hide(StateName.PopupBuyEnergy);
    }
    public void OnClickViewAds()
    {
        DebugX.Log("OnClickViewAds");
    }
    public void onClickBuyEnergy()
    {
        //huy hard code
        BCDataControler.api.converGemToEnergy(100,20);
        DebugX.Log("onClickBuyEnergy");
    }
}
