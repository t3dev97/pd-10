﻿using Studio;
using Studio.BC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BCEquipmentInfoView : BaseView
{
    public BCAnimationMain equipment;

    public ImageUIHelper imgTypeEquipment;
    public ImageUIHelper imgGradeEquipment;

    //public ImageUIHelper imgEquipment;
    //public ImageUIHelper imgMaterial;
    public BCDynamicImage imgEquipment;
    public BCDynamicImage imgMaterial;

    public Text2 NameMaterial;
    public Text2 AmountMaterial;
    public Text2 UseMaterial;
    public Text2 DecEquipment;

    public Text2 StatsMainEquipment;
    public Text2 StatsEquipment;

    public Text2 NameEquipment;
    public Text2 GradeEquipment;
    public Text2 txtLevel;
    public Text2 txtEquip;
    public Text2 txtGold;

    public Button btnEquip;
    public Button btnUpdate;
    public Button btnClose;

    public bool CheckEquipment;

    public void SetData(BCInventory data,bool isEquipment=false)
    {
        string statsEquipment = "";
        StatsEquipment.text = "";
        StatsMainEquipment.text = "";

        NameEquipment.text = LanguageManager.api.GetKey(data.IDItem.nameEquipment);
        GradeEquipment.text = LanguageManager.api.GetKey(data.IDItem.gradeEquipment.nameGrade);
        NameMaterial.text = LanguageManager.api.GetKey(BCCache.Api.DataConfig.MaterialConfigData[data.IDItem.typeEquipment.id].nameId) + " :";
        AmountMaterial.text = BCDataControler.api.data.listMaterials[data.IDItem.typeEquipment.id].amount.ToString() + " ";
        DecEquipment.text = LanguageManager.api.GetKey(data.IDItem.decEquipment);
        StatsMainEquipment.text = LanguageManager.api.GetKey(data.IDItem.mainPropertype.Dec_Effect, new string[] { data.GetEffectEquipment().ToString() }) + LanguageManager.api.GetKey(BCLocalize.COLOR_NAME_BLUE, new string[] { " ( + " + data.IDItem.bounusEnhance.ToString() + " )" });

        txtGold.text = "x"+BCCache.Api.DataConfig.materialUpdateConfigData[data.intLevel +1].requireGold.ToString();

        //UseMaterial.text = BCCache.Api.DataConfig.materialUpdateConfigData[data.intLevel+1].requireMaterial.ToString();
        UseMaterial.text = BCCache.Api.DataConfig.materialUpdateConfigData[data.intLevel + 1].requireMaterial.ToString();
        UpdateDataView(data);

        foreach (BCEffectEquipment tg in data.IDItem.subPropertype)
        {
            statsEquipment += LanguageManager.api.GetKey(tg.Dec_Effect, new string[] { tg.Value.ToString() }) + "\n";
        }
     
        if (isEquipment)
            txtEquip.text = LanguageManager.api.GetKey(BCLocalize.GUI_NAME_UNEQUIP);
        else
            txtEquip.text = LanguageManager.api.GetKey(BCLocalize.GUI_NAME_EQUIP);

        txtLevel.SetLocalize(BCLocalize.TEXT_NAME_LEVEL, new string[] { data.intLevel.ToString() });
        StatsEquipment.text = statsEquipment;
        imgTypeEquipment.SetSprite(data.IDItem.typeEquipment.AssetName);
        imgGradeEquipment.SetSprite(data.IDItem.gradeEquipment.assetName);


        BCEquipment equipment = data.IDItem;
        //imgType.SetSprite(equipment.typeEquipment.AssetName);
        //if (!Equip)
        //    imgType.gameObject.SetActive(false);
        imgGradeEquipment.SetSprite(equipment.gradeEquipment.assetName);


        //imgEquipment.SetSprite(data.IDItem.assetName);
        //imgMaterial.SetSprite(BCCache.Api.DataConfig.MaterialConfigData[data.IDItem.typeEquipment.id].assetName);
        imgEquipment.Type = EnumDynamicImageType.Inventory;
        imgEquipment.SetImage(data.IDItem.assetName, _isSetNativeSize: true);

        imgMaterial.Type = EnumDynamicImageType.Inventory;
        imgMaterial.SetImage(BCCache.Api.DataConfig.MaterialConfigData[data.IDItem.typeEquipment.id].assetName);

        CheckEquipment = isEquipment;
        setEventButton(data);

       

    }
    public void setEventButton(BCInventory data)
    {
        btnEquip.onClick.RemoveAllListeners();
        btnUpdate.onClick.RemoveAllListeners();
        btnClose.onClick.RemoveAllListeners();

        btnEquip.onClick.AddListener(() => OnClickEquip(data));
        btnUpdate.onClick.AddListener(() => OnClickUpdate(data));
        btnClose.onClick.AddListener(()=>OnClickClose());
    }
    public void OnClickClose()
    {
        BCViewHelper.Api.Popup.Hide(StateName.EquipmentInfo);
        BCEquipmentController.api.UpdateData();
    }

    public void OnClickEquip(BCInventory data)
    {
        if (!CheckEquipment)
        {
          
            BCEquipmentController.api.AniEquip(data.ID);
            //BCEquipmentController.api.UpdateEquipmentData(data);
        }
        else
        {
          
           BCEquipmentController.api.AniUnEquip(data.ID);
            //BCEquipmentController.api.UpdateEquipmentData(data, -1);
        }
           
        OnClickClose();
    }
    public void UpdateDataView(BCInventory data)
    {
        int goldCurrent = BCCache.Api.DataConfig.materialUpdateConfigData[BCDataControler.api.data.listIventory[data.ID].intLevel].requireGold;
        int goldNext = BCCache.Api.DataConfig.materialUpdateConfigData[BCDataControler.api.data.listIventory[data.ID].intLevel+1].requireGold;
        if (int.Parse(AmountMaterial.text) > int.Parse(UseMaterial.text) && BCDataControler.api.data.Gold > goldNext)
        {
            btnUpdate.interactable = true;
            ColorBlock colors = btnUpdate.colors;
            colors.colorMultiplier = 5;
            btnUpdate.colors = colors;
        }
        else
        {
            btnUpdate.interactable = false;
            ColorBlock colors = btnUpdate.colors;
            colors.colorMultiplier = 1;
            btnUpdate.colors = colors;
        }
    }
    public void OnClickUpdate(BCInventory data)
    {
        gameObject.GetComponent<BCAnimationMain>().Play(() => {

         


            btnUpdate.interactable = false;
            equipment.Play(() => {

                //data.intLevel++;
                //BCDataControler.api.UpdateEquipmentData(listEquipmentData[id], -1);

                //BCEquipmentController.api.UpdateEquipmentData(data, -1);
                //BCDataControler.api.UpdateEquipemnt(data.ID);
                //BCEquipmentController.api.UpdateEquipmentData(data);

                if (!CheckEquipment)
                {
                    //BCEquipmentController.api.AniEquip(data.ID);
                    //BCEquipmentController.api.UpdateEquipmentData(data);
                    BCDataControler.api.UpdateEquipemnt(data.ID);
                }
                else
                {
                    //BCEquipmentController.api.AniUnEquip(data.ID);
                    //BCEquipmentController.api.UpdateEquipmentData(data, -1);
                    BCEquipmentController.api.UpdateEquipmentData(data, -1);
                    BCDataControler.api.UpdateEquipemnt(data.ID);
                    BCEquipmentController.api.UpdateEquipmentData(data);
                }
                //data.listIventory[id].intLevel++;
                BCDataControler.api.MinusCoin(BCCache.Api.DataConfig.materialUpdateConfigData[BCDataControler.api.data.listIventory[data.ID].intLevel].requireGold);
                txtGold.text ="x"+ BCCache.Api.DataConfig.materialUpdateConfigData[BCDataControler.api.data.listIventory[data.ID].intLevel+1].requireGold.ToString();


                txtLevel.text = "";
                txtLevel.SetLocalize(BCLocalize.TEXT_NAME_LEVEL, new string[] { data.intLevel.ToString() });
                StatsMainEquipment.gameObject.GetComponent<BCAnimationMain>().Play(() =>
                {
                    StatsMainEquipment.text = "";
                    StatsMainEquipment.text = LanguageManager.api.GetKey(data.IDItem.mainPropertype.Dec_Effect, new string[] { data.GetEffectEquipment().ToString() }) +  LanguageManager.api.GetKey(BCLocalize.COLOR_NAME_BLUE, new string[] { " ( + " + data.IDItem.bounusEnhance.ToString() + " )" }) ;
                    UseMaterial.text= BCCache.Api.DataConfig.materialUpdateConfigData[BCDataControler.api.data.listIventory[data.ID].intLevel + 1].requireMaterial.ToString();
                    AmountMaterial.text = BCDataControler.api.data.listMaterials[data.IDItem.typeEquipment.id].amount.ToString();

                    //btnUpdate.interactable = true;
                    UpdateDataView(data);
                });

            });

        });
    }
   
}

