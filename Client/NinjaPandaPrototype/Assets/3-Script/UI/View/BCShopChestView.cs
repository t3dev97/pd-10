﻿using Studio;
using Studio.BC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class BCShopChestView : MonoBehaviour
{
    // Start is called before the first frame update
    public Image img;
    public Button btnOpen;
    public Text2 txtNameChest;
    public Text2 txtDecChest;
    public Text2 txtGem;



    public void Parse(ChestShopConfigData data)
    {
        btnOpen.onClick.AddListener(() => ButtonClick(data.PriceText, data.ID,data));
        img.gameObject.GetComponent<ImageUIHelper>().SetSprite(data.AssetName);
        img.SetNativeSize();
        txtNameChest.text = LanguageManager.api.GetKey(data.NameID); 
        txtDecChest.text = LanguageManager.api.GetKey(data.DecID);
        txtGem.text = "X "+data.PriceText;
    }
    void ButtonClick(string gem,int id, ChestShopConfigData data)
    {
        BCDataControler.api.MinusGem(int.Parse(gem),()=> {

            BCViewHelper.Api.PopupNotify.Show(StateName.OpenChestFX, (go) => {

                int idItem = Random.RandomRange(1, BCCache.Api.DataConfig.EquipmentConfigData.Count);

                BCDataControler.api.AddInventoryView(idItem);
           

                BCOpenChestView view = go.GetComponent<BCOpenChestView>();
                view.Init(BCDataControler.api.data.listIventory[BCDataControler.api.data.listIventory.Count].IDItem, id,data);
            });

        });

      

       
    }
}
