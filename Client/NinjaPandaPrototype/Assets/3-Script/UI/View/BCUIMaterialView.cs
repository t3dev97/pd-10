﻿using Studio;
using Studio.BC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class BCUIMaterialView : MonoBehaviour
{
    public Text2 numAmount;
    public BCDynamicImage imgDynamicMaterial;
    public Button btnClick;

    public void setdata(BCMaterialResource data)
    {
        numAmount.text = "x"+data.amount.ToString();

        //imgMaterial.SetSprite(data.material.assetName);
        imgDynamicMaterial.Type = EnumDynamicImageType.Inventory;
        imgDynamicMaterial.SetImage(data.material.assetName);
        imgDynamicMaterial.GetComponent<Image>().SetNativeSize();
        btnClick.onClick.AddListener(()=> { ShowPopup(data.material.description); });
    }
   
    public void ShowPopup(string content)
    {
        //BCViewHelper.Api.System.ShowMessage(content);
        BCViewHelper.Api.ShowMessageBoxCustom(LanguageManager.api.GetKey(content));
    }
}
