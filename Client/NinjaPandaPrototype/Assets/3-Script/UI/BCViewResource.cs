﻿using Studio.BC;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BCViewResource : MonoBehaviour
{
    public ResourceGame type;
    public Text txtValue;
    public int value;
    public Button Button;
    public Image fillExp;

    IEnumerator delayTime(float time ,Action action)
    {
        action.Invoke();
        yield return new WaitForSeconds(time);
    }
    public void UpdateResource()
    {
        switch (type)
        {
            case ResourceGame.Energy: value = BCDataControler.api.GetEnergy(); ; break;
            case ResourceGame.Gem: value = BCDataControler.api.GetGem(); break;
            case ResourceGame.Gold: value = BCDataControler.api.GetCoin(); break;
            case ResourceGame.Level:
                value = BCDataControler.api.GetLevel();
                float valueFill = 0;
                valueFill =( BCDataControler.api.GetExp()*1.0f) / BCCache.Api.DataConfig.UserLevelConfigData[BCDataControler.api.GetLevel()].expRequire;
                fillExp.fillAmount = valueFill;
                break;
        }
        txtValue.text = value.ToString();
    }
}
