﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BCMoveCamTestArt : MonoBehaviour
{
    public  GameObject player;
    public  Vector3 distanceV3;
    public float distance;
    public Vector3 posCam;
    public float posX;
    public static int Width = 800;
    public static int Height = 1280;
    public static float Size = 10f;
    public static float MinCamY;
    public static float MaxCamY;
    public void Awake()
    {
        posCam = gameObject.transform.position;
        distance = player.transform.position.z- posCam.z;
        
    }
    public void Update()
    {

        Vector3 vec = new Vector3(player.transform.position.x,
            gameObject.transform.position.y,
            player.transform.position.z - distance);
        vec = Vector3.MoveTowards(gameObject.transform.position, vec, 1);
        gameObject.transform.position = vec;
    }
   
}
