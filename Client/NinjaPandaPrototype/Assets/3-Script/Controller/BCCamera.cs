﻿using UnityEngine;

public class BCCamera : MonoBehaviour
{

    [SerializeField]
    private Transform target;

    [SerializeField]
    private Vector3 offsetPosition;

    [SerializeField]
    private Space offsetPositionSpace = Space.Self;

    [SerializeField]
    private bool lookAt = true;
    [SerializeField]
    private float smoote;

    private void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player").transform;
    }
    private void LateUpdate()
    {
        Refresh();
    }

    public void Refresh()
    {
        if (target == null)
        {
            Debug.LogWarning("Missing target ref !", this);

            return;
        }

        // compute position
        if (offsetPositionSpace == Space.Self)
        {
            //if (target.AddToggleToList<PlayerController>().checkColler)
            transform.position = Vector3.Lerp(transform.position, target.TransformPoint(offsetPosition), Time.deltaTime * smoote);
            //else
            //transform.position = Vector3.MoveTowards(transform.position, target.TransformPoint(ChangeAxis(offsetPosition)), Time.deltaTime * smoote);
        }
        else // world
        {
            //transform.position = target.position + ChangeAxis(offsetPosition);
            transform.position = Vector3.Lerp(transform.position, target.position + offsetPosition, Time.deltaTime * smoote);
        }

        //compute rotation
        if (lookAt)
        {
            transform.LookAt(target);
        }
        else
        {
            transform.rotation = target.rotation;
        }
    }
}

