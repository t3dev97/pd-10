﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public enum Status
{
    live,
    die
}
public class BCCharacterDetail : MonoBehaviour
{
    public static BCCharacterDetail api;
    public float HealtBasic = 500;
    public float CurrentHealt = 100;
    //[SerializeField]
    public Image healtBar;
    public Status GetStatus = Status.live;
    private Animator anim;
    public float Healt
    {
        get { return CurrentHealt; }
        set
        {
            CurrentHealt = value;
            if (CurrentHealt > 1)
                healtBar.fillAmount = CurrentHealt / HealtBasic;
        }
    }

    private void Awake()
    {
        if (api == null)
            api = this;
        else
            Destroy(api);

        anim = GetComponent<Animator>();
    }

    public void ChangeHealt(float reduceHealt)
    {
        //Debug.Log("Hit");
        Healt -= reduceHealt;

        StartCoroutine(BC_GameController.api.Camera.GetComponent<BCMoveCam>().Shake(1f, 1f));
        if (Healt < 1)
        {
            Die();
            return;
        }
        StartCoroutine(HitRoutine());
    }
    public void Die()
    {
        if (anim == null)
        {
            Debug.LogError("Not Player");
            return;
        }
        anim.SetInteger(BCKeyAnimator.Condition, (int)AnimationType.die);
        GetStatus = Status.die;
        Destroy(gameObject, 3.0f);
        //StartCoroutine(HidePlayer());
    }

    IEnumerator HidePlayer()
    {
        yield return new WaitForSeconds(3.0f);
        gameObject.SetActive(false);
    }


    IEnumerator HitRoutine()
    {
        yield return new WaitForSeconds(0.7f);
        anim.SetInteger(BCKeyAnimator.Condition, (int)AnimationType.hit);
        yield return new WaitForSeconds(0.5f);
        anim.SetInteger(BCKeyAnimator.Condition, (int)AnimationType.idle);
    }
}
