﻿using UnityEngine;

public enum StatusMonsterEmun
{
    none,
    seleced
}

public class BCStatusMonster : MonoBehaviour
{
    //[HideInInspector]
    public StatusMonsterEmun VStatus;

    public void ChangeStatus(StatusMonsterEmun statusOld, StatusMonsterEmun statusNew)
    {
        //Debug.Log(1);
        if (statusOld == statusNew) return;
        VStatus = statusNew;
        switch (statusNew)
        {
            case StatusMonsterEmun.none:
                //gameObject.AddToggleToList<Renderer>().material.color = Color.white;
                transform.GetChild(0).gameObject.SetActive(false);
                break;
            case StatusMonsterEmun.seleced:
                //gameObject.AddToggleToList<Renderer>().material.color = Color.yellow;
                transform.GetChild(0).gameObject.SetActive(true);
                break;
        }
    }
}
