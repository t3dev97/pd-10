﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeantweenController {
	static LeantweenController _api;
	public static LeantweenController Api
	{
		get
		{
			if (_api == null)
				_api = new LeantweenController();
			return _api;
		}
	}

	Dictionary<int, LTDescr> _lt;
	Dictionary<int, LTDescr> lts
	{
		get
		{
			if (_lt == null)
				_lt = new Dictionary<int, LTDescr>();
			return _lt;
		}
	}

	Dictionary<int, LTSeq> _ltseq;
	Dictionary<int, LTSeq> ltseqs
	{
		get
		{
			if (_ltseq == null)
				_ltseq = new Dictionary<int, LTSeq>();
			return _ltseq;
		}
	}
	Dictionary<int, Action> _callBacks;
	Dictionary<int, Action> callBacks
	{
		get
		{
			if (_callBacks == null)
				_callBacks = new Dictionary<int, Action>();
			return _callBacks;
		}
	}
	int currentId;

	public void Run(LTDescr lt, Action onCompleteAction = null)
	{
		int id = GenerateId();
		if (onCompleteAction != null)
			callBacks.Add(id, onCompleteAction);
		lts.Add(id, lt);
		Destroy(id, lt);
	}

	public int Run(LTSeq lt, Action onCompleteAction = null)
	{
		int id = GenerateId();
		if (onCompleteAction != null)
			callBacks.Add(id, onCompleteAction);
		ltseqs.Add(id, lt);
		Destroy(id, lt);
        return id;
	}

    public void ForceDestroy(int id)
    {
        if (LeanTween.descr(id) != null) {
            LeanTween.cancel(id);
        }
        
        lts.Remove(id);
        if (callBacks.ContainsKey(id))
        {
            callBacks[id].Invoke();
            callBacks.Remove(id);
        }
    }

    void Destroy(int id, LTDescr lt)
	{
		lt.setOnComplete(() =>
		{
			LeanTween.cancel(lt.id);
			lts.Remove(id);
			if (callBacks.ContainsKey(id))
			{
				callBacks[id].Invoke();
				callBacks.Remove(id);
			}
		});
	}

	void Destroy(int id, LTSeq lt)
	{
		lt.tween.setOnComplete(() =>
		{
			LeanTween.cancel(lt.id);
			lts.Remove(id);
			if (callBacks.ContainsKey(id))
			{
				callBacks[id].Invoke();
				callBacks.Remove(id);
			}
		});
	}

	int GenerateId()
	{
		if (currentId == 0)
			currentId = lts.Count + ltseqs.Count;
		else
			currentId++;
		return currentId;
	}
}
