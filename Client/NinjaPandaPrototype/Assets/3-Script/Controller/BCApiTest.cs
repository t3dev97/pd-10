﻿
using Studio.BC;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class BCApiTest : MonoBehaviour
{
    public static BCApiTest api;
    public string strGem = "";

    private int width = 200;
    private int height = 50;
    public int mGem = 0;

    public bool isEnableTest = false;
    public bool isShowAddGame = false;
    public bool isShowSkill = false;
    public bool isLoadData = false;
    public bool isShowListSkill = false;
    public bool isShowInfoChapter = false;
    public bool isShowSpamMonster = false;
    public bool isGoToTest = false;
    public bool isAutoAttack = true;

    public GUILayoutOption opWidth;
    public GUILayoutOption opHeight;
    public GUIStyle styleText;
    public int idListSkill = 0;
    public int idListQuai = 0;
    public Dictionary<int, string> ListTypeSkill;
    public Dictionary<int, string> ListSkillMutil;
    public Dictionary<int, string> ListSkillParamter;
    public Dictionary<int, string> ListSkillSpecial;

    public Dictionary<int, string> ListSkillSummon;
    public Dictionary<int, string> ListSkillEffect;
    public Dictionary<int, string> ListSkillPassive;

    [Header("Bullet Test")]
    [SerializeField]
    public GameObject[] listBullet;
    [SerializeField]
    public int indexBullet = 0;


    public void SetOffAllButtonShow()
    {
        //isEnableTest = false;
        isShowAddGame = false;
        isShowSkill = false;
    }
    public void Awake()
    {
        if (api == null)
            api = this;
        else
            Destroy(this);

        styleText = new GUIStyle("button");
        styleText.fontSize = 25;

        opWidth = GUILayout.Width(width);
        opHeight = GUILayout.Height(height);

        DontDestroyOnLoad(gameObject);
    }
#if EDITOR
    public void OnGUI()
    {
        GUILayout.Space(40);
        GUILayout.BeginHorizontal();
        GUILayout.BeginVertical();

        if (isEnableTest)
        {
            EnableTool();

            GUILayout.BeginVertical();
            ShowGoToTest();
            GUILayout.EndVertical();

        }
        else
        {
            DisableTool();
        }

        GUILayout.EndVertical();

        ShowToolSelection();

        GUILayout.EndHorizontal();
    }
#endif
    public void ShowToolSelection()
    {
        GUILayout.BeginVertical();
        if (isShowAddGame)
        {
            EnableShowAddGame();
        }
        GUILayout.EndVertical();


        GUILayout.BeginVertical();
        if (isShowSkill)
        {
            EnableShowAddSkill();
        }
        GUILayout.EndVertical();


        GUILayout.BeginVertical();
        if (isShowSkill)
        {
            EnableShowListSkill();
        }
        GUILayout.EndVertical();

        GUILayout.BeginVertical();
        if (isShowSpamMonster)
        {
            EnableShowListMonster();
        }
        GUILayout.EndVertical();
        GUILayout.BeginVertical();
        if (isShowSpamMonster)
        {
            EnableShowListMonsterBrunsh();
        }
        GUILayout.EndVertical();

    }
    public void ShowGoToTest()
    {
        string str = "Go To Test";
        if (!isGoToTest)
            str = "Go To Main";
        if (!SceneManager.GetActiveScene().name.Equals("Main"))
        {
            if (GUILayout.Button(str, styleText, new GUILayoutOption[] { opWidth, opHeight }))
            {
                if (isGoToTest)
                    isGoToTest = false;
                else
                    isGoToTest = true;
            }
        }
        if (isGoToTest && (SceneManager.GetActiveScene().name.Equals("Main") || SceneManager.GetActiveScene().name.Equals("MainArt")))
        {
            string str_ = "Auto Attack";
            if (!isAutoAttack)
                str_ = "Not Auto Attack";

            if (GUILayout.Button(str_, styleText, new GUILayoutOption[] { opWidth, opHeight }))
            {
                if (isAutoAttack)
                {
                    isAutoAttack = false;
                    //if (listMonsterInStage[0] != null)
                    //{
                    //    listMonsterInStage[0].transform.position = new Vector3(0, 0, posA);
                    //}

                    //if (listMonsterInStage[1] != null)
                    //{
                    //    listMonsterInStage[1].transform.position = new Vector3(0, 0, posB);
                    //}
                }
                else
                {
                    isAutoAttack = true;


                    //GUILayout.EndVertical();
                }
            }
            GUILayout.BeginVertical();
            if (!isAutoAttack)
            {
                if (SceneManager.GetActiveScene().name.Equals("Main"))
                {
                    if (listMonsterInStage[0] != null)
                    {
                        //Debug.Log(2);
                        if (GUILayout.Button(listMonsterInStage[0].name, styleText, new GUILayoutOption[] { opWidth, opHeight }))
                        {
                            if (listMonsterInStage[0].GetComponent<ShootSkill>() != null)
                                StartCoroutine(listMonsterInStage[0].GetComponent<ShootSkill>().SkillStart());
                        }
                    }

                    if (listMonsterInStage[1] != null)
                    {
                        //Debug.Log(3);
                        if (GUILayout.Button(listMonsterInStage[1].name, styleText, new GUILayoutOption[] { opWidth, opHeight }))
                        {
                            StartCoroutine(listMonsterInStage[1].GetComponent<ShootSkill>().SkillStart());
                        }
                    }
                }
                else if (SceneManager.GetActiveScene().name.Equals("MainArt"))
                {
                    List<GameObject> listMonster = new List<GameObject>();
                    listMonster.AddRange(GameObject.FindGameObjectsWithTag("Monster"));
                    foreach (var item in listMonster)
                    {
                        if (GUILayout.Button(item.name, styleText, new GUILayoutOption[] { opWidth, opHeight }))
                        {
                            StartCoroutine(item.GetComponent<ShootSkill>().SkillStart());
                        }
                    }
                }
            }
            GUILayout.EndVertical();
        }
    }
    GameObject[] listMonsterInStage = new GameObject[2];
    int index = 1;
    Vector3 posMonster = Vector3.zero;
    float posA = 6;
    float posB = -3;
    public void EnableShowListMonsterBrunsh()
    {
        int[] dsquaispam = { 19, 35, 34, 32, 9, 10, 5, 2, 3, 30, 31, 47, 48, 0, 28, 29, 25, 26, 27 };
        int[] dsquaispamBoss = { 25, 26, 12, 3, 1, 27/*, 12, 3, 1,36,27*/ };

        if (idListQuai == 1)
        {
            foreach (int i in dsquaispam)
            {
                if (GUILayout.Button(BC_DataGame.api.currentTileSetModeMoster[i].name, styleText, opWidth, opHeight))
                {
                    GameObject game = BC_Chapter_Info.api.AddListStageMonster(Instantiate(BC_DataGame.api.currentTileSetModeMoster[i]));
                    BC_Chapter_Info.api.listStageMonter[0].listMonster.Add(game);

                    if (index >= 2)
                    {
                        index = 0;
                        posMonster = new Vector3(0, 0, posB);
                    }
                    else
                        posMonster = new Vector3(0, 0, posA);

                    game.transform.position = posMonster;

                    if (listMonsterInStage[index] != null)
                        Destroy(listMonsterInStage[index]);


                    listMonsterInStage[index] = game;
                    ++index;

                    isShowSpamMonster = false;
                }
            }
        }
        if (idListQuai == 2)
        {
            foreach (int i in dsquaispamBoss)
            {
                if (GUILayout.Button(BC_DataGame.api.currentTileSetModeMosterBoos[i].name, styleText, opWidth, opHeight))
                {
                    GameObject game = BC_Chapter_Info.api.AddListStageMonster(Instantiate(BC_DataGame.api.currentTileSetModeMosterBoos[i]));
                    BC_Chapter_Info.api.listStageMonter[0].listMonster.Add(game);
                    game.transform.position = new Vector3(0, 0, 0);
                    isShowSpamMonster = false;
                }
            }
        }
    }

    public void EnableShowListMonster()
    {

        if (GUILayout.Button("Monster", styleText, opWidth, opHeight))
        {
            idListQuai = 1;
        }
        if (GUILayout.Button("Boos", styleText, opWidth, opHeight))
        {
            idListQuai = 2;
        }


    }
    public void EnableShowListSkill()
    {
        if (isShowListSkill)
        {
            ShowListSkill();
        }
    }
    public void ShowListSkill()
    {
        Dictionary<int, string> listSkillShow = new Dictionary<int, string>();
        switch (idListSkill)
        {
            case (int)EnumSkillType.AddParameter: listSkillShow = ListSkillParamter; break;
            case (int)EnumSkillType.EffectAttack: listSkillShow = ListSkillEffect; break;
            case (int)EnumSkillType.MutilAttack: listSkillShow = ListSkillMutil; break;
            case (int)EnumSkillType.Passive: listSkillShow = ListSkillPassive; break;
            case (int)EnumSkillType.SkillSummoner: listSkillShow = ListSkillSummon; break;
            case (int)EnumSkillType.Special: listSkillShow = ListSkillSpecial; break;
        }

        foreach (var skill in listSkillShow)
        {
            if (GUILayout.Button(skill.Value, styleText, opWidth, opHeight))
            {
                isShowListSkill = false;
                PlayerController.api.AddSkill(skill.Key);
            }
        }
    }

    public void EnableShowAddSkill()
    {
        LoadData();
        GUILayout.BeginVertical();
        foreach (var tg in ListTypeSkill)
        {
            if (GUILayout.Button(tg.Value, styleText, opWidth, opHeight))
            {
                isShowListSkill = true;
                switch (tg.Key)
                {
                    case (int)EnumSkillType.AddParameter: idListSkill = (int)EnumSkillType.AddParameter; break;
                    case (int)EnumSkillType.EffectAttack: idListSkill = (int)EnumSkillType.EffectAttack; break;
                    case (int)EnumSkillType.MutilAttack: idListSkill = (int)EnumSkillType.MutilAttack; break;
                    case (int)EnumSkillType.Passive: idListSkill = (int)EnumSkillType.Passive; break;
                    case (int)EnumSkillType.SkillSummoner: idListSkill = (int)EnumSkillType.SkillSummoner; ; break;
                    case (int)EnumSkillType.Special: idListSkill = (int)EnumSkillType.Special; break;
                }
            }
        }
        GUILayout.EndVertical();
    }
    public void LoadData()
    {

        if (!isLoadData)
        {
            isLoadData = true;

            Dictionary<int, BCSkillBaseData> data = BCCache.Api.DataConfig.SkillBases;

            ListTypeSkill = new Dictionary<int, string>();
            ListSkillMutil = new Dictionary<int, string>();
            ListSkillParamter = new Dictionary<int, string>();
            ListSkillSpecial = new Dictionary<int, string>();
            ListSkillSummon = new Dictionary<int, string>();
            ListSkillEffect = new Dictionary<int, string>();
            ListSkillPassive = new Dictionary<int, string>();

            foreach (var skill in data)
            {
                if (!ListTypeSkill.ContainsKey((int)skill.Value.TypeSkill))
                {
                    ListTypeSkill.Add((int)skill.Value.TypeSkill, Enum.GetName(typeof(EnumSkillType), skill.Value.TypeSkill));
                }
                switch (skill.Value.TypeSkill)
                {
                    case EnumSkillType.AddParameter: ListSkillParamter.Add(skill.Value.id, Enum.GetName(typeof(EnumSkillAddParameterID), skill.Value.id)); break;
                    case EnumSkillType.EffectAttack: ListSkillEffect.Add(skill.Value.id, Enum.GetName(typeof(EnumSkillEffectAttackID), skill.Value.id)); break;
                    case EnumSkillType.MutilAttack: ListSkillMutil.Add(skill.Value.id, Enum.GetName(typeof(EnumSkillMutilAttackID), skill.Value.id)); break;
                    case EnumSkillType.Passive: ListSkillPassive.Add(skill.Value.id, Enum.GetName(typeof(EnumSkillPassiveID), skill.Value.id)); break;
                    case EnumSkillType.SkillSummoner: ListSkillSummon.Add(skill.Value.id, Enum.GetName(typeof(EnumSkillSummon), skill.Value.id)); break;
                    case EnumSkillType.Special: ListSkillSpecial.Add(skill.Value.id, Enum.GetName(typeof(EnumSkillSpecical), skill.Value.id)); break;
                }
            }

        }
    }

    public void EnableTool()
    {
        if (GUILayout.Button("Disable Tool", styleText, opWidth, opHeight))
        {
            isEnableTest = !isEnableTest;
        }
        if (GUILayout.Button("Add Gem", styleText, opWidth, opHeight))
        {
            //SetOffAllButtonShow();
            isShowAddGame = !isShowAddGame;
        }
        if (SceneManager.GetActiveScene().name.Equals("Main"))
        {
            if (GUILayout.Button("Add Skill", styleText, opWidth, opHeight))
            {
                //SetOffAllButtonShow();
                isShowSkill = !isShowSkill;
            }

            if (GUILayout.Button("Spam quái", styleText, opWidth, opHeight))
            {
                //SetOffAllButtonShow();
                isShowSpamMonster = !isShowSpamMonster;
            }

        }
    }

    public void EnableShowAddGame()
    {

        GUILayout.BeginHorizontal();
        strGem = GUILayout.TextField(mGem.ToString(), styleText, opWidth, opHeight);
        int.TryParse(strGem, out mGem);
        if (GUILayout.Button("Add", styleText, opWidth, opHeight))
        {
            BCDataControler.api.PlusGem(int.Parse(strGem));
            isShowAddGame = !isShowAddGame;
        }
        GUILayout.EndHorizontal();
    }

    public void DisableTool()
    {

        if (GUILayout.Button("Enable Tool", styleText, opWidth, opHeight))
        {
            isEnableTest = !isEnableTest;
        }
    }


}
