﻿using Studio;
using Studio.BC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BCPopupThienThanController : MonoBehaviour
{
    static public BCPopupThienThanController api;

    [Header("Skill Right")]
    public BCDynamicImage ImgSkill;
    public Text2 NameSkill;
    public Transform Content_1;
    int _listNumber_1;
    int _listNumber_2;

    [Header("Skill Left")]
    public BCDynamicImage ImgItem;
    public Text2 NameItem;
    public Transform Content_2;

    [Header("Button")]
    public Button BtnUseGem;
    public Button BtnWatchAds;
    public Button BtnLeft;
    public Button BtnRight;

    [Header("")]
    public Text2 TxtUseItem;
    public int Gem;

    int skill_2 = 5;
    int skill_1 = 4;
    RectTransform _rectTransform;
    private void Awake()
    {
        if (api == null)
        {
            api = this;
        }
        else
            Destroy(this);
        _rectTransform = GetComponent<RectTransform>();
    }
    private void OnEnable()
    {
        _rectTransform.anchoredPosition = Vector3.zero;
    }
    private void Start()
    {
        Init();
    }
    public void Init()
    {
        Content_1.ClearAllChild();
        Content_2.ClearAllChild();
        _listNumber_1 = 0;
        _listNumber_2 = 0;

        List<int> listSkill_1 = BCCache.Api.DataConfig.StageInfos[BC_DataGame.api.currentIDMap].AngleSkill_1; // danh sach skill co trong stage
        List<int> listSkill_2 = BCCache.Api.DataConfig.StageInfos[BC_DataGame.api.currentIDMap].AngleSkill_2; // danh sach skill co trong stage

        // skill 1
        #region Skill 1
        List<int> tg = new List<int>();
        foreach (var idSkill in listSkill_1)
        {
            if (BCCache.Api.DataConfig.SkillBases[idSkill].Level <= BCCache.Api.DataConfig.SkillBases[idSkill].limitSkill)
            {
                tg.Add(idSkill);
            }

            var skill = Instantiate(ImgSkill, Content_1);
            skill.gameObject.SetActive(true);
            skill.name = "skill_item_" + idSkill;
            skill.Type = EnumDynamicImageType.Skill;
            skill.GetComponent<BCDynamicImage>().SetImage(BCCache.Api.DataConfig.SkillBases[idSkill].iconSkill);
        }
        listSkill_1 = tg;

        skill_1 = listSkill_1[Random.Range(0, listSkill_1.Count)];

        //var skill_temp = Content_1.Find("skill_item_" + skill_1);
        //if (skill_temp != null)
        //{
        //    Destroy(skill_temp);
        //}

        var skillItem1 = Instantiate(ImgSkill, Content_1);
        skillItem1.gameObject.SetActive(true);
        skillItem1.Type = EnumDynamicImageType.Skill;
        skillItem1.GetComponent<BCDynamicImage>().SetImage(BCCache.Api.DataConfig.SkillBases[skill_1].iconSkill);

        NameSkill.text = LanguageManager.api.GetKey(BCCache.Api.DataConfig.SkillBases[skill_1].nameSkill);
        NameSkill.gameObject.SetActive(false);
        skillItem1.transform.SetAsFirstSibling();

        _listNumber_1 = Content_1.childCount;
        #endregion

        // skill 2
        #region Skill 2
        tg.Clear();

        foreach (var idItem in listSkill_2)
        {
            if (BCCache.Api.DataConfig.SkillBases[idItem].Level <= BCCache.Api.DataConfig.SkillBases[idItem].limitSkill)
            {
                tg.Add(idItem);
            }

            var skill = Instantiate(ImgItem, Content_2);
            skill.gameObject.SetActive(true);
            skill.name = "item_" + idItem;
            skill.Type = EnumDynamicImageType.Skill;
            skill.GetComponent<BCDynamicImage>().SetImage(BCCache.Api.DataConfig.SkillBases[idItem].iconSkill);
        }
        listSkill_2 = tg;

        skill_2 = listSkill_2[Random.Range(0, listSkill_2.Count)];

        //var skill_temp_2 = Content_2.Find("item_" + skill_2);
        //if (skill_temp_2 != null)
        //{
        //    Destroy(skill_temp_2);
        //}

        var skillItem2 = Instantiate(ImgSkill, Content_2);
        skillItem2.gameObject.SetActive(true);
        skillItem2.Type = EnumDynamicImageType.Skill;
        skillItem2.GetComponent<BCDynamicImage>().SetImage(BCCache.Api.DataConfig.SkillBases[skill_2].iconSkill);

        NameItem.text = LanguageManager.api.GetKey(BCCache.Api.DataConfig.SkillBases[skill_2].nameSkill);
        NameItem.gameObject.SetActive(false);
        skillItem2.transform.SetAsFirstSibling();

        _listNumber_2 = Content_2.childCount;
        #endregion

        BtnUseGem.interactable = false;
        BtnWatchAds.interactable = false;

        if (BCCache.Api.DataConfig.PopupThienThanConfig[BC_Chapter_Info.api.IDMap].Gem > 0)
            TxtUseItem.text = LanguageManager.api.GetKey((BCLocalize.POPUP_THIEN_THAN_GEM), new string[] { BCCache.Api.DataConfig.PopupThienThanConfig[BC_Chapter_Info.api.IDMap].Gem.ToString() });
        else
            BtnUseGem.gameObject.SetActive(false);

        if (BCCache.Api.DataConfig.PopupThienThanConfig[BC_Chapter_Info.api.IDMap].Ads_rate <= 0)
            BtnWatchAds.gameObject.SetActive(false);

        LeanTween.moveLocalY(Content_1.gameObject, (Content_1.childCount) * (Content_1.GetComponent<VerticalLayoutGroup>().spacing + ImgSkill.GetComponent<RectTransform>().rect.height), 0f);
        LeanTween.moveLocalY(Content_2.gameObject, (Content_2.childCount) * (Content_2.GetComponent<VerticalLayoutGroup>().spacing + ImgItem.GetComponent<RectTransform>().rect.height), 0f);

        StartCoroutine(StartSpinning());
    }
    IEnumerator StartSpinning(/*System.Action a*/)
    {
        yield return new WaitForEndOfFrame();

        LeanTween.moveLocalY(Content_1.gameObject, 0, 1f).setEaseInOutSine();
        LeanTween.moveLocalY(Content_2.gameObject, 0, 0.5f).setEaseInOutSine();

        yield return new WaitForSeconds(1f);

        NameSkill.gameObject.SetActive(true);
        NameItem.gameObject.SetActive(true);

        // button
        BtnUseGem.GetComponent<Button>().onClick.AddListener(() =>
        {
            if (BCDataControler.api.MinusGemWithChecker(BCCache.Api.DataConfig.PopupLevelUpConfig[PlayerDetail.api.Level].Gem))
            {
                AddAllSkill();
            }
        });

        BtnWatchAds.GetComponent<Button>().onClick.AddListener(() =>
        {
            //BCViewHelper.Api.ShowMessageBox(LanguageManager.api.GetKey(BCLocalize.POPUP_NOT_CONNET_NETWORK));
            AddAllSkill();
        });

        BtnLeft.GetComponent<Button>().AddClickListener(() =>
        {
            BtnLeft.interactable = false;
            PlayerController.api.AddSkill(skill_1);
            BCPopupManager.api.Hide(PopupName.PopupThienThan, (go) =>
            {
                if (gameObject.GetComponent<BCPopupDetail>() != null && gameObject.GetComponent<BCPopupDetail>().ObjCallBack != null)
                    gameObject.GetComponent<BCPopupDetail>().Hide();
                else
                    Debug.LogError("Obj null");
            });
        });

        BtnRight.GetComponent<Button>().AddClickListener(() =>
        {
            BtnRight.interactable = false;
            PlayerController.api.AddSkill(skill_2);
            BCPopupManager.api.Hide(PopupName.PopupThienThan, (go) =>
            {
                if (gameObject.GetComponent<BCPopupDetail>() != null && gameObject.GetComponent<BCPopupDetail>().ObjCallBack != null)
                    gameObject.GetComponent<BCPopupDetail>().Hide();
                else
                    Debug.LogError("Obj null");
            });
        });
        //a?.Invoke();

        if (BCCache.Api.DataConfig.PopupThienThanConfig[BC_Chapter_Info.api.IDMap].Gem > 0)
        {
            BtnUseGem.interactable = true;
        }

        float rd = Random.Range(0.0f, 1.0f);
        if (rd < BCCache.Api.DataConfig.PopupThienThanConfig[BC_Chapter_Info.api.IDMap].Ads_rate)
        {
            BtnWatchAds.interactable = true;
        }
    }
    public void RandomSkill(Dictionary<int, Skill> skills)
    {
        do
        {
            skill_2 = Random.Range(1, skills.Count + 1);
            skill_1 = Random.Range(1, skills.Count + 1);

        } while (skill_2 == skill_1);
    }

    void AddAllSkill()
    {
        PlayerController.api.AddSkill(skill_2);
        BCPopupManager.api.Hide(PopupName.PopupThienThan, (go) =>
        {
            if (gameObject.GetComponent<BCPopupDetail>() != null && gameObject.GetComponent<BCPopupDetail>().ObjCallBack != null)
                gameObject.GetComponent<BCPopupDetail>().Hide();
            else
                Debug.LogError("Obj null");
        });

        PlayerController.api.AddSkill(skill_1);
        BCPopupManager.api.Hide(PopupName.PopupThienThan, (go) =>
        {
            if (gameObject.GetComponent<BCPopupDetail>() != null && gameObject.GetComponent<BCPopupDetail>().ObjCallBack != null)
                gameObject.GetComponent<BCPopupDetail>().Hide();
            else
                Debug.LogError("Obj null");
        });
    }

}
