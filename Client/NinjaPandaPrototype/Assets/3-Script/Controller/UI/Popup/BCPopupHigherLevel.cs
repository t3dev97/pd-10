﻿using Studio;
using Studio.BC;
using UnityEngine;
using UnityEngine.UI;

public class BCPopupHigherLevel : MonoBehaviour
{
    public Text2 txtLevel;
    public GameObject Reward;
    public Button btnClose;
    RectTransform _rectTransform;
    private void Awake()
    {
        _rectTransform = GetComponent<RectTransform>();
    }
    private void OnEnable()
    {
        _rectTransform.anchoredPosition = Vector3.zero;

        btnClose.GetComponent<Button>().AddClickListener(() =>
        {
            BCPopupManager.api.Hide(PopupName.PopupHigherLevel);
        });
    }
}
