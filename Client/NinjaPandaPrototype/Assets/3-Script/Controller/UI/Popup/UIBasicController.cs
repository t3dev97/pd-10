﻿using Studio;
using Studio.BC;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIBasicController : MonoBehaviour
{
    static public UIBasicController api;
    public GameObject EXPObject;
    public Slider SliderLV;
    public Text2 TxtLevelPlayerShow;
    public Text2 TxtLevelPlayer;
    public TextMeshPro2 TxtLevelTMP;
    public Text TxtGold;
    public TextMeshProUGUI TmpGold;
    public TextMeshPro2 TmpStage;

    [Header("Healt Boss")]
    [SerializeField]
    GameObject healtBoss;

    [SerializeField]
    Slider sliderHealtBoss;

    [Header("Skill Detail")]
    public GameObject DetailSkill;
    public Text2 TxtNameSkill;
    public Text2 TxtDescriptionSkill;
    public Text2 TxtDescriptionShadowSkill;
    public GameObject HealtBoss { get => healtBoss; set => healtBoss = value; }
    public Slider SliderHealtBoss { get => sliderHealtBoss; set => sliderHealtBoss = value; }
    private void Awake()
    {
        if (api == null)
            api = this;
        else
            Destroy(this);

        DetailSkill = GameObject.Find("DetailSkill").gameObject;
        //TxtGold = GameObject.Find("TxtGold").GetComponent<Text>();
        SliderLV = GameObject.Find("sldLevel").GetComponent<Slider>();
        TxtNameSkill = GameObject.Find("TxtNameSkill").GetComponent<Text2>();
        TxtDescriptionSkill = GameObject.Find("TxtDescriptionSkill").GetComponent<Text2>();
        TxtDescriptionShadowSkill = GameObject.Find("TxtDescriptionShadowSkill").GetComponent<Text2>();
        EXPObject = GameObject.Find("ExpLine").gameObject;
        TxtLevelTMP = GameObject.Find("txtLevelTMP").GetComponent<TextMeshPro2>();
        if (TmpGold == null)
            TmpGold = GameObject.Find("TmpGold").GetComponent<TextMeshProUGUI>();
        DetailSkill.SetActive(false);
    }
    private void Start()
    {
        SliderLV.maxValue = BCCache.Api.DataConfig.LevelChapters[PlayerPrefs.GetInt("chap")].level_exps[PlayerDetail.api.Level - 1];

        //TxtGold.text = "0";
        TmpGold.text = "0";
    }
    public void BtnPauseGame()
    {
        Debug.Log("fghdfkhsgs:");
        BCPopupManager.api.Show(PopupName.PopupPause);
    }
}
