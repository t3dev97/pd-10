﻿using Studio;
using Studio.BC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BCPopupLevelUpController : MonoBehaviour
{
    [Header("Skill 1")]
    public BCDynamicImage ImgSkill_1;
    public Transform ContentSkill_1;
    public Button BtnSkill_1;
    public Text2 TxtSkill_1;

    [Header("Skill 2")]
    public BCDynamicImage ImgSkill_2;
    public Transform ContentSkill_2;
    public Button BtnSkill_2;
    public Text2 TxtSkill_2;

    [Header("Skill 3")]
    public BCDynamicImage ImgSkill_3;
    public Transform ContentSkill_3;
    public Button BtnSkill_3;
    public Text2 TxtSkill_3;

    [Header("Button")]
    public Button BtnUseGem;
    public Button BtnWatchAds;
    int idSkill_1;
    int idSkill_2;
    int idSkill_3;

    public Text2 TxtNumberGem;
    public Text2 TxtNumberGemShadow;

    RectTransform _rectTransform;
    private void Awake()
    {
        _rectTransform = GetComponent<RectTransform>();
    }

    private void OnEnable()
    {
        _rectTransform.anchoredPosition = Vector3.zero;
    }
    private void Start()
    {
        InitSkills();
    }
    private void InitSkills()
    {
        BtnWatchAds.GetComponent<Button>().interactable = false;
        BtnUseGem.GetComponent<Button>().interactable = false;

        ContentSkill_1.ClearAllChild();
        ContentSkill_2.ClearAllChild();
        ContentSkill_3.ClearAllChild();

        TxtSkill_1.gameObject.SetActive(false);
        TxtSkill_2.gameObject.SetActive(false);
        TxtSkill_3.gameObject.SetActive(false);

        List<int> listSkillOriginInStage = BCCache.Api.DataConfig.StageInfos[BC_DataGame.api.currentIDMap].RandomSkills; // danh sach skill co trong stage

        List<int> tg = new List<int>();
        foreach (var idSkill in listSkillOriginInStage)
        {
            if (BCCache.Api.DataConfig.SkillBases[idSkill].Level <= BCCache.Api.DataConfig.SkillBases[idSkill].limitSkill)
            {
                // hoc duoc
                tg.Add(idSkill);
            }
        }
        listSkillOriginInStage = tg;

        if (listSkillOriginInStage.Count >= 3)
        {
            List<int> listSkill1 = new List<int>();
            List<int> listSkill2 = new List<int>();
            List<int> listSkill3 = new List<int>();

            for (int i = 0; i < listSkillOriginInStage.Count; i++)
            {
                var skillItem = Instantiate(ImgSkill_1);
                //skillItem.transform.localScale = new Vector3(1, 1, 1);
                if (i <= listSkillOriginInStage.Count / 3)
                {
                    listSkill1.Add(listSkillOriginInStage[i]);
                    skillItem.transform.SetParent(ContentSkill_1, false);
                }
                else if (i <= listSkillOriginInStage.Count * 2 / 3)
                {
                    listSkill2.Add(listSkillOriginInStage[i]);
                    skillItem.transform.SetParent(ContentSkill_2, false);
                }
                else
                {
                    listSkill3.Add(listSkillOriginInStage[i]);
                    skillItem.transform.SetParent(ContentSkill_3, false);
                }
                skillItem.name = "skill_item_" + listSkillOriginInStage[i];

                skillItem.gameObject.SetActive(true);
                skillItem.Type = EnumDynamicImageType.Skill;
                skillItem.GetComponent<BCDynamicImage>().SetImage(BCCache.Api.DataConfig.SkillBases[listSkillOriginInStage[i]].iconSkill);
            }

            idSkill_1 = listSkill1[Random.Range(0, listSkill1.Count)];
            //var item1 = ContentSkill_1.Find("skill_item_" + idSkill_1);
            //if (item1 != null)
            //    Destroy(item1);
            listSkill1.Remove(idSkill_1);

            idSkill_2 = listSkill2[Random.Range(0, listSkill2.Count)];
            //var item2 = ContentSkill_2.Find("skill_item_" + idSkill_2);
            //if (item2 != null)
            //    Destroy(item2);
            listSkill2.Remove(idSkill_2);

            idSkill_3 = listSkill3[Random.Range(0, listSkill3.Count)];
            //var item3 = ContentSkill_3.Find("skill_item_" + idSkill_3);
            //if (item3 != null)
            //    Destroy(item3);
            listSkill3.Remove(idSkill_3);
        }
        else
        {
            idSkill_1 = listSkillOriginInStage[Random.Range(0, listSkillOriginInStage.Count)];
            idSkill_2 = listSkillOriginInStage[Random.Range(0, listSkillOriginInStage.Count)];
            idSkill_3 = listSkillOriginInStage[Random.Range(0, listSkillOriginInStage.Count)];
        }

        var skillItem1 = Instantiate(ImgSkill_1, ContentSkill_1, false);
        skillItem1.gameObject.SetActive(true);
        skillItem1.Type = EnumDynamicImageType.Skill;
        skillItem1.GetComponent<BCDynamicImage>().SetImage(BCCache.Api.DataConfig.SkillBases[idSkill_1].iconSkill);

        var skillItem2 = Instantiate(ImgSkill_2, ContentSkill_2, false);
        skillItem2.gameObject.SetActive(true);
        skillItem2.Type = EnumDynamicImageType.Skill;
        skillItem2.GetComponent<BCDynamicImage>().SetImage(BCCache.Api.DataConfig.SkillBases[idSkill_2].iconSkill);

        var skillItem3 = Instantiate(ImgSkill_3, ContentSkill_3, false);
        skillItem3.gameObject.SetActive(true);
        skillItem3.Type = EnumDynamicImageType.Skill;
        skillItem3.GetComponent<BCDynamicImage>().SetImage(BCCache.Api.DataConfig.SkillBases[idSkill_3].iconSkill);

        // Set the selected skill last for spinning effect.
        skillItem1.transform.SetAsFirstSibling();
        skillItem2.transform.SetAsFirstSibling();
        skillItem3.transform.SetAsFirstSibling();

        TxtNumberGem.text = LanguageManager.api.GetKey((BCLocalize.POPUP_LEVEL_UP_GEM), new string[] { BCCache.Api.DataConfig.PopupLevelUpConfig[PlayerDetail.api.Level].Gem.ToString() });
        TxtNumberGemShadow.text = LanguageManager.api.GetKey((BCLocalize.POPUP_LEVEL_UP_GEM), new string[] { BCCache.Api.DataConfig.PopupLevelUpConfig[PlayerDetail.api.Level].Gem.ToString() });

        LeanTween.moveLocalY(ContentSkill_1.gameObject, (ContentSkill_1.childCount) * (ContentSkill_1.GetComponent<VerticalLayoutGroup>().spacing + ImgSkill_1.GetComponent<RectTransform>().rect.height), 0f);
        LeanTween.moveLocalY(ContentSkill_2.gameObject, (ContentSkill_2.childCount) * (ContentSkill_2.GetComponent<VerticalLayoutGroup>().spacing + ImgSkill_2.GetComponent<RectTransform>().rect.height), 0f);
        LeanTween.moveLocalY(ContentSkill_3.gameObject, (ContentSkill_3.childCount) * (ContentSkill_3.GetComponent<VerticalLayoutGroup>().spacing + ImgSkill_3.GetComponent<RectTransform>().rect.height), 0f);

        StartCoroutine(StartSpinning());
    }
    IEnumerator StartSpinning()
    {
        yield return new WaitForEndOfFrame();

        LeanTween.moveLocalY(ContentSkill_1.gameObject, 0, .5f).setEaseInOutSine();
        LeanTween.moveLocalY(ContentSkill_2.gameObject, 0, 1f).setEaseInOutSine();
        LeanTween.moveLocalY(ContentSkill_3.gameObject, 0, 2f).setEaseInOutSine();

        yield return new WaitForSeconds(2f);

        BtnSkill_1.AddClickListener(() =>
        {
            PlayerController.api.AddSkill(idSkill_1);
            BCPopupManager.api.Hide(PopupName.PopupLevelUp);
        });
        BtnSkill_2.AddClickListener(() =>
        {
            PlayerController.api.AddSkill(idSkill_2);
            BCPopupManager.api.Hide(PopupName.PopupLevelUp);
        });
        BtnSkill_3.AddClickListener(() =>
        {
            PlayerController.api.AddSkill(idSkill_3);
            BCPopupManager.api.Hide(PopupName.PopupLevelUp);
        });

        BtnUseGem.GetComponent<Button>().AddClickListener(() =>
        {
            if (BCDataControler.api.MinusGemWithChecker(BCCache.Api.DataConfig.PopupLevelUpConfig[PlayerDetail.api.Level].Gem))
                InitSkills();
        });

        BtnWatchAds.GetComponent<Button>().AddClickListener(() =>
        {
            BCViewHelper.Api.ShowMessageBox(LanguageManager.api.GetKey(BCLocalize.POPUP_NOT_CONNET_NETWORK));
        });

        TxtSkill_1.gameObject.SetActive(true);
        TxtSkill_2.gameObject.SetActive(true);
        TxtSkill_3.gameObject.SetActive(true);

        TxtSkill_1.text = LanguageManager.api.GetKey(BCCache.Api.DataConfig.SkillBases[idSkill_1].nameSkill);
        TxtSkill_2.text = LanguageManager.api.GetKey(BCCache.Api.DataConfig.SkillBases[idSkill_2].nameSkill);
        TxtSkill_3.text = LanguageManager.api.GetKey(BCCache.Api.DataConfig.SkillBases[idSkill_3].nameSkill);

        if (BCCache.Api.DataConfig.PopupLevelUpConfig[PlayerDetail.api.Level].Gem > 0)
            BtnUseGem.GetComponent<Button>().interactable = true;

        if (Random.Range(0.0f, 1.0f) < BCCache.Api.DataConfig.PopupLevelUpConfig[PlayerDetail.api.Level].Ads)
            BtnWatchAds.GetComponent<Button>().interactable = true;
    }
}
