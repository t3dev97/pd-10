﻿using Studio.BC;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BCPopupPause : MonoBehaviour
{
    public GameObject itemPre;
    public Transform Content;

    [Header("Button")]
    public Button btnSound;
    public Button btnBackHome;
    public Button btnResume;

    private void Start()
    {
        gameObject.GetComponent<RectTransform>().anchorMin = new Vector2(0, 0);
        gameObject.GetComponent<RectTransform>().anchorMax = new Vector2(1, 1);
        gameObject.GetComponent<RectTransform>().offsetMax = new Vector2(0, 0);
        gameObject.GetComponent<RectTransform>().offsetMin = new Vector2(0, 0);

        btnSound.GetComponent<Button>().AddClickListener(() =>
        {
            Debug.Log("On/Off Sound");
            Time.timeScale = 1;
            BCPopupManager.api.Hide(PopupName.PopupPause);
        });

        btnBackHome.GetComponent<Button>().AddClickListener(() =>
        {
            BCPopupManager.api.Hide(PopupName.PopupPause);
            Time.timeScale = 1;
            LoadScene.api.Load(NameScene.LayoutDemo);
        });

        btnResume.GetComponent<Button>().AddClickListener(() =>
        {
            Time.timeScale = 1;
            BCPopupManager.api.Hide(PopupName.PopupPause);
        });

        GameObject temp;
        Dictionary<int, BCSkillBaseData> listSkill = BCCache.Api.DataConfig.SkillBases;
        foreach (var item in listSkill) // hiện danh sách skill
        {
            if (item.Value.Level > 1)
            {
                temp = Instantiate(itemPre, transform.position, Quaternion.identity, Content);
                temp.SetActive(true);
                temp.GetComponent<ItemView>().img.Type = EnumDynamicImageType.Skill;
                temp.GetComponent<ItemView>().img.SetImage(BCCache.Api.DataConfig.SkillBases[item.Value.id].iconSkill);
            }
        }
        Time.timeScale = 0;
    }
    private void OnDisable()
    {
        Time.timeScale = 1;
    }


}
