﻿using Studio;
using Studio.BC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BCPopupAcQuyController : MonoBehaviour
{
    public BCDynamicImage ImgSkill;
    public Text2 TxtSkillName;
    public Text2 Content;
    public int HP;
    public Transform Container;

    [Header("Button")]
    public Button BtnLeft;
    public Button BtnRight;
    public Button BtnUseGem;
    public Button BtnWatchAds;

    int idSkill;
    RectTransform _rectTransform;
    private void Awake()
    {
        _rectTransform = GetComponent<RectTransform>();
    }
    private void OnEnable()
    {
        _rectTransform.anchoredPosition = Vector3.zero;
    }
    private void Start()
    {
        Init();
    }
    private void Init()
    {
        Container.ClearAllChild();

        List<int> listSkill = BCCache.Api.DataConfig.StageInfos[BC_Chapter_Info.api.IDMap].DevilSkills;
        List<int> tg = new List<int>();

        foreach (var item in listSkill)
        {
            if (BCCache.Api.DataConfig.SkillBases[item].Level <= BCCache.Api.DataConfig.SkillBases[item].limitSkill)
                tg.Add(item);

            var skill = Instantiate(ImgSkill, Container);
            skill.gameObject.SetActive(true);
            skill.name = "skill_" + item;
            skill.Type = EnumDynamicImageType.Skill;
            skill.SetImage(BCCache.Api.DataConfig.SkillBases[item].iconSkill);
        }
        listSkill = tg;

        idSkill = listSkill[Random.Range(0, listSkill.Count)];
        var temp = Instantiate(ImgSkill, Container);
        temp.gameObject.SetActive(true);
        temp.name = "skill_" + idSkill;
        temp.Type = EnumDynamicImageType.Skill;
        temp.SetImage(BCCache.Api.DataConfig.SkillBases[idSkill].iconSkill);
        temp.transform.SetAsFirstSibling();

        TxtSkillName.text = LanguageManager.api.GetKey(BCCache.Api.DataConfig.SkillBases[idSkill].nameSkill);

        BtnLeft.GetComponent<Button>().onClick.AddListener(() =>
        {
            BCPopupManager.api.Hide(PopupName.PopupAcQuy, (go) =>
            {
                if (gameObject.GetComponent<BCPopupDetail>() != null && gameObject.GetComponent<BCPopupDetail>().ObjCallBack != null)
                    gameObject.GetComponent<BCPopupDetail>().Hide();
                else
                    Debug.LogError("Obj null");
            });
        });

        float conditionHp = BCCache.Api.DataConfig.PopupDevilConfig[BC_Chapter_Info.api.IDMap].Hp; // phầm trăm
        conditionHp /= 100.0f;
        float currentHpTemp = PlayerDetail.api.HP * conditionHp;
        float maxHpTemp = PlayerDetail.api.MaxHPInStage * conditionHp;

        Content.text = LanguageManager.api.GetKey((BCLocalize.POPUP_ACQUY_CONTENT), new string[] { maxHpTemp.ToString() });

        BtnRight.GetComponent<Button>().onClick.AddListener(() =>
        {
            PlayerDetail.api.MaxHPInStage -= maxHpTemp;
            if (PlayerDetail.api.HP - currentHpTemp < 0) PlayerDetail.api.HP = 1;
            else
                PlayerDetail.api.HP -= currentHpTemp;

            PlayerController.api.AddSkill(idSkill);

            BCPopupManager.api.Hide(PopupName.PopupAcQuy, (go) =>
            {
                if (gameObject.GetComponent<BCPopupDetail>() != null && gameObject.GetComponent<BCPopupDetail>().ObjCallBack != null)
                    gameObject.GetComponent<BCPopupDetail>().Hide();
                else
                    Debug.LogError("Obj null");
            });
        });

        LeanTween.moveLocalY(Container.gameObject, (Container.childCount) * (Container.GetComponent<VerticalLayoutGroup>().spacing + ImgSkill.GetComponent<RectTransform>().rect.height), 0);

        StartCoroutine(StartSpinning());
    }

    IEnumerator StartSpinning()
    {
        BtnLeft.interactable = false;
        BtnRight.interactable = false;
        TxtSkillName.gameObject.SetActive(false);

        yield return new WaitForEndOfFrame();

        LeanTween.moveLocalY(Container.gameObject, 0, 1).setEaseInOutSine();

        yield return new WaitForSeconds(1.0f);

        BtnLeft.interactable = true;
        BtnRight.interactable = true;
        TxtSkillName.gameObject.SetActive(true);
    }

    IEnumerator Hide(float time)
    {
        yield return new WaitForSeconds(time);
        if (gameObject.GetComponent<BCPopupDetail>() != null && gameObject.GetComponent<BCPopupDetail>().ObjCallBack != null)
            gameObject.GetComponent<BCPopupDetail>().Hide();
        else
            Debug.LogError("Obj null");
    }

}
