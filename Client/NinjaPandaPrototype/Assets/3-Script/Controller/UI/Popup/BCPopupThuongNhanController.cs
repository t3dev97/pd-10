﻿using Studio;
using Studio.BC;
using UnityEngine;
using UnityEngine.UI;

public class BCPopupThuongNhanController : MonoBehaviour
{

    [Header("Left")]
    public BCDynamicImage imgLeft;
    public Text2 txtNameLeft;
    public BCDynamicImage imgCostLeft;
    public Text2 txtCostLeft;

    [Header("Right")]
    public BCDynamicImage imgRight;
    public Text2 txtNameRight;
    public BCDynamicImage imgCostRight;
    public Text2 txtCostRight;

    [Header("Button")]
    public Button BtnClose;
    public Button BtnLeft;
    public Button BtnRight;

    int idItemLeft;
    int idItemRight;
    RectTransform _rectTransform;

    private void Awake()
    {
        _rectTransform = GetComponent<RectTransform>();
    }
    private void OnEnable()
    {
        _rectTransform.anchoredPosition = Vector3.zero;
        Init();
    }
    private void Init()
    {
        idItemLeft = Random.Range(1, BCCache.Api.DataConfig.MaterialConfigData.Count - 2);
        idItemRight = Random.Range(1, BCCache.Api.DataConfig.MaterialConfigData.Count - 2);

        imgLeft.Type = EnumDynamicImageType.Inventory;
        imgLeft.SetImage(BCCache.Api.DataConfig.MaterialConfigData[idItemLeft].assetName);
        txtNameLeft.text = LanguageManager.api.GetKey((BCCache.Api.DataConfig.MaterialConfigData[idItemLeft].nameId));

        imgRight.Type = EnumDynamicImageType.Inventory;
        imgRight.SetImage(BCCache.Api.DataConfig.MaterialConfigData[idItemRight].assetName);
        txtNameRight.text = LanguageManager.api.GetKey((BCCache.Api.DataConfig.MaterialConfigData[idItemRight].nameId));

        imgCostLeft.Type = EnumDynamicImageType.Currency;
        imgCostLeft.SetImage(BCCache.Api.DataConfig.ShopConfigData.listGemData[1].AssetName);

        imgCostRight.Type = EnumDynamicImageType.Currency;
        imgCostRight.SetImage(BCCache.Api.DataConfig.ShopConfigData.listGemData[1].AssetName);

        txtCostLeft.text = BCCache.Api.DataConfig.MaterialConfigData[idItemLeft].price_gold.ToString();
        txtCostRight.text = BCCache.Api.DataConfig.MaterialConfigData[idItemRight].price_gold.ToString();

        BtnClose.onClick.AddListener(() =>
        {
            BCPopupManager.api.Hide(PopupName.PopupThuongNhan, (go) =>
            {
                if (gameObject.GetComponent<BCPopupDetail>() != null && gameObject.GetComponent<BCPopupDetail>().ObjCallBack != null)
                    gameObject.GetComponent<BCPopupDetail>().Hide();
                else
                    Debug.LogError("Obj null");
            });
        });

        BtnLeft.GetComponent<Button>().AddClickListener(() =>
        {
            if (BCDataControler.api.MinusGemWithChecker(BCCache.Api.DataConfig.MaterialConfigData[idItemLeft].price_gold))
            {
                if (!BCCache.Api.DataConfig.ResourceGame.dataTemp.ListMaterialInStage.ContainsKey(idItemLeft))
                    BCCache.Api.DataConfig.ResourceGame.dataTemp.ListMaterialInStage.Add(idItemLeft, 1);
                else
                    BCCache.Api.DataConfig.ResourceGame.dataTemp.ListEquipmentsInStage[idItemLeft]++;

                BCPopupManager.api.Hide(PopupName.PopupThuongNhan, (go) =>
                {
                    if (gameObject.GetComponent<BCPopupDetail>() != null && gameObject.GetComponent<BCPopupDetail>().ObjCallBack != null)
                        gameObject.GetComponent<BCPopupDetail>().Hide();
                    else
                        Debug.LogError("Obj null");
                });
            }
        });
        BtnRight.GetComponent<Button>().AddClickListener(() =>
        {
            if (BCDataControler.api.MinusGemWithChecker(BCCache.Api.DataConfig.MaterialConfigData[idItemLeft].price_gold))
            {
                if (!BCCache.Api.DataConfig.ResourceGame.dataTemp.ListMaterialInStage.ContainsKey(idItemRight))
                    BCCache.Api.DataConfig.ResourceGame.dataTemp.ListMaterialInStage.Add(idItemRight, 1);
                else
                    BCCache.Api.DataConfig.ResourceGame.dataTemp.ListEquipmentsInStage[idItemRight]++;

                BCPopupManager.api.Hide(PopupName.PopupThuongNhan, (go) =>
                {
                    if (gameObject.GetComponent<BCPopupDetail>() != null && gameObject.GetComponent<BCPopupDetail>().ObjCallBack != null)
                        gameObject.GetComponent<BCPopupDetail>().Hide();
                    else
                        Debug.LogError("Obj null");
                });
            }
        });
    }
}
