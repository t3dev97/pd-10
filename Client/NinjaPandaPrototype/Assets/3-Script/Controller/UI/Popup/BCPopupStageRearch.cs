﻿using Studio;
using Studio.BC;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BCPopupStageRearch : MonoBehaviour
{
    public GameObject txtTile;
    public Text2 txtStage;
    public Text2 txtChapter;
    public Text2 txtLevel;
    public Text2 txtContent;

    public Slider sliderLevel;
    public Button btnClose;

    [Header("Show Reward")]
    public GameObject itemPre;
    public Transform Content;
    int id_common = 1;
    RectTransform _rectTransform;
    GameObject _player;

    private void Awake()
    {
        _rectTransform = GetComponent<RectTransform>();

    }
    private void Start()
    {
        _player = GameObject.FindGameObjectWithTag("Player");
    }
    private void OnEnable()
    {
        _rectTransform.anchoredPosition = Vector3.zero;
        btnClose.GetComponent<Button>().AddClickListener(() =>
        {
            BCPopupManager.api.Hide(PopupName.PopupStageRearch);
            LoadScene.api.Load(NameScene.LayoutDemo);
        });

        // ListEquipmentsInStage
        GameObject itemDrop;
        Dictionary<int, int> listItemDrop = BCCache.Api.DataConfig.ResourceGame.dataTemp.ListEquipmentsInStage;
        foreach (var item in listItemDrop)
        {
            for (int j = 0; j < item.Value; j++)
            {
                BCDataControler.api.AddInventory(item.Key);
            }
            itemDrop = Instantiate(itemPre, transform.position, Quaternion.identity, Content);
            itemDrop.SetActive(true);

            itemDrop.GetComponent<ItemView>().img.SetImage(BCCache.Api.DataConfig.EquipmentConfigData[item.Key].assetName);
            itemDrop.GetComponent<ItemView>().TxtCount.text = item.Value.ToString();
        }
        listItemDrop.Clear();
        BCCache.Api.DataConfig.ResourceGame.dataTemp.ListEquipmentsInStage.Clear();

        // ListMaterialInStage
        Dictionary<int, int> listMaterial = BCCache.Api.DataConfig.ResourceGame.dataTemp.ListMaterialInStage;
        foreach (var item in listMaterial)
        {
            for (int i = 0; i < item.Value; i++)
            {
                BCDataControler.api.data.listMaterials[item.Key].amount++;
            }
            itemDrop = Instantiate(itemPre, transform.position, Quaternion.identity, Content);
            itemDrop.SetActive(true);

            itemDrop.GetComponent<ItemView>().img.SetImage(BCCache.Api.DataConfig.MaterialConfigData[item.Key].assetName);
            itemDrop.GetComponent<ItemView>().TxtCount.text = item.Value.ToString();
        }
        listMaterial.Clear();
        BCCache.Api.DataConfig.ResourceGame.dataTemp.ListMaterialInStage.Clear();

        // EXP
        if (PlayerPrefs.GetInt("round") > PlayerPrefs.GetInt("roundHight"))
        {
            PlayerPrefs.SetInt("roundHight", PlayerPrefs.GetInt("round"));
            txtTile.gameObject.SetActive(true);
        }
        txtContent.text = PlayerPrefs.GetInt("round").ToString();
        txtChapter.SetLocalize(BCLocalize.POPUP_STAGE_REARCH_BOTTOM_CONTENT, new string[] { PlayerPrefs.GetInt("chap").ToString() });

        BCDataControler.api.UpdateLevel((level, exp) =>
        {
            txtLevel.text = level.ToString();
            sliderLevel.maxValue = BCCache.Api.DataConfig.UserLevelConfigData[BCDataControler.api.data.Level + 1].expRequire;
            sliderLevel.value = exp;
        });

        // Gold 
        BCDataControler.api.data.Gold += PlayerDetail.api.GoldInstage;

        // Gem
        BCDataControler.api.data.Gem += PlayerDetail.api.GemInStage;
    }
}
