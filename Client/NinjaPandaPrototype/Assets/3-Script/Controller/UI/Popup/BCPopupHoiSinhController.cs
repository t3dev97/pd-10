﻿using Studio;
using Studio.BC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BCPopupHoiSinhController : MonoBehaviour
{
    float currentTime;
    float maxTime = 5f;


    [Header("Time line")]
    public Image imgTimeLine;
    public Text2 txtTime;

    [Header("Button")]
    public Button btnLeft;
    public Button btnRight;
    public Button btnBack;
    RectTransform _rectTransform;

    private void Awake()
    {
        _rectTransform = GetComponent<RectTransform>();
    }
    //private void OnDisable()
    //{
    //    isShowPopup = false;
    //}
    private void OnEnable()
    {
        _rectTransform.anchoredPosition = Vector3.zero;
        currentTime = maxTime;
        txtTime.text = currentTime.ToString();

        //Debug.Log("BCPopupManager.api.isShowPopupHoiSinh" + BCPopupManager.api.isShowPopupHoiSinh);
        if (BCPopupManager.api.isShowPopupHoiSinh)
        {
            ShowPopupStageRearch();
        }
        else
        {
            StartCoroutine(timeText());
            StartCoroutine(timeLine());
        }


        btnLeft.interactable = true;
        btnLeft.onClick.AddListener(() =>
        {
            BCPopupManager.api.Hide(PopupName.PopupHoiSinh);
            PlayerController.api.Revival();
            BCPopupManager.api.isShowPopupHoiSinh = true;
        });

        btnRight.interactable = false;
        btnRight.onClick.AddListener(() =>
        {
            BCPopupManager.api.Hide(PopupName.PopupHoiSinh);
            LoadScene.api.Load(NameScene.LayoutDemo);
        });

        btnBack.onClick.AddListener(() =>
        {
            BCPopupManager.api.Hide(PopupName.PopupHoiSinh);
            Dictionary<int, int> listItemDrop = BCCache.Api.DataConfig.ResourceGame.dataTemp.ListEquipmentsInStage;
            foreach (var item in listItemDrop)
            {
                for (int j = 0; j < item.Value; j++)
                {
                    BCDataControler.api.AddInventory(item.Key);
                }
            }

            LoadScene.api.Load(NameScene.LayoutDemo);
        });
    }

    IEnumerator timeText()
    {
        yield return new WaitForSeconds(1.0f);
        txtTime.text = (--currentTime).ToString();

        if (currentTime > 0)
            StartCoroutine(timeText());
        else
        {
            ShowPopupStageRearch();
        }
    }

    public void ShowPopupStageRearch()
    {
        //Debug.Log("ShowPopupStageRearch");
        BCPopupManager.api.Hide(PopupName.PopupHoiSinh);
        BCPopupManager.api.isShowPopupHoiSinh = false;
        BCPopupManager.api.Show(PopupName.PopupStageRearch);
    }
    IEnumerator timeLine()
    {
        yield return new WaitForEndOfFrame();
        imgTimeLine.fillAmount += Time.deltaTime / maxTime;
        if (imgTimeLine.fillAmount < 1)
            StartCoroutine(timeLine());
    }

}
