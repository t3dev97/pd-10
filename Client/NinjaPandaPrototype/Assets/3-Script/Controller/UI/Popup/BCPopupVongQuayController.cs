﻿using Studio.BC;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BCPopupVongQuayController : MonoBehaviour
{
    public Button BtnSpin;
    public Button BtnWatchAds;
    public Button BtnClose;
    public EnumVongQuayType VongQuayType;
    public SpinWheelController wheelController;
    public List<ItemWheel> LlistItem;
    public GameObject ObjCalled;

    RectTransform _rectTransform;
    private void Awake()
    {
        _rectTransform = GetComponent<RectTransform>();
    }
    public void Init(EnumVongQuayType vongQuayType)
    {
        VongQuayType = vongQuayType;
        switch (vongQuayType)
        {
            case EnumVongQuayType.Start:
                {
                    BtnSpin.gameObject.SetActive(true);
                    BtnWatchAds.gameObject.SetActive(false);
                    BtnClose.gameObject.SetActive(false);

                    LlistItem = BCCache.Api.DataConfig.WheelContainers[BCCache.Api.DataConfig.StageInfos[BC_Chapter_Info.api.IDMap].StartWheelId];
                    break;
                }
            case EnumVongQuayType.Boss:
                {
                    BtnSpin.gameObject.SetActive(true);
                    BtnWatchAds.gameObject.SetActive(false);
                    BtnClose.gameObject.SetActive(false);

                    LlistItem = BCCache.Api.DataConfig.WheelContainers[BCCache.Api.DataConfig.StageInfos[BC_Chapter_Info.api.IDMap].BossWheelID];
                    break;
                }
            case EnumVongQuayType.Ads:
                {
                    BtnSpin.gameObject.SetActive(false);
                    BtnWatchAds.gameObject.SetActive(true);

                    LlistItem = BCCache.Api.DataConfig.WheelContainers[BCCache.Api.DataConfig.StageInfos[BC_Chapter_Info.api.IDMap].AdsWheelID];
                    break;
                }
        }
        wheelController.Init(this);

        BtnSpin.onClick.AddListener(() =>
        {
            BtnSpin.interactable = false;
            wheelController.SpinWheel(this);
        });

        BtnWatchAds.AddClickListener(() =>
        {
            wheelController.SpinWheel(this);
        });

        BtnClose.AddClickListener(() =>
        {
            BCPopupManager.api.HideAll();
        });
    }
    public void Hide()
    {
        gameObject.SetActive(false);
    }

}
