﻿using UnityEngine;

public class BCPopupDetail : MonoBehaviour
{
    public GameObject ObjCallBack; // chứa đối tượng đã tạo ra popup, dùng chung cho tất cả các popup
    public Animator Anim;
    public void Hide(float time = 0.5f)
    {
        Anim.SetBool("Die", true);
        Destroy(ObjCallBack, time);
    }
}
