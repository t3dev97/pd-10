﻿using System.Collections;
using UnityEngine;

public class DetailSkillController : MonoBehaviour
{
    public float time;
    Animator anim;
    private void Awake()
    {
        anim = GetComponent<Animator>();
    }
    private void OnEnable()
    {
        StartCoroutine(Delay(time));
    }
    IEnumerator Delay(float time)
    {
        yield return new WaitForSeconds(time);
        anim.SetTrigger("Hide");
        yield return new WaitForSeconds(time / 10);
        gameObject.SetActive(false);
    }
}
