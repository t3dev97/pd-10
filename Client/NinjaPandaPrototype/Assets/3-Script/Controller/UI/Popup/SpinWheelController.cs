﻿using Studio;
using Studio.BC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpinWheelController : MonoBehaviour
{
    public GameObject ItemDemo; // item thiết lập mẫu
    public List<ItemWheelData> listItemSpinData = new List<ItemWheelData>(); // danh sách các item sẻ quay
    public List<GameObject> listItemSpinOBJ = new List<GameObject>(); // danh sách các item sẻ quay
    public List<AnimationCurve> animationCurves;

    [SerializeField]
    float angle; // góc giữa mõi item

    [SerializeField]
    int numberPer = 6;

    private bool spinning;
    private int randomTime;
    private int indexItem;
    private BCPopupVongQuayController _controller;
    private Transform _player;
    private void Start()
    {
        _player = GameObject.FindGameObjectWithTag(TagManager.api.Player).transform;
    }
    public void Init(BCPopupVongQuayController controller)
    {
        _controller = controller;
        GameObject itemOnWheel;
        spinning = true;
        angle = 360 / numberPer;
        float angleBegin = angle;

        for (int i = 0; i < 6; i++)
        {
            ItemWheel item = controller.LlistItem[i];
            ItemWheelData data = new ItemWheelData(i, item);

            itemOnWheel = Instantiate(ItemDemo, transform.position, Quaternion.identity, transform);

            if (data.Info.ItemType == EnumItemWheelType.Skill)
            {
                int idSkill = data.Info.Values[Random.Range(0, data.Info.Values.Count)];
                data.Value = idSkill;

                itemOnWheel.transform.GetChild(0).gameObject.SetActive(true);
                itemOnWheel.transform.GetChild(0).GetComponent<BCDynamicImage>().Type = EnumDynamicImageType.Skill;
                itemOnWheel.transform.GetChild(0).GetComponent<BCDynamicImage>().SetImage(BCCache.Api.DataConfig.SkillBases[idSkill].iconSkill);
            }
            else
            {
                int numberGoldOrGem = data.Info.Values[Random.Range(0, data.Info.Values.Count)];
                data.Value = numberGoldOrGem;

                itemOnWheel.transform.GetChild(2).gameObject.SetActive(true);
                itemOnWheel.transform.GetChild(2).GetComponent<Text2>().text = numberGoldOrGem.ToString();
                ConverColor tool = new ConverColor();
                itemOnWheel.transform.GetChild(2).GetComponent<Text2>().color = tool.GetColorFromString(data.Info.ColorCode);

                itemOnWheel.transform.GetChild(1).gameObject.SetActive(true);
                itemOnWheel.transform.GetChild(1).GetComponent<BCDynamicImage>().Type = EnumDynamicImageType.Currency;
                if (data.Info.AssetName == "gold_1")
                    itemOnWheel.transform.GetChild(1).GetComponent<BCDynamicImage>().SetImage(data.Info.AssetName, false, null, true);
                else
                    itemOnWheel.transform.GetChild(1).GetComponent<BCDynamicImage>().SetImage(data.Info.AssetName);
            }
            itemOnWheel.gameObject.name = "Item_" + i;
            itemOnWheel.gameObject.SetActive(true);
            itemOnWheel.transform.eulerAngles = new Vector3(0, 0, angleBegin -= angle);

            listItemSpinData.Add(data);
            listItemSpinOBJ.Add(itemOnWheel);
        }
    }
    public void SpinWheel(BCPopupVongQuayController vongquay)
    {
        _controller = vongquay;
        indexItem = Random.Range(0, listItemSpinData.Count);
        randomTime = 5; // thời gian quay

        float maxAngle = 720 * randomTime + (indexItem * angle);// góc sau khi quay
        if (spinning)
            StartCoroutine(SpinTheWheel(randomTime, maxAngle));
    }
    IEnumerator SpinTheWheel(float time, float maxAngle)
    {
        spinning = false;
        _controller.BtnSpin.gameObject.SetActive(false);
        _controller.BtnWatchAds.interactable = false;

        float timer = 0.0f;
        float startAngle = transform.eulerAngles.z;
        maxAngle -= startAngle; // độ lệch so với góc ban đầu

        if (_controller.BtnClose != null && _controller.BtnClose.gameObject.activeSelf)
            _controller.BtnClose.gameObject.SetActive(false);

        int animationCurveNumber = Random.Range(0, animationCurves.Count); // chọn hình thức quay
        while (timer < time)
        {
            float angle = maxAngle * animationCurves[animationCurveNumber].Evaluate(timer / time); // tính góc quay
            transform.localEulerAngles = new Vector3(0.0f, 0.0f, angle + startAngle);
            timer += Time.deltaTime;
            yield return 0;
        }

        transform.localEulerAngles = new Vector3(0.0f, 0.0f, maxAngle + startAngle);
        StartCoroutine(running(() =>
        {
            var item = listItemSpinData[indexItem];

            Transform posCreateItem;
            if (_controller.GetComponent<BCPopupDetail>().ObjCallBack == null)
                posCreateItem = _player;
            else
                posCreateItem = _controller.GetComponent<BCPopupDetail>().ObjCallBack.transform;

            switch (item.Info.ItemType)
            {
                case EnumItemWheelType.Gold:
                    {
                        _controller.gameObject.GetComponent<InGameGoldSpawner>().SpawnGoldReward(Random.Range(15, 50), _player, _player);
                        break;
                    }
                case EnumItemWheelType.Gem:
                    {
                        PlayerDetail.api.GemInStage += item.Value;
                        _controller.gameObject.GetComponent<InGameGoldSpawner>().SpawnGoldReward(Random.Range(15, 50), _player, _player);
                        break;
                    }
                case EnumItemWheelType.Skill:
                    {
                        PlayerController.api.AddSkill(item.Value);
                        break;
                    }
            }
            StartCoroutine(ShowIconTick(listItemSpinOBJ[indexItem].transform));
        }));
        StartCoroutine(Delay());
    }
    IEnumerator ShowIconTick(Transform obj)
    {
        foreach (Transform item in obj)
        {
            if (item.gameObject.activeSelf)
            {
                item.GetComponent<Animator>().SetTrigger("Done");
                break;
            }
        }
        yield return new WaitForSeconds(0.5f);
        foreach (Transform item in obj)
        {
            item.gameObject.SetActive(false);
        }
        obj.GetChild(obj.childCount - 1).gameObject.SetActive(true);
        yield return new WaitForSeconds(0.5f);
    }
    IEnumerator running(System.Action a)
    {
        yield return new WaitForSeconds(0.5f);
        a?.Invoke();
    }
    IEnumerator Delay()
    {
        yield return new WaitForSeconds(BCCache.Api.DataConfig.ConfigGameData.DelayTransitionPopup);

        BC_Chapter_Info.api.UpdateGold(listItemSpinData[indexItem].Value);

        if (_controller.gameObject.GetComponent<BCPopupDetail>() != null && _controller.gameObject.GetComponent<BCPopupDetail>().ObjCallBack != null)
            _controller.gameObject.GetComponent<BCPopupDetail>().Hide();
        //else
        //    Debug.LogError("Obj null");

        if (_controller.VongQuayType == EnumVongQuayType.Boss)
        {
            BCPopupManager.api.ShowReturn(PopupName.PopupVongQuay, (go) => { go.GetComponent<BCPopupVongQuayController>().Init(EnumVongQuayType.Ads); });
            _controller.Hide();
        }
        else
            BCPopupManager.api.HideAll();
    }
}
public class ItemWheelData
{
    public int ID; // su dung trong vong quay nay
    public ItemWheel Info;
    public int Value; // nếu là skill thì là skill thì sẻ là id, ngược lại là số lượng của gold/gem trên wheel
    public ItemWheelData(int id, ItemWheel item)
    {
        ID = id;
        Info = item;
    }
}
public class ConverColor
{
    public static ConverColor api;
    int HexToDec(string hex)
    {
        return System.Convert.ToInt32(hex, 16);
    }
    string DecToHex(int value)
    {
        return value.ToString("X2");
    }
    string FloatNomalizeToHex(float value)
    {
        return DecToHex(Mathf.RoundToInt(value * 255f));
    }
    float HexToFlotNomalized(string hex)
    {
        return HexToDec(hex) / 255f;
    }
    public Color GetColorFromString(string hexString)
    {
        float red = HexToFlotNomalized(hexString.Substring(1, 2));
        float green = HexToFlotNomalized(hexString.Substring(3, 2));
        float blue = HexToFlotNomalized(hexString.Substring(5, 2));
        return new Color(red, green, blue);
    }
    public string GetStringFromColor(Color color)
    {
        string red = FloatNomalizeToHex(color.r);
        string green = FloatNomalizeToHex(color.g);
        string blue = FloatNomalizeToHex(color.b);
        return "#" + red + green + blue;
    }
}