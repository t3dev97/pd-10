﻿using Studio;
using Studio.BC;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class BC_UIController : MonoBehaviour
{
    public static BC_UIController api;
    public GameObject uiGroup;
    public int PaddingChose=50;
    public GridLayoutGroup grdLayout;
    public BCScrollCustom bcScrollCustom;

    public int intChose = 3;
    public List<Toggle> ListToggle;
    public Button btnPlay;
    public List<GameObject> ListToggleChose;

    public BCUITopView uiTop;
    public BCEquipmentController equipment;
    public BCShopController shop;
    public BCTalentController talent;
    public BCWorldController world;
    public BCChonseMapController choseMap;

    public GameObject ChoseMapView;
    public GameObject AchivenemntView;
    public List<int> ListUnlock;
    public void Awake()
    {
        if (!BCConfig.Api.IsFirstInit)
        {
            SceneManager.LoadScene(NameScene.BCLoading, LoadSceneMode.Single);
            return;
        }
        if (api == null) api = this;
        else if (api != this) Destroy(this);
        GetComponent();
        InitUI();
        updateUI();
        int width = Camera.main.pixelWidth;
        int height = Camera.main.pixelHeight;
        float fill = (width*1.0f) / height;
        grdLayout.cellSize = new Vector2(ScreenSize.MaxWidth, (ScreenSize.MaxWidth*1.0f) / fill);
    }

    public void OnChageScene()
    {
        SceneManager.LoadScene(NameScene.Main, LoadSceneMode.Single);
    }
    public void GetComponent()
    {
        foreach (Transform tg in uiGroup.transform)
        {
            ListToggle.Add(tg.GetComponent<Toggle>());
            ListToggleChose.Add(tg.GetComponent<Toggle>().graphic.gameObject);
        }
    }
  
    public void InitUI()
    {
        TurnOffAllToggle();
        setChoseToggen(ListToggle[intChose-1]);
        int index = 1;
        foreach (Toggle tg in ListToggle)
        {
            if (!ListUnlock.Contains(index))
            {
                tg.onValueChanged.AddListener(delegate
                {
                    ToggleValueChanged(tg);
                });
                tg.transform.Find("Fader").gameObject.SetActive(false);
            }
            else
                tg.transform.Find("Fader").gameObject.SetActive(true);
            index++;
        }
    }
    public void updateUI()
    {
        gameObject.SetActive(true);

        uiTop.updataUITop();
        equipment.UpdateEquipment();
        shop.updateShop();
        talent.UpdateViewTalent();
        world.UpdateViewWorld();
        //choseMap.UpdateViewChoseMap();
    }
    public void Update()
    {
        if (!bcScrollCustom.isDrag)
        {
            if (ListUnlock.Contains(bcScrollCustom.currenChap))
            {

                if (intChose - bcScrollCustom.currenChap > 0)
                {
                    intChose = bcScrollCustom.currenChap;
                    if (intChose == 0)
                        intChose++;
                    else
                        intChose--;
                }
                else
                {
                    intChose = bcScrollCustom.currenChap;
                    if (intChose == ListToggle.Count)
                    {
                        intChose--;
                    }

                    else
                        intChose++;
                }
                ToggleValueChanged(ListToggle[intChose - 1]);
            }
            else
            {
                intChose = bcScrollCustom.currenChap;
            }

            setChoseToggen(ListToggle[intChose - 1]);
        }
    }

    public void setChoseToggen(Toggle toggle)
    {
        toggle.graphic.gameObject.SetActive(true);
        toggle.isOn = true;
        toggle.gameObject.GetComponent<LayoutElement>().preferredWidth = PaddingChose;

    }
    public void ToggleValueChanged(Toggle isChose)
    {
        int currenchap = bcScrollCustom.currenChap;
        DebugX.Log("isChoose: "+ isChose.name);
        if (isChose.isOn)
        {
            TurnOffAllToggle();
            switch (isChose.name)
            {
                case UISceneName.Shop: DebugX.Log("Chose : " + isChose.name); bcScrollCustom.currenChap = 1; break;
                case UISceneName.Equipment: DebugX.Log("Chose : " + isChose.name); bcScrollCustom.currenChap = 2; break;
                case UISceneName.World: DebugX.Log("Chose : " + isChose.name); bcScrollCustom.currenChap = 3; break;
                case UISceneName.Talents: DebugX.Log("Chose : " + isChose.name); bcScrollCustom.currenChap = 4; break;
                case UISceneName.Setting: DebugX.Log("Chose : " + isChose.name); bcScrollCustom.currenChap = 5; break;
            }
            if (currenchap != bcScrollCustom.currenChap)
                  bcScrollCustom.speed = bcScrollCustom.currentspeed * (Mathf.Abs(currenchap- bcScrollCustom.currenChap));
            setChoseToggen(isChose);
        }
    }

    public void ToggleValueChanged(Lobby_Position pos)
    {
        ToggleValueChanged(ListToggle[(int)pos]);
    }

    public void UpdateSizeToggle(Toggle toggle)
    {
        ResizeAllToggle();
    }
    public void ResizeAllToggle()
    {
        foreach (Toggle tg in ListToggle)
        {
            Vector2 width = (tg.gameObject.GetComponent<RectTransform>()).sizeDelta;
          
            float width1 = width.x - PaddingChose / (ListToggle.Count - 1);
            tg.gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(width1, width.y);
        }
    }
    public void TurnOffAllToggle()
    {
        foreach (GameObject tg in ListToggleChose)
        {
            tg.SetActive(false);
        }

        foreach (Toggle tg in ListToggle)
        {
            tg.gameObject.GetComponent<LayoutElement>().preferredWidth = 0;
        }
    }

    public void ShowChoseMap()
    {
        ChoseMapView.SetActive(true);
    }

    public void ShowAchivenmentMap()
    {
        AchivenemntView.SetActive(true);
    }
    public void ChoseMap(int vt)
    {
        world.chapCurrent = vt;
        world.UpdateViewWorld();
    }
    public void HideChoseMap(string name)
    {
        GameObject.Find(name).SetActive(false);
    }
}
