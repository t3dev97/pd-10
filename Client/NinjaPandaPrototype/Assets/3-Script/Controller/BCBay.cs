﻿using System.Collections;
using UnityEngine;
public class BCBay : MonoBehaviour
{
    public float Damage = 10;

    float speedAtk = 1; // thời gian giản cách giữa các lần atk
    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "Player")
        {
            Debug.Log("Atk by Bay");
            Attack();
        }
    }
    bool isAttack = false;
    void Attack()
    {
        isAttack = true;
        StartCoroutine(Attackking());
    }
    IEnumerator Attackking()
    {
        Debug.Log("Atkking");
        if (BCCharacterDetail.api == null)
        {
            Debug.LogError("Not Player");
            yield return new WaitForEndOfFrame();
        }
        BCCharacterDetail.api.ChangeHealt(Damage);
        yield return new WaitForSeconds(speedAtk);
        if (isAttack)
            StartCoroutine(Attackking());
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.transform.tag == "Player")
        {
            isAttack = false;
        }
    }

}
