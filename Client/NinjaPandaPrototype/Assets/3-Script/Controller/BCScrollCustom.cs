﻿using Studio.BC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class BCScrollCustom : MonoBehaviour, IEndDragHandler,IDragHandler
{
    public enum ENUMSTYLESCROLL
    {
        normal=1,
        customScrollBar=2
    }
    // Start is called before the first frame update
    public ENUMSTYLESCROLL style = ENUMSTYLESCROLL.normal;
    public int speed;
    public int currentspeed;
    public ScrollRect scrollRect;
    public Scrollbar scrollBar;
    private Transform trContain;
    private int childCount;
    public float offset;
    public int currenChap;
    public bool isDrag;
    private Vector3 posStart;
    private Vector3 posEnd;
    private float dic;

    public void OnEndDrag(PointerEventData eventData)
    {

        isDrag = false;
        currenChap = (Mathf.RoundToInt(scrollBar.value / offset)) + 1;
        dic = Mathf.Abs(scrollBar.value - (currenChap - 1) * offset);

    }

    private void Awake()
    {
        currentspeed = speed;
        scrollRect = GetComponent<ScrollRect>();
        scrollBar = scrollRect.horizontalScrollbar;
        trContain = scrollRect.content;
        childCount = trContain.childCount;
        offset = 1.0f / (childCount - 1);
        switch (style)
        {
            case ENUMSTYLESCROLL.normal: break;
            case ENUMSTYLESCROLL.customScrollBar:
                scrollBar.value = (currenChap - 1) * offset;
                scrollBar.size = ((ScreenSize.MaxWidth / childCount) + 50) / ScreenSize.MaxWidth;
                break;
        }

    }
    public void updateData()
    {
        currentspeed = speed;
        scrollRect = GetComponent<ScrollRect>();
        scrollBar = scrollRect.horizontalScrollbar;
        trContain = scrollRect.content;
        childCount = trContain.childCount;
        offset = 1.0f / (childCount - 1);
        switch (style)
        {
            case ENUMSTYLESCROLL.normal: break;
            case ENUMSTYLESCROLL.customScrollBar:
                scrollBar.value = (currenChap - 1) * offset;
                scrollBar.size = ((ScreenSize.MaxWidth / childCount) + 50) / ScreenSize.MaxWidth;
                break;
        }
    }

    void Update()
    {
        if (!isDrag)
        {
            scrollBar.value = Vector2.MoveTowards(new Vector2(scrollBar.value, 0), new Vector2((currenChap - 1) * offset, 0), (((speed * 1.0f)/10) * Time.deltaTime)).x;
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        speed = currentspeed;
        isDrag = true;
    }
}
