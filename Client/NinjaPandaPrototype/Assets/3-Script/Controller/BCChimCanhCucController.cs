﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class BCChimCanhCucController : MonoBehaviour
{
    public float CurrentHealt = 100;
    public float HealtBasic = 100;
    public Image healtBar;

    public float DistanceToAttack;
    public float Damage = 2;
    private Animator anim;
    private BCMoveToCharacter toCharacter;
    private Transform player;
    private float currentDistance;



    public float Healt
    {
        get { return CurrentHealt; }
        set
        {
            CurrentHealt = value;
            if (CurrentHealt > 0)
                healtBar.fillAmount = CurrentHealt / HealtBasic;
        }
    }



    private void Awake()
    {
        anim = GetComponent<Animator>();
        toCharacter = GetComponent<BCMoveToCharacter>();
        //healtBar = transform.FindChild("HealtBar").AddToggleToList<Image>();
    }

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }


    private void FixedUpdate()
    {
        if (Healt > 0)
            Attack();
        else
            Die();
    }

    public void ChangeHealt(float reduceHealt)
    {

        StartCoroutine(HitRoutine());
        Healt -= reduceHealt;
    }
    void Die()
    {
        StartCoroutine(DieRoutine());
    }
    IEnumerator DieRoutine()
    {
        Debug.Log("Died");
        toCharacter.IsWalk = false;
        if (anim.GetBool("Running"))
            anim.SetBool("Running", false);
        if (anim.GetBool("Hittinh"))
            anim.SetBool("Hittinh", false);
        toCharacter.MoveSpeed = 0;
        anim.SetBool("Running", false);
        anim.SetBool("Die", true);
        GetComponent<Collider>().enabled = false;
        GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
        //AddToggleToList<Collider>().enabled = false;
        transform.tag = "MonsterDie";
        //anim.SetInteger(BCKeyAnimator.Condition, (int)AnimationType.die);
        yield return new WaitForSeconds(5f);

        Destroy(gameObject/*, 2.0f*/);
    }

    IEnumerator HitRoutine()
    {
        //if (anim.GetBool("Running") == true)
        //    anim.SetBool("Running", false);
        //anim.SetBool("Hittinh", true);
        //toCharacter.IsWalk = false;
        //anim.SetInteger(BCKeyAnimator.Condition, (int)AnimationType.hit);
        if (anim.GetBool("Running"))
            anim.SetBool("Running", false);
        anim.SetBool("Hittinh", true);
        yield return new WaitForSeconds(0.5f);
        anim.SetBool("Hittinh", false);
        //anim.SetBool("Running", true);
        //anim.SetBool("Hittinh", false);
        //anim.SetBool("Running", true);
        //anim.SetInteger(BCKeyAnimator.Condition, (int)AnimationType.run);
    }

    void Attack()
    {
        if (player == null || player.GetComponent<BCCharacterDetail>().GetStatus == Status.die) return;

        currentDistance = Vector3.Distance(transform.position, player.position);
        if (currentDistance <= DistanceToAttack)
        {
            transform.LookAt(player);
            toCharacter.IsWalk = false;
            Attacking();
        }
        else
        {
            if (anim.GetBool("Hittinh") == false)
                toCharacter.IsWalk = true;
        }

    }
    float timeLast = 0;
    float timeAnimation = 3;// speedAttack
    Vector3 posPlayer;
    int allowMoveWhenAttacking = 0;
    public void ChangeAllowMoveWhenAttacking(int a)
    {
        allowMoveWhenAttacking = a;
    }
    private void Attacking()
    {
        if (Time.time - timeLast > timeAnimation)
        {
            //anim.SetBool("Running", false);
            //anim.SetBool("Atkking", true);
            timeLast = Time.time;
            //Debug.Log("Attack");
            //player.AddToggleToList<>
            posPlayer = player.transform.position;
            StartCoroutine(AttackRoutine());
            //anim.SetInteger(BCKeyAnimator.Condition, (int)AnimationType.atk);
            //Debug.Log(anim.playbackTime);
        }
        else
        {

            if (allowMoveWhenAttacking != 0)
                transform.position = Vector3.Lerp(transform.position, posPlayer, GetComponent<BCMoveToCharacter>().MoveSpeed * 2 * Time.fixedDeltaTime);
        }
        //else
        //{
        //    Debug.Log("Idle");
        //    anim.SetInteger(BCKeyAnimator.Condition, (int)AnimationType.idle);
        //}
    }
    bool hitPlayer = false;
    IEnumerator AttackRoutine()
    {
        //if (anim.GetBool("Atkking") == true)
        //    anim.SetInteger(BCKeyAnimator.Condition, (int)AnimationType.atk);
        //hitPlayer = true;
        //Debug.Log(hitPlayer);
        anim.SetBool("Running", false);
        anim.SetBool("Hittinh", false);
        anim.SetBool("Atkking", true);

        yield return new WaitForSeconds(.5f);
        anim.SetBool("Atkking", false);
        anim.SetBool("Running", true);
        //hitPlayer = false;
        //anim.SetBool("Atkking", false);
        //if (anim.GetBool("Atkking") == false)
        //    anim.SetInteger(BCKeyAnimator.Condition, (int)AnimationType.idle);
    }
    bool allowHitDame = false;
    public void HitCharacter()
    {
        if (allowHitDame == true && player != null)
            player.GetComponent<BCCharacterDetail>().ChangeHealt(Damage);
    }

    private void OnTriggerStay(Collider obj)
    {
        if (obj.transform.tag == "Player" && hitPlayer == true)
        {
            allowHitDame = true;
            hitPlayer = false;
        }
    }

    private void OnTriggerExit(Collider obj)
    {
        if (obj.transform.tag == "Player")
            allowHitDame = true;

    }
}
