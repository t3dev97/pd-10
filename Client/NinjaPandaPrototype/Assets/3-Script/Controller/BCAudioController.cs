﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BCAudioController : MonoBehaviour
{
    // Start is called before the first frame update
    public AudioSource audioSource;
    public static BCAudioController api;
    public List<AudioClip> audioClip;
    void Awake()
    {
        if (api == null)
            api = this;
        else if (api != this) Destroy(this);
        DontDestroyOnLoad(this);
    }
    public void PlayAudio(EnumAudioSource audio)
    {
        audioSource.PlayOneShot(audioClip[(int)audio - 1]);
    }

    
}
