﻿using Studio.BC;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
public class BCPopupManager : MonoBehaviour
{
    static public BCPopupManager api;

    public GameObject Fader;
    public List<GameObject> Popups; // danh sách các popup
    public Transform Tran; // chứa popup khi bật nó lên

    internal void Show(object popupNam)
    {
        throw new NotImplementedException();
    }
    public bool isShowPopupHoiSinh = false;

    GameObject currentPopup;
    string currentPopupName;
    string pathPopup = "UIPrefab/Popups";
    private void Awake()
    {
        if (api == null)
            api = this;
        else
            Destroy(this);
        Popups.AddRange(Resources.LoadAll<GameObject>(pathPopup));
    }
    public void Show(string popupName, Action<GameObject> action = null)
    {
        //Fader.SetActive(true);
        GameManager.api.GameState = GAME_STATE.Pause;
        if (EventSystem.current != null && EventSystem.current.currentInputModule != null)
        {
            EventSystem.current.currentInputModule.DeactivateModule();
            PlayerController.api.JoyStickControl.ResetJoyStick();
        }
        for (int i = 0; i < transform.childCount; i++)
        {
            if (popupName.Equals(transform.GetChild(i).name))
            {
                transform.GetChild(i).gameObject.SetActive(true);
                return;
            }
        }
        foreach (var item in Popups)
        {
            if (popupName.Equals(item.name))
            {
                currentPopup = Instantiate(item);
                currentPopup.transform.SetParent(Tran, false);
                currentPopup.name = popupName;
                currentPopup.SetActive(true);
                currentPopupName = item.name;
                return;
            }
        }
    }
    public void ShowReturn(string popupName, Action<GameObject> action = null, GameObject objCallBack = null)
    {
        GameManager.api.GameState = GAME_STATE.Pause;
        if (EventSystem.current != null && EventSystem.current.currentInputModule != null)
        {
            EventSystem.current.currentInputModule.DeactivateModule();
            PlayerController.api.JoyStickControl.ResetJoyStick();
        }
        foreach (var item in Popups)
        {
            if (popupName.Equals(item.name))
            {
                currentPopup = Instantiate(item);
                currentPopup.transform.SetParent(Tran, false);
                currentPopup.name = popupName;
                currentPopup.SetActive(true);
                currentPopupName = item.name;
                if (action != null)
                {
                    action(currentPopup);
                }
            }
        }
    }

    public void Hide(string popupName, Action<GameObject> action = null, bool isDestroy = true, float time = 0)
    {
      

        Time.timeScale = 1;
        GameManager.api.GameState = GAME_STATE.Playing;

        for (int i = 0; i < transform.childCount; i++)
        {
        
            if (popupName.Equals(transform.GetChild(i).name))
            {
                if (isDestroy) // nếu muốn xóa luôn
                {
                    if (action != null)
                        action(transform.GetChild(i).gameObject);
                    Fader.SetActive(false);
                    if (transform.GetChild(i).Equals(Fader)) continue;
                    Destroy(transform.GetChild(i).gameObject, time);
                    return;
                }
                else // chỉ ẩn không xóa
                {

                    StartCoroutine(hiding(time, () =>
                    {
                        Fader.SetActive(false);
                        if (action != null)
                            action(transform.GetChild(i).gameObject);
                        transform.GetChild(i).gameObject.SetActive(false);
                    }));
                    return;
                }
            }
        }
    }
    public void HideAll(bool isDestroy = true, float time = 0)
    {
        Time.timeScale = 1;
        GameManager.api.GameState = GAME_STATE.Playing;

        for (int i = 0; i < transform.childCount; i++)
        {
            if (isDestroy) // nếu muốn xóa luôn
            {
                if (transform.GetChild(i).gameObject.Equals(Fader)) continue;

                Destroy(transform.GetChild(i).gameObject, time);
            }
            else // chỉ ẩn không xóa
            {
                StartCoroutine(hiding(time, () =>
                {
                    transform.GetChild(i).gameObject.SetActive(false);
                }));
            }
        }
    }

    IEnumerator hiding(float time, Action action)
    {
        yield return new WaitForSeconds(time);
    }
    public bool showGUITest = false;
    private void OnGUI()
    {
        if (showGUITest == false) return;
        if (GUI.Button(new Rect(0, 0, 150, 20), "Popup Thien Than"))
        {
            Show(PopupName.PopupThienThan);
        }
        if (GUI.Button(new Rect(0, 30, 150, 20), "Popup Ac Quy"))
        {
            Show(PopupName.PopupAcQuy);
        }
        if (GUI.Button(new Rect(0, 60, 150, 20), "Popup Thuong Nhan"))
        {
            Show(PopupName.PopupThuongNhan);
        }
        if (GUI.Button(new Rect(0, 90, 150, 20), "Popup Level Up"))
        {
            Show(PopupName.PopupLevelUp);
        }
        if (GUI.Button(new Rect(0, 120, 150, 20), "Popup Hoi Sinh"))
        {
            Show(PopupName.PopupHoiSinh);
        }
        if (GUI.Button(new Rect(0, 150, 150, 20), "Popup Vong Quay"))
        {
            ShowReturn(PopupName.PopupVongQuay, (go) => { go.GetComponent<BCPopupVongQuayController>().Init(EnumVongQuayType.Boss); });
        }
        if (GUI.Button(new Rect(0, 180, 150, 20), "Popup Pause"))
        {
            Show(PopupName.PopupPause);
        }
        if (GUI.Button(new Rect(0, 210, 150, 20), "Popup Higher Level"))
        {
            Show(PopupName.PopupHigherLevel);
        }
        if (GUI.Button(new Rect(0, 240, 150, 20), "Popup Rearch Stage"))
        {
            Show(PopupName.PopupStageRearch);
        }
    }



}
