﻿using UnityEngine;



public class BCCharacterController : MonoBehaviour
{
    public Joystick JoyStickControl;
    public float Speed;
    public bool IsMoving;

    private Rigidbody rb;
    private Animator anim;

    static public BCCharacterController api;
    private void Awake()
    {
        if (api == null)
            api = this;
        else
            Destroy(this);
    }
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        JoyStickControl = GameObject.FindGameObjectWithTag("Joytick").GetComponent<VariableJoystick>() as Joystick;
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        ControllMove();
    }
    void ControllMove()
    {
        if ((JoyStickControl.Horizontal > 0.1f || JoyStickControl.Horizontal < -0.1f
               || JoyStickControl.Vertical > 0.1f || JoyStickControl.Vertical < -0.1f) && BCCharacterDetail.api.GetStatus == Status.live)
        {
            Vector3 direction = new Vector3(JoyStickControl.Horizontal, 0f, JoyStickControl.Vertical).normalized;
            //rb.AddForce(direction, ForceMode.Force);


            rb.velocity = direction /* Time.deltaTime*/ * Speed;
            //Debug.Log(rb.velocity);


            var angle = Vector2.Angle(JoyStickControl.Direction, Vector2.up);
            if (JoyStickControl.Horizontal < 0)
                angle = -angle;
            transform.rotation = Quaternion.Euler(0, angle, 0);

            IsMoving = true;
            anim.SetBool("Running", true);
            anim.SetInteger(BCKeyAnimator.Condition, (int)AnimationType.run);
            //_runTime = 0.05f;
            //anim.SetFloat("run_time", _runTime);
        }
        else
        {
            if (IsMoving == false) return;
            Debug.Log("Idle");
            if (anim.GetBool("Running") == true)
                anim.SetInteger(BCKeyAnimator.Condition, (int)AnimationType.idle);
            IsMoving = false;
        }
    }

    private void OnCollisionEnter(Collision obj)
    {
        //if (obj.transform.tag == "PosEnd")
        //{
        //    if(PlayerPrefs.GetInt("round") == 2){
        //        PlayerPrefs.SetInt("round", PlayerPrefs.GetInt("round") - 1);
        //    }
        //    else
        //    {
        //        PlayerPrefs.SetInt("round", PlayerPrefs.GetInt("round") + 1);
        //    }
    
        //    StartCoroutine(BC_DataGame.api.LoadMap(() => { }));
        //}
    }


}
