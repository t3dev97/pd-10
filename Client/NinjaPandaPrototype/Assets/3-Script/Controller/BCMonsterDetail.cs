﻿using UnityEngine;
using UnityEngine.UI;

public class BCMonsterDetail : MonoBehaviour
{

    public float CurrentHealt = 100;
    public float HealtBasic = 100;
    //[SerializeField]
    public Image healtBar;

    public float Damage = 2;
    private Animator anim;
    private BCMoveToCharacter toCharacter;
    public float Healt
    {
        get { return CurrentHealt; }
        set
        {
            CurrentHealt = value;
            if (CurrentHealt < 1)
                Die();
            healtBar.fillAmount = CurrentHealt / HealtBasic;
        }
    }

    private void Awake()
    {
        anim = GetComponent<Animator>();
        toCharacter = GetComponent<BCMoveToCharacter>();
        //healtBar = transform.FindChild("HealtBar").AddToggleToList<Image>();
    }

    public void ChangeHealt(float reduceHealt)
    {
        Healt -= reduceHealt;
        if (CurrentHealt < 1) Die();
        healtBar.fillAmount = CurrentHealt / HealtBasic;
    }
    void Die()
    {
        toCharacter.IsWalk = false;
        anim.SetInteger(BCKeyAnimator.Condition, (int)AnimationType.die);

        Destroy(gameObject, 2.0f);
    }
}
