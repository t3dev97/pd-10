﻿using System.Collections;
using UnityEngine;

public class BCMoveCam : MonoBehaviour
{
    public GameObject player;
    public Vector3 distanceV3;
    public Vector3 posCam;
    public float posX;
    public static int Width = 800;
    public static int Height = 1280;
    public static float Size = 10f;
    public static float MinCamY;
    public static float MaxCamY;
    public void Awake()
    {
        posCam = gameObject.transform.localPosition;
        posX = posCam.x;
        //StartCoroutine(Shake(1f, 1f));
        Camera cam = Camera.main;
        float scale = ((Width * 1.0f) / Height) / ((cam.pixelWidth * 1.0f) / cam.pixelHeight * 1.0f);
        GetComponent<Camera>().orthographicSize = Size * scale;
        if (player == null)
            player = PlayerController.api.gameObject;
    }
    public void Update()
    {

        //Debug.Log(transform.position.z >= MinCamY && transform.position.z <= MaxCamY);
        //Debug.Log("minCamY" + MinCamY);
        //Debug.Log("MaxCamY" + MaxCamY);

        if (transform.position.z >= MinCamY && transform.position.z <= MaxCamY)
        {
            Vector3 vec = distanceV3 + player.transform.position;
            vec.y = posCam.y;
            vec.x = posX;
            if (vec.z < MinCamY)
            {
                vec.z = MinCamY;
            }
            if (vec.z > MaxCamY)
            {
                vec.z = MaxCamY;
            }
            gameObject.transform.position = vec;
        }
        else
        {
            Vector3 vec = distanceV3 + player.transform.position;
            vec.y = posCam.y;
            vec.x = posX;
            if (vec.z < MinCamY)
            {
                vec.z = MinCamY;
            }
            if (vec.z > MaxCamY)
            {
                vec.z = MaxCamY;
            }
            gameObject.transform.position = vec;
        }



    }
    public void updateDistranCam()
    {
        posCam = new Vector3(posCam.x, posCam.y, MinCamY);
        gameObject.transform.localPosition = posCam;

        distanceV3 = posCam - player.transform.position;
    }
    public IEnumerator Shake(float duration, float magitude)
    {
        float elapsed = 0.0f;
        Vector3 originalPos = transform.localPosition;
        //Debug.Log(originalPos);
        while (elapsed < duration)
        {
            float x = Random.Range(-1f, 1f) * magitude;
            float z = Random.Range(-1f, 1f) * magitude;

            transform.localPosition = new Vector3(originalPos.x + x, originalPos.y, originalPos.z + z);
            elapsed += Time.deltaTime;
            yield return null;

        }
        transform.localPosition = originalPos;
    }
}
