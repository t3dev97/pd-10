﻿using System.Collections;
using UnityEngine;
public class BayController : MonoBehaviour
{
    public float Damage = 10;

    float speedAtk = 1; // thời gian giản cách giữa các lần atk
    bool isAttack = false;
    private void Start()
    {
        if (GetComponent<MonsterDetail>() != null)
            Damage = GetComponent<MonsterDetail>().BasicATKDamge;
    }
    void Attack()
    {
        isAttack = true;
        StartCoroutine(Attackking());
    }
    IEnumerator Attackking()
    {
        while (isAttack)
        {
            //Debug.Log(gameObject.name + "Atkking");
            yield return new WaitForSeconds(speedAtk);

            if (PlayerController.api == null)
            {
                //Debug.LogError(gameObject.name + " Not Player");
                yield return new WaitForEndOfFrame();
            }
            PlayerController.api.Hit(Damage);
        }
    }
}
