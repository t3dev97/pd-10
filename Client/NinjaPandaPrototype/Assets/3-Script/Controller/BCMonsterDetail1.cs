﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class BCMonsterDetail1 : MonoBehaviour
{

    public float CurrentHealt = 100;
    public float HealtBasic = 100;
    //[SerializeField]
    public Image healtBar;
    public GameObject childMini;

    public Transform pos1;
    public Transform pos2;

    public float Damage = 2;
    private Animator anim;
    private BCMoveToCharacter toCharacter;
    public float Healt
    {
        get { return CurrentHealt; }
        set
        {
            CurrentHealt = value;
            if (CurrentHealt < 1)
                Die();
            healtBar.fillAmount = CurrentHealt / HealtBasic;
        }
    }

    private void Awake()
    {
        anim = GetComponent<Animator>();
        toCharacter = GetComponent<BCMoveToCharacter>();
        //healtBar = transform.FindChild("HealtBar").AddToggleToList<Image>();
    }

    public void ChangeHealt(float reduceHealt)
    {
        anim.SetInteger(BCKeyAnimator.Condition, (int)AnimationType.hit);
        Healt -= reduceHealt;
        if (CurrentHealt < 1) Die();
        healtBar.fillAmount = CurrentHealt / HealtBasic;
    }
    void Die()
    {
        StartCoroutine(DieRoutine());
    }
    IEnumerator DieRoutine()
    {
        toCharacter.IsWalk = false;
        anim.SetInteger(BCKeyAnimator.Condition, (int)AnimationType.die);
        yield return new WaitForSeconds(.5f);
        Instantiate(childMini, pos1.position, Quaternion.identity);
        Instantiate(childMini, pos2.position, Quaternion.identity);
        Destroy(gameObject/*, 2.0f*/);
    }

}
