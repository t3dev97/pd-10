﻿using Newtonsoft.Json;
using Studio;
using Studio.BC;
using System;
using UnityEngine;

public class BCDataControler : MonoBehaviour
{
    public static BCDataControler api;
    public static int waitedPurchaseGems = 0;

    public BCResourceGame data;
    public void Awake()
    {
        if (api == null) api = this;
        else if (api != this) Destroy(this);

    }
    public void UpdateData()
    {
        data = BCCache.Api.DataConfig.ResourceGame;
    }

    public void AddInventory(int id)
    {
        //data.listIventory.Add(data.listIventory.Count +1,new BCInventory(data.listIventory.Count+1,id));
        data.AddIventory(new BCInventory(data.listIventory.Count + 1, id));
    }
    public void AddInventoryView(int id)
    {
        //data.listIventory.Add(data.listIventory.Count +1,new BCInventory(data.listIventory.Count+1,id));
        data.AddIventory(new BCInventory(data.listIventory.Count + 1, id));
        BCEquipmentController.api.UpdateViewEquipment();
    }
    public void PlusGem(int intGem)
    {
        data.Gem += intGem;
        BCUIMainController.api.uiTop.updataUITop();
    }
    //action level, exp
    public void UpdateLevel(Action<int, int> action)
    {
        //data.Exp += data.RewardExpInLife;
        data.Exp += data.dataTemp.RewardExpInLife;
        while (data.Exp > (BCCache.Api.DataConfig.UserLevelConfigData[data.Level].expRequire))
        {
            data.Exp -= BCCache.Api.DataConfig.UserLevelConfigData[data.Level].expRequire;
            data.Level++;
        }
        action(data.Level, data.Exp);

        data.ResetRewardExpInLife();
    }
    public int GetExp()
    {
        return data.Exp;
    }

    public void PlusLevel(int intLevel)
    {
        data.Level += intLevel;
    }
    public void PlusCoin(int intCoin)
    {
        data.Gold += intCoin;
        BCUIMainController.api.uiTop.updataUITop();
        BCUIMainController.api.uiTop.updataUITop();
    }
    public void PlusEnergy(int intEnergy)
    {
        data.Energy += intEnergy;
        BCUIMainController.api.uiTop.updataUITop();
    }

    public void PurchaseSucceded()
    {
        PlusGem(waitedPurchaseGems);
        waitedPurchaseGems = 0;
        StartCoroutine(BCUIMainController.api.DelayOffLoading());
    }
    public void MinusGem(int intGem, Action onComplete)
    {

        if (data.Gem - intGem >= 0)
        {
            data.Gem -= intGem;
            EffecAsync(intGem);
            BCUIMainController.api.uiTop.updataUITop();
            onComplete();
        }
        else
        {
            DebugX.Log("not enough Gem");
            DebugX.Log("===============================");
            NotEnough();
        }

    }
    public bool MinusGemWithChecker(int intGem)
    {

        if (data.Gem - intGem >= 0)
        {
            data.Gem -= intGem;
            EffecAsync(intGem);
            if (BCConfig.Api.CurrentScene == CurrentSceneName.LayoutDemo)
                BCUIMainController.api.uiTop.updataUITop();
            return true;
        }
        else
        {
            DebugX.Log("not enough Gem");
            DebugX.Log("===============================");
            if (BCConfig.Api.CurrentScene == CurrentSceneName.Main)
            {
                BCViewHelper.Api.ShowMessageBox(LanguageManager.api.GetKey(BCLocalize.POPUP_NOT_GEM));
                return false;
            }

            if (BCConfig.Api.CurrentScene == CurrentSceneName.LayoutDemo)
                NotEnough();
            return false;
        }

    }
    public void converGemToEnergy(int intGem, int energy)
    {

        if (data.Gem - intGem >= 0)
        {
            data.Gem -= intGem;
            PlusEnergy(energy);
            EffecAsync(intGem);
            BCUIMainController.api.uiTop.updataUITop();
            Hide();
        }
        else
        {
            DebugX.Log("not enough Gem");
            DebugX.Log("===============================");
            NotEnough();
        }

    }
    public void AddResource(string key, int value)
    {
        switch (key)
        {
            case "Energy": PlusEnergy(value); break;
            case "Gem": PlusGem(value); break;
            case "Key_Normal": Debug.Log("add key normal"); break;
            case "Key_Epic": Debug.Log("add  Key_Epic"); break;
        }
    }
    public void Hide()
    {
        BCViewHelper.Api.Popup.HideAll(true);
        BCViewHelper.Api.PopupNotify.HideAll(true);
    }
    public void converGemToCoint(int intGem, int coint)
    {
        if (data.Gem - intGem > 0)
        {
            data.Gem -= intGem;
            PlusCoin(coint);
            EffecAsync(intGem);
            BCUIMainController.api.uiTop.updataUITop();

        }
        else
        {
            DebugX.Log("not enough Gem");
            DebugX.Log("===============================");
            NotEnough();
        }

    }
    public void MinusLevel(int intLevel)
    {
        data.Level -= intLevel;
    }
    public void MinusCoin(int intCoin)
    {

        if (data.Gold - intCoin > 0)
        {
            data.Gold -= intCoin;
            EffecAsync(intCoin);
        }
        BCUIMainController.api.uiTop.updataUITop();
    }
    public void EffecAsync(int intCoin)
    {
        //BCCache.Api.GetEffectPrefabAsync(EffectName.EffectNum, (go) => {
        //    GameObject game = Instantiate(go, BCUIMainController.api.transformEffect);
        //    go.GetComponent<BCEffectNum>().num = intCoin;
        //    go.GetComponent<BCEffectNum>().play(() => { Destroy(game); });
        //    });
    }
    public void MinusEnergy(int intEnergy, Action actionComplete)
    {

        if (data.Energy - intEnergy >= 0)
        {
#if !EDITOR
            data.Energy -= intEnergy;
#endif
            EffecAsync(intEnergy);
            actionComplete.Invoke();
        }
        else
        {
            //BCCache.Api.GetUIPrefabAsync(UIPrefabName.PopupBuyEnergy, (go) => {

            //});
            ShowPopupBuyEnergy();
        }
        BCUIMainController.api.uiTop.updataUITop();
    }
    public void ShowPopupBuyEnergy()
    {
        //BCCache.Api.GetUIPrefabAsync(UIPrefabName.PopupBuyEnergy, (go) => {

        //});
        BCViewHelper.Api.PopupNotify.Show(StateName.PopupBuyEnergy);
    }
    public void NotEnough()
    {
        BCViewHelper.Api.ShowMessageBoxCustom(LanguageManager.api.GetKey(BCLocalize.GUI_NAME_NOT_ENOUGH));
        BCUIMainController.api.ToggleValueChanged(Lobby_Position.SHOP);
        BCViewHelper.Api.Popup.HideAll();

    }
    public int GetGem()
    {
        return data.Gem;
    }
    public int GetLevel()
    {
        return data.Level;
    }
    public int GetCoin()
    {
        return data.Gold;
    }
    public int GetEnergy()
    {
        return data.Energy;
    }
    public void UpdateEquipemnt(int id)
    {
        data.listIventory[id].intLevel++;
        PlayerPrefs.SetString(PlayerPrefsKey.ListInventory, JsonConvert.SerializeObject(data.listIventory));

        data.listMaterials[data.listIventory[id].IDItem.typeEquipment.id].amount -= BCCache.Api.DataConfig.materialUpdateConfigData[data.listIventory[id].intLevel].requireMaterial;
        PlayerPrefs.SetString(PlayerPrefsKey.ListMaterial, JsonConvert.SerializeObject(data.listMaterials));
    }

}
