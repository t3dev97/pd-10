﻿using UnityEngine;

public class BCLockObject : MonoBehaviour
{
    public GameObject objectLock;

    private void Awake()
    {
        if (objectLock == null)
            objectLock = PlayerController.api.gameObject;
    }

    void Update()
    {
        transform.position = objectLock.transform.position;
    }
}
