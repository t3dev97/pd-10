﻿using Studio;
using Studio.BC;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class BC_DataGame : MonoBehaviour
{
    public static BC_DataGame api;

    public YuME_tilesetData dataTileSetData;
    public GameObject yumeMapEditor;
    public BC_DataMapEditor fileSaveData;
    public GameObject PlayerObj;
    public PhysicMaterial masat;

    public int chapter;
    public int stage;
    public int currentIDMap;
    public int WidthMap;
    public int HeightMap;

    public bool isLoadData = false;

    public string[] tileSetNames;

    public Dictionary<int, Material> currentTileSetMaterial;
    public Dictionary<int, Dictionary<int, GameObject>> currentTileSetTexture;
    public Dictionary<int, GameObject> currentTileSetTextureMain;
    public Dictionary<int, GameObject> currentTileSetModeMoster;
    public Dictionary<int, GameObject> currentTileSetModeMosterBoos;
    public Dictionary<int, GameObject> currentTileSetDoor;
    public Dictionary<int, GameObject> borderMap;
    public Dictionary<int, GameObject> mapCut;
    public Dictionary<int, GameObject> backGroundMap;
    public Dictionary<int, GameObject> npcMap;
    public Dictionary<int, bool> listMapCreate;
    public Dictionary<int, Dictionary<int, Vector3>> listBlock = new Dictionary<int, Dictionary<int, Vector3>>();


    private static int intCutMap = 6;    // loc bao nhieu image là 1 skype
    private static float HeightWallBorder = 8;    // loc bao nhieu image water la 1 style
    private static int intJumpWater = 24;
    private static int intCapAnh = 2; // phân loại cặp ảnh

    private int MaxImageTrangTri = 6;
    private int MinImageTrangTri = 2;

    private float gridOffset = 0.25f;
    private float _HeightCharater = 4;
    private float _DoLechImageTop = 0.5f;
    private float _Offset = 0.5f;

    private Dictionary<int, GameObject> _currentTileSetObjects;

    private GameObject[] listCube;

    void Awake()
    {

        if (api == null)
        {
            api = this;
        }
        else if (this != api)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);

        PlayerPrefs.SetInt("chap", 1);
        PlayerPrefs.SetInt("round", 1);

        if (!api.isLoadData)
        {
            Init();
            LoadData();
            UploadFile();
        }
    }
    private void Start()
    {
        FindObject();
    }
    public void FindObject()
    {
        api.listMapCreate.Clear();
        api.yumeMapEditor = GameObject.Find("YuME_MapData");

        if (BCConfig.Api.CurrentScene == CurrentSceneName.Main)
            PlayerObj = PlayerDetail.api.gameObject;
    }

    public void Init()
    {
        _currentTileSetObjects = new Dictionary<int, GameObject>();
        currentTileSetMaterial = new Dictionary<int, Material>();
        currentTileSetTexture = new Dictionary<int, Dictionary<int, GameObject>>();
        currentTileSetTextureMain = new Dictionary<int, GameObject>();
        currentTileSetModeMoster = new Dictionary<int, GameObject>();
        currentTileSetModeMosterBoos = new Dictionary<int, GameObject>();
        npcMap = new Dictionary<int, GameObject>();
        currentTileSetDoor = new Dictionary<int, GameObject>(); ;
        borderMap = new Dictionary<int, GameObject>();
        mapCut = new Dictionary<int, GameObject>();
        backGroundMap = new Dictionary<int, GameObject>();
        listMapCreate = new Dictionary<int, bool>();
        listCube = new GameObject[10];
    }

    public void UploadFile()
    {
        fileSaveData.listMap.Clear();
        foreach (MapStageEditor tg in fileSaveData.listMaptg)
        {
            fileSaveData.listMap.Add(tg.ID, tg);
        }
    }

    public void LoadData()
    {
        _currentTileSetObjects = LoadNinjaMapResource("BlockBrushes");
        currentTileSetMaterial = LoadNinjaMapMaterial("GroundBrushes");
        currentTileSetModeMoster = LoadNinjaMapResource("MonsterBrushes");
        currentTileSetModeMosterBoos = LoadNinjaMapResource("BossBrushes");
        currentTileSetDoor = LoadNinjaMapResource("DoorBrush");
        npcMap = LoadNinjaMapResource("NPC");
        currentTileSetTexture = LoadNinjaMapWaterResource("WaterBrushes");
        currentTileSetTextureMain = LoadNinjaMapWaterStyleResource("WaterBrushes");
        borderMap = LoadNinjaMapResource("BoderMap");
        mapCut = LoadNinjaMapResource("MapCut");
        isLoadData = true;
    }

    #region LoadNinjaMapResource
    public static Dictionary<int, GameObject> LoadNinjaMapResource(string resourceName)
    {
        var request = Resources.Load<TextAsset>("Config/Resource_" + resourceName);
        var dict = request.text.JsonToDictionary();
        var data = dict.Get<Dictionary<string, object>>(BCKey.data);
        try
        {
            var returnGameObjects = new Dictionary<int, GameObject>();
            foreach (var item in data)
            {
                string loadPath = resourceName + "/" + item.Value;
                GameObject game = Resources.Load(loadPath, typeof(GameObject)) as GameObject;
                returnGameObjects.Add(int.Parse(item.Key), game);

            }
            return returnGameObjects;
        }
        catch
        {
            return null;
        }
    }
    #endregion

    #region LoadNinjaMapMaterial
    public static Dictionary<int, Material> LoadNinjaMapMaterial(string resourceName)
    {
        var request = Resources.Load<TextAsset>("Config/Resource_" + resourceName);
        var dict = request.text.JsonToDictionary();
        var data = dict.Get<Dictionary<string, object>>(BCKey.data);
        try
        {
            var returnGameObjects = new Dictionary<int, Material>();
            foreach (var item in data)
            {
                string loadPath = resourceName + "/" + item.Value;
                Material game = Resources.Load(loadPath, typeof(Material)) as Material;
                returnGameObjects.Add(int.Parse(item.Key), game);
            }

            return returnGameObjects;
        }
        catch
        {
            return null;
        }
    }
    #endregion

    #region LoadNinjaMapWaterResource
    public static Dictionary<int, Dictionary<int, GameObject>> LoadNinjaMapWaterResource(string resourceName)
    {
        var request = Resources.Load<TextAsset>("Config/Resource_" + resourceName);
        var dict = request.text.JsonToDictionary();
        var data = dict.Get<Dictionary<string, object>>(BCKey.data);
        var returnGameObjects = new Dictionary<int, Dictionary<int, GameObject>>();

        try
        {
            int i = 0;
            foreach (var item in data)
            {
                var waterStylesDatas = (Dictionary<string, object>)item.Value;
                var returnGameObjectChilds = new Dictionary<int, GameObject>();
                foreach (var waterStyleData in waterStylesDatas)
                {
                    string loadPath = resourceName + "/" + item.Key + "/" + waterStyleData.Value;
                    //returnGameObjectChilds.Add(int.Parse(item.Key), AssetDatabase.LoadAssetAtPath(loadPath, typeof(GameObject)) as GameObject);
                    GameObject game = Resources.Load(loadPath, typeof(GameObject)) as GameObject;
                    returnGameObjectChilds.Add(int.Parse(waterStyleData.Key), game);
                    i++;
                }
                returnGameObjects.Add(int.Parse(item.Key), returnGameObjectChilds);
            }
            return returnGameObjects;
        }
        catch (Exception EX)
        {
            Debug.LogError(EX);
            return null;
        }
    }
    #endregion

    #region LoadNinjaWaterStyleResource
    public static Dictionary<int, GameObject> LoadNinjaMapWaterStyleResource(string resourceName)
    {
        var request = Resources.Load<TextAsset>("Config/Resource_" + resourceName);
        var dict = request.text.JsonToDictionary();
        var data = dict.Get<Dictionary<string, object>>(BCKey.data);
        var returnGameObjects = new Dictionary<int, GameObject>();

        try
        {
            int i = 0;
            foreach (var item in data)
            {
                var waterStylesDatas = (Dictionary<string, object>)item.Value;
                var returnGameObjectChilds = new Dictionary<int, GameObject>();
                foreach (var waterStyleData in waterStylesDatas)
                {
                    string loadPath = "Assets/Resources/" + resourceName + "/" + item.Key + "/" + waterStyleData.Value;

                    returnGameObjects.Add(int.Parse(item.Key), Resources.Load(loadPath, typeof(GameObject)) as GameObject);
                    break;
                }
            }
            return returnGameObjects;
        }
        catch
        {
            return null;
        }
    }
    #endregion

    #region LoadDirectoryMaterial
    public static Material[] loadDirectoryMaterial(string path)
    {
        try
        {
            var returnGameObjects = Resources.LoadAll(path);

            Material[] returnGameObjects1 = new Material[returnGameObjects.Length];
            for (int i = 0; i < returnGameObjects.Length; i++)
            {
                returnGameObjects1[i] = (Material)returnGameObjects[i];
            }

            return returnGameObjects1;
        }
        catch
        {
            return null;
        }
    }
    #endregion

    #region loadDirectoryContents
    public static GameObject[] loadDirectoryContents(string path)
    {
        try
        {
            var returnGameObjects = Resources.LoadAll(path, typeof(GameObject));
            GameObject[] returnGameObjects1 = new GameObject[returnGameObjects.Length];
            for (int i = 0; i < returnGameObjects.Length; i++)
            {
                returnGameObjects1[i] = (GameObject)returnGameObjects[i];
            }

            return returnGameObjects1;
        }
        catch
        {
            return null;
        }
    }
    #endregion

    public void ScaleMap()
    {
        Vector3 vec = yumeMapEditor.transform.GetChild(0).localScale;
        vec.z = BC_GameController.api.heSoScale;
        yumeMapEditor.transform.GetChild(0).localScale = vec;
    }

    public void ClearObjectMap()
    {
        FindObject();
        foreach (Transform layout in yumeMapEditor.transform)
        {
            if (layout.childCount > 0)
            {
                foreach (Transform gameObject in layout)
                {
                    Destroy(gameObject.gameObject);
                }
            }
        }

        listBlock.Clear();

        yumeMapEditor.transform.GetChild(0).localScale = new Vector3(1, 1, 1);
    }

    public void CreateMapDecorate(int matPlan, MapStageEditor map)
    {
        System.Random rnd = new System.Random();
        int soLuongHoa = rnd.Next(MinImageTrangTri, MaxImageTrangTri);
        for (int i = 0; i < soLuongHoa; i++)
        {
            int widthBlock = rnd.Next(0, map.Width);
            int heightBlock = rnd.Next(0, map.height);
            int rndImage = rnd.Next(0, 2);
            while (!listBlock[widthBlock].ContainsKey(heightBlock))
            {
                widthBlock = rnd.Next(0, map.Width);
                heightBlock = rnd.Next(0, map.height);
            }
            GameObject game = Instantiate(mapCut[2 + rndImage + (matPlan * intCapAnh)], yumeMapEditor.transform.GetChild(0));
            game.transform.localPosition = listBlock[widthBlock][heightBlock];
        }
    }

    public GameObject CreateObject(ObjectMapEditor item, MapStageEditor map, ref int matPlan, ref List<GameObject> listObject)
    {
        GameObject obj = null;
        switch (item.style)
        {
            case STYLE_BRUSHES.block:
                obj = Instantiate(_currentTileSetObjects[item.indexObject]);
                break;
            case STYLE_BRUSHES.ground:
                obj = CreateGround(true, item.indexObject, map.isSymmetry);
                matPlan = item.indexObject;
                break;
            case STYLE_BRUSHES.water:
                int ac = (item.indexObject / intJumpWater) + 1;
                int acc = item.indexObject % intJumpWater;

                obj = Instantiate(currentTileSetTexture[ac][acc]);
                break;
            case STYLE_BRUSHES.monster:
                //Debug.Log("item.indexObject"+ item.indexObject);
                obj = Instantiate(currentTileSetModeMoster[item.indexObject]);
                obj.GetComponent<BC_ObjectInfo>().Id = item.indexObject;

                listObject.Add(obj);
                break;
            case STYLE_BRUSHES.boss:
                obj = Instantiate(currentTileSetModeMosterBoos[item.indexObject]);
                obj.GetComponent<BC_ObjectInfo>().Id = item.indexObject;
                yumeMapEditor.GetComponent<BC_Chapter_Info>().isStageBoos = true;
                listObject.Add(obj);
                break;
        }
        return obj;
    }

    public int CreateObjectMap(MapStageEditor map, int indexChild, bool isdungeon)
    {
        int matPlan = 0;

        LoadObjectMap(map, indexChild, isdungeon, ref matPlan);

        CreateObjectHardCode();

        return matPlan;
    }

    public void LoadObjectMap(MapStageEditor map, int indexChild, bool isdungeon, ref int matPlan)
    {
        HeightMap = map.height;
        WidthMap = map.Width;

        int indexX = 0;
        int indexY = 0;

        float widthmin = ((map.Width * 1.0f) / 2) * -1;
        float heightmin = ((map.height * 1.0f) / 2) * -1;
        float heightMax = ((map.height * 1.0f) / 2) - 1;

        List<ObjectMapEditor> list = new List<ObjectMapEditor>();
        MonsterArray listMonster = new MonsterArray();
        List<GameObject> listObject = new List<GameObject>();

        if (isdungeon)
        {
            list = map.listMonster[indexChild].list;
            indexChild = indexChild + 2;
        }
        else
        {
            list = map.listBlock;
        }

        if (listBlock.Count == 0)
        {
            for (int i = 0; i < map.Width; i++)
            {
                Dictionary<int, Vector3> listTG = new Dictionary<int, Vector3>();

                for (int y = 0; y < map.height; y++)
                {
                    listTG.Add(y, new Vector3(widthmin + i, -0.49F, heightmin + y));
                }
                listBlock.Add(i, listTG);
            }
        }

        foreach (ObjectMapEditor item in list)
        {
            STYLE_BRUSHES style = item.style;
            GameObject obj = CreateObject(item, map, ref matPlan, ref listObject);

            if (obj != null)
            {
                if (item.style == STYLE_BRUSHES.monster || item.style == STYLE_BRUSHES.custom || item.style == STYLE_BRUSHES.boss)
                {
                    obj.transform.SetParent(yumeMapEditor.transform.GetChild(indexChild).transform);
                    Vector3 vec = item.pos;
                    vec.z = vec.z * BC_GameController.api.heSoScale;
                    vec.y = -0.5f;
                    obj.transform.localPosition = vec;
                }
                else
                {
                    obj.transform.SetParent(yumeMapEditor.transform.GetChild(0).transform);
                    if (item.style == STYLE_BRUSHES.door && item.indexObject == 0)
                    {
                        Vector3 vec = BC_Chapter_Info.api.posEnd;
                        vec.x = item.pos.x;
                        BC_Chapter_Info.api.posEnd = vec;

                        obj.transform.localPosition = BC_Chapter_Info.api.posEnd;
                        obj.transform.localScale = new Vector3(2, 1, 1);
                    }
                    else
                        obj.transform.localPosition = item.pos;
                }

                if (item.style == STYLE_BRUSHES.block || item.style == STYLE_BRUSHES.water)
                {
                    indexX = (int)(item.pos.x - widthmin);
                    indexY = (int)(heightMax + item.pos.z) + 1;

                    listBlock[indexX].Remove(indexY);
                }
            }
        }


        listMonster.listMonster = listObject;
        if (listMonster.listMonster.Count > 0)
        {
            yumeMapEditor.GetComponent<BC_Chapter_Info>().listStageMonter.Add(listMonster);
        }

    }

    public void CreateObjectHardCode()
    {
        for (int i = 0; i < 2; i++)
        {
            GameObject game = Instantiate(currentTileSetDoor[i], yumeMapEditor.transform.GetChild(0));
            switch (i)
            {
                case 0: game.transform.localPosition = BC_Chapter_Info.api.posEnd; game.transform.localScale = new Vector3(2, 1, 1); break;
                case 1: game.transform.localPosition = BC_Chapter_Info.api.posStart; break;
            }
        }
    }

    public int RandomIDMap(BCStageInfo info)
    {
        int index = UnityEngine.Random.Range(0, info.listIDMap.Count);
        int ID = info.listIDMap[index];


        while (api.listMapCreate.ContainsKey(ID))
        {
            index = UnityEngine.Random.Range(0, info.listIDMap.Count);
            ID = info.listIDMap[index];
        }
        api.listMapCreate.Add(ID, true);

        return ID;
    }

    public GameObject CreatePlance(bool collider, int mat)
    {
        GameObject go = new GameObject("Plane");
        go.AddComponent<BoxCollider>();
        go.tag = TagManager.api.Ground;
        MeshFilter mf = go.AddComponent(typeof(MeshFilter)) as MeshFilter;
        MeshRenderer mr = go.AddComponent(typeof(MeshRenderer)) as MeshRenderer;
        Mesh m = new Mesh();
        int witdhPlanTop = WidthMap / 2;
        m.vertices = new Vector3[]
        {
            new Vector3(0,0,0),
            new Vector3(0,0, HeightMap ),
            new Vector3(WidthMap, 0,HeightMap),
            new Vector3(WidthMap,0,0)
        };
        m.uv = new Vector2[]
        {
            new Vector2(0,0),
            new Vector2(1,0),
            new Vector2(1,1),
            new Vector2(0,1),
        };
        m.triangles = new int[] { 0, 2, 3, 0, 1, 2 };
        mf.mesh = m;
        if (collider)
        {
            (go.AddComponent(typeof(MeshCollider)) as MeshCollider).sharedMesh = m;
        }
        //Debug.Log("MAT" + mat);
        currentTileSetMaterial[mat].SetTextureScale("_MainTex", new Vector2(HeightMap * 1.0f / 2, WidthMap * 1.0f / 2));
        mr.material = currentTileSetMaterial[mat];
        m.RecalculateBounds();
        m.RecalculateNormals();

        mr.lightProbeUsage = UnityEngine.Rendering.LightProbeUsage.Off;
        mr.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
        mr.reflectionProbeUsage = UnityEngine.Rendering.ReflectionProbeUsage.Off;

        go.transform.parent = yumeMapEditor.transform.GetChild(0).transform;
        go.transform.position = new Vector3(-(((int)WidthMap * 1.0f) / 2) - gridOffset * 2, -0.5f, -(((int)HeightMap * 1.0f) / 2) - gridOffset * 2);
        return go;
    }

    public GameObject CreateGround(bool collider, int mat, bool checkDoiXung)
    {
        GameObject go = CreatePlance(collider, mat);
        BC_ObjectInfo info = go.AddComponent<BC_ObjectInfo>();
        info.Id = mat;
        info.style = STYLE_BRUSHES.ground;
        info.pos = go.transform.position;

        UpdateListCube(go);

        UpdatePositionChapter();



        GameObject[] listMapBackground = new GameObject[intCapAnh];
        for (int i = 0; i < intCapAnh; i++)
        {
            listMapBackground[i] = Instantiate(mapCut[i + (mat * intCapAnh)], go.transform);
        }
        GameObject[] WallLeft = new GameObject[intCapAnh];
        GameObject[] WallRight = new GameObject[intCapAnh];

        for (int i = 0; i < intCapAnh; i++)
        {
            WallLeft[i] = Instantiate(mapCut[i + 4 + (mat * intCapAnh)], go.transform);
            WallRight[i] = Instantiate(mapCut[i + 4 + (mat * intCapAnh)], go.transform);
            WallRight[i].GetComponent<SpriteRenderer>().flipX = true;

        }

        double cam = BC_GameController.api.DegreeToRadian((double)BC_GameController.api.Camera.transform.eulerAngles.x);

        float posZ = (float)((double)_HeightCharater * (Math.Cos(cam)));
        setMapCut(listMapBackground[0], new Vector3(((listMapBackground[0].GetComponent<SpriteRenderer>().size.x / 2) - 1), _HeightCharater - 1, (float)((((listMapBackground[0].GetComponent<SpriteRenderer>().size.y / 2) - 1f) * -1) - posZ + .5f)));
        setMapCut(listMapBackground[1], new Vector3(((listMapBackground[1].GetComponent<SpriteRenderer>().size.x / 2) - 1), 0, (HeightMap + ((listMapBackground[1].GetComponent<SpriteRenderer>().size.y / 2))) - 0.8f));

        float CamZ = (float)((double)BC_GameController.api.Camera.transform.position.y * (Math.Cos(BC_GameController.api.Camera.transform.eulerAngles.x * Mathf.Deg2Rad)));

        float heighBot = Camera.main.transform.position.y - listMapBackground[0].transform.position.y - _Offset;
        float heightTop = Camera.main.transform.position.y - listMapBackground[1].transform.position.y - _Offset;
        float posZTop = (float)((double)heightTop * (Math.Cos(cam)));
        float posZBot = (float)((double)heighBot * (Math.Cos(cam)));
        float minCam = (((listMapBackground[0].transform.position.z) - ((listMapBackground[0].GetComponent<SpriteRenderer>().size.y / 2)) - _Offset) * BC_GameController.api.heSoScale);
        float MaxCam = (((listMapBackground[1].transform.position.z) + ((listMapBackground[1].GetComponent<SpriteRenderer>().size.y / 2))) - _Offset) * BC_GameController.api.heSoScale;

        float PosZMinCam = (minCam - (posZBot) * BC_GameController.api.heSoScale);
        float PozMaxCam = (MaxCam - (posZTop) * BC_GameController.api.heSoScale);

        float zCamToCamBot = (Camera.main.orthographicSize / Mathf.Cos((90 - BC_GameController.api.Camera.transform.eulerAngles.x) * Mathf.Deg2Rad));

        BCMoveCam.MaxCamY = PozMaxCam - zCamToCamBot - _Offset;
        BCMoveCam.MinCamY = PosZMinCam + zCamToCamBot;

        Vector3 vec1 = WallLeft[0].GetComponent<SpriteRenderer>().size;

        Vector3 vec3 = WallRight[0].GetComponent<SpriteRenderer>().size;

        if (checkDoiXung)
        {
            Vector3 vec2 = WallRight[1].GetComponent<SpriteRenderer>().size;
            vec2.y = vec2.y + vec1.y / 3;
            WallRight[1].GetComponent<SpriteRenderer>().size = vec2;
        }


        vec1.y = HeightMap - WallLeft[1].GetComponent<SpriteRenderer>().size.y + 1;
        // center
        WallLeft[0].GetComponent<SpriteRenderer>().size = vec1;
        WallRight[0].GetComponent<SpriteRenderer>().size = vec1;

        setMapCut(WallLeft[0], new Vector3(-(WallLeft[0].GetComponent<SpriteRenderer>().size.x / 2) + 0.5f,
            _HeightCharater,
            (HeightMap - ((WallLeft[1].GetComponent<SpriteRenderer>().size.y))) - WallLeft[0].GetComponent<SpriteRenderer>().size.y / 2 - posZ + _DoLechImageTop + 0.5f));
        setMapCut(WallLeft[1], new Vector3(-(WallLeft[1].GetComponent<SpriteRenderer>().size.x / 2) + 0.5f, _HeightCharater, (HeightMap + ((WallLeft[1].GetComponent<SpriteRenderer>().size.y / 2))) - posZ - (WallLeft[1].GetComponent<SpriteRenderer>().size.y / 2) - _DoLechImageTop));



        if (checkDoiXung)
        {
            Vector3 vec2 = WallRight[0].GetComponent<SpriteRenderer>().size;
            vec2.y = HeightMap - WallRight[1].GetComponent<SpriteRenderer>().size.y + 1;
            WallRight[0].GetComponent<SpriteRenderer>().size = vec2;
        }

        setMapCut(WallRight[0], new Vector3(WidthMap - (WallLeft[0].GetComponent<SpriteRenderer>().size.x / 2) + 1f,
         _HeightCharater,
         (HeightMap - ((WallRight[1].GetComponent<SpriteRenderer>().size.y))) - WallRight[0].GetComponent<SpriteRenderer>().size.y / 2 - posZ + _DoLechImageTop + 0.5f));


        setMapCut(WallRight[1], new Vector3(WidthMap - (WallRight[1].GetComponent<SpriteRenderer>().size.x / 2) + 1f, _HeightCharater, (HeightMap + ((WallLeft[1].GetComponent<SpriteRenderer>().size.y / 2))) - posZ - (WallRight[1].GetComponent<SpriteRenderer>().size.y / 2) - _DoLechImageTop));
        GameObject borderShaDowMap = Instantiate(borderMap[0], go.transform, true);
        //borderShaDowMap.GetComponent<Renderer>().material.
        borderShaDowMap.GetComponent<SpriteRenderer>().size = new Vector2(WidthMap, HeightMap);

        Vector3 vecBorder = borderShaDowMap.transform.position;
        vecBorder.x = (WidthMap * 1.0f) / 2;

        vecBorder.z = (HeightMap * 1.0f) / 2;
        vecBorder.y = 0.01f;
        borderShaDowMap.transform.localPosition = vecBorder;

        return go;
    }
    public void UpdatePositionChapter()
    {
        BC_Chapter_Info.api.door = listCube[4];
        Vector3 vec = listCube[8].transform.position;
        vec.y = 0;
        vec.z += 0.5f;
        BC_Chapter_Info.api.posEnd = vec;
        BC_Chapter_Info.api.posNPC = vec;

        vec = listCube[2].transform.position;
        vec.y = 0;
        vec.z += BC_GameController.api.heSoScale / 2 + 0.5f;
        BC_Chapter_Info.api.posStart = vec;
    }

    public void UpdateListCube(GameObject go)
    {
        for (int i = 0; i < listCube.Length; i++)
        {
            listCube[i] = GameObject.CreatePrimitive(PrimitiveType.Cube);
            listCube[i].transform.localScale = new Vector3(1, 1, 1);
            listCube[i].name = "cube id" + i;
            listCube[i].tag = TagManager.api.WallBorder;
            listCube[i].layer = LayerMask.NameToLayer("HideObject");
            listCube[i].transform.SetParent(go.transform);
            listCube[i].GetComponent<BoxCollider>().material = masat;
        }

        listCube[0].transform.Rotate(new Vector3(0, 90, 0));
        listCube[1].transform.Rotate(new Vector3(0, 90, 0));
        listCube[6].transform.Rotate(new Vector3(0, 90, 0));
        listCube[7].transform.Rotate(new Vector3(0, 90, 0));
        listCube[8].transform.Rotate(new Vector3(90, 0, 0));

        setInfoGame(listCube[0], HeightMap, new Vector3(((((WidthMap * 1.0f) / 2)) * -1) - 1f, 0, 0));
        setInfoGame(listCube[1], HeightMap, new Vector3(((((WidthMap * 1.0f) / 2))), 0, 0));
        setInfoGame(listCube[2], WidthMap, new Vector3(-0.5f, 0, ((((HeightMap * 1.0f) / 2)) * -1) - 0.5f));
        setInfoGame(listCube[3], WidthMap / 2 - 0.5f, new Vector3(((((WidthMap * 1.0f) / 2) / 2) * -1) - 1, 0, 0.5f + (((HeightMap * 1.0f) / 2))));
        setInfoGame(listCube[4], 2, new Vector3(-0.5f, 0, 0.5f + (((HeightMap * 1.0f) / 2))));
        setInfoGame(listCube[5], WidthMap / 2 - 0.5f, new Vector3((((WidthMap * 1.0f) / 2) / 2), 0, 0.5f + (((HeightMap * 1.0f) / 2))));
        setInfoGame(listCube[6], 1, new Vector3(-2f, 0, (((HeightMap * 1.0f) / 2)) + 1.5f));
        setInfoGame(listCube[7], 1, new Vector3(1f, 0, (((HeightMap * 1.0f) / 2)) + 1.5f));
        setInfoGame(listCube[9], 2, new Vector3(-0.5f, 0, (((HeightMap * 1.0f) / 2)) + 1.5f));
        setInfoGame(listCube[8], 2, new Vector3(-0.5f, -1.5f, (((HeightMap * 1.0f) / 2)) + 0.5f));
    }

    public static void setMapCut(GameObject game, Vector3 pos)
    {
        Vector3 vec = game.transform.localPosition;
        vec.x = pos.x;
        vec.y = pos.y;
        vec.z = pos.z;
        game.transform.localPosition = vec;
    }
    public static void setInfoGame(GameObject game, float scale, Vector3 pos)
    {
        game.transform.localScale = new Vector3(scale, HeightWallBorder, 1);

        Vector3 vec = pos;
        vec.z = vec.z - 0.5f;
        game.transform.position = vec;
    }
    public static void setInfoGame(GameObject game, float scale, Vector3 pos, Vector3 rotate)
    {
        Transform trWall = game.transform;
        trWall.localScale = new Vector3(scale, 1, 0);
        trWall.position = pos;
        game.transform.position = trWall.position;
        game.transform.localScale = trWall.localScale;
        trWall.Rotate(rotate);

    }

    public void ResetMap()
    {
        ClearObjectMap();
        yumeMapEditor.GetComponent<BC_Chapter_Info>().isStageBoos = false;
        BC_Chapter_Info.api.FinalStage = false;
        BC_Chapter_Info.api.ResetList();
    }
    public IEnumerator LoadMap(Action action)
    {
        ResetMap();

        UIBasicController.api.TmpStage.SetLocalize(BCLocalize.UIBASIC_LEVEL_STAGE, new string[] { PlayerPrefs.GetInt("round").ToString() });

        BC_Chapter_Info chapterInfo = yumeMapEditor.GetComponent<BC_Chapter_Info>();
        BCStageInfo info = BCCache.Api.DataConfig.MapInfo[PlayerPrefs.GetInt("chap")].listStage[PlayerPrefs.GetInt("round")];
        int ID = RandomIDMap(info);

        if (BCApiTest.api.isGoToTest)
            ID = 9999;

        //Debug.Log(PlayerPrefs.GetInt("round"));
        currentIDMap = ID;
        MapStageEditor map = fileSaveData.listMap[ID];

        HeightMap = map.height;
        WidthMap = map.Width;
        chapterInfo.isDungeon = map.isDungeon;
        chapterInfo.IDMap = ID;

        int matPlan = CreateObjectMap(map, 1, false);

        if (chapterInfo.isDungeon)
        {
            int countLayout = 1;
            int countStage = 0;
            foreach (Transform tg in yumeMapEditor.transform)
            {
                if (countLayout != 1 && countLayout != 2)
                {
                    chapterInfo.listStage.Add(tg.gameObject);
                    tg.gameObject.SetActive(false);
                    CreateObjectMap(map, countStage, true);
                    countStage++;
                    if (countStage == map.listMonster.Count)
                    {
                        break;
                    }
                }
                countLayout++;
            }
            chapterInfo.time = fileSaveData.listMap[ID].TimeCreateMonster;
        }

        ScaleMap();

        CreateMapDecorate(matPlan, map);

        yumeMapEditor.transform.gameObject.isStatic = false;

        foreach (Transform item in yumeMapEditor.transform.GetChild(0))
        {
            item.gameObject.isStatic = false;
        }

        BC_GameController.api.reset();

        action.Invoke();

        yield return null;

    }


}
