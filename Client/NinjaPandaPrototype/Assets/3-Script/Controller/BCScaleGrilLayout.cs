﻿using Studio.BC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BCScaleGrilLayout : MonoBehaviour
{
    void Start()
    {
        RectTransform parentRect = gameObject.GetComponent<RectTransform>();
        GridLayoutGroup gridLayout = gameObject.GetComponent<GridLayoutGroup>();
        LayoutElement layoutElement = gameObject.GetComponent<LayoutElement>();
        gridLayout.cellSize = new Vector2(gridLayout.cellSize.x, gridLayout.cellSize.y * BCUIMainController.api.scaleHeight);
       
    }
}
