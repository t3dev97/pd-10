﻿using Studio.BC;
using System.Collections;
using UnityEngine;
public class FloatingDamage : MonoBehaviour
{

    public Vector3 Offset = new Vector3(0, -.3f, 0);
    public Vector3 RandomPos = new Vector3(0.5f, .2f, 0);

    private void OnEnable()
    {
        //transform.GetChild(0).localPosition += Offset;
        //transform.GetChild(0).localPosition += new Vector3(
        //    Random.Range(-RandomPos.x, RandomPos.x),
        //    Random.Range(-RandomPos.y, RandomPos.y),
        //    Random.Range(-RandomPos.z, RandomPos.z)
        //    );

        transform.parent = GameObject.FindObjectOfType<BCCache>().transform;
        Hide();
    }
    public void Hide()
    {
        Destroy(gameObject, 1.5f);
        StartCoroutine(HideText());
    }
    IEnumerator HideText()
    {
        yield return new WaitForSeconds(0.5f);
        transform.GetChild(0).gameObject.SetActive(false);
    }
}
