﻿using UnityEngine;
using UnityEngine.AI;

public class InitMap : MonoBehaviour
{
    static public InitMap api;

    [SerializeField]
    bool testMapDone = false; // dùng để test o scene done

    [SerializeField]
    NavMeshSurface navMeshSurface;


    private void Awake()
    {
        if (api == null)
            api = this;
        else
            Destroy(this);
        gameObject.isStatic = true;

        navMeshSurface = GetComponent<NavMeshSurface>();

        if (testMapDone)
            CreateNavigation();
    }
    public void CreateNavigation()
    {
        navMeshSurface.BuildNavMesh();
        //Debug.Log("Created Navigation");
    }
    public void RemoveNavigation()
    {
        navMeshSurface.RemoveData();
        //Debug.Log("Removed Navigation");
    }
    
}
