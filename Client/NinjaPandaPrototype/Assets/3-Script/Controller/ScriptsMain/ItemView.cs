﻿using UnityEngine;
using UnityEngine.UI;

public class ItemView : MonoBehaviour
{
    public BCDynamicImage img;
    public ImageUIHelper imgUI;
    public Text TxtCount;
}
