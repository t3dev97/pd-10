﻿using UnityEngine;

[CreateAssetMenu(fileName = "PLAYER", menuName = "Character Asset")]
public class CharacterAsset : ScriptableObject // dung để tạo thông tin asset, sử dụng để tool đọc lên và thay đổi, còn gameplay sẻ đọc lên(không thay đổi) để thiết lập khi vào game
{
    [SerializeField]
    string name;
    [SerializeField]
    float basicHP; // HP cở bản
    [SerializeField]
    float basicATKDamage; // sát thương cơ bản
    [SerializeField]
    float basicMS; // tốc độ di chuyển cơ bản
    [SerializeField]
    float basicAS; // tốc độ tấn công cơ bản, tính theo cái/giây : càng CAO càng NHANH
    [SerializeField]
    float basicCR; // phần trăm khả năng gây chí mạng cơ bản
    [SerializeField]
    float basicCD; // phần trăm sát thương khi chí mạng cơ bản 
    [SerializeField]
    float basicDodge; // phầm trăm khả năng né khi bị trúng đòn cơ bản 
    [SerializeField]
    float basicMR; // phần trăm kháng sát thương CẬN chiến cơ bản
    [SerializeField]
    float basicRR; // phần trăm kháng sát thương tầm XA cơ bản
    [SerializeField]
    float basicRL; // phản x sát thương khi bị trúng đòn cơ bản

    [SerializeField]
    float basicChanceKill; // tỷ lệ giết mod
    [SerializeField]
    bool basicDealingPoison; // gay doc len tat ca quai
    [SerializeField]
    bool basicDealingFreeze; // gay doc len tat ca quai
    [SerializeField]
    float basicDamageResistance; // tỷ lệ kháng damage
    [SerializeField]
    float basicRangeResistance; // tỷ lệ kháng damage tầm xa
    [SerializeField]
    float basicMeleeResistance; // tỷ lệ kháng damage cận
    [SerializeField]
    float basicMeleeAtkDamage; // sát thương với quái cận  
    [SerializeField]
    float basicRangeAtkDamage; // sát thương với quái tầm xa  
    [SerializeField]
    float basicGroundAtkDamage; // sát thương với quái cận  
    [SerializeField]
    float basicIncreaseHPFromHealt; // tắng HP từ Tim
    [SerializeField]
    float basicIncreaseHPFromUpLevel; // tắng HP từ Tim
    [SerializeField]
    float basicBounusExp; // tắng HP từ Tim
    [SerializeField]
    bool basicGlory;
    [SerializeField]
    float basicEnhance;
   [SerializeField]
    float dameEffect;
    [SerializeField]
    float basicExtraLife;
    #region Properties
    public float BasicHP { get => basicHP; set => basicHP = value; }
    public float BasicATKDamage { get => basicATKDamage; set => basicATKDamage = value; }
    public float BasicMS { get => basicMS; set => basicMS = value; }
    public float BasicAS { get => basicAS; set => basicAS = value; }
    public float BasicCR { get => basicCR; set => basicCR = value; }
    public float BasicCD { get => basicCD; set => basicCD = value; }
    public float BasicDodge { get => basicDodge; set => basicDodge = value; }
    public float BasicMR { get => basicMR; set => basicMR = value; }
    public float BasicRR { get => basicRR; set => basicRR = value; }
    public float BasicRL { get => basicRL; set => basicRL = value; }
    public string Name { get => name; set => name = value; }
    public float BasicChanceKill { get => basicChanceKill; set => basicChanceKill = value; }
    public bool BasicDealingPoison { get => basicDealingPoison; set => basicDealingPoison = value; }
    public bool BasicDealingFreeze { get => basicDealingFreeze; set => basicDealingFreeze = value; }
    public float BasicDamageResistance { get => basicDamageResistance; set => basicDamageResistance = value; }
    public float BasicRangeResistance { get => basicRangeResistance; set => basicRangeResistance = value; }
    public float BasicMeleeResistance { get => basicMeleeResistance; set => basicMeleeResistance = value; }
    public float BasicMeleeAtkDamage { get => basicMeleeAtkDamage; set => basicMeleeAtkDamage = value; }
    public float BasicRangeAtkDamage { get => basicRangeAtkDamage; set => basicRangeAtkDamage = value; }
    public float BasicGroundAtkDamage { get => basicGroundAtkDamage; set => basicGroundAtkDamage = value; }
    public float BasicIncreaseHPFromHealt { get => basicIncreaseHPFromHealt; set => basicIncreaseHPFromHealt = value; }
    public float BasicBounusExp { get => basicBounusExp; set => basicBounusExp = value; }
    public float BasicIncreaseHPFromUpLevel { get => basicIncreaseHPFromUpLevel; set => basicIncreaseHPFromUpLevel = value; }
    public bool BasicGlory { get => basicGlory; set => basicGlory = value; }
    public float DameEffect { get => dameEffect; set => dameEffect = value; }
    public float BasicEnhance { get => basicEnhance; set => basicEnhance = value; }
    public float BacsicExtraLife { get => basicExtraLife; set => basicExtraLife = value; }
    //public bool BasicEnhance { get => basicGlory; set => basicGlory = value; }

    #endregion
    public void Reset()
    {
        BasicHP = 0;
        BasicATKDamage = 0;
        BasicMS = 0;
        BasicAS = 0;
        BasicCR = 0;
        BasicCD = 0;
        BasicDodge = 0;
        BasicMR = 0;
        BasicRR= 0;
        BasicRL = 0;
        Name = "";
        BasicChanceKill = 0;
        BasicDealingPoison =false;
        BasicDealingFreeze = false;
        BasicDamageResistance = 0;
        BasicRangeResistance = 0;
        BasicMeleeResistance = 0;
        BasicMeleeAtkDamage = 0;
        BasicRangeAtkDamage = 0;
        BasicGroundAtkDamage = 0;
        BasicIncreaseHPFromHealt = 0;
        BasicBounusExp = 0;
        BasicIncreaseHPFromUpLevel = 0;
        BasicGlory = false;
        BasicEnhance = 0;
        DameEffect = 0;
        BacsicExtraLife = 0;
    }
}
