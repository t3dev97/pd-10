﻿using UnityEngine;

[CreateAssetMenu(fileName = "Monster New", menuName = "Monster Asset")]
[System.Serializable]
public class MonsterAsset : ScriptableObject
{
    [Header("Main")]
    [SerializeField]
    string nameMonster;

    [SerializeField]
    int id;

    [SerializeField]
    float basicHP;

    [Header("ATK")]
    [SerializeField]
    float basicATKDamge;

    [Header("Speeds")]
    [SerializeField]
    float basicMS; // tốc độ di chuyển

    [SerializeField]
    float basicASMeeleUnit;// tốc độ ra đòn

    [SerializeField]
    float basicASRangeUnit;// tốc đạn bay

    [Header("Cool Down Atk")]
    [SerializeField]
    float basicCoolDownMin = 0.0f; // thời gian cần để thực hiện lần đánh tiếp theo
    [SerializeField]
    float basicCoolDownMax; // thời gian cần để thực hiện lần đánh tiếp theo

    [Header("Monster Type")]
    [SerializeField]
    MONSTER_RACE_TYPE raceType;
    [SerializeField]
    MONSTER_ATK_TYPE atkType;

    [Header("Heart")]
    [SerializeField]
    float heartDropRate; // ti lệ rơi
    [SerializeField]
    float heartNumberMin; // số lượng min 
    [SerializeField]
    float heartNumberMax; // số lượng max

    [Header("Gold")]
    [SerializeField]
    float goldNumbertMin;
    [SerializeField]
    float goldNumberMax;

    [Header("OffSet Selection Position")]
    [SerializeField]
    Vector3 offSetSelectionTop = new Vector3(0, 3.5f, 0);
    [SerializeField]
    Vector3 offSetSelectionScaleFoot = new Vector3(1.5f, 1.5f, 1.0f);
    [SerializeField]
    Vector3 offSetSelectionFx = new Vector3(0f, 0.75f, 0f);
    public MonsterAsset() { }
    public MonsterAsset(string _name, int _id, float _hp, float _atkDamame, float _moveSpeed, float _asMeele, float _asRange, float _coolDownMin, float _coolDownMax, MONSTER_RACE_TYPE _raceType, MONSTER_ATK_TYPE _atkType, float _heartDrop, float _heartCountMin, float _heartCountMax, float _goldCountMin, float _goldCountMax, Vector3 _offsetSelectionTop, Vector3 _offsetSelectionScaleFoot,Vector3 _offsetSelectionFx)
    {
        NameMonster = _name;
        Id = _id;
        BasicHP = _hp;
        BasicATKDamge = _atkDamame;
        BasicMS = _moveSpeed;
        BasicASMeeleUnit = _asMeele;
        BasicASRangeUnit = _asRange;
        BasicCoolDownMax = _coolDownMax;
        BasicCoolDownMin = _coolDownMin;
        AtkType = _atkType;
        RaceType = _raceType;
        HeartDropRate = _heartDrop;
        HeartNumberMin = _heartCountMin;
        HeartNumberMax = _heartCountMax;
        GoldNumberMin = _goldCountMin;
        GoldNumberMax = _goldCountMax;
        OffSetSelectionTop = _offsetSelectionTop;
        OffSetSelectionScaleFoot = _offsetSelectionScaleFoot;
        offSetSelectionFx = _offsetSelectionFx;
    }


    public int Id { get => id; set => id = value; }
    public float BasicHP { get => basicHP; set => basicHP = value; }
    public float BasicATKDamge { get => basicATKDamge; set => basicATKDamge = value; }
    public float BasicMS { get => basicMS; set => basicMS = value; }
    public float BasicASMeeleUnit { get => basicASMeeleUnit; set => basicASMeeleUnit = value; }
    public float BasicASRangeUnit { get => basicASRangeUnit; set => basicASRangeUnit = value; }
    public float BasicCoolDownMax { get => basicCoolDownMax; set => basicCoolDownMax = value; }
    public float HeartDropRate { get => heartDropRate; set => heartDropRate = value; }
    public string NameMonster { get => nameMonster; set => nameMonster = value; }
    public float BasicCoolDownMin { get => basicCoolDownMin; set => basicCoolDownMin = value; }
    public MONSTER_ATK_TYPE AtkType { get => atkType; set => atkType = value; }
    public MONSTER_RACE_TYPE RaceType { get => raceType; set => raceType = value; }
    public float GoldNumberMin { get => goldNumbertMin; set => goldNumbertMin = value; }
    public float GoldNumberMax { get => goldNumberMax; set => goldNumberMax = value; }
    public Vector3 OffSetSelectionTop { get => offSetSelectionTop; set => offSetSelectionTop = value; }
    public Vector3 OffSetSelectionScaleFoot { get => offSetSelectionScaleFoot; set => offSetSelectionScaleFoot = value; }
    public Vector3 OffSetSelectionFX { get => offSetSelectionFx; set => offSetSelectionFx = value; }
    public float HeartNumberMin { get => heartNumberMin; set => heartNumberMin = value; }
    public float HeartNumberMax { get => heartNumberMax; set => heartNumberMax = value; }


}
