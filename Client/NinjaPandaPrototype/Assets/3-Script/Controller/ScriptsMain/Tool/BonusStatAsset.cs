﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BonusStatAsset", menuName = "Bonus Stat Asset")]
public class BonusStatAsset : ScriptableObject
{
    public List<RoundBonus> Maps = new List<RoundBonus>(); // chứa các bonus của map

    //public RoundBonus[] Maps { get => maps; set => maps = value; }
}
[System.Serializable]
public class RoundBonus
{
    public List<InfoBonus> Rounds = new List<InfoBonus>(); //  chứa các bonus của round

    //public InfoBonus[] Rounds { get => rounds; set => rounds = value; }
}
[System.Serializable]
public class InfoBonus // chứa thông tin bonus của 1 round trong 1 map
{
    // các thuộc tính được bonus
    //[Range(0, 100)]
    public float BonusHP;
    //[Range(0, 100)]
    public float BonusATK;
    //[Range(0, 100)]
    public float VongQuayMayMan;
    //[Range(0, 100)]
    public float ThuongNhanThanBi;
    //[Range(0, 100)]
    public float ThoaThuanAcQuy;
}

