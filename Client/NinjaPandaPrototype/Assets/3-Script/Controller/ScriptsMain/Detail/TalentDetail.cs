﻿using UnityEngine;

public enum TalentElement
{
    strenght, // tăng max HP
    power, // tăng atkDamage
    recover, // tăng thêm HP khi nhận được tim máu
    agile, // tăng tốc độ bắn 
    inspire, // số HP hồi mõi khi lên cấp
    enhance, // tăng % các chỉ số các trang bị
    time_reward, // tăng nhanh tốc độ lên cấp
    glory // được chọn 1 skill vừa vào chapter
}
public class TalentDetail : MonoBehaviour
{
    static public TalentDetail api;

    float basicStrenght; // tăng max HP
    float basicRecover; // tăng thêm HP khi nhận được tim máu
    float basicAgile; // tăng tốc độ bắn 
    float basicInspire; // số HP hồi mõi khi lên cấp
    float basicEnhance; // tăng % các chỉ số các trang bị
    float basicTimeReward; // tăng nhanh tốc độ lên cấp
    float basicGlory; // được chọn 1 skill vừa vào chapter

    int lvStrenght; // cấp của Strenght
    int lvRecover; // cấp của Recover
    #region Properties
    public float BasicStrenght { get => basicStrenght; set => basicStrenght = value; }
    public float BasicRecover { get => basicRecover; set => basicRecover = value; }
    public float BasicAgile { get => basicAgile; set => basicAgile = value; }
    public float BasicInspire { get => basicInspire; set => basicInspire = value; }
    public float BasicEnhance { get => basicEnhance; set => basicEnhance = value; }
    public float BasicTimeReward { get => basicTimeReward; set => basicTimeReward = value; }
    public float BasicGlory { get => basicGlory; set => basicGlory = value; }
    #endregion

    private void Awake()
    {
        if (api == null)
            api = this;
        else
            Destroy(this);
    }
}
