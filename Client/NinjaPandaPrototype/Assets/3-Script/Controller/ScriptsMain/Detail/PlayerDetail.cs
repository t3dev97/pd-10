﻿using Studio;
using Studio.BC;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerDetail : CharacterDetail
{
    static public PlayerDetail api;

    [Header("Bonus skill")]
    public float dameBonus = 0;
    public float speedBonus = 0;
    public float dogeBonus = 0;
    public float chanceBonus = 0;
    public float rampageBonus = 0;
    [Space(10)]

    [Header("Parameter")]
    [SerializeField]
    private float moveSpeedInStage; // tốc độ di chuyển hiện tại trong stage

    [SerializeField]
    private float damageATKInStage;

    private float tgDamageATKInStage;

    [Tooltip("Tốc độ tấn công/ thời gian giản cách atk")]
    [SerializeField]
    private float atkSpeedInStage; // Tốc độ tấn công/ thời gian giản cách atk

    private float tgAtkSpeedInStage; // Tốc độ tấn công/ thời gian giản cách atk

    [Tooltip("Độ đẩy lùi")]
    [SerializeField]
    private float repelForce = 4; // sau này chuyển về cho bullet

    [SerializeField]
    private float hpInStage; // máu hiện tại

    [SerializeField]
    private float maxHPInStage; // max máu hiện tại
    private float tgMaxHPInStage; // max máu hiện tại

    [SerializeField]
    private Image imgHealtBar; // thanh máu

    [SerializeField]
    private Image imgDamageBar;

    [SerializeField]
    private TextMesh txtCurrenHP;

    [SerializeField]
    private TextMeshProUGUI tmpCurrentHP;

    [SerializeField]
    private int expInStage = 0; // exp trong stage

    [SerializeField]
    private int levelInStage = 1; // level trong stage

    [SerializeField]
    private int goldInStage;

    [SerializeField]
    private int gemInStage;

    [SerializeField]
    private float criticalRate; // phần trăm khả năng gây chí mạng cơ bản trong stage
    private float tgCriticalRate;

    [SerializeField]
    private float criticalDame; // phần trăm sát thương khi chí mạng cơ bản trong stage
    private float tgCriticalDame;

    [SerializeField]
    private float dodge; // phầm trăm khả năng né khi bị trúng đòn cơ bản trong stage
    private float tgDodgeDame;

    [SerializeField]
    private float reductionMeleeDame; // phần trăm kháng sát thương CẬN chiến cơ bản trong stage
    private float tgReductionMeleeDame;

    [SerializeField]
    private float reductionRangerDame; // phần trăm kháng sát thương tầm XA cơ bản trong stage
    private float tgReductionRangerDame;

    [SerializeField]
    private float increasesMeleeDame;
    private float tgIncreasesMeleeDame;

    [SerializeField]
    private float increasesRangerDame;
    private float tgIncreasesRangerDame;

    [SerializeField]
    private float increasesEXP;
    private float tgIncreasesEXP;

    [SerializeField]
    private float increasesHPFormHeal;
    private float tgIncreasesHPToHeal;
    [SerializeField]
    private float increasesDameEffect;
    private float tgIncreasesDameEffect;
    [SerializeField]
    private float increasesExtraLife;
    private float tgIncreasesExtraLife;

    private List<int> listSkill;

    public bool isGetGem = false;
    public bool isLevelUp = false;

    #region Properties
    public float Dodge { get { return dodge; } set { dodge = value; } }
    public float TGDodge { get { return tgDodgeDame; } set { tgDodgeDame = value; } }
    public float ReductionMeleeDame { get { return reductionMeleeDame; } set { reductionMeleeDame = value; } }
    public float TGReductionMeleeDame { get { return tgReductionMeleeDame; } set { tgReductionMeleeDame = value; } }
    public float ReductionRangerDame { get { return reductionRangerDame; } set { reductionRangerDame = value; } }
    public float TGReductionRangerDame { get { return tgReductionRangerDame; } set { tgReductionRangerDame = value; } }
    public float IncreasesMeleeDame { get { return increasesMeleeDame; } set { increasesMeleeDame = value; } }
    public float TGIncreasesMeleeDame { get { return tgIncreasesMeleeDame; } set { tgIncreasesMeleeDame = value; } }
    public float IncreasesRangerDame { get { return increasesRangerDame; } set { increasesRangerDame = value; } }
    public float TGIncreasesRangerDame { get { return tgIncreasesRangerDame; } set { tgIncreasesRangerDame = value; } }
    public float IncreasesEXP { get { return increasesEXP; } set { increasesEXP = value; } }
    public float IncreasesHPFromHeal { get { return increasesHPFormHeal; } set { increasesHPFormHeal = value; } }
    public float IncreasesDameEffect { get { return increasesDameEffect; } set { increasesDameEffect = value; } }
    public float IncreasesExtraLife { get { return increasesExtraLife; } set { increasesExtraLife = value; } }

    public float HP
    {
        get { return hpInStage; }
        set
        {

            hpInStage = value;
            imgHealtBar.fillAmount = hpInStage / MaxHPInStage;

            LeanTween.value(imgDamageBar.fillAmount, imgHealtBar.fillAmount, 0.5f).setOnUpdate((float go) =>
            {
                if (imgDamageBar != null)
                    imgDamageBar.fillAmount = go;
            });

            //txtCurrenHP.text = hpInStage.ToString();
            float hp = (hpInStage > 0) ? hpInStage : 0;
            tmpCurrentHP.SetText(hp.ToString());
            if (PlayerController.api != null && PlayerController.api.playerSkills.skillPassive.dataSkillsPassive != null)
            {
                PlayerController.api.playerSkills.PlayPassive();
            }
            if (hpInStage > MaxHPInStage)
            {
                hpInStage = MaxHPInStage;
            }
        }
    }


    public float SpeedMoveInStage
    {
        get { return moveSpeedInStage; }
        set { moveSpeedInStage = value; }
    }
    public float SpeedAtkInStage
    {
        get { return atkSpeedInStage; }
        set
        {
            atkSpeedInStage = value;
            //if (PlayerController.api != null)
            //    PlayerController.api.UpdateSpeedAtk();
        }
    }
    public float TGSpeedAtkInStage
    {
        get { return tgAtkSpeedInStage; }
        set
        {
            tgAtkSpeedInStage = value;
            //if (PlayerController.api != null)
            //    PlayerController.api.UpdateSpeedAtk();
        }
    }
    public float DamageAtkInStage { get { return damageATKInStage; } set { damageATKInStage = value; } }
    public float TGDamageAtkInStage { get { return tgDamageATKInStage; } set { tgDamageATKInStage = value; } }
    public float RepelForceInStage { get { return repelForce; } set { repelForce = value; } }
    public float CrInStage { get => criticalRate; set => criticalRate = value; }
    public float TGCrInStage { get => tgCriticalRate; set => tgCriticalRate = value; }
    public float CdInStage { get => criticalDame; set => criticalDame = value; }
    public float TGCdInStage { get => criticalDame; set => criticalDame = value; }
    public float DodgeInStage { get => dodge; set => dodge = value; }
    public float MrInStage { get => reductionMeleeDame; set => reductionMeleeDame = value; }
    public float RrInStage { get => reductionRangerDame; set => reductionRangerDame = value; }
    public float MaxHPInStage { get => maxHPInStage; set => maxHPInStage = value; }
    public float TGMaxHPInStage { get => tgMaxHPInStage; set => tgMaxHPInStage = value; }
    public int ExpInStage
    {
        get => expInStage;
        set
        {
            expInStage = value;
        }
    }

    public void UpdateExpInStage()
    {
        float expInMap = UnityEngine.Random.Range(BCCache.Api.DataConfig.StageInfos[BC_DataGame.api.currentIDMap].ExpMin, BCCache.Api.DataConfig.StageInfos[BC_DataGame.api.currentIDMap].ExpMax);
        expInMap += (expInMap * (IncreasesEXP / 100));
        if (PlayerPrefs.GetInt("chap") > PlayerPrefs.GetInt("chapHight"))
            PlayerPrefs.SetInt("chapHight", PlayerPrefs.GetInt("chap"));

        if (PlayerPrefs.GetInt("round") > PlayerPrefs.GetInt("roundHight"))
            PlayerPrefs.SetInt("roundHight", PlayerPrefs.GetInt("round"));

        UpdateSlider(expInMap);
    }
    public void UpdateSlider(float exp)
    {
        if (levelInStage >= 11)
        {
            //UIBasicController.api.TxtLevelPlayer.SetLocalize(LanguageManager.api.GetKey(BCLocalize.MAIN_LEVEL_MAX));
            //UIBasicController.api.TxtLevelPlayerShow.SetLocalize(LanguageManager.api.GetKey(BCLocalize.MAIN_LEVEL_MAX));
            UIBasicController.api.TxtLevelTMP.SetLocalize(LanguageManager.api.GetKey(BCLocalize.MAIN_LEVEL_MAX));
            return;
        }

        if (UIBasicController.api.SliderLV.maxValue == 0)
            UIBasicController.api.SliderLV.maxValue = BCCache.Api.DataConfig.LevelChapters[PlayerPrefs.GetInt("chap")].level_exps[levelInStage - 1];

        var totalExp = expInStage + exp;
        UIBasicController.api.SliderLV.maxValue = BCCache.Api.DataConfig.LevelChapters[PlayerPrefs.GetInt("chap")].level_exps[levelInStage - 1];
        float tempExp = expInStage;

        float targetExp = totalExp;
        if (totalExp > UIBasicController.api.SliderLV.maxValue)
            targetExp = UIBasicController.api.SliderLV.maxValue;

        LeanTween.value(expInStage, targetExp, 0.5f).setOnUpdate((float value) =>
        {
            isGetGem = true;
            tempExp = value;
            UIBasicController.api.SliderLV.value = tempExp;

            if (tempExp >= UIBasicController.api.SliderLV.maxValue && levelInStage < 10)
            {
                levelInStage++;

                BCCache.Api.GetEffectPrefabAsync(EffectKey.LevelUp, (effect) =>
                {
                    if (PlayerDetail.api != null)
                    {
                        GameObject obj = Instantiate(effect, PlayerDetail.api.transform);
                        effect.transform.localPosition = Vector3.zero;
                        obj.GetComponent<ParticleSystem>().enableEmission = true;
                        Destroy(obj, 1.0f);
                    }
                });
                isLevelUp = true;

                UIBasicController.api.TxtLevelTMP.SetLocalize(LanguageManager.api.GetKey(BCLocalize.UIBASIC_LEVEL_PLAYER), new string[] { levelInStage.ToString() });

                var lastLevelExp = 0;
                if (levelInStage > 1)
                    lastLevelExp = BCCache.Api.DataConfig.LevelChapters[PlayerPrefs.GetInt("chap")].level_exps[levelInStage - 2];

                expInStage = 0;

                UIBasicController.api.SliderLV.maxValue = BCCache.Api.DataConfig.LevelChapters[PlayerPrefs.GetInt("chap")].level_exps[levelInStage - 1] - lastLevelExp;
                UpdateSlider(totalExp - UIBasicController.api.SliderLV.maxValue);
                StartCoroutine(ShowLevelUp());
                return;
            }
            //else
            //{
            //    UIBasicController.api.TxtLevelTMP.SetLocalize(LanguageManager.api.GetKey(BCLocalize.MAIN_LEVEL_MAX), new string[] { levelInStage.ToString() });
            //}

        }).setOnComplete(() =>
        {
            expInStage = (int)(tempExp);
            isGetGem = false;
        });
    }

    IEnumerator ShowLevelUp()
    {
        yield return new WaitForSeconds(1.0f);
        BCPopupManager.api.Show(PopupName.PopupLevelUp);
    }
    public int Level { get => levelInStage; set => levelInStage = value; }
    public int GoldInstage
    {
        get => goldInStage;
        set
        {
            goldInStage = value;
            if (UIBasicController.api != null)
            {
                //UIBasicController.api.TxtGold.text = goldInStage.ToString();
                UIBasicController.api.TmpGold.text = goldInStage.ToString();
            }
        }
    }
    public List<int> ListSkill { get => listSkill; set => listSkill = value; }
    public int GemInStage { get => gemInStage; set => gemInStage = value; }
    #endregion
    private void Awake()
    {
        string[] str = gameObject.name.ToString().Split('(');
        string name = str[0];
        base.SetupData(name);
        if (api == null)
            api = this;
        else
            Destroy(this);

        //tmpCurrentHP = transform.Find("tmpCurrentHP").GetComponent<TextMeshPro>();

        Init();
    }
    private void Start()
    {
        listSkill = new List<int>();
        if (!BCApiTest.api.isGoToTest)
        {
            StartCoroutine(ShowLevelUp());
        }

    }
    void Init() // load các thuộc tính khi vào Battle
    {
        base.setData(characterAsset);

        Name = characterAsset.Name;
        MaxHPInStage = characterAsset.BasicHP + EquipmentAsset.BasicHP + TalentAsset.BasicHP;
        TGMaxHPInStage = MaxHPInStage;
        HP = MaxHPInStage;

        DamageAtkInStage = characterAsset.BasicATKDamage + EquipmentAsset.BasicATKDamage + TalentAsset.BasicATKDamage;
        TGDamageAtkInStage = DamageAtkInStage;
        SpeedMoveInStage = characterAsset.BasicMS + EquipmentAsset.BasicMS + TalentAsset.BasicMS;

        SpeedAtkInStage = characterAsset.BasicAS + EquipmentAsset.BasicAS + TalentAsset.BasicAS;
        TGSpeedAtkInStage = SpeedAtkInStage;
        CrInStage = characterAsset.BasicCR + EquipmentAsset.BasicCR + TalentAsset.BasicCR;
        TGCrInStage = CrInStage;

        CdInStage = characterAsset.BasicCD + EquipmentAsset.BasicCD + TalentAsset.BasicCD;
        TGCdInStage = CdInStage;
        DodgeInStage = characterAsset.BasicDodge + EquipmentAsset.BasicDodge + TalentAsset.BasicDodge;
        TGDodge = DodgeInStage;
        MrInStage = characterAsset.BasicMR + EquipmentAsset.BasicMR + TalentAsset.BasicMR;
        RrInStage = characterAsset.BasicRR + EquipmentAsset.BasicRR + TalentAsset.BasicRR;

        levelInStage = 1;

        IncreasesMeleeDame = characterAsset.BasicMeleeAtkDamage + EquipmentAsset.BasicMeleeAtkDamage + TalentAsset.BasicMeleeAtkDamage;
        IncreasesRangerDame = characterAsset.BasicRangeAtkDamage + EquipmentAsset.BasicRangeAtkDamage + TalentAsset.BasicRangeAtkDamage;

        IncreasesEXP = characterAsset.BasicBounusExp + EquipmentAsset.BasicBounusExp + TalentAsset.BasicBounusExp;

        IncreasesHPFromHeal = characterAsset.BasicIncreaseHPFromHealt + EquipmentAsset.BasicMeleeAtkDamage + TalentAsset.BasicMeleeAtkDamage;
        IncreasesDameEffect = characterAsset.DameEffect + EquipmentAsset.DameEffect + TalentAsset.DameEffect;
    }

}
