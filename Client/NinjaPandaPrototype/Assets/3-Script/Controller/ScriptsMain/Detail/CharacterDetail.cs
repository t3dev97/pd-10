﻿using UnityEngine;


// Chứa các thông tin cơ bản của nhân vật: máu, damage,... khi chưa vào Battle
public class CharacterDetail : MonoBehaviour
{
    public CharacterAsset characterAsset; // dùng để load data từ trong asset lên character
    public CharacterAsset EquipmentAsset;
    public CharacterAsset TalentAsset;
    [SerializeField]
    string name = "Character No.1";
    [SerializeField]
    float basicHP; // HP cở bản
    float basicATKDamge; // sát thương cơ bản
    float basicMS; // tốc độ di chuyển cơ bản
    float basicAS; // tốc độ tấn công cơ bản, tính theo cái/giây : càng CAO càng NHANH
    float basicCR; // phần trăm khả năng gây chí mạng cơ bản
    float basicCD; // phần trăm sát thương khi chí mạng cơ bản 
    float basicDodge; // phầm trăm khả năng né khi bị trúng đòn cơ bản 
    float basicMR; // phần trăm kháng sát thương CẬN chiến cơ bản
    float basicRR; // phần trăm kháng sát thương tầm XA cơ bản
    float basicRL; // phản x sát thương khi bị trúng đòn cơ bản
    float basicChanceKill; // tỷ lệ giết mod
    bool basicDealingPoison; // gay doc len tat ca quai
    bool basicDealingFreeze; // gay doc len tat ca quai
    float basicDamageResistance; // tỷ lệ kháng damage
    float basicRangeResistance; // tỷ lệ kháng damage tầm xa
    float basicMeleeResistance; // tỷ lệ kháng damage cận
    float basicMeleeAtkDamage; // sát thương với quái cận  
    float basicRangeAtkDamage; // sát thương với quái tầm xa  
    float basicGroundAtkDamage; // sát thương với quái cận  
    float basicIncreaseHPFromHealt; // tắng HP từ Tim
    float basicIncreaseDameEffect;
    float basicExtraLife;
    //float basicSpeedLevelUp

    #region Properties
    public float BasicHP { get => basicHP; set => basicHP = value; }
    public float BasicATKDamage { get => basicATKDamge; set => basicATKDamge = value; }
    public float BasicMS { get => basicMS; set => basicMS = value; }
    public float BasicAS { get => basicAS; set => basicAS = value; }
    public float BasicCR { get => basicCR; set => basicCR = value; }
    public float BasicCD { get => basicCD; set => basicCD = value; }
    public float BasicDodge { get => basicDodge; set => basicDodge = value; }
    public float BasicMR { get => basicMR; set => basicMR = value; }
    public float BasicRR { get => basicRR; set => basicRR = value; }
    public float BasicRL { get => basicRL; set => basicRL = value; }
    public string Name { get => name; set => name = value; }
    public bool BasicDealingPoison { get => basicDealingPoison; set => basicDealingPoison = value; }
    public float BasicChanceKill { get => basicChanceKill; set => basicChanceKill = value; }
    public bool BasicDealingFreeze { get => basicDealingFreeze; set => basicDealingFreeze = value; }
    public float BasicDamageResistance { get => basicDamageResistance; set => basicDamageResistance = value; }
    public float BasicRangeResistance { get => basicRangeResistance; set => basicRangeResistance = value; }
    public float BasicMeleeResistance { get => basicMeleeResistance; set => basicMeleeResistance = value; }
    public float BasicMeleeAtkDamage { get => basicMeleeAtkDamage; set => basicMeleeAtkDamage = value; }
    public float BasicRangeAtkDamage { get => basicRangeAtkDamage; set => basicRangeAtkDamage = value; }
    public float BasicGroundAtkDamage { get => basicGroundAtkDamage; set => basicGroundAtkDamage = value; }
    public float BasicIncreaseHPFromHealt { get => basicIncreaseHPFromHealt; set => basicIncreaseHPFromHealt = value; }

    #endregion

    public void setData(CharacterAsset asset) // Load data từ trong asset lên 
    {
        basicHP = asset.BasicHP;
        basicATKDamge = asset.BasicATKDamage;
        basicMS = asset.BasicMS;
        basicAS = asset.BasicAS;
        basicCR = asset.BasicCR;
        basicCD = asset.BasicCD;
        basicDodge = asset.BasicDodge;
        basicMR = asset.BasicMR;
        basicRR = asset.BasicRR;
        basicRL = asset.BasicRL;
        basicChanceKill = asset.BasicChanceKill;
        basicDealingPoison = asset.BasicDealingPoison;
        basicDealingFreeze = asset.BasicDealingFreeze;
        basicDamageResistance = asset.BasicDamageResistance;
        basicMeleeResistance = asset.BasicMeleeResistance;
        basicMeleeAtkDamage = asset.BasicMeleeAtkDamage;
        basicRangeAtkDamage = asset.BasicRangeAtkDamage;
        basicGroundAtkDamage = asset.BasicGroundAtkDamage;
        basicIncreaseHPFromHealt = asset.BasicIncreaseHPFromHealt;
        basicExtraLife = asset.BacsicExtraLife;
    }
    string link = "ModifyTool/Player/";
    public void SetupData(string NAME)
    {
        characterAsset = Resources.Load<CharacterAsset>(link + NAME);
        EquipmentAsset = Resources.Load<CharacterAsset>(link + "PLAYEREQUIPMENT");
        TalentAsset = Resources.Load<CharacterAsset>(link + "PLAYERTALENT");
        //Debug.Log("Resources.Load<TextAsset>(Player/Player): " + characterAsset);

    }
}
