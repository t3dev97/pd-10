﻿using Studio.BC;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
// Store all data of monster

public class MonsterDetail : Monster
{



    [Header("Current Stat Value")]
    [SerializeField]
    float _currentHP = 100;

    [SerializeField]
    float _currentSpeedMove = 2; // speed sử dụng trong stage

    [SerializeField]
    float _currentATKDamage = 30;

    [SerializeField]
    float _currentASMeeleUnit = 1.5f;

    [SerializeField]
    float _currentASRangeUnit = 1.5f;

    [SerializeField]
    float _currentCoolDownMax;

    [SerializeField]
    float _currentCoolDownMin;

    [Header("Object Reference")]
    [SerializeField]
    bool _isTrap = false;
    [SerializeField]
    Image imgHealtBar;

    [SerializeField]
    Image imgDamageBar;

    [SerializeField]
    GameObject healtBar;

    [SerializeField]
    HPBar hPBar;

    [SerializeField]
    bool isBoss;


    NavMeshAgent agent; // dùng thuật toán AI Unity
    float rangeATK = 3; // bản cũ
    float speedMoveATK = 4;// tốc độ lao đến/ độ xa bản cũ
    bool doneLoadData = false;

    public EnumEffectDamageType EffectDamageType;
    public bool isHopQua = false;
    public bool isDropItem = true;
    private void Start()
    {
        if (GetComponent<NavMeshAgent>() != null)
            agent = GetComponent<NavMeshAgent>();

        if (GetComponent<BC_ObjectInfo>() != null)
            IsBoss = GetComponent<BC_ObjectInfo>().style == STYLE_BRUSHES.boss;

        if (_isTrap == false)
        {
            if (IsBoss)
            {
                healtBar = UIBasicController.api.HealtBoss;
                UIBasicController.api.SliderHealtBoss.value = BasicHP;
                UIBasicController.api.SliderHealtBoss.maxValue = 1;
                imgDamageBar = UIBasicController.api.HealtBoss.transform.FindChild("DamageBar").GetComponent<Image>();

                if (GetComponentInChildren<HPBar>() != null)
                    GetComponentInChildren<HPBar>().HealtBarObj.gameObject.SetActive(false);
            }
            else
            {
                hPBar = GetComponentInChildren<HPBar>();
                if (hPBar != null)
                {
                    healtBar = hPBar.HealtBarObj;
                    imgHealtBar = hPBar.HealthContrainer;
                    imgDamageBar = hPBar.DamageBar;
                }
            }
        }

        StartCoroutine(DelayLoad());
    }
    IEnumerator DelayLoad()
    {
        yield return new WaitForSeconds(0.1f);
        SetupCurrentStatValue();
    }
    void SetupCurrentStatValue() // thiết lập các thuộc tính từ Monster sang Detail
    {
        try
        {
            BasicHP *= BCCache.Api.DataConfig.StageInfos[BC_Chapter_Info.api.IDMap].HPBouns;
            _currentATKDamage = BasicATKDamge * BCCache.Api.DataConfig.StageInfos[BC_Chapter_Info.api.IDMap].ATKBouns;
        }
        catch (System.Exception)
        {
            _currentATKDamage = BasicATKDamge;
        }

        _currentHP = BasicHP;
        _currentSpeedMove = BasicMS;
        _currentASMeeleUnit = BasicASMeeleUnit;
        _currentASRangeUnit = BasicASRangeUnit;
        _currentCoolDownMax = BasicCoolDownMax;
        _currentCoolDownMin = BasicCoolDownMin;
        if (agent != null)
            agent.speed = _currentSpeedMove;


        doneLoadData = true;
    }
    #region Properties
    public float CurrentHP
    {
        get { return _currentHP; }
        set
        {
            if (doneLoadData)
            {
                _currentHP = value;
                if (IsBoss)
                {
                    if (_currentHP <= 0)
                        healtBar.gameObject.SetActive(false);

                    UIBasicController.api.SliderHealtBoss.value = _currentHP / BasicHP;
                    StartCoroutine(UpdateDamageImg_Boss(imgDamageBar.fillAmount, UIBasicController.api.SliderHealtBoss.value));
                }
                else
                {
                    if (_currentHP <= 0)
                        healtBar.gameObject.SetActive(false);

                    ImgHealtBar.fillAmount = _currentHP / BasicHP;
                    StartCoroutine(UpdateDamageImg(imgDamageBar.fillAmount, ImgHealtBar.fillAmount));
                }
            }
            else
            {
                Debug.Log("Miss");
            }
        }
    }
    IEnumerator UpdateDamageImg(float currentValue, float tagerValue)
    {
        LeanTween.value(currentValue, tagerValue, 0.5f).setOnUpdate((float value) =>
        {
            imgDamageBar.fillAmount = value;
        });

        imgDamageBar.fillAmount = tagerValue;
        yield return null;
    }
    IEnumerator UpdateDamageImg_Boss(float currentValue, float tagerValue)
    {
        LeanTween.value(currentValue, tagerValue, 0.5f).setOnUpdate((float value) =>
        {
            imgDamageBar.fillAmount = value;
        });

        imgDamageBar.fillAmount = tagerValue;
        yield return null;
    }
    public float CurrentSpeedMove
    {
        get { return _currentSpeedMove; }
        set
        {
            _currentSpeedMove = value;
            if (agent != null)
                agent.speed = value;
        }
    }
    public float CurrentASMeeleUnit
    {
        get { return _currentASMeeleUnit; }
        set { _currentASMeeleUnit = value; }
    }
    public float CurrentATKDamage
    {
        get { return _currentATKDamage; }
        set { _currentATKDamage = value; }
    }
    public GameObject HealtBar { get => healtBar; set => healtBar = value; }
    public float CurrentASRangeUnit { get => _currentASRangeUnit; set => _currentASRangeUnit = value; }
    public float CurrentCoolDownMax { get => _currentCoolDownMax; set => _currentCoolDownMax = value; }
    public float CurrentCoolDownMin { get => _currentCoolDownMin; set => _currentCoolDownMin = value; }
    #endregion
    public float RangeAtk
    {
        get { return rangeATK; }
        set { rangeATK = value; }
    }
    public float SpeedMoveAtk
    {
        get { return speedMoveATK; }
        set { speedMoveATK = value; }
    }

    public Image ImgHealtBar { get => imgHealtBar; set => imgHealtBar = value; }
    public bool IsBoss { get => isBoss; set => isBoss = value; }
}
