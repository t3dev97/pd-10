﻿using Studio.BC;
using System.Collections;
using UnityEngine;

public class BulletDetail : MonoBehaviour
{
    public MONSTER_RACE_TYPE GetMonsterType;
    public Transform PosTarget;
    public float SpeedMove = 1000;
    public float FoceBack = 1.5f;
    public float Damage;
    public EnumBulletType BulletType = EnumBulletType.Thuong;
    public bool ShootDown = false;// Ban huong xuong nhan vat 
    Vector3 _offsetDirBullet = Vector3.zero;
    public bool AllowMove = false;
    public GameObject MyMonster;

    [Header("Normal")]
    public GameObject EffectThuong; // effect destroy

    // Đạn nổ
    [Header("No")]
    public float ThoiGianNo;// là thời gian delay no, thời gian duy trùy sát thương
    public float ThoiGianGaySatThuong = 0;// là thời gian delay no, thời gian duy trùy sát thương
    public float RangeDamage = 5;
    public bool FollowPlayer;
    public GameObject SubBullet;
    public GameObject EffectNo; // effect destroy
    public bool IsShake = true;

    [Header("Set Auto Explode")]
    public bool ExplodeAffter = false;
    public float TimeDelay = 2;
    public GameObject EffectRangeExplode;

    // Đạn phân chia
    [Header("Phân chia")]
    public int NumberBullet;
    public float Angle;
    public float OffsetAngle;

    Transform _container;

    private void Awake()
    {
        if (EffectNo == null)
            EffectNo = Resources.Load("Effects/FloatingDamage", typeof(GameObject)) as GameObject;
    }
    private void Start()
    {
        if (GameObject.FindGameObjectWithTag(TagManager.api.Player) != null)
            PosTarget = GameObject.FindGameObjectWithTag(TagManager.api.Player).transform;

        if (ExplodeAffter)
            StartCoroutine(No(TimeDelay));


        _container = BCCache.Api.transform;
    }
    public void SetUpBullet(float damage, MONSTER_RACE_TYPE monsterType, bool ShootDown = false)
    {
        Damage = damage;
        //Debug.Log(Damage);
        GetMonsterType = monsterType;
        this.ShootDown = ShootDown;
    }
    IEnumerator Delay()
    {
        GetComponent<SphereCollider>().enabled = false;
        yield return new WaitForSeconds(.01f);
        GetComponent<SphereCollider>().enabled = true;
    }
    private void OnTriggerEnter(Collider obj)
    {
        string sTag = obj.transform.tag;
        switch (BulletType)
        {
            case EnumBulletType.Thuong:
                {
                    if (sTag == TagManager.api.Player)
                    {
                        obj.GetComponent<PlayerController>().Hit(Damage);
                    }
                    else
                    {
                        //Instantiate(EffectThuong, transform.position, Quaternion.identity, _container);
                        BCCache.Api.GetEffectPrefabAsync(EffectThuong.name, (effect) =>
                        {
                            GameObject obj_ = Instantiate(effect, transform.position, Quaternion.Euler(transform.eulerAngles), BCCache.Api.transform);
                            if (obj_.GetComponent<ClearObj>() != null)
                            {
                                obj_.GetComponent<ClearObj>().TimeCoolDown = .3f;
                                obj_.GetComponent<ClearObj>().Auto = true;
                            }
                            else
                            {
                                obj_.gameObject.AddComponent<ClearObj>();
                                obj_.GetComponent<ClearObj>().TimeCoolDown = .3f;
                                obj_.GetComponent<ClearObj>().Auto = true;
                            }

                            obj_.transform.gameObject.SetActive(true);
                            obj_.transform.GetChild(0).GetComponent<ParticleSystem>().enableEmission = true;
                        });
                    }
                    Destroy(gameObject,.1f);
                    break;
                }
            case EnumBulletType.PhanChia:
                {
                    ChiaDan(transform, false, NumberBullet, Angle, OffsetAngle);

                    var effectno = Instantiate(EffectNo, transform.position, Quaternion.identity, _container);
                    if (effectno.GetComponent<TextMesh>() != null)
                        effectno.GetComponent<TextMesh>().text = "";

                    Destroy(gameObject);
                    break;
                }
        }
    }
    private void OnCollisionEnter(Collision obj)
    {
        string sTag = obj.transform.tag;
        if (sTag.Equals("Shield"))
        {
            Destroy(gameObject);
            return;
        }

        switch (BulletType)
        {
            case EnumBulletType.Thuong:
                {
                    if (sTag == TagManager.api.Player)
                    {
                        obj.transform.GetComponent<PlayerController>().Hit(Damage, GetMonsterType, MONSTER_ATK_TYPE.range);
                        StartCoroutine(obj.transform.GetComponent<PlayerController>().PushBack(transform.position, obj.transform));
                    }
                    else
                    {
                        //Instantiate(EffectThuong, transform.position, Quaternion.identity, _container);
                        string nameEffect = (EffectThuong == null) ? "EffectThuong" : EffectThuong.name;
                        BCCache.Api.GetEffectPrefabAsync(nameEffect, (effect) =>
                        {
                            GameObject obj_ = Instantiate(effect, transform.position, Quaternion.Euler(transform.eulerAngles), BCCache.Api.transform);
                            if (obj_.GetComponent<ClearObj>() != null)
                            {
                                obj_.GetComponent<ClearObj>().TimeCoolDown = .3f;
                                obj_.GetComponent<ClearObj>().Auto = true;
                            }
                            else
                            {
                                obj_.gameObject.AddComponent<ClearObj>();
                                obj_.GetComponent<ClearObj>().TimeCoolDown = .3f;
                                obj_.GetComponent<ClearObj>().Auto = true;
                            }

                            obj_.transform.gameObject.SetActive(true);
                            obj_.transform.GetChild(0).GetComponent<ParticleSystem>().enableEmission = true;
                        });
                    }
                    Destroy(gameObject);
                    break;
                }
            case EnumBulletType.No:
                {
                    if (sTag == TagManager.api.Player)
                    {
                        obj.transform.GetComponent<PlayerController>().Hit(Damage, GetMonsterType, MONSTER_ATK_TYPE.range);
                        StartCoroutine(No(0)); // nỗ ngay
                    }
                    else
                    {
                        if (GetComponent<SphereCollider>() != null)
                            GetComponent<SphereCollider>().isTrigger = false;
                        StartCoroutine(No(ThoiGianNo));// chờ 1 thời gian mới nổ
                    }

                    break;
                }
            case EnumBulletType.PhanChia:
                {
                    ChiaDan(transform, false, NumberBullet, Angle, OffsetAngle);

                    var effectno = Instantiate(EffectNo, transform.position, Quaternion.identity, _container);
                    if (effectno.GetComponent<TextMesh>() != null)
                        effectno.GetComponent<TextMesh>().text = "";

                    Destroy(gameObject);
                    break;
                }
        }
    }
    IEnumerator No(float delayTime)
    {
        yield return new WaitForSeconds(delayTime);
        GameObject pl = GameObject.FindGameObjectWithTag(TagManager.api.Player);
        if (pl != null)
        {
            if (Vector3.Distance(transform.position, pl.transform.position) < RangeDamage)
            {
                pl.GetComponent<PlayerController>().Hit(Damage, GetMonsterType, MONSTER_ATK_TYPE.range);
                StartCoroutine(pl.transform.GetComponent<PlayerController>().PushBack(transform.position, pl.transform));
            }

            var effectno = Instantiate(EffectNo, transform.position, Quaternion.identity, _container);
            if (effectno != null && effectno.GetComponent<TextMesh>() != null)
                effectno.GetComponent<TextMesh>().text = "";

            if (EffectRangeExplode != null)
            {
                GameObject Explode = Instantiate(EffectRangeExplode, transform.position, Quaternion.identity, _container);
                Explode.GetComponent<PoisonController>().SetUp(RangeDamage, Damage, ThoiGianNo);
            }
        }
#if EDITOR
        if (CameraEffect.api != null && IsShake)
            CameraEffect.api.ShouldShake = true;
#endif
        Destroy(gameObject);
    }

    void ChiaDan(Transform originBulletTrans, bool Player = false, int numberBullet = 4, float _angle = 360, float offset = 0) // thêm 2 tia phía trước
    {
        GameObject temp;
        Vector3 pos = transform.position + Vector3.up; // vị trí sinh đạn
        if (Player)
            pos -= Vector3.up * 2;
        float angle = _angle / numberBullet;
        float angleYLeft = originBulletTrans.rotation.eulerAngles.y + offset;
        float angleYRight = originBulletTrans.rotation.eulerAngles.y + offset;

        float damage = Damage/* / numberBullet*/;

        // Viên đạn đầu tiên
        temp = Instantiate(SubBullet, pos, transform.rotation); // bắn đạn
        temp.transform.eulerAngles = new Vector3(0, angleYLeft, 0);
        temp.GetComponent<BulletDetail>().Damage = damage;

        for (int i = 0; i < numberBullet / 2; i++)
        {
            temp = Instantiate(SubBullet, pos, transform.rotation); // bắn đạn
            temp.transform.eulerAngles = new Vector3(0, angleYLeft += angle, 0);
            temp.GetComponent<BulletDetail>().Damage = damage;

            temp = Instantiate(SubBullet, pos, transform.rotation); // bắn đạn
            temp.transform.eulerAngles = new Vector3(0, angleYRight -= angle, 0);
            temp.GetComponent<BulletDetail>().Damage = damage;
        }
    }

}
