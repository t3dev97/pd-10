﻿using UnityEngine;
using UnityEngine.UI;

public class HPBar : MonoBehaviour
{
    public GameObject HealtBarObj;
    public Image HealthContrainer;
    public Image DamageBar;

    private void Awake()
    {
        if (HealtBarObj == null)
            HealtBarObj = gameObject;

        if (HealthContrainer == null)
            HealthContrainer = transform.FindChild("HealthContainer").GetComponent<Image>();

        if (transform.FindChild("Damage") != null)
            DamageBar = transform.FindChild("Damage").GetComponent<Image>();
    }
    void LateUpdate()
    {
        transform.rotation = Quaternion.identity;
    }
}
