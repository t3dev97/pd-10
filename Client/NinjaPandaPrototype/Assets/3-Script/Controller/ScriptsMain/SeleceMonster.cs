﻿using UnityEngine;

// quản lý Monster 
public class SeleceMonster : MonoBehaviour
{
    static public SeleceMonster api;
    public Vector3 Offset = new Vector3(0, 2.0f, 0);

    public GameObject Top;
    public GameObject Foot;


    GameObject target;

    public GameObject Target { get => target; set => target = value; } // thoa dieu kien chinh de di chuyen theo
    public GameObject tg;
    private void Awake()
    {
        if (api == null)
            api = this;
        else
            Destroy(api);
    }

    private void LateUpdate()
    {
        if (Target != null)
        {
            tg = target;
            if (tg.transform.tag == TagManager.api.MonsterBurrow || (tg.GetComponent<MonsterDetail>() != null && tg.GetComponent<MonsterDetail>().CurrentHP <= 0))
            {
                StartCoroutine(FindMonster.api.Find());
                return;
            }
            try
            {
                Top.transform.position = tg.transform.position + tg.GetComponent<MonsterDetail>().OffSetSelectionTop;
            }
            catch (System.Exception)
            {
                //Debug.Log(tg.name + tg.GetComponent<MonsterDetail>() == null);
                return;
                throw;
            }

            Vector3 temp = tg.transform.position;
            temp.y = -0.5f;
            Foot.transform.position = temp;
            Top.gameObject.SetActive(true);
            Foot.transform.localScale = tg.GetComponent<MonsterDetail>().OffSetSelectionScaleFoot;
            Foot.gameObject.SetActive(true);
        }
        else
        {
            Top.gameObject.SetActive(false);
            Foot.gameObject.SetActive(false);
        }
    }
}
