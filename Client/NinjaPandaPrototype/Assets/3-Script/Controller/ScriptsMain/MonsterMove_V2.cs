﻿using System.Collections;
using UnityEngine;
using UnityEngine.AI;
public class MonsterMove_V2 : MonoBehaviour
{
    public EnumMoveSkill MoveType;

    NavMeshAgent _agent;
    Transform _player;
    MonsterController _myControl;
    MonsterDetail _myDetail;
    Animator _anim;
    Rigidbody _rb;
    private void Awake()
    {
        _anim = GetComponent<Animator>();
        _agent = GetComponent<NavMeshAgent>();
        _myControl = GetComponent<MonsterController>();
        _myDetail = GetComponent<MonsterDetail>();
        _rb = GetComponent<Rigidbody>();
    }
    private void Start()
    {
        _player = GameObject.FindGameObjectWithTag(TagManager.api.Player).transform;
    }

    private void Update()
    {
        if (_myControl.CheckMoveSkill == 0) return;

        switch (MoveType)
        {
            case EnumMoveSkill.DonTho:
                {
                    break;
                }
            case EnumMoveSkill.LuotNhanh:
                {
                    Debug.Log("1");
                    StartCoroutine(LuotNhanh());
                    break;
                }
            case EnumMoveSkill.XuyenKhong:
                {
                    break;
                }
        }
    }
    IEnumerator LuotNhanh()
    {
        _myControl.CheckMoveBasic = false;
        _myControl.FollowPlayer = false;
        RunAnimation("Atk");
        for (int i = 0; i < 10; ++i)
        {
            _rb.velocity = transform.forward * 5 * _myDetail.CurrentSpeedMove;
            yield return new WaitForEndOfFrame();
        }
        yield return new WaitForSeconds(0.5f);
        _myControl.CheckMoveSkill = 0;
        _myControl.CheckAtk = true;
        yield return new WaitForSeconds(1f);
        _myControl.FollowPlayer = true;
    }
    void RunAnimation(string name)
    {
        foreach (var item in _nameAnim)
        {
            _anim.SetBool(item, false);
        }
        _anim.SetBool(name, true);
    }
    string[] _nameAnim = new string[] { "Idle", "Run", "Atk", "Hit", "Die" }; // các trạng thái aniamtino nhân vật 

}
