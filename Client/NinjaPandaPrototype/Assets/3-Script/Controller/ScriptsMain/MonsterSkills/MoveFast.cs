﻿using Studio.BC;
using System;
using System.Collections;
using UnityEngine;

public class MoveFast : SkillBase
{
    //[SerializeField]
    bool _randomDir = true;

    //[SerializeField]
    float _timeReRandomMin;

    //[SerializeField]
    float _timeReRandomMax;

    MonsterSkillController _skillController;
    MonsterController_new _myController;
    MonsterDetail _myDetail;
    Transform _container;
    GameObject _player;
    Rigidbody _rb;
    Vector3 _posRandom;

    [SerializeField]
    float _speedMove;

    private void Awake()
    {
        if (GetComponent<MonsterSkillController>() == null)
            gameObject.AddComponent<MonsterSkillController>();

        _skillController = GetComponent<MonsterSkillController>();

        if (GetComponent<Rigidbody>() != null)
            _rb = GetComponent<Rigidbody>();

        _myDetail = GetComponent<MonsterDetail>();
        _myController = GetComponent<MonsterController_new>();
    }
    private void Start()
    {
        _player = GameObject.FindGameObjectWithTag(TagManager.api.Player);
        _container = GameObject.FindObjectOfType<BCCache>().transform;
        TimeCooldown = 0;
        AddFreeSkillToSkillController();
    }
    public override IEnumerator SkillStart()
    {
        if (_rb != null)
            _rb.velocity = Vector3.zero;
        _skillController.Skilling = true;
        _skillTimeLive = 0;
        _isDoneSkill = false;
        _isReady = false;

        StartCoroutine(RandomDir()); // random dir 

        transform.LookAt(_posRandom);
        if (!string.IsNullOrEmpty(StartAnimStr))
            _skillController.RunAnimation(StartAnimStr);

        yield return new WaitForSeconds(TimeOfStartAnim);
        StartCoroutine(SkillIn());
    }
    protected override IEnumerator SkillIn()
    {
        if (!string.IsNullOrEmpty(InAnimStr))
            _skillController.Amin.Play(InAnimStr);

        MoveToTarget();

        yield return new WaitForSeconds(TimeOfInAnim);
        if (_loop < TotalLoop)
        {
            _loop++;
            StartCoroutine("SkillIn");
        }
        else
            StartCoroutine(SkillEnd());
    }
    protected override IEnumerator SkillEnd()
    {
        if (!string.IsNullOrEmpty(EndAnimStr))
            _skillController.RunAnimation(EndAnimStr);


        yield return new WaitForSeconds(TimeOfEndAnim);
        _skillController.RunAnimation(EnumAnimName.Idle.ToString());
        _isDoneSkill = true;
        _isReady = true;
        _loop = 0;
        _skillController.Skilling = false;
        AddFreeSkillToSkillController();
    }
    void MoveToTarget()
    {
        float dis = Vector3.Distance(gameObject.transform.position, _posRandom);
        float timeMove = dis / _speedMove;
        TimeOfInAnim = timeMove;
        LeanTween.move(gameObject, _posRandom, timeMove).setOnUpdate((float v) =>
        {
            //transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(_posRandom), v);
        });
    }

    IEnumerator RandomDir()
    {
        if (_randomDir)
        {
            if (_myDetail.RaceType == MONSTER_RACE_TYPE.earth)
            {
                if (_myController.InGround && _myController.ObjCheckInGround != null && _myController.ObjCheckInGround.GetComponent<CheckInGround>() != null && _myController.ObjCheckInGround.GetComponent<CheckInGround>().InGround)
                {
                    _posRandom.y = transform.position.y;
                    _posRandom.x = UnityEngine.Random.Range(-BC_DataGame.api.WidthMap * 1.0f / 2 + 1, BC_DataGame.api.WidthMap * 1.0f / 2 - 1);
                    _posRandom.z = UnityEngine.Random.Range(-BC_DataGame.api.HeightMap * 1.0f / 2 + 1, BC_DataGame.api.HeightMap * 1.0f / 2 - 1);
                }
                //yield return new WaitForSeconds(UnityEngine.Random.Range(_timeReRandomMin, _timeReRandomMax));
                //StartCoroutine(RandomDir());
            }
            else // loai di chuyen khac
            {
                _posRandom.y = transform.position.y;
                _posRandom.x = UnityEngine.Random.Range(-BC_DataGame.api.WidthMap * 1.0f / 2 + 1, BC_DataGame.api.WidthMap * 1.0f / 2 - 1);
                _posRandom.z = UnityEngine.Random.Range(-BC_DataGame.api.HeightMap * 1.0f / 2 + 1, BC_DataGame.api.HeightMap * 1.0f / 2 - 1);
            }
        }
        else // follow player
        {

        }
        yield return null;
    }
    private Action _skillStart;
    private void AddFreeSkillToSkillController()
    {
        _skillStart = () =>
        {
            StartCoroutine(SkillStart());
        };
        _skillController.AddFreeSkill(_skillStart, SkillPriority);
    }
}
