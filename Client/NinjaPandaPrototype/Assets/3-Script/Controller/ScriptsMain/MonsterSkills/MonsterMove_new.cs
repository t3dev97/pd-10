﻿using System.Collections;
using UnityEngine;
using UnityEngine.AI;
public class MonsterMove_new : MonoBehaviour
{
    public EnumMoveBasic MoveType;
    public float JumpUp = 100;
    public bool MoveStep = true; // di chuyen theo buoc

    [Tooltip("sử dụng cho Kappa, các loại quái có khả năng nhảy cao(ép lực khi nhảy), nhưng loại quái (Ếch, khỉ nây) khi nhảy không dùng lực nên không cần phải sử dụng ")]
    public bool UseIsRun = false; // sử dụng cho Kappa, các loại quái có khả năng nhảy cao(ép lực khi nhảy), nhưng loại quái (Ếch, khỉ nây) khi nhảy không dùng lực nên không cần phải sử dụng 

    public bool FollowPlayer = true; // huong di chuyen, random hay theo nhan vat
    public float TimeRandomDir = 5;
    public float DistancePlayer = 3;
    public bool UseAI = false;
    public CheckInGround ObjCheckInGround;

    bool inGround = false;
    bool _canMove;
    float _TimeLine; // thời điểm thực hiện di chuyển
    NavMeshAgent _agent;
    Transform _player;
    MonsterController_new _myControl;
    MonsterDetail _myDetail;
    Animator _anim;
    Animation _animation;
    string[] _nameAnim = new string[] { "Idle", "Run", "Atk", "Hit", "Die" }; // các trạng thái aniamtino nhân vật 
    Vector3 _posTarget;
    Rigidbody _rb;
    MonsterSkillController _monsterSkill1;
    Vector3 _dirRandom;
    CapsuleCollider _capsuleCollider;

    private void Awake()
    {
        _capsuleCollider = GetComponent<CapsuleCollider>();
        _anim = GetComponent<Animator>();
        _animation = GetComponent<Animation>();
        _agent = GetComponent<NavMeshAgent>();
        _myControl = GetComponent<MonsterController_new>();
        _myDetail = GetComponent<MonsterDetail>();
        _rb = GetComponent<Rigidbody>();
        _monsterSkill1 = GetComponent<MonsterSkillController>();
        ObjCheckInGround = GetComponentInChildren<CheckInGround>();
    }
    bool _isTriggerDefault;
    private void Start()
    {
        StartCoroutine(SetupData());
    }
    bool doneSetupData = false;
    IEnumerator SetupData()
    {
        yield return new WaitForSeconds(1.0f);
        _isTriggerDefault = _capsuleCollider.isTrigger;

        if (!FollowPlayer && !UseAI)
            StartCoroutine(RandomDir());
        else
            _player = GameObject.FindGameObjectWithTag(TagManager.api.Player).transform;

        doneSetupData = true;
    }
    IEnumerator RandomDir()
    {
        if (MoveType != EnumMoveBasic.Bay)
        {
            if (inGround && ObjCheckInGround != null && ObjCheckInGround.GetComponent<CheckInGround>() != null && ObjCheckInGround.GetComponent<CheckInGround>().InGround)
            {
                _dirRandom.x = Random.Range(-BC_DataGame.api.WidthMap * 1.0f / 2, BC_DataGame.api.WidthMap * 1.0f / 2);
                _dirRandom.z = Random.Range(-BC_DataGame.api.HeightMap * 1.0f / 2, BC_DataGame.api.HeightMap * 1.0f / 2);
                _dirRandom.y = transform.position.y;

                _dirRandom = _dirRandom - transform.position;
            }
        }
        else
        {
            _dirRandom.x = Random.Range(-BC_DataGame.api.WidthMap * 1.0f / 2, BC_DataGame.api.WidthMap * 1.0f / 2);
            _dirRandom.z = Random.Range(-BC_DataGame.api.HeightMap * 1.0f / 2, BC_DataGame.api.HeightMap * 1.0f / 2);
            _dirRandom.y = transform.position.y;

            _dirRandom = _dirRandom - transform.position;
        }

        yield return new WaitForSeconds(Random.Range(0, TimeRandomDir));
        StartCoroutine(RandomDir());
    }
    private void Update()
    {
        if (BCApiTest.api.isGoToTest || doneSetupData == false)
            return;
        if (GameManager.api.GameState == GAME_STATE.Playing && BC_Chapter_Info.api.AllowMonsterAtk)
        {
            _canMove = (_monsterSkill1.Skilling == false /*&& _myControl.Hit == false*/ && _myDetail.CurrentHP > 0);

            if (FollowPlayer && _canMove)
            {
                #region random dir
                Vector3 temp = _player.position;
                temp.y = transform.position.y;
                _dirRandom = temp - transform.position;

                transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(_dirRandom), Time.deltaTime * _myDetail.CurrentSpeedMove * 5);

                if (DistancePlayer < Distance(_player.position, transform.position))
                    _canMove = false;
                #endregion
            }

            if (_canMove && MoveType != EnumMoveBasic.KhongDi) // nếu không phải di chuyển bằng đi
            {
                // dir 
                if (FollowPlayer == false && !UseAI && _dirRandom != Vector3.zero)
                {
                    if (MoveType == EnumMoveBasic.Nhay)
                    {
                        if (inGround)
                        {
                            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(_dirRandom), Time.deltaTime * _myDetail.CurrentSpeedMove);
                        }
                    }
                    else
                    {
                        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(_dirRandom), Time.deltaTime * _myDetail.CurrentSpeedMove);
                    }
                }


                #region Di chuyển
                if (!MoveStep) // không di chuyển từng bước
                {
                    RunAnimation("Run");
                    if (!UseAI) // nhảy của ếch/ không dủ dụng AI
                    {
                        _rb.velocity = transform.forward * _myDetail.CurrentSpeedMove;
                    }
                    else // di chuyển không dùng AI
                    {
                        if (inGround)
                        {
                            FollowPlayer = false;
                            _agent.enabled = true;
                            _agent.SetDestination(_player.position);
                        }
                    }
                }
                else
                {
                    if (ObjCheckInGround != null && ObjCheckInGround.InGround)
                    {
                        if (UseIsRun)
                        {
                            if (isRun)
                            {
                                RunAnimation("Idle");
                                isRun = false;
                            }
                            else
                            {
                                isRun = false;
                                RunAnimation("Run");
                            }
                        }
                        else
                        {

                            //if (_anim.GetCurrentAnimatorStateInfo(0).IsName("Run"))
                            //{
                            //    _anim.StartPlayback();
                            //}
                            //else
                            //{
                            if (_anim.GetCurrentAnimatorStateInfo(0).IsName("Run") && !_anim.IsInTransition(0))
                            {
                                RunAnimation("Idle");
                            }
                            else
                            {
                                RunAnimation("Run");
                            }
                            //}
                        }
                    }
                }
                #endregion
            }
            else
            {
                _agent.enabled = false;
            }
        }
        else
        {
            _agent.enabled = false;
        }
    }
    bool isRun = false;

    public bool CanMove { get => _canMove; set => _canMove = value; }

    void EndRun()
    {
        isRun = true;
    }
    void BeginMoveTo()
    {
        if (!doneSetupData || (BCApiTest.api.isGoToTest && !BCApiTest.api.isAutoAttack)) return;

        if (GameManager.api.GameState == GAME_STATE.Playing)
        {
            if (!UseAI)
                _rb.velocity = transform.forward * _myDetail.CurrentSpeedMove;
            else
            {
                _agent.enabled = true;
                StartCoroutine(findplayer());
            }
        }
    }
    public IEnumerator findplayer()
    {
        while (true)
        {
            if (_player != null)
            {
                break;
            }
            yield return new WaitForSeconds(1);
        }
        _agent.SetDestination(_player.position);

        yield return null;
    }
    void EndMoveTo()
    {
        if (!UseAI)
            _rb.velocity = Vector3.zero;
        else
            _agent.enabled = false;
    }
    void BeginJump() // Chim Canh Cut
    {
        if (!UseAI && _canMove && MoveType != EnumMoveBasic.KhongDi)
        {
            _rb.AddForce(transform.up * JumpUp);
            _rb.velocity = transform.forward * _myDetail.CurrentSpeedMove;
        }
    }

    float Distance(Vector3 origin, Vector3 target)
    {
        Vector3 temp = target;
        temp.y = origin.y;

        return Mathf.Abs(Vector3.Distance(temp, origin));
    }
    void EndJump()
    {
    }
    void RunAnimation(string name)
    {
        foreach (var item in _nameAnim)
        {
            _anim.SetBool(item, false);
        }
        _anim.SetBool(name, true);
    }
    private void OnCollisionEnter(Collision obj)
    {
        if (obj.transform.tag != TagManager.api.Ground)
        {
            inGround = false;
            int chose = Random.Range(-5, 5);

            if (chose > 0)
                _dirRandom.x = transform.position.x * 2;
            else
                _dirRandom.x = transform.position.x * (-2);
        }
    }
    private void OnCollisionStay(Collision obj)
    {
        if (obj.transform.tag == TagManager.api.Ground)
        {
            inGround = true;
        }
    }
    bool AnimatorIsPlaying(string stateName)
    {
        return AnimatorIsPlaying() && _anim.GetCurrentAnimatorStateInfo(0).IsName(stateName);
    }
    bool AnimatorIsPlaying()
    {
        return _anim.GetCurrentAnimatorStateInfo(0).length >
               _anim.GetCurrentAnimatorStateInfo(0).normalizedTime;
    }
}
