﻿using System.Collections;
using UnityEngine;

public class PhongDienQuanhThanhSkill : SkillBase
{
    private MonsterSkillController _skillController;
    private MonsterController_new _myController;
    GameObject _objSub;
    Rigidbody _rb;

    private float _radius = 5;
    private void Awake()
    {
        _skillController = GetComponent<MonsterSkillController>();
        _myController = GetComponent<MonsterController_new>();
        _rb = GetComponent<Rigidbody>();
    }
    private void Start()
    {
        _objSub = new GameObject("DienQuang");
        _objSub.transform.parent = this.transform;
        _objSub.transform.localPosition = Vector3.zero;
        _objSub.AddComponent<SphereCollider>().radius = _radius;
        _objSub.GetComponent<SphereCollider>().isTrigger = true;
        _objSub.AddComponent<AtkPhongDien>();
        _objSub.SetActive(false);
    }

    public override IEnumerator SkillStart()
    {
        _skillController.Skilling = true;
        _skillTimeLive = 0;
        _isDoneSkill = false;
        _isReady = false;
        if (!_skillController.AllowMoveWhenSkilling) // nếu không muốn cho phép di chuyển khi đang sử dụng skill
            _rb.velocity = Vector3.zero;
        //Debug.Log("Chuẩn bị tấn công");
        if (!string.IsNullOrEmpty(StartAnimStr))
            _skillController.RunAnimation(StartAnimStr);
        else
            _skillController.RunAnimation(EnumAnimName.Idle.ToString());

        yield return new WaitForSeconds(TimeOfStartAnim);
        StartCoroutine("SkillIn");
    }
    protected override IEnumerator SkillIn()
    {
        if (!string.IsNullOrEmpty(InAnimStr))
            _skillController.RunAnimation(InAnimStr);
        else
            _skillController.RunAnimation(EnumAnimName.Idle.ToString());

        _objSub.SetActive(true);

        yield return new WaitForSeconds(TimeOfInAnim);
        StartCoroutine("SkillEnd");
    }
    protected override IEnumerator SkillEnd()
    {
        _skillController.ResetAnimation("");
        //Debug.Log("Kết thúc tấn công");
        if (!string.IsNullOrEmpty(EndAnimStr))
            _skillController.RunAnimation(EndAnimStr);

        _objSub.SetActive(false);

        yield return new WaitForSeconds(TimeOfEndAnim);
        _skillController.RunAnimation(EnumAnimName.Idle.ToString());
        _isDoneSkill = true;
        _loop = 0;
        _skillController.Skilling = false;
    }

    private void Update()
    {
        bool canUseSkill = !_skillController.Skilling && _isReady /*&& _myController.Hit == false*/;

        if (canUseSkill) // khoong cos skill nao dang hoat dong va da san sang
        {
            StartCoroutine(SkillStart());
        }

        if (_isDoneSkill)
        {
            _skillTimeLive += Time.deltaTime;
            if (_skillTimeLive > TimeCooldown)
                _isReady = true;
        }
    }
}
