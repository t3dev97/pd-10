﻿using UnityEngine;

public class AtkPhongDien : MonoBehaviour
{
    MonsterDetail _myDetail;
    private void Awake()
    {
        _myDetail = transform.parent.GetComponent<MonsterDetail>();
    }
    private void OnTriggerStay(Collider obj)
    {
        if (obj.transform.tag == TagManager.api.Player)
        {
            obj.transform.GetComponent<PlayerController>().Hit(_myDetail.CurrentATKDamage, _myDetail.RaceType, _myDetail.AtkType);
        }
    }
}
