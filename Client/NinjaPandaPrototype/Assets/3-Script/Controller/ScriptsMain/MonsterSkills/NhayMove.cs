﻿using System.Collections;
using UnityEngine;
using UnityEngine.AI;
public class NhayMove : SkillBase
{
    MonsterSkillController _skillController;
    MonsterDetail _myDetail;
    MonsterController_new _myController;
    Rigidbody _rb;

    Vector3 _dir = Vector3.zero;
    NavMeshAgent _agent;
    GameObject _player;

    private void Awake()
    {
        _skillController = GetComponent<MonsterSkillController>();
        _myDetail = GetComponent<MonsterDetail>();
        _myController = GetComponent<MonsterController_new>();
        _rb = GetComponent<Rigidbody>();
        _agent = GetComponent<NavMeshAgent>();

    }
    private void Start()
    {
        _player = GameObject.FindGameObjectWithTag(TagManager.api.Player);
    }

    public override IEnumerator SkillStart()
    {
        _skillController.Skilling = true;
        _skillTimeLive = 0;
        _isDoneSkill = false;
        _isReady = false;
        //Debug.Log("Chuẩn bị tấn công");
        if (!string.IsNullOrEmpty(StartAnimStr))
            _skillController.RunAnimation(StartAnimStr);

        yield return new WaitForSeconds(TimeOfStartAnim);

        StartCoroutine(SkillIn());
    }
    protected override IEnumerator SkillIn()
    {

        //Debug.Log("Đang thực hiện tấn công");
        if (!string.IsNullOrEmpty(InAnimStr))
            _skillController.Amin.Play(InAnimStr);

        yield return new WaitForSeconds(TimeOfInAnim);
        _skillController.RunAnimation(EnumAnimName.Idle.ToString());


        if (_loop < TotalLoop)
        {
            _loop++;
            StartCoroutine("SkillIn");
        }
        else
            StartCoroutine(SkillEnd());
    }

    void MoveTo()
    {
        _agent.enabled = true;
        _agent.speed = _myDetail.CurrentSpeedMove;
        _agent.SetDestination(_player.transform.position);
        //_rb.velocity = transform.forward * _myDetail.SpeedMove;
    }

    void EndMove()
    {
        _agent.enabled = false;
    }

    protected override IEnumerator SkillEnd()
    {
        _rb.velocity = Vector3.zero;
        _skillController.ResetAnimation("");
        //Debug.Log("Kết thúc tấn công");
        if (!string.IsNullOrEmpty(EndAnimStr))
            _skillController.RunAnimation(EndAnimStr);
        else
            _skillController.RunAnimation("Idle");

        yield return new WaitForSeconds(TimeOfEndAnim);
        _isDoneSkill = true;
        _loop = 0;
        _skillController.RunAnimation(EnumAnimName.Idle.ToString());
        _skillController.Skilling = false;
    }



    private void Update()
    {
        bool canUseSkill = !_skillController.Skilling && _isReady /*&& _myController.Hit == false*/;

        if (canUseSkill) // khoong cos skill nao dang hoat dong va da san sang
        {
            _rb.velocity = Vector3.zero;
            StartCoroutine(SkillStart());
        }


        if (_isDoneSkill)
        {
            _skillTimeLive += Time.deltaTime;
            if (_skillTimeLive > TimeCooldown)
                _isReady = true;
        }
    }
}
