﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;


public class DonThoSkill : SkillBase
{
    public bool FollowPlayer = false;
    public float Distance = 10;
    public GameObject EffectBegin;
    public GameObject EffectMoving;
    public GameObject EffectEnd;
    public float TimeDontTakeDamage = 0.1f; // when monster in statu pre-move

    MonsterSkillController _skillController;
    MonsterDetail _myDetail;
    MonsterController_new _myController;
    Rigidbody _rb;
    Vector3 _dir = Vector3.zero;
    GameObject _player;
    NavMeshAgent _agent;
    GameObject effectmove;
    bool ismoving = false;
    private void Awake()
    {
        if (GetComponent<MonsterSkillController>() == null)
            gameObject.AddComponent<MonsterSkillController>();
    }
    private void Start()
    {
        _skillController = GetComponent<MonsterSkillController>();
        _myDetail = GetComponent<MonsterDetail>();
        _myController = GetComponent<MonsterController_new>();
        _rb = GetComponent<Rigidbody>();
        _player = GameObject.FindGameObjectWithTag("Player");
        _agent = GetComponent<NavMeshAgent>();
        TimeCooldown = 0;
        StartCoroutine(Delay());
    }
    IEnumerator Delay()
    {
        yield return new WaitForSeconds(0.5f);
        InitMap.api.CreateNavigation();
        if (FollowPlayer)
        {
            try
            {
                _agent.SetDestination(_player.transform.position);
            }
            catch (Exception)
            {
                Debug.Log("InitMap.api.CreateNavigation();");
                throw;
            }
        }
        else
        {
            Vector3 posRb = GetRandomPoint(transform.position, Distance);
            _agent.SetDestination(posRb);
        }
    }
    public override IEnumerator SkillStart()
    {
        _skillTimeLive = 0;
        _isDoneSkill = false;
        _isReady = false;

        if (!_myController.CheckDie)
            EffectBeginF();

        if (!string.IsNullOrEmpty(StartAnimStr))
            _skillController.RunAnimation(StartAnimStr);

        yield return new WaitForSeconds(TimeOfStartAnim);

        if (!_myController.CheckDie)
            StartCoroutine(SkillIn());
    }
    protected override IEnumerator SkillIn()
    {
        if (!string.IsNullOrEmpty(InAnimStr))
            _skillController.RunAnimation(InAnimStr);

        ismoving = true;
        _agent.enabled = true;

        float timeLive = 0;
        while (timeLive < TimeOfInAnim && ismoving)
        {
            timeLive += Time.deltaTime;
            _agent.SetDestination(_player.transform.position); // cập nhật lại vị trí nhân vật
            yield return new WaitForSeconds(Time.deltaTime);
        }
        yield return new WaitForEndOfFrame();

        if (!_myController.CheckDie)
            StartCoroutine(SkillEnd());
    }
    protected override IEnumerator SkillEnd()
    {
        if (effectmove != null)
        {
            effectmove.transform.GetChild(1).GetComponent<ParticleSystem>().Stop();
            effectmove.transform.GetChild(0).GetComponent<ParticleSystem>().Stop();
            Destroy(effectmove, 2);
        }

        EffectEndF();
        _rb.velocity = Vector3.zero;

        if (_agent.enabled)
            _agent.enabled = false;

        if (!string.IsNullOrEmpty(EndAnimStr))
            _skillController.RunAnimation(EndAnimStr);

        gameObject.layer = 13;
        gameObject.tag = TagManager.api.Monster;
        ismoving = false;

        yield return new WaitForSeconds(TimeOfEndAnim);

        if (!_myController.CheckDie)
        {
            _isDoneSkill = true;
            _isReady = true;
            _loop = 0;
            _skillController.RunAnimation(EnumAnimName.Idle.ToString());
        }
    }
    void MoveTo()
    {
        if (GameManager.api.GameState != GAME_STATE.Playing) return;
        gameObject.tag = TagManager.api.MonsterBurrow; // không đưa quái này vào danh sách quái mục tiêu
        gameObject.layer = 20;  // không bắt va chạm với đạn

        effectmove = Instantiate(EffectMoving, transform.position, Quaternion.identity, transform);
        effectmove.transform.GetChild(0).GetComponent<ParticleSystem>().startLifetime = TimeOfInAnim;
        effectmove.transform.localPosition = Vector3.zero;
        effectmove.gameObject.SetActive(true);
    }

    void EffectBeginF()
    {
        GameObject ef = Instantiate(EffectBegin, transform.position, Quaternion.identity);
        Destroy(ef, TimeOfStartAnim);
    }
    void EffectMoveF()
    {
        GameObject ef = Instantiate(EffectBegin, transform.position, Quaternion.identity);
    }
    void EffectEndF()
    {
        GameObject ef = Instantiate(EffectEnd, transform.position, Quaternion.identity);
        Destroy(ef, TimeOfEndAnim);
    }
    private void Update()
    {
        if (GameManager.api.GameState == GAME_STATE.Playing && BC_Chapter_Info.api.AllowMonsterAtk)
        {
            bool canUseSkill = !_skillController.Skilling && _isReady /*&& _myController.Hit == false*/ && _myDetail.CurrentHP > 0;
            if (_myDetail.CurrentHP > 0)
            {
                _dir = _player.transform.position;
                _dir.y = transform.position.y;
                transform.LookAt(_dir);
            }

            if (canUseSkill == false)
            {
                _agent.enabled = false;
                _isReady = true;
            }

            if (_isDoneSkill)
            {
                _skillTimeLive += Time.deltaTime;
                if (_skillTimeLive > TimeCooldown)
                {
                    _isReady = true;
                    if (canUseSkill)
                        StartCoroutine(SkillStart());
                }
            }
        }
    }
    private Action _skillStart;
    private void AddFreeSkillToSkillController()
    {
        _skillStart = () =>
        {
            StartCoroutine(SkillStart());
        };
        _skillController.AddFreeSkill(_skillStart);
    }
    public static Vector3 GetRandomPoint(Vector3 center, float maxDistance)
    {
        Vector3 randomPos = UnityEngine.Random.insideUnitSphere * maxDistance + center;
        NavMeshHit hit; // NavMesh Sampling Info Container

        NavMesh.SamplePosition(randomPos, out hit, maxDistance, NavMesh.AllAreas);
        return hit.position;
    }

    private void OnTriggerEnter(Collider obj)
    {
        if (obj.transform.tag == TagManager.api.Player)
        {
            ismoving = false;
        }
    }
}
