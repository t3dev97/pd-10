﻿using Studio.BC;
using System;
using System.Collections;
using UnityEngine;

public class MoveSuperFast : SkillBase
{
    //[SerializeField]
    bool _randomDir = true;

    //[SerializeField]
    float _timeReRandomMin;

    //[SerializeField]
    float _timeReRandomMax;

    MonsterSkillController _skillController;
    MonsterController_new _myController;
    MonsterDetail _myDetail;
    Transform _container;
    GameObject _player;
    Rigidbody _rb;
    Vector3 _posRandom;

    [SerializeField]
    float _speedMove;

    [SerializeField]
    float _distance;

    [SerializeField]
    SkinnedMeshRenderer _skinnedMeshRenderer;

    private void Awake()
    {
        if (GetComponent<MonsterSkillController>() == null)
            gameObject.AddComponent<MonsterSkillController>();

        _skillController = GetComponent<MonsterSkillController>();

        if (GetComponent<Rigidbody>() != null)
            _rb = GetComponent<Rigidbody>();

        _myDetail = GetComponent<MonsterDetail>();
        _myController = GetComponent<MonsterController_new>();
    }
    private void Start()
    {
        _player = GameObject.FindGameObjectWithTag(TagManager.api.Player);
        _container = GameObject.FindObjectOfType<BCCache>().transform;
        TimeCooldown = 0;
        AddFreeSkillToSkillController();
    }
    public override IEnumerator SkillStart()
    {
        if (_rb != null)
            _rb.velocity = Vector3.zero;
        _skillController.Skilling = true;
        _skillTimeLive = 0;
        _isDoneSkill = false;
        _isReady = false;

        StartCoroutine(RandomDir()); // random dir 

        transform.LookAt(_posRandom);
        if (!string.IsNullOrEmpty(StartAnimStr))
            _skillController.RunAnimation(StartAnimStr);

        yield return new WaitForSeconds(TimeOfStartAnim);
        StartCoroutine(SkillIn());
    }
    protected override IEnumerator SkillIn()
    {
        BCCache.Api.GetEffectPrefabAsync(EffectKey.EnemyDieFX, (go) =>
        {
            var _dieEffect = Instantiate(go);
            _dieEffect.transform.position = new Vector3(transform.position.x, transform.position.y + 1f, transform.position.z);
            _dieEffect.SetActive(true);
            Destroy(_dieEffect, 2f);
        });

        gameObject.tag = TagManager.api.MonsterBurrow; // không đưa quái này vào danh sách quái mục tiêu
        gameObject.layer = 20;  // không bắt va chạm với đạn

        _skinnedMeshRenderer.enabled = false;
        _myDetail.HealtBar.SetActive(false);
        //if (!string.IsNullOrEmpty(InAnimStr))
        //    _skillController.Amin.Play(InAnimStr);

        MoveToTarget();

        yield return new WaitForSeconds(TimeOfInAnim);
        if (_loop < TotalLoop)
        {
            _loop++;
            StartCoroutine("SkillIn");
        }
        else
            StartCoroutine(SkillEnd());
    }
    protected override IEnumerator SkillEnd()
    {
        BCCache.Api.GetEffectPrefabAsync(EffectKey.EnemyDieFX, (go) =>
        {
            var _dieEffect = Instantiate(go);
            _dieEffect.transform.position = new Vector3(transform.position.x, transform.position.y + 1f, transform.position.z);
            _dieEffect.SetActive(true);
            Destroy(_dieEffect, 2f);
        });

        _skinnedMeshRenderer.enabled = true;
        _myDetail.HealtBar.SetActive(true);
        if (!string.IsNullOrEmpty(EndAnimStr))
            _skillController.RunAnimation(EndAnimStr);


        yield return new WaitForSeconds(TimeOfEndAnim);
        gameObject.layer = 13;
        gameObject.tag = TagManager.api.Monster;

        _skillController.RunAnimation(EnumAnimName.Idle.ToString());
        _isDoneSkill = true;
        _isReady = true;
        _loop = 0;
        _skillController.Skilling = false;
        AddFreeSkillToSkillController();
    }
    void MoveToTarget()
    {
        float dis = Vector3.Distance(gameObject.transform.position, _posRandom);
        float timeMove = dis / _speedMove;
        TimeOfInAnim = timeMove;
        transform.position = _posRandom;
    }

    IEnumerator RandomDir()
    {
        if (_randomDir)
        {
            if (_myDetail.RaceType == MONSTER_RACE_TYPE.earth)
            {
                if (_myController.InGround && _myController.ObjCheckInGround != null && _myController.ObjCheckInGround.GetComponent<CheckInGround>() != null && _myController.ObjCheckInGround.GetComponent<CheckInGround>().InGround)
                {
                    _posRandom = PosRandom(_distance);
                }
                //yield return new WaitForSeconds(UnityEngine.Random.Range(_timeReRandomMin, _timeReRandomMax));
                //StartCoroutine(RandomDir());
            }
            else // loai di chuyen khac
            {
                _posRandom = PosRandom(_distance);
            }
        }
        else // follow player
        {

        }
        yield return null;
    }

    Vector3 PosRandom(float _distance)
    {
        Vector3 t;
        t.y = transform.position.y;
        t.x = UnityEngine.Random.Range(-BC_DataGame.api.WidthMap * 1.0f / 2 + 1, BC_DataGame.api.WidthMap * 1.0f / 2 - 1);
        t.z = UnityEngine.Random.Range(-BC_DataGame.api.HeightMap * 1.0f / 2 + 1, BC_DataGame.api.HeightMap * 1.0f / 2 - 1);

        t.x = Mathf.Clamp(t.x + _distance, -BC_DataGame.api.WidthMap * 1.0f / 2 + 1, BC_DataGame.api.WidthMap * 1.0f / 2 - 1);
        t.z = Mathf.Clamp(t.z + _distance, -BC_DataGame.api.HeightMap * 1.0f / 2 + 1, BC_DataGame.api.HeightMap * 1.0f / 2 - 1);

        return t;
    }
    private Action _skillStart;
    private void AddFreeSkillToSkillController()
    {
        _skillStart = () =>
        {
            StartCoroutine(SkillStart());
        };
        _skillController.AddFreeSkill(_skillStart, SkillPriority);
    }
    private void OnDrawGizmos()
    {

        Gizmos.DrawSphere(_posRandom, 1);
        //RaycastHit objHit;
        //if (Physics.SphereCast(_posRandom, 1, transform.up, out objHit))
        //{
        //    if (objHit.transform.tag == TagManager.api.Wall)
        //        Gizmos.color = Color.red;
        //    else Gizmos.color = Color.green;
        //}
        //else
        //    Gizmos.color = Color.black;
    }
}
