﻿using System.Collections;
using UnityEngine;

public enum SkillType
{
    ATK,
    MOVE
}
public class SkillBase : MonoBehaviour
{
    public string StartAnimStr = "PreAtk";
    public string InAnimStr = "Atk";
    public string EndAnimStr = "EndAtk";
    public int TotalLoop; // số lần thực hiện skill

    public float TimeOfStartAnim = 0.5f;// thời gian thực hiện Animation
    public float TimeOfInAnim = 1.5f;
    public float TimeOfEndAnim = 0.5f;
    [HideInInspector]
    public float TimeCooldown; // thời gian giản cách để thực hiện skill
    public int SkillPriority = 0;
    [HideInInspector]
    public SkillType SkillType = SkillType.ATK;

    protected int _loop = 0;
    [SerializeField]
    protected float _skillTimeLive;


    protected bool _isDoneSkill = true; // skill đã thực hiện xong, khi true se bat dau dem cooldown
    [SerializeField]
    protected bool _isReady = true; // có thể thực hiên skill này sau thời gian cooldown
    protected MonsterSkillController monsterController;
    protected void SetData()
    {
        monsterController = GetComponent<MonsterSkillController>();
    }
    public virtual IEnumerator SkillStart()
    {
        //monsterController.Skilling = true;
        //_skillTimer = 0;
        //_isDoneSkill = false;
        //_isReady = false;
        //if (!string.IsNullOrEmpty(StartAnimStr))
        //    monsterController.RunAnimation(StartAnimStr);

        yield return new WaitForSeconds(TimeOfStartAnim);
        StartCoroutine(SkillIn());
    }

    protected virtual IEnumerator SkillIn()
    {
        //// thực hiện skill
        //Debug.Log("Đang thực hiện tấn công");
        //if (!string.IsNullOrEmpty(InAnimStr))
        //    monsterController.Amin.Play(InAnimStr);
        //Skilling();
        // kết thúc 1 lần thực hiện 
        yield return new WaitForSeconds(TimeOfInAnim);
        if (_loop < TotalLoop)
        {
            _loop++;
            StartCoroutine("SkillIn");
        }
        else
            StartCoroutine(SkillEnd());
    }

    protected virtual IEnumerator SkillEnd()
    {
        //if (!string.IsNullOrEmpty(EndAnimStr))
        //    monsterController.RunAnimation(EndAnimStr);
        yield return new WaitForSeconds(TimeOfEndAnim);
        monsterController.RunAnimation(EnumAnimName.Idle.ToString());
        _isDoneSkill = true;
        _loop = 0;
        monsterController.Skilling = false;
    }
    public virtual bool CheckSkillAvailable()
    {
        return (_skillTimeLive >= TimeCooldown && _isDoneSkill);
    }
    protected virtual void Skilling()
    {

    }
}
