﻿using System;
using System.Collections;
using UnityEngine;

public class RonllSkill : SkillBase
{
    MonsterSkillController _skillController;
    MonsterDetail _myDetail;
    MonsterController_new _myController;
    Rigidbody _rb;

    Vector3 _dir = Vector3.zero;
    GameObject _player;

    [Header("Speed Move")]
    [SerializeField]
    float _speedToFront;
    [SerializeField]
    float _speedToBack;

    [SerializeField]
    bool _lookAt = true;


    private void Awake()
    {
        if (GetComponent<MonsterSkillController>() == null)
            gameObject.AddComponent<MonsterSkillController>();

        _skillController = GetComponent<MonsterSkillController>();
        _myDetail = GetComponent<MonsterDetail>();
        _myController = GetComponent<MonsterController_new>();
        _rb = GetComponent<Rigidbody>();
        _player = GameObject.FindGameObjectWithTag("Player");
    }

    public override IEnumerator SkillStart()
    {
        _skillController.Skilling = true;
        _skillTimeLive = 0;
        _isDoneSkill = false;
        _isReady = false;
        gameObject.tag = TagManager.api.MonsterBurrow;

        if (_lookAt)
        {
            var lookDir = _player.transform.position - transform.position;
            lookDir.y = 0; // keep only the horizontal direction
            transform.rotation = Quaternion.LookRotation(lookDir);
        }

        if (!string.IsNullOrEmpty(StartAnimStr))
            _skillController.RunAnimation(StartAnimStr);

        yield return new WaitForSeconds(TimeOfStartAnim);

        StartCoroutine(SkillIn());
    }
    protected override IEnumerator SkillIn()
    {
        if (!string.IsNullOrEmpty(InAnimStr))
            _skillController.RunAnimation(InAnimStr);
        yield return new WaitForSeconds(.1f);
        MoveToFront();

        yield return new WaitForSeconds(TimeOfInAnim / 2);

        if (!string.IsNullOrEmpty(InAnimStr))
            _skillController.RunAnimation(InAnimStr);
        MoveToBack();

        yield return new WaitForSeconds(TimeOfInAnim / 2);
        StartCoroutine(SkillEnd());
    }

    protected override IEnumerator SkillEnd()
    {
        if (ef != null)
        {
            ef.transform.GetChild(1).GetComponent<ParticleSystem>().Stop();
            ef.transform.GetChild(0).GetComponent<ParticleSystem>().Stop();
        }

        if (!string.IsNullOrEmpty(EndAnimStr))
            _skillController.RunAnimation(EndAnimStr);
        yield return new WaitForSeconds(TimeOfEndAnim);

        _rb.velocity = Vector3.zero;
        gameObject.tag = TagManager.api.Monster;

        Debug.Log("Kết thúc tấn công");
        _isDoneSkill = true;
        _loop = 0;
        _skillController.RunAnimation(EnumAnimName.Idle.ToString());
        _skillController.Skilling = false;
    }
    GameObject ef;
    void MoveToFront()
    {
        if (ef != null)
        {
            ef.transform.localPosition = Vector3.zero;
            ef.gameObject.SetActive(true);
        }

        _rb.velocity = transform.forward * _speedToFront;
    }
    void MoveToBack()
    {
        if (ef != null)
        {
            ef.transform.localPosition = Vector3.zero;
            ef.gameObject.SetActive(true);
        }
        _rb.velocity = -transform.forward * _speedToBack;
    }

    private void Update()
    {
        bool canUseSkill = /*!_skillController.Skilling &&*/ _isReady /*&& _myController.Hit == false*/;

        if (canUseSkill) // khoong cos skill nao dang hoat dong va da san sang
        {
            AddFreeSkillToSkillController();
        }

        if (_isDoneSkill)
        {
            _skillTimeLive += Time.deltaTime;
            if (_skillTimeLive > TimeCooldown)
                _isReady = true;
        }
    }
    private Action _skillStart;
    private void AddFreeSkillToSkillController()
    {
        _skillStart = () =>
        {
            StartCoroutine(SkillStart());
        };
        _skillController.AddFreeSkill(_skillStart);
    }

    private void OnTriggerEnter(Collider obj)
    {
        if (obj.transform.tag == TagManager.api.Wall)
        {
            _rb.velocity = Vector3.zero;
        }
    }
}
