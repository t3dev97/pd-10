﻿using Studio.BC;
using System;
using System.Collections;
using UnityEngine;

public class ShootSkill : SkillBase
{
    //public bool Auto = false;

    [Header("Shoot Info")]
    public GameObject bullet;
    public EnumShootSkill ShootType;
    public int NumberShoot;
    public float SpeedShoot;
    public bool ShootDown = false;

    [Header("Option Dan Phao")]
    public float TimeMove = 2;
    public float OffSetY = 0;

    [Header("Target")]
    [Tooltip("TRUE: tự bắn, FALSE: bắn phụ thuộc vào anmation")]
    public bool Auto = true;// true: tự bắn, false: phụ thuộc vào animation 
    [Tooltip("Không xác định mục tiêu")]
    public bool ARound = false; // 
    public float Distance;
    public bool LookPlayer = true;

    [Header("Test Shoot")]
    public bool Shoot = false;

    [Header("Bullet Info")]
    public Transform PosBullet;
    public float Angle;
    public bool RandomAngle = false;
    public float OffsetAngle;
    public EnumBulletType BulletType;
    public float speedBulletMove = 0.5f;

    [Header("Other Setting")]
    [Tooltip("Skill được kích hoạt bởi skill khác")]
    [HideInInspector]
    public bool DependentOtherSkill = false;

    MonsterSkillController _skillController;
    MonsterDetail _myDetail;
    Transform _container;
    GameObject _player;
    Rigidbody _rb;
    private void Awake()
    {
        if (GetComponent<MonsterSkillController>() == null)
            gameObject.AddComponent<MonsterSkillController>();

        _skillController = GetComponent<MonsterSkillController>();

        if (GetComponent<Rigidbody>() != null)
            _rb = GetComponent<Rigidbody>();

        _myDetail = GetComponent<MonsterDetail>();
    }
    private void Start()
    {
        _player = GameObject.FindGameObjectWithTag(TagManager.api.Player);
        _container = GameObject.FindObjectOfType<BCCache>().transform;
        TimeCooldown = 0;
        AddFreeSkillToSkillController();
    }

    public override IEnumerator SkillStart()
    {
        if (_rb != null)
            _rb.velocity = Vector3.zero;
        _skillController.Skilling = true;
        _skillTimeLive = 0;
        _isDoneSkill = false;
        _isReady = false;
        if (!string.IsNullOrEmpty(StartAnimStr))
            _skillController.RunAnimation(StartAnimStr);

        if (LookPlayer)
        {
            if (_player == null)
                _player = GameObject.FindGameObjectWithTag(TagManager.api.Player);

            var lookDir = _player.transform.position - transform.position;
            lookDir.y = 0; // keep only the horizontal direction
            transform.rotation = Quaternion.LookRotation(lookDir);
        }
        Debug.Log("1");
        yield return new WaitForSeconds(TimeOfStartAnim);
        StartCoroutine(SkillIn());
    }
    protected override IEnumerator SkillIn()
    {
        if (!string.IsNullOrEmpty(InAnimStr))
            _skillController.Amin.Play(InAnimStr);

        if (Auto)
            OneShoot();
        Debug.Log("2");
        yield return new WaitForSeconds(TimeOfInAnim);
        if (_loop < TotalLoop)
        {
            _loop++;
            StartCoroutine("SkillIn");
        }
        else
            StartCoroutine(SkillEnd());
    }
    protected override IEnumerator SkillEnd()
    {
        if (!string.IsNullOrEmpty(EndAnimStr))
            _skillController.RunAnimation(EndAnimStr);
        Debug.Log("3");
        yield return new WaitForSeconds(TimeOfEndAnim);
        _skillController.RunAnimation(EnumAnimName.Idle.ToString());
        _isDoneSkill = true;
        _isReady = true;
        _loop = 0;
        _skillController.Skilling = false;
        AddFreeSkillToSkillController();
    }
    void OneShoot() // dùng để test
    {
        {
            switch (ShootType)
            {
                case EnumShootSkill.BanThang:
                    {
                        StartCoroutine(BanTia(PosBullet, NumberShoot, Angle, OffsetAngle));
                        break;
                    }
                case EnumShootSkill.DanPhao:
                    {
                        if (ARound)
                        {
                            StartCoroutine(CannonShootAround(transform.position, NumberShoot, Distance));
                            return;
                        }
                        else
                        if (_player == null) return;

                        StartCoroutine(CannonShoot(_player.transform.position, NumberShoot));
                        break;
                    }
                case EnumShootSkill.Boomerang:
                    {
                        StartCoroutine(BanBoomerang(PosBullet, NumberShoot, Angle, OffsetAngle));
                        break;
                    }
                default:
                    {
                        break;
                    }
            }
        }
    }

    public void OneShootTime() // 
    {
        if (GameManager.api.GameState == GAME_STATE.Playing)
        {
            if (!Auto) // không dùng trong animation
            {
                switch (ShootType)
                {
                    case EnumShootSkill.BanThang:
                        {
                            StartCoroutine(BanTia(PosBullet, NumberShoot, Angle, OffsetAngle));
                            break;
                        }
                    case EnumShootSkill.DanPhao:
                        {
                            if (ARound)
                            {
                                StartCoroutine(CannonShootAround(transform.position, NumberShoot, Distance));
                                return;
                            }
                            else
                            if (_player == null) return;
                            StartCoroutine(CannonShoot(_player.transform.position, NumberShoot));
                            break;
                        }
                    case EnumShootSkill.Boomerang:
                        {
                            StartCoroutine(BanBoomerang(PosBullet, NumberShoot, Angle, OffsetAngle));
                            break;
                        }
                    default:
                        {
                            break;
                        }
                }
            }
        }
    }
    IEnumerator BanTia(Transform originBulletTrans, int numberBullet = 8, float _angle = 360, float offset = 0) // thêm 2 tia phía trước
    {
        if (_myDetail.CurrentHP <= 0) yield return null;

        GameObject t;
        Vector3 pos = PosBullet.position; // vị trí sinh đạn
        float angle = _angle / numberBullet;
        float angleYLeft = originBulletTrans.rotation.eulerAngles.y + offset;
        float angleYRight = originBulletTrans.rotation.eulerAngles.y + offset;

        if (ShootDown)
            PosBullet.LookAt(_player.transform.position);

        // Viên đạn đầu tiên
        if (bullet == null)
        {
            //Debug.Log("Bullet Null");
            yield return null;
        }
        t = Instantiate(bullet, pos, transform.rotation, _container); // bắn đạn
        t.transform.eulerAngles = new Vector3(0, angleYLeft, 0);
        t.gameObject.SetActive(true);

        // setup bullet
        //if (t.gameObject.GetComponent<BulletMoveController>() != null)
        //    t.gameObject.GetComponent<BulletMoveController>().SetUpCurveMove(_player.transform, speedBulletMove, 2, damage: _myDetail.CurrentATKDamage);

        //if (t.gameObject.GetComponent<BulletDetail>() != null)
        //    t.gameObject.GetComponent<BulletDetail>().SetUpBullet(_myDetail.CurrentATKDamage, _myDetail.RaceType, ShootDown);

        //if (t.GetComponent<MonsterDetail>() != null) 
        //    t.GetComponent<MonsterDetail>().isDropItem = false;

        SetUpBullet(ref t);

        yield return new WaitForSeconds(SpeedShoot);
        if (RandomAngle == false)
        {

            for (int i = 0; i < numberBullet / 2; i++)
            {
                if (LookPlayer)
                {
                    var lookDir = _player.transform.position - transform.position;
                    lookDir.y = 0; // keep only the horizontal direction
                    transform.rotation = Quaternion.LookRotation(lookDir);
                }

                t = Instantiate(bullet, pos, transform.rotation, _container); // bắn đạn
                t.transform.eulerAngles = new Vector3(0, angleYLeft += angle, 0);
                t.gameObject.SetActive(true);

                // setup bullet
                //if (t.gameObject.GetComponent<BulletDetail>() != null)
                //    t.gameObject.GetComponent<BulletDetail>().SetUpBullet(_myDetail.CurrentATKDamage, _myDetail.RaceType, ShootDown);

                //if (t.gameObject.GetComponent<BulletMoveController>() != null)
                //    t.gameObject.GetComponent<BulletMoveController>().SetUpStraightMove(_player.transform, 5);

                SetUpBullet(ref t);

                if (numberBullet > 2 && i < numberBullet / 2)
                {
                    yield return new WaitForSeconds(SpeedShoot);
                    t = Instantiate(bullet, pos, transform.rotation, _container); // bắn đạn
                    t.transform.eulerAngles = new Vector3(0, angleYRight -= angle, 0);
                    t.gameObject.SetActive(true);
                }

                //setup bullet
                //if (t.gameObject.GetComponent<BulletDetail>() != null)
                //    t.gameObject.GetComponent<BulletDetail>().SetUpBullet(_myDetail.CurrentATKDamage, _myDetail.RaceType, ShootDown);

                //if (t.gameObject.GetComponent<BulletMoveController>() != null)
                //    t.gameObject.GetComponent<BulletMoveController>().SetUpCurveMove(_player.transform, speedBulletMove, 2, damage: _myDetail.CurrentATKDamage);

                SetUpBullet(ref t);

                yield return new WaitForSeconds(SpeedShoot);
            }
        }
        else
        {
            for (int i = 0; i < numberBullet / 2; i++)
            {
                if (LookPlayer)
                {
                    var lookDir = _player.transform.position - transform.position;
                    lookDir.y = 0; // keep only the horizontal direction
                    transform.rotation = Quaternion.LookRotation(lookDir);
                }

                t = Instantiate(bullet, pos, transform.rotation, _container); // bắn đạn
                t.transform.eulerAngles = new Vector3(0, angleYLeft + UnityEngine.Random.Range(0, _angle / 2), 0);
                t.gameObject.SetActive(true);

                // setup bullet
                //if (t.gameObject.GetComponent<BulletDetail>() != null)
                //    t.gameObject.GetComponent<BulletDetail>().SetUpBullet(_myDetail.CurrentATKDamage, _myDetail.RaceType, ShootDown);

                //if (t.gameObject.GetComponent<BulletMoveController>() != null)
                //    t.gameObject.GetComponent<BulletMoveController>().SetUpStraightMove(_player.transform, 5);
                SetUpBullet(ref t);

                if (numberBullet > 2)
                {
                    yield return new WaitForSeconds(SpeedShoot);
                    t = Instantiate(bullet, pos, transform.rotation, _container); // bắn đạn
                    t.transform.eulerAngles = new Vector3(0, angleYRight - UnityEngine.Random.Range(0, _angle / 2), 0);
                    t.gameObject.SetActive(true);
                }

                //setup bullet
                //if (t.gameObject.GetComponent<BulletDetail>() != null)
                //    t.gameObject.GetComponent<BulletDetail>().SetUpBullet(_myDetail.CurrentATKDamage, _myDetail.RaceType, ShootDown);

                //if (t.gameObject.GetComponent<BulletMoveController>() != null)
                //    t.gameObject.GetComponent<BulletMoveController>().SetUpCurveMove(_player.transform, speedBulletMove, 2, damage: _myDetail.CurrentATKDamage);
                SetUpBullet(ref t);

                yield return new WaitForSeconds(SpeedShoot);
            }
        }
    }

    void SetUpBullet(ref GameObject t)
    {
        if (t.gameObject.GetComponent<BulletMoveController>() != null)
            t.gameObject.GetComponent<BulletMoveController>().SetUpCurveMove(_player.transform, speedBulletMove, 2, damage: _myDetail.CurrentATKDamage);

        if (t.gameObject.GetComponent<BulletDetail>() != null)
            t.gameObject.GetComponent<BulletDetail>().SetUpBullet(_myDetail.CurrentATKDamage, _myDetail.RaceType, ShootDown);

        if (t.GetComponent<MonsterDetail>() != null)
            t.GetComponent<MonsterDetail>().isDropItem = false;
    }
    IEnumerator BanBoomerang(Transform originBulletTrans, int numberShoot = 8, float _angle = 360, float offset = 0) // thêm 2 tia phía trước
    {
        GameObject temp;
        Vector3 pos = PosBullet.position; // vị trí sinh đạn
        float angle = _angle / numberShoot;
        float angleYLeft = originBulletTrans.rotation.eulerAngles.y + offset;
        float angleYRight = originBulletTrans.rotation.eulerAngles.y + offset;

        // Viên đạn đầu tiên
        temp = Instantiate(bullet, pos, transform.rotation, _container); // bắn đạn
        temp.transform.eulerAngles = new Vector3(0, angleYLeft, 0);
        temp.gameObject.SetActive(true);

        // setup bullet
        if (temp.gameObject.GetComponent<BulletMoveController>() != null)
            temp.gameObject.GetComponent<BulletMoveController>().SetUpCurveMove(_player.transform, speedBulletMove, 1, damage: _myDetail.CurrentATKDamage);

        yield return new WaitForSeconds(SpeedShoot);
        for (int i = 0; i < numberShoot / 2; i++)
        {
            temp = Instantiate(bullet, pos, transform.rotation, _container); // bắn đạn
            temp.transform.eulerAngles = new Vector3(0, angleYLeft += angle, 0);
            temp.gameObject.SetActive(true);

            // setup bullet
            if (temp.gameObject.GetComponent<BulletMoveController>() != null)
                temp.gameObject.GetComponent<BulletMoveController>().SetUpCurveMove(_player.transform, speedBulletMove, 1, damage: _myDetail.CurrentATKDamage);

            yield return new WaitForSeconds(SpeedShoot);
            temp = Instantiate(bullet, pos, transform.rotation, _container); // bắn đạn
            temp.transform.eulerAngles = new Vector3(0, angleYRight -= angle, 0);
            temp.gameObject.SetActive(true);

            //setup bullet
            if (temp.gameObject.GetComponent<BulletMoveController>() != null)
                temp.gameObject.GetComponent<BulletMoveController>().SetUpCurveMove(_player.transform, speedBulletMove, 1, damage: _myDetail.CurrentATKDamage);

            yield return new WaitForSeconds(SpeedShoot);
        }
    }
    #region Cannon
    IEnumerator CannonShoot(Vector3 target_, int numberShoot = 1, float distance = 0)
    {
        for (int i = 0; i < numberShoot; i++)
        {
            Vector3 targetPos = VelocityBulletOfCannon(target_, PosBullet.position, TimeMove, OffSetY);

            // one shoot
            Rigidbody t = Instantiate(bullet, PosBullet.position, Quaternion.identity).GetComponent<Rigidbody>();
            t.gameObject.SetActive(true);
            t.gameObject.GetComponent<Collider>().isTrigger = false;

            // setup 
            if (t.gameObject.GetComponent<BulletDetail>() != null)
                t.gameObject.GetComponent<BulletDetail>().SetUpBullet(_myDetail.CurrentATKDamage, _myDetail.RaceType);

            if (t.gameObject.GetComponent<BulletCuaController>() != null)
                t.gameObject.GetComponent<BulletCuaController>().enabled = false;

            if (t.GetComponent<MonsterDetail>() != null)
                t.GetComponent<MonsterDetail>().isDropItem = false;

            //SetUpBullet(ref t);

            t.useGravity = true;
            t.velocity = targetPos;

            yield return new WaitForSeconds(SpeedShoot);
        }
    }
    IEnumerator CannonShootAround(Vector3 origin, int numberShoot = 8, float distance = 2)
    {
        Vector3 posTarget = PosBullet.position + new Vector3(distance, distance, distance);
        //PosBullet.GetChild(0).position = posTarget;
        float angle = Angle / numberShoot;
        Vector3 angleDefault = PosBullet.rotation.eulerAngles;
        //PosBullet.transform.eulerAngles = new Vector3(0, 45, 0);
        float angleYLeft = PosBullet.rotation.eulerAngles.y;

        Rigidbody t;
        for (int i = 0; i < numberShoot; i++)
        {
            Vector3 velocity = VelocityBulletOfCannon(PosBullet.TransformPoint(posTarget * 0.2f), PosBullet.position, TimeMove, OffSetY);

            t = Instantiate(bullet, PosBullet.position, PosBullet.rotation, _container).GetComponent<Rigidbody>();
            PosBullet.transform.eulerAngles = new Vector3(0, angleYLeft += angle, 0);
            
            // setup
            if (t.gameObject.GetComponent<BulletDetail>() != null)
                t.gameObject.GetComponent<BulletDetail>().SetUpBullet(_myDetail.CurrentATKDamage, _myDetail.RaceType);

            if (t.gameObject.GetComponent<BulletCuaController>() != null)
                t.gameObject.GetComponent<BulletCuaController>().enabled = false;

            if (t.GetComponent<MonsterDetail>() != null)
                t.GetComponent<MonsterDetail>().isDropItem = false;

            t.gameObject.SetActive(true);
            t.gameObject.GetComponent<Collider>().isTrigger = false;

            t.useGravity = true;
            t.velocity = velocity;
            yield return new WaitForSeconds(SpeedShoot);
        }

        PosBullet.transform.eulerAngles = angleDefault;
        yield return null;
    }

    Vector3 VelocityBulletOfCannon(Vector3 target, Vector3 origin, float time, float offsetY = 0)
    {
        Vector3 dir = target - origin;
        Vector3 dirXZ = dir;
        dirXZ.y = 0;

        float Sy = dir.y + offsetY;
        float Sxz = dirXZ.magnitude;

        float Vxz = Sxz / time;
        float Vy = Sy / time + 0.5f * Mathf.Abs(Physics.gravity.y) * time;

        Vector3 result = dirXZ.normalized;
        result *= Vxz;
        result.y = Vy;

        return result;
    }
    #endregion
    private void Update()
    {
        //if (GameManager.api.GameState == GAME_STATE.Playing)
        //{
        //    bool canUseSkill = /*!_skillController.Skilling &&*/ _isReady /*&& _myController.Hit == false*/;
        //    if (canUseSkill) // khoong cos skill nao dang hoat dong va da san sang
        //    {
        //        AddFreeSkillToSkillController();
        //    }
        //}
#if EDITOR
        if (Shoot)
        {
            Shoot = false;
            StartCoroutine(SkillStart());
        }
#endif
    }
    private Action _skillStart;
    private void AddFreeSkillToSkillController()
    {
        if (DependentOtherSkill == true) return;
        _skillStart = () =>
        {
            StartCoroutine(SkillStart());
        };
        _skillController.AddFreeSkill(_skillStart, SkillPriority);
    }

}
