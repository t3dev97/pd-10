﻿using UnityEngine;

[RequireComponent(typeof(ShootSkill))]
public class MoveLeftRight : MonoBehaviour
{
    [SerializeField]
    float _distanceAllowShoot;

    [SerializeField]
    float _speedMove;

    [SerializeField]
    float _speedShoot;

    Transform _player;
    Vector3 _posTarget;
    float _distance;
    MonsterDetail _myDetail;
    bool _playerMoving = false;
    float _timeLive;
    ShootSkill _shootSkill;

    Animator _anim;
    Animation _animation;
    string[] _nameAnim = new string[] { "Idle", "Run", "Atk", "Hit", "Die" }; // các trạng thái aniamtino nhân vật 
    private void Awake()
    {
        _anim = GetComponent<Animator>();
        _myDetail = GetComponent<MonsterDetail>();
        _shootSkill = GetComponent<ShootSkill>();
    }
    private void OnEnable()
    {
        PlayerController.PlayerMoving += CheckPlayerMoving;
    }
    private void OnDisable()
    {
        PlayerController.PlayerMoving -= CheckPlayerMoving;
    }
    private void Start()
    {
        _player = PlayerDetail.api.transform;
        _shootSkill.DependentOtherSkill = true;
    }
    void CheckPlayerMoving(bool v)
    {
        _playerMoving = v;
        if (_playerMoving)
        {
            //Move();
        }
    }
    void Move()
    {
        if (_player == null) return;
        _posTarget.x = _player.transform.position.x;
        _posTarget.z = transform.position.z;
        _posTarget.y = transform.position.y;
        _distance = Vector3.Distance(transform.position, _posTarget);

        float timeMove = _distance / _speedMove;
        RunAnimation("Run");
        transform.position = Vector3.Lerp(transform.position, _posTarget, _speedMove * Time.smoothDeltaTime);

        if (_distance < _distanceAllowShoot)
        {
            _timeLive += Time.deltaTime;
            if (_timeLive > _speedShoot)
            {
                _timeLive = 0;
                StartCoroutine(GetComponent<ShootSkill>().SkillStart());
            }
        }
    }
    private void Update()
    {
        if (_myDetail.CurrentHP > 0 && GameManager.api.GameState == GAME_STATE.Playing)
            Move();
    }
    Vector3 PosRandom(float _distance)
    {
        Vector3 t;
        t.y = transform.position.y;
        t.x = UnityEngine.Random.Range(-BC_DataGame.api.WidthMap * 1.0f / 2 + 1, BC_DataGame.api.WidthMap * 1.0f / 2 - 1);
        t.z = UnityEngine.Random.Range(-BC_DataGame.api.HeightMap * 1.0f / 2 + 1, BC_DataGame.api.HeightMap * 1.0f / 2 - 1);

        t.x = Mathf.Clamp(t.x + _distance, -BC_DataGame.api.WidthMap * 1.0f / 2 + 1, BC_DataGame.api.WidthMap * 1.0f / 2 - 1);
        t.z = Mathf.Clamp(t.z + _distance, -BC_DataGame.api.HeightMap * 1.0f / 2 + 1, BC_DataGame.api.HeightMap * 1.0f / 2 - 1);

        return t;
    }
    void RunAnimation(string name)
    {
        foreach (var item in _nameAnim)
        {
            _anim.SetBool(item, false);
        }
        _anim.SetBool(name, true);
    }


}
