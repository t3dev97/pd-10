﻿using Studio.BC;
using System.Collections;
using UnityEngine;
public class MonsterSkill_V1 : MonoBehaviour
{
    public EnumShootSkill ShotType;
    public float TimeDelay = 1;
    public EnumBulletType BulletType;
    public bool FollowPlayer = false;// cho phép đạn dí theo
    public int NumberBullet;
    public int Angle;
    public int OffsetAngle;
    public GameObject bullet;
    public Transform PosBullet;

    public int NumberShoot;


    Rigidbody _rb;
    Transform _player;
    MonsterController _myControl;
    MonsterDetail _myDetail;
    Animator _amin;
    Transform _contaner;
    MonsterMove_V2 _MoveSkill;

    bool _doneATK = true;
    private void Awake()
    {
        _amin = GetComponent<Animator>();
        _myControl = GetComponent<MonsterController>();
        _myDetail = GetComponent<MonsterDetail>();
        _rb = GetComponent<Rigidbody>();
        _MoveSkill = GetComponent<MonsterMove_V2>();
    }

    private void Start()
    {
        _player = GameObject.FindGameObjectWithTag(TagManager.api.Player).transform;
        _contaner = GameObject.FindObjectOfType<BCCache>().transform;
    }
    float _timeStart = 0;
    private void Update()
    {
        if (_myControl.myAction == false) return;

        if (_myControl.CheckAtk) // 1: thời gian thực hiện skill
        {
            _doneATK = false;
            _rb.velocity = Vector3.zero;
            if (ShotType != EnumShootSkill.Khong)
                RunAnimation("Atk");
            else
            {
                DoneATK();
            }
            //switch (ShotType)
            //{
            //    case EnumShotSkill.Khong:
            //        {
            //            StartCoroutine(Delay());
            //            break;
            //        }
            //    case EnumShotSkill.BanThang:
            //        {
            //            DanVongCung(PosBullet, _myControl.FollowPlayer, NumberBullet, Angle);
            //            break;
            //        }
            //    case EnumShotSkill.VongCung:
            //        {
            //            DanVongCung(PosBullet, _myControl.FollowPlayer, NumberBullet, Angle);
            //            break;
            //        }
            //    case EnumShotSkill.DanPhao:
            //        {
            //            Phao();
            //            break;
            //        }
            //    case EnumShotSkill.XoeQuat:
            //        {
            //            StartCoroutine(DanQuat(PosBullet, _myControl.FollowPlayer, 7, 180));
            //            break;
            //        }
            //    case EnumShotSkill.Ziczac:
            //        {
            //            break;
            //        }
            //    case EnumShotSkill.XoayTron:
            //        {
            //            break;
            //        }
            //}
        }
    }
    IEnumerator Delay()
    {
        yield return new WaitForSeconds(TimeDelay);
        _myControl.CheckAtk = false;
        _myControl.TimeStartATK = Time.time;
        //Debug.Log("_myControl.CheckAtk: "+ _myControl.CheckAtk);
    }
    void Phao()
    {
        //RunAnimation("Atk");
        //Debug.Log("Shot Phao");
        Vector3 targetPos = DanPhao(_player.position, PosBullet.position, 2);

        var lookDir = _player.position - transform.position;
        lookDir.y = 0; // keep only the horizontal direction
        transform.rotation = Quaternion.LookRotation(lookDir);

        //PosBullet.LookAt(targetPos);
        Rigidbody obj = Instantiate(bullet, PosBullet.position, Quaternion.identity).GetComponent<Rigidbody>();
        obj.gameObject.GetComponent<BulletDetail>().BulletType = BulletType;
        obj.gameObject.GetComponent<Collider>().isTrigger = false;
        obj.gameObject.GetComponent<BulletCuaController>().enabled = false;
        obj.useGravity = true;
        obj.velocity = targetPos;

        _myControl.CheckAtk = false;
        _myControl.TimeStartATK = Time.time;
    }

    Vector3 DanPhao(Vector3 target, Vector3 origin, float time)
    {
        Vector3 dir = target - origin;
        Vector3 dirXZ = dir;
        dirXZ.y = 0;

        float Sy = dir.y;
        float Sxz = dirXZ.magnitude;

        float Vxz = Sxz / time;
        float Vy = Sy / time + 0.5f * Mathf.Abs(Physics.gravity.y) * time;

        Vector3 result = dirXZ.normalized;
        result *= Vxz;
        result.y = Vy;

        return result;

    }


    Vector3 DanPhaoTheoThoiGian(Vector3 pos, Vector3 origin, float time)
    {
        Vector3 Vxz = pos;
        pos.y = 0;

        Vector3 result = origin + pos * time;

        float sY = (-0.5f * Mathf.Abs(Physics.gravity.y) * (time * time)) + (pos.y * time) + origin.y;
        result.y = sY;
        return result;
    }

    void ShotSkill(Transform originBulletTrans, int numberBullet) // thêm 2 tia phía trước
    {
        //RunAnimation("Atk");
        GameObject temp;
        Vector3 pos = PosBullet.position;
        float angle = 360 / numberBullet;
        float angleY = originBulletTrans.rotation.eulerAngles.y;
        for (int i = 0; i < numberBullet; i++)
        {
            temp = Instantiate(bullet, pos, transform.rotation, _contaner); // bắn đạn
            temp.transform.eulerAngles = new Vector3(0, angleY += angle, 0);
            temp.GetComponent<BulletDetail>().FollowPlayer = FollowPlayer;
            //temp.GetComponent<BulletCuaController>().speedMove = _myDetail.BasicASRangeUnit;
        }
        //_myControl.CheckAtk = false;
        //_myControl.TimeStartATK = Time.time;
    }
    IEnumerator DanQuat(Transform originBulletTrans, bool lookPlayer = false, int numberBullet = 8, float _angle = 360, float offset = 0) // thêm 2 tia phía trước
    {
        //RunAnimation("Atk");
        GameObject temp;
        Vector3 pos = PosBullet.position; // vị trí sinh đạn
        //if (lookPlayer)
        //    transform.LookAt(_player.transform);

        float angle = _angle / numberBullet;
        float angleYLeft = originBulletTrans.rotation.eulerAngles.y + offset;

        // Viên đạn đầu tiên
        for (int i = 0; i < numberBullet; i++)
        {
            temp = Instantiate(bullet, pos, transform.rotation, _contaner); // bắn đạn
            temp.transform.eulerAngles = new Vector3(0, angleYLeft += angle, 0);
            temp.GetComponent<BulletDetail>().FollowPlayer = FollowPlayer;
            LeanTween.value(0, numberBullet, 2);
            //temp.GetComponent<BulletCuaController>().speedMove = _myDetail.BasicASRangeUnit;
        }
        //_myControl.CheckAtk = false;
        //_myControl.TimeStartATK = Time.time;
        yield return null;
    }
    void DanVongCung(Transform originBulletTrans, bool lookPlayer = false, int numberBullet = 8, float _angle = 360, float offset = 0) // thêm 2 tia phía trước
    {
        //RunAnimation("Atk");
        GameObject temp;
        Vector3 pos = PosBullet.position; // vị trí sinh đạn
        //if (lookPlayer)
        //    transform.LookAt(_player.transform);

        float angle = _angle / numberBullet;
        float angleYLeft = originBulletTrans.rotation.eulerAngles.y + offset;
        float angleYRight = originBulletTrans.rotation.eulerAngles.y + offset;

        // Viên đạn đầu tiên
        temp = Instantiate(bullet, pos, transform.rotation, _contaner); // bắn đạn
        temp.transform.eulerAngles = new Vector3(0, angleYLeft, 0);
        temp.GetComponent<BulletDetail>().FollowPlayer = FollowPlayer;
        temp.gameObject.GetComponent<BulletDetail>().BulletType = BulletType;
        //temp.GetComponent<BulletCuaController>().speedMove = _myDetail.BasicASRangeUnit;
        for (int i = 0; i < numberBullet / 2; i++)
        {
            temp = Instantiate(bullet, pos, transform.rotation, _contaner); // bắn đạn
            temp.transform.eulerAngles = new Vector3(0, angleYLeft += angle, 0);
            temp.GetComponent<BulletDetail>().FollowPlayer = FollowPlayer;
            temp.gameObject.GetComponent<BulletDetail>().BulletType = BulletType;
            //temp.GetComponent<BulletCuaController>().speedMove = _myDetail.BasicASRangeUnit;

            temp = Instantiate(bullet, pos, transform.rotation, _contaner); // bắn đạn
            temp.transform.eulerAngles = new Vector3(0, angleYRight -= angle, 0);
            temp.GetComponent<BulletDetail>().FollowPlayer = FollowPlayer;
            temp.gameObject.GetComponent<BulletDetail>().BulletType = BulletType;
            //temp.GetComponent<BulletCuaController>().speedMove = _myDetail.BasicASRangeUnit;
        }
        //_myControl.CheckAtk = false;
        //_myControl.TimeStartATK = Time.time;
    }
    void ShotFrontArow(Transform originBulletTrans, int numberBullet = 2, float distance = 1, float xOffset = 0) // thêm 2 tia phía trước
    {
        //RunAnimation("Atk");
        GameObject temp;
        Vector3 pos = PosBullet.position;
        float angle = distance / numberBullet;
        float angleYLeft = originBulletTrans.position.x + xOffset;
        float angleYRight = originBulletTrans.position.x + xOffset;

        // Viên đạn đầu tiên
        if (numberBullet > 2)
        {
            temp = Instantiate(bullet, pos, transform.rotation, _contaner); // bắn đạn
            temp.transform.localPosition = new Vector3(angleYLeft, transform.position.y, transform.position.z);
            temp.GetComponent<BulletDetail>().FollowPlayer = FollowPlayer;
            temp.gameObject.GetComponent<BulletDetail>().BulletType = BulletType;
            //temp.GetComponent<BulletCuaController>().speedMove = _myDetail.BasicASRangeUnit;
        }
        for (int i = 0; i < numberBullet / 2; i++)
        {
            temp = Instantiate(bullet, pos, transform.rotation, _contaner); // bắn đạn
            temp.transform.localPosition = new Vector3(angleYLeft += angle, transform.position.y, transform.position.z);
            temp.GetComponent<BulletDetail>().FollowPlayer = FollowPlayer;
            temp.gameObject.GetComponent<BulletDetail>().BulletType = BulletType;
            //temp.GetComponent<BulletCuaController>().speedMove = _myDetail.BasicASRangeUnit;

            temp = Instantiate(bullet, pos, transform.rotation, _contaner); // bắn đạn
            temp.transform.localPosition = new Vector3(angleYRight -= angle, transform.position.y, transform.position.z);
            temp.GetComponent<BulletDetail>().FollowPlayer = FollowPlayer;
            temp.gameObject.GetComponent<BulletDetail>().BulletType = BulletType;
            //temp.GetComponent<BulletCuaController>().speedMove = _myDetail.BasicASRangeUnit;
        }
        //_myControl.CheckAtk = false;
        //_myControl.TimeStartATK = Time.time;
    }

    string[] _nameAmin = new string[] { "Idle", "Run", "Atk", "Hit", "Die" }; // các trạng thái aniamtino nhân vật 
    void RunAnimation(string name)
    {
        foreach (var item in _nameAmin)
        {
            _amin.SetBool(item, false);
        }
        _amin.SetBool(name, true);
    }

    void DoneATK() // được gọi ở cuối animation ATK
    {
        switch (ShotType)
        {
            case EnumShootSkill.Khong:
                {
                    StartCoroutine(Delay());
                    break;
                }
            case EnumShootSkill.BanThang:
                {
                    DanVongCung(PosBullet, _myControl.FollowPlayer, NumberBullet, Angle);
                    break;
                }
            case EnumShootSkill.VongCung:
                {
                    DanVongCung(PosBullet, _myControl.FollowPlayer, NumberBullet, Angle);
                    break;
                }
            case EnumShootSkill.DanPhao:
                {
                    Phao();
                    break;
                }
            case EnumShootSkill.Boomerang:
                {
                    StartCoroutine(DanQuat(PosBullet, _myControl.FollowPlayer, 7, 180));
                    break;
                }
            case EnumShootSkill.Ziczac:
                {
                    break;
                }
            case EnumShootSkill.XoayTron:
                {
                    break;
                }
        }
        _myControl.CheckAtk = false;
        _myControl.TimeStartATK = Time.time;
    }


}
