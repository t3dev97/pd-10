﻿using System.Collections;
using UnityEngine;

public class NhayTaoDanSkill : SkillBase
{
    MonsterSkillController _skillController;
    MonsterDetail _myDetail;
    MonsterController_new _myController;
    ShootSkill _shootSkill;
    Rigidbody _rb;

    Vector3 _dir = Vector3.zero;

    public Transform Target;
    public float TimDelayATK = 0.5f;
    public bool AllowMoveWhenATK = false;



    Vector3 _posTarget;
    float SpeeDefault;
    private void Awake()
    {
        _skillController = GetComponent<MonsterSkillController>();
        _myDetail = GetComponent<MonsterDetail>();
        _myController = GetComponent<MonsterController_new>();
        _rb = GetComponent<Rigidbody>();
        _shootSkill = GetComponent<ShootSkill>();
    }
    private void Start()
    {
        Target = GameObject.FindGameObjectWithTag(TagManager.api.Player).transform;
        SpeeDefault = _myDetail.CurrentSpeedMove;
    }

    public override IEnumerator SkillStart()
    {
        _skillController.Skilling = true;
        _skillTimeLive = 0;
        _isDoneSkill = false;
        _isReady = false;
        //Debug.Log("Chuẩn bị tấn công");
        if (!string.IsNullOrEmpty(StartAnimStr))
            _skillController.RunAnimation(StartAnimStr);

        yield return new WaitForSeconds(TimeOfStartAnim);
        _rb.velocity = Vector3.zero;
        StartCoroutine(SkillIn());
    }
    protected override IEnumerator SkillIn()
    {

        Debug.Log("Đang thực hiện tấn công");
        if (!string.IsNullOrEmpty(InAnimStr))
            _skillController.RunAnimation(InAnimStr);

        _rb.AddForce(Vector3.up * 200);
        yield return new WaitForSeconds(TimeOfInAnim / 2);
        _rb.AddForce(Vector3.down * 500);

        yield return new WaitForSeconds(TimeOfInAnim / 2);

        _skillController.RunAnimation(EnumAnimName.Idle.ToString());
        yield return new WaitForSeconds(0.5f);

        if (_loop < TotalLoop)
        {
            _loop++;
            StartCoroutine(SkillIn());
        }
        else
            StartCoroutine(SkillEnd());
    }

    void MoveTo()
    {
        if (AllowMoveWhenATK)
            _rb.velocity = transform.forward * _myDetail.CurrentSpeedMove;
    }

    void MoveToTarget(Vector3 posTarget)
    {

    }


    protected override IEnumerator SkillEnd()
    {
        _shootSkill.OneShootTime();
        Debug.Log("Skill End");
        _rb.velocity = Vector3.zero;
        _skillController.ResetAnimation("");
        //Debug.Log("Kết thúc tấn công");
        if (!string.IsNullOrEmpty(EndAnimStr))
            _skillController.RunAnimation(EndAnimStr);
        else
            _skillController.RunAnimation("Idle");

        yield return new WaitForSeconds(TimeOfEndAnim);
        _isDoneSkill = true;
        _loop = 0;
        _skillController.RunAnimation(EnumAnimName.Idle.ToString());
        _skillController.Skilling = false;
        _myDetail.CurrentSpeedMove = SpeeDefault;
    }



    private void Update()
    {
        bool canUseSkill = !_skillController.Skilling && _isReady /*&& _myController.Hit == false*/;

        if (canUseSkill) // khoong cos skill nao dang hoat dong va da san sang
        {
            _rb.velocity = Vector3.zero;
            StartCoroutine(SkillStart());
        }


        if (_isDoneSkill)
        {
            _skillTimeLive += Time.deltaTime;
            if (_skillTimeLive > TimeCooldown)
                _isReady = true;
        }
    }

    void JumpTo(Vector3 target, Vector3 current, float timeMove)
    {
        //RunAnimation("Atk");
        //Debug.Log("Shot Phao");
        Vector3 targetPos = CalculateVelocity(target, current, timeMove);

        _rb.velocity = targetPos;
    }

    Vector3 CalculateVelocity(Vector3 target, Vector3 origin, float time)
    {
        Vector3 dir = target - origin;
        Vector3 dirXZ = dir;
        dirXZ.y = 0;

        float Sy = dir.y;
        float Sxz = dirXZ.magnitude;

        float Vxz = Sxz / time;
        float Vy = Sy / time + 0.5f * Mathf.Abs(Physics.gravity.y) * time;

        Vector3 result = dirXZ.normalized;
        result *= Vxz;
        result.y = Vy;

        return result;
    }

}
