﻿using System;
using System.Collections;
using UnityEngine;

public class MoveAndDropBomb : SkillBase
{
    [Header("Setting Move")]
    public float SpeedMove;

    [Header("Shoot Info")]
    public GameObject bullet;

    [HideInInspector]
    public EnumShootSkill ShootType;
    //public int NumberShoot;
    public float SpeedShoot;

    [Header("Bullet Info")]
    public Transform PosBullet;
    public float Angle;
    public bool RandomAngle = false;
    public float OffsetAngle;
    public EnumBulletType BulletType;
    public float TimeBulletMove = 2;
    public float OffSetY = 0;

    MonsterSkillController _skillController;
    MonsterDetail _myDetail;
    MonsterController_new _myController;
    Transform _container;
    GameObject _player;
    Rigidbody _rb;
    Vector3 _posTarget;
    private void Awake()
    {
        if (GetComponent<MonsterSkillController>() == null)
            gameObject.AddComponent<MonsterSkillController>();

        _skillController = GetComponent<MonsterSkillController>();

        if (GetComponent<Rigidbody>() != null)
            _rb = GetComponent<Rigidbody>();

        _myDetail = GetComponent<MonsterDetail>();
    }
    private void Start()
    {
        TimeOfInAnim = SpeedMove;
        AddFreeSkillToSkillController();
    }

    public override IEnumerator SkillStart()
    {
        // setup
        if (_rb != null)
            _rb.velocity = Vector3.zero;

        _posTarget = PlayerController.api.transform.position;
        _posTarget.y = transform.position.y;

        _skillController.Skilling = true;
        _skillTimeLive = 0;
        _isDoneSkill = false;
        _isReady = false;

        // run animation
        if (!string.IsNullOrEmpty(StartAnimStr))
            _skillController.RunAnimation(StartAnimStr);

        yield return new WaitForSeconds(TimeOfStartAnim);

        transform.LookAt(_posTarget);

        StartCoroutine(SkillIn());
    }

    protected override IEnumerator SkillIn()
    {
        if (!string.IsNullOrEmpty(InAnimStr))
            _skillController.Amin.Play(InAnimStr);

        MoveToTarget();

        yield return new WaitForSeconds(TimeOfInAnim);
        if (_loop < TotalLoop)
        {
            _loop++;
            StartCoroutine("SkillIn");
        }
        else
            StartCoroutine(SkillEnd());
    }

    protected override IEnumerator SkillEnd()
    {
        if (!string.IsNullOrEmpty(EndAnimStr))
            _skillController.RunAnimation(EndAnimStr);

        yield return new WaitForSeconds(TimeOfEndAnim);
        _skillController.RunAnimation(EnumAnimName.Idle.ToString());
        _isDoneSkill = true;
        _isReady = true;
        _loop = 0;
        _skillController.Skilling = false;
        AddFreeSkillToSkillController();
    }

    void MoveToTarget()
    {
        StartCoroutine(CannonShootAround(PosBullet.position, 1, 0));
        LeanTween.move(gameObject, _posTarget - new Vector3(1, 0, 1), SpeedMove).setOnUpdate((float v) =>
        {
            Debug.Log(v);
        }).setOnComplete(() =>
        {
            allowDropBomb = false;
        });
        allowDropBomb = true;
    }

    bool allowDropBomb = true;
    IEnumerator CannonShootAround(Vector3 origin, int numberShoot = 8, float distance = 0)
    {
        Vector3 posTarget = PosBullet.position + new Vector3(distance, distance, distance);
        PosBullet.GetChild(0).position = posTarget;
        float angle = Angle / numberShoot;

        PosBullet.transform.eulerAngles = new Vector3(0, 45, 0);
        float angleYLeft = PosBullet.rotation.eulerAngles.y;

        Rigidbody obj;
        //for (int i = 0; i < numberShoot; i++)
        //{
            Vector3 velocity = VelocityBulletOfCannon(PosBullet.TransformPoint(posTarget * 0.2f), PosBullet.position, TimeBulletMove, OffSetY);

            obj = Instantiate(bullet, PosBullet.position, PosBullet.rotation, _container).GetComponent<Rigidbody>();
            PosBullet.transform.eulerAngles = new Vector3(0, angleYLeft += angle, 0);

            if (obj.gameObject.GetComponent<BulletDetail>() != null)
                obj.gameObject.GetComponent<BulletDetail>().SetUpBullet(_myDetail.CurrentATKDamage, _myDetail.RaceType);
            if (obj.gameObject.GetComponent<BulletCuaController>() != null)
                obj.gameObject.GetComponent<BulletCuaController>().enabled = false;

            obj.gameObject.SetActive(true);
            obj.gameObject.GetComponent<Collider>().isTrigger = false;

            obj.useGravity = true;
            obj.velocity = velocity;
            //yield return new WaitForSeconds(SpeedShoot);
        //}
        yield return new WaitForSeconds(SpeedShoot);
        if (allowDropBomb)
            StartCoroutine(CannonShootAround(PosBullet.position, 1, 0));
    }

    Vector3 VelocityBulletOfCannon(Vector3 target, Vector3 origin, float time, float offsetY = 0)
    {
        Vector3 dir = target - origin;
        Vector3 dirXZ = dir;
        dirXZ.y = 0;

        float Sy = dir.y + offsetY;
        float Sxz = dirXZ.magnitude;

        float Vxz = Sxz / time;
        float Vy = Sy / time + 0.5f * Mathf.Abs(Physics.gravity.y) * time;

        Vector3 result = dirXZ.normalized;
        result *= Vxz;
        result.y = Vy;

        return result;
    }
    private Action _skillStart;
    private void AddFreeSkillToSkillController()
    {
        _skillStart = () =>
        {
            StartCoroutine(SkillStart());
        };
        _skillController.AddFreeSkill(_skillStart, SkillPriority);
    }
}
