﻿using Studio.BC;
using UnityEngine;

public class SpecialSkill : MonoBehaviour
{
    public EnumSpecialSkill SpecialSkillType;

    [Header("Rai Doc")]
    public GameObject PoisonPrefab;
    public float TimeIns;
    public float TimeLife;
    public float Range;
    public Vector3 PosCreate;

    [Header("Goi Them Nhan Ban")]
    public GameObject[] ListLinh;
    public float TimeCreate;
    public int MaxClone = 4;

    MonsterDetail _myDetail;
    MonsterController_new _myController;
    Transform _container;

    GameObject[] listClone;
    private void Awake()
    {
        _myDetail = GetComponent<MonsterDetail>();
        _myController = GetComponent<MonsterController_new>();
        _container = BCCache.Api.transform;
    }
    private void Start()
    {
        listClone = new GameObject[MaxClone];
    }
    private void Update()
    {
        if (GameManager.api.GameState == GAME_STATE.Playing)
        {
            switch (SpecialSkillType)
            {
                case EnumSpecialSkill.None:
                    {
                        break;
                    }
                case EnumSpecialSkill.ChetPhanThan:
                    {
                        // được gọi trong hàm OnDisable
                        break;
                    }
                case EnumSpecialSkill.GoiThemNhanBan:
                    {
                        CreateDouble();
                        break;
                    }
                case EnumSpecialSkill.RaiDoc:
                    {
                        CreatePoison();
                        break;
                    }
            }
        }
    }
    float timerDouble = 0;
    int numberClone = 0;
    void CreateDouble()
    {
        timerDouble += Time.deltaTime;

        if (timerDouble > TimeCreate)
        {
            int index = -1;
            for (int i = 0; i < MaxClone; i++)
            {
                if (listClone[i] == null)
                    index = i;
            }

            if (index > 0)
            {
                Vector3 pos = transform.position + Vector3.up;
                GameObject obj = Instantiate(ListLinh[Random.Range(0, ListLinh.Length)], pos, Quaternion.identity);
                listClone[index] = obj;
                BC_Chapter_Info.api.AddListStageMonster(obj);

                timerDouble = 0;
            }
        }
    }
    float timerPoison;
    void CreatePoison()
    {
        timerPoison += Time.fixedDeltaTime;
        if (timerPoison > TimeIns && !_myController.CheckDie)
        {
            TimeIns = 0.4f;
            GameObject Poison = Instantiate(PoisonPrefab, transform.position + PosCreate, Quaternion.identity, _container);
            Poison.GetComponent<PoisonController>().SetUp(Range, _myDetail.CurrentATKDamage, TimeLife);
            timerPoison = 0;
        }
    }
    private void OnDisable()
    {
        if (SpecialSkillType == EnumSpecialSkill.ChetPhanThan)
        {
            timerDouble += TimeCreate;
            CreateDouble();
        }
    }

}
