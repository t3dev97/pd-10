﻿using Studio.BC;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreeSkillData
{
    public Action SkillAction;
    public int Priority;

    public FreeSkillData(Action skillAction, int priority)
    {
        SkillAction = skillAction;
        Priority = priority;
    }
}
public class MonsterSkillController : MonoBehaviour
{
    public bool isAttack = true;

    [HideInInspector]
    public EnumShootSkill ShotType;

    [HideInInspector]
    public float TimeDelay = 1;
    public bool AllowMoveWhenSkilling = false;

    [HideInInspector]
    public EnumBulletType BulletType;

    [HideInInspector]
    public bool FollowPlayer = false;// cho phép đạn dí theo

    [HideInInspector]
    public int NumberBullet;

    [HideInInspector]
    public int Angle;

    [HideInInspector]
    public int OffsetAngle;

    [HideInInspector]
    public GameObject bullet;

    [HideInInspector]
    public Transform PosBullet;

    [HideInInspector]
    public MonsterController_new MyControl;

    [HideInInspector]
    public Transform PlayerObj;

    [HideInInspector]
    public Animator Amin;


    [HideInInspector]
    SkillBase[] _list;

    [SerializeField]
    MonsterDetail _myDetail;

    [SerializeField]
    bool _skilling = false; // có 1 skill đang thực hiện

    [SerializeField]
    float _timelive = 0;

    [SerializeField]
    float _currentTimeCoolDown;

    [SerializeField]
    [Tooltip("Đối với Ma dù")]
    bool _conditionInGround = false;

    MonsterController_new _myController;
    Transform _contaner;
    MonsterMove_V2 _MoveSkill;
    CheckInGround _inGround;
    List<FreeSkillData> _freeSkills;
    bool _doSkill = true;
    bool _doneATK = true;
    bool doneSetUp = false;
    string[] _nameAmin = new string[] { "Idle", "Run", "PreAtk", "Atk", "EndAtk", "Hit", "Die", "Roll" }; // các trạng thái aniamtino nhân vật 

    [SerializeField]
    int _freeSkillNumber;

    [SerializeField]
    [Tooltip("Không quan tâm đến độ ưu tiên")]
    bool skillRandom = false;

    public List<FreeSkillData> freeSkills
    {
        get
        {
            if (_freeSkills == null)
                _freeSkills = new List<FreeSkillData>();
            return _freeSkills;
        }
    }
    public void AddFreeSkill(Action action, int priority = 0)
    {
        //var dupplicate = freeSkills.Find(x => x.SkillAction.Method == action.Method);
        //if (dupplicate == null)
        //{
        freeSkills.Add(new FreeSkillData(action, priority));
        //}
    }
    private void CheckFreeSkills()
    {
        if (BC_Chapter_Info.api.AllowMonsterAtk == false) return;
        if (freeSkills != null && freeSkills.Count > 0)
        {
            FreeSkillData currentSkill;
            if (skillRandom)
            {
                currentSkill = freeSkills[UnityEngine.Random.Range(0, freeSkills.Count)];
            }
            else
            {
                currentSkill = freeSkills[0];
                foreach (FreeSkillData freeSkill in freeSkills)
                {
                    if (freeSkill.Priority < currentSkill.Priority)
                        currentSkill = freeSkill;
                }
            }
            if (currentSkill != null)
                currentSkill.SkillAction.Invoke();
            freeSkills.Remove(currentSkill);
        }
    }
    private void Awake()
    {
        Amin = GetComponent<Animator>();
        MyControl = GetComponent<MonsterController_new>();
        _myDetail = GetComponent<MonsterDetail>();
        _MoveSkill = GetComponent<MonsterMove_V2>();
        _myController = GetComponent<MonsterController_new>();
        _list = GetComponents<SkillBase>();
        _inGround = GetComponentInChildren<CheckInGround>();
    }
    private void Start()
    {
        StartCoroutine(SetupData());
    }
    IEnumerator SetupData()
    {
        yield return new WaitForSeconds(0.5f);
        if (BC_DataGame.api != null)
            PlayerObj = BC_DataGame.api.PlayerObj.transform;
        else
            PlayerObj = GameObject.FindGameObjectWithTag(TagManager.api.Player).transform;
        _contaner = GameObject.FindObjectOfType<BCCache>().transform;
        doneSetUp = true;
    }
    public bool Skilling
    {
        get
        {
            if (GameManager.api.GameState == GAME_STATE.Playing)
                return _skilling;
            return true;
        }
        set => _skilling = value;
    }
    private void Update()
    {
        _freeSkillNumber = freeSkills.Count;
        if (GameManager.api.GameState == GAME_STATE.Playing)
        {
            if (doneSetUp == false || PlayerObj.gameObject == null || PlayerObj.gameObject.GetComponent<PlayerDetail>().HP < 0) return;

            if (Skilling == false)
            {
                //if (_conditionInGround)
                //{
                //    if (_inGround.InGround)
                //        _timelive += Time.deltaTime;

                //}
                //else // đk thường
                //{
                _timelive += Time.deltaTime;
                //}
                if (_timelive >= _currentTimeCoolDown)
                {
                    if (_conditionInGround)
                    {
                        _doSkill = true; // cho phep thuc hien skill
                    }
                    else
                    {
                        _timelive = 0;
                        _doSkill = true; // cho phep thuc hien skill
                        _currentTimeCoolDown = UnityEngine.Random.Range(_myDetail.BasicCoolDownMin, _myDetail.BasicCoolDownMax);
                    }
                }
            }
            else
                _timelive = 0;

            DoSkill();
        }
    }
    void DoSkill()
    {
        if (_myDetail.CurrentHP > 0 && _doSkill == true)
        {
            _currentTimeCoolDown = UnityEngine.Random.Range(_myDetail.BasicCoolDownMin, _myDetail.BasicCoolDownMax);
            _doSkill = false;
#if EDITOR
                if (BCApiTest.api.isGoToTest)
                {
                    if (BCApiTest.api.isAutoAttack)
                    {
                        CheckFreeSkills();
                    }
                }
                else
#endif
            if (_conditionInGround)
            {
                if (_inGround.InGround)
                {
                    CheckFreeSkills();
                }
            }
            else
            {
                //Debug.Log(freeSkills.Count);
                CheckFreeSkills();
            }
        }
    }
    IEnumerator Delay()
    {
        yield return new WaitForSeconds(TimeDelay);
        MyControl.CheckAtk = false;
        MyControl.TimeStartATK = Time.time;
    }
    void Phao()
    {
        Vector3 targetPos = DanPhao(PlayerObj.position, PosBullet.position, 2);

        var lookDir = PlayerObj.position - transform.position;
        lookDir.y = 0; // keep only the horizontal direction
        transform.rotation = Quaternion.LookRotation(lookDir);

        Rigidbody obj = Instantiate(bullet, PosBullet.position, Quaternion.identity).GetComponent<Rigidbody>();
        obj.gameObject.GetComponent<BulletDetail>().BulletType = BulletType;
        obj.gameObject.GetComponent<Collider>().isTrigger = false;
        obj.gameObject.GetComponent<BulletCuaController>().enabled = false;
        obj.useGravity = true;
        obj.velocity = targetPos;

        MyControl.CheckAtk = false;
        MyControl.TimeStartATK = Time.time;
    }

    Vector3 DanPhao(Vector3 target, Vector3 origin, float time)
    {
        Vector3 dir = target - origin;
        Vector3 dirXZ = dir;
        dirXZ.y = 0;

        float Sy = dir.y;
        float Sxz = dirXZ.magnitude;

        float Vxz = Sxz / time;
        float Vy = Sy / time + 0.5f * Mathf.Abs(Physics.gravity.y) * time;

        Vector3 result = dirXZ.normalized;
        result *= Vxz;
        result.y = Vy;

        return result;
    }
    Vector3 DanPhaoTheoThoiGian(Vector3 pos, Vector3 origin, float time)
    {
        Vector3 Vxz = pos;
        pos.y = 0;
        Vector3 result = origin + pos * time;
        float sY = (-0.5f * Mathf.Abs(Physics.gravity.y) * (time * time)) + (pos.y * time) + origin.y;
        result.y = sY;

        return result;
    }
    void ShotSkill(Transform originBulletTrans, int numberBullet) // thêm 2 tia phía trước
    {
        GameObject temp;
        Vector3 pos = PosBullet.position;
        float angle = 360 / numberBullet;
        float angleY = originBulletTrans.rotation.eulerAngles.y;

        for (int i = 0; i < numberBullet; i++)
        {
            temp = Instantiate(bullet, pos, transform.rotation, _contaner); // bắn đạn
            temp.transform.eulerAngles = new Vector3(0, angleY += angle, 0);
            temp.GetComponent<BulletDetail>().FollowPlayer = FollowPlayer;
        }
    }
    IEnumerator DanQuat(Transform originBulletTrans, bool lookPlayer = false, int numberBullet = 8, float _angle = 360, float offset = 0) // thêm 2 tia phía trước
    {
        GameObject temp;
        Vector3 pos = PosBullet.position; // vị trí sinh đạn

        float angle = _angle / numberBullet;
        float angleYLeft = originBulletTrans.rotation.eulerAngles.y + offset;

        // Viên đạn đầu tiên
        for (int i = 0; i < numberBullet; i++)
        {
            temp = Instantiate(bullet, pos, transform.rotation, _contaner); // bắn đạn
            temp.transform.eulerAngles = new Vector3(0, angleYLeft += angle, 0);
            temp.GetComponent<BulletDetail>().FollowPlayer = FollowPlayer;
            LeanTween.value(0, numberBullet, 2);
        }
        yield return null;
    }
    void DanVongCung(Transform originBulletTrans, bool lookPlayer = false, int numberBullet = 8, float _angle = 360, float offset = 0) // thêm 2 tia phía trước
    {
        GameObject temp;
        Vector3 pos = PosBullet.position; // vị trí sinh đạn

        float angle = _angle / numberBullet;
        float angleYLeft = originBulletTrans.rotation.eulerAngles.y + offset;
        float angleYRight = originBulletTrans.rotation.eulerAngles.y + offset;

        // Viên đạn đầu tiên
        temp = Instantiate(bullet, pos, transform.rotation, _contaner); // bắn đạn
        temp.transform.eulerAngles = new Vector3(0, angleYLeft, 0);
        temp.GetComponent<BulletDetail>().FollowPlayer = FollowPlayer;
        temp.gameObject.GetComponent<BulletDetail>().BulletType = BulletType;
        for (int i = 0; i < numberBullet / 2; i++)
        {
            temp = Instantiate(bullet, pos, transform.rotation, _contaner); // bắn đạn
            temp.transform.eulerAngles = new Vector3(0, angleYLeft += angle, 0);
            temp.GetComponent<BulletDetail>().FollowPlayer = FollowPlayer;
            temp.gameObject.GetComponent<BulletDetail>().BulletType = BulletType;

            temp = Instantiate(bullet, pos, transform.rotation, _contaner); // bắn đạn
            temp.transform.eulerAngles = new Vector3(0, angleYRight -= angle, 0);
            temp.GetComponent<BulletDetail>().FollowPlayer = FollowPlayer;
            temp.gameObject.GetComponent<BulletDetail>().BulletType = BulletType;
        }
    }

    void ShotFrontArow(Transform originBulletTrans, int numberBullet = 2, float distance = 1, float xOffset = 0) // thêm 2 tia phía trước
    {
        GameObject temp;
        Vector3 pos = PosBullet.position;

        float angle = distance / numberBullet;
        float angleYLeft = originBulletTrans.position.x + xOffset;
        float angleYRight = originBulletTrans.position.x + xOffset;

        // Viên đạn đầu tiên
        if (numberBullet > 2)
        {
            temp = Instantiate(bullet, pos, transform.rotation, _contaner); // bắn đạn
            temp.transform.localPosition = new Vector3(angleYLeft, transform.position.y, transform.position.z);
            temp.GetComponent<BulletDetail>().FollowPlayer = FollowPlayer;
            temp.gameObject.GetComponent<BulletDetail>().BulletType = BulletType;
        }
        for (int i = 0; i < numberBullet / 2; i++)
        {
            temp = Instantiate(bullet, pos, transform.rotation, _contaner); // bắn đạn
            temp.transform.localPosition = new Vector3(angleYLeft += angle, transform.position.y, transform.position.z);
            temp.GetComponent<BulletDetail>().FollowPlayer = FollowPlayer;
            temp.gameObject.GetComponent<BulletDetail>().BulletType = BulletType;

            temp = Instantiate(bullet, pos, transform.rotation, _contaner); // bắn đạn
            temp.transform.localPosition = new Vector3(angleYRight -= angle, transform.position.y, transform.position.z);
            temp.GetComponent<BulletDetail>().FollowPlayer = FollowPlayer;
            temp.gameObject.GetComponent<BulletDetail>().BulletType = BulletType;
        }
    }
    string _currentAnim = "Idle";
    bool first = true;
    public void RunAnimation(string name)
    {
        //foreach (var item in _nameAmin)
        //{
        //    Amin.SetBool(item, false);
        //}
        //for (int i = 0; i < Amin.parameterCount; ++i)
        //{
        //    Debug.Log(Amin.parameters[i].name + ":" + Amin.GetInteger(Amin.parameters[i].name));
        //    Amin.SetBool(Amin.parameters[i].name, false);
        //}
        //Debug.Log("-----------------------");
        if (!first)
        {
            Amin.SetBool(_currentAnim, false);
            _currentAnim = name;
            Amin.SetBool(name, true);
        }
        else
        {
            first = false;
            Amin.SetBool(name, true);
            Amin.SetBool(_currentAnim, false);
            _currentAnim = name;
        }

    }
    public void SetSpeedAnimation(float speed)
    {
        Amin.SetFloat("Speed", speed);
    }
    public void ResetAnimation(string name)
    {
        Amin.StopPlayback();
    }
    void DoneATK() // được gọi ở cuối animation ATK
    {
        switch (ShotType)
        {
            case EnumShootSkill.Khong:
                {
                    StartCoroutine(Delay());
                    break;
                }
            case EnumShootSkill.BanThang:
                {
                    DanVongCung(PosBullet, MyControl.FollowPlayer, NumberBullet, Angle);
                    break;
                }
            case EnumShootSkill.VongCung:
                {
                    DanVongCung(PosBullet, MyControl.FollowPlayer, NumberBullet, Angle);
                    break;
                }
            case EnumShootSkill.DanPhao:
                {
                    Phao();
                    break;
                }
            case EnumShootSkill.Boomerang:
                {
                    StartCoroutine(DanQuat(PosBullet, MyControl.FollowPlayer, 7, 180));
                    break;
                }
            case EnumShootSkill.Ziczac:
                {
                    break;
                }
            case EnumShootSkill.XoayTron:
                {
                    break;
                }
        }
        MyControl.CheckAtk = false;
        MyControl.TimeStartATK = Time.time;
    }
}
