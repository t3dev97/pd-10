﻿using System.Collections;
using UnityEngine;

public class LuotVeTruocSkill : SkillBase
{
    private MonsterSkillController _mySkillController;
    private MonsterController_new _myController;
    MonsterDetail _myDetail;
    Rigidbody _rb;
    GameObject _objSub;
    public bool Sky = false;
    public bool LookAtPlayer = true;

    private void Awake()
    {
        _rb = GetComponent<Rigidbody>();
        _myDetail = GetComponent<MonsterDetail>();
        _myController = GetComponent<MonsterController_new>();
        _mySkillController = GetComponent<MonsterSkillController>();
    }
    public override IEnumerator SkillStart()
    {
        _rb.velocity = Vector3.zero;
        _mySkillController.Skilling = true;
        _skillTimeLive = 0;
        _isDoneSkill = false;
        _isReady = false;
        if (LookAtPlayer && _mySkillController.PlayerObj != null)
        {
            Vector3 target = new Vector3(_mySkillController.PlayerObj.transform.position.x, transform.position.y, _mySkillController.PlayerObj.transform.position.z);
            transform.LookAt(target);
        }

        if (!string.IsNullOrEmpty(StartAnimStr))
            _mySkillController.RunAnimation(StartAnimStr);

        yield return new WaitForSeconds(TimeOfStartAnim);
        StartCoroutine(SkillIn());
    }

    protected override IEnumerator SkillIn()
    {
        //// thực hiện skill
        //Debug.Log("Đang thực hiện tấn công");
        //var asd = transform.forward * _myDetail.SpeedMove * 2/*;*/
        //Debug.Log("transform.forward 1: " + asd);
        if (!string.IsNullOrEmpty(InAnimStr))
            _mySkillController.Amin.Play(InAnimStr);
        _rb.velocity = transform.forward * _myDetail.CurrentSpeedMove * 2;
        // kết thúc 1 lần thực hiện 
        yield return new WaitForSeconds(TimeOfInAnim);
        if (_loop < TotalLoop)
        {
            _loop++;
            StartCoroutine("SkillIn");
        }
        else
            StartCoroutine(SkillEnd());
    }

    protected override IEnumerator SkillEnd()
    {
        if (!string.IsNullOrEmpty(EndAnimStr))
            _mySkillController.RunAnimation(EndAnimStr);

        yield return new WaitForSeconds(TimeOfEndAnim);
        _mySkillController.RunAnimation(EnumAnimName.Idle.ToString());
        _isDoneSkill = true;
        _loop = 0;
        _mySkillController.Skilling = false;
    }

    private void Update()
    {
        bool canUseSkill;
        if (Sky)
            canUseSkill = !_mySkillController.Skilling && _isReady && _myController.Hit == false;
        else
            canUseSkill = !_mySkillController.Skilling && _isReady && _myController.Hit == false && _inEarth;
        Debug.Log(canUseSkill);
        if (canUseSkill) // khoong cos skill nao dang hoat dong va da san sang
        {
            StartCoroutine(SkillStart());
        }

        if (_isDoneSkill)
        {
            _skillTimeLive += Time.deltaTime;
            if (_skillTimeLive > TimeCooldown)
                _isReady = true;
        }
    }

    void BeginMoveTo() // chim canh cut
    {
        //for (int i = 0; i < 10; i++)
        //{
        //var asd = transform.forward * _myDetail.SpeedMove * 50;
        //_rb.AddForce(transform.forward * _myDetail.SpeedAtk * 500);
        //for (int i = 0; i < 10; i++)
        //{
        //    transform.Translate(transform.forward * Time.deltaTime);
        //}
        //_rb.velocity = transform.forward * _myDetail.SpeedMove * 50;
        //}
    }
    void EndMoveTo()
    {

    }

    bool _inEarth = false;
    private void OnCollisionStay(Collision collision)
    {
        if (collision.transform.tag == TagManager.api.Ground)
            _inEarth = true;
    }
    private void OnCollisionExit(Collision collision)
    {
        if (collision.transform.tag == TagManager.api.Ground)
            _inEarth = false;
    }
}
