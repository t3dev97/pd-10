﻿using Studio.BC;
using System;
using System.Collections;
using UnityEngine;

public class JumpJumpSkill : SkillBase
{
    [SerializeField]
    float JumpUp;

    MonsterSkillController _skillController;
    MonsterDetail _myDetail;
    Transform _container;
    GameObject _player;
    Rigidbody _rb;
    //float _h = 25;
    //float _gravity = 18;
    private void Awake()
    {
        if (GetComponent<MonsterSkillController>() == null)
            gameObject.AddComponent<MonsterSkillController>();

        _skillController = GetComponent<MonsterSkillController>();

        if (GetComponent<Rigidbody>() != null)
            _rb = GetComponent<Rigidbody>();

        _myDetail = GetComponent<MonsterDetail>();
    }
    private void Start()
    {
        _player = GameObject.FindGameObjectWithTag(TagManager.api.Player);
        _container = GameObject.FindObjectOfType<BCCache>().transform;
        TimeCooldown = 0;
        AddFreeSkillToSkillController();
        //StartCoroutine(RunDelay());
    }
    //IEnumerator RunDelay()
    //{
    //    yield return new WaitForSeconds(5);
    //    Launch();
    //}
    public override IEnumerator SkillStart()
    {
        if (_rb != null)
            _rb.velocity = Vector3.zero;
        _skillController.Skilling = true;
        _skillTimeLive = 0;
        _isDoneSkill = false;
        _isReady = false;
        if (!string.IsNullOrEmpty(StartAnimStr))
            _skillController.RunAnimation(StartAnimStr);

        yield return new WaitForSeconds(TimeOfStartAnim);
        StartCoroutine(SkillIn());
    }
    protected override IEnumerator SkillIn()
    {
        if (!string.IsNullOrEmpty(InAnimStr))
            _skillController.Amin.Play(InAnimStr);

        Jump();

        yield return new WaitForSeconds(TimeOfInAnim);
        if (_loop < TotalLoop)
        {
            _loop++;
            StartCoroutine("SkillIn");
        }
        else
            StartCoroutine(SkillEnd());
    }
    protected override IEnumerator SkillEnd()
    {
        if (!string.IsNullOrEmpty(EndAnimStr))
            _skillController.RunAnimation(EndAnimStr);
        Debug.Log("3");
        yield return new WaitForSeconds(TimeOfEndAnim);
        _skillController.RunAnimation(EnumAnimName.Idle.ToString());
        _isDoneSkill = true;
        _isReady = true;
        _loop = 0;
        _skillController.Skilling = false;
        AddFreeSkillToSkillController();
    }
    void Jump()
    {
        _rb.AddForce(transform.up * JumpUp);
        _rb.velocity = transform.forward * _myDetail.CurrentSpeedMove;
    }

    //void Launch()
    //{
    //    Physics.gravity = Vector3.up * _gravity;
    //    _rb.useGravity = true;
    //    _rb.velocity = GetVelocity(_player.transform);
    //}
    //Vector3 GetVelocity(Transform _player)
    //{
    //    float displacementY = _player.transform.position.y - _rb.position.y;
    //    Vector3 displacementXZ = new Vector3(_player.transform.position.x - _rb.position.x, 0, _player.transform.position.z - _rb.position.z);

    //    Vector3 velocityY = Vector3.up * Mathf.Sqrt(-2 * _gravity * _h);
    //    Vector3 velocityXZ = displacementXZ / (Mathf.Sqrt(-2 * _h / _gravity) + Mathf.Sqrt(2 * (displacementY - _h) / _gravity));

    //    return velocityXZ + velocityY * -Mathf.Sign(_gravity);
    //}
    private Action _skillStart;
    private void AddFreeSkillToSkillController()
    {
        _skillStart = () =>
        {
            StartCoroutine(SkillStart());
        };
        _skillController.AddFreeSkill(_skillStart, SkillPriority);
    }
}
