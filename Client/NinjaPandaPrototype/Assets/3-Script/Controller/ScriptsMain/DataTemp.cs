﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataTemp 
{
    public int chapter;
    public int stage;
    
    public int RewardExpInLife = 0; // tong exp nhan duoc khi ket thuc cac stage, duoc tong ket khi cet 
    public Dictionary<int, int> ListEquipmentsInStage = new Dictionary<int, int>();
    public Dictionary<int, int> ListMaterialInStage = new Dictionary<int, int>();
}
