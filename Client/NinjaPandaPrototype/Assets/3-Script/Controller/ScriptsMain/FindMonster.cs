﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class FindMonster : MonoBehaviour
{
    static public FindMonster api;
    [SerializeField]
    GameObject MonsterPoint;
    public bool checkFindMonster = false;
    //[HideInInspector];
    [Tooltip("Monster thỏa điều kiện đề ra")]
    public GameObject Target; // monster thỏa điều kiện để ra [tam thoi]


    private void Awake()
    {
        if (api == null)
            api = this;
        else
            Destroy(this);
    }
    private void Start()
    {
        MonsterPoint = Instantiate(MonsterPoint, transform.position, Quaternion.identity);
        StartCoroutine(Find()); // mới vào trận
    }
    public IEnumerator Find()  // tim 
    {
        if (GameManager.api.GameState == GAME_STATE.Playing)
        {

            if (PlayerDetail.api.HP > 0)
            {

                Target = GetMonster(GetListMonster());
                //if (BC_Chapter_Info.api != null)
                //{
                //    if (BC_Chapter_Info.api.FinalStage == false) // nếu stage chưa hoàn thành thì sẻ tìm quái
                //    {
                //        if (Target == null)
                //        {
                //            //checkFindMonster = false;
                //            yield return new WaitForSeconds(1.0f);  // fix tim nam phan than
                //            Target = GetMonster(GetListMonster());
                //            //#if !Scene_done
                //            if (Target == null && BC_Chapter_Info.api.FinalStage == false)
                //            {
                //                if (BC_Chapter_Info.api.currenStage < BC_Chapter_Info.api.listStage.Count && !BC_Chapter_Info.api.isNextMap) // nếu stage hiện tại vẫn còn đợt quái tiếp theo
                //                {

                //                    BC_Chapter_Info.api.isNextMap = true;
                //                    //BC_Chapter_Info.api.updateStage();
                //                }
                //                else
                //                {
                //                    //Debug.Log("qưe");
                //                    if (BC_Chapter_Info.api.CheckMonsterInStage() == false)
                //                        BC_Chapter_Info.api.OpenDoor();
                //                }
                //                //checkFindMonster = true;
                //            }
                //            //#endif
                //        }
                //        SelecedMonster(Target);
                //    }
                //}
                SelecedMonster(Target);
            }
            //BC_Chapter_Info.api.isNextMap = true;
            //BC_Chapter_Info.api.updateStage();
            yield return null;
        }

        //StartCoroutine(Find());
    }
    List<GameObject> GetListMonster()
    {
        List<GameObject> temp = new List<GameObject>();
        //List<GameObject> temp = new List<GameObject>(GameObject.FindGameObjectsWithTag(TagManager.api.MonsterBurrow));
        //temp.AddRange(GameObject.FindGameObjectsWithTag(TagManager.api.Monster));
        //foreach (var item in BC_Chapter_Info.api.listStageMonter)
        //{
        //    temp.AddRange(item.listMonster);
        //}
        if (BC_Chapter_Info.api.ListMonster.Count > 0)
            temp.AddRange(BC_Chapter_Info.api.ListMonster);
        //Debug.Log("GetListMonster" + temp.Count);
        return temp; // danh sách monster trong screen;
    }
    //public GameObject[] monsters;
    GameObject GetMonster(List<GameObject> listMonster) // Tìm monster thỏa điều kiện: Bắn được, gần nhất.
    {
        if (listMonster != null && listMonster.Count < 1)  // không có monster nào trong screen
        {
            //Debug.Log("Not Monster in Screen");
            return null;
        }
        //monsters = listMonster.ToArray();

        float minTemp = 1000; // Khoảng cách nhỏ nhất đến player
        int indexMonsterCanShoot = -1; // vị trí của monster thỏa điều kiện: bắn được
        int indexMonsterDistance = -1; // vị trí của monster thỏa điều kiện: gần nhất
        int indexMonsterDistanceCanShoot = -1; // vị trí của monster thỏa điều kiện: gần nhất

        // Tìm monster có thể bắn được trong danh sách các monster có trong screen
        foreach (var monsterItem in listMonster)
        {
            if (monsterItem != null && monsterItem.tag == TagManager.api.Monster)
            {
                RaycastHit objHit;
                Vector3 begin = transform.position/* + Vector3.up*/;
                Vector3 end = monsterItem.transform.position + Vector3.up;
                Vector3 dirction = end - begin;
                if (Physics.Raycast(begin, dirction, out objHit, (12 << 13)))
                {
                    //Debug.DrawLine(transform.position, monsterItem.transform.position, Color.red);
                    if (objHit.collider.tag == "Monster")
                    {
                        indexMonsterCanShoot = listMonster.IndexOf(monsterItem);

                        float distanceTemp = Vector3.Distance(transform.position, monsterItem.transform.position); // tính khoảng cách
                        if (minTemp > distanceTemp)
                        {
                            minTemp = distanceTemp;
                            indexMonsterDistanceCanShoot = listMonster.IndexOf(monsterItem);
                            //objTarget = monsterItem;
                        }
                    }
                }
                float distance = Vector3.Distance(transform.position, monsterItem.transform.position); // tính khoảng cách
                if (minTemp > distance)
                {
                    minTemp = distance;
                    indexMonsterDistance = listMonster.IndexOf(monsterItem);
                    //objTarget = monsterItem;
                }
            }
        }

        if (indexMonsterDistanceCanShoot >= 0)
        {
            //Debug.Log("MonsterCanShootDistance " + listMonster[indexMonsterDistanceCanShoot].name + " index: " + indexMonsterDistanceCanShoot);
            return listMonster[indexMonsterDistanceCanShoot];
        }
        else
        if (indexMonsterCanShoot >= 0)
        {
            //Debug.Log("MonsterCanShoot " + listMonster[indexMonsterCanShoot].name + " index: " + indexMonsterCanShoot);
            return listMonster[indexMonsterCanShoot];
        }
        else
        if (indexMonsterDistance >= 0)
        {
            //Debug.Log("MonsterDistance " + listMonster[indexMonsterDistance].name + " index: " + indexMonsterDistance);
            return listMonster[indexMonsterDistance];
        }
        else
        {
            //Debug.Log("Nonsense");
            return null;
        }
    }

    void SelecedMonster(GameObject monster)
    {
        if (monster == null)
        {
            MonsterPoint.SetActive(false);
            //Debug.Log("Not Monster");
            return;
        }
        else
        {
            MonsterPoint.SetActive(true);
            if (SeleceMonster.api != null && monster != null)
                SeleceMonster.api.Target = monster;
            //MonsterPoint.transform.position = monster.transform.position + SeleceMonster.api.Offset;
        }
    }
}
