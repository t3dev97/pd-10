﻿using UnityEngine;
public class TagManager : MonoBehaviour
{
    static public TagManager api;
    private void Awake()
    {
        if (api == null)
            api = this;
        else
            Destroy(this);
    }

    public string Untagged = "Untagged";
    public string Player = "Player";
    public string Monster = "Monster";
    public string MonsterDie = "MonsterDie";
    public string MonsterBurrow = "MonsterBurrow";
    public string Bullet = "Bullet";
    public string Ground = "Ground";
    public string Trap = "Trap";
    public string Wall = "Wall";
    public string WallBorder = "WallBorder";
    public string MonsterList = "MonsterList";
    public string Shield = "Shield";
    public void AddTag()
    {

    }
}
