﻿using Studio.BC;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadScene : MonoBehaviour
{
    public static LoadScene api;
    private void Awake()
    {
        if (api == null)
            api = this;
        else
            Destroy(this);
    }
    public void Load(string name)
    {
        foreach (Transform item in GameObject.FindObjectOfType<BCCache>().transform) //  xóa tất cả gold trên màn hình
        {
            Destroy(item.gameObject);
        }
        PlayerController.api.playerSkills.ResetSkill();
        SceneManager.LoadScene(name);
    }
    public void Load(int name)
    {
        foreach (Transform item in GameObject.FindObjectOfType<BCCache>().transform) //  xóa tất cả gold/effe trên màn hình
        {
            Destroy(item.gameObject);
        }
        SceneManager.LoadScene(name);
    }
}
