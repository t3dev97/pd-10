﻿using Studio;
using Studio.BC;
using System.Collections;
using UnityEngine;
public class PlayerController : MonoBehaviour
{
    public static PlayerController api;
    public static event System.Action<bool> PlayerMoving;
    public bool AllowShoot = true;
    public bool IsAutoShoot = false;

    public bool isBulletBlast;
    public bool isBulletIce;
    public bool isBulletPoison;
    public bool isBulletBolt;

    public int mIntBlockHit = 0;
    public int mIntLife = 0;
    public BCPlayerSkillController playerSkills;
    public SkinnedMeshRenderer Mesh;
    public Joystick JoyStickControl;
    public Transform posCreateBullet;
    public Transform posCreateWalkSmoke;

    public Vector3 tagetBullet;
    public Vector3 tagetCreateBullet;

    public GameObject WalkSmoke;

    [SerializeField]
    float timeWalkSmoke;
    float timeLiveWalkSmoke = 0;

    [SerializeField]
    PlayerDetail _myDetail;


    [SerializeField]
    GameObject bullet;

    [SerializeField]
    GameObject hitEff;

    [SerializeField]
    GameObject GodEffect;

    [SerializeField]
    float _pushBack;

    Rigidbody rb;
    Animator animator;
    string[] animations = new string[] { "Idle", "Run", "Atk", "Hit", "Die" }; // các trạng thái aniamtino nhân vật 
    bool checkAtk = false; // đang trong trạng thái Attack
    bool checkHit = false; // đang trong trạng thái Hit
    bool checkRun = false;  // đang trong trạng thái Run
    bool checkIdle = false; // đang ở trong trạng thái nghĩ
    bool checkFind = true; // true neu cho phep tim, 
    bool isMoving = false;
    public bool CheckFind { get => checkFind; set => checkFind = value; }



    private void Awake()
    {
        if (api == null)
            api = this;
        else
            Destroy(api);

        hitEff = Resources.Load("Effects/FloatingDamage", typeof(GameObject)) as GameObject;

        if (GetComponent<PlayerDetail>() == null)
        {
            gameObject.AddComponent<PlayerDetail>();
        }
        _myDetail = GetComponent<PlayerDetail>();
        animator = GetComponent<Animator>();

        //BCCache.Api.GetBulletAsync("BulletPool", (go) =>
        //{
        //    BCObjectPool.Api.InitBullet(go, 20);
        //});

        BCCache.Api.GetBulletAsync("BulletPool Kunai2", (go) =>
        {
            BCObjectPool.Api.InitBullet(go, 20);
        });

        if (WalkSmoke == null)
        {
            WalkSmoke = Resources.Load("Effects/WalkSmoke", typeof(GameObject)) as GameObject;
        }

        posCreateBullet = transform.GetChild(0);
    }
    private void OnEnable()
    {
        PlayerMoving += AC_Move;
    }
    private void OnDisable()
    {
        PlayerMoving -= AC_Move;
    }

    private void AC_Move(bool v)
    {

    }
    void Start()
    {

        JoyStickControl = GameObject.FindGameObjectWithTag("JoytickController").GetComponent<Joystick>();
        rb = GetComponent<Rigidbody>();

        timeLastAtk = Time.time;
        _isShoot = false;

        //playerSkills.AddSkill((int)EnumSkillAddParameterID.ExtraLife);
        StartCoroutine(ReFindTarget());
    }
    [SerializeField]
    float _timeLiveTakeHit = 0;
    float _timeCoolDownTakeHit = 1.0f;

    public float _atkCooldownCounter;

    bool _canTakeHit = true;

    IEnumerator ReFindTarget()
    {
        StartCoroutine(FindMonster.api.Find());
        yield return new WaitForSeconds(1);
        StartCoroutine(ReFindTarget());
    }
    void Update()
    {
        if (GameManager.api.GameState == GAME_STATE.Playing)
        {
            if (_myDetail.HP > 0)
            {
                _timeLiveTakeHit += Time.deltaTime;
                if (_timeLiveTakeHit > _timeCoolDownTakeHit)
                {
                    _canTakeHit = true;
                    _timeLiveTakeHit = 0;
                }
                Controller();
                _atkCooldownCounter += Time.deltaTime;
            }
            else
            {
                Die();
            }
        }
        else
        {
            RunAnimation("Idle");
        }
    }
    public void Revival()
    {
        gameObject.SetActive(true);
        _myDetail.HP = _myDetail.MaxHPInStage;
        BCCache.Api.GetEffectPrefabAsync(EffectKey.FXReborn, (go) =>
        {
            GameObject game = Instantiate(go);
            game.transform.position = gameObject.transform.position;
            Destroy(game, 1f);
        });
        Idle();
    }
    private string currentAnim;
    void RunAnimation(string name)
    {
        if (currentAnim != name)
        {
            foreach (var item in animations)
            {
                animator.SetBool(item, false);
            }
        }
        animator.SetBool(name, true);
        currentAnim = name;
    }

    void Die()
    {
        RunAnimation("Die");
    }
    public void EndDie() // Được gọi khi animation Die kết thúc
    {

        if (_myDetail.IncreasesExtraLife > 0)
        {
            _myDetail.IncreasesExtraLife--;
            Revival();
        }
        else
        {

            gameObject.SetActive(false);

            if (BCPopupManager.api.isShowPopupHoiSinh)
            {
                BCPopupManager.api.Show(PopupName.PopupStageRearch);
            }
            else
            {
#if !Scene_done
                BCPopupManager.api.Show(PopupName.PopupHoiSinh);
#endif
            }

        }
    }
    // Jump
    float timeLastJump = 0;
    float timeJump = 2; // thời gian giản cách Jump
    void Jump()
    {
        if (Time.time - timeLastJump > timeJump)
        {
            timeLastJump = Time.time;
            rb.AddForce(transform.up * 200);
        }
    }

    // Atk
    public float timeLastAtk; // thời gian atk trước
    bool Attack()
    {
        if ((FindMonster.api.Target == null && !IsAutoShoot) || (_atkCooldownCounter < PlayerDetail.api.SpeedAtkInStage && !_isShoot))
        {
            Idle();
            return true;
        }
        if (SeleceMonster.api.Target != null && _atkCooldownCounter >= PlayerDetail.api.SpeedAtkInStage)
        {

            _atkCooldownCounter = 0;

            Vector3 target = new Vector3(SeleceMonster.api.Target.transform.position.x, this.transform.position.y, SeleceMonster.api.Target.transform.position.z);

            transform.LookAt(target);
            timeLastAtk = Time.time; // lưu thời gian tấn công hiện tại
            tagetBullet = transform.eulerAngles; // lay goc
            tagetCreateBullet = posCreateBullet.position;

            if (GameManager.api.GameState == GAME_STATE.Playing)
                RunAnimation("Atk");

            _isShoot = true;
            return true;
        }
        return false;
    }
    private bool _isShoot;
    void AutoAttack()
    {
        var targetPos = transform.forward;
        Vector3 target = new Vector3(targetPos.x, this.transform.position.y, targetPos.z);
        transform.LookAt(target);
        if (Time.time - timeLastAtk > 1 / _myDetail.SpeedAtkInStage /*|| timeLastAtk < 0.5f*/)
        {
            timeLastAtk = Time.time; // lưu thời gian tấn công hiện tại
            //if (GameManager.api.GameState == GAME_STATE.Playing)
            //    RunAnimation("Atk");
            _isShoot = true;
        }
    }

    public void ShootComplete()
    {
        _isShoot = false;
        _atkCooldownCounter = 0;
    }
    public void Shoot() // được gọi khi animation Atk gần kết thúc
    {
        if (GameManager.api.GameState == GAME_STATE.Playing)
            playerSkills.PlayMultiShot();
    }
    public void ShootVirtual(float posXAlpla = 0, float angles = 0)
    {
        BCAudioController.api.PlayAudio(EnumAudioSource.ShotPlayer);

        var angle = new Vector3(0, tagetBullet.y - angles, 0);
        var originBullet = BCObjectPool.Api.GetBullet(0, tagetCreateBullet, Quaternion.Euler(angle));

        originBullet.GetComponent<BulletController>().damage = _myDetail.DamageAtkInStage;
        //originBullet.transform.eulerAngles = new Vector3(0, originBullet.transform.eulerAngles.y - angles, 0);

        Vector3 vec = originBullet.transform.GetChild(0).localPosition;
        Vector3 vec1 = originBullet.transform.GetChild(0).localPosition;

        vec.x = posXAlpla / originBullet.transform.GetChild(0).localScale.x;
        originBullet.transform.GetChild(0).localPosition = vec;


        originBullet.transform.position = originBullet.transform.GetChild(0).position;

        originBullet.transform.GetChild(0).localPosition = vec1;
        originBullet.GetComponent<BulletController>().SetUpData();

        originBullet.SetActive(true);
    }

    public void UpdateSpeedAtk()
    {
        animator.SetFloat("SpeedAtk", _myDetail.SpeedAtkInStage);
    }
    void Idle()
    {
        RunAnimation("Idle");
    }
    float canMove = 0.3f;
    void Controller()
    {

#if !EDITOR
        if (JoyStickControl.Horizontal > 0.1f || JoyStickControl.Horizontal < -0.1f
              || JoyStickControl.Vertical > 0.1f || JoyStickControl.Vertical < -0.1f)
        {
            Vector3 direction = new Vector3(JoyStickControl.Horizontal, 0f, JoyStickControl.Vertical).normalized;
            //Debug.Log(JoyStickControl.Horizontal);

            if (JoyStickControl.Horizontal > canMove || JoyStickControl.Horizontal < -canMove
              || JoyStickControl.Vertical > canMove || JoyStickControl.Vertical < -canMove)
                rb.velocity = direction * _myDetail.SpeedMoveInStage;

            var angle = Vector2.Angle(JoyStickControl.Direction, Vector2.up);
            if (JoyStickControl.Horizontal < 0)
                angle = -angle;
            transform.rotation = Quaternion.Euler(0, angle, 0);

            if (SeleceMonster.api != null)
                SeleceMonster.api.Target = null;
            Run(); // thực hiện chạy khi đang di chuyển
                   //StartCoroutine(FindMonster.api.Find()); // tìm quái mới
            PlayerMoving(true);
        }
#else
        float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");
        if (h > 0 || h < 0 || v > 0 || v < 0)
        {
            Vector3 direction = new Vector3(h, 0f, v).normalized;

            rb.velocity = direction * _myDetail.SpeedMoveInStage;

            var angle = Vector2.Angle(new Vector2(h, v), Vector2.up);
            if (h < 0)
                angle = -angle;
            transform.rotation = Quaternion.Euler(0, angle, 0);

            if (SeleceMonster.api != null)
                SeleceMonster.api.Target = null;

            Run(); // thực hiện chạy khi đang di chuyển
            StartCoroutine(FindMonster.api.Find()); // tìm quái mới
        }
#endif
        else
        {
            PlayerMoving(false);
            if (SeleceMonster.api != null)
            {
                if (SeleceMonster.api.Target == null || SeleceMonster.api.Target.tag != TagManager.api.Monster)
                {
                    // Khi đứng im và quái đã chọn bị chết
                    if (BC_Chapter_Info.api.ListMonster == null || BC_Chapter_Info.api.ListMonster.Count < 1)
                    {
                        RunAnimation("Idle");
                        return;
                    }
                    //Debug.Log("khi quái hiện tại đã chuyển tag khác (Cây măng khi độn thổ)");

                    StartCoroutine(FindMonster.api.Find()); // khi quái hiện tại đã chuyển tag khác (Cây măng khi độn thổ)
                }
            }
            if (AllowShoot)
            {
                if (!Attack() && IsAutoShoot)
                {
                    AutoAttack();
                }
            }
            // nếu không di chuyển thì tất công
        }
    }
    void Run()
    {
        RunAnimation("Run");

        timeLiveWalkSmoke += Time.deltaTime;
        if (timeLiveWalkSmoke > timeWalkSmoke)
        {
            timeLiveWalkSmoke = 0;
            CreateWalkSmoke();
        }
    }
    void CreateWalkSmoke()
    {
        if (WalkSmoke != null)
            Instantiate(WalkSmoke, posCreateWalkSmoke.transform.position, Quaternion.identity, GameObject.FindObjectOfType<BCCache>().transform);
        else
            Debug.Log("Null WalkSmoke");
    }
    public void Hit(float damage, Transform monster = null)
    {

        if (_canTakeHit)
        {
            _canTakeHit = false;
            StartCoroutine(DelayGodEffect());
            _myDetail.HP -= damage;
            ShowHitDamage(damage, Color.white); // hiện hiệu ứng mất máu
        }
    }
    public void Hit(float damage, MONSTER_RACE_TYPE monsterType, MONSTER_ATK_TYPE monsterATKType)
    {
#if EDITOR
        if (BCApiTest.api != null)
        {
            if (BCApiTest.api.isGoToTest)
                return;
        }
#endif
        if (_canTakeHit)
        {
            int result = UnityEngine.Random.RandomRange(0, 100);
            StartCoroutine(DelayGodEffect());

            if (result > _myDetail.DodgeInStage)
            {
                //Debug.Log(damage);
                if (monsterATKType == MONSTER_ATK_TYPE.range)
                {
                    damage -= (damage * _myDetail.ReductionRangerDame / 100);
                }
                else
                {
                    damage -= (damage * _myDetail.ReductionMeleeDame / 100);
                }

                if (damage < 0)
                {
                    damage = 0;
                }
                if (_canTakeHit)
                {
                    _myDetail.HP -= damage;
                    ShowHitDamage(damage, Color.white); // hiện hiệu ứng mất máu
                }
                _canTakeHit = false;
            }
            else
            {
                ShowHitDamage(LanguageManager.api.GetKey(BCLocalize.GUI_NAME_MISS), Color.green);
            }
        }
    }
    IEnumerator DelayGodEffect()
    {
        GodEffect.gameObject.SetActive(true);
        GodEffect.GetComponent<ParticleSystem>().enableEmission = true;
        yield return new WaitForSeconds(1.0f);
        GodEffect.gameObject.SetActive(false);
        GodEffect.GetComponent<ParticleSystem>().enableEmission = false;
    }
    public IEnumerator PushBack(Vector3 origin, Transform target)
    {
        Vector3 posTarget = transform.TransformPoint(target.position);
        origin.y = posTarget.y;

        Vector3 dir = (origin - target.position).normalized;

        LeanTween.move(target.gameObject, target.transform.position - dir * _pushBack, 0.2f);
        yield return new WaitForSeconds(0.5f);
    }
    public void ActiveGlow(int Active)
    {
        if (Mesh == null)
            return;
        var mat = Mesh.material;
        mat.SetInt("_Active_Glow", Active);
    }
    void ShowHitDamage(float damage, Color color)
    {
        if (hitEff == null)
            return;
        GameObject eff = Instantiate(hitEff, transform.position, Quaternion.identity);// Hiệu ứng hit
        TextMesh textMesh = eff.transform.GetChild(0).GetComponent<TextMesh>();

        Vector3 vector = new Vector3(UnityEngine.Random.Range(-1.0f, 1.0f), 5, UnityEngine.Random.Range(-1.0f, 1.0f));
        eff.transform.GetChild(0).localPosition += vector;

        textMesh.text = damage.ToString();
        textMesh.color = color;
        eff.SetActive(true); // bật hiệu ứng hit, hiệu ứng tự tắt trong thời gian quy định
    }

    void ShowHitDamage(string damage, Color color)
    {
        if (hitEff == null)
            return;

        GameObject eff = Instantiate(hitEff, transform.position, Quaternion.identity);// Hiệu ứng hit
        TextMesh textMesh = eff.transform.GetChild(0).GetComponent<TextMesh>();
        textMesh.text = damage.ToString();
        textMesh.color = color;
        eff.SetActive(true); // bật hiệu ứng hit, hiệu ứng tự tắt trong thời gian quy định
    }


    public void AddSkill(int idSkill)
    //if (LanguageManager.api.GetKey(BCCache.Api.DataConfig.Skills[idSkill].Name).Equals(SKILL_NAME.Heal.ToString()))
    //{
    {
        //    _myDetail.HP += 0.2f * _myDetail.BasicHP;
        //    //Debug.Log("Đã hồi " + 0.2f * myDetail.BasicHP + "HP");
        //    return;
        //}
        //if (LanguageManager.api.GetKey(BCCache.Api.DataConfig.Skills[idSkill].Name).Equals(SKILL_NAME.Attack.ToString()))
        //{
        //    _myDetail.DamageAtkInStage += 0.3f * _myDetail.BasicATKDamage;
        //    //Debug.Log("Đã thêm " + 0.3f * myDetail.BasicATKDamage + "ATK");
        //    return;
        //}
        //if (LanguageManager.api.GetKey(BCCache.Api.DataConfig.Skills[idSkill].Name).Equals(SKILL_NAME.Speed.ToString()))
        //{
        //    _myDetail.SpeedAtkInStage += 0.5f * _myDetail.BasicAS;
        //    //Debug.Log("Đã thêm " + 0.5f * myDetail.BasicAS + "AS");
        //    return;
        //}


        ////Debug.Log(myDetail.Skills.Count);
        //_myDetail.SkillsInStage[idSkill]++;
        //foreach (var item in _myDetail.SkillsInStage)
        //{
        //}


        //----------------New------------------------------------
        playerSkills.AddSkill(idSkill);

        string nameSkill = LanguageManager.api.GetKey(BCCache.Api.DataConfig.SkillBases[idSkill].nameSkill);
        string descriptionSkill = LanguageManager.api.GetKey(BCCache.Api.DataConfig.SkillBases[idSkill].Description);

        UIBasicController.api.TxtNameSkill.text = nameSkill;
        UIBasicController.api.TxtDescriptionSkill.text = descriptionSkill;
        UIBasicController.api.TxtDescriptionShadowSkill.text = descriptionSkill;
        UIBasicController.api.DetailSkill.gameObject.SetActive(true);
        //_myDetail.SkillsInStage[idSkill]++;

        //Debug.Log("Add Skill " + LanguageManager.api.GetKey(BCCache.Api.DataConfig.Skills[idSkill].Name));
    }


    float timer;
    private void OnTriggerEnter(Collider obj)
    {
        if (obj.transform.tag == "PosEnd" && FindMonster.api.Target == null)
        {
            var finalChap = BC_DataGame.api.fileSaveData.listMap.Count;
            //Debug.Log("final chap: " + finalChap);
            //Debug.Log("Chap: " + (PlayerPrefs.GetInt("chap") - 1));
            int a = PlayerPrefs.GetInt("chap");
            int b = PlayerPrefs.GetInt("round");
            var finalRound = BCCache.Api.DataConfig.MapInfo[PlayerPrefs.GetInt("chap")].listStage.Count;
            //Debug.Log("final round: " + finalRound);
            if (PlayerPrefs.GetInt("round") < finalRound)
            {
                PlayerPrefs.SetInt("round", PlayerPrefs.GetInt("round") + 1);

            }
            else
            {
                foreach (Transform item in GameObject.FindObjectOfType<BCCache>().transform) //  xóa tất cả gold trên màn hình
                {
                    Destroy(item);
                }
                PlayerController.api.playerSkills.ResetSkill();
                // hoan thanh chapter
                //LoadScene.api.Load(NameScene.LayoutDemo);
                BCPopupManager.api.Show(PopupName.PopupStageRearch);
                //SceneManager.LoadScene(NameScene.LayoutDemo, LoadSceneMode.Single);
                return;
            }

            //Prefs.GetInt("chap")].listRound[PlayerPrefs.GetInt("round")] != null)

            StartCoroutine(BC_DataGame.api.LoadMap(() =>
            {
                playerSkills.PlaySpecial();

                InitMap.api.RemoveNavigation();
                InitMap.api.CreateNavigation();

                if (BC_Chapter_Info.api.isStageBoos)
                {
                    UIBasicController.api.HealtBoss.gameObject.SetActive(true);
                    UIBasicController.api.EXPObject.gameObject.SetActive(false);
                }
                else
                {
                    UIBasicController.api.HealtBoss.gameObject.SetActive(false);
                    UIBasicController.api.EXPObject.gameObject.SetActive(true);
                }
                StartCoroutine(BC_Chapter_Info.api.DelayAllowMonsterAtk());
            }));
        }
    }
    private void OnTriggerStay(Collider obj)
    {
        if (obj.transform.tag == TagManager.api.Trap)
        {
            if (Time.time - timer > 0.5f)
            {
                timer = Time.time;
                Hit(obj.transform.GetComponent<BayController>().Damage);
                StartCoroutine(PushBack(obj.transform.position, transform));
            }
        }
    }
    public void DropGold(int numberGold)
    {
        //var spawner = GetComponent<InGameGoldSpawner>();
        //if (spawner != null)
        //{
        //    spawner.SpawnGoldReward(numberGold);
        //}
        //StartCoroutine(ClearGold());
    }

    IEnumerator ClearGold()
    {
        yield return new WaitForSeconds(0.5f);
        //BC_Chapter_Info.api.ClearGold();
    }
}
