﻿using UnityEngine;
using UnityEditor;
using UnityEditor.UI;

[CustomEditor (typeof (BulletMoveController))]
public class BulletMoveControllerEditor : Editor
{
    private SerializedProperty _bulletMoveType;
    private SerializedProperty _target;
    private SerializedProperty _bullet;
    private SerializedProperty _autoStart;

    private SerializedProperty _bulletSpeed;
    // Curve
    private SerializedProperty _curve_speed;
    private SerializedProperty _curve_offset;
    private SerializedProperty _curve_reverse;

    // Zig Zag
    private SerializedProperty _zigzag_speed;
    private SerializedProperty _zigzag_offset;
    private SerializedProperty _zigzag_reverse;

    //Circle
    private SerializedProperty _circle_speed;
    private SerializedProperty _circle_startAngle;
    private SerializedProperty _circle_distance;
    private SerializedProperty _circle_reverse;

    protected void OnEnable()
    {
        _bulletMoveType = serializedObject.FindProperty("BulletMoveType");
        _target = serializedObject.FindProperty("Target");
        _bullet = serializedObject.FindProperty("Bullet");
        _autoStart = serializedObject.FindProperty("IsAutoStart");

        _bulletSpeed = serializedObject.FindProperty("BulletMoveSpeed");

        _curve_speed = serializedObject.FindProperty("Curve_Speed");
        _curve_offset = serializedObject.FindProperty("Curve_Offset");
        _curve_reverse = serializedObject.FindProperty("Curve_Reverse_Rotation");

        _zigzag_speed = serializedObject.FindProperty("ZigZag_Speed");
        _zigzag_offset = serializedObject.FindProperty("ZigZag_Offset");
        _zigzag_reverse = serializedObject.FindProperty("ZigZag_Reverse_Rotation");

        _circle_speed = serializedObject.FindProperty("Circle_Speed");
        _circle_startAngle = serializedObject.FindProperty("Circle_StartAngle");
        _circle_distance = serializedObject.FindProperty("Circle_Distance");
        _circle_reverse = serializedObject.FindProperty("Circle_Reverse_Rotation");
    }

    public override void OnInspectorGUI()
    {
        BulletMoveController controller = target as BulletMoveController;
        serializedObject.Update();

        EditorGUILayout.PropertyField(_bulletMoveType);
        EditorGUILayout.PropertyField(_autoStart);

        EditorGUILayout.PropertyField(_bullet);

        switch (controller.BulletMoveType)
        {
            case BulletMoveController.MoveType.None:
                {
                    break;
                }
            case BulletMoveController.MoveType.Straight:
                {
                    EditorGUILayout.PropertyField(_target);
                    EditorGUILayout.PropertyField(_bulletSpeed);
                    break;
                }
            case BulletMoveController.MoveType.Curve:
                {
                    EditorGUILayout.PropertyField(_target);
                    EditorGUILayout.PropertyField(_curve_speed);
                    EditorGUILayout.PropertyField(_curve_offset);
                    EditorGUILayout.PropertyField(_curve_reverse);
                    break;
                }
            case BulletMoveController.MoveType.ZigZag:
                {
                    EditorGUILayout.PropertyField(_target);
                    EditorGUILayout.PropertyField(_bulletSpeed);
                    EditorGUILayout.PropertyField(_zigzag_speed);
                    EditorGUILayout.PropertyField(_zigzag_offset);
                    EditorGUILayout.PropertyField(_zigzag_reverse);
                    break;
                }
            case BulletMoveController.MoveType.Circle:
                {
                    EditorGUILayout.PropertyField(_circle_speed);
                    EditorGUILayout.PropertyField(_circle_startAngle);
                    EditorGUILayout.PropertyField(_circle_distance);
                    EditorGUILayout.PropertyField(_circle_reverse);
                    break;
                }
        }
        serializedObject.ApplyModifiedProperties();

    }
}
