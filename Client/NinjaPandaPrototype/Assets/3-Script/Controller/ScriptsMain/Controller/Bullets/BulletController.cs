﻿using Studio.BC;
using System.Collections;
using UnityEngine;

public class BulletController : MonoBehaviour
{
    public float damage = 0;
    public float PushBack;
    public float bouncingTimes = 0;
    public float ricochetTimes = 0;

    public GameObject FXEquipBullet;
    public GameObject FXBulletBlast;
    public GameObject FXBulletIce;
    public GameObject FXBulletPoison;
    public GameObject FXBulletBolt;

    public bool WallHorizontal = false;
    public bool isCollisder = false;
    public RaycastHit hitLeft;
    //public GameObject EffHit;

    //Transform _constaner;
    Rigidbody rb;

    [SerializeField]
    private float _speedMove;
    private float _time;
    private int _timeDestroy = 1;

    private void Awake()
    {
        ReferenceFX();
    }
    void ReferenceFX()
    {
        if (FXEquipBullet == null)
        {
            FXEquipBullet = (transform.FindChild("FX_Equip_Bullet_Fix(Clone)").gameObject != null) ? transform.FindChild("FX_Equip_Bullet_Fix(Clone)").gameObject : transform.FindChild("FX_Equip_Bullet_Fix").gameObject;
        }
        FXBulletIce = FXEquipBullet.transform.GetChild(0).gameObject;
        FXBulletBlast = FXEquipBullet.transform.GetChild(2).gameObject;
        FXBulletBolt = FXEquipBullet.transform.GetChild(1).gameObject;
        FXBulletPoison = FXEquipBullet.transform.GetChild(3).gameObject;

    }
    public void SetUpData()
    {
        _time = 0;
        rb = GetComponent<Rigidbody>();
        rb.velocity = Vector3.zero;
        bouncingTimes = PlayerController.api.playerSkills.skillEffects.bouncingTimes;
        ricochetTimes = PlayerController.api.playerSkills.skillEffects.ricochetTimes;

        ResetHit();
    }

    public void UpdateEffectButllet(EnumBulletEffect effect)
    {
        switch (effect)
        {
            case EnumBulletEffect.Blast: FXBulletBlast.SetActive(true); break;
            case EnumBulletEffect.Ice: FXBulletIce.SetActive(true); break;
            case EnumBulletEffect.Poision: FXBulletPoison.SetActive(true); break;
            case EnumBulletEffect.Bolt: FXBulletBolt.SetActive(true); break;
        }
    }
    public IEnumerator DestroyByTime()
    {
        _speedMove = 0;
        yield return new WaitForSeconds(_timeDestroy);
        DestroyBullet();
    }

    private void Update()
    {
        rb.velocity = transform.forward * _speedMove;
        if (_time >= 10)
            DestroyBullet();
        else
            _time += Time.deltaTime;

        Debug.DrawRay(transform.transform.position, transform.forward, Color.cyan);
    }
    GameObject hit;
    float distance = 0;
    Color color;
    private void OnDrawGizmos()
    {
        Gizmos.color = color;
        Gizmos.DrawWireSphere(transform.position + transform.forward * 1, .2f);
    }
    public void ResetHit()
    {
        if (Physics.SphereCast(transform.transform.position, .2f, transform.forward, out hitLeft,
            maxDistance: Mathf.Infinity, ~((1 << LayerMask.NameToLayer("Player"))
            | (1 << LayerMask.NameToLayer("Monster"))
            | (1 << LayerMask.NameToLayer("MonsterDie"))
            | (1 << LayerMask.NameToLayer("Default"))
            | (1 << LayerMask.NameToLayer("Burrow"))
            | (1 << LayerMask.NameToLayer("Poison"))
            | (1 << LayerMask.NameToLayer("Bullet")))))
        {
            hit = hitLeft.transform.gameObject;
            //distance = hitLeft.distance;
            if (hitLeft.transform.tag == "Wall" || hitLeft.transform.tag == "WallBorder")
            {
                color = Color.red;
                if (hitLeft.normal.x == 0)
                {
                    WallHorizontal = true;
                }
                else
                {
                    WallHorizontal = false;
                }
                //Debug.Log("bullet.WallHorizontal " + WallHorizontal);
            }
            else
            {
                color = Color.green;
            }
        }
        //else
        //{
        //    distance = 0;
        //    hit = null;
        //}
    }
    IEnumerator delayFrame()
    {
        transform.gameObject.GetComponent<SphereCollider>().enabled = false;
        yield return new WaitForSeconds(Time.deltaTime * 2);
        transform.gameObject.GetComponent<SphereCollider>().enabled = true;
    }
    public void OnTriggerEnter(Collider obj)
    {
        float Dame = damage;
        bool isCrInStage = false;

        string sTag = obj.transform.tag;

        if (sTag == TagManager.api.Monster && obj.GetType() == typeof(CapsuleCollider)) // Xử lý với CapsuleColler của quái
        {

            BCAudioController.api.PlayAudio(EnumAudioSource.HitMonster);

            if (obj.GetComponent<MonsterDetail>().AtkType == MONSTER_ATK_TYPE.range)
            {
                Dame += damage * ((PlayerDetail.api.IncreasesRangerDame) / 100);
            }
            else
            {
                Dame += damage * ((PlayerDetail.api.IncreasesMeleeDame) / 100);
            }

            int rate = Random.RandomRange(0, 100);

            if (rate < PlayerDetail.api.CrInStage)
            {
                Dame = Dame * (PlayerDetail.api.CdInStage / 100);
                isCrInStage = true;
            }

            Dame += PlayerDetail.api.IncreasesDameEffect;
        }

        if (!isCollisder)
        {
            DoPushBack(PushBack, PlayerDetail.api.transform.position, obj.transform);
            PlayerController.api.playerSkills.PlayEffectBullet(sTag, this, obj.transform,
        () =>
        {
            obj.GetComponent<MonsterController_new>().TakeDamage(Dame, isCrInStage, false, false);
        });
        }

    }
    public void DoPushBack(float pushback, Vector3 origin, Transform target)
    {
        if (target.GetComponent<Rigidbody>() == null) return;
        StartCoroutine(Back(pushback, origin, target));
    }

    IEnumerator Back(float foceBack, Vector3 origin, Transform target)
    {
        bool isAgent = false;
        bool isRbMove = false;
        Vector3 velocity = Vector3.zero;
        if (target.GetComponent<AgentMesh>() != null && target.GetComponent<AgentMesh>().enabled == true)
        {
            target.GetComponent<AgentMesh>().enabled = false;
            isAgent = true;
        }

        if (target.GetComponent<MonsterMove_new>() != null && target.GetComponent<MonsterMove_new>().CanMove)
        {
            target.GetComponent<MonsterMove_new>().CanMove = false;
            isRbMove = true;
        }

        Vector3 posTarget = transform.TransformPoint(target.position);
        origin.y = posTarget.y;

        Vector3 dir = (origin - target.position).normalized;

        LeanTween.move(target.gameObject, target.transform.position - dir * PushBack, 0.2f).setOnUpdate((float v) =>
        {

        }).setOnComplete(() =>
        {
            Vector3 t = target.position;
            t.x = Mathf.Clamp(t.x, -BC_DataGame.api.WidthMap * 1.0f / 2 + .5f, BC_DataGame.api.WidthMap * 1.0f / 2 - .5f);
            t.z = Mathf.Clamp(t.z, -BC_DataGame.api.HeightMap * 1.0f / 2 + .5f, BC_DataGame.api.HeightMap * 1.0f / 2 - .5f);
            target.transform.position = t;
        });
        yield return new WaitForSeconds(0.5f);

        if (isAgent)
        {
            target.GetComponent<AgentMesh>().enabled = true;
        }

        if (isRbMove)
        {
            target.GetComponent<MonsterMove_new>().CanMove = true;
        }
    }
    public void OnTriggerExit(Collider other)
    {
        isCollisder = false;
    }
    public IEnumerator RotateBullet(float angle)
    {
        if (!isCollisder)
        {
            gameObject.transform.rotation = Quaternion.Euler(0f, angle, 0);
            gameObject.transform.position = hitLeft.point;
            ResetHit();
            bouncingTimes--;
            isCollisder = true;
            yield return null;
        }
    }

    public void RotateBulletCustom(Transform angle, Vector3 hit)
    {
        Vector3 vec = Vector3.zero;
        vec.x = float.Parse(angle.position.x.ToString("0.000000"));
        vec.y = float.Parse(angle.position.y.ToString("0.000000"));
        vec.z = float.Parse(angle.position.z.ToString("0.000000"));
        gameObject.transform.LookAt(new Vector3(vec.x, vec.y, vec.z), Vector3.up);

        gameObject.transform.position = hit;
        ResetHit();
        ricochetTimes--;
    }


    public void DestroyBullet(bool isMonster = false)
    {
        if (!isMonster)
        {
            //BCCache.Api.GetEffectPrefabAsync(EffectKey.FX_PhiTieuTouch, (effect) =>
            //{
            //    GameObject obj_ = Instantiate(effect, transform.position, Quaternion.identity, GameObject.FindObjectOfType<BCCache>().transform);
            //    obj_.transform.SetParent(BCCache.Api.transform);
            //    if (obj_.GetComponent<ClearObj>() != null)
            //    {
            //        obj_.GetComponent<ClearObj>().TimeCoolDown = 1.5f;
            //        obj_.GetComponent<ClearObj>().Auto = true;
            //    }
            //    else
            //    {
            //        obj_.gameObject.AddComponent<ClearObj>();
            //        obj_.GetComponent<ClearObj>().TimeCoolDown = 1.5f;
            //        obj_.GetComponent<ClearObj>().Auto = true;
            //    }

            //    obj_.transform.gameObject.SetActive(true);
            //    obj_.transform.GetComponent<ParticleSystem>().enableEmission = true;
            //});

            FXEquipBullet.transform.SetParent(BCCache.Api.transform);
            FXEquipBullet.GetComponent<ClearObj>().Clear(1.5f);
            GameObject FXEquipBulletNext = Instantiate(FXEquipBullet, transform);
            FXEquipBulletNext.name = "FX_Equip_Bullet_Fix";
            FXEquipBullet = FXEquipBulletNext;
            FXEquipBullet.transform.localPosition = Vector3.zero;
            ReferenceFX();
        }

        transform.position = Vector3.zero;
        BCObjectPool.Api.PushBullet(gameObject);
    }
}
