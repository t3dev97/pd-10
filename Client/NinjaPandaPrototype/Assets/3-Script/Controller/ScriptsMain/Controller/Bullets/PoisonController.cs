﻿using UnityEngine;

public class PoisonController : MonoBehaviour
{
    public float Damage = 1;
    public float TimeLife = 1;
    public bool Default = true;
    private void Start()
    {
        if (Default)
            Destroy(gameObject, TimeLife);
    }
    public void SetUp(float radius, float damage, float timeLife)
    {
        GetComponent<SphereCollider>().radius = radius;
        GetComponent<SphereCollider>().isTrigger = true;
        Damage = damage;
        TimeLife = timeLife;
        Destroy(gameObject, timeLife);
    }

    private void OnTriggerStay(Collider obj)
    {
        string sTag = obj.gameObject.tag;
        if (sTag == TagManager.api.Player)
        {
            obj.GetComponent<PlayerController>().Hit(Damage);
            StartCoroutine(obj.transform.GetComponent<PlayerController>().PushBack(transform.position, obj.transform));
        }
    }
}
