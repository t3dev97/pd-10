﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class BulletCuaController : MonoBehaviour
{

    Rigidbody _rb;
    BulletDetail _myDetail;
    Vector3 _offsetDirBullet = Vector3.zero;
    public GameObject EffectNo;
    public bool AllowMove = false;
    [Header("Set Auto Explode")]
    public bool ExplodeAffter = false;
    public float TimeDelay = 2;
    public GameObject EffectRangeExplode;

    private void Awake()
    {
        _myDetail = GetComponent<BulletDetail>();
        _rb = GetComponent<Rigidbody>();
        EffectNo = Resources.Load("Effects/FloatingDamage", typeof(GameObject)) as GameObject;
    }

    private void Start()
    {
        if (EffectNo != null)
            EffectNo.GetComponent<TextMesh>().text = "";
        else
           //Debug.Log("EffectNo null");

        if (transform.GetChild(0) != null && transform.GetChild(0).GetComponent<SphereCollider>() != null)
            transform.GetChild(0).GetComponent<SphereCollider>().radius = _myDetail.RangeDamage;

        if (_rb.useGravity)
            _offsetDirBullet = Vector3.down;

        if (_myDetail.PosTarget != null)
        {
            Vector3 temp = _myDetail.PosTarget.position;
            temp += Vector3.up;
            _offsetDirBullet = (temp - transform.position).normalized;
        }
#if EDITOR
        try
        {
            if (SceneManager.GetActiveScene().name == "MainArt")
            {
                if (GetComponent<ClearObj>() != null)
                {
                    var clear = GetComponent<ClearObj>();
                    clear.enabled = true;
                    clear.TimeCoolDown = 5;
                    clear.Auto = true; ;
                }
                else
                {
                    gameObject.AddComponent<ClearObj>();
                    var clear = GetComponent<ClearObj>();
                    clear.enabled = true;
                    clear.TimeCoolDown = 5;
                    clear.Auto = true; ;
                }
            }
        }
        catch (System.Exception)
        {

           //Debug.Log("MainArt chua build");
        }
#endif
    }
    float timer = 0;
    private void FixedUpdate()
    {
        if (_myDetail.FollowPlayer)
        {
            timer += Time.deltaTime;
            if (timer < _myDetail.ThoiGianNo)
            {
                Vector3 temp = GameObject.FindGameObjectWithTag(TagManager.api.Player).transform.position;
                temp.y = transform.position.y;
                Vector3 dir = temp - transform.position;
                transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(dir), Time.deltaTime * _myDetail.SpeedMove);
            }
        }

        if (AllowMove)
        {
            if (!_myDetail.ShootDown)
                _rb.velocity = (transform.forward + _offsetDirBullet * Time.deltaTime * _myDetail.SpeedMove) * _myDetail.SpeedMove;
            else
                _rb.velocity = _offsetDirBullet * _myDetail.SpeedMove;
        }
    }


}
