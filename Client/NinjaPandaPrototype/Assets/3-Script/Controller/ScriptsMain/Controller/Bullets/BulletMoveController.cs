﻿using System.Collections;
using UnityEngine;

public class BulletMoveController : MonoBehaviour
{
    public MoveType BulletMoveType;

    [Header("General")]
    public Transform Target;
    public Transform Bullet;
    public float BulletMoveSpeed = 1f;
    public bool IsAutoStart;

    private float _globalSpeedPenalty = 1;
    private Rigidbody _rb;
    public bool _isMoving;

    [Header("Curve Attributes")]
    public float Curve_Speed = 2;
    public float Curve_Offset = 2;
    public bool Curve_Reverse_Rotation;

    private float _curve_Distance;
    private float _curve_angle;
    private float _curveMove;
    private float _curveTotalTime;
    private Vector3 _curveCenter;

    [Header("ZigZag Attributes")]
    public float ZigZag_Speed = 2;
    public float ZigZag_Offset = 1;
    public bool ZigZag_Reverse_Rotation;

    [Header("Circle Attributes")]
    public float Circle_Speed = 2f;
    public float Circle_StartAngle = 0;
    public float Circle_Distance = 2f;
    public bool Circle_Reverse_Rotation;

    private float _circleAngle;

    private float _changeDistanceSpeed;
    private float _minDistance = 1f;
    private float _maxDistance = 10f;
    private bool _isInit;


    public void SetUpNoneMove()
    {
        BulletMoveType = MoveType.None;
        StartMoving();
    }

    public void SetUpStraightMove(Transform target, float bulletSpeed)
    {
        BulletMoveType = MoveType.Straight;

        Target = target;
        BulletMoveSpeed = bulletSpeed;

        StartMoving();
    }


 
        public void SetUpCurveMove(Transform target, float curveSpeed,float curveOffset, float damage = 0, bool curveReverse = false)
    {
        BulletMoveType = MoveType.Curve;
        Bullet.GetComponent<BulletDetail>().Damage = damage;
        Target = target;
        Curve_Speed = curveSpeed;
        Curve_Offset = curveOffset;
        Curve_Reverse_Rotation = curveReverse;

        StartMoving();
      
    }

    public void SetUpZigZagMove(Transform target, float zigzagSpeed, float zigzagOffset, bool zigzagReverse)
    {
        BulletMoveType = MoveType.ZigZag;

        Target = target;
        ZigZag_Speed = zigzagSpeed;
        ZigZag_Offset = zigzagOffset;
        ZigZag_Reverse_Rotation = zigzagReverse;

        StartMoving();
    }

    public void SetUpCircleMove(float circleSpeed, float circleStartAngle, float circleDistance, bool circleReverse)
    {
        BulletMoveType = MoveType.Circle;

        Circle_Speed = circleSpeed;
        Circle_StartAngle = circleStartAngle;
        Circle_Distance = circleDistance;
        Circle_Reverse_Rotation = circleReverse;

        //Vector3 mov = new Vector3(Mathf.Cos(_circleAngle * Mathf.Deg2Rad) * Circle_Distance, 0, Mathf.Sin(_circleAngle * Mathf.Deg2Rad) * Circle_Distance);
        //transform.localPosition = mov;
        StartMoving();
    }

    public void SpeedUpCircle(float speed)
    {
        if (BulletMoveType == MoveType.Circle)
            Circle_Speed = speed;
    }

    public void CircleIn(float speed)
    {
        if (BulletMoveType == MoveType.Circle)
            _changeDistanceSpeed = -speed;
    }

    public void CircleOut(float speed)
    {
        if (BulletMoveType == MoveType.Circle)
            _changeDistanceSpeed = speed;
    }

    public void CircleStop()
    {
        if (BulletMoveType == MoveType.Circle)
            _changeDistanceSpeed = 0;
    }

    public void SetGlobalSpeedPenalty(float speed)
    {
        _globalSpeedPenalty = speed;
    }

    private void StartMoving()
    {
        if (Target != null)
            transform.LookAt(Target);

        _rb = Bullet.GetComponent<Rigidbody>();
        if (_rb == null)
            _rb = Bullet.gameObject.AddComponent<Rigidbody>();

        _rb.useGravity = false;
        _rb.freezeRotation = true;

        switch (BulletMoveType)
        {
            case MoveType.None:
                {
                    BulletMoveSpeed = 0;
                    break;
                }
            case MoveType.Straight:
                {
                    break;
                }
            case MoveType.Curve:
                {
                    BulletMoveSpeed = 0;
                    _curveCenter = new Vector3((Target.position.x + transform.position.x) / 2, (Target.position.y + transform.position.y) / 2, (Target.position.z + transform.position.z) / 2);
                    _curve_Distance = Vector3.Distance(Target.position, transform.position) / 2;
                    _curve_angle = -1.5f;

                    transform.position = _curveCenter;

                    Curve_Speed *= _globalSpeedPenalty;
                    break;
                }
            case MoveType.ZigZag:
                {
                    break;
                }
            case MoveType.Circle:
                {
                    BulletMoveSpeed = 0;
                    _circleAngle = Circle_StartAngle;
                    break;
                }
        }
        BulletMoveSpeed *= _globalSpeedPenalty;
        _isMoving = true;
    }

   

    void Update()
    {
        if (!_isMoving)
            return;
        transform.Translate(transform.forward.x * Time.deltaTime * BulletMoveSpeed, 0, transform.forward.z * Time.deltaTime * BulletMoveSpeed, Space.World);
        if (_rb != null)
        {
            Vector3 mov = Vector3.zero;
            switch (BulletMoveType)
            {
                case MoveType.Curve:
                    {
                        if (Mathf.Abs(_curve_angle) >= 4.5f)
                            Destroy(gameObject);

                        mov = new Vector3(Mathf.Cos(_curve_angle) * _curve_Distance / Curve_Offset, 0, Mathf.Sin(_curve_angle) * _curve_Distance);
                        if (!Curve_Reverse_Rotation)
                            _curve_angle += Time.deltaTime * Curve_Speed;
                        else
                            _curve_angle -= Time.deltaTime * Curve_Speed;
                        break;
                    }
                case MoveType.ZigZag:
                    {
                        var lifeTime = Time.time;
                        if (ZigZag_Reverse_Rotation)
                            lifeTime = -lifeTime;
                        mov = new Vector3(Mathf.Sin(ZigZag_Speed * lifeTime) * ZigZag_Offset, Bullet.transform.localPosition.y, Bullet.transform.localPosition.z);
                        break;
                    }
                case MoveType.Circle:
                    {
                        if (_changeDistanceSpeed != 0)
                        {
                            if (Circle_Distance > _minDistance && _changeDistanceSpeed < 0)
                                Circle_Distance += _changeDistanceSpeed;
                            else if (Circle_Distance < _maxDistance && _changeDistanceSpeed > 0)
                                Circle_Distance += _changeDistanceSpeed;
                            else
                                _changeDistanceSpeed = 0;
                        }
                      //  mov = new Vector3(Mathf.Cos(_circleAngle) * Circle_Distance, 0, Mathf.Sin(_circleAngle) * Circle_Distance);
                        mov = new Vector3(Mathf.Cos(_circleAngle *Mathf.Deg2Rad) * Circle_Distance , 0, Mathf.Sin(_circleAngle * Mathf.Deg2Rad) * Circle_Distance);
                    
                        if (!Circle_Reverse_Rotation)
                            _circleAngle += Time.deltaTime * Circle_Speed;
                        else
                            _circleAngle -= Time.deltaTime * Circle_Speed;
                        if (_circleAngle >= 360)
                            _circleAngle = 0;
                       
                        break;
                    }
            }
            Bullet.transform.localPosition = mov;

        }
    }
   


    public enum MoveType
    {
        None = 0,
        Straight = 1,
        Curve = 2,
        ZigZag = 3,
        Circle = 4
    }
}