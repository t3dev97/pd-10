﻿using UnityEngine;

public class RangeDamageController : MonoBehaviour
{
    public GameObject Target;
    private void OnTriggerStay(Collider obj)
    {
        string sTag = obj.transform.tag;
        if (sTag == TagManager.api.Player)
        {
            Target = obj.gameObject;
        }
        else
        {
            if(sTag == TagManager.api.Shield)
            {
                Destroy(this);
            }
        }
    }
}
