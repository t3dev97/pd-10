﻿using System.Collections;
using UnityEngine;
public class CuaDoController : MonoBehaviour
{
    public GameObject bullet;

    MonsterDetail _myDetail;
    Rigidbody _rb;
    CheckInGround _checkIsGround;

    float _timer = 0;
    float _timerCoolDownATK;
    Vector3 _posTarget;

    Animator _animator;
    string _lastAnimation;
    string[] _animations = new string[] { "Idle", "Run", "Atk", "Hit", "Die" }; // các trạng thái aniamtino nhân vật 
    bool _inGround = false;
    bool _checkAtk = false; // đang trong trạng thái Attack
    bool _checkHit = false; // đang trong trạng thái Hit
    bool _checkRun = false;  // đang trong trạng thái Run
    bool _checkIdle = false; // đang ở trong trạng thái nghĩ
    bool _checkFind = true; // cho phép tìm đường
    private void Awake()
    {
        _myDetail = GetComponent<MonsterDetail>();
        _rb = GetComponent<Rigidbody>();
        _animator = GetComponent<Animator>();
        _checkIsGround = transform.GetChild(0).GetComponent<CheckInGround>();
    }

    private void Update()
    {
        _timer += Time.deltaTime;
        if (_timer > _timerCoolDownATK) // bắt đầu tấn công
        {
            _timerCoolDownATK = Random.Range(_myDetail.CurrentCoolDownMax, _myDetail.CurrentCoolDownMax);
            Debug.Log(_timerCoolDownATK);
            RandomDir();
            _timer = 0; _checkAtk = true;
            RunAnimation("Atk");
        }
        else
        {
            if (_checkIsGround.InGround)
            {
                transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(_posTarget - transform.position), Time.deltaTime);
                RunAnimation("Run");
                Move();
            }
        }
    }
    void Atk()// được gọi ở cuối animation ATK
    {
        //Debug.Log("Atk");
        ShotSkill(transform, 8);
    }
    void DoneAtk() // được gọi trong animation ATK
    {
        //StartCoroutine(SetRun());
    }
    IEnumerator SetRun()
    {
        yield return new WaitForSeconds(1f);
        _checkAtk = false;
    }
    void ShotSkill(Transform originBulletTrans, int numberBullet) // thêm 2 tia phía trước
    {
        GameObject temp;
        Vector3 pos = transform.position;
        float angle = 360 / numberBullet;
        float angleY = originBulletTrans.rotation.eulerAngles.y;
        for (int i = 0; i < numberBullet; i++)
        {
            temp = Instantiate(bullet, pos, transform.rotation); // bắn đạn
            temp.transform.eulerAngles = new Vector3(0, angleY += angle, 0);
            //temp.GetComponent<BulletCuaController>().speedMove = _myDetail.BasicASRangeUnit;
        }
    }

    void RandomDir()
    {
        Vector3 temp = transform.position;
        temp.x += Random.Range(-10, 10);
        temp.z += Random.Range(-10, 10);
        _posTarget = temp;
    }

    void Move()
    {
        _rb.velocity = transform.forward * _myDetail.CurrentSpeedMove;
        RunAnimation("Run");
    }
    void RunAnimation(string name)
    {
        foreach (var item in _animations)
        {
            if (_animator.GetBool(item) == true)
                _lastAnimation = item;
            _animator.SetBool(item, false);
        }
        _animator.SetBool(name, true);
    }

    //public void Hit()


}
