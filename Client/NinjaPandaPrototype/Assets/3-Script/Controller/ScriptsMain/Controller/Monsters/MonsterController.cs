﻿using System.Collections;
using UnityEngine;
using UnityEngine.AI;
public enum EnumCondition
{
    distance,
    timer
}
public enum EnumAnimation
{
    Idle,
    Run,
    Atk,
    Hit,
    Die
}
public class MonsterController : MonoBehaviour
{
    public bool FollowPlayer; // true: tìm và di chuyển đến player, false: di chuyển ngẫu nhiên
    public EnumCondition ATKCondition;
    public bool CheckAtk = false; // đang trong trạng thái Attack
    public bool CheckMoveBasic = true; // đang trong trạng thái Attack
    public bool CheckDie = false; // cho phép tìm đường 
    public int CheckMoveSkill = 0;
    public Vector3 DirMove;
    public float TimeStartATK; // thời điểm bắt đầu thực hiện tấn công 
    public bool myAction = false;
    public CheckInGround MyCheckInGround; // kiểm tra đang chạm đất

    public SkinnedMeshRenderer Mesh;
    public SkinnedMeshRenderer ShadowMesh;
    Transform _player;
    MonsterDetail _myDetail; // chứa tất cả thông tin về Monster
    MonsterMove_V2 _MoveSkill;

    [SerializeField]
    float _timeAtk; // thời gian của 1 lần tấn công

    // Animation
    Animator _amin;
    string[] _nameAmin = new string[] { "Idle", "Run", "Atk", "Hit", "Die" }; // các trạng thái aniamtino nhân vật 
    bool _checkHit = false; // đang trong trạng thái Hit
    bool _checkRun = false;  // đang trong trạng thái Run
    bool _checkIdle = false; // đang ở trong trạng thái nghĩ
    bool _checkFind = true; // cho phép tìm đường 
    float _timeInGround = 0;
    Rigidbody _rb;
    NavMeshAgent _agent;
    bool inGround = false;
    public bool InGround { get => inGround; set => inGround = value; }
    private void Awake()
    {
        if (GetComponent<MonsterDetail>() == null)
            gameObject.AddComponent<MonsterDetail>();

        _amin = GetComponent<Animator>();
        _myDetail = GetComponent<MonsterDetail>();
        _rb = GetComponent<Rigidbody>();
        _MoveSkill = GetComponent<MonsterMove_V2>();
        _agent = GetComponent<NavMeshAgent>();
        if (GetComponent<InGameGoldSpawner>() == null)
            gameObject.AddComponent<InGameGoldSpawner>();

    }
    private void Start()
    {
        _player = GameObject.FindGameObjectWithTag(TagManager.api.Player).transform;
    }
    // Live
    private void Update()
    {
        if (MyCheckInGround == null)
        {
            myAction = true;
        }
        else

            if (MyCheckInGround.InGround == true && myAction == false)
        {
            //_rb.useGravity = false;
            myAction = true;
        }
        // khi chạm đất lần đầu
        if (myAction == false) return;
        // nếu FollowPlayer == true
        // => tìm vị trí player
        // => kiểm tra điều kiện tấn công (điều kiện có thể: khoản cách or thời gian)
        //      => có thể tấn công => không di chuyển => tiến hành tấn công => kiểm tra điều kiện tấn công 
        //      => không thể tấn công => chi chuyển đến player
        // => di chuyển đến player

        // nếu FollowPlayer == false
        // => random hướng di chuyển
        // => kiểm tra điều kiện tấn công (điều kiện có thể: khoản cách or thời gian)
        //      => có thể tấn công => không di chuyển => tiến hành tấn công => kiểm tra điều kiện tấn công 
        //      => không thể tấn công => di chuyển theo hướng random
        // => di chuyển theo hướng random
        if (CheckDie)
        {
            CheckAtk = false;
            CheckMoveBasic = false;
            _agent.enabled = false;
            Destroy(gameObject, 2.0f);
            return;
        }
        else
        if (_checkHit == false)
        {
            if (FollowPlayer)
            {
                //transform.LookAt(_player);

                Vector3 target = new Vector3(_player.position.x, transform.position.y, _player.position.z);
                transform.LookAt(target);

                switch (ATKCondition)
                {
                    case EnumCondition.timer:
                        {
                            if (Time.time - TimeStartATK > _myDetail.CurrentCoolDownMax) // tại thời điểm 5s sẻ tấn công
                            {
                                //
                                // kiểm tra xe có skill move không,
                                //if (_MoveSkill != null)
                                //    CheckMoveSkill = true;
                                // nếu có thì thực hiện skill move,
                                {
                                    // thực hiện skill move
                                }
                                // ngược lại thì thực hiện atk
                                {
                                    if (CheckAtk) break; // đang thực hiện atk
                                    CheckAtk = true; // cho phép tấn công
                                    CheckMoveBasic = false; // không cho phép di chuyển
                                }
                            }
                            else
                            if (CheckAtk == false && _checkHit == false) // Check atk = true, còn đang tấn công
                            {
                                CheckMoveBasic = true;
                            }
                            else
                            {
                                RunAnimation("Idle");
                            }
                            break;
                        }
                    case EnumCondition.distance: // sử dụng AI
                        {
                            //Debug.Log(_myDetail.RangeATK);
                            if (Vector3.Distance(_player.position, transform.position) < _myDetail.RangeAtk) // đế cự ly có thể tấn công
                            {
                                //
                                // kiểm tra xem có skill move không, 
                                if (_MoveSkill != null)
                                {
                                    ++CheckMoveSkill;
                                }
                                // nếu có thì thực hiện skill move,
                                {
                                    // thực hiện skill move
                                    Debug.Log(CheckAtk);
                                }
                                if (CheckAtk)
                                // ngược lại thì thực hiện atk
                                {
                                    if (CheckAtk) break; // đang thực hiện atk
                                    CheckAtk = true; // cho phép tấn công
                                    CheckMoveBasic = false; // không cho phép di chuyển
                                }
                            }
                            else
                            if (CheckAtk == false && _checkHit == false)
                            {
                                CheckMoveBasic = true;
                            }
                            break;
                        }
                }
            }
            else // hướng ngẫu nhiên
            {
                transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(DirMove - transform.position), Time.deltaTime);
                switch (ATKCondition)
                {
                    case EnumCondition.timer:
                        {
                            if (Time.time - TimeStartATK > _myDetail.CurrentCoolDownMax) // tại thời điểm 5s sẻ tấn công
                            {
                                //
                                // kiểm tra xe có skill move không, 
                                // nếu có thì thực hiện skill move,
                                {
                                    // thực hiện skill move
                                }
                                // ngược lại thì thực hiện atk
                                {
                                    if (CheckAtk) break; // đang thực hiện atk
                                    RandomDir();
                                    CheckAtk = true; // cho phép tấn công
                                    CheckMoveBasic = false; // không cho phép di chuyển
                                }
                            }
                            else
                            if (CheckAtk == false && _checkHit == false) // Check atk = true, còn đang tấn công
                            {
                                CheckMoveBasic = true;
                            }
                            else
                            {
                                RunAnimation("Idle");
                            }
                            break;
                        }
                    case EnumCondition.distance:
                        {
                            break;
                        }
                }
            }
        }

        // di chuyển _myDetail.BasicCoolDownMax thì ATK, ATK kết thúc thì MOVE

    }
    // Action
    #region Idle
    // trạng thái nghỉ

    #endregion

    #region Run
    // trạng thái di chuyển
    #endregion

    #region AtkBasic
    // trạng thái tấn công
    private void OnTriggerEnter(Collider obj)
    {
        string sTag = obj.tag;
        if (sTag == TagManager.api.Player)
        {
            obj.GetComponent<PlayerController>().Hit(_myDetail.CurrentATKDamage, _myDetail.RaceType, _myDetail.AtkType);
        }
    }
    #endregion

    #region Hit
    // trạng thái bị tấn công
    public enum MonsterMoveType
    {
        Ground,
        Air,
        Water
    }
    MonsterMoveType _moveType;
    public enum MonsterAttackType
    {
        Range,
        Melee
    }
    MonsterAttackType _atkType;
    public MonsterMoveType CheckMonsterMoveType()
    {
        return _moveType;
    }
    public MonsterAttackType CheckMonsterAtkType()
    {
        return _atkType;
    }
    public void TakeDamage(float damage, Vector3 pos)
    {
        //RandomDir();
        //Debug.Log(CheckAtk);
        //Debug.Log(CheckMove);
        _myDetail.CurrentHP -= damage;
        CheckAtk = false;
        CheckMoveBasic = false;
        _checkHit = true;
        //RunAnimation("Hit");
        if (_myDetail.CurrentHP <= 0)
        {
            CheckDie = true;
            Die();
            return;
        }
        //StartCoroutine(Delay(pos));
    }

    public void ActiveGlow(int Active)
    {
        if (Mesh == null)
            return;
        var mat = Mesh.material;
        mat.SetFloat("_Active_Glow_Time", Time.timeSinceLevelLoad);
    }
    #endregion
    IEnumerator Delay(Vector3 pos)
    {
        pos.y = transform.position.y;
        for (int i = 0; i < 10; i++)
        {
            transform.Translate((pos - transform.position).normalized * PlayerDetail.api.RepelForceInStage * Time.fixedDeltaTime);
            yield return new WaitForFixedUpdate();
        }
        //yield return new WaitForSeconds(0.1f);

        //Debug.Log(_checkHit);
        _checkHit = false;
        CheckMoveBasic = true;
        RunAnimation("Idle");
        //Debug.Log("Hit");
    }
    #region Die
    // trạng thái chết

    public void Die()
    {
        gameObject.tag = TagManager.api.MonsterDie;
        //StartCoroutine(FindMonster.api.Find());
        RunAnimation("Die");
        CheckDie = true;
        GetComponent<CapsuleCollider>().enabled = false;
        _rb.velocity = Vector3.down;

        var goldSpawner = GetComponent<InGameGoldSpawner>();
        if (goldSpawner != null)
            goldSpawner.SpawnGold(Random.Range(3, 8), _agent, _player.transform);
        _agent.enabled = false;

        if (ShadowMesh == null)
            return;
        ShadowMesh.gameObject.SetActive(true);
        var shadowmat = ShadowMesh.material;
        shadowmat.SetFloat("_Active_dead_glow_time", Time.timeSinceLevelLoad);
    }
    #endregion

    void RunAnimation(string name)
    {
        foreach (var item in _nameAmin)
        {
            _amin.SetBool(item, false);
        }
        _amin.SetBool(name, true);
    }
    void RunAnimation(EnumAnimation name)
    {
        foreach (var item in _nameAmin)
        {
            _amin.SetBool(item.ToString(), false);
        }
        _amin.SetBool(name.ToString(), true);
    }
    void RandomDir()
    {
        Vector3 temp = transform.position;
        temp.x += Random.Range(-5, 5);
        temp.z += Random.Range(-5, 5);
        DirMove = temp;
        //Debug.Log(temp);
    }


    private void OnCollisionStay(Collision obj)
    {
        if (obj.transform.tag == TagManager.api.Ground)
        {
            inGround = true;
        }
    }
}
