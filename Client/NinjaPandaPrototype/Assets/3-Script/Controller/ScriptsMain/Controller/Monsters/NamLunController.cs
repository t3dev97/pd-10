﻿using System.Collections;
using UnityEngine;
using UnityEngine.AI;
public class NamLunController : MonoBehaviour
{
    [SerializeField]
    bool isDouble; // nếu isDouble = true, thì khi chết sẻ tạo 2 nấm con

    [SerializeField]
    GameObject[] listNamLunMini;

    [Tooltip("vị trí random xung quanh khi sinh nấm con")]
    [SerializeField]
    Vector3 posOffset;// vị trí random khi sinh nấm con

    Transform player;

    [SerializeField]
    GameObject hitEff;

    public SkinnedMeshRenderer Mesh;
    public SkinnedMeshRenderer ShadowMesh;

    MonsterDetail myDetail; // chứa các thuộc tính nhân vật: tốc độ, sát thương, máu...
    Rigidbody rb;
    //AI
    NavMeshAgent agent; // dùng thuật toán AI Unity

    // Animation
    Animator animator;
    string[] animations = new string[] { "Idle", "Run", "Atk", "Hit", "Die" }; // các trạng thái aniamtino nhân vật 
    bool checkAtk = false; // đang trong trạng thái Attack
    bool checkHit = false; // đang trong trạng thái Hit
    bool checkRun = false;  // đang trong trạng thái Run
    bool checkIdle = false; // đang ở trong trạng thái nghĩ
    bool checkFind = true; // cho phép tìm đường 

    private void Awake()
    {
        myDetail = GetComponent<MonsterDetail>();
        animator = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();
        rb = GetComponent<Rigidbody>();
    }
    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        agent.SetDestination(player.position);
        timeLastAtk = Time.time;
        myDetail.RangeAtk = 1.5f;
    }

    private void Update()
    {
        if (player == null || player.gameObject.activeSelf == false) // nếu không tìm thấy player thì về tt Idle và kết thúc không làm gì cả 
        {
            //Debug.Log(gameObject.name + " Not Player");
            agent.isStopped = true;
            RunAnimation("Idle");
            return;
        }
        if (myDetail.CurrentHP <= 0)
        {
            //Debug.Log(gameObject.name + "DIE_U");
            return;
        }
        // tìm thấy player
        float distance = Vector3.Distance(transform.position, player.position); // tính khoản cách đến nhân vật
        if (distance < myDetail.RangeAtk) // nếu khoản cách hiện tại nhỏ hơn phạm vi tấn công
        {
            checkFind = false; // không cho phép tìm đường, vào trạng thái atk
            Attack();
        }
        else
        {
            checkFind = true;
        }
    }
    private void LateUpdate()
    {
        if (myDetail.CurrentHP <= 0)
        {
            agent.isStopped = true;
            //Debug.Log(gameObject.name + " DIE_F");
            return;
        }
        if (checkFind)
        {
            //agent.enabled = true;
            agent.isStopped = false;
            float distance = Vector3.Distance(transform.position, player.position); // tính khoản cách đến nhân vật
            if (distance > myDetail.RangeAtk / 2)
                Run();
        }
        else
        {
            agent.isStopped = true;
            Idle();
        }
    }

    #region atk
    float timeLastAtk = 0; // thời gian lần tất công trước đó
    void Attack()
    {

        if (checkHit)
        {
            //Debug.Log(gameObject.name + ": Hitting");
            //return;
        }
        if (Time.time - timeLastAtk > myDetail.CurrentASMeeleUnit) // nếu thời gian hiện tại - thời gian atk trước đó > thời gian giản cách tất công, thì thực hiện tất công
        {
            PlayerController.api.Hit(myDetail.CurrentATKDamage);
            timeLastAtk = Time.time; // lưu thời gian lần atk hiện tại
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(player.position - transform.position), 0.1f); // xoay nhân vật vào hướng player
            //agent.SetDestination(transform.position);
            RunAnimation("Atk"); // chạy animaton atk
        }
    }
    void BeginAtk() // được gọi khi mới vào animation atk 
    {
        //Debug.Log(gameObject.name + " BeginAtk");
        checkAtk = true;

        StartCoroutine(MoveTo()); // thực hiện di chuyển đến player trong khi animation atk đang chạy, chạy cùng nhau
    }
    IEnumerator MoveTo()
    {
        transform.LookAt(player);
        //rb.AddForce(transform.up * 300);
        transform.Translate(Vector3.forward * myDetail.SpeedMoveAtk * Time.fixedDeltaTime);
        yield return new WaitForFixedUpdate();
        if (checkAtk) // nếu animation atk vẫn còn đang thực thi thì vẫn tiếp tục di chuyển về hướng player
            StartCoroutine(MoveTo());
    }
    void EndAtk() // được gọ khi gần kết thúc animaton atk
    {
        //Debug.Log(gameObject.name + " EndAtk");
        checkAtk = false;
        if (atkDamage)
        {
            //Debug.Log(gameObject.name + " atkDamage: " + atkDamage);
            atkDamage = false;
            //PlayerController.api.Hit(myDetail.DamageAtk);
        }
        checkFind = true; //  phép tìm đường, khi kết thúc animtion 
    }
    #endregion

    #region Hit
    public void Hit()
    {
        checkFind = false;
        //Debug.Log(myDetail);
        myDetail.CurrentHP -= PlayerDetail.api.DamageAtkInStage;
        ShowHitDamage(PlayerDetail.api.DamageAtkInStage, Color.red); // hiện hiệu ứng mất máu

        if (myDetail.CurrentHP <= 0)
        {
            Die();
        }
        else
            RunAnimation("Hit");
    }

    Vector3 dir; // hướng đi lui khi bị đạn bắn trúng
    public void BeginHit()
    {
        checkHit = true;
        //Debug.Log(gameObject.name + " BeginHit");
        dir = player.position - transform.position;
        dir.y = transform.position.y;
        dir = dir.normalized;
        StartCoroutine(Repelling());
    }
    IEnumerator Repelling()
    {
        //Debug.Log(gameObject.name + " Repelling" + agent.isStopped);
        //rb.AddForce(-transform.forward * 20);
        transform.Translate(dir * PlayerDetail.api.RepelForceInStage * Time.fixedDeltaTime);
        //rb.velocity = -Vector3.back * 10;

        yield return new WaitForFixedUpdate();
        if (checkHit) // nếu animation atk vẫn còn đang thực thi thì vẫn tiếp tục di chuyển về hướng player
            StartCoroutine(Repelling());
    }
    public void EndHit()
    {
        checkHit = false;
        checkFind = true;
        //hitEff.SetActive(false);// tắt hiệu ứng hit

        //Debug.Log(gameObject.name + " EndHit");

    }
    #endregion

    float timeLastRun = 0;
    void Run()
    {
        agent.SetDestination(player.position);
        //Debug.Log(checkHit + "_" + checkFind);
        //if (checkFind)
        RunAnimation("Run");
    }

    void Die()
    {
        var goldSpawner = GetComponent<InGameGoldSpawner>();
        if (goldSpawner != null)
            goldSpawner.SpawnGold(Random.Range(3, 8), agent, player.transform);

        if (ShadowMesh == null)
            return;
        ShadowMesh.gameObject.SetActive(true);
        var shadowmat = ShadowMesh.material;
        shadowmat.SetFloat("_Active_dead_glow_time", Time.timeSinceLevelLoad);

        gameObject.tag = TagManager.api.MonsterDie;
        agent.isStopped = true;
        checkFind = false; // không cho tìm đường nữa
        myDetail.HealtBar.SetActive(false); // Tắt thanh máu trước khi chạy animation die
        //Debug.Log(gameObject.name + " Die");
        GetComponent<Collider>().enabled = false; // tắt va chạm vật lý để các đối tượng khác đi qua
        transform.tag = TagManager.api.MonsterDie;
        //StartCoroutine(FindMonster.api.Find()); // tìm quái mới cho player
        DisableAllComponent();
        RunAnimation("Die");
    }
    private void DisableAllComponent()
    {
        foreach (Collider c in GetComponents<Collider>())
            c.enabled = false;
    }
    public void EndDie() // được gọi khi anmation die kết thúc
    {
        Destroy(gameObject);
        //gameObject.SetActive(false);
    }
    IEnumerator _Jump()
    {
        bool temp = agent.isStopped;
        //Debug.Log(gameObject.name + "Jump " + agent.isStopped);
        //agent.isStopped = false;
        yield return new WaitForSeconds(1.0f);
        //agent.isStopped = temp;
    }
    public void Jum()
    {
        rb.AddForce(transform.up * 400);
    }
    void RunAnimation(string name)
    {
        foreach (var item in animations)
        {
            animator.SetBool(item, false);
        }
        animator.SetBool(name, true);
    }

    void ShowHitDamage(float damage, Color color)
    {

        GameObject eff = Instantiate(hitEff, transform.position, Quaternion.identity);// Hiệu ứng hit
        TextMesh textMesh = eff.GetComponent<TextMesh>();
        textMesh.text = damage.ToString();
        textMesh.color = color;
        eff.SetActive(true); // bật hiệu ứng hit, hiệu ứng tự tắt trong thời gian quy định
    }

    public void CreateNamMini() // được gọi khi kết thúc anmation Die
    {
        if (isDouble)
        {
            foreach (var item in listNamLunMini)
            {
                Vector3 posCreate = transform.position; // khởi tạo điểm sinh nấm con
                posCreate.x += Random.Range(-posOffset.x, posOffset.x); // thiết lập lại vị trí theo độ lệch ngẫu nhiên 
                posCreate.z += Random.Range(-posOffset.z, posOffset.z);
                Instantiate(item, posCreate, Quaternion.identity, transform.parent);
            }
        }
    }

    void Idle()
    {
        RunAnimation("Idle");
    }
    public void ActiveGlow(int Active)
    {
        if (Mesh == null)
            return;
        var mat = Mesh.material;
        mat.SetFloat("_Active_Glow_Time", Time.timeSinceLevelLoad);
    }

    GameObject objCollider;
    private void OnTriggerEnter(Collider obj)
    {
        //string sTag = obj.transform.tag;
        //if (sTag == TagManager.api.Bullet)
        //{
        //    objCollider = obj.gameObject;
        //    Hit();
        //}
    }

    // HitDamage Player
    public bool atkDamage = false; // phát hiện va chạm đúng player, gây sát thương lên player
    //private void OnTriggerStay(Collider obj)
    //{
    //    string sTag = obj.transform.tag;
    //    if (sTag == TagManager.api.Player)
    //    {
    //        //Debug.Log(gameObject.name + " Chạm");
    //        atkDamage = true;
    //    }
    //}
}
