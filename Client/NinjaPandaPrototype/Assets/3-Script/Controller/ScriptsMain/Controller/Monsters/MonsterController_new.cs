﻿using Studio;
using Studio.BC;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class MonsterController_new : MonoBehaviour
{
    public bool FollowPlayer; // true: tìm và di chuyển đến player, false: di chuyển ngẫu nhiên
    public bool CheckAtk = false; // đang trong trạng thái Attack
    public bool CheckDie = false; // cho phép tìm đường 
    public float TimeStartATK; // thời điểm bắt đầu thực hiện tấn công 
    public Transform _player;
    public MonsterDetail _myDetail; // chứa tất cả thông tin về Monster

    [Header("Skill Hide")]
    public bool isPoision = false;
    public bool isIce = false;
    public bool isBlast = false;
    public bool isCold = false;
    public CheckInGround ObjCheckInGround;

    [Header("Effect Hit")]
    [SerializeField]
    SkinnedMeshRenderer Mesh;

    [SerializeField]
    SkinnedMeshRenderer ShadowMesh;

    [SerializeField]
    Animator _amin;

    MonsterSkillController _MonsterSkill1;
    string[] _nameAmin = new string[] { "Idle", "Run", "Atk", "Hit", "Die" }; // các trạng thái aniamtino nhân vật 
    bool _checkHit = false; // đang trong trạng thái Hit
    bool _doneSetupData = false;
    bool _inGround = false;
    Rigidbody _rb;
    NavMeshAgent _agent;
    GameObject _dieEffect;
    Transform _containerEF;
    BC_ObjectInfo _info;

    private void Awake()
    {
        if (GetComponent<MonsterDetail>() == null)
            gameObject.AddComponent<MonsterDetail>();

        if (_amin == null)
            _amin = GetComponent<Animator>();

        _myDetail = GetComponent<MonsterDetail>();

        if (GetComponent<Rigidbody>() != null)
            _rb = GetComponent<Rigidbody>();

        _agent = GetComponent<NavMeshAgent>();
        _MonsterSkill1 = GetComponent<MonsterSkillController>();

        if (GetComponent<InGameGoldSpawner>() == null)
            gameObject.AddComponent<InGameGoldSpawner>();

        if (GetComponent<BC_ObjectInfo>() != null)
        {
            _info = GetComponent<BC_ObjectInfo>();
        }
        _containerEF = BCCache.Api.transform;
        if (Mesh != null)
        {
            textureOld = Mesh.material.GetTexture("_MatCap");
            material = Mesh.materials[0];
        }

        StartCoroutine(SetupData());
    }
    private void Start()
    {
        if (_myDetail.isHopQua)
        {
            transform.localRotation = Quaternion.Euler(0, 0, 0);
        }

        StartCoroutine(SetupData());
    }
    IEnumerator SetupData()
    {
        yield return new WaitForSeconds(.5f);
        _player = GameObject.FindGameObjectWithTag(TagManager.api.Player).transform;
        if (_rb != null)
            _rb.mass = 1;

        transform.LookAt(_player);
        _timeCoolDownMaxDefault = _myDetail.BasicCoolDownMax;
        _timeCoolDownMinDefault = _myDetail.BasicCoolDownMin;
        _doneSetupData = true;
    }
    [SerializeField]
    Material material;

    [SerializeField]
    Texture textureOld;

    [SerializeField]
    Texture textureHit;

    [SerializeField]
    Texture textureIce;
    // Live
    private void Update()
    {
        if (GameManager.api.GameState == GAME_STATE.Playing)
        {
            if (_doneSetupData == false)
                return;
            if (CheckDie)
            {
                _agent.enabled = false;
                Destroy(gameObject, 3f);
                return;
            }
        }
        if (BC_Chapter_Info.api.AllowMonsterAtk == false)
        {
            RunAnimation("Idle");
        }
    }


    #region Idle
    // trạng thái nghỉ

    #endregion

    #region Run
    // trạng thái di chuyển
    #endregion
    #region AtkBasic
    // trạng thái tấn công
    private void OnTriggerEnter(Collider obj)
    {
        string sTag = obj.tag;
        if (sTag == TagManager.api.Player)
        {
            obj.GetComponent<PlayerController>().Hit(_myDetail.CurrentATKDamage, _myDetail.RaceType, _myDetail.AtkType);
            StartCoroutine(obj.GetComponent<PlayerController>().PushBack(obj.transform.position, transform));
        }
    }
    #endregion
    public bool Hit = false;
    #region Hit
    // trạng thái bị tấn công
    public void TakeDamage(float damage, bool isCritical, bool isEffectDamage = false, bool hide = false)
    {
        if (!CheckDie)
        {

            if (!isIce)
                StartCoroutine(EffectHitLinght(0.01f, isEffectDamage));

            _checkHit = true;

            Hit = false;
            _checkHit = false;
            BCCache.Api.GetEffectPrefabAsync(_myDetail.EffectDamageType.ToString(), (go) =>
            {
                GameObject effectDamage = Instantiate(go, transform.position, Quaternion.identity, _containerEF);

                if (isEffectDamage)
                {
                    foreach (Transform item in effectDamage.transform)
                    {
                        if (item.GetComponent<ParticleSystem>() != null)
                        {
                            item.GetComponent<ParticleSystem>().enableEmission = false;
                        }
                    }
                }

                Vector3 vector = new Vector3(Random.Range(-2.0f, 2.0f), (_myDetail.OffSetSelectionTop.y * 2.5f) + Random.Range(-1.5f, 1.5f), 0);
                Vector3 vt = effectDamage.transform.GetChild(0).localPosition;
                vt.y += vector.y;
                effectDamage.transform.GetChild(0).localPosition = vt;

                if (!isCritical)
                {
                    effectDamage.transform.GetChild(0).GetComponent<TextMesh>().text = ((int)damage).ToString();
                    //effectDamage.transform.GetChild(0).localPosition += vector;

                    float xTarget = (Random.Range(0, 2) == 0) ? effectDamage.transform.GetChild(0).localPosition.x + 2.5f : effectDamage.transform.GetChild(0).localPosition.x - 2.5f;
                    LeanTween.value(effectDamage.transform.GetChild(0).localPosition.x, xTarget, 0.5f).setOnUpdate((float value) =>
                    {
                        if (effectDamage != null && effectDamage.transform.GetChild(0) != null)
                        {
                            Vector3 temp = effectDamage.transform.GetChild(0).localPosition;
                            temp.x = value;
                            effectDamage.transform.GetChild(0).localPosition = temp;
                        }
                    });
                }
                else
                {
                    CameraEffect.api.ShouldShake = true;
                    effectDamage.transform.GetChild(0).GetComponent<TextMesh>().text = LanguageManager.api.GetKey(BCLocalize.GUI_NAME_CRITICAL, new string[] { ((int)damage).ToString() });
                    //effectDamage.transform.GetChild(0).localPosition += vector;

                    float xTarget = (Random.Range(0, 2) == 0) ? effectDamage.transform.GetChild(0).localPosition.x + 2.5f : effectDamage.transform.GetChild(0).localPosition.x - 2.5f;
                    LeanTween.value(effectDamage.transform.GetChild(0).localPosition.x, xTarget, 0.5f).setOnUpdate((float value) =>
                     {
                         if (effectDamage != null && effectDamage.transform.GetChild(0) != null)
                         {
                             Vector3 temp = effectDamage.transform.GetChild(0).localPosition;
                             temp.x = value;
                             effectDamage.transform.GetChild(0).localPosition = temp;
                         }
                     });
                }

                if (hide)
                {
                    foreach (Transform tg in effectDamage.transform)
                    {
                        tg.gameObject.SetActive(false);
                    }
                }
            });
#if EDITOR
            if (BCApiTest.api != null)
            {
                if (BCApiTest.api.isGoToTest)
                    return;
            }
#endif
            _myDetail.CurrentHP -= damage;
            if (_myDetail.CurrentHP <= 0)
            {
                Die();
                return;
            }
            else if (!isEffectDamage)
            {
                _amin.SetTrigger("Hit_Trigger");
            }
        }
    }
    public void FXIce()
    {
        if (textureIce != null)
            Mesh.material.SetTexture("_MatCap", textureIce);
    }
    public void ResetFXIce()
    {
        Mesh.material.SetTexture("_MatCap", textureOld);
    }

    IEnumerator EffectHitLinght(float time, bool isEffectDamage)
    {
        if (!_myDetail.isHopQua)
        {
            if (isEffectDamage) yield return null;

            if (textureHit != null)
                Mesh.material.SetTexture("_MatCap", textureHit);
            yield return new WaitForSeconds(time);
            Mesh.material.SetTexture("_MatCap", textureOld);
        }
    }
    public void OneHitMonster()
    {
        _myDetail.CurrentHP = 0;
        BCCache.Api.GetEffectPrefabAsync(EffectKey.FXFloatingDamageRoBot, (go) =>
        {
            GameObject temp = Instantiate(go, transform.position, Quaternion.identity, _containerEF);

            temp.GetComponent<TextMesh>().text = LanguageManager.api.GetKey(BCLocalize.GUI_NAME_ONEHIT);
        });

        if (_myDetail.CurrentHP <= 0)
        {
            Die();
            //BC_Chapter_Info info = BC_DataGame.api.yumeMapEditor.GetComponent<BC_Chapter_Info>();
            //info.RemoveListStageMonster(gameObject);
            return;
        }
    }
    public void ActiveGlow()
    {
        if (Mesh == null)
            return;
        var mat = Mesh.material;
        mat.SetFloat("_Active_Glow_Time", Time.timeSinceLevelLoad);
    }
    public void ActiveDeadGlow()
    {
        if (ShadowMesh == null)
            return;
        ShadowMesh.gameObject.SetActive(true);
        var shadowmat = ShadowMesh.material;
        shadowmat.SetFloat("_Active_dead_glow_time", Time.timeSinceLevelLoad);
    }
    #endregion
    IEnumerator Delay(Vector3 pos)
    {
        pos.y = transform.position.y;

        for (int i = 0; i < 10; i++)
        {
            yield return new WaitForFixedUpdate();
        }

        Hit = false;
        yield return new WaitForSeconds(0.5f);
        _checkHit = false;
    }
    #region Die
    // trạng thái chết
    public void Die()
    {
        if (!CheckDie)
        {
            _amin.SetTrigger("Die_Trigger");
            BC_Chapter_Info info = BC_DataGame.api.yumeMapEditor.GetComponent<BC_Chapter_Info>();
            info.RemoveListStageMonster(gameObject);
            gameObject.tag = TagManager.api.MonsterDie;
            RunAnimation("Die");
            CheckDie = true;
            gameObject.layer = 22;
            if (_info != null && _info.style == STYLE_BRUSHES.boss)
            {
                BC_Chapter_Info.api.MoveGoldToPlayer();
            }

            if (_rb != null)
                _rb.velocity = Vector3.zero;

            _player = GameObject.FindWithTag(TagManager.api.Player).transform;

            if (gameObject.active)
            {
                StartCoroutine(FindMonster.api.Find());
            }

            BCCache.Api.GetEffectPrefabAsync(EffectKey.EnemyDieFX, (go) =>
            {
                _dieEffect = Instantiate(go);
                _dieEffect.SetActive(false);
            });

            if (BCApiTest.api.isGoToTest) return;

            var spawner = GetComponent<InGameGoldSpawner>();

            if (spawner != null && _myDetail.isDropItem)
            {
                if (_player != null)
                    spawner.SpawnGold(Random.Range((int)_myDetail.GoldNumberMin, (int)_myDetail.GoldNumberMax), _agent, _player.transform);
                #region Spawner Item/Heart
                // item
                float rb = Random.Range(0.0f, 1.0f);

                var InfoStage = BCCache.Api.DataConfig.StageInfos[BC_DataGame.api.currentIDMap];
                int monsterSuiteDropRate = InfoStage.MonsterSuite;
                //Debug.Log(monsterSuiteDropRate);
                if (rb < InfoStage.ItemDrop && monsterSuiteDropRate > 0)
                {
                    int rbIndexId = Random.Range(0, BCCache.Api.DataConfig.SuiteDropRateInfos[monsterSuiteDropRate].listSuiteDropRate.Count);

                    switch (BCCache.Api.DataConfig.SuiteDropRateInfos[monsterSuiteDropRate].listSuiteDropRate[rbIndexId].Key)
                    {
                        case "equipment":
                            {
                                int IdItem = BCCache.Api.DataConfig.SuiteDropRateInfos[monsterSuiteDropRate].listSuiteDropRate[rbIndexId].Value; // id cua item
                                int numberItem = BCCache.Api.DataConfig.SuiteDropRateInfos[monsterSuiteDropRate].listSuiteDropRate[rbIndexId].Quantity;
                                spawner.SpawnItem(BCCache.Api.DataConfig.EquipmentConfigData[IdItem], numberItem, _agent, _player.transform);

                                if (!BCCache.Api.DataConfig.ResourceGame.dataTemp.ListEquipmentsInStage.ContainsKey(IdItem))
                                    BCCache.Api.DataConfig.ResourceGame.dataTemp.ListEquipmentsInStage.Add(IdItem, 1);
                                else
                                    BCCache.Api.DataConfig.ResourceGame.dataTemp.ListEquipmentsInStage[IdItem]++;
                                break;
                            }
                        default:
                            {
                                break;
                            }
                    }
                }
                //--------------------------------
                // heart
                rb = Random.Range(0, 100);
                if (rb < _myDetail.HeartDropRate)
                {
                    spawner.SpawnHeart("38-HPBoost", (int)Random.Range(_myDetail.HeartNumberMin, _myDetail.HeartNumberMax), _agent, _player.transform);
                }
                #endregion
            }
        }
        BC_Chapter_Info.api.ListMonster.Remove(gameObject);
        BC_Chapter_Info.api.ChechkOpenDoor();
    }
    public void EndDie()
    {
        _dieEffect.transform.position = new Vector3(transform.position.x, transform.position.y + 1f, transform.position.z);
        _dieEffect.SetActive(true);
        Destroy(_dieEffect, 2f);
        Destroy(gameObject);
    }
    #endregion
    void RunAnimation(string name)
    {
        foreach (var item in _nameAmin)
        {
            _amin.SetBool(item, false);
        }
        _amin.SetBool(name, true);
    }
    void RunAnimation(EnumAnimation name)
    {
        foreach (var item in _nameAmin)
        {
            _amin.SetBool(item.ToString(), false);
        }
        _amin.SetBool(name.ToString(), true);
    }
    void RandomDir()
    {
        Vector3 temp = transform.position;
        temp.x += Random.Range(-5, 5);
        temp.z += Random.Range(-5, 5);
    }
    private void OnCollisionStay(Collision obj)
    {
        if (obj.transform.tag == TagManager.api.Player)
        {
            obj.transform.GetComponent<PlayerController>().Hit(_myDetail.CurrentATKDamage, _myDetail.RaceType, _myDetail.AtkType);
            StartCoroutine(obj.transform.GetComponent<PlayerController>().PushBack(transform.position, obj.transform));
        }

        if (obj.transform.tag == TagManager.api.Ground)
        {
            _inGround = true;
        }
    }
    public float _timeCoolDownMaxDefault;
    public float _timeCoolDownMinDefault;
    public void SlowAtk(float timerCoolDown)
    {

        _myDetail.BasicCoolDownMax = _timeCoolDownMaxDefault + timerCoolDown;
        _myDetail.BasicCoolDownMin = _timeCoolDownMinDefault + timerCoolDown;

    }
    public void ResetSlowAtk()
    {
        _myDetail.BasicCoolDownMax = _timeCoolDownMaxDefault;
        _myDetail.BasicCoolDownMin = _timeCoolDownMinDefault;

    }
    private float _tgBasicMS = 0;

    public bool InGround { get => _inGround; set => _inGround = value; }

    public void SlowMs(float timerCoolDown)
    {
        if (_tgBasicMS == 0)
        {
            _tgBasicMS = _myDetail.CurrentSpeedMove;
        }
        _myDetail.CurrentSpeedMove = _tgBasicMS * (timerCoolDown / 100);
    }
    public void ResetMS()
    {
        _myDetail.CurrentSpeedMove = _tgBasicMS;
    }
}
