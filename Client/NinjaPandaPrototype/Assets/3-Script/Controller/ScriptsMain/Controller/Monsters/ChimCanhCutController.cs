﻿using System.Collections;
using UnityEngine;
using UnityEngine.AI;
public class ChimCanhCutController : MonoBehaviour
{
    public GameObject ATK;
    Transform player;
    [SerializeField]
    GameObject hitEff;

    MonsterDetail myDetail; // chứa các thuộc tính nhân vật: tốc độ, sát thương, máu...
    Rigidbody rb;
    //AI
    NavMeshAgent agent; // dùng thuật toán AI Unity

    // Animation
    Animator animator;
    string[] animations = new string[] { "Idle", "Run", "Atk", "Hit", "Die" }; // các trạng thái aniamtino nhân vật 
    bool checkAtk = false; // đang trong trạng thái Attack
    bool checkHit = false; // đang trong trạng thái Hit
    bool checkRun = false;  // đang trong trạng thái Run
    bool checkIdle = false; // đang ở trong trạng thái nghĩ
    bool checkFind = true; // cho phép tìm đường 

    [SerializeField]
    private SkinnedMeshRenderer Mesh;
    [SerializeField]
    private SkinnedMeshRenderer ShadowMesh;
    private void Awake()
    {
        myDetail = GetComponent<MonsterDetail>();
        animator = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();
        rb = GetComponent<Rigidbody>();
        //        Debug.Log("Have Player: " + player != null);
    }
    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        //hitEff = GameObject.FindGameObjectWithTag("FloatingDamage");
        agent.SetDestination(player.position);
        timeLastAtk = Time.time;
        myDetail.RangeAtk = 4;
    }
    private void Update()
    {
        if (player == null || player.gameObject.activeSelf == false) // nếu không tìm thấy player thì về tt Idle và kết thúc không làm gì cả 
        {
            agent.isStopped = true;
            //Debug.Log("Not Player");
            RunAnimation("Idle");
            return;
        }
        if (myDetail.CurrentHP <= 0)
        {
            return;
        }
        // tìm thấy player
        float distance = Vector3.Distance(transform.position, player.position); // tính khoản cách đến nhân vật
        if (distance < myDetail.RangeAtk) // nếu khoản cách hiện tại nhỏ hơn phạm vi tấn công
        {
            checkFind = false; // không cho phép tìm đường, vào trạng thái atk
            Attack();
        }
        else
        {
            checkFind = true;
        }
    }
    private void LateUpdate()
    {
        if (myDetail.CurrentHP <= 0)
        {
            agent.isStopped = true;
            return;
        }
        if (checkFind)
        {
            //agent.enabled = true;
            agent.isStopped = false;

            Run();
        }
        else
        {
            agent.isStopped = true;
            Idle();
        }
    }
    #region atk
    float timeLastAtk = 0; // thời gian lần tất công trước đó
    void Attack()
    {
        if (myDetail.CurrentHP > 0)
        {
            if (checkHit)
            {
                //return;
            }
            if (Time.time - timeLastAtk > myDetail.CurrentASMeeleUnit) // nếu thời gian hiện tại - thời gian atk trước đó > thời gian giản cách tất công, thì thực hiện tất công
            {
                timeLastAtk = Time.time; // lưu thời gian lần atk hiện tại
                transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(player.position - transform.position), 0.1f); // xoay nhân vật vào hướng player
                RunAnimation("Atk"); // chạy animaton atk
            }
            ATK.SetActive(true);                                                                                                           //agent.SetDestination(transform.position);
            //else // thực hiện animation idle khi đã hết animation atk nhưng vẫn còn trong trạng thái atk 
            //{
            //    Debug.Log("Idle");
            //    RunAnimation("Idle");
            //}
        }
    }
    void BeginAtk() // được gọi khi mới vào animation atk 
    {
        checkAtk = true;
        if (myDetail.CurrentHP > 0)
        {
            StartCoroutine(MoveTo()); // thực hiện di chuyển đến player trong khi animation atk đang chạy, chạy cùng nhau
        }
    }
    IEnumerator MoveTo()
    {
        if (myDetail.CurrentHP > 0)
        {
            transform.LookAt(player);
            //rb.AddForce(transform.up * 300);
            float distance = Vector3.Distance(transform.position, player.position); // tính khoản cách đến nhân vật
            if (distance > myDetail.RangeAtk / 2)
                transform.Translate(Vector3.forward * myDetail.SpeedMoveAtk * Time.fixedDeltaTime);
            yield return new WaitForFixedUpdate();
            if (checkAtk) // nếu animation atk vẫn còn đang thực thi thì vẫn tiếp tục di chuyển về hướng player
                StartCoroutine(MoveTo());
        }
    }
    void EndAtk() // được gọ khi gần kết thúc animaton atk
    {

        checkAtk = false;
        if (atkDamage)
        {
            atkDamage = false;
            PlayerController.api.Hit(myDetail.CurrentATKDamage);
        }
        checkFind = true; //  phép tìm đường, khi kết thúc animtion 
    }
    #endregion

    #region Hit
    public void Hit()
    {
        checkFind = false;
        myDetail.CurrentHP -= PlayerDetail.api.DamageAtkInStage;
        ShowHitDamage(PlayerDetail.api.DamageAtkInStage, Color.red);
        if (myDetail.CurrentHP <= 0)
        {
            Die();
        }
        else
            RunAnimation("Hit");
    }

    Vector3 dir;// hướng đi lui khi bị đạn bắn
    public void BeginHit() // được gọi khi bắt đầu animation Hit
    {
        checkHit = true;
        dir = player.position - transform.position;
        dir.y = transform.position.y;
        dir = dir.normalized;
        StartCoroutine(Repelling());
    }
    IEnumerator Repelling()
    {
        //rb.AddForce(-transform.forward * 20);

        transform.Translate(dir * PlayerDetail.api.RepelForceInStage * Time.fixedDeltaTime);
        //rb.velocity = -Vector3.back * 10;

        yield return new WaitForFixedUpdate();
        if (checkHit) // nếu animation atk vẫn còn đang thực thi thì vẫn tiếp tục di chuyển về hướng player
            StartCoroutine(Repelling());
    }
    public void EndHit() // được gọi khi kết thúc animation hit
    {
        checkHit = false;
        checkFind = true;
        //hitEff.SetActive(false);// tắt hiệu ứng hit
    }
    #endregion

    float timeLastRun = 0;
    void Run()
    {
        agent.SetDestination(player.position);
        //Debug.Log(checkHit + "_" + checkFind);
        //if (checkFind)
        RunAnimation("Run");
    }

    void Die()
    {
        //PlayerController.api.myDetail.Exp += (int)myDetail.GoldDrop;
        var goldSpawner = GetComponent<InGameGoldSpawner>();
        if (goldSpawner != null)
            goldSpawner.SpawnGold(Random.Range(3, 8), agent, player.transform);

        if (ShadowMesh == null)
            return;
        ShadowMesh.gameObject.SetActive(true);
        var shadowmat = ShadowMesh.material;
        shadowmat.SetFloat("_Active_dead_glow_time", Time.timeSinceLevelLoad);
        GetComponent<ChimCanhCutController>().enabled = false;
        agent.isStopped = true;
        gameObject.tag = TagManager.api.MonsterDie;
        //StartCoroutine(FindMonster.api.Find()); // tìm quái mới cho player
        checkFind = false; // không cho tìm đường nữa
        //Debug.Log("Die");
        myDetail.HealtBar.SetActive(false); // Tắt thanh máu trước khi chạy animation die
        GetComponent<Collider>().enabled = false; // tắt va chạm vật lý để các đối tượng khác đi qua
        DisableAllComponent();
        RunAnimation("Die");
    }
    private void DisableAllComponent()
    {
        foreach (Collider c in GetComponents<Collider>())
            c.enabled = false;
    }
    public void EndDie() // được gọi khi anmation die kết thúc
    {
        Destroy(gameObject);
        //gameObject.SetActive(false);
    }
    IEnumerator _Jump()
    {
        bool temp = agent.isStopped;
        Debug.Log("Jump " + agent.isStopped);
        //agent.isStopped = false;
        yield return new WaitForSeconds(1.0f);
        //agent.isStopped = temp;
    }
    public void Jum()
    {
        rb.AddForce(transform.up * 400);
    }
    void RunAnimation(string name)
    {
        foreach (var item in animations)
        {
            animator.SetBool(item, false);
        }
        animator.SetBool(name, true);
    }

    void ShowHitDamage(float damage, Color color)
    {

        GameObject eff = Instantiate(hitEff, transform.position, Quaternion.identity);// Hiệu ứng hit
        TextMesh textMesh = eff.GetComponent<TextMesh>();
        textMesh.text = damage.ToString();
        textMesh.color = color;
        eff.SetActive(true); // bật hiệu ứng hit, hiệu ứng tự tắt trong thời gian quy định
    }

    void Idle()
    {
        RunAnimation("Idle");
    }
    public void ActiveGlow(int Active)
    {
        if (Mesh == null)
            return;
        var mat = Mesh.material;
        mat.SetFloat("_Active_Glow_Time", Time.timeSinceLevelLoad);
    }

    //GameObject objCollider;
    private void OnTriggerEnter(Collider obj)
    {
        //string sTag = obj.transform.tag;
        //if (sTag == TagManager.api.Bullet)
        //{
        //    objCollider = obj.gameObject;
        //    //Hit();
        //}
    }

    // HitDamage Player
    public bool atkDamage = false; // phát hiện va chạm đúng player, gây sát thương lên player
    //private void OnTriggerStay(Collider obj)
    //{
    //    string sTag = obj.transform.tag;
    //    //Debug.Log("obj.GetType(): "+ obj.GetType());
    //    if (sTag == TagManager.api.Player)
    //    {
    //        atkDamage = true;
    //    }
    //}
}
