﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GateController : MonoBehaviour
{
    public GameObject GateEffect;
    private void OnEnable()
    {
        InitGateContainer();
    }
    public void InitGateContainer()
    {
        if (BC_Chapter_Info.api != null)
            BC_Chapter_Info.api.Gate = this;
        EnableGateEffect(false);

    }
    public void EnableGateEffect(bool isOn) 
    {
        GateEffect.SetActive(isOn);
    }
}
    