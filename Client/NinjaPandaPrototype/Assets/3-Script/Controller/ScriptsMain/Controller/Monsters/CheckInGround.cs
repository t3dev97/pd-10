﻿using UnityEngine;

public class CheckInGround : MonoBehaviour
{
    MonsterDetail monsterDetail;
    public bool InGround = false;
    private void Awake()
    {
        monsterDetail = GetComponentInParent<MonsterDetail>();
    }
    private void OnTriggerStay(Collider obj)
    {
        if (obj.transform.tag == TagManager.api.Ground)
        {
            //Debug.Log("In ground");
            InGround = true;
        }
    }
    private void OnTriggerExit(Collider obj)
    {
        if (obj.transform.tag == TagManager.api.Ground)
        {
            //Debug.Log("Sky");
            InGround = false;
        }
    }
    private void Update()
    {
        if (monsterDetail.CurrentHP <= 0)
        {
            gameObject.SetActive(false);
        }
    }
}
