﻿using System.Collections;
using UnityEngine;

public class HeartController : MonoBehaviour
{
    private Transform _playerTransform;
    private bool _isMoveToPlayer;
    private float _moveSpeed;
    private float _rotationSpeed = 100;
    private float _rotation;

    public void Init(Vector3 spawnPosition, Vector3 destinationPosition, Transform playerTransform)
    {
        StartCoroutine(Move(spawnPosition, new Vector3(destinationPosition.x, 0.3f, destinationPosition.z), playerTransform));
        _playerTransform = playerTransform;
        _isMoveToPlayer = true;
        _rotation = Random.Range(0, 360);
    }

    IEnumerator Move(Vector3 spawnPosition, Vector3 destinationPosition, Transform playerTransform)
    {
        LTBezierPath ltPath = new LTBezierPath(new Vector3[]
        { new Vector3(spawnPosition.x, spawnPosition.y, spawnPosition.z)
            , new Vector3((destinationPosition.x + spawnPosition.x) / 2, destinationPosition.y + 5, (destinationPosition.z + spawnPosition.z) / 2)
                    , new Vector3((destinationPosition.x + spawnPosition.x) / 3, destinationPosition.y + 3, (destinationPosition.z + spawnPosition.z) / 3)
        , new Vector3(destinationPosition.x, destinationPosition.y, destinationPosition.z)});
        LeanTween.move(gameObject, ltPath, 1f).setOrientToPath(true).setEase(LeanTweenType.linear); // animate 
        yield return null;
    }
    public void MoveToPlayer()
    {
        _isMoveToPlayer = true;
    }
    private void Update()
    {
        if (_isMoveToPlayer)
        {
            if (Vector3.Distance(transform.position, _playerTransform.position) <= 1)
            {
                UIBasicController.api.TmpGold.text = (PlayerDetail.api.GoldInstage += 1).ToString();
                Destroy(gameObject);
            }
            else
            {
                _moveSpeed += Time.deltaTime;
                transform.position = Vector3.MoveTowards(transform.position, _playerTransform.position, _moveSpeed / 3.5f);
            }
        }
        _rotation += (_rotationSpeed * Time.deltaTime);
        transform.localRotation = Quaternion.Euler(0, _rotation, _rotation);
    }
    private void OnTriggerEnter(Collider obj)
    {
        if (obj.transform.tag == TagManager.api.Player)
        {
            obj.transform.GetComponent<PlayerController>().playerSkills.AddSkill((int)EnumSkillAddParameterID.Heal);
        }


    }
}
