﻿using System.Collections;
using UnityEngine;

public class InGameGoldController : MonoBehaviour
{
    private Transform _playerTransform;
    [SerializeField]
    private bool _isMoveToPlayer;
    private float _moveSpeed;
    private float _rotationSpeed = 100;
    private float _rotation;
    bool autoIncreaseGoldPlayer = true;

    public bool IsItem = false;
    public void Init(Vector3 spawnPosition, Vector3 destinationPosition, Transform playerTransform, bool autoIncreaseGoldPlayer = true)
    {
        this.autoIncreaseGoldPlayer = autoIncreaseGoldPlayer;
        StartCoroutine(Move(spawnPosition, new Vector3(destinationPosition.x, 0.3f, destinationPosition.z), playerTransform));
        _playerTransform = playerTransform;
        _isMoveToPlayer = false;
        _rotation = Random.Range(0, 360);
    }

    IEnumerator Move(Vector3 spawnPosition, Vector3 destinationPosition, Transform playerTransform)
    {
        LTBezierPath ltPath = new LTBezierPath(new Vector3[]
        { new Vector3(spawnPosition.x, spawnPosition.y, spawnPosition.z)
            , new Vector3((destinationPosition.x + spawnPosition.x) / 2, destinationPosition.y + 5, (destinationPosition.z + spawnPosition.z) / 2)
                    , new Vector3((destinationPosition.x + spawnPosition.x) / 3, destinationPosition.y + 3, (destinationPosition.z + spawnPosition.z) / 3)
        , new Vector3(destinationPosition.x, destinationPosition.y, destinationPosition.z)});
        LeanTween.move(gameObject, ltPath, 1f).setOrientToPath(true).setEase(LeanTweenType.linear); // animate 
        yield return null;
    }
    public void MoveToPlayer()
    {
        _isMoveToPlayer = true;
    }
    private void Update()
    {
        if (_isMoveToPlayer)
        {
            if (Vector3.Distance(transform.position, _playerTransform.position) <= 1)
            {
                if (autoIncreaseGoldPlayer)
                    UIBasicController.api.TmpGold.text = (PlayerDetail.api.GoldInstage += 1).ToString();
                Destroy(gameObject);
            }
            else
            {

                _moveSpeed += (Time.deltaTime * 2.5f);
                transform.position = Vector3.MoveTowards(transform.position, _playerTransform.position, _moveSpeed);
            }
        }
        if (IsItem == false)
        {
            _rotation += (_rotationSpeed * Time.deltaTime);
            transform.localRotation = Quaternion.Euler(0, _rotation, _rotation);
        }
        else
        {
            transform.localRotation = Quaternion.Euler(45, 0, 0);
        }
    }
}
