﻿using Studio;
using Studio.BC;
using TMPro;
using UnityEngine;
using UnityEngine.AI;

public class InGameGoldSpawner : MonoBehaviour
{
    private NavMeshAgent _agent;
    public GameObject GoldPrefab;
    public GameObject ItemDrop;
    public GameObject HeartDrop;
    public Transform ContentEff;
    private void Awake()
    {
        GoldPrefab = Resources.Load("Effects/gold_01", typeof(GameObject)) as GameObject;
        ItemDrop = Resources.Load("Effects/ItemDrop", typeof(GameObject)) as GameObject;
        HeartDrop = Resources.Load("Effects/HeartDrop", typeof(GameObject)) as GameObject;
    }
    private void Start()
    {
        if (ContentEff == null)
            ContentEff = GameObject.FindObjectOfType<BCCache>().transform;
    }
    public void SpawnGold(int GoldCounter, NavMeshAgent agent, Transform playerTransform)
    {
        for (int i = 0; i < GoldCounter; i++)
        {
            if (ContentEff == null)
                ContentEff = GameObject.FindObjectOfType<BCCache>().transform;

            var go = Instantiate(GoldPrefab, transform.position, Quaternion.identity, ContentEff);
            go.transform.localRotation.SetEulerAngles(new Vector3(0, Random.Range(1, 360), 0));

            var gold = go.GetComponent<InGameGoldController>();
            if (gold != null)
            {
                Vector3 pos = getPosition(transform.position, Random.Range(1, 2), 1);
                gold.Init(transform.position, pos, playerTransform);
            }
        }
    }
    public void SpawnGoldReward(int GoldCounter, Transform PosCreate, Transform playerTransform = null)
    {
        for (int i = 0; i < GoldCounter; i++)
        {
            if (ContentEff == null)
                ContentEff = GameObject.FindObjectOfType<BCCache>().transform;

            var go = Instantiate(GoldPrefab, PosCreate.position, Quaternion.identity, ContentEff);
            go.transform.localRotation.SetEulerAngles(new Vector3(0, Random.Range(1, 360), 0));

            var goldReward = go.GetComponent<InGameGoldController>();
            if (goldReward != null)
            {
                Vector3 randDirection = Random.insideUnitSphere * Random.Range(3, 6);
                Vector3 pos = PosCreate.position + randDirection;
                goldReward.Init(PosCreate.position, pos, playerTransform, false);
            }
        }
    }
    public void SpawnItem(BCEquipment obj, int ItemCounter, NavMeshAgent agent, Transform playerTransform)
    {
        for (int i = 0; i < ItemCounter; i++)
        {
            if (ContentEff == null)
                ContentEff = GameObject.FindObjectOfType<BCCache>().transform;

            var go = Instantiate(ItemDrop, transform.position, Quaternion.Euler(new Vector3(0, 0, 0)), ContentEff);
            go.GetComponent<BCDynamicImage>().Type = EnumDynamicImageType.Inventory;
            go.GetComponent<BCDynamicImage>().SetSpriteRenderer(obj.assetName);
            go.GetComponentInChildren<TextMeshPro>().text = LanguageManager.api.GetKey(obj.nameEquipment);
            //go.transform.localRotation.SetEulerAngles(new Vector3(0, Random.Range(1, 360), 0));

            var item = go.GetComponent<InGameGoldController>();
            item.GetComponent<InGameGoldController>().IsItem = true;
            if (item != null)
            {
                //var pos = new Vector3(transform.position.x+ Random.Range (-5,5),transform.position.y,transform.position.z + Random.Range(-5,5));
                Vector3 pos = getPosition(transform.position, Random.Range(1, 10), 1);
                item.GetComponent<InGameGoldController>().IsItem = true;
                item.Init(transform.position, pos, playerTransform, true);
            }
        }
    }
    public void SpawnHeart(string nameAssset, int ItemCounter, NavMeshAgent agent, Transform playerTransform)
    {
        for (int i = 0; i < ItemCounter; i++)
        {
            if (ContentEff == null)
                ContentEff = GameObject.FindObjectOfType<BCCache>().transform;

            var go = Instantiate(HeartDrop, transform.position, Quaternion.identity, ContentEff);
            go.transform.localRotation.SetEulerAngles(new Vector3(0, Random.Range(1, 360), 0));

            var healt = go.GetComponent<HeartController>();
            if (healt != null)
            {
                Vector3 pos = getPosition(transform.position, Random.Range(1, 10), 1);
                healt.Init(transform.position, pos, playerTransform);
            }
        }
    }

    private Vector3 getPosition(Vector3 origin, float dist, int layermask)
    {
        Vector3 randDirection = Random.insideUnitSphere * dist;

        randDirection += origin;

        NavMeshHit navHit;

        if (NavMesh.SamplePosition(randDirection, out navHit, dist, layermask))
            return navHit.position;
        return transform.position;

    }
}
