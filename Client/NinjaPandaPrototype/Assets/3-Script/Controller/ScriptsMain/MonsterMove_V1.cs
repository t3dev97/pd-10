﻿using UnityEngine;
using UnityEngine.AI;
public class MonsterMove_V1 : MonoBehaviour
{
    [Tooltip("Tổng thời gian di chuyển")]
    public float TimeMove; // tổng thời gian di chuyển, bao gồm chuẩn bị, di chuyển và kết thúc
    public float TimePreMove; // thời gian chuẩn bị di chuyển
    public float TimeMoving; // thời gian đang di chuyển
    public float TimeEndMove; // thời gian chuẩn bị kết thúc

    public EnumMoveBasic MoveType;
    public float JumpUp = 50;

    float _TimeLine; // thời điểm thực hiện di chuyển
    NavMeshAgent _agent;
    Transform _player;
    MonsterController _myControl;
    MonsterDetail _myDetail;
    Animator _anim;

    // Animation
    string[] _nameAnim = new string[] { "Idle", "Run", "Atk", "Hit", "Die" }; // các trạng thái aniamtino nhân vật 
    bool _checkAtk = false; // đang trong trạng thái Attack
    bool _checkHit = false; // đang trong trạng thái Hit
    bool _checkRun = false;  // đang trong trạng thái Run
    bool _checkIdle = false; // đang ở trong trạng thái nghĩ
    bool _checkFind = true; // cho phép tìm đường 
    private Vector3 _posTarget;
    private Rigidbody _rb;

    private void Awake()
    {
        _anim = GetComponent<Animator>();
        _agent = GetComponent<NavMeshAgent>();
        _myControl = GetComponent<MonsterController>();
        _myDetail = GetComponent<MonsterDetail>();
        _rb = GetComponent<Rigidbody>();
    }
    private void Start()
    {
        _player = GameObject.FindGameObjectWithTag(TagManager.api.Player).transform;
    }

    private void Update()
    {
        if (_myControl.myAction == false) return;

        if (_myControl.CheckMoveBasic == false)
        {
            _rb.velocity = Vector3.zero;
            //_agent.isStopped = true;
            return;
        }
        RunAnimation("Run");
        switch (MoveType)
        {
            case EnumMoveBasic.Di:
                {
                    Di();
                    break;
                }
            case EnumMoveBasic.Bay:
                {
                    _agent.enabled = false;
                    Bay();
                    break;
                }
            case EnumMoveBasic.Nhay:
                {
                    Nhay();
                    break;
                }
            case EnumMoveBasic.KhongDi:
                {
                    if (_myControl.FollowPlayer)
                        transform.LookAt(_player);
                    RunAnimation("Idle");
                    break;
                }
            default:
                {
                    break;
                }
        }
    }

    void Nhay()
    {
        if (Vector3.Distance(transform.position, _player.position) > 1.5f)
            _rb.velocity = (transform.forward) * _myDetail.CurrentSpeedMove;
    }
    void Di()
    {
        if (_myControl.FollowPlayer) // chọn hướng theo nhân vật
        {
            _agent.enabled = true;
            _agent.isStopped = false;
            _agent.SetDestination(_player.position);
        }
        else // chọn hướng ngẫu nhiên
        {
            _rb.velocity = transform.forward * _myDetail.CurrentSpeedMove;
        }
        RunAnimation("Run");

    }
    void RunAnimation(string name)
    {
        foreach (var item in _nameAnim)
        {
            _anim.SetBool(item, false);
        }
        _anim.SetBool(name, true);
    }


    void Move()
    {
        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(_myControl.DirMove - transform.position), Time.deltaTime);
        _rb.velocity = transform.forward * _myDetail.CurrentSpeedMove;
        RunAnimation("Run");
    }
    void Jump()
    {
        if (_myControl.MyCheckInGround.InGround == false) // di chuyển
        {
            _rb.useGravity = true;
            //transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(_myControl.DirMove - transform.position), Time.deltaTime);
            //_rb.velocity = (transform.forward) * _myDetail.SpeedMove;
        }
        else
        {
            _rb.AddForce(transform.up * JumpUp);
        }
    }
    void MoveTo() // được gọi ở animation run của monster
    {
        for (int i = 0; i < 10; i++)
        {
            //transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(_myControl.DirMove - transform.position), Time.deltaTime);
            //_rb.velocity = (transform.forward) * _myDetail.SpeedMove;
        }
    }
    void EchMove()
    {
        _rb.velocity = transform.forward * _myDetail.CurrentSpeedMove;
    }
    void Bay()
    {

        Vector3 temp = transform.position;
        temp.y = _player.position.y;

        if (Vector3.Distance(temp, _player.position) > 1.5f)
            _rb.velocity = (transform.forward) * _myDetail.CurrentSpeedMove;
        //_agent.isStopped = false;
        //RunAnimation("Run");
        //_agent.SetDestination(_player.position);
    }




}
