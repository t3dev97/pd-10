﻿using System.Collections;
using UnityEngine;
public class ATK : MonoBehaviour
{
    public ChimCanhCutController myChimCanhCut;
    public NamLunController myNamLun;
    private void OnTriggerStay(Collider other)
    {
        if (other.transform.tag == TagManager.api.Player)
        {
            if (myChimCanhCut != null)
                myChimCanhCut.atkDamage = true;
            if(myNamLun != null)
                myNamLun.atkDamage = true;
            StartCoroutine(Hide());
        }
    }
    IEnumerator Hide()
    {
        yield return new WaitForSeconds(0.2f);
        gameObject.SetActive(false);
    }
}
