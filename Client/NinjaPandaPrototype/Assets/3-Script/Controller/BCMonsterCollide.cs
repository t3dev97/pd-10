﻿using UnityEngine;
public class BCMonsterCollide : MonoBehaviour
{
    private BCMoveToCharacter startMoveTo;
    private Vector2 posGrid = new Vector2();
    private BCMonsterDetail detail;

    private Animator anim;
    private void Awake()
    {
        startMoveTo = GetComponent<BCMoveToCharacter>();
        detail = GetComponent<BCMonsterDetail>();
        anim = GetComponent<Animator>();
    }
    private void OnCollisionEnter(Collision obj)
    {
        string sTag = obj.transform.tag;
        switch (sTag)
        {
            case "Player":
                {
                    startMoveTo.IsWalk = false;
                    posGrid.x = Grid.api.NodeFromWorldPoint(transform.position).PosX;
                    posGrid.y = Grid.api.NodeFromWorldPoint(transform.position).PosY;
                    Grid.api.NodeFromWorldPoint(transform.position).IsWall = true;

                    break;
                }

            case "Bullet":
                {
                    //Debug.Log(anim.playbackTime);
                    break;
                }
        }
    }
    private void OnCollisionExit(Collision obj)
    {
        if (obj.transform.tag == "Player")
        {
            startMoveTo.IsWalk = true;
            Grid.api.NodeArray[(int)posGrid.x, (int)posGrid.y].IsWall = false;
        }
    }
    float timeLast = 0;
    float timeAttack = 1;

    private void OnTriggerStay(Collider obj)
    {
        string sTag = obj.transform.tag;
        switch (sTag)
        {
            case "Player":
                {
                    if (Time.time - timeLast > timeAttack)
                    {
                        timeLast = Time.time;
                        Debug.Log("Attack");
                        //anim.speed = .3f; // 0.3 1
                        anim.SetInteger(BCKeyAnimator.Condition, (int)AnimationType.atk);
                        //Debug.Log("Speed: " + anim.speed);//     2
                        if (obj.transform.GetComponent<BCCharacterDetail>() != null)
                            obj.transform.GetComponent<BCCharacterDetail>().ChangeHealt(detail.Damage);
                    }
                    break;
                }
        }

    }

}
