﻿using Studio;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BCSkillPassiveData : BCSkillBaseData
{
    public float Effect;
    public BCSkillPassiveData(Dictionary<string, object> data) : base(data)
    {
        Parse(data);
 
    }
    public void Parse(Dictionary<string, object> data)
    {
        Effect= float.Parse(data.Get<string>(BCKey.Effect));
    }
}
