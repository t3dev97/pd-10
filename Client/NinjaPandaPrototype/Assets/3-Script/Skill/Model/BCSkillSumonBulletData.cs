﻿using Studio;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BCSkillSumonBulletData : BCSkillBaseData
{

    public float time;
    public float radius;
    public int DameEffect;
    public float Effect;
    public BCSkillSumonBulletData(Dictionary<string, object> data) : base (data)
    {
        Parse(data);
 
    }
    public void Parse(Dictionary<string, object> data)
    {
        time = float.Parse(data.Get<string>(BCKey.Time));
        radius = float.Parse(data.Get<string>(BCKey.Radius));
        DameEffect = int.Parse(data.Get<string>(BCKey.DameEffect));
        Effect = float.Parse(data.Get<string>(BCKey.Effect));
    }
}
