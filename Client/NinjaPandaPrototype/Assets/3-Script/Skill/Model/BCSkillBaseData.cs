﻿using Studio;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BCSkillBaseData 
{
    public int id;
    public int limitSkill;
    public int Level=1;
    public float DameAttack;
    public EnumSkillType TypeSkill;

    public string iconSkill;
    public string nameSkill;
    public string Description;
    public BCSkillBaseData(Dictionary<string,object> data)
    {
        DameAttack = int.Parse(data.Get<string>(BCKey.DameAttack));
        id = int.Parse(data.Get<string>(BCKey.ID_Skill));
        limitSkill = int.Parse(data.Get<string>(BCKey.Limit));
        TypeSkill = (EnumSkillType)int.Parse(data.Get<string>(BCKey.Type));
        iconSkill = data.Get<string>(BCKey.Icon_Skill);
        nameSkill = data.Get<string>(BCKey.Name_Skill);
        Description = data.Get<string>(BCKey.Description);
    }

    public static BCSkillBaseData GetStyleSkill(Dictionary<string,object> data)
    {
        BCSkillBaseData baseData = null;
        EnumSkillType typeSkill = (EnumSkillType)int.Parse(data.Get<string>(BCKey.Type));
        switch (typeSkill)
        {
            case EnumSkillType.MutilAttack: baseData = new BCSkillMutilBulletData(data);

                break;
            case EnumSkillType.EffectAttack: baseData = new BCSkillEffectBulletData(data); break;
            case EnumSkillType.SkillSummoner: baseData = new BCSkillSumonBulletData(data); break;
            case EnumSkillType.AddParameter: baseData = new BCSkillParameterData(data); break;
            case EnumSkillType.Passive: baseData = new BCSkillPassiveData(data); break;
            case EnumSkillType.Special: baseData = new BCSkillSpecialData(data); break;
        }
        return baseData;
    }

}
