﻿using Studio;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BCSkillEffectBulletData : BCSkillBaseData
{

    public float effect;
    public int time;
    public float radius;
    public BCSkillEffectBulletData(Dictionary<string, object> data):base(data)
    {
        Parse(data);
 
    }
    public void Parse(Dictionary<string, object> data)
    {
        time = int.Parse(data.Get<string>(BCKey.Time));
        effect = float.Parse(data.Get<string>(BCKey.Effect));
        radius = float.Parse(data.Get<string>(BCKey.Radius));
    }
}
