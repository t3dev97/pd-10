﻿using Studio;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BCSkillMutilBulletData : BCSkillBaseData
{

    public int Effect;


    public BCSkillMutilBulletData(Dictionary<string, object> data) :base(data)
    {
        Parse(data);
 
    }
    public void Parse(Dictionary<string, object> data)
    {

        Effect = int.Parse(data.Get<string>(BCKey.Effect));
        Level = 1;
    }
}
