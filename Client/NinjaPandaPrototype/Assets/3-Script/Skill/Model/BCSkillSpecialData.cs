﻿using Studio;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BCSkillSpecialData : BCSkillBaseData
{

    public float radius;
    public float effect;
    public float time;
    public BCSkillSpecialData(Dictionary<string, object> data) :base(data)
    {
        Parse(data);
 
    }
    public void Parse(Dictionary<string, object> data)
    {
        radius = float.Parse(data.Get<string>(BCKey.Radius));
        effect = float.Parse(data.Get<string>(BCKey.Effect));
        time = float.Parse(data.Get<string>(BCKey.Time));
    }
}
