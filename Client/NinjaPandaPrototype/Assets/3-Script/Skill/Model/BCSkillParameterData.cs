﻿using Studio;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BCSkillParameterData : BCSkillBaseData
{
    public float DameEffect;
    public BCSkillParameterData(Dictionary<string, object> data) :base(data)
    {
        Parse(data);
 
    }
    public void Parse(Dictionary<string, object> data)
    {
        DameEffect = float.Parse( data.Get<string>(BCKey.DameEffect));
    }
}
