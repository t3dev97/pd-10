﻿using Studio;
using Studio.BC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BCSkillSummon : MonoBehaviour, IFSkillBase
{
    public Dictionary<int, BCSkillSumonBulletData> dataSkillSummon;
    public Transform trSkillPlayer;
    private bool isAddSkill = false;
    public void Start()
    {
        dataSkillSummon = new Dictionary<int, BCSkillSumonBulletData>();
        trSkillPlayer = GameObject.Find("SkillSpecial").transform;
    }
    public void AddSkill(int id, BCSkillBaseData data)
    {
        isAddSkill = true;
        if (!dataSkillSummon.ContainsKey(id)) 
            dataSkillSummon.Add(id, (BCSkillSumonBulletData)data);
        InitSkill(id);
    }

    public void InitSkill(int id)
    {
        StopAllSkill();

        if (dataSkillSummon.ContainsKey((int)EnumSkillSummon.BlastCharm)
            && dataSkillSummon[(int)EnumSkillSummon.BlastCharm].Level > 1)
        {
            SkillBlastCharm(dataSkillSummon[(int)EnumSkillSummon.BlastCharm]);
        }

        if (dataSkillSummon.ContainsKey((int)EnumSkillSummon.LightingCharm)
           && dataSkillSummon[(int)EnumSkillSummon.LightingCharm].Level > 1)
        {
            SkillLightCharm(dataSkillSummon[(int)EnumSkillSummon.LightingCharm]);
        }
        if (dataSkillSummon.ContainsKey((int)EnumSkillSummon.ToxicCharm)
           && dataSkillSummon[(int)EnumSkillSummon.ToxicCharm].Level > 1)
        {
            SkillToxicCharm(dataSkillSummon[(int)EnumSkillSummon.ToxicCharm]);
        }
        if (dataSkillSummon.ContainsKey((int)EnumSkillSummon.IceCharm)
           && dataSkillSummon[(int)EnumSkillSummon.IceCharm].Level > 1)
        {
            SkillIceCharm(dataSkillSummon[(int)EnumSkillSummon.IceCharm]);
        }
        if (dataSkillSummon.ContainsKey((int)EnumSkillSummon.FireBall)
          && dataSkillSummon[(int)EnumSkillSummon.FireBall].Level > 1)
        {
            SkillFireBall(dataSkillSummon[(int)EnumSkillSummon.FireBall]);
        }
        if (dataSkillSummon.ContainsKey((int)EnumSkillSummon.SnowBall)
          && dataSkillSummon[(int)EnumSkillSummon.SnowBall].Level > 1)
        {
            Debug.Log("aaaaaa");
            SkillSnowBall(dataSkillSummon[(int)EnumSkillSummon.SnowBall]);
        }
        
        switch (id)
        {
            case (int)EnumSkillSummon.PoisonGas: SkillPoisonGas(dataSkillSummon[(int)EnumSkillSummon.PoisonGas]); break;
            case (int)EnumSkillSummon.ThunderStorm: SkillThunderStorm(dataSkillSummon[(int)EnumSkillSummon.ThunderStorm]); break;
            //case (int)EnumSkillSummon.SnowBall: SkillThunderStorm(dataSkillSummon[(int)EnumSkillSummon.SnowBall]); break;
            //case (int)EnumSkillSummon.FireBall: SkillThunderStorm(dataSkillSummon[(int)EnumSkillSummon.FireBall]); break;
            //case (int)EnumSkillSummon.BlastCharm: SkillThunderStorm(dataSkillSummon[(int)EnumSkillSummon.BlastCharm]); break;
            //case (int)EnumSkillSummon.LightingCharm: SkillThunderStorm(dataSkillSummon[(int)EnumSkillSummon.LightingCharm]); break;
            //case (int)EnumSkillSummon.ToxicCharm: SkillThunderStorm(dataSkillSummon[(int)EnumSkillSummon.ToxicCharm]); break;
            //case (int)EnumSkillSummon.IceCharm: SkillThunderStorm(dataSkillSummon[(int)EnumSkillSummon.IceCharm]); break;
        }
    }

    public void InitSkill()
    {
        StopAllSkill();

        if (dataSkillSummon.ContainsKey((int)EnumSkillSummon.BlastCharm)
            && dataSkillSummon[(int)EnumSkillSummon.BlastCharm].Level > 1)
        {
            SkillBlastCharm(dataSkillSummon[(int)EnumSkillSummon.BlastCharm]);
        }

        if (dataSkillSummon.ContainsKey((int)EnumSkillSummon.LightingCharm)
           && dataSkillSummon[(int)EnumSkillSummon.LightingCharm].Level > 1)
        {
            SkillLightCharm(dataSkillSummon[(int)EnumSkillSummon.LightingCharm]);
        }
        if (dataSkillSummon.ContainsKey((int)EnumSkillSummon.ToxicCharm)
           && dataSkillSummon[(int)EnumSkillSummon.ToxicCharm].Level > 1)
        {
            SkillToxicCharm(dataSkillSummon[(int)EnumSkillSummon.ToxicCharm]);
        }
        if (dataSkillSummon.ContainsKey((int)EnumSkillSummon.IceCharm)
           && dataSkillSummon[(int)EnumSkillSummon.IceCharm].Level > 1)
        {
            SkillIceCharm(dataSkillSummon[(int)EnumSkillSummon.IceCharm]);
        }

        if (dataSkillSummon.ContainsKey((int)EnumSkillSummon.PoisonGas)
          && dataSkillSummon[(int)EnumSkillSummon.PoisonGas].Level > 1)
        {
            SkillPoisonGas(dataSkillSummon[(int)EnumSkillSummon.PoisonGas]);
        }
        if (dataSkillSummon.ContainsKey((int)EnumSkillSummon.ThunderStorm)
         && dataSkillSummon[(int)EnumSkillSummon.ThunderStorm].Level > 1)
        {
            SkillThunderStorm(dataSkillSummon[(int)EnumSkillSummon.ThunderStorm]);
        }
        if (dataSkillSummon.ContainsKey((int)EnumSkillSummon.FireBall)
         && dataSkillSummon[(int)EnumSkillSummon.FireBall].Level > 1)
        {
            SkillFireBall(dataSkillSummon[(int)EnumSkillSummon.FireBall]);
        }
        if (dataSkillSummon.ContainsKey((int)EnumSkillSummon.SnowBall)
         && dataSkillSummon[(int)EnumSkillSummon.SnowBall].Level > 1)
        {
            SkillSnowBall(dataSkillSummon[(int)EnumSkillSummon.SnowBall]);
        }
    }

    public void SkillPoisonGas(BCSkillSumonBulletData data)
    {
        BCCache.Api.GetEffectPrefabAsync(EffectKey.PoisonGas, (go) =>
        {
            GameObject game = Instantiate(go, trSkillPlayer);
            BCSkillSummonArea skill = game.GetComponent<BCSkillSummonArea>();
            int dame = (int)(PlayerDetail.api.BasicATKDamage * ((dataSkillSummon[(int)EnumSkillSummon.PoisonGas].DameEffect * 1.0f) / 100));
            skill.SetData(dame, dataSkillSummon[(int)EnumSkillSummon.PoisonGas].time, dataSkillSummon[(int)EnumSkillSummon.PoisonGas].radius);

            go.transform.SetParent(trSkillPlayer);

        });
    }
    public void SkillThunderStorm(BCSkillSumonBulletData data)
    {
        BCCache.Api.GetEffectPrefabAsync(EffectKey.Thunderstorm, (go) =>
        {
            GameObject game = Instantiate(go, trSkillPlayer);
            BCSkillSummonArea skill = game.GetComponent<BCSkillSummonArea>();
            int dame = (int)(PlayerDetail.api.BasicATKDamage * ((dataSkillSummon[(int)EnumSkillSummon.ThunderStorm].DameEffect * 1.0f) / 100));
            skill.SetData(dame, dataSkillSummon[(int)EnumSkillSummon.ThunderStorm].time, dataSkillSummon[(int)EnumSkillSummon.ThunderStorm].radius);
            go.transform.SetParent(trSkillPlayer);

        });
    }
    public void SkillFireBall(BCSkillSumonBulletData data)
    {
        InvokeRepeating("SummonFireBall", 2, data.time);
    }

    public  void SummonFireBall()
    {
        List<MonsterArray> listMonsterInRound = BC_Chapter_Info.api.listStageMonter;
        BCSkillSumonBulletData data = dataSkillSummon[(int)EnumSkillSummon.FireBall];
        if (listMonsterInRound.Count > 0)
        {
            if (listMonsterInRound[0].listMonster.Count > 0)
            {
                int indexMonster = Random.RandomRange(0, listMonsterInRound[0].listMonster.Count);
                //DebugX.Log("indexMonster => " + indexMonster);
                GameObject monster = listMonsterInRound[0].listMonster[indexMonster];
                BCCache.Api.GetEffectPrefabAsync(EffectKey.FireBall, (go) =>
                {
                    //FireBallArea
                    GameObject ball = Instantiate(go);
                    Vector3 vec= monster.transform.position;
                    vec.y += BCHardCode.HeightBall;
                    ball.transform.position = vec;
                    BCSkillSummonBall skill = ball.GetComponent<BCSkillSummonBall>();
                    skill.setData(data);
                });
            }
        }
    }
    public void SummonSnowBall()
    {
 
        List<MonsterArray> listMonsterInRound = BC_Chapter_Info.api.listStageMonter;
        BCSkillSumonBulletData data = dataSkillSummon[(int)EnumSkillSummon.SnowBall];
        if (listMonsterInRound.Count > 0)
        {
            if (listMonsterInRound[0].listMonster.Count > 0)
            {
                int indexMonster = Random.RandomRange(0, listMonsterInRound[0].listMonster.Count);
                DebugX.Log("indexMonster => " + indexMonster);
                GameObject monster = listMonsterInRound[0].listMonster[indexMonster];
                BCCache.Api.GetEffectPrefabAsync(EffectKey.SnowBall, (go) =>
                {
                    //FireBallArea
                    GameObject ball = Instantiate(go);
                    Vector3 vec = monster.transform.position;
                    vec.y += BCHardCode.HeightBall;
                    ball.transform.position = vec;
                    BCSkillSummonBall skill = ball.GetComponent<BCSkillSummonBall>();
                    skill.setData(data);

                });
            }
        }
    }

    public void SkillSnowBall(BCSkillSumonBulletData data)
    {
        InvokeRepeating("SummonSnowBall", 2, data.time);
    }

    public void SkillBlastCharm(BCSkillSumonBulletData data)
    {
        InvokeRepeating("SummonBlastCharm",2, data.time);
    }

    public void SkillIceCharm(BCSkillSumonBulletData data)
    {
        InvokeRepeating("SummonIceCharm", 2, data.time);
    }

    public void SkillToxicCharm(BCSkillSumonBulletData data)
    {
        InvokeRepeating("SummonToxicCharm", 2, data.time);
    }

    public void SkillLightCharm(BCSkillSumonBulletData data)
    {
        InvokeRepeating("SummonLightCharm", 2, data.time);
    }


    public void StopAllSkill()
    {
        CancelInvoke();
    }

    void SummonBlastCharm()
    {
      
        BCCache.Api.GetEffectPrefabAsync(EffectKey.BlastCharm, (go) =>
        {
            GameObject game = Instantiate(go);
            game.GetComponent<BCBulletSummon>().SetUpData((int)(PlayerDetail.api.BasicATKDamage * ((dataSkillSummon[(int)EnumSkillSummon.BlastCharm].DameEffect * 1.0f) / 100)),  EnumSkillEffect.EffectBlast);

        });
    }
    void SummonIceCharm()
    {

        BCCache.Api.GetEffectPrefabAsync(EffectKey.IceCharm, (go) =>
        {
            GameObject game = Instantiate(go);
            game.GetComponent<BCBulletSummon>().SetUpData((int)(PlayerDetail.api.BasicATKDamage * ((dataSkillSummon[(int)EnumSkillSummon.IceCharm].DameEffect * 1.0f) / 100)), EnumSkillEffect.EffectIce);

        });
    }
    void SummonToxicCharm()
    {

        BCCache.Api.GetEffectPrefabAsync(EffectKey.ToxicCharm, (go) =>
        {
            GameObject game = Instantiate(go);
            game.GetComponent<BCBulletSummon>().SetUpData((int)(PlayerDetail.api.BasicATKDamage * ((dataSkillSummon[(int)EnumSkillSummon.ToxicCharm].DameEffect * 1.0f) / 100)), EnumSkillEffect.EffectPosion);

        });
    }
    void SummonLightCharm()
    {
        BCCache.Api.GetEffectPrefabAsync(EffectKey.LightingCharm, (go) =>
        {
            GameObject game = Instantiate(go);
            game.GetComponent<BCBulletSummon>().SetUpData((int)(PlayerDetail.api.BasicATKDamage * ((dataSkillSummon[(int)EnumSkillSummon.LightingCharm].DameEffect * 1.0f) / 100)), EnumSkillEffect.EffectLight);

        });
    }



}
