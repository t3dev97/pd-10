﻿using Studio.BC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BCSkillAddParameter : MonoBehaviour, IFSkillBase
{
    public Dictionary<int, BCSkillParameterData> dataSkillsMutil;
    private bool isAddSkill = false;
    public void Start()
    {
        dataSkillsMutil = new Dictionary<int, BCSkillParameterData>();
    }
    public void AddSkill(int id, BCSkillBaseData data)
    {
        isAddSkill = true;
        if (!dataSkillsMutil.ContainsKey(id))
            dataSkillsMutil.Add(id, (BCSkillParameterData)data);

        SetData(data);

    }

    public void SetData(BCSkillBaseData data)
    {
        switch (data.id)
        {
            case (int)EnumSkillAddParameterID.Attack: Attack(); break;
            case (int)EnumSkillAddParameterID.AttackMini: AttackMini(); break;
            case (int)EnumSkillAddParameterID.CriticalRate: CriticalRate(); break;
            case (int)EnumSkillAddParameterID.CriticalRateMini: CriticalRateMini(); break;
            case (int)EnumSkillAddParameterID.CriticalDame: CriticalDame(); break;
            case (int)EnumSkillAddParameterID.CriticalDameMini: CriticalDameMini(); break;
            case (int)EnumSkillAddParameterID.Speed: Speed(); break;
            case (int)EnumSkillAddParameterID.SpeedMini: SpeedMini(); break;
            case (int)EnumSkillAddParameterID.Endurance: Endurance(); break;
            case (int)EnumSkillAddParameterID.Shield: Shield(); break;
            case (int)EnumSkillAddParameterID.StrongHeart: StrongHeart(); break;
            case (int)EnumSkillAddParameterID.Smart: Smart(); break;
            case (int)EnumSkillAddParameterID.Heal: Heal(); break;
            case (int)EnumSkillAddParameterID.HPBoost: HPBoost(); break;
            case (int)EnumSkillAddParameterID.Wound: Wound(); break;
            case (int)EnumSkillAddParameterID.Sharp: Sharp(); break;
            case (int)EnumSkillAddParameterID.ExtraLife: ExtraLife(); break;
        }
    }

    public void ExtraLife()
    {

        PlayerDetail.api.IncreasesExtraLife++;
    }

    public void Attack()
    {
        PlayerDetail.api.DamageAtkInStage += (((dataSkillsMutil[(int)EnumSkillAddParameterID.Attack].DameEffect * 1.0f) / 100) * PlayerDetail.api.TGDamageAtkInStage);
        BCCache.Api.GetEffectPrefabAsync(EffectKey.FXAtkUp, (effec) =>
        {
            Transform obj = Instantiate(effec, PlayerDetail.api.transform).transform;
            obj.transform.localPosition = Vector3.zero;

            if (obj.GetComponent<ClearObj>() != null)
            {
                obj.GetComponent<ClearObj>().TimeCoolDown = 1.5f;
                obj.GetComponent<ClearObj>().Auto = true;
            }

            obj.transform.GetChild(2).gameObject.SetActive(true);
            obj.transform.GetChild(2).GetComponent<ParticleSystem>().enableEmission = true;
        });
    }
    public void AttackMini()
    {
        PlayerDetail.api.DamageAtkInStage += (((dataSkillsMutil[(int)EnumSkillAddParameterID.AttackMini].DameEffect * 1.0f) / 100) * PlayerDetail.api.TGDamageAtkInStage);
    }
    public void CriticalRate()
    {
        PlayerDetail.api.CrInStage += dataSkillsMutil[(int)EnumSkillAddParameterID.CriticalRate].DameEffect;
    }
    public void CriticalRateMini()
    {
        PlayerDetail.api.CrInStage += dataSkillsMutil[(int)EnumSkillAddParameterID.CriticalRateMini].DameEffect;
    }
    public void CriticalDame()
    {
        PlayerDetail.api.CdInStage += dataSkillsMutil[(int)EnumSkillAddParameterID.CriticalDame].DameEffect;
    }
    public void CriticalDameMini()
    {
        PlayerDetail.api.CdInStage += dataSkillsMutil[(int)EnumSkillAddParameterID.CriticalDameMini].DameEffect;
    }
    public void Speed()
    {
        //PlayerDetail.api.SpeedAtkInStage += (((dataSkillsMutil[(int)EnumSkillAddParameterID.Speed].DameEffect*1.0f)/100) * PlayerDetail.api.TGSpeedAtkInStage);
        //if(PlayerDetail.api.SpeedAtkInStage>1)
        //    PlayerDetail.api.SpeedAtkInStage -= PlayerDetail.api.SpeedAtkInStage*(dataSkillsMutil[(int)EnumSkillAddParameterID.Speed].DameEffect/100);
        //else
        PlayerDetail.api.SpeedAtkInStage -= PlayerDetail.api.SpeedAtkInStage * (dataSkillsMutil[(int)EnumSkillAddParameterID.Speed].DameEffect);
        PlayerDetail.api.SpeedAtkInStage -= dataSkillsMutil[(int)EnumSkillAddParameterID.Speed].DameEffect;
    }
    public void SpeedMini()
    {
        //PlayerDetail.api.SpeedAtkInStage += (((dataSkillsMutil[(int)EnumSkillAddParameterID.SpeedMini].DameEffect * 1.0f) / 100) * PlayerDetail.api.TGSpeedAtkInStage);
        PlayerDetail.api.SpeedAtkInStage -= dataSkillsMutil[(int)EnumSkillAddParameterID.SpeedMini].DameEffect;
    }
    public void Endurance()
    {
        PlayerDetail.api.MrInStage += dataSkillsMutil[(int)EnumSkillAddParameterID.Endurance].DameEffect;
    }
    public void Shield()
    {
        PlayerDetail.api.RrInStage += dataSkillsMutil[(int)EnumSkillAddParameterID.Shield].DameEffect;
    }
    public void StrongHeart()
    {
        PlayerDetail.api.IncreasesHPFromHeal += dataSkillsMutil[(int)EnumSkillAddParameterID.StrongHeart].DameEffect;
    }
    public void Smart()
    {
        PlayerDetail.api.IncreasesEXP += dataSkillsMutil[(int)EnumSkillAddParameterID.Smart].DameEffect;
    }
    public void Heal()
    {
        float hp = 0;
        hp = ((dataSkillsMutil[(int)EnumSkillAddParameterID.Heal].DameEffect * 1.0f) / 100) * PlayerDetail.api.MaxHPInStage;
        hp = hp + hp * (PlayerDetail.api.IncreasesHPFromHeal / 100);
        PlayerDetail.api.HP += hp;

        BCCache.Api.GetEffectPrefabAsync(EffectKey.FXHealth, (go) =>
                {
                    GameObject game = Instantiate(go, PlayerController.api.gameObject.transform);
                    game.transform.localPosition = Vector3.zero;
                    //delayAni(0.5f, () =>
                    //{
                    //    Debug.Log("abba");
                    //    game.transform.GetChild(0).GetComponent<ParticleSystem>().enableEmission = false;
                    //    Destroy(game, 0.5f);
                    //});
                    StartCoroutine(delayAni(game));

                });
        if (PlayerDetail.api.HP > PlayerDetail.api.MaxHPInStage)
        {
            PlayerDetail.api.HP = PlayerDetail.api.MaxHPInStage;
        }

        BCCache.Api.DataConfig.SkillBases[(int)EnumSkillAddParameterID.Heal].Level--;
    }
    public IEnumerator delayAni(GameObject game)
    {
        ParticleSystem particle = game.transform.GetChild(0).GetComponent<ParticleSystem>();
        while (true)
        {
            yield return new WaitForSeconds(0.2f);

            if (particle.particleCount == 0)
            {
                break;
            }
        }
        Destroy(game);
    }
    public void HPBoost()
    {
        PlayerDetail.api.MaxHPInStage += ((dataSkillsMutil[(int)EnumSkillAddParameterID.HPBoost].DameEffect * 1.0f) / 100) * PlayerDetail.api.TGMaxHPInStage;
        PlayerDetail.api.HP += ((dataSkillsMutil[(int)EnumSkillAddParameterID.HPBoost].DameEffect * 1.0f) / 100) * PlayerDetail.api.TGMaxHPInStage;

    }

    public void Wound()
    {
        PlayerDetail.api.IncreasesMeleeDame += dataSkillsMutil[(int)EnumSkillAddParameterID.Wound].DameEffect;
    }
    public void Sharp()
    {
        PlayerDetail.api.IncreasesRangerDame += dataSkillsMutil[(int)EnumSkillAddParameterID.Sharp].DameEffect;
    }
}
