﻿using Studio;
using Studio.BC;
using System;
using UnityEngine;

public class BCPlayerSkillController : MonoBehaviour
{
    public BCSkillMutilBullet skillMutils;
    public BCSkillEffectBullet skillEffects;
    public BCSkillAddParameter skillAddParameter;
    public BCSkillPassive skillPassive;
    public BCSkillSpecial skillSpecial;
    public BCSkillSummon skillSummon;
    public void AddSkill(int id)
    {
        BCSkillBaseData data = BCCache.Api.DataConfig.SkillBases[id];

        int levelSkill = BCCache.Api.DataConfig.SkillBases[data.id].Level++;

        if (data.TypeSkill == EnumSkillType.EffectAttack)
        {
            PlayerDetail.api.IncreasesDameEffect += data.DameAttack;
        }
        else
        {
            PlayerDetail.api.DamageAtkInStage += (((data.DameAttack) / 100) * PlayerDetail.api.DamageAtkInStage);
        }

        BCCache.Api.GetEffectPrefabAsync(EffectKey.FXAtkUp, (effec) =>
        {
            Transform obj = Instantiate(effec, PlayerDetail.api.transform).transform;
            obj.transform.localPosition = Vector3.zero;
            if (obj.GetComponent<ClearObj>() != null)
            {
                obj.GetComponent<ClearObj>().TimeCoolDown = 1.5f;
                obj.GetComponent<ClearObj>().Auto = true;
            }
            obj.transform.GetChild(0).gameObject.SetActive(true);
            obj.transform.GetChild(0).GetComponent<ParticleSystem>().enableEmission = true;
        });

        switch (data.TypeSkill)
        {
            case EnumSkillType.MutilAttack:
                skillMutils.AddSkill(id, (BCSkillMutilBulletData)data);

                break;
            case EnumSkillType.EffectAttack:
                skillEffects.AddSkill(id, (BCSkillEffectBulletData)data);
                break;
            case EnumSkillType.AddParameter:
                skillAddParameter.AddSkill(id, (BCSkillParameterData)data);
                break;
            case EnumSkillType.Passive:
                skillPassive.AddSkill(id, (BCSkillPassiveData)data);
                break;
            case EnumSkillType.Special:
                skillSpecial.AddSkill(id, (BCSkillSpecialData)data);
                break;
            case EnumSkillType.SkillSummoner:
                skillSummon.AddSkill(id, (BCSkillSumonBulletData)data);
                break;
            default:
                DebugX.Log("======== Skill Not Exist =============");
                break;
        }



    }

    public void PlayPassive()
    {
        skillPassive.InitSkill();
    }

    public void PlaySpecial()
    {
        skillSpecial.ResetSkill();
        skillSpecial.InitSkill();
    }

    public void PlayMultiShot()
    {
        skillMutils.InitSkill();
    }

    public void PlayEffectBullet(string tag, BulletController bullet, Transform parent, Action effectAction)
    {
        skillEffects.InitSkill(tag, bullet, parent, effectAction);
    }
    public void ResetSkill()
    {
        foreach (var skill in BCCache.Api.DataConfig.SkillBases)
        {
            skill.Value.Level = 1;
        }
        BCCache.Api.DataConfig.ConfigGameData.ResetListSkill();
    }

}
