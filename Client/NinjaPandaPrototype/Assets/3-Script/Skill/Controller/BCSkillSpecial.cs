﻿using Studio.BC;
using System.Collections.Generic;
using UnityEngine;

public class BCSkillSpecial : MonoBehaviour, IFSkillBase
{
    public Dictionary<int, BCSkillSpecialData> dataSkillSpecial;
    public Transform trSkillPlayer;
    private bool isAddSkill = false;
    public void Awake()
    {
        dataSkillSpecial = new Dictionary<int, BCSkillSpecialData>();

    }
    private void Start()
    {
        trSkillPlayer = GameObject.Find("SkillSpecial").transform;
    }
    public void AddSkill(int id, BCSkillBaseData data)
    {
        isAddSkill = true;
        if (!dataSkillSpecial.ContainsKey(id))
            dataSkillSpecial.Add(id, (BCSkillSpecialData)data);

        AddSkillInit(id);

    }

    public void AddSkillInit(int id)
    {
        switch (id)
        {
            case (int)EnumSkillSpecical.Amulet: SkillAmulet(dataSkillSpecial[(int)EnumSkillSpecical.Amulet]); break;
            case (int)EnumSkillSpecical.ColdArea: SkillColdArena(dataSkillSpecial[(int)EnumSkillSpecical.ColdArea]); break;
            case (int)EnumSkillSpecical.Halo: SkillHalo(dataSkillSpecial[(int)EnumSkillSpecical.Halo]); break;
            case (int)EnumSkillSpecical.CollectAttack: SkillCollectAttack(dataSkillSpecial[(int)EnumSkillSpecical.CollectAttack]); break;
            case (int)EnumSkillSpecical.collectSpped: SkillCollectSpeed(dataSkillSpecial[(int)EnumSkillSpecical.collectSpped]); break;
            case (int)EnumSkillSpecical.Wind: SkillWind(dataSkillSpecial[(int)EnumSkillSpecical.Wind]); break;
        }
    }
    public void InitSkill()
    {
        if (dataSkillSpecial.ContainsKey((int)EnumSkillSpecical.Amulet)
         && dataSkillSpecial[(int)EnumSkillSpecical.Amulet].Level > 1)
        {
            SkillAmulet(dataSkillSpecial[(int)EnumSkillSpecical.Amulet]);
        }

        if (dataSkillSpecial.ContainsKey((int)EnumSkillSpecical.ColdArea)
        && dataSkillSpecial[(int)EnumSkillSpecical.ColdArea].Level > 1)
        {
            SkillColdArena(dataSkillSpecial[(int)EnumSkillSpecical.ColdArea]);
        }
        if (dataSkillSpecial.ContainsKey((int)EnumSkillSpecical.Halo)
        && dataSkillSpecial[(int)EnumSkillSpecical.Halo].Level > 1)
        {
            SkillHalo(dataSkillSpecial[(int)EnumSkillSpecical.Halo]);
        }
        if (dataSkillSpecial.ContainsKey((int)EnumSkillSpecical.CollectAttack)
        && dataSkillSpecial[(int)EnumSkillSpecical.CollectAttack].Level > 1)
        {
            SkillCollectAttack(dataSkillSpecial[(int)EnumSkillSpecical.CollectAttack]);
        }
        if (dataSkillSpecial.ContainsKey((int)EnumSkillSpecical.collectSpped)
        && dataSkillSpecial[(int)EnumSkillSpecical.collectSpped].Level > 1)
        {
            SkillCollectSpeed(dataSkillSpecial[(int)EnumSkillSpecical.collectSpped]);
        }


    }

    public void SkillWind(BCSkillSpecialData data)
    {

    }

    public void SkillAmulet(BCSkillSpecialData data)
    {
        BCCache.Api.GetEffectPrefabAsync(EffectKey.SkillAmulet, (go) =>
        {
            GameObject game = Instantiate(go, trSkillPlayer);
            BulletMoveController shieldLeft = game.transform.GetChild(0).GetComponent<BulletMoveController>();
            BulletMoveController shieldRight = game.transform.GetChild(1).GetComponent<BulletMoveController>();

            //game.transform.GetChild(0).GetComponent<BulletMoveController>().SetUpCircleMove(data.effect, 0, data.radius + 1, false);
            shieldLeft.SetUpCircleMove(data.effect, 0, data.radius, false);

            // shieldLeft._isMoving = true;
            shieldRight.SetUpCircleMove(data.effect, 180, data.radius, false);
        });

    }

    public void SkillHalo(BCSkillSpecialData data)
    {
        BCCache.Api.GetEffectPrefabAsync(EffectKey.Halo, (go) =>
        {
            GameObject game = Instantiate(go, trSkillPlayer);
            BCSkillHalo skill = game.transform.GetChild(0).GetComponent<BCSkillHalo>();
            skill.SetData(data.effect, data.radius);
        });

    }

    public void SkillCollectAttack(BCSkillSpecialData data)
    {
        //BCCache.Api.GetEffectPrefabAsync(EffectKey.Halo, (go) =>
        //{
        //    GameObject game = Instantiate(go, trSkillPlayer);
        //    BCSkillHalo skill = game.transform.GetChild(0).GetComponent<BCSkillHalo>();
        //    skill.SetData(data.effect);
        //});
        PlayerDetail.api.DamageAtkInStage += (((dataSkillSpecial[(int)EnumSkillSpecical.CollectAttack].effect * 1.0f) / 100) * PlayerDetail.api.TGDamageAtkInStage);


    }
    public void SkillCollectSpeed(BCSkillSpecialData data)
    {
        //BCCache.Api.GetEffectPrefabAsync(EffectKey.Halo, (go) =>
        //{
        //    GameObject game = Instantiate(go, trSkillPlayer);
        //    BCSkillHalo skill = game.transform.GetChild(0).GetComponent<BCSkillHalo>();
        //    skill.SetData(data.effect);
        //});
        PlayerDetail.api.SpeedAtkInStage -= PlayerDetail.api.SpeedAtkInStage * (dataSkillSpecial[(int)EnumSkillSpecical.collectSpped].effect);

    }

    public void ResetSkill()
    {
        foreach (Transform tg in trSkillPlayer)
        {
            Destroy(tg.gameObject);
        }
    }
    public void SkillColdArena(BCSkillSpecialData data)
    {
        BCCache.Api.GetEffectPrefabAsync(EffectKey.ColdArea, (go) =>
        {

            GameObject goAreaCold = Instantiate(go, trSkillPlayer);

            BCSkillColdArea skill = goAreaCold.GetComponent<BCSkillColdArea>();
            skill.SetData(data.effect, data.radius, (other) =>
            {

                BCCache.Api.GetEffectPrefabAsync(EffectKey.FXCold, (fxCold) =>
                {
                    MonsterController_new monster = other.GetComponent<MonsterController_new>();
                    GameObject goFXCold = null;
                    if (!monster.isCold)
                    {
                        goFXCold = Instantiate(fxCold, other.transform);
                    }
                    else
                    {

                        goFXCold = monster.gameObject.transform.Find(EffectKey.FXCold).gameObject;
                        goFXCold.SetActive(true);


                    }
                    goFXCold.name = EffectKey.FXCold;
                    BCSkillColdAreaEffect skillColdAreaEffect = goFXCold.GetComponent<BCSkillColdAreaEffect>();
                    skillColdAreaEffect.SetData(data.effect, data.time);
                    monster.isCold = true;

                });
            });
            //shieldLeft = game.transform.GetChild(0).GetComponent<BulletMoveController>();
            //shieldRight = game.transform.GetChild(1).GetComponent<BulletMoveController>();

            ////game.transform.GetChild(0).GetComponent<BulletMoveController>().SetUpCircleMove(data.effect, 0, data.radius + 1, false);
            //shieldLeft.SetUpCircleMove(data.effect, 0, data.radius, false);

            //// shieldLeft._isMoving = true;
            //shieldRight.SetUpCircleMove(data.effect, 180, data.radius, false);
        });

    }
}
