﻿using Studio.BC;
using System;
using System.Collections.Generic;
using UnityEngine;

public class BCSkillEffectBullet : MonoBehaviour, IFSkillBase
{
    public Dictionary<int, BCSkillEffectBulletData> dataSkillsEffect;
    public static BCSkillEffectBullet api;
    public bool isBouncyArrow = false;
    public float bouncingTimes = 0;
    public float ricochetTimes = 0;

    public Action action;

    private Vector3 direction;

    void Start()
    {
        if (api == null)
            api = this;
        else if (api != this)
            Destroy(gameObject);
        dataSkillsEffect = new Dictionary<int, BCSkillEffectBulletData>();
    }

    public void InitSkill(string tag, BulletController bullet, Transform parent, Action action)
    {
        Enum.TryParse(tag, out EnumTag enumTag);
        this.action = action;
        switch (enumTag)
        {
            case EnumTag.Wall:
            case EnumTag.WallBorder: InitSkillTouchWall(bullet); break;
            case EnumTag.Monster: InitSkillTouchMonster(bullet, parent); break;
            default: /*Debug.Log("default");*/ break;
        }

    }


    public void AddSkill(int id, BCSkillBaseData data)
    {
        //isAddSkill = true;

        if (!dataSkillsEffect.ContainsKey(id))
            dataSkillsEffect.Add(id, (BCSkillEffectBulletData)data);

        SetData(data);
    }

    //UpdateBullet
    public void SetData(BCSkillBaseData data)
    {
        switch (data.id)
        {
            case (int)EnumSkillEffectAttackID.BouncyArrow:
                isBouncyArrow = true;
                bouncingTimes = dataSkillsEffect[data.id].effect;
                break;
            case (int)EnumSkillEffectAttackID.Ricochet:
                ricochetTimes = dataSkillsEffect[data.id].effect;
                break;
            case (int)EnumSkillEffectAttackID.Blast: BCObjectPool.Api.UpdateBullet(EnumBulletEffect.Blast); PlayerController.api.isBulletBlast = true; break;
            case (int)EnumSkillEffectAttackID.Ice: BCObjectPool.Api.UpdateBullet(EnumBulletEffect.Ice); PlayerController.api.isBulletIce = true; break;
            case (int)EnumSkillEffectAttackID.Poision: BCObjectPool.Api.UpdateBullet(EnumBulletEffect.Poision); PlayerController.api.isBulletPoison = true; break;
            case (int)EnumSkillEffectAttackID.Bolt:
                BCObjectPool.Api.UpdateBullet(EnumBulletEffect.Bolt); PlayerController.api.isBulletBolt = true; break;

        }
    }

    public void InitSkillTouchWall(BulletController bullet)
    {
        //BCCache.Api.GetEffectPrefabAsync(EffectKey.FX_PhiTieuTouch, (effect) =>
        //{
        //    GameObject obj_ = Instantiate(effect, bullet.transform.position, Quaternion.Euler(bullet.transform.eulerAngles), BCCache.Api.transform);
        //    if (obj_.GetComponent<ClearObj>() != null)
        //    {
        //        obj_.GetComponent<ClearObj>().TimeCoolDown = 1.0f;
        //        obj_.GetComponent<ClearObj>().Auto = true;
        //    }
        //    else
        //    {
        //        obj_.gameObject.AddComponent<ClearObj>();
        //        obj_.GetComponent<ClearObj>().TimeCoolDown = 1.0f;
        //        obj_.GetComponent<ClearObj>().Auto = true;
        //    }

        //    obj_.transform.gameObject.SetActive(true);
        //    obj_.transform.GetChild(0).GetComponent<ParticleSystem>().enableEmission = true;
        //});

        if (dataSkillsEffect.ContainsKey((int)EnumSkillEffectAttackID.BouncyArrow)
            && dataSkillsEffect[(int)EnumSkillEffectAttackID.BouncyArrow].Level > 1)
        {
            BouncyArrowSkill(bullet, () => { bullet.DestroyBullet(); });
        }
        else
        {
            BCCache.Api.GetEffectPrefabAsync(EffectKey.FX_PhiTieuTouch, (effect) =>
            {
                GameObject obj_ = Instantiate(effect, bullet.transform.position, Quaternion.Euler(bullet.transform.eulerAngles), BCCache.Api.transform);
                if (obj_.GetComponent<ClearObj>() != null)
                {
                    obj_.GetComponent<ClearObj>().TimeCoolDown = .3f;
                    obj_.GetComponent<ClearObj>().Auto = true;
                }
                else
                {
                    obj_.gameObject.AddComponent<ClearObj>();
                    obj_.GetComponent<ClearObj>().TimeCoolDown = .3f;
                    obj_.GetComponent<ClearObj>().Auto = true;
                }

                obj_.transform.gameObject.SetActive(true);
                obj_.transform.GetChild(0).GetComponent<ParticleSystem>().enableEmission = true;
            });
            bullet.DestroyBullet();
        }
    }

    public void InitSkillTouchMonster(BulletController bullet, Transform parent)
    {
        if (dataSkillsEffect.ContainsKey((int)EnumSkillEffectAttackID.Poision))
        {
            PoisionSkill(bullet, parent);
        }
        if (dataSkillsEffect.ContainsKey((int)EnumSkillEffectAttackID.Blast))
        {
            BlastSkill(bullet, parent);
        }

        if (dataSkillsEffect.ContainsKey((int)EnumSkillEffectAttackID.Ice))
        {
            IceSkill(bullet, parent);
        }
        if (dataSkillsEffect.ContainsKey((int)EnumSkillEffectAttackID.Headshot))
        {
            if (HeadShotSkill(bullet, parent))
            {
                return;
            }

        }
        if (dataSkillsEffect.ContainsKey((int)EnumSkillEffectAttackID.Bolt))
        {
            BoltSkill(bullet, parent);
        }

        if (dataSkillsEffect.ContainsKey((int)EnumSkillEffectAttackID.Absorb))
        {
            AbsorbSkill(bullet, parent);
        }


        if (dataSkillsEffect.ContainsKey((int)EnumSkillEffectAttackID.Ricochet))
        {
            RicochetSkill(bullet, parent);
        }

        else if ((!dataSkillsEffect.ContainsKey((int)EnumSkillEffectAttackID.Pierced)))
        {
            bullet.DestroyBullet(true);
        }
        action();
    }

    public void AbsorbSkill(BulletController bullet, Transform parent)
    {
        MonsterController_new monsterController = parent.gameObject.GetComponent<MonsterController_new>();
        if (monsterController._myDetail.CurrentHP <= bullet.damage)
        {
            float hp = 0;
            hp = ((dataSkillsEffect[(int)EnumSkillEffectAttackID.Absorb].effect * 1.0f) / 100) * PlayerDetail.api.MaxHPInStage;
            hp = hp + hp * (PlayerDetail.api.IncreasesHPFromHeal / 100);
            PlayerDetail.api.HP += hp;
        }
    }

    public void BoltSkill(BulletController bullet, Transform action)
    {
        AddSkillBolt(action);
    }

    public void AddSkillBolt(Transform trObjectCollider)
    {
        BCSkillEffectBulletData data = (BCSkillEffectBulletData)BCCache.Api.DataConfig.SkillBases[(int)EnumSkillEffectAttackID.Bolt];

        int dameMonster = (int)(PlayerDetail.api.DamageAtkInStage * ((data.DameAttack * 1.0f) / 100));
        if (BC_Chapter_Info.api.listStageMonter.Count > 0)
        {
            List<GameObject> listMonsters = BC_Chapter_Info.api.listStageMonter[0].listMonster;

            List<MonsterController_new> listMonsterController = new List<MonsterController_new>();


            foreach (GameObject list in listMonsters)
            {
                if (!list.Equals(trObjectCollider.gameObject))
                {
                    if (Vector3.Distance(trObjectCollider.position, list.transform.position) <= data.radius)
                    {

                        BCCache.Api.GetEffectPrefabAsync(EffectKey.FXBolt, (go) =>
                        {
                            MonsterController_new monsterController = list.gameObject.GetComponent<MonsterController_new>();
                            var game = Instantiate(go, list.transform, false);

                            BCLightFishFxView view = game.GetComponent<BCLightFishFxView>();

                            //Debug.Log("================>"+ list.transform.FindChild("HealtBar").gameObject);



                            game.transform.position = list.transform.position;

                            if (dameMonster >= monsterController._myDetail.CurrentHP)
                            {
                                listMonsterController.Add(monsterController);
                            }
                            else
                            {
                                monsterController.TakeDamage(dameMonster, false);
                            }

                            Destroy(game, 1);
                        });
                    }
                }
            }

            foreach (MonsterController_new monster in listMonsterController)
            {

                monster.TakeDamage(dameMonster, false);

            }
        }
    }

    public void RicochetSkill(BulletController bullet, Transform parent)
    {
        if (bullet.ricochetTimes > 0)
        {
            Vector3 vec = parent.transform.position;
            vec.y += 0.5f;
            float MinDirection = 0;
            Transform posTarget = null;
            if (BC_Chapter_Info.api.listStageMonter.Count > 0)
            {
                List<GameObject> listMonsters = BC_Chapter_Info.api.listStageMonter[0].listMonster;
                RaycastHit hit = new RaycastHit();
                foreach (GameObject list in listMonsters)
                {


                    if (!list.Equals(parent.gameObject))
                    {
                        if (Vector3.Distance(parent.position, list.transform.position) <= dataSkillsEffect[(int)EnumSkillEffectAttackID.Ricochet].radius)
                        {
                            if (Physics.Raycast(vec, (parent.position - list.transform.position).normalized * -1, out hit,
                                dataSkillsEffect[(int)EnumSkillEffectAttackID.Ricochet].radius,
                                (1 << LayerMask.NameToLayer("Monster"))
                                | (1 << LayerMask.NameToLayer("Wall"))
                                | (1 << LayerMask.NameToLayer("WallBorder"))))
                            {
                                if (hit.transform.tag.Equals("Monster"))
                                {
                                    if (MinDirection == 0)
                                    {

                                        MinDirection = hit.distance;
                                        posTarget = list.transform;
                                    }
                                    else
                                    {
                                        if (MinDirection > hit.distance)
                                        {
                                            MinDirection = hit.distance;
                                            posTarget = list.transform;
                                        }
                                    }

                                }
                                else if (hit.transform.tag.Equals("Wall") || hit.transform.tag.Equals("WallBorder"))
                                {
                                    break;
                                }
                                //Debug.DrawRay(vec, (parent.position - list.transform.position).normalized * -1 * dataSkillsEffect[(int)EnumSkillEffectAttackID.Ricochet].radius, Color.red);
                                //direction = (parent.position - list.transform.position).normalized;
                            }
                        }
                    }
                }
                if (posTarget != null)
                {
                    Debug.DrawRay(vec, (parent.position - posTarget.position).normalized * -1 * dataSkillsEffect[(int)EnumSkillEffectAttackID.Ricochet].radius, Color.red);

                    bullet.RotateBulletCustom(posTarget, vec);
                }
                else
                {
                    if ((!dataSkillsEffect.ContainsKey((int)EnumSkillEffectAttackID.Pierced)))
                    {
                        bullet.DestroyBullet();
                    }

                }
            }
            else
            {
                //Debug.Log("listStageMonter.Count <= 0");
                bullet.DestroyBullet();
            }
        }
        else
        {
            bullet.DestroyBullet();
        }
    }

    public void BouncyArrowSkill(BulletController bullet, Action action)
    {
        if (bullet.bouncingTimes > 0)
        {
            float y = bullet.gameObject.transform.rotation.eulerAngles.y;
            float angle = 0;
            if (bullet.WallHorizontal)
            {
                //Debug.Log("bullet.WallHorizontal " + bullet.WallHorizontal);
                angle = (y + 180) * -1;
            }
            else
            {
                //Debug.Log("bullet.WallHorizontal " + bullet.WallHorizontal);
                angle = y * -1;
            }
            //Debug.Log("bullet.WallHorizontal " + bullet.WallHorizontal);
            y = angle;
            StartCoroutine(bullet.RotateBullet(y));
        }
        else
        {
            action();
        }
    }

    public void PoisionSkill(BulletController bullet, Transform parent)
    {
        AddPosionSkill(parent);
    }

    public void AddPosionSkill(Transform parent)
    {
        BCSkillEffectBulletData data = (BCSkillEffectBulletData)BCCache.Api.DataConfig.SkillBases[(int)EnumSkillEffectAttackID.Poision];
        BCCache.Api.GetEffectPrefabAsync(EffectKey.FXPoision, (go) =>
        {
            MonsterController_new monsterController = parent.gameObject.GetComponent<MonsterController_new>();
            if (!monsterController.isPoision)
            {
                var game = Instantiate(go, parent, false);
                game.transform.position = parent.position;
                BCSkillEffect poision = game.GetComponent<BCSkillEffect>();
                //Debug.Log(dataSkillsEffect[EnumSkillEffectAttackID.Poision].effect);
                //poision.SetData((int)(PlayerDetail.api.BasicATKDamage * ((data.DameAttack * 1.0f) / 100)), data.effect, data.time);

                poision.setPosion(monsterController);
                poision.SetData((int)(PlayerDetail.api.DamageAtkInStage * ((data.DameAttack * 1.0f) / 100)), data.effect, data.time);

                monsterController.isPoision = true;

                //go.transform.GetChild(0).localScale = parent.localScale;
                //Debug.Log(go.transform.GetChild(0).localScale);
            }

        });
    }
    public bool HeadShotSkill(BulletController bullet, Transform parent)
    {
        if (parent.GetComponent<BC_ObjectInfo>().style == STYLE_BRUSHES.monster)
        {
            int rdInt = UnityEngine.Random.Range(1, 100);

            if (rdInt <= dataSkillsEffect[(int)EnumSkillEffectAttackID.Headshot].effect)
            {

                MonsterController_new monsterController = parent.gameObject.GetComponent<MonsterController_new>();

                // monsterController.Die();

                //monsterController.TakeDamage(monsterController._myDetail.BasicHP, false);
                monsterController.OneHitMonster();
                return true;
            }
        }
        return false;
    }

    public void BlastSkill(BulletController bullet, Transform parent)
    {
        AddBlast(parent);
    }

    public void AddBlast(Transform parent)
    {
        BCSkillEffectBulletData data = (BCSkillEffectBulletData)BCCache.Api.DataConfig.SkillBases[(int)EnumSkillEffectAttackID.Blast];

        BCCache.Api.GetEffectPrefabAsync(EffectKey.FXBlast, (go) =>
        {
            MonsterController_new monsterController = parent.gameObject.GetComponent<MonsterController_new>();
            BCSkillEffect poision = null;
            if (!monsterController.isBlast)
            {
                var game = Instantiate(go, parent, false);
                game.name = EffectKey.FXBlast;
                game.transform.position = parent.position;
                poision = game.GetComponent<BCSkillEffect>();
            }
            else
            {
                poision = parent.Find(EffectKey.FXBlast).GetComponent<BCSkillEffect>();
            }
            //go.transform.GetChild(0).localScale = parent.localScale;

            poision.gameObject.SetActive(true);
            //poision.SetData((int)(PlayerDetail.api.BasicATKDamage * ((data.DameAttack * 1.0f) / 100)), data.effect, data.time);
            poision.SetData((int)(PlayerDetail.api.DamageAtkInStage * ((data.DameAttack * 1.0f) / 100)), data.effect, data.time);
            poision.setPosion(monsterController);
            monsterController.isBlast = true;
        });
    }

    public void IceSkill(BulletController bullet, Transform parent)
    {
        AddIce(parent);
    }

    public void AddIce(Transform parent)
    {
        BCSkillEffectBulletData data = (BCSkillEffectBulletData)BCCache.Api.DataConfig.SkillBases[(int)EnumSkillEffectAttackID.Ice];

        BCCache.Api.GetEffectPrefabAsync(EffectKey.FXIce, (go) =>
        {
            MonsterController_new monsterController = parent.gameObject.GetComponent<MonsterController_new>();
            BCSkillEffect poision = null;
            if (!monsterController.isIce)
            {
                var game = Instantiate(go, parent, false);
                game.name = EffectKey.FXIce;
                game.transform.position = parent.position;

                poision = game.GetComponent<BCSkillEffect>();
            }
            else
            {
                poision = parent.Find(EffectKey.FXIce).GetComponent<BCSkillEffect>();
            }
            //go.transform.GetChild(0).localScale = parent.localScale;

            poision.gameObject.SetActive(true);
            poision.SetData((int)(PlayerDetail.api.BasicATKDamage * ((data.DameAttack * 1.0f) / 100)), data.effect, data.time, monsterController);
            poision.setPosion(monsterController);
            monsterController.isIce = true;
        });
    }





}
