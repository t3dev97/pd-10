﻿using Studio.BC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BCSkillMutilBullet : MonoBehaviour,IFSkillBase
{
    public Dictionary<int,BCSkillMutilBulletData> dataSkillsMutil;
    private bool isAddSkill=false;
    private bool isUseBulletCenter = true;
    private float _distanceFontArrow = .5f;

    void Start()
    {
        dataSkillsMutil = new Dictionary<int, BCSkillMutilBulletData>();
    }

    public  void AddSkill(int id,BCSkillBaseData data)
    {
        isAddSkill = true;
        if(!dataSkillsMutil.ContainsKey(id))
             dataSkillsMutil.Add(id,(BCSkillMutilBulletData)data);
    }

    public void InitSkill()
    {
        StartCoroutine(IEMutilShot());
    }

    public IEnumerator IEMutilShot()
    {
        BCSkillMutilBulletData data = (BCSkillMutilBulletData) BCCache.Api.DataConfig.SkillBases[(int)EnumSkillMutilAttackID.MultiShot];
        for (int i = 0; i < data.Level; i++)
        {
            shotSkill();
            yield return new WaitForSeconds(((1 / PlayerDetail.api.characterAsset.BasicAS) / data.Level) / 30);

            
        }
    }

    public void shotSkill()
    {
        if (isAddSkill)
        {
            if (dataSkillsMutil.ContainsKey((int)EnumSkillMutilAttackID.FrontArrow))
            {
                FrontArrowSkill();
            }
            if (dataSkillsMutil.ContainsKey((int)EnumSkillMutilAttackID.RearArrow))
            {
                RearArrowSkill();
            }
            if (dataSkillsMutil.ContainsKey((int)EnumSkillMutilAttackID.Branching))
            {
                BranchingSkill();
            }
            if (dataSkillsMutil.ContainsKey((int)EnumSkillMutilAttackID.SideArrow))
            {
                SideArrowSkill();
            }

            if (isUseBulletCenter)
                 PlayerController.api.ShootVirtual();
        }
        else
            PlayerController.api.ShootVirtual();
    }

   


    public void FrontArrowSkill() // thêm 2 tia phía trước
    {
        if (dataSkillsMutil[(int)EnumSkillMutilAttackID.FrontArrow].Level % 2 == 0)
        {
            isUseBulletCenter = false;
            for (int i=0;i< dataSkillsMutil[(int)EnumSkillMutilAttackID.FrontArrow].Level / 2; i++)
            {

                if (i != 0)
                {
                    PlayerController.api.ShootVirtual( ((_distanceFontArrow * (i + 1)))- (_distanceFontArrow / 2));
                    PlayerController.api.ShootVirtual(-((_distanceFontArrow * (i + 1))) + (_distanceFontArrow / 2));
                }
                else
                {
                    PlayerController.api.ShootVirtual( ((_distanceFontArrow/2) * (i + 1)));
                    PlayerController.api.ShootVirtual( -((_distanceFontArrow/2) * (i + 1)));
                }
            }
        }
        else
        {
            isUseBulletCenter = true;
            for (int i = 0; i < dataSkillsMutil[(int)EnumSkillMutilAttackID.FrontArrow].Level / 2; i++)
            {
                PlayerController.api.ShootVirtual( (_distanceFontArrow * (i + 1)));
                PlayerController.api.ShootVirtual( -(_distanceFontArrow * (i + 1)));
            }
        }
    }

    void SideArrowSkill() 
    {
        float z =PlayerController.api.transform.rotation.y;
        if (dataSkillsMutil[(int)EnumSkillMutilAttackID.SideArrow].Level % 2 == 0)
        {
            for (int i = 0; i < dataSkillsMutil[(int)EnumSkillMutilAttackID.SideArrow].Level / 2 ; i++)
            {
                if (i == 0)
                {
                    PlayerController.api.ShootVirtual( (_distanceFontArrow * (i)), 90);
                    PlayerController.api.ShootVirtual( (_distanceFontArrow * (i)), -90);
                }
                else
                {
                    PlayerController.api.ShootVirtual( -(_distanceFontArrow * (i)), 90);
                    PlayerController.api.ShootVirtual( (_distanceFontArrow * (i)), 90);

                    PlayerController.api.ShootVirtual( -(_distanceFontArrow * (i)), -90);
                    PlayerController.api.ShootVirtual( (_distanceFontArrow * (i)), -90);
                }
            }
        }
        else
        {
            for (int i = 0; i < dataSkillsMutil[(int)EnumSkillMutilAttackID.SideArrow].Level / 2; i++)
            {
                if (i != 0)
                {
                    PlayerController.api.ShootVirtual( -(((_distanceFontArrow) * (i + 1)))+ (_distanceFontArrow / 2), 90);
                    PlayerController.api.ShootVirtual( (((_distanceFontArrow ) * (i + 1)))- (_distanceFontArrow / 2), 90);

                    PlayerController.api.ShootVirtual( -(((_distanceFontArrow) * (i+1))) + (_distanceFontArrow / 2), -90);
                    PlayerController.api.ShootVirtual( (((_distanceFontArrow) * (i+1))) - (_distanceFontArrow / 2), -90);

                }
                else
                {
                    PlayerController.api.ShootVirtual( -((_distanceFontArrow/2) * (i+1)), 90);
                    PlayerController.api.ShootVirtual( ((_distanceFontArrow/2) * (i+1)), 90);

                    PlayerController.api.ShootVirtual( -((_distanceFontArrow/2) * (i + 1)), -90);
                    PlayerController.api.ShootVirtual( ((_distanceFontArrow/2) * (i + 1)), -90);

                }
            }
        
        }
    }

    void BranchingSkill() // 3 tia
    {
        int angle = 90 / (dataSkillsMutil[(int)EnumSkillMutilAttackID.Branching].Level);
        for (int i = 0; i < dataSkillsMutil[(int)EnumSkillMutilAttackID.Branching].Level - 1; i++)
        {
            PlayerController.api.ShootVirtual( 0,
             (angle * (i + 1)));
            PlayerController.api.ShootVirtual( 0,
                -(angle * (i + 1)));
        }
    }

    void RearArrowSkill()
    {
        if (dataSkillsMutil[(int)EnumSkillMutilAttackID.RearArrow].Level % 2 != 0)
        {
            for (int i = 0; i < dataSkillsMutil[(int)EnumSkillMutilAttackID.RearArrow].Level / 2; i++)
            {
                if (i != 0)
                {
                    PlayerController.api.ShootVirtual( ((_distanceFontArrow * (i + 1))) - (_distanceFontArrow / 2), 180);
                    PlayerController.api.ShootVirtual( -((_distanceFontArrow * (i + 1))) + (_distanceFontArrow / 2),180);
                }
                else
                {
                    PlayerController.api.ShootVirtual( ((_distanceFontArrow / 2) * (i + 1)), 180);
                    PlayerController.api.ShootVirtual( -((_distanceFontArrow / 2) * (i + 1)),180);
                }
            }
        }
        else
        {
            PlayerController.api.ShootVirtual(0, 180);
            for (int i = 1; i < dataSkillsMutil[(int)EnumSkillMutilAttackID.RearArrow].Level / 2; i++)
            {
                PlayerController.api.ShootVirtual( (_distanceFontArrow * (i + 1)), 180);
                PlayerController.api.ShootVirtual( -(_distanceFontArrow * (i + 1)),180);
            }
        }

    }

}
