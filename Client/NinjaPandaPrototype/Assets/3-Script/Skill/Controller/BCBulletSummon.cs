﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BCBulletSummon : MonoBehaviour
{


    // Start is called before the first frame update
    public int damage = 0;
    public EnumSkillEffect type;
    [SerializeField]
    private float _speedMove;
    [SerializeField]
    private float _time;
    [SerializeField]
    private int _timeDestroy;

    public Transform target;

    private Rigidbody rb;
    
    public void SetUpData(int dame,EnumSkillEffect type)
    {


        damage = dame;
        _speedMove = 30;
        rb = GetComponent<Rigidbody>();
        rb.velocity = Vector3.zero;
      //  Vector3 vec = PlayerController.api.posCreateBullet.position;
        Vector3 vec = PlayerController.api.gameObject.transform.position;
        vec.x += Random.RandomRange(-1.0f, 1.0f);
        vec.y += 2f;
       // vec.y += Random.RandomRange(-1, 1);
        transform.position = vec;
        this.type = type;

        if(SeleceMonster.api.Target == null || PlayerDetail.api.HP<=0)
        {
            Destroy(gameObject);
        }
        else
        {
            transform.LookAt(SeleceMonster.api.Target.transform, Vector3.up);
            target = SeleceMonster.api.Target.transform;
        }
        Destroy(gameObject,3);
    }

    void Update()
    {
        float step = _speedMove * Time.deltaTime;
        //rb.velocity = transform.forward * _speedMove;
        transform.position = Vector3.MoveTowards(transform.position, target.position, step);
    }

    public void OnTriggerEnter(Collider obj)
    {
        string sTag = obj.transform.tag;

        if (sTag == TagManager.api.Wall || sTag == TagManager.api.WallBorder
          && sTag != TagManager.api.MonsterDie && sTag != TagManager.api.Untagged)
        {
            //PlayerController.api.playerSkills.PlayEffectBullet(sTag, this, () =>
            //{

           
            Destroy(gameObject);
            //});
        }
        else if (sTag == TagManager.api.Monster && obj.GetType() == typeof(CapsuleCollider)) // Xử lý với CapsuleColler của quái
        {


            switch (type)
            {
                case EnumSkillEffect.EffectBlast: BCSkillEffectBullet.api.AddBlast(obj.gameObject.transform);break;
                case EnumSkillEffect.EffectIce:  BCSkillEffectBullet.api.AddIce(obj.gameObject.transform); break;
                case EnumSkillEffect.EffectLight: BCSkillEffectBullet.api.AddSkillBolt(obj.gameObject.transform); break;
                case EnumSkillEffect.EffectPosion: BCSkillEffectBullet.api.AddPosionSkill(obj.gameObject.transform); break;
            }
            obj.GetComponent<MonsterController_new>().TakeDamage(damage, false);

            Destroy(gameObject);
        }

    }
}
