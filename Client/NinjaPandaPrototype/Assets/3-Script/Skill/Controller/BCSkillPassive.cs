﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BCSkillPassive : MonoBehaviour, IFSkillBase
{
    public Dictionary<int, BCSkillPassiveData> dataSkillsPassive;
    private bool isAddSkill = false;
    private float factorHp=0;

    public void AddSkill(int id, BCSkillBaseData data)
    {
        isAddSkill = true;
        if (!dataSkillsPassive.ContainsKey(id))
            dataSkillsPassive.Add(id, (BCSkillPassiveData)data);
    }

    public void Start()
    {
        dataSkillsPassive = new Dictionary<int, BCSkillPassiveData>();
    }

    public void InitSkill()
    {
        factorHp = (1 - (PlayerDetail.api.HP / PlayerDetail.api.MaxHPInStage));
        if (dataSkillsPassive.ContainsKey((int)EnumSkillPassiveID.Rage))
        {
            Rage();
        }
        if (dataSkillsPassive.ContainsKey((int)EnumSkillPassiveID.Fury))
        {
            Fury();
        }
        if (dataSkillsPassive.ContainsKey((int)EnumSkillPassiveID.Evasion))
        {
            Evasion();
        }
        if (dataSkillsPassive.ContainsKey((int)EnumSkillPassiveID.Chance))
        {
            Chance();
        }
        if (dataSkillsPassive.ContainsKey((int)EnumSkillPassiveID.Rampage))
        {
            Rampage();
        }
    }

    public void Rage()
    {
        PlayerDetail.api.DamageAtkInStage -= PlayerDetail.api.dameBonus;
        PlayerDetail.api.dameBonus  =  (PlayerDetail.api.TGDamageAtkInStage* factorHp);
        PlayerDetail.api.DamageAtkInStage += PlayerDetail.api.dameBonus;
    }
    public void Fury()
    {
        PlayerDetail.api.SpeedAtkInStage += PlayerDetail.api.speedBonus;
        PlayerDetail.api.speedBonus =(0.2f * factorHp);
        PlayerDetail.api.SpeedAtkInStage -= PlayerDetail.api.speedBonus;
    }
    public void Evasion()
    {
        PlayerDetail.api.DodgeInStage -= PlayerDetail.api.dogeBonus;
        PlayerDetail.api.dogeBonus = (PlayerDetail.api.TGDodge * factorHp);
        PlayerDetail.api.DodgeInStage += PlayerDetail.api.dogeBonus;
    }
    public void Chance()
    {
        PlayerDetail.api.CrInStage -= PlayerDetail.api.chanceBonus;
        PlayerDetail.api.chanceBonus = (PlayerDetail.api.TGCrInStage * factorHp);
        PlayerDetail.api.CrInStage += PlayerDetail.api.chanceBonus;
    }
    public void Rampage()
    {
        PlayerDetail.api.CdInStage -= PlayerDetail.api.rampageBonus;
        PlayerDetail.api.rampageBonus = (PlayerDetail.api.TGCdInStage * factorHp);
        PlayerDetail.api.CdInStage += PlayerDetail.api.rampageBonus;
    }
}
