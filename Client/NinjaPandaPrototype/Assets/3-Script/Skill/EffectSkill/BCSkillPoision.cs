﻿using System.Collections;
using UnityEngine;

public class BCSkillPoision : MonoBehaviour
{
    public int mDame;
    public float mCooldownDame;
    public float mTime;

    //public void Awake()
    //{
    //    Debug.Log(" Awake");
    //}

    //public void Start()
    //{
    //    Debug.Log(" Start");
    //    Play();

    //}

    public void SetData(int dame, float cooldownDame, int time)
    {
        //Debug.Log("Set data");
        mDame = dame;
        mCooldownDame = cooldownDame;
        mTime = time;
    }
    public void Play()
    {
        StopCoroutine(TakeDameFromToTime());
        StartCoroutine(TakeDameFromToTime());
    }

    IEnumerator TakeDameFromToTime()
    {
        //Debug.Log("TakeDameFromToTime");
        if (mTime <= 0)
        {

            while (true)
            {
                yield return new WaitForSeconds(mCooldownDame);
                transform.parent.GetComponent<MonsterController_new>().TakeDamage(mDame,  false);
            }
        }


    }
}
