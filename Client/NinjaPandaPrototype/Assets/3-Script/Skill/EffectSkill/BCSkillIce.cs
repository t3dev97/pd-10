﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BCSkillIce : MonoBehaviour
{
    public float mDame;
    public float mEffect;
    public float mTime;
    public bool isLoad = false;

    public void SetData(float dame, float effect, int time)
    {
        mDame = dame;
        mEffect = effect;
        //mCooldownDame = (time * 1.0f) / cooldownDame;
        mTime = time;
        Play();
    }
    public void Play()
    {
        StopAllCoroutines();
        StartCoroutine(TakeDameFromToTime());
    }

    IEnumerator TakeDameFromToTime()
    {
        if (mTime > 0)
        {
            transform.parent.GetComponent<MonsterController_new>().SlowAtk(mEffect);

            yield return new WaitForSeconds(mTime);

            transform.parent.GetComponent<MonsterController_new>().ResetSlowAtk();

            gameObject.SetActive(false);
        }
    
      


    }
}
