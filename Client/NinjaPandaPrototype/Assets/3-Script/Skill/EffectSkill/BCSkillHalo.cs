﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BCSkillHalo : MonoBehaviour
{
    public int mIntBlock=0;
    public void SetData(float intBlock,float radius)
    {
        mIntBlock = (int)intBlock;
        float scale = radius * BCHardCode.SizeScale;
        transform.parent.transform.localScale = new Vector3(scale, scale, scale);
    }
    public void OnTriggerEnter(Collider other)
    {
        string tag = other.tag;
        if(tag.Equals("Bullet") || tag.Equals("Monster"))
        {
            mIntBlock--;
            if (mIntBlock <= 0)
            {
                Destroy(transform.parent.gameObject);
            }
        }
    }
}
