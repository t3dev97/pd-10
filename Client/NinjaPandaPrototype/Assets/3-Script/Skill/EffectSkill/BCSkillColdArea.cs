﻿using Studio.BC;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BCSkillColdArea : MonoBehaviour
{
    public float mEffect;
    public float mRadius;
    public Transform mTransformEffect;
    public Action<Collider>  mAction;
    public void SetData(float effect,float radius,Action<Collider> action)
    {
        mEffect = effect;
        mRadius = radius;
        mAction = action;
        //mTransformEffect = gameObject.transform.GetChild(0);
        //mTransformEffect.localScale = Vector3.zero;
        float scale = BCHardCode.SizeScale * mRadius;
        mTransformEffect.localScale = new Vector3(scale, scale, scale);
    }

    public void OnTriggerEnter(Collider other)
    {
        Debug.Log("Monster");
        if (other.tag.Equals("Monster"))
        {
            mAction(other);
            //BCCache.Api.GetEffectPrefabAsync(EffectKey.FXCold, (go) =>
            //{

            //    BCCache.Api.GetEffectPrefabAsync(EffectKey.FXPoision, (go) =>
            //    {
            //        MonsterController_new monsterController = parent.gameObject.GetComponent<MonsterController_new>();
            //        if (!monsterController.isPoision)
            //        {
            //            var game = Instantiate(go, parent, false);
            //            game.transform.position = parent.position;
            //            BCSkillPoision poision = game.GetComponent<BCSkillPoision>();
            //            //Debug.Log(dataSkillsEffect[EnumSkillEffectAttackID.Poision].effect);
            //            poision.SetData((int)(PlayerDetail.api.BasicATKDamage * ((dataSkillsEffect[(int)EnumSkillEffectAttackID.Poision].DameAttack * 1.0f) / 100)), dataSkillsEffect[(int)EnumSkillEffectAttackID.Poision].effect, dataSkillsEffect[(int)EnumSkillEffectAttackID.Poision].time);
            //            poision.Play();
            //            monsterController.isPoision = true;
            //        }

            //    });
            //});

        }
    }
}
