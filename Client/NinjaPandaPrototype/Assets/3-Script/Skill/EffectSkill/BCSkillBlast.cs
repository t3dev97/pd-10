﻿using System.Collections;
using UnityEngine;

public class BCSkillBlast : MonoBehaviour
{
    public int mDame;
    public float mCooldownDame;
    public float mTime;
    public bool isLoad = false;


    public void SetData(int dame, float cooldownDame, int time)
    {
        mDame = dame;
        mCooldownDame = (time * 1.0f) / cooldownDame;
        mTime = time;
        Play();
    }
    public void Play()
    {
        StopAllCoroutines();
        StartCoroutine(TakeDameFromToTime());
    }

    IEnumerator TakeDameFromToTime()
    {
        if (mTime > 0)
        {
            while (mTime > mCooldownDame)
            {
                yield return new WaitForSeconds(mCooldownDame);
                transform.parent.GetComponent<MonsterController_new>().TakeDamage(mDame, false);
                mTime -= mCooldownDame;

            }
            gameObject.SetActive(false);
        }
    }
}
