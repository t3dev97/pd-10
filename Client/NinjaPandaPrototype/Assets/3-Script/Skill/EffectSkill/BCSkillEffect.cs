﻿using System.Collections;
using UnityEngine;

public class BCSkillEffect : MonoBehaviour
{
    public float mDame;
    public float mCooldownDame;
    public float mTime;
    public EnumSkillEffect effect;
    public MonsterController_new monsterController;
    private Vector3 posEffect = Vector3.zero;

    private float _mTimeDameBlast;
    public void SetData(float dame, float CooldownDame, int time, MonsterController_new monsterController = null)
    {
        this.monsterController = monsterController;
        mDame = dame;
        mCooldownDame = CooldownDame;
        mTime = time;
        _mTimeDameBlast = (time * 1.0f) / CooldownDame;
        PlayEffect(effect);
    }
    public void setPosion(MonsterController_new monster)
    {
        Vector3 vec = posEffect;
        switch (effect)
        {
            case EnumSkillEffect.EffectBlast: vec.y += monster._myDetail.OffSetSelectionTop.y; break;
            case EnumSkillEffect.EffectIce: break;
            case EnumSkillEffect.EffectLight: vec.y += monster._myDetail.OffSetSelectionTop.y; break;
            case EnumSkillEffect.EffectPosion: vec.y += monster._myDetail.OffSetSelectionTop.y; break;

        }
        transform.position = new Vector3(monster.transform.position.x, vec.y, monster.transform.position.z);

        Vector3 vec1 = transform.localPosition;
        vec1.y = monster._myDetail.OffSetSelectionScaleFX.y;
        transform.localPosition = vec1;

    }
    public IEnumerator delayAni()
    {
        ParticleSystem particle = transform.GetChild(0).GetComponent<ParticleSystem>();
        particle.enableEmission = false;
        while (true)
        {
            yield return new WaitForSeconds(0.2f);

            if (particle.particleCount == 0)
            {
                break;
            }
        }
        gameObject.SetActive(false);
    }
    public void PlayEffect(EnumSkillEffect effect)
    {
        StopAllCoroutines();
        ParticleSystem particle = transform.GetChild(0).GetComponent<ParticleSystem>();
        if (effect == EnumSkillEffect.EffectIce)
            particle.enableEmission = false;
        else
            particle.enableEmission = true;

        switch (effect)
        {
            case EnumSkillEffect.EffectBlast: StartCoroutine(TakeDameFromBlast()); break;
            case EnumSkillEffect.EffectIce: StartCoroutine(TakeDameFromIce()); break;
            case EnumSkillEffect.EffectLight: break;
            case EnumSkillEffect.EffectPosion: StartCoroutine(TakeDameFromPoision()); break;
        }
    }

    IEnumerator TakeDameFromBlast()
    {
        if (mTime > 0)
        {
            while (mTime > _mTimeDameBlast)
            {
                yield return new WaitForSeconds(_mTimeDameBlast);
                transform.parent.GetComponent<MonsterController_new>().TakeDamage(mDame, false, true);
                mTime -= _mTimeDameBlast;

            }
            StartCoroutine(delayAni());
            //gameObject.SetActive(false);
        }
    }


    IEnumerator TakeDameFromIce()
    {
        if (mTime > 0)
        {
            monsterController.FXIce();
            transform.parent.GetComponent<MonsterController_new>().SlowMs(mCooldownDame);
            yield return new WaitForSeconds(mTime);
            transform.parent.GetComponent<MonsterController_new>().ResetMS();
            monsterController.ResetFXIce();
            gameObject.SetActive(false);
        }
    }

    IEnumerator TakeDameFromPoision()
    {
        if (mTime <= 0)
        {

            while (true)
            {
                yield return new WaitForSeconds(mCooldownDame);
                transform.parent.GetComponent<MonsterController_new>().TakeDamage(mDame, false, true);
            }
        }


    }
}




























































































