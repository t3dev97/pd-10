﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BCSkillSummonArea : MonoBehaviour
{
    public EnumSkillEffect Type;
    public StyleSummon style = StyleSummon.area;
    public float mTime;
    public float mRadius;
    public int mDame;
    public Transform mTransformEffect;
    public CapsuleCollider collider;
    //public Action<Collider> mAction;
    public bool isEnableDame=true;
    public void SetData(int dame,float time, float radius)
    {
        mDame = dame;
        mTime = time;
        mRadius = radius;
        collider = gameObject.GetComponent<CapsuleCollider>();
        float scale = BCHardCode.SizeScale * mRadius;
        mTransformEffect.localScale = new Vector3(scale, scale, scale);

        if (SeleceMonster.api.Target == null || PlayerDetail.api.HP <= 0)
        {
            Destroy(gameObject);
        }

        switch (style)
        {
            case StyleSummon.area: StartCoroutine(DameAoe()); break;
        }
        
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Monster"))
        {
            if (isEnableDame)
            {
                MonsterController_new monster = other.GetComponent<MonsterController_new>();

                monster.TakeDamage(mDame,false);

                switch (Type)
                {
                    case EnumSkillEffect.EffectBlast: BCSkillEffectBullet.api.AddBlast(other.gameObject.transform); break;
                    case EnumSkillEffect.EffectIce: BCSkillEffectBullet.api.AddIce(other.gameObject.transform); break;
                    case EnumSkillEffect.EffectLight: BCSkillEffectBullet.api.AddSkillBolt(other.gameObject.transform); break;
                    case EnumSkillEffect.EffectPosion: BCSkillEffectBullet.api.AddPosionSkill(other.gameObject.transform); break;

                }
            }
         }

    }
    public IEnumerator DameAoe()
    {
        while (true)
        {
            collider.enabled = false;
            isEnableDame = false;
            yield return new WaitForSeconds(mTime);
            isEnableDame = true;
            collider.enabled = true;
            yield return new WaitForSeconds(1);
        }
    }

    
}

