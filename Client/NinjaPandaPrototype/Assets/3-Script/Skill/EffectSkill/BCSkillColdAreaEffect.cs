﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BCSkillColdAreaEffect : MonoBehaviour
{
    public float mEffect;
    public float mTime;
    public bool isLoad = false;

    public void SetData( float effect, float time)
    {
        mEffect = effect;
        mTime = time;
        Play();
    }
    public void Play()
    {
        StopAllCoroutines();
        StartCoroutine(TakeDameFromToTime());
    }

    IEnumerator TakeDameFromToTime()
    {
        if (mTime > 0)
        {
            transform.parent.GetComponent<MonsterController_new>().SlowMs(mEffect);

            yield return new WaitForSeconds(mTime);

            transform.parent.GetComponent<MonsterController_new>().ResetMS();
            transform.parent.GetComponent<MonsterController_new>().isCold = false;
            Destroy(gameObject);
        }
    
      


    }
}
