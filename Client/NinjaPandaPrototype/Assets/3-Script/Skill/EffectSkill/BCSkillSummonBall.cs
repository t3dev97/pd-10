﻿using Studio.BC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BCSkillSummonBall : MonoBehaviour
{
    public int dame;
    public float time;
    public float radius;
    public float timeLife;
    public EnumSkillEffect type;
    //public void SetData(int dame,int ra)
    public void setData(BCSkillSumonBulletData data)
    {
        dame =(int)( PlayerDetail.api.BasicATKDamage * ((data.DameEffect*1.0f) / 100));
        time = data.time;
        radius = data.radius;
        timeLife = data.Effect;
        if (SeleceMonster.api.Target == null || PlayerDetail.api.HP <= 0)
        {
            Destroy(gameObject);
        }

    }
    public void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag.Equals(TagManager.api.Ground))
        {
            switch (type)
            {
                case EnumSkillEffect.EffectBlast:
                    BCCache.Api.GetEffectPrefabAsync(EffectKey.FireBallArea, (go) => {
                        SetDataPrefab(go);
                    });

                    break;
                case EnumSkillEffect.EffectIce:
                    BCCache.Api.GetEffectPrefabAsync(EffectKey.SnowBallArea, (go) => {
                        SetDataPrefab(go);
                    });

                    break;
            }

        }
        if (collision.gameObject.tag.Equals(TagManager.api.Monster))
        {
            //collision.GetComponent<MonsterController_new>().TakeDamage(dame, false);
         
        }
    }

   public void SetDataPrefab(GameObject go)
    {
        GameObject game = Instantiate(go);
        game.transform.position = transform.position;

        BCSkillSummonArea skill = game.GetComponent<BCSkillSummonArea>();
        skill.SetData(dame, time, radius);

        Destroy(game, timeLife);
        Destroy(gameObject);
    }
}
