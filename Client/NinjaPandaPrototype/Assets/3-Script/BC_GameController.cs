﻿using System;
using UnityEngine;

public class BC_GameController : MonoBehaviour
{
    public static BC_GameController api;
    public GameObject Player;
    public BC_Chapter_Info Chapter;
    public Camera Camera;
    public Vector3 posCamera;
    public float heSoScale;
    public float distanceCamPlayer;


    public void Awake()
    {

        //Player.SetActive(true);
        //Player.transform.localPosition = (Chapter.posStart*heSoScale);
        if (api == null)
            api = this;
        else if (api != this)
            Destroy(gameObject);
        heSoScale = (float)HeSoScale();
        //PlayerPrefs.DeleteAll();
        //PlayerPrefs.SetInt("chap", PlayerPrefs.GetInt("chap", 1));
        //PlayerPrefs.SetInt("round", PlayerPrefs.GetInt("round ", 1));
        if (Player == null)
            Player = PlayerController.api.gameObject;

        Vector3 vec = Chapter.transform.GetChild(0).localScale;
    }
    public void reset()
    {

        Player.transform.localPosition = Chapter.posStart * heSoScale;
        Vector3 vec = Player.transform.localPosition;
        //vec.y = 0.8f;
        vec.x = vec.x + 0.2f;
        Player.transform.localPosition = vec;
        Camera.GetComponent<BCMoveCam>().updateDistranCam();
        //distanceV3 = gameObject.transform.position - player.transform.position;
    }
    public double HeSoScale()
    {
        double apha = (90 - Camera.transform.eulerAngles.x);
        double angle = Math.Cos(DegreeToRadian(apha));
        double heso = 1f / angle;
        return heso;
    }
    public double DegreeToRadian(double angle)
    {
        return Math.PI * angle / 180.0;
    }
}
