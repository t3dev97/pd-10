﻿using UnityEngine;

public class BC_ObjectInfo : MonoBehaviour
{
    public int Id;
    public STYLE_BRUSHES style;
    public Vector3 pos;
    private void Start()
    {
        if (style == STYLE_BRUSHES.monster || style == STYLE_BRUSHES.boss)
        {
            BC_Chapter_Info.api.ListMonster.Add(gameObject);
        }
    }
    private void OnDisable()
    {
        BC_Chapter_Info.api.ListMonster.Remove(gameObject);
        BC_Chapter_Info.api.ChechkOpenDoor();
    }
}
