﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BC_DataConfig : MonoBehaviour
{
    public static Dictionary<int, GameObject> currentTileSetObjects;
    public static Dictionary<int, Material> currentTileSetMaterial;
    public static Dictionary<int, Dictionary<int,GameObject>> currentTileSetTexture;
    //public static Dictionary<int, GameObject> currentTileSetTextureMain;
    public static Dictionary<int, GameObject> currentTileSetModeMoster;
    public static Dictionary<int, GameObject> currentTileSetDoor;
    public static int intJumpWater;
    public static int currentBrushIndex;
}
