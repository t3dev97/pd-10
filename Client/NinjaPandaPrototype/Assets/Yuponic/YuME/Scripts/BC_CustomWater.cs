﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class BC_CustomWater : MonoBehaviour
{
    public int id;
    public BC_CustomWater WaterTop;
    public BC_CustomWater WaterBot;
    public BC_CustomWater WaterRight;
    public BC_CustomWater WaterLeft;
    public BC_CustomWater WaterTopLeft;
    public BC_CustomWater WaterTopRight;
    public BC_CustomWater WaterBotLeft;
    public BC_CustomWater WaterBotRight;
    public int countWater = 0;
    private int hesoJump;
   public  enum statusWater
    {
        MUNTIPLE,
        SINGLE
    }
    public statusWater status = statusWater.SINGLE;
    private GameObject[,] _listGameObject;


    public void updateStatus(GameObject[,] listGameObject,int heso)
    {
        hesoJump = heso;
        _listGameObject = listGameObject;
        // check ngang
        if (isExistWater(1,0))
        {
            WaterLeft = _listGameObject[1, 0].GetComponent<BC_CustomWater>();
            WaterLeft.WaterRight = this;
            WaterLeft.UpdateCountWater();
            
        }
        if (isExistWater(1, 2))
        {
            WaterRight = _listGameObject[1, 2].GetComponent<BC_CustomWater>();
            WaterRight.WaterLeft = this;
            WaterRight.UpdateCountWater();
        }
        if (isExistWater(0, 1))
        {
            WaterBot = _listGameObject[0, 1].GetComponent<BC_CustomWater>();
            WaterBot.WaterTop = this;
            WaterBot.UpdateCountWater();
        }
        if (isExistWater(2, 1))
        {
            WaterTop = _listGameObject[2, 1].GetComponent<BC_CustomWater>();
            WaterTop.WaterBot = this;
            WaterTop.UpdateCountWater();
        }

        if (isExistWater(2, 0))
        {
            WaterTopLeft = _listGameObject[2, 0].GetComponent<BC_CustomWater>();
            WaterTopLeft.WaterBotRight = this;
            WaterTopLeft.UpdateCountWater();
        }
        if (isExistWater(2, 2))
        {
            WaterTopRight = _listGameObject[2, 2].GetComponent<BC_CustomWater>();
            WaterTopRight.WaterBotLeft = this;
            WaterTopRight.UpdateCountWater();
        }
        if (isExistWater(0, 0))
        {
            WaterBotLeft = _listGameObject[0, 0].GetComponent<BC_CustomWater>();
            WaterBotLeft.WaterTopRight = this;
            WaterBotLeft.UpdateCountWater();
        }
        if (isExistWater(0, 2))
        {
            WaterBotRight= _listGameObject[0, 2].GetComponent<BC_CustomWater>();
            WaterBotRight.WaterTopLeft= this;
            WaterBotRight.UpdateCountWater();
        }
        UpdateCountWater();
        updateImage();
        updateAllWater();
    }

    public void UpdateCountWater()
    {
        countWater = 0;
        if (WaterLeft != null)
        {
            countWater++;
        }
        if (WaterTopLeft != null)
        {
            countWater++;
        }
        if (WaterTop != null)
        {
            countWater++;
        }
        if (WaterTopRight != null)
        {
            countWater++;
        }

        if (WaterRight != null)
        {
            countWater++;
        }
        if (WaterBotRight != null)
        {
            countWater++;
        }
        if (WaterBot != null)
        {
            countWater++;
        }
        if (WaterBotLeft != null)
        {
            countWater++;
        }
    }
    public void updateAllWater()
    {
        if (WaterLeft != null)
        {
            BC_CustomWater tg = WaterLeft;

            while (tg != null)
            {
                tg.updateImage();
                tg = tg.WaterLeft;
            }
            //WaterLeft.updateImage();
        }
        if (WaterTopLeft != null)
        {
            WaterTopLeft.updateImage();
        }
        if (WaterTop != null)
        {
            BC_CustomWater tg = WaterTop;

            while (tg != null)
            {
                tg.updateImage();
                tg = tg.WaterTop;
            }

            WaterTop.updateImage();
        }
        if (WaterTopRight != null)
        {
            WaterTopRight.updateImage();
        }

        if (WaterRight != null)
        {
            WaterRight.updateImage();
        }
        if (WaterBotRight != null)
        {
            WaterBotRight.updateImage();
        }
        if (WaterBot != null)
        {
            WaterBot.updateImage();
        }
        if (WaterBotLeft != null)
        {
            WaterBotLeft.updateImage();
        }
    }
    public void updateImage()
    {

        if (WaterLeft == null && WaterTop == null && WaterRight != null && WaterBot == null)
            ReplaceWater(12);


        else if (WaterLeft != null && WaterRight == null && countWater < 2)
            ReplaceWater(13);
        else if (WaterLeft != null && WaterRight != null && WaterTop == null && WaterBot == null)
            ReplaceWater(2);
        else if (WaterBot != null && WaterLeft != null && WaterTop == null && WaterRight == null && WaterBotLeft == null)
            ReplaceWater(9);
        else if (WaterTop != null && WaterBot == null && WaterRight == null && WaterLeft == null)
            ReplaceWater(15);
        else if (WaterTop != null && WaterBot != null && WaterLeft == null && WaterRight == null)
            ReplaceWater(3);
        else if (WaterTop != null && WaterLeft != null && WaterRight == null && WaterBot == null && WaterTopLeft == null)
            ReplaceWater(11);
        else if (WaterTop != null && WaterRight != null && WaterLeft == null && WaterBot == null && WaterTopRight == null)
            ReplaceWater(10);
        else if (WaterRight != null && WaterBot != null && WaterLeft == null && WaterTop == null && WaterBotRight == null)
            ReplaceWater(8);
        else if (WaterRight == null && WaterBot != null && WaterLeft == null && WaterTop == null)
            ReplaceWater(14);
        else if (WaterRight != null && WaterBot != null && WaterLeft != null && WaterTop != null && WaterTopRight != null && WaterBotLeft != null && WaterTopLeft != null && WaterBotRight != null)
        {
            ReplaceWater(1);
            status = statusWater.MUNTIPLE;
        }

        else if ((WaterLeft == null && WaterBot != null && WaterTop == null && WaterRight != null && WaterBotRight != null && WaterBotRight.GetComponent<BC_CustomWater>().status == statusWater.MUNTIPLE)
            || (WaterRight != null && WaterBot != null && WaterBotRight != null && countWater == 3))
        {
            ReplaceWater(4);
            status = statusWater.MUNTIPLE;
        }
        else if ((WaterLeft != null && WaterBot != null && WaterTop == null && WaterRight == null && WaterBotLeft != null && WaterBotLeft.GetComponent<BC_CustomWater>().status == statusWater.MUNTIPLE)
            || (WaterLeft != null && WaterBot != null && WaterBotLeft != null && countWater == 3))

        {
            ReplaceWater(5);
            status = statusWater.MUNTIPLE;
        }
        else if ((WaterLeft != null && WaterBot == null && WaterTop != null && WaterRight == null && WaterTopLeft != null && WaterTopLeft.GetComponent<BC_CustomWater>().status == statusWater.MUNTIPLE)
          || (WaterTop != null && WaterLeft != null && WaterTopLeft != null && countWater == 3))
        {
            ReplaceWater(7);
            status = statusWater.MUNTIPLE;
        }
        else if ((WaterLeft == null && WaterBot == null && WaterTop != null && WaterRight != null && WaterTopRight != null && WaterTopRight.GetComponent<BC_CustomWater>().status == statusWater.MUNTIPLE)
         || (WaterTop != null && WaterRight != null && WaterTopRight != null && countWater == 3))
        {
            ReplaceWater(6);
            status = statusWater.MUNTIPLE;
        }
        else if ((WaterRight == null && WaterTop != null && WaterBot != null && WaterLeft != null && WaterLeft.GetComponent<BC_CustomWater>().status == statusWater.MUNTIPLE)
             || (WaterRight == null && ((WaterTop != null && WaterTop.GetComponent<BC_CustomWater>().status == statusWater.MUNTIPLE)
             || (WaterBot != null && WaterBot.GetComponent<BC_CustomWater>().status == statusWater.MUNTIPLE))))
        {
            ReplaceWater(17);
            status = statusWater.MUNTIPLE;
        }
        else if (WaterLeft == null && WaterTop != null && WaterBot != null && WaterRight != null && WaterRight.GetComponent<BC_CustomWater>().status == statusWater.MUNTIPLE)
        {
            ReplaceWater(16);
            status = statusWater.MUNTIPLE;
        }
        else if (WaterBot == null && WaterLeft != null && WaterRight != null && WaterTop != null && WaterTop.GetComponent<BC_CustomWater>().status == statusWater.MUNTIPLE)
        {
            ReplaceWater(19);
            status = statusWater.MUNTIPLE;
        }
        else if ((WaterTop == null && WaterLeft != null && WaterRight != null && WaterBot != null && WaterBot.GetComponent<BC_CustomWater>().status == statusWater.MUNTIPLE)
        || (WaterTop == null && ((WaterLeft != null && WaterLeft.GetComponent<BC_CustomWater>().status == statusWater.MUNTIPLE)
        || (WaterRight != null && WaterRight.GetComponent<BC_CustomWater>().status == statusWater.MUNTIPLE))))
        {
            ReplaceWater(18);
            status = statusWater.MUNTIPLE;
        }
        else if (WaterBotRight == null && WaterLeft != null && WaterRight != null && WaterTop != null && WaterBot != null && WaterTopLeft != null && WaterTopRight != null && WaterBotLeft != null)
        {
            status = statusWater.MUNTIPLE;
            ReplaceWater(20);
        }

        else if (WaterBotLeft == null && WaterLeft != null && WaterRight != null && WaterTop != null && WaterBot != null && WaterTopLeft != null && WaterTopRight != null && WaterBotRight != null)
        {
            status = statusWater.MUNTIPLE;
            ReplaceWater(21);
        }

        else if (WaterTopRight == null && WaterLeft != null && WaterRight != null && WaterTop != null && WaterBot != null && WaterTopLeft != null && WaterBotRight != null && WaterBotLeft != null)
        {
            status = statusWater.MUNTIPLE;
            ReplaceWater(22);
        }

        else if (WaterTopLeft == null && WaterLeft != null && WaterRight != null && WaterTop != null && WaterBot != null && WaterBotRight != null && WaterTopRight != null && WaterBotLeft != null)
        {
            status = statusWater.MUNTIPLE;
            ReplaceWater(23);
        }
        else
        {
            status = statusWater.SINGLE;
            ReplaceWater(0);
        }
        


    }
    public bool isExistWater(int x,int y)
    {
        if (_listGameObject[x, y] != null
            && _listGameObject[x, y].GetComponent<BC_ObjectInfo>().style == STYLE_BRUSHES.water)
            return true;
        return false;
    }
    public void ReplaceWater(int index)
    {
        //currentTile = currentTileSetTexture[(item.indexObject / intJumpWater) + 1][item.indexObject % intJumpWater];
        //Debug.Log("iNDEX " + index + "currentTileSetTexture" + index * (BC_DataConfig.currentBrushIndex + 1));
        int newIndex = index + ((BC_DataConfig.intJumpWater) * hesoJump);
        //GameObject game= BC_DataConfig.currentTileSetTexture[newIndex].transform.GetChild(0).gameObject;
        GameObject game = BC_DataConfig.currentTileSetTexture[hesoJump+1][index % (BC_DataConfig.intJumpWater)].transform.GetChild(0).gameObject;
        gameObject.transform.GetChild(0).gameObject.GetComponent<SpriteRenderer>().sprite = game.GetComponent<SpriteRenderer>().sprite;
        gameObject.name = game.name;
        id = newIndex;
        gameObject.GetComponent<BC_ObjectInfo>().Id = newIndex;
    }
}
