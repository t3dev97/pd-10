﻿using Studio.BC;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[Serializable]
public class MonsterArray
{
    public List<GameObject> listMonster;
}
[Serializable]
public class NPCArray
{
    public GameObject game;
    public Vector3 posion;
    public int index;
}
public class BC_Chapter_Info : MonoBehaviour
{
    public static BC_Chapter_Info api;

    public GateController Gate; // ánh sáng cửa 
    public GameObject door;

    public Transform EffectContainer;
    public Transform GoldContainer;
    public List<GameObject> listStage;
    public List<NPCArray> listNPC;
    public List<MonsterArray> listStageMonter;
    private List<GameObject> listMonster; // dung de tim qua ban 

    public Vector3 posStart;
    public Vector3 posEnd;
    public Vector3 showMonster;
    public Vector3 posNPC;

    public int chapter;
    public int stage;
    public int roundStage = 0;
    public int IDMap;
    public int currenStage = 0;

    public float time;

    public bool isDungeon;
    public bool isStageBoos;
    public bool isNextMap = false;
    public bool FinalStage = false; // true khi stage được hoàn thành
    public float TimeDelayMonsterAtt;
    bool _AllowMonsterAtk = false;

    public bool AllowMonsterAtk { get => _AllowMonsterAtk; set => _AllowMonsterAtk = value; }
    int max = -1;
    public List<GameObject> ListMonster
    {
        get
        {
            return listMonster;
        }
        set
        {
            listMonster = value;
        }
    }
    public void ChechkOpenDoor()
    {
        if (listMonster.Count >= max)
        {
            max = listMonster.Count;
        }
        else if (listMonster.Count <= 0)
        {
            if (currenStage < listStage.Count && !isNextMap)
            {
                isNextMap = true;
                updateStage();
            }
            else
                OpenDoor();
        }
        else
        {
            max = listMonster.Count;
        }
    }
    public void Awake()
    {
        if (api == null)
            api = this;
        else if (api != this)
            Destroy(api);

        listStageMonter = new List<MonsterArray>();
        ListMonster = new List<GameObject>();
        PlayerPrefs.SetInt("CharacterExp", 0);
    }



    private void Start()
    {
        StartCoroutine(DelayLoad());
        StartCoroutine(BC_DataGame.api.LoadMap(() =>
        {
            InitMap.api.CreateNavigation();

            if (BC_Chapter_Info.api.isStageBoos)
            {
                UIBasicController.api.HealtBoss.gameObject.SetActive(true);
                UIBasicController.api.EXPObject.gameObject.SetActive(false);
            }
            else
            {
                UIBasicController.api.HealtBoss.gameObject.SetActive(false);
                UIBasicController.api.EXPObject.gameObject.SetActive(true);
            }
            StartCoroutine(DelayAllowMonsterAtk());
        })); ;

        Application.targetFrameRate = 60;
    }
    public IEnumerator DelayAllowMonsterAtk()
    {
        AllowMonsterAtk = false;
        yield return new WaitForSeconds(BCCache.Api.DataConfig.ConfigGameData.DelayAtkBeginStage);
        AllowMonsterAtk = true;
    }
    IEnumerator DelayLoad()
    {
        yield return new WaitForSeconds(0.5f);
        EffectContainer = FindObjectOfType<BCCache>().transform;
        GoldContainer = FindObjectOfType<BCCache>().transform;
        BCConfig.Api.CurrentScene = CurrentSceneName.Main;

        BC_DataGame.api.FindObject();
    }
    public void ResetList()
    {
        listStage.Clear();
        listStageMonter.Clear();
    }
    public bool CheckMonsterInStage()
    {
        foreach (Transform item in transform)
        {
            if (item.tag == TagManager.api.MonsterList)
            {
                if (item.childCount > 0)
                    return true;
            }
        }
        return false;
    }
    public void UpdateGold(int numberGold)
    {
        StartCoroutine(UpdateGoldDelay(numberGold));
    }
    public IEnumerator UpdateGoldDelay(int numberGold)
    {
        int goldTager = PlayerDetail.api.GoldInstage + numberGold;
        yield return new WaitForSeconds(1.0f);

        BC_Chapter_Info.api.MoveGoldToPlayer();

        LeanTween.value(PlayerDetail.api.GoldInstage, goldTager, 1.0f).setOnUpdate((float value) =>
        {
            UIBasicController.api.TmpGold.text = ((int)value).ToString();
        });

        UIBasicController.api.TmpGold.text = goldTager.ToString();
        PlayerDetail.api.GoldInstage = goldTager;
    }
    public GameObject AddListStageMonster(GameObject game, bool NextStage = false)
    {
        if (!NextStage)
        {
            if (listStageMonter.Count == 0)
            {
                MonsterArray monster = new MonsterArray();
                monster.listMonster = new List<GameObject>();
                listStageMonter.Add(monster);
            }
            else
            {
                listStageMonter[roundStage].listMonster.Add(game);
            }

        }
        else
        {
            if (listStageMonter.Count == 0)
            {
                isDungeon = false;
            }
            else
            {
                transform.GetChild(listStage.Count + 1).gameObject.SetActive(false);
                isDungeon = true;

                //listStageMonter[roundStage].listMonster.Add(game);
            }
            MonsterArray monster = new MonsterArray();
            monster.listMonster = new List<GameObject>();
            monster.listMonster.Add(game);
            listStageMonter.Add(monster);

            game.transform.SetParent(transform.GetChild(listStage.Count + 1));
            listStage.Add(transform.GetChild(listStage.Count + 1).gameObject);
        }
        return game;
    }
    public void RemoveListStageMonster(GameObject game)
    {
        if (listStageMonter != null && listStageMonter[0] != null
            && listStageMonter[0].listMonster != null)
        {
            listStageMonter[0].listMonster.Remove(game);
        }
        if (listStageMonter[0].listMonster.Count == 0)
        {
            listStageMonter.RemoveAt(0);
            if (listStageMonter.Count > 0)
            {
                updateStage();
            }
            UpdateExpInStage();
        }
    }
    public void updateStage()
    {
        if (listStage.Count > 0)
        {
            listStage[0].SetActive(true);
            listStage.RemoveAt(0);
        }
        isNextMap = false;
    }
    public void ResetRoundStage()
    {
        roundStage = 0;
    }

    public void OpenDoor()
    {
        if (!BCApiTest.api.isGoToTest)
        {
            if (BC_Chapter_Info.api.FinalStage == false)
            {
                FinalStage = true; // hoàn thành stage
                BCDataControler.api.data.dataTemp.RewardExpInLife += (BCCache.Api.DataConfig.StageInfos[BC_DataGame.api.currentIDMap].ExpUser);
                PlayerDetail.api.UpdateExpInStage();
                if (UIBasicController.api.EXPObject != null)
                    UIBasicController.api.EXPObject.gameObject.SetActive(true);
                StartCoroutine(Delay());
            }
        }
    }

    public void UpdateExpInStage()
    {
        //ClearGold();
        //MoveGoldToPlayer();
        //PlayerDetail.api.UpdateExpInStage();
    }
    IEnumerator Delay()
    {
        if (Gate != null)
            Gate.EnableGateEffect(true);

        MoveGoldToPlayer();
        yield return new WaitForSeconds(.5f);
        door.SetActive(false);
    }

    public void MoveGoldToPlayer()
    {
        foreach (Transform item in EffectContainer)
        {
            if (item != null && item.gameObject.GetComponent<ClearObj>() != null)
                item.gameObject.GetComponent<ClearObj>().Clear();
            else
                if (item.gameObject.GetComponent<InGameGoldController>() != null)
                item.gameObject.GetComponent<InGameGoldController>().MoveToPlayer();
            else
                Destroy(item.gameObject);
        }
    }

    public void ShowNPC(EnumNPCPopup npc)
    {
        Dictionary<int, NPC> listNpc = BCCache.Api.DataConfig.ConfigGameData.listNPC;
        GameObject objectNPC = Instantiate(BC_DataGame.api.npcMap[(int)npc]);

        Vector3 vec = door.transform.position;

        vec.z -= BC_GameController.api.heSoScale * listNpc[(int)npc].posion;

        objectNPC.transform.position = vec;
        AddListStageMonster(objectNPC, true);

    }
    //public void ClearGold()
    //{
    //    Debug.Log("ClearGold");
    //    foreach (Transform trans in GoldContainer)
    //    {
    //        var gold = trans.gameObject.GetComponent<InGameGoldController>();
    //        if (gold != null)
    //        {
    //            gold.MoveToPlayer();
    //        }
    //    }
    //}
}
