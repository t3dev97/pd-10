﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

    public enum STYLE_BRUSHES
    {
        ground,
        block,
        water,
        monster,
        door,
        custom,
        boss
    }
[CreateAssetMenu(fileName ="new DataMapEditor",menuName ="Data Map")]
public class BC_DataMapEditor : ScriptableObject
{
    [SerializeField]
    public Dictionary<int , MapStageEditor> listMap = new Dictionary<int, MapStageEditor>();

    public List< MapStageEditor> listMaptg = new List<MapStageEditor>();

}
[System.Serializable]
public class ObjectMapEditor
{
    public STYLE_BRUSHES style = STYLE_BRUSHES.block;
    public int indexObject;
    public Vector3 pos;
}
[System.Serializable]
public class ListStage
{
    [SerializeField]
    public List<ObjectMapEditor> list = new List<ObjectMapEditor>();
    
}
[Serializable]
public class MapStageEditor
{
    public int ID;
    public int Width;
    public int height;
    public bool isSymmetry;
    public int PlanIndex;
    public bool isDungeon;
    public int TimeCreateMonster=0;
    public List<ObjectMapEditor> listBlock = new List<ObjectMapEditor>();
    public List<ListStage> listMonster = new List<ListStage>();
   
}