#import <UnityAppController.h>

@interface StringTools : NSObject

+(NSString*) createNSString:(const char*)string;
+(char*) createCString:(const char*)string;

@end
