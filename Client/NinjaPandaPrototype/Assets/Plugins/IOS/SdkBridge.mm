
#import <UIKit/UIKit.h>
#import "SdkBridge.h"
#import <CommonCrypto/CommonCrypto.h>

@implementation StringTools

+(NSString*) createNSString:(const char*)string
{
    if(string)
        return [NSString stringWithUTF8String:string];
    else
        return [NSString stringWithUTF8String:""];
}

+(char*) createCString:(const char *)string
{
    if(string == NULL) return NULL;
    
    char* res = (char*)malloc(strlen(string) + 1);
    strcpy(res, string);
    
    return res;
}	

@end

extern "C"
{
    void scheduleLocalNotification(char* title, char* message, char* idx, int delayInSecond)
    {
        if(floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_7_1)
        {
            // Register for notifications if iOS 8
            UIUserNotificationType types = UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert;
            UIUserNotificationSettings *mySettings = [UIUserNotificationSettings settingsForTypes:types categories:nil];
            [[UIApplication sharedApplication] registerUserNotificationSettings:mySettings];
            [[UIApplication sharedApplication] registerForRemoteNotifications];
            
            NSLog(@"###### register for notifications");
        }
        
        NSDate* currentDate = [NSDate date];
        NSDate* notifyDate = [currentDate dateByAddingTimeInterval:delayInSecond];
        
        NSString* nsId = [StringTools createNSString:idx];
        NSDictionary* userInfo = @{ @"uid":nsId };
        
        UILocalNotification *notification = [[UILocalNotification alloc] init];
        if (notification)
        {
            notification.fireDate = notifyDate;
            notification.timeZone = [NSTimeZone defaultTimeZone];
            notification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
            notification.repeatInterval = 0;
            notification.alertBody = [StringTools createNSString:message];
            notification.userInfo = userInfo;
            notification.soundName = UILocalNotificationDefaultSoundName;
            
            [[UIApplication sharedApplication] scheduleLocalNotification:notification];
            
            NSLog(@"###### send local notification");
        }
    }
    
    void clearAllLocalNotifications()
    {
        NSLog(@"###### clear all local notifications");
        
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber: 0];
        [[UIApplication sharedApplication] cancelAllLocalNotifications];
    }

}

