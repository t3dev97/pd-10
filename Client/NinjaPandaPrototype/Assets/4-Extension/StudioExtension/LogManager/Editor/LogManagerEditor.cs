﻿using System.IO;
using UnityEditor;
using UnityEngine;
using System.Collections;

public class LogManagerEditor
{
    private const string LogManagerMenu = "Log Manager/Show Log";
    private const string PathLogDll = "/4-Extension/StudioExtension/LogManager/Editor/Temp";
    private const string StatusPath = "/4-Extension/StudioExtension/LogManager/Editor/Temp/Status";
    private const string PluginPath = "/Plugins/LogManager.dll";


    [MenuItem(LogManagerMenu)]
    static void ShowLog()
    {
        var isShowLog = !IsStatus;
        IsStatus = isShowLog;

        UnityEngine.Debug.Log("=======>>>>>>> SHOW DEBUG = " + isShowLog.ToString().ToUpper() + " <<<<<<<<=============");
        CopyFile(isShowLog);
    }

    [MenuItem(LogManagerMenu, true)]
    static bool ShowLogToggle()
    {
        UnityEditor.Menu.SetChecked(LogManagerMenu, IsStatus);
        return true;
    }

    public static void CopyFile(bool isDebug)
    {
        //copy dll
        var appPath = Application.dataPath;
        File.Copy(appPath + PathLogDll + "/" + (isDebug ? "Debug" : "NotDebug"), appPath + PluginPath, true);
        AssetDatabase.Refresh();
    }

    static bool IsStatus
    {
        get
        {
          /*  var o = false;
            if (bool.TryParse(File.ReadAllText(Application.dataPath + StatusPath), out o)) {
                return o;
            }
            return true;*/
            return EditorPrefs.GetBool("bts_log_manager", false);
        }

        set
        {
            EditorPrefs.SetBool("bts_log_manager", value);
            // File.WriteAllText(Application.dataPath + StatusPath, value.ToString());
        }
    }
}
