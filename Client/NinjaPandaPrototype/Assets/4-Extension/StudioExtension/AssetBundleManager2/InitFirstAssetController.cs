﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using System.Collections;

namespace Studio
{

    public class InitFirstAssetController : MonoBehaviour
    {
        private const string INIT_DATACONFIG = "init_data_config";
        private const string INIT_ASSET2D = "init_asset2d";
        private const string INIT_ASSETBUNDLE = "init_assetbundle";
        private const string INIT_CONFIG = "init_config.txt";

        private static InitFirstAssetController _api;

        public static InitFirstAssetController Api
        {
            get
            {
                if (_api == null)
                {
                    var go = new GameObject("InitFirstAssetManager");
                    _api = go.AddComponent<InitFirstAssetController>();
                }

                return _api;
            }
        }

        public Action<float> OnSpeedDownload;
        private bool _isFirstDownload = true;

        public string LocalUrlAsset2D
        {
            get { return BundleUtils.GetCachePath(); }
        }

        public string LocalUrlDataConfig
        {
            get { return BundleUtils.GetCachePath() + "/" + "DataConfig"; }
        }

        public string LocalUrlAssetBundle
        {
#if UNITY_EDITOR || UNITY_STANDALONE
            get { return BundleUtils.GetCachePath() + "/" + "AssetBundles/" + BaseLoader2.GetFirstAssetBundleFileName(); }
#else
        get { return BundleUtils.GetCachePath() + "/" + "AssetBundles"; }
#endif
        }

        public bool HasDownloadZipFileAsset2D
        {
            get
            {
                var fileExits = File.Exists(LocalUrlAsset2D + "/" + INIT_CONFIG);
                var prefExits = PlayerPrefs.GetInt(INIT_ASSET2D, 0) > 0;

                if (!fileExits && prefExits) PlayerPrefs.DeleteKey(INIT_ASSET2D);

                return fileExits && prefExits;
            }
        }

        public bool HasDownLoadZipDataConfig(int serverId)
        {
            var fileExits = File.Exists(LocalUrlDataConfig + "/" + serverId + INIT_CONFIG);
            var prefExits = PlayerPrefs.GetInt(serverId + INIT_DATACONFIG, 0) > 0;

            if (!fileExits && prefExits) PlayerPrefs.DeleteKey(serverId + INIT_DATACONFIG);

            return fileExits && prefExits;
        }

        public bool HasDownloadZipFileAssetBundle
        {
            get
            {
                var fileExits = File.Exists(LocalUrlAssetBundle + "/" + INIT_CONFIG);
                var prefExits = PlayerPrefs.GetInt(INIT_ASSETBUNDLE, 0) > 0;

                if (!fileExits && prefExits) PlayerPrefs.DeleteKey(INIT_ASSETBUNDLE);

                return fileExits && prefExits;
            }
        }

        public IEnumerator LoadZipFileAsset2D(string onlinetUrl, List<AssetInfo2> data, float totalSize,
            Action<string, float, float> onProgressAction, Action<string, float, float> onExtractProgressAction,
            Action<string, string> onErrorAction)
        {
            var isError = false;

            yield return
                StartCoroutine(LoadZipFile(onlinetUrl, LocalUrlAsset2D, INIT_CONFIG, data, totalSize, onProgressAction,
                    onExtractProgressAction,
                    (assetName, errorStr) =>
                    {
                        if (onErrorAction != null) onErrorAction(assetName, errorStr);
                        isError = true;
                    }));

            if (!isError)
            {
                PlayerPrefs.SetInt(INIT_ASSET2D, 1);
                PlayerPrefs.Save();
            }
        }

        public IEnumerator LoadZipFileAssetBundle(string onlinetUrl, List<AssetInfo2> data, float totalSize,
            Action<string, float, float> onProgressAction, Action<string, float, float> onExtractProgressAction,
            Action<string, string> onErrorAction)
        {
            var isError = false;
            var onErrorActionTemp = onErrorAction;

            yield return
                StartCoroutine(LoadZipFile(onlinetUrl, LocalUrlAssetBundle, INIT_CONFIG, data, totalSize,
                    onProgressAction, onExtractProgressAction,
                    (assetName, errorStr) =>
                    {
                        if (onErrorActionTemp != null) onErrorActionTemp(assetName, errorStr);
                        isError = true;
                    }));

            if (!isError)
            {
                PlayerPrefs.SetInt(INIT_ASSETBUNDLE, 1);
                PlayerPrefs.Save();
            }
        }

        public IEnumerator LoadZipDataConfig(string onlinetUrl, int serverId, List<AssetInfo2> data,
            Action<string, float, float> onProgressAction, Action<string, float, float> onExtractProgressAction,
            Action<string, string> onErrorAction)
        {
            var isError = false;

            yield return
                StartCoroutine(LoadZipFile(onlinetUrl, LocalUrlDataConfig, serverId + INIT_CONFIG, data, 1,
                    onProgressAction, onExtractProgressAction,
                    (assetName, errorStr) =>
                    {
                        if (onErrorAction != null) onErrorAction(assetName, errorStr);
                        isError = true;
                    }));

            if (!isError)
            {
                PlayerPrefs.SetInt(serverId + INIT_DATACONFIG, 1);
                PlayerPrefs.Save();
            }
        }

        public void Asset2DFinished()
        {

        }

        public void AssetBundleFinished()
        {

        }

        private IEnumerator LoadZipFile(string onlinetUrl, string localUrl, string fileName, List<AssetInfo2> data,
            float totalSize, Action<string, float, float> onProgressAction,
            Action<string, float, float> onExtractProgressAction, Action<string, string> onErrorAction)
        {
            float[] sizeLoaded = {0f};
            var fileDownloaded = GetFileDownload(localUrl);
            var downloadErrorList = new Dictionary<string, int>();
            var extractFileErrorList = new List<string>();

            if (onExtractProgressAction != null) onExtractProgressAction("", 0, 1);

            while (data.Count > 0)
            {
                var assetBundleInfo2 = data[0];
                data.RemoveAt(0);

                //wait for dispose
                if (downloadErrorList.ContainsKey(assetBundleInfo2.Name))
                {
                    onErrorAction(assetBundleInfo2.Name,
                        "Retry [" + downloadErrorList[assetBundleInfo2.Name] + "] download file <" +
                        assetBundleInfo2.Name + ">");
                    yield return new WaitForSeconds(1);
                }

                var isDownloaded = false;
                var isExtracted = false;

                //check file in downloadFile
                if (fileDownloaded.ContainsKey(assetBundleInfo2.Name))
                {
                    isDownloaded = fileDownloaded[assetBundleInfo2.Name].Downloaded;
                    isExtracted = fileDownloaded[assetBundleInfo2.Name].Extracted;
                }
                else
                {
                    fileDownloaded.Add(assetBundleInfo2.Name, new ZipFileStatusInfo()
                    {
                        Name = assetBundleInfo2.Name,
                        Size = assetBundleInfo2.Size,
                        ChildCount = assetBundleInfo2.ChildCount
                    });
                }

                var nameTemp = assetBundleInfo2.Name;
                var sizeTemp = assetBundleInfo2.Size;
                var countTemp = assetBundleInfo2.ChildCount;

                var isError = false;
                if (onExtractProgressAction != null)
                    onExtractProgressAction(assetBundleInfo2.Name, 0, assetBundleInfo2.ChildCount);

                if (isDownloaded)
                {
                    sizeLoaded[0] += assetBundleInfo2.Size;
                    onProgressAction(nameTemp, sizeLoaded[0], totalSize);
                }
                else
                {
                    yield return
                        StartCoroutine(LoadFile(onlinetUrl, localUrl, assetBundleInfo2.Name, assetBundleInfo2.Size,
                            (valueProgress, finished) =>
                            {
                                var sizeLoadedTemp = sizeLoaded[0] + (sizeTemp*valueProgress);
                                if (onProgressAction != null) onProgressAction(nameTemp, sizeLoadedTemp, totalSize);
                                if (finished) sizeLoaded[0] += sizeTemp;

                            },
                            (errorStr) =>
                            {
                                isError = true;
                                onErrorAction(nameTemp, errorStr);
                            }));

                    if (isError)
                    {
                        if (!downloadErrorList.ContainsKey(assetBundleInfo2.Name))
                        {
                            downloadErrorList.Add(assetBundleInfo2.Name, 0);
                        }

                        downloadErrorList[assetBundleInfo2.Name] += 1;
                        data.Add(assetBundleInfo2);
                    }
                }

                //update download done
                fileDownloaded[assetBundleInfo2.Name].Downloaded = !isError;

                if (!isError && !isExtracted)
                {
                    var pathFileZip = localUrl + "/" + assetBundleInfo2.Name;
                    var pathExtract = localUrl;
                    var isExtractError = false;

                    do
                    {
                        isExtractError = false;
                        yield return StartCoroutine(DecompresZip.Instance.Decompression(pathFileZip, pathExtract,
                            (valueProgress) =>
                            {
                                if (onExtractProgressAction != null)
                                    onExtractProgressAction(nameTemp, valueProgress, countTemp);
                            },
                            (errorFile) =>
                            {
                                if (!extractFileErrorList.Contains(nameTemp) && onErrorAction != null)
                                {
                                    extractFileErrorList.Add(nameTemp);
                                    onErrorAction(nameTemp, errorFile);
                                }
                                isExtractError = true;
                            }));

                        if (!isExtractError)
                        {
                            extractFileErrorList.Remove(assetBundleInfo2.Name);
                        }

                    } while (isExtractError);
                }

                //update extract done
                fileDownloaded[assetBundleInfo2.Name].Extracted = !isError;

                //write file config
                UpdateFileDownload(localUrl, fileName, fileDownloaded.Values.ToList());
            }

            #region Temp

            /*foreach (var assetBundleInfo2 in data)
        {
            var isDownloaded = false;
            var isExtracted = false;

            //check file in downloadFile
            if (fileDownloaded.ContainsKey(assetBundleInfo2.Name))
            {
                isDownloaded = fileDownloaded[assetBundleInfo2.Name].Downloaded;
                isExtracted = fileDownloaded[assetBundleInfo2.Name].Extracted;
            }
            else
            {
                fileDownloaded.Add(assetBundleInfo2.Name, new ZipFileStatusInfo()
                {
                    Name = assetBundleInfo2.Name,
                    Size = assetBundleInfo2.Size,
                    ChildCount = assetBundleInfo2.ChildCount
                });
            }

            var nameTemp = assetBundleInfo2.Name;
            var sizeTemp = assetBundleInfo2.Size;
            var countTemp = assetBundleInfo2.ChildCount;

            var isError = false;
            if (onExtractProgressAction != null) onExtractProgressAction(assetBundleInfo2.Name, 0, assetBundleInfo2.ChildCount);

            if (isDownloaded)
            {
                sizeLoaded[0] += assetBundleInfo2.Size;
                onProgressAction(nameTemp, sizeLoaded[0], totalSize);
            }
            else
            {
                do
                {
                    isError = false;
                    yield return StartCoroutine(LoadFile(onlinetUrl, localUrl, assetBundleInfo2.Name, assetBundleInfo2.Size,
                        (valueProgress, finished) =>
                        {
                            var sizeLoadedTemp = sizeLoaded[0] + (sizeTemp * valueProgress);
                            if (onProgressAction != null) onProgressAction(nameTemp, sizeLoadedTemp, totalSize);
                            if (finished) sizeLoaded[0] += sizeTemp;

                        },
                        (errorStr) =>
                        {
                            isError = true;
                            if (!downloadErrorList.Contains(nameTemp) && onErrorAction != null)
                            {
                                downloadErrorList.Add(nameTemp);
                                onErrorAction(nameTemp, errorStr);
                            }
                        }));

                    if (!isError)
                    {
                        downloadErrorList.Remove(assetBundleInfo2.Name);
                    }

                } while (isError);
            }

            //update download done
            fileDownloaded[assetBundleInfo2.Name].Downloaded = !isError;

            if (!isError && !isExtracted)
            {
                var pathFileZip = localUrl + "/" + assetBundleInfo2.Name;
                var pathExtract = localUrl;
                var isExtractError = false;

                do
                {
                    isExtractError = false;
                    yield return StartCoroutine(DecompresZip.Instance.Decompression(pathFileZip, pathExtract,
                        (valueProgress) =>
                        {
                            if (onExtractProgressAction != null) onExtractProgressAction(nameTemp, valueProgress, countTemp);
                        },
                        (errorFile) =>
                        {
                            if (!extractFileErrorList.Contains(nameTemp) && onErrorAction != null)
                            {
                                extractFileErrorList.Add(nameTemp);
                                onErrorAction(nameTemp, errorFile);
                            }
                            isExtractError = true;
                        }));

                    if (!isExtractError)
                    {
                        extractFileErrorList.Remove(assetBundleInfo2.Name);
                    }

                } while (isExtractError);
            }

            //update extract done
            fileDownloaded[assetBundleInfo2.Name].Extracted = !isError;

            //write file config
            UpdateFileDownload(localUrl, fileName, fileDownloaded.Values.ToList());
        }*/

            #endregion
        }

        public IEnumerator LoadFile(string onlineUrl, string localUrl, string assetName, float size,
            Action<float, bool> onProgressAction, Action<string> onErrorAction)
        {
            var www = new WWW(onlineUrl + "/" + assetName + "?" + Utils.GenerateFakeVersionForWeb());

            DebugX.Log("download file url:: " + www.url);

            var firstSize = 0f;
            var firstTime = Time.realtimeSinceStartup;
            var progress = 0f;

            while (!www.isDone)
            {
                if (www.error != null)
                {
                    DebugX.LogError("download error:: " + www.url);
                    if (onErrorAction != null) onErrorAction(www.error);

                    www.Dispose();
                    yield break;
                }

                progress = www.progress;

                if (onProgressAction != null) onProgressAction(progress, false);

                //cal speed download
                var endTime = Time.realtimeSinceStartup;
                if (OnSpeedDownload != null && (_isFirstDownload || (endTime - firstTime) >= 1f))
                {
                    var byteDownloaded = progress*size;

                    OnSpeedDownload(((byteDownloaded - firstSize)/1024f)/(endTime - firstTime));

                    firstSize = byteDownloaded;
                    firstTime = endTime;

                    _isFirstDownload = false;
                }

                yield return 0;
            }

            yield return new WaitForEndOfFrame();

            if (www.error != null)
            {
                DebugX.LogError("download error:: " + www.url);
                if (onErrorAction != null) onErrorAction(www.error);

                www.Dispose();
                yield break;
            }

            progress = www.progress;

            if (progress <= 0)
            {
                if (onErrorAction != null) onErrorAction("File size is not enough. Try download again !");
                www.Dispose();
                yield break;
            }

            www.bytes.WriteFile(localUrl + "/" + assetName, true);
            www.Dispose();

            if (onProgressAction != null) onProgressAction(1, true);
        }

        private void UpdateFileDownload(string url, string fileName, List<ZipFileStatusInfo> data)
        {
            var dataStr = "";
            var count = 0;
            foreach (var zipFileStatusInfo in data)
            {
                if (count > 0) dataStr += "\n";
                dataStr += zipFileStatusInfo.ToData();
                count += 1;
            }

            dataStr.WriteFile(url + "/" + fileName, true);
        }

        private Dictionary<string, ZipFileStatusInfo> GetFileDownload(string url)
        {
            var dataStr = "";
            var fullUrl = url + "/" + INIT_CONFIG;
            if (File.Exists(fullUrl))
            {
                dataStr = File.ReadAllText(fullUrl);
            }

            var list = new Dictionary<string, ZipFileStatusInfo>();
            if (!string.IsNullOrEmpty(dataStr))
            {
                foreach (var data in dataStr.Split('\n'))
                {
                    var zipFileInfo = new ZipFileStatusInfo().Parse(data);
                    list.Add(zipFileInfo.Name, zipFileInfo);
                }
            }

            return list;
        }
    }

    public class AssetInfo2
    {
        public string Name;
        public float Size;
        public int ChildCount;
    }

    public class ZipFileStatusInfo
    {
        public string Name;
        public bool Downloaded;
        public bool Extracted;

        public float Size;
        public int ChildCount;

        public string ToData()
        {
            return Name + ":" + (Downloaded ? 1 : 0) + ":" + (Extracted ? 1 : 0) + ":" + Size + ":" + ChildCount;
        }

        public ZipFileStatusInfo Parse(string data)
        {
            var split = data.Split(':');
            Name = split[0];
            Downloaded = split[1].ToInt().ToBool();
            Extracted = split[2].ToInt().ToBool();
            Size = split[3].ToFloat();
            ChildCount = split[4].ToInt();

            return this;
        }
    }

    public class FirstAssetBundleKey
    {
        public const string LOAD_ASSER2D_SUCCESS = "first_load_dynamic";
        public const string LOAD_ASSETBUNDLE_SUCCESS = "load_assetbundle_success";

        public const string VERSION_ASSET2D = "version_asset2d";
        public const string VERSION_ASSETBUNDLE = "version_assetbundle";
    }
}

