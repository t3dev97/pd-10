﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using Object = UnityEngine.Object;

namespace Studio
{
    public abstract class AssetBundleLoadOperation2 : IEnumerator
    {
        protected string m_AssetBundleName;

        public string AssetBundleName
        {
            get { return m_AssetBundleName; }
        }

        public object Current
        {
            get
            {
                return null;
            }
        }

        public bool MoveNext()
        {
            return !IsDone();
        }

        public void Reset()
        {
        }

        abstract public bool Update();

        abstract public bool IsDone();
    }

    public class AssetBundleLoadLevelSimulationOperation2 : AssetBundleLoadOperation2
    {
        public AssetBundleLoadLevelSimulationOperation2()
        {
        }

        public override bool Update()
        {
            return false;
        }

        public override bool IsDone()
        {
            return true;
        }
    }

    public class AssetBundleLoadAsyncLevelOperation2 : AssetBundleLoadOperation2
    {
        //protected string 				m_AssetBundleName;
        protected string m_LevelName;
        protected bool m_IsAdditive;
        protected string m_DownloadingError;
        protected AsyncOperation m_Request;

        public Action<float> OnProgressAction = delegate { };

        public AssetBundleLoadAsyncLevelOperation2(string assetbundleName, string levelName, bool isAdditive)
        {
            m_AssetBundleName = assetbundleName;
            m_LevelName = levelName;
            m_IsAdditive = isAdditive;
        }

        public override bool Update()
        {
            if (m_Request != null)
                return false;

            LoadedAssetBundle2 bundle = AssetBundleManager2.GetLoadedAssetBundle(m_AssetBundleName, out m_DownloadingError);

            if (bundle != null)
            {
                m_Request = SceneManager.LoadSceneAsync(m_LevelName, m_IsAdditive ? LoadSceneMode.Additive : LoadSceneMode.Single);
                return false;
            }
            else
                return true;
        }

        public override bool IsDone()
        {
            //DebugX.LogError("IsDone");

            // Return if meeting downloading error.
            // m_DownloadingError might come from the dependency downloading.
            if (m_Request == null && m_DownloadingError != null)
            {
                DebugX.LogError(m_DownloadingError);
                return true;
            }

            //if (m_Request != null && OnProgressAction != null) {
            //    OnProgressAction(m_Request.progress);
            //}

            var isDone = m_Request != null && m_Request.isDone;
            //if (isDone) OnProgressAction = null;

            return isDone;
        }

        public bool NotifyProgress()
        {
            //if (m_Request != null) DebugX.LogError("progress:: " + m_Request.progress);
            if (m_Request != null && OnProgressAction != null)
            {
                OnProgressAction(m_Request.progress);
            }

            var isDone = m_Request != null && m_Request.isDone;
            if (isDone)
            {
                OnProgressAction = null;
                return false;
            }

            return true;
        }
    }

    public abstract class AssetBundleLoadAssetOperation2 : AssetBundleLoadOperation2
    {
        public abstract T GetAsset<T>() where T : UnityEngine.Object;
    }

    public class AssetBundleLoadAssetOperationSimulation2 : AssetBundleLoadAssetOperation2
    {
        Object m_SimulatedObject;

        public AssetBundleLoadAssetOperationSimulation2(Object simulatedObject)
        {
            m_SimulatedObject = simulatedObject;
        }

        public override T GetAsset<T>()
        {
            return m_SimulatedObject as T;
        }

        public override bool Update()
        {
            return false;
        }

        public override bool IsDone()
        {
            return true;
        }
    }

    public class AssetBundleLoadAssetOperationFull2 : AssetBundleLoadAssetOperation2
    {
        protected string m_AssetName;
        protected string m_DownloadingError;
        protected System.Type m_Type;
        protected Object m_Asset;
        protected bool isDone;

        public AssetBundleLoadAssetOperationFull2(string bundleName, string assetName, System.Type type)
        {
            m_AssetBundleName = bundleName;
            m_AssetName = assetName;
            m_Type = type;
            isDone = false;
        }

        public override T GetAsset<T>()
        {
            if (isDone) return m_Asset as T;
            return null;
        }

        // Returns true if more Update calls are required.
        public override bool Update()
        {
            if (isDone) return false;

            var bundle = AssetBundleManager2.GetLoadedAssetBundle(m_AssetBundleName, out m_DownloadingError);
            if (!string.IsNullOrEmpty(m_DownloadingError)) return false;
            if (bundle != null)
            {
                m_Asset = bundle.m_AssetBundle.LoadAsset(m_AssetName, m_Type);
                isDone = true;

                return false;
            }
            else
            {
                return true;
            }
        }

        public override bool IsDone()
        {
            // Return if meeting downloading error.
            // m_DownloadingError might come from the dependency downloading.
            if (m_DownloadingError != null)
            {
                DebugX.LogError(m_DownloadingError);
                return true;
            }

            return isDone;
        }
    }

    public class AssetBundleLoadAssetAsyncOperationFull2 : AssetBundleLoadAssetOperation2
    {
        //protected string 				m_AssetBundleName;
        protected string m_AssetName;
        protected string m_DownloadingError;
        protected System.Type m_Type;
        protected AssetBundleRequest m_Request = null;
        protected bool isError = false;

        public AssetBundleLoadAssetAsyncOperationFull2(string bundleName, string assetName, System.Type type)
        {
            m_AssetBundleName = bundleName;
            m_AssetName = assetName;
            m_Type = type;
        }

        public override T GetAsset<T>()
        {
            if (m_Request != null && m_Request.isDone && m_Request.asset != null)
                return m_Request.asset as T;
            else
                return null;
        }

        // Returns true if more Update calls are required.
        public override bool Update()
        {
            if (this == null) return false;
            if (m_Request != null)
                return false;

            var bundle = AssetBundleManager2.GetLoadedAssetBundle(m_AssetBundleName, out m_DownloadingError);
            if (!string.IsNullOrEmpty(m_DownloadingError)) return false;
            if (bundle != null)
            {
                if (bundle.m_AssetBundle == null)
                {
                    isError = true;
                    return false;
                }

                m_Request = bundle.m_AssetBundle.LoadAssetAsync(m_AssetName, m_Type);
                return false;
            }
            else
            {
                return true;
            }
        }

        public override bool IsDone()
        {
            // Return if meeting downloading error.
            // m_DownloadingError might come from the dependency downloading.
            if (m_Request == null && m_DownloadingError != null)
            {
                DebugX.LogError(m_DownloadingError);
                return true;
            }

            if (isError)
            {
                DebugX.LogError(this + ": can't find asset " + m_AssetName + " of assetbundle: " + m_AssetBundleName);
                return true;
            }

            return (m_Request != null && m_Request.isDone);
        }
    }

    public class AssetBundleLoadMultiAssetAsyncOperationFull2 : AssetBundleLoadAssetOperation2
    {
        //protected string 				m_AssetBundleName;
        protected string m_AssetName;
        protected string m_DownloadingError;
        protected System.Type m_Type;
        protected AssetBundleRequest m_Request = null;
        protected bool isError = false;

        public AssetBundleLoadMultiAssetAsyncOperationFull2(string bundleName, string assetName, System.Type type)
        {
            m_AssetBundleName = bundleName;
            m_AssetName = assetName;
            m_Type = type;
        }

        public override T GetAsset<T>()
        {
            /* if (m_Request != null && m_Request.isDone && m_Request.asset != null)
                 return m_Request.asset as T;
             else*/
            return null;
        }

        public T[] GetAllAsset<T>() where T : UnityEngine.Object
        {
            return m_Request.allAssets as T[];
        }

        // Returns true if more Update calls are required.
        public override bool Update()
        {
            if (this == null) return false;
            if (m_Request != null)
                return false;

            var bundle = AssetBundleManager2.GetLoadedAssetBundle(m_AssetBundleName, out m_DownloadingError);
            if (!string.IsNullOrEmpty(m_DownloadingError)) return false;
            if (bundle != null)
            {
                if (bundle.m_AssetBundle == null)
                {
                    isError = true;
                    return false;
                }

                m_Request = bundle.m_AssetBundle.LoadAssetWithSubAssetsAsync(m_AssetName, m_Type);
                return false;
            }
            else
            {
                return true;
            }
        }

        public override bool IsDone()
        {
            // Return if meeting downloading error.
            // m_DownloadingError might come from the dependency downloading.
            if (m_Request == null && m_DownloadingError != null)
            {
                DebugX.LogError(m_DownloadingError);
                return true;
            }

            if (isError)
            {
                DebugX.LogError(this + ": can't find asset " + m_AssetName + " of assetbundle: " + m_AssetBundleName);
                return true;
            }

            return (m_Request != null && m_Request.isDone);
        }
    }

    public class AssetBundleLoadOperationFull2 : AssetBundleLoadOperation2
    {
        protected string m_DownloadingError;
        protected bool m_IsDone;

        public AssetBundleLoadOperationFull2(string bundleName)
        {
            m_AssetBundleName = bundleName;
        }

        // Returns true if more Update calls are required.
        public override bool Update()
        {
            var bundle = AssetBundleManager2.GetLoadedAssetBundle(m_AssetBundleName, out m_DownloadingError);
            if (!string.IsNullOrEmpty(m_DownloadingError)) return false;
            if (bundle != null)
            {
                m_IsDone = true;
                return false;
            }
            else
            {
                return true;
            }
        }

        public override bool IsDone()
        {
            if (!string.IsNullOrEmpty(m_DownloadingError))
            {
                DebugX.LogError(m_DownloadingError);
                return true;
            }

            return m_IsDone;
        }
    }

    public class AssetBundleLoadManifestOperation2 : AssetBundleLoadAssetAsyncOperationFull2
    {
        public AssetBundleLoadManifestOperation2(string bundleName, string assetName, System.Type type)
            : base(bundleName, assetName, type)
        {
        }

        public override bool Update()
        {
            base.Update();

            AssetBundleManager2.AssetBundleManifestObject = GetAsset<AssetBundleManifest>();
            if (AssetBundleManager2.AssetBundleManifestObject != null)
            {
                return false;
            }
            else
                return true;
        }
    }

}


