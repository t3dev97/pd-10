﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;

namespace Studio
{
    public static class BundleUtils
    {
        public static string GetCachePath()
        {
#if UNITY_EDITOR || UNITY_STANDALONE
            var path = Application.temporaryCachePath;
            var utf8Bytes = Encoding.UTF8.GetBytes(path);
            var unicodeBytes = Encoding.Convert(Encoding.UTF8, Encoding.ASCII, utf8Bytes);
            return Encoding.ASCII.GetString(unicodeBytes).ReplaceSpace().Replace('?', '_');

#elif UNITY_IOS
        return Application.temporaryCachePath;

#elif (UNITY_WSA_8_1 || UNITY_WP_8_1 || UNITY_WINRT_8_1 || UNITY_WSA)
		return UnityEngine.Windows.Directory.localFolder;

#else
        return Application.persistentDataPath;

#endif
        }

        public static string ReplaceSpace(this string text)
        {
            return text.Replace(" ", "");
        }

        public static void CreateDirectory(string directory)
        {
#if (UNITY_WP8 || UNITY_WSA || UNITY_WINRT_8_1) && !UNITY_EDITOR
        var path = directory;
        var listPathCreate = new List<string>();
        while (!Directory.Exists(path))
        {
            listPathCreate.Insert(0, path);
            path = path.GetDirectory();
        }

        foreach (var p in listPathCreate) {
            Directory.CreateDirectory(p);
        }
#else
            if (!Directory.Exists(directory)) Directory.CreateDirectory(directory);
#endif
        }

        public static string GetDirectory(this string path)
        {
            var s = new StringBuilder();
            var canInsert = false;

            for (var i = path.Length - 1; i >= 0; i--)
            {

                if (!canInsert && (path[i] == '/' || path[i] == '\\'))
                {
                    canInsert = true;
                    continue;
                }

                if (canInsert) s.Insert(0, path[i]);
            }

            return s.ToString();
        }

        public static string ConvertToPath(this string path)
        {
#if UNITY_WSA_8_1 || UNITY_WP_8_1 || UNITY_WINRT_8_1 || UNITY_WSA
            return path.Replace('/', '\\');
#endif
            return path;
        }

        public static string EncryptMd5(this string strToEncrypt)
        {
            var ue = new UTF8Encoding();
            var bytes = ue.GetBytes(strToEncrypt);

            // encrypt bytes
            var md5 = new MD5CryptoServiceProvider();
            var hashBytes = md5.ComputeHash(bytes);

            // Convert the encrypted bytes back to a string (base 16)
            var hashString = "";
            for (var i = 0; i < hashBytes.Length; i++)
            {
                hashString += Convert.ToString(hashBytes[i], 16).PadLeft(2, '0');
            }

            return hashString.PadLeft(32, '0');
        }

        public static byte[] EncryptFile(this byte[] data)
        {
            if (data == null || data.Length == 0)
            {
                DebugX.LogWarning("GameResource ErrorString - Decrypt data should not be null");
                return null;
            }

            const int passConst = 2906;
            if (data.Length > 4)
            {
                var byteTemp = new byte[4];
                Array.Copy(data, 0, byteTemp, 0, 4);
                var pass = BitConverter.ToInt32(byteTemp, 0);
                if (pass == passConst)
                {
                    return data;
                }
            }

            var encryptTotal = data.Length;
            var newData = new byte[encryptTotal + 4];

            //add pass
            var ar = BitConverter.GetBytes(passConst);
            for (var i = 0; i < ar.Length; i++)
            {
                newData[i] = ar[i];
            }

            for (var i = 0; i < encryptTotal; i++)
            {
                newData[i + 4] = (byte)~data[i];
            }

            return newData;
        }

        public static byte[] DecyptFile(this byte[] data)
        {
            if (data == null || data.Length == 0)
            {
                DebugX.LogWarning("GameResource ErrorString - Decrypt data should not be null");
                return null;
            }

            const int passConst = 2906;
            if (data.Length > 4)
            {
                var byteTemp = new byte[4];
                Array.Copy(data, 0, byteTemp, 0, 4);
                var pass = BitConverter.ToInt32(byteTemp, 0);
                if (pass != passConst)
                {
                    return data;
                }
            }

            var encryptTotal = data.Length;
            var newData = new byte[encryptTotal - 4];

            for (var i = 4; i < encryptTotal; i++)
            {
                newData[i - 4] = (byte)~data[i];
            }

            return newData;
        }

        public static void WriteFile(this string content, string fileUri, bool overwrite)
        {
//#if !UNITY_WEBGL
            var url = fileUri.ConvertToPath();

            var folderPath = url.GetDirectory().ConvertToPath();
            if (!Directory.Exists(folderPath)) CreateDirectory(folderPath);

            try
            {
#if UNITY_WP8 || UNITY_WSA || UNITY_WINRT_8_1
        File.WriteAllBytes(url, Encoding.UTF8.GetBytes(content));
#else
                File.WriteAllText(url, content);
#if UNITY_IOS
        //prevent this file to be backup to iCloud (thus, resulted in a rejection)
		UnityEngine.iOS.Device.SetNoBackupFlag(url);
#endif
#endif
            }
            catch (Exception ex)
            {
                DebugX.LogError(ex.Message);
            }
//#endif
        }

        public static void WriteFile(this byte[] content, string fileURI, bool overwrite)
        {
//#if !UNITY_WEBGL
            var url = fileURI.ConvertToPath();
            var folderPath = url.GetDirectory().ConvertToPath();
            if (!Directory.Exists(folderPath)) CreateDirectory(folderPath);

#if UNITY_WP8 || UNITY_WSA || UNITY_WINRT_8_1
        File.WriteAllBytes(url, content);
#else
            try
            {
                var write = new FileStream(url, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                write.Write(content, 0, content.Length);
                write.Close();

#if UNITY_IOS
        //prevent this file to be backup to iCloud (thus, resulted in a rejection)
        UnityEngine.iOS.Device.SetNoBackupFlag(url);
#endif
            }
            catch (Exception ex)
            {
                DebugX.LogError(ex.Message);
            }
#endif
//#endif
        }
    }
}
