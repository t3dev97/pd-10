﻿using System.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Studio
{
    public class BaseLoader2 : MonoBehaviour
    {
        private const string KAssetBundlesPath = "/AssetBundles/";
        private const string KAssetBundlePathEncrypt = "/AssetBundleEncrypt/";
        private const int MaxRetry = 3;

        public static bool IsInternalResource;
        public static bool EncryptFileName;

        public static bool HasStreamingAsset = false;
        public static bool IsCheckUpdate = true;

        private static BaseLoader2 _api;

        public static BaseLoader2 Api
        {
            get
            {
                if (_api == null)
                {
                    var go = new GameObject("BaseLoader2", typeof(BaseLoader2));
                    _api = go.GetComponent<BaseLoader2>();
                    DontDestroyOnLoad(go);
                }

                return _api;
            }
        }

        #region Cache

        public bool IsCacheAsset;
        private readonly List<string> _assetBundleNameCache = new List<string>();

        private void AddAssetBundleCache(string assetBundleName)
        {
            if (!IsCacheAsset) return;
            _assetBundleNameCache.Add(assetBundleName);
        }

        public void ClearAssetBundleCache(bool turnOffCache = true)
        {
            foreach (var assetBundleName in _assetBundleNameCache)
                UnloadAssetBundle(assetBundleName);

            _assetBundleNameCache.Clear();

            if (turnOffCache) IsCacheAsset = false;
        }

        #endregion

        // Initialize the downloading url and AssetBundleManifest object.
        public IEnumerator Initialize(string url, string assetBundleVariant, Action<bool> onCompleteAction, Action<string, string> onErrorAction)
        {
            var platformFolderForAssetBundles = GetPlatformFolderForAssetBundles();
            if (IsInternalResource)
            {
                var kAssetPath = EncryptFileName ? KAssetBundlePathEncrypt : KAssetBundlesPath;
                AssetBundleManager2.BaseDownloadingURL = GetRelativePath() + kAssetPath + platformFolderForAssetBundles + "/";
            }
            else
                AssetBundleManager2.BaseDownloadingURL = url + "/" + platformFolderForAssetBundles + "/";

            AssetBundleManager2.HasStreamingAsset = HasStreamingAsset;

            DebugX.Log("===> path online: " + AssetBundleManager2.BaseDownloadingURL);
            DebugX.Log("===> path local: " + AssetBundleManager2.LocalURL);
            DebugX.Log("===> path StreamingAssets:: " + AssetBundleManager2.StreamingAssetsUrl);

            var hasChange = false;

#if !UNITY_WEBGL

            var urlManifest = AssetBundleManager2.LocalURL + platformFolderForAssetBundles;
            if (HasStreamingAsset)
            {
                AssetBundleManager2.ResourcesHasUpdate = GetLocalUpdateList();

                if (!File.Exists(urlManifest))
                {
                    AssetBundleManager2.RemoveResourcesHasUpdate(platformFolderForAssetBundles);
                }
            }
            else
            {
                if (!File.Exists(urlManifest))
                {
                    hasChange = true;
                    yield return StartCoroutine(LoadAssetBundleManifest(
                       AssetBundleManager2.BaseDownloadingURL,
                       GetPlatformFolderForAssetBundles(),
                       (assetBundle, errorStr) =>
                       {
                           if (onErrorAction != null) onErrorAction(assetBundle, errorStr);
                       }, null));
                }
            }
#endif
            AssetBundleManager2.Variants = new string[] { assetBundleVariant };

            var request = AssetBundleManager2.Initialize(platformFolderForAssetBundles);
            if (request == null) yield break;
            yield return StartCoroutine(request);

            UnloadAssetBundle(platformFolderForAssetBundles);

            if (onCompleteAction != null) onCompleteAction(hasChange);
        }

        public IEnumerator UpdateAssetBundle(bool hasChange, Action<string, float, float> onProgressAction, Action<string, string> onErrorAction)
        {
            if (!IsCheckUpdate)
            {
                yield break;
            }

            var assetUpdateList = new List<string>();

            AssetBundleManifest assetBundleManifest = null;
            if (!HasStreamingAsset && hasChange)
            {
                foreach (var assetBundleInfo2 in AssetBundleManager2.AssetBundleManifestObject.GetAllAssetBundles())
                {
                    if (!File.Exists(AssetBundleManager2.LocalURL + "/" + assetBundleInfo2))
                    {
                        assetUpdateList.Add(assetBundleInfo2);
                    }
                }
            }
            else
            {
                yield return StartCoroutine(LoadAssetBundleManifest(
                    AssetBundleManager2.BaseDownloadingURL,
                    GetPlatformFolderForAssetBundles(),
                    (assetBundle, errorStr) =>
                    {
                        if (onErrorAction != null) onErrorAction(assetBundle, errorStr);
                    },
                    (assetBundleManifestReturn) =>
                    {
                        assetBundleManifest = assetBundleManifestReturn;
                    }));

                if (assetBundleManifest == null)
                {
                    if (onProgressAction != null) onProgressAction("", 1, 1);
                    yield break;
                }
                var oldAssetBundleList = new List<string>();

                if (AssetBundleManager2.AssetBundleManifestObject != null)
                {
                    var oldAssetBundleItems = AssetBundleManager2.AssetBundleManifestObject.GetAllAssetBundles();
                    if (oldAssetBundleItems != null)
                        oldAssetBundleList = oldAssetBundleItems.ToList();
                }
                //check new version
                foreach (var assetBundleInfo2 in assetBundleManifest.GetAllAssetBundles())
                {
                    var newVersion = assetBundleManifest.GetAssetBundleHash(assetBundleInfo2);
                    var isNew = !oldAssetBundleList.Contains(assetBundleInfo2);

                    if (isNew)
                    {
                        assetUpdateList.Add(assetBundleInfo2);

                        AssetBundleManager2.AddResourcesHasUpdate(assetBundleInfo2);
                        continue;
                    }

                    var oldVersion = AssetBundleManager2.AssetBundleManifestObject.GetAssetBundleHash(assetBundleInfo2);
                    if (newVersion != oldVersion)
                    {
                        assetUpdateList.Add(assetBundleInfo2);

                        AssetBundleManager2.AddResourcesHasUpdate(assetBundleInfo2);
                    }
                    else
                    {
                        if ((AssetBundleManager2.ResourcesHasUpdate.Contains(assetBundleInfo2) || !HasStreamingAsset) &&
                            !File.Exists(AssetBundleManager2.LocalURL + "/" + assetBundleInfo2))
                        {
                            assetUpdateList.Add(assetBundleInfo2);
                        }
                    }

                    oldAssetBundleList.Remove(assetBundleInfo2);
                }

                foreach (var oldAssetBundle in oldAssetBundleList)
                {
                    var path = AssetBundleManager2.LocalURL + "/" + oldAssetBundle;
                    try
                    {
                        AssetBundleManager2.RemoveResourcesHasUpdate(oldAssetBundle);

                        if (File.Exists(path))
                        {
                            File.Delete(path);
                            DebugX.Log("delete assetbundle:: " + path);
                        }
                    }
                    catch (Exception ex)
                    {
                        DebugX.LogError("UpdateAssetBundle:: " + ex.Message);
                    }
                }

                SaveLocalUpdateList(AssetBundleManager2.ResourcesHasUpdate);
            }

            var totalSize = assetUpdateList.Count;
            if (totalSize == 0) yield break;

            // if (onProgressAction != null) onProgressAction("", 0, totalSize);

            var sizeLoaded = 0f;
            var onlineUrl = AssetBundleManager2.BaseDownloadingURL;
            var localUrl = AssetBundleManager2.LocalURL;
            var downloadErrorList = new Dictionary<string, int>();

            //download new version
            while (assetUpdateList.Count > 0)
            {
                var assetBundleInfo2 = assetUpdateList[0];
                assetUpdateList.RemoveAt(0);

                //wait for dispose
                if (downloadErrorList.ContainsKey(assetBundleInfo2))
                {
                    onErrorAction(assetBundleInfo2, "Retry [" + downloadErrorList[assetBundleInfo2] + "] download file <" + assetBundleInfo2 + ">");
                    yield return new WaitForSeconds(1);
                }

                var nameTemp = assetBundleInfo2;
                var isError = false;

                yield return StartCoroutine(LoadFile(onlineUrl, localUrl, assetBundleInfo2,
                    (progressValue, finished) =>
                    {
                        var sizeLoadedTemp = sizeLoaded + (progressValue);
                        if (onProgressAction != null) onProgressAction(nameTemp, sizeLoadedTemp, totalSize);
                        if (finished) sizeLoaded += 1;
                    },
                    (errorStr) =>
                    {
                        isError = true;
                        onErrorAction(nameTemp, errorStr);
                    }));

                if (isError)
                {
                    if (!downloadErrorList.ContainsKey(assetBundleInfo2))
                    {
                        downloadErrorList.Add(assetBundleInfo2, 0);
                    }

                    downloadErrorList[assetBundleInfo2] += 1;

                    if (downloadErrorList[assetBundleInfo2] <= MaxRetry)
                    {
                        assetUpdateList.Add(assetBundleInfo2);
                    }
                }
            }

            //set current is new AssetBundleManifest
            if (!hasChange) AssetBundleManager2.AssetBundleManifestObject = assetBundleManifest;
        }

        private IEnumerator LoadAssetBundleManifest(string url, string assetName, Action<string, string> onErrorAction, Action<AssetBundleManifest> onCompleteActon)
        {
            //wait for download manifest complete

            var retry = 1;
            while (true)
            {
                using (var www = new WWW(url + assetName + "?" + Utils.GenerateFakeVersionForWeb()))
                {
                    DebugX.Log("download manifest:: " + www.url);

                    yield return www;
                    yield return new WaitForEndOfFrame();

                    if (www.error != null)
                    {
                        DebugX.LogError("LoadAssetBundleManifest: " + www.error);
                        www.Dispose();
                        yield return new WaitForSeconds(1);

                        DebugX.LogError("Retry [" + (retry++) + "] LoadAssetBundleManifest");
                        if (onErrorAction != null)
                            onErrorAction(assetName, "Retry [" + (retry++) + "] Load config dữ liệu từ máy chủ.");
                        continue;
                    }

                    if (www.isDone)
                    {
                        www.bytes.WriteFile(AssetBundleManager2.LocalURL + assetName, true);

                        if (onCompleteActon != null)
                        {
                            var bundle = AssetBundle.LoadFromMemory(www.bytes.DecyptFile());
                            onCompleteActon(bundle.LoadAsset<AssetBundleManifest>("AssetBundleManifest"));
                            bundle.Unload(false);
                        }

                        www.Dispose();
                        yield break;
                    }
                }
            }
        }

        private IEnumerator LoadFile(string onlineUrl, string localUrl, string assetName, Action<float, bool> onProgressAction, Action<string> onErrorAction)
        {
            var www = new WWW(onlineUrl + assetName + "?" + Utils.GenerateFakeVersionForWeb());

            DebugX.Log("download file url:: " + www.url);

            var progress = 0f;
            while (!www.isDone)
            {
                if (www.error != null)
                {
                    var errorStr = www.error;
                    DebugX.LogError("download error:: " + errorStr);
                    www.Dispose();
                    if (onErrorAction != null) onErrorAction(errorStr);
                    yield break;
                }

                progress = www.progress;
                if (onProgressAction != null) onProgressAction(progress, false);

                yield return null;
            }

            yield return new WaitForEndOfFrame();

            //check again file error
            if (www.error != null)
            {
                var errorStr = www.error;
                DebugX.LogError("download error:: " + errorStr);
                www.Dispose();

                if (onErrorAction != null) onErrorAction(errorStr);
                yield break;
            }

            progress = www.progress;

            if (progress <= 0)
            {
                www.Dispose();
                if (onErrorAction != null) onErrorAction("File size is not enought. Please download again !");
                yield break;
            }

            www.bytes.WriteFile(localUrl + assetName, true);
            www.Dispose();

            if (onProgressAction != null) onProgressAction(1, true);
        }

        public static string GetPlatformFolderForAssetBundles()
        {
            var platformFolderForAssetBundles =

#if UNITY_EDITOR
                GetPlatformFolderForAssetBundles(EditorUserBuildSettings.activeBuildTarget);
#else
		    GetPlatformFolderForAssetBundles(Application.platform);
#endif
            return platformFolderForAssetBundles;
        }

        private static List<string> GetLocalUpdateList()
        {
            var url = AssetBundleManager2.LocalURL + "Updater.txt";
            var lst = new List<string>();
            if (File.Exists(url))
            {
                lst = File.ReadAllText(url).Split('\n').ToList();
            }

            return lst;
        }

        private static void SaveLocalUpdateList(List<string> lst)
        {
            var url = AssetBundleManager2.LocalURL + "Updater.txt";
            var str = "";
            for (var i = 0; i < lst.Count; i++)
            {
                if (i > 0) str += "\n";
                str += lst[i];
            }

            File.WriteAllText(url, str);
        }

        private string GetRelativePath()
        {
#if UNITY_EDITOR
            if (Application.isEditor)
                return "file://" + System.Environment.CurrentDirectory.Replace("\\", "/");
            // Use the build output folder directly.
            //else
#endif
                //if (Application.isWebPlayer)
                //return System.IO.Path.GetDirectoryName(Application.absoluteURL).Replace("\\", "/") +
                //       "/StreamingAssets";
            if (Application.isMobilePlatform || Application.isConsolePlatform)
                return Application.streamingAssetsPath;
            else // For standalone player.
                return "file://" + Application.streamingAssetsPath;
        }

        private string[] GetAllAssetBundleName()
        {
            return AssetBundleManager2.AssetBundleManifestObject.GetAllAssetBundles();
        }

#if UNITY_EDITOR
        public static string GetPlatformFolderForAssetBundles(BuildTarget target)
        {
            switch (target)
            {
                case BuildTarget.Android:
                    return "android.unity3d";
                case BuildTarget.iOS:
                    return "ios.unity3d";
                case BuildTarget.StandaloneWindows:
                case BuildTarget.StandaloneWindows64:
                    return "window.unity3d";
                case BuildTarget.WebGL:
                    return "webgl.unity3d";
                case BuildTarget.WSAPlayer:
                    return "wsa.unity3d";

                // AddValue more build targets for your own.
                // If you add more targets, don't forget to add the same platforms to GetPlatformFolderForAssetBundles(RuntimePlatform) function.
                default:
                    return null;
            }
        }
#endif

        public static string GetPlatformFolderForAssetBundles(RuntimePlatform platform)
        {
            switch (platform)
            {
                case RuntimePlatform.Android:
                    return "android.unity3d";
                case RuntimePlatform.IPhonePlayer:
                    return "ios.unity3d";
                case RuntimePlatform.WindowsPlayer:
                    return "window.unity3d";
                case RuntimePlatform.OSXPlayer:
                    return "osx.unity3d";
                case RuntimePlatform.WebGLPlayer:
                    return "webgl.unity3d";

                case RuntimePlatform.WSAPlayerARM:
                case RuntimePlatform.WSAPlayerX64:
                case RuntimePlatform.WSAPlayerX86:
                    return "wsa.unity3d";

                // AddValue more build platform for your own.
                // If you add more platforms, don't forget to add the same targets to GetPlatformFolderForAssetBundles(BuildTarget) function.
                default:
                    return null;
            }
        }

        public static string GetFirstAssetBundleFileName()
        {
#if UNITY_EDITOR
            switch (EditorUserBuildSettings.activeBuildTarget)
            {
                case BuildTarget.Android:
                    return "android";
                case BuildTarget.iOS:
                    return "ios";
                case BuildTarget.StandaloneWindows:
                case BuildTarget.StandaloneWindows64:
                    return "window";
                case BuildTarget.WebGL:
                    return "webgl";
                default:
                    return null;
            }
#else
        switch (Application.platform)
        {
            case RuntimePlatform.Android:
                return "android";
            case RuntimePlatform.IPhonePlayer:
                return "ios";
            case RuntimePlatform.WindowsPlayer:
                return "window";
            case RuntimePlatform.OSXPlayer:
                return "osx";
            case RuntimePlatform.WebGLPlayer:
                return "webgl";
            default:
                return null;
        }
#endif
        }

        #region API

        public void UnloadAllAssetBundle()
        {
            AssetBundleManager2.UnloadAllAssetBundle();
        }

        public void UnloadAssetBundle(string assetBundleName)
        {
            AssetBundleManager2.UnloadAssetBundle(assetBundleName);
        }

        public IEnumerator LoadAssetBundle<T>(string assetBundleName, string assetName, Action<T> onCompleteAction, bool isFast = false) where T : UnityEngine.Object
        {
            var assetBundleName2 = assetBundleName.ToLower();
            if (EncryptFileName)
                assetBundleName2 = assetBundleName2.Replace(".unity3d", "").EncryptMd5() + ".unity3d";

            var request = AssetBundleManager2.LoadAssetAsync(assetBundleName2, assetName, isFast, typeof(T));
            if (request == null) yield break;

            yield return StartCoroutine(request);

            var asset = request.GetAsset<T>();
            if (IsCacheAsset)
                AddAssetBundleCache(assetBundleName2);
            else
                UnloadAssetBundle(assetBundleName2);

            onCompleteAction?.Invoke(asset);
        }

        public IEnumerator DownloadGlobalConfigFile(string url, string configName, string configPath, Action<string> onCompleteAction)
        {
            WWW www = new WWW(url + configName+".txt");
            yield return www;
            string downloadText = www.text;
            string decodeStr = Utils.SimpleEncryptDecrypt.Decrypt(downloadText);
            www.Dispose();
            onCompleteAction(decodeStr);
        }

        private string wwwTextDecrypt(string input)
        {
            var result = input.Remove(0);
            result.Remove(result.Length);
            
            return result;
        }

        public IEnumerator LoadMultiAssetBundle<T>(string assetBundleName, string assetName, Action<T[]> onCompleteAction, bool isFast = false) where T : UnityEngine.Object
        {
            var assetBundleName2 = assetBundleName.ToLower();
            if (EncryptFileName)
            {
                assetBundleName2 = assetBundleName2.Replace(".unity3d", "").EncryptMd5() + ".unity3d";
            }

            var request = AssetBundleManager2.LoadMultiAssetAsync(assetBundleName2, assetName, isFast, typeof(T));
            if (request == null) yield break;

            yield return StartCoroutine(request);

            var asset = request.GetAllAsset<T>();
            if (IsCacheAsset) AddAssetBundleCache(assetBundleName2);
            else UnloadAssetBundle(assetBundleName2);

            if (onCompleteAction != null)
            {
                onCompleteAction(asset);
            }
        }

        public IEnumerator LoadScene(string assetBundleName, string levelName, bool isAdditive, Action<float> onProgressAction)
        {
            var assetBundleName2 = assetBundleName.ToLower();
            if (EncryptFileName)
            {
                assetBundleName2 = assetBundleName2.Replace(".unity3d", "").EncryptMd5() + ".unity3d";
            }

            // Load level from assetBundle.
            var request = AssetBundleManager2.LoadLevelAsync(assetBundleName2, levelName, isAdditive,
                onProgressAction);
            if (request == null) yield break;

            yield return StartCoroutine(request);

            if (IsCacheAsset) AddAssetBundleCache(assetBundleName2);
            else UnloadAssetBundle(assetBundleName2);
        }

        #endregion
    }
}
