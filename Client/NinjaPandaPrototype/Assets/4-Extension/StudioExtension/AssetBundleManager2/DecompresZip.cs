﻿using System;
using UnityEngine;
using System.Collections;
using System.Threading;

#if (UNITY_WSA_8_1 || UNITY_WP_8_1 || UNITY_WINRT_8_1) && !UNITY_EDITOR
 using UnityEngine.Windows;
#else
using System.IO;
#endif

#if NETFX_CORE
#if UNITY_WSA_10_0
    using System.Threading.Tasks;
    using static System.IO.Directory;
    using static System.IO.File;
    using static System.IO.FileStream;
#endif
#endif

namespace Studio
{


    public class DecompresZip : MonoBehaviour
    {

        private static DecompresZip _instance;

        public static DecompresZip Instance
        {
            get
            {
                if (_instance == null)
                {
                    var go = new GameObject();
                    go.name = "decompresZip";
                    _instance = go.AddComponent<DecompresZip>();
                    DontDestroyOnLoad(go);
                }
                return _instance;
            }
        }

        public bool IsProcessing = false;
        public bool IsDecompressDone = false;

        private string _pathExtractionFolder;
        private string _pathFileZip;

        private readonly int[] _progressValue = new int[1];
        private readonly int[] _progress2 = new int[1];

        private Action<string> _onErrorAction;

        public int GetProgressFiles()
        {
            return _progressValue[0];
        }

        public int GetProgressBytes()
        {
            return _progress2[0];
        }

        public IEnumerator Decompression(string pathFileZip, string pathExtractionFolder, Action<int> onProgressAction,
            Action<string> onErrorAction, bool delAfterFinished = true)
        {
            if (IsProcessing)
            {
                Done();
                yield break;
            }

            IsProcessing = true;
            IsDecompressDone = false;

            _onErrorAction = onErrorAction;
            _pathFileZip = pathFileZip.ConvertToPath();
            _pathExtractionFolder = pathExtractionFolder.ConvertToPath();
            _progressValue[0] = 0;
            _progress2[0] = 0;

//#if !UNITY_WEBGL || UNITY_EDITOR
#if !NETFX_CORE
            var decompressionThread = new Thread(ThreadedDecompressionFunction);
            decompressionThread.Start();

#endif
#if NETFX_CORE
#if UNITY_WSA_10_0
		    Task task = new Task(new Action(ThreadedDecompressionFunction)); 
            task.Start();
#else
            ThreadedDecompressionFunction();
#endif
#endif
//#endif
            while (!IsDecompressDone)
            {
                if (onProgressAction != null) onProgressAction(GetProgressFiles());
                yield return null;
            }

            if (delAfterFinished)
            {
                if (File.Exists(_pathFileZip))
                {
                    //Debug.LogError("delete file zip:: " + _pathFileZip);
                    File.Delete(_pathFileZip);
                }
            }

            if (onProgressAction != null) onProgressAction(GetProgressFiles());

            Done();
        }

        private void Done()
        {
            IsProcessing = false;
            Destroy(gameObject);
        }

        private void ThreadedDecompressionFunction()
        {
            if (!Directory.Exists(_pathExtractionFolder))
                BundleUtils.CreateDirectory(_pathExtractionFolder);

            try
            {
                //var result = lzip.decompress_File(_pathFileZip, _pathExtractionFolder, _progressValue, null, _progress2);
                //Debug.LogError("decompress error:: " + result + " :: file: " + _pathFileZip);
            }
            catch (Exception ex)
            {
                if (_onErrorAction != null) _onErrorAction(ex.Message);
            }

            IsDecompressDone = true;
        }
    }
}
