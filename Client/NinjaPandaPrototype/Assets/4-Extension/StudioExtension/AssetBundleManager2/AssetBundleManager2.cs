﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System.Collections;
#if (UNITY_WSA_8_1 || UNITY_WP_8_1 || UNITY_WINRT_8_1) && !UNITY_EDITOR
 using UnityEngine.Windows;
#else
using System.IO;
#endif

namespace Studio
{
    public class LoadedAssetBundle2
    {
        public AssetBundle m_AssetBundle;
        public int m_ReferencedCount;

        public LoadedAssetBundle2(AssetBundle assetBundle)
        {
            m_AssetBundle = assetBundle;
            m_ReferencedCount = 1;
        }
    }

    public class AssetBundleManager2 : MonoBehaviour
    {
        static string m_BaseDownloadingURL = "";
        static string[] m_Variants = { };
        static AssetBundleManifest m_AssetBundleManifest = null;
        static List<string> m_AssetBundleList = null;

        public static Action<string, string> OnErrorLoadAction = delegate { };

#if UNITY_EDITOR
        public static bool IsShowAssetBundleInMemory = false;
        static int m_SimulateAssetBundleInEditor = -1;
        const string kSimulateAssetBundles = "SimulateAssetBundles";
#endif

        public static Dictionary<string, LoadedAssetBundle2> m_LoadedAssetBundles = new Dictionary<string, LoadedAssetBundle2>();
#if UNITY_EDITOR
        public static Dictionary<string, AssetBundleCreateRequest> m_DownloadingWWWs = new Dictionary<string, AssetBundleCreateRequest>();
#else
        public static Dictionary<string, WWW> m_DownloadingWWWs = new Dictionary<string, WWW>();
//#endif
#endif
        public static Dictionary<string, string> m_DownloadingErrors = new Dictionary<string, string>();
        static List<AssetBundleLoadOperation2> m_InProgressOperations = new List<AssetBundleLoadOperation2>();
        public static Dictionary<string, string[]> m_Dependencies = new Dictionary<string, string[]>();
        static List<string> m_DependenciesCacheList = new List<string>();
        static List<AssetBundleLoadAsyncLevelOperation2> m_SceneOperatorList = new List<AssetBundleLoadAsyncLevelOperation2>();
        static Dictionary<string, List<Action<float>>> m_AssetBundleLoadAction = new Dictionary<string, List<Action<float>>>();
        static Dictionary<string, int> m_AssetBundleReferencedCount = new Dictionary<string, int>();

        static List<string> m_AssetBundleFastQueue = new List<string>();
        static List<string> m_AssetBundleQueue = new List<string>();
        static List<string> m_AssetBundleQueueLoading = new List<string>();

        public static bool HasStreamingAsset;
        public static List<string> ResourcesHasUpdate = new List<string>();

        public static Action<string, float, bool> OnAssetBundleProcessingAction = delegate { };

        // The base downloading url which is used to generate the full downloading url with the assetBundle names.
        public static string BaseDownloadingURL
        {
            get { return m_BaseDownloadingURL; }
            set { m_BaseDownloadingURL = value; }
        }

        public static string OnlineUrl
        {
            get { return m_BaseDownloadingURL; }
        }

        public static string LocalURL
        {
#if UNITY_EDITOR || UNITY_STANDALONE
            get
            {
                return BundleUtils.GetCachePath() + "/" + "AssetBundles/" + BaseLoader2.GetFirstAssetBundleFileName() + "/";
            }
#else
            get 
            { 
                return BundleUtils.GetCachePath() + "/" + "AssetBundles/"; 
            }
#endif
        }

        public static string StreamingAssetsUrl
        {
            get
            {
#if UNITY_WEBGL && !UNITY_EDITOR
                return BC.BCConfig.Api.WebGLStreamingAssetUrl;
#else
                return Application.streamingAssetsPath + "/";
#endif
            }
        }

        // Variants which is used to define the active variants.
        public static string[] Variants
        {
            get { return m_Variants; }
            set { m_Variants = value; }
        }

        public static int AssetBundleLoadedCount
        {
            get { return m_LoadedAssetBundles.Count; }
        }

        public static List<string> AssetBundleList
        {
            get
            {
                if (m_AssetBundleList == null && m_AssetBundleManifest != null)
                {
                    m_AssetBundleList = m_AssetBundleManifest.GetAllAssetBundles().ToList();
                }

                return m_AssetBundleList ?? new List<string>();
            }
        }

        // AssetBundleManifest object which can be used to load the dependecies and check suitable assetBundle variants.
        public static AssetBundleManifest AssetBundleManifestObject
        {
            set { m_AssetBundleManifest = value; }
            get { return m_AssetBundleManifest; }
        }

        // Load AssetBundleManifest.
        static public AssetBundleLoadManifestOperation2 Initialize(string manifestAssetBundleName)
        {
            var go = new GameObject("_AssetBundleManager2", typeof(AssetBundleManager2));
            DontDestroyOnLoad(go);

            LoadAssetBundle(manifestAssetBundleName, false, true, true);
            var operation = new AssetBundleLoadManifestOperation2(manifestAssetBundleName, "AssetBundleManifest", typeof(AssetBundleManifest));
            m_InProgressOperations.Add(operation);
            return operation;
        }

        // Load AssetBundle and its dependencies.
        static protected void LoadAssetBundle(string assetBundleName, bool async, bool isFast, bool isLoadingAssetBundleManifest = false)
        {
            if (!isLoadingAssetBundleManifest && !AssetBundleList.Contains(assetBundleName))
            {
                DebugX.LogError("assetbundle not exits::" + assetBundleName);

                if (!m_DownloadingErrors.ContainsKey(assetBundleName))
                {
                    m_DownloadingErrors.Add(assetBundleName, "assetbundle not exits:: " + assetBundleName);
                }
                return;
            }

            //is loading assetbundle
            if (async)
            {
                AddQueue(assetBundleName, isFast);
                return;
            }

            // Check if the assetBundle has already been processed.
            var isAlreadyProcessed = LoadAssetBundleInternal(assetBundleName, isLoadingAssetBundleManifest, false);

            // Load dependencies.
            if (!isAlreadyProcessed && !isLoadingAssetBundleManifest)
                LoadDependencies(assetBundleName, false);
        }

        static protected void LoadAssetBundleQueue(string assetBundleName)
        {
            var isAlreadyProcessed = LoadAssetBundleInternal(assetBundleName, false, true);
            if (!isAlreadyProcessed) LoadDependencies(assetBundleName, true);
        }

        // Get loaded AssetBundle, only return vaild object when all the dependencies are downloaded successfully.
        static public LoadedAssetBundle2 GetLoadedAssetBundle(string assetBundleName, out string error)
        {
            if (m_DownloadingErrors.TryGetValue(assetBundleName, out error))
                return null;

            LoadedAssetBundle2 bundle = null;
            m_LoadedAssetBundles.TryGetValue(assetBundleName, out bundle);
            if (bundle == null)
                return null;

            // No dependencies are recorded, only the bundle itself is required.
            string[] dependencies = null;
            if (!m_Dependencies.TryGetValue(assetBundleName, out dependencies))
                return bundle;

            // Make sure all dependencies are loaded
            foreach (var dependency in dependencies)
            {
                if (m_DownloadingErrors.TryGetValue(assetBundleName, out error))
                    return bundle;

                // Wait all the dependent assetBundles being loaded.
                LoadedAssetBundle2 dependentBundle;
                if (!m_LoadedAssetBundles.TryGetValue(dependency, out dependentBundle))
                    return null;
            }

            return bundle;
        }

        // Remaps the asset bundle name to the best fitting asset bundle variant.
        static protected string RemapVariantName(string assetBundleName)
        {
            if (m_Variants == null || m_Variants.Length == 0) return assetBundleName;

            var bundlesWithVariant = m_AssetBundleManifest.GetAllAssetBundlesWithVariant();
            var split = assetBundleName.Split('.');

            var bestFit = int.MaxValue;
            var bestFitIndex = -1;

            // Loop all the assetBundles with variant to find the best fit variant assetBundle.
            for (var i = 0; i < bundlesWithVariant.Length; i++)
            {
                var curSplit = bundlesWithVariant[i].Split('.');
                if (curSplit[0] != split[0]) continue;

                var found = System.Array.IndexOf(m_Variants, curSplit[curSplit.Length - 1]);

                // If there is no active variant found. We still want to use the first 
                if (found == -1) found = int.MaxValue - 1;
                if (found < bestFit)
                {
                    bestFit = found;
                    bestFitIndex = i;
                }
            }

            if (bestFit == int.MaxValue - 1)
            {
                DebugX.LogWarning("Ambigious asset bundle variant chosen because there was no matching active variant: " + bundlesWithVariant[bestFitIndex]);
            }

            if (bestFitIndex != -1)
            {
                return bundlesWithVariant[bestFitIndex];
            }
            else
            {
                return assetBundleName;
            }
        }

        // Where we actuall call WWW to download the assetBundle.
        static protected bool LoadAssetBundleInternal(string assetBundleName, bool isLoadingAssetBundleManifest, bool async)
        {
            if (string.IsNullOrEmpty(assetBundleName))
            {
                DebugX.LogError("LoadAssetBundleInternal: assetBundleName is null or empty!");
                return true;
            }

            // Already loaded.
            LoadedAssetBundle2 bundle = null;
            m_LoadedAssetBundles.TryGetValue(assetBundleName, out bundle);
            if (bundle != null)
            {
                AddReferencedCount(assetBundleName);
                return false;
            }

            var url = GetBundlePath(assetBundleName).ConvertToPath();
            if (ResourcesHasUpdate.Contains(assetBundleName) && !File.Exists(LocalURL + assetBundleName))
            {
                DebugX.LogError("can't find url assetbundle::" + url);
                if (!m_DownloadingErrors.ContainsKey(assetBundleName))
                {
                    m_DownloadingErrors.Add(assetBundleName, "can't find assetbundle:: " + assetBundleName);
                }

                return true;
            }

            if (m_DownloadingWWWs.ContainsKey(assetBundleName))
            {
                AddReferencedCount(assetBundleName);
                return false;
            }
            if (BC.BCConfig.Api.IsShowAssetBundleDebug)
                DebugX.Log("AssetBundle Loading: " + url);

            AddReferencedCount(assetBundleName);
#if UNITY_EDITOR
            var dataBytes = File.ReadAllBytes(url).DecyptFile();
            m_DownloadingWWWs.Add(assetBundleName, AssetBundle.LoadFromMemoryAsync(dataBytes));
#else
            m_DownloadingWWWs.Add(assetBundleName, new WWW(url));
#endif
            //#endif

            return false;
        }

        // Where we get all the dependencies and load them all.
        static protected void LoadDependencies(string assetBundleName, bool async)
        {
            if (m_Dependencies.ContainsKey(assetBundleName)) return;

            if (m_AssetBundleManifest == null)
            {
                DebugX.LogError("Please initialize AssetBundleManifest by calling AssetBundleManager.Initialize()");
                return;
            }

            // Get dependecies from the AssetBundleManifest object..
            string[] dependencies = m_AssetBundleManifest.GetAllDependencies(assetBundleName);
            if (dependencies.Length == 0)
                return;

            var depenList = new List<string>();
            foreach (var dependency in dependencies)
            {
                if (string.IsNullOrEmpty(dependency))
                {
                    DebugX.LogError("LoadDependencies: " + assetBundleName + " has null dependencies !");
                    continue;
                }

                //cheat animator refference with other animator
                if (assetBundleName.Contains("animator") && dependency.Contains("animator"))
                {
                    DebugX.LogError("has animator: <" + dependency + "> in animator : <" + assetBundleName + ">");
                    continue;
                }

                // depenList.Add(RemapVariantName(dependency));
                if (!m_DependenciesCacheList.Contains(dependency))
                {
                    depenList.Add(dependency);
                    m_DependenciesCacheList.Add(dependency);
                }

            }

            // Record and load all dependencies.
            if (depenList.Count <= 0)
                return;

            m_Dependencies.Add(assetBundleName, depenList.ToArray());

            for (int i = 0; i < depenList.Count; i++)
                LoadAssetBundleInternal(depenList[i], false, async);
        }

        // Unload assetbundle and its dependencies.
        static public void UnloadAssetBundle(string assetBundleName)
        {
            //DebugX.Log(m_LoadedAssetBundles.Count + " assetbundle(s) in memory before unloading " + assetBundleName);
            UnloadAssetBundleInternal(assetBundleName);
            UnloadDependencies(assetBundleName);

            //DebugX.Log(m_LoadedAssetBundles.Count + " assetbundle(s) in memory after unloading " + assetBundleName);

#if UNITY_EDITOR
            if (IsShowAssetBundleInMemory)
            {
                foreach (var mLoadedAssetBundle in m_LoadedAssetBundles)
                {
                    DebugX.Log("assetbundle(s) in memory: " + mLoadedAssetBundle.Key);
                }
            }
#endif
        }

        static public void UnloadAllAssetBundle()
        {
            m_AssetBundleReferencedCount.Clear();
            m_AssetBundleFastQueue.Clear();
            m_AssetBundleQueue.Clear();
            m_AssetBundleQueueLoading.Clear();
            m_DownloadingWWWs.Clear();

            m_InProgressOperations.Clear();
            m_SceneOperatorList.Clear();
            m_AssetBundleLoadAction.Clear();

            m_DownloadingErrors.Clear();
            m_DependenciesCacheList.Clear();

            foreach (var assetBundleName in m_LoadedAssetBundles.Keys.ToList())
            {
                DebugX.LogError("Unloaded: " + assetBundleName);
                UnloadAssetBundleInternal(assetBundleName);
            }
        }

        static protected void UnloadDependencies(string assetBundleName)
        {
            string[] dependencies = null;
            if (!m_Dependencies.TryGetValue(assetBundleName, out dependencies))
                return;

            // Loop dependencies.
            foreach (var dependency in dependencies)
            {
                if (m_DependenciesCacheList.Contains(dependency))
                    m_DependenciesCacheList.Remove(dependency);
                UnloadAssetBundleInternal(dependency);
            }

            m_Dependencies.Remove(assetBundleName);
        }

        static protected void UnloadAssetBundleInternal(string assetBundleName)
        {
            string error;
            var bundle = GetLoadedAssetBundle(assetBundleName, out error);
            if (bundle == null) return;

            if (!HasReferencedCount(assetBundleName))
            {
                if (bundle.m_AssetBundle != null)
                {
                    bundle.m_AssetBundle.Unload(false);
                }

                if (m_LoadedAssetBundles.ContainsKey(assetBundleName))
                {
                    m_LoadedAssetBundles.Remove(assetBundleName);
                }
            }
        }

        static protected void CheckQueue()
        {
            if (m_AssetBundleQueueLoading.Count > 0)
            {
                var assetBundle = m_AssetBundleQueueLoading[0];
                if (!m_LoadedAssetBundles.ContainsKey(assetBundle) && !m_DownloadingWWWs.ContainsKey(assetBundle))
                {
                    LoadAssetBundleQueue(assetBundle);
                }
                return;
            }

            //ưu tiên load trc
            if (m_AssetBundleFastQueue.Count > 0)
            {
                var assetBundle = m_AssetBundleFastQueue[0];

                if (!m_AssetBundleQueueLoading.Contains(assetBundle))
                {
                    m_AssetBundleQueueLoading.Add(assetBundle);
                }

                LoadAssetBundleQueue(assetBundle);
                m_AssetBundleFastQueue.RemoveAt(0);
            }
            else if (m_AssetBundleQueue.Count > 0)
            {
                var assetBundle = m_AssetBundleQueue[0];

                if (!m_AssetBundleQueueLoading.Contains(assetBundle))
                {
                    m_AssetBundleQueueLoading.Add(assetBundle);
                }

                LoadAssetBundleQueue(assetBundle);
                m_AssetBundleQueue.RemoveAt(0);
            }
        }

        static protected void AddQueue(string assetBundle, bool isFast)
        {
            if (m_AssetBundleQueueLoading.Count == 0)
            {
                m_AssetBundleQueueLoading.Add(assetBundle);
                return;
            }

            if (isFast)
            {
                if (!m_AssetBundleFastQueue.Contains(assetBundle))
                {
                    m_AssetBundleFastQueue.Add(assetBundle);
                }
            }
            else
            {
                if (!m_AssetBundleQueue.Contains(assetBundle))
                {
                    m_AssetBundleQueue.Add(assetBundle);
                }
            }
        }

        static protected void AddReferencedCount(string assetBundleName)
        {
            if (!m_AssetBundleReferencedCount.ContainsKey(assetBundleName))
            {
                m_AssetBundleReferencedCount.Add(assetBundleName, 0);
            }

            m_AssetBundleReferencedCount[assetBundleName] += 1;
        }

        static protected bool HasReferencedCount(string assetBundleName)
        {
            var result = true;
            if (m_AssetBundleReferencedCount.ContainsKey(assetBundleName))
            {
                m_AssetBundleReferencedCount[assetBundleName] -= 1;

                if (m_AssetBundleReferencedCount[assetBundleName] <= 0)
                {
                    m_AssetBundleReferencedCount.Remove(assetBundleName);
                    result = false;
                }
            }
            else
            {
                result = false;
            }

            return result;
        }

        public static string GetBundlePath(string relativePath)
        {
            if (!HasStreamingAsset || ResourcesHasUpdate.Contains(relativePath))
            {
#if UNITY_EDITOR
                return LocalURL + relativePath;

#elif UNITY_WEBGL
                return AssetBundleManager2.BaseDownloadingURL + relativePath;
#else
                return "file://" + LocalURL + relativePath;
#endif
            }
            else
            {
#if UNITY_EDITOR
                return StreamingAssetsUrl + relativePath;
#else
#if UNITY_IOS
                return "file://" + StreamingAssetsUrl + relativePath;
#else
                return StreamingAssetsUrl + relativePath;
#endif
#endif
            }
        }

        public static void AddResourcesHasUpdate(string assetBundleName)
        {
            if (!ResourcesHasUpdate.Contains(assetBundleName))
            {
                ResourcesHasUpdate.Add(assetBundleName);
            }
        }

        public static void RemoveResourcesHasUpdate(string assetBundleName)
        {
            if (ResourcesHasUpdate.Contains(assetBundleName))
            {
                ResourcesHasUpdate.Remove(assetBundleName);
            }
        }

        void Update()
        {
#region Load
            // Collect all the finished WWWs.
            var keysToRemove = new List<string>();
            var preloadAsset = new List<string>();
            foreach (var keyValue in m_DownloadingWWWs)
            {
                var download = keyValue.Value;

                // If downloading succeeds.
#if !UNITY_EDITOR
                if (download.error != null)
                {
                    var errorStr = download.error;
                    if (!m_DownloadingErrors.ContainsKey(keyValue.Key))
                    {
                        m_DownloadingErrors.Add(keyValue.Key, errorStr);
                    }

                    if (OnErrorLoadAction != null) {
                        OnErrorLoadAction(keyValue.Key, errorStr);
                    }

                    preloadAsset.Add(keyValue.Key);
                    keysToRemove.Add(keyValue.Key);
                    download.Dispose();
                }
                else
#endif
                if (download.isDone)
                {
#if UNITY_EDITOR
                    var assetBundle = download.assetBundle;
#else
                    var byteData = download.bytes.DecyptFile();
                    var assetBundle = byteData != null ? AssetBundle.LoadFromMemory(byteData) : null;
#endif
                    if (assetBundle == null)
                    {
                        if (!m_DownloadingErrors.ContainsKey(keyValue.Key))
                        {
                            m_DownloadingErrors.Add(keyValue.Key, "can't find assetbundle:: " + keyValue.Key);
                        }

                        if (OnErrorLoadAction != null)
                        {
                            OnErrorLoadAction(keyValue.Key, "can't find assetbundle");
                        }
                    }
                    else
                    {
                        // Debug.Log("Downloading " + keyValue.Key + " is done:: " + download.bytesDownloaded);
                        if (!m_LoadedAssetBundles.ContainsKey(keyValue.Key))
                        {
                            m_LoadedAssetBundles.Add(keyValue.Key, new LoadedAssetBundle2(assetBundle));
                        }
                        else
                        {
                            m_LoadedAssetBundles[keyValue.Key].m_AssetBundle = assetBundle;
                        }
                    }

                    keysToRemove.Add(keyValue.Key);
#if !UNITY_EDITOR
                    download.Dispose();
#endif
                }
            }
            // Remove the finished WWWs.
            foreach (var key in keysToRemove)
            {
                if (m_DownloadingWWWs.ContainsKey(key))
                {
                    m_DownloadingWWWs.Remove(key);
                }
                if (m_AssetBundleLoadAction.ContainsKey(key))
                {
                    m_AssetBundleLoadAction.Remove(key);
                }
            }

            // Update all in progress operations
            var inProgressOperation = new List<string>();
            for (var i = 0; i < m_InProgressOperations.Count;)
            {
                if (!m_InProgressOperations[i].Update())
                {
                    //check download done
                    m_InProgressOperations.RemoveAt(i);
                }
                else
                {
                    inProgressOperation.Add(m_InProgressOperations[i].AssetBundleName);
                    i++;
                }
            }

            for (var i = 0; i < m_SceneOperatorList.Count;)
            {
                if (!m_SceneOperatorList[i].NotifyProgress()) m_SceneOperatorList.RemoveAt(i);
                else i++;
            }

#endregion

#region check preload

            foreach (var assetBundle in preloadAsset)
            {
                if (m_DownloadingWWWs.ContainsKey(assetBundle))
                {
                    m_DownloadingWWWs.Remove(assetBundle);
                }

                var url = GetBundlePath(assetBundle);
#if UNITY_EDITOR
                var dataBytes = File.ReadAllBytes(url).DecyptFile();
                m_DownloadingWWWs.Add(assetBundle, AssetBundle.LoadFromMemoryAsync(dataBytes));
#else
                m_DownloadingWWWs.Add(assetBundle, new WWW(url));
#endif
            }

#endregion

#region Check Queue

            for (var i = 0; i < m_AssetBundleQueueLoading.Count;)
            {
                if (!inProgressOperation.Contains(m_AssetBundleQueueLoading[i]))
                {
                    m_AssetBundleQueueLoading.RemoveAt(i);
                }
                else i++;
            }

            CheckQueue();

#endregion
        }

        // Load asset from the given assetBundle.
        static public AssetBundleLoadAssetOperation2 LoadAssetAsync(string assetBundleName, string assetName, bool isFast, System.Type type)
        {
            // assetBundleName = RemapVariantName(assetBundleName);
            LoadAssetBundle(assetBundleName, true, isFast);

            var operation = new AssetBundleLoadAssetAsyncOperationFull2(assetBundleName, assetName, type);
            m_InProgressOperations.Add(operation);

            return operation;
        }

        static public AssetBundleLoadMultiAssetAsyncOperationFull2 LoadMultiAssetAsync(string assetBundleName, string assetName, bool isFast, System.Type type)
        {
            // assetBundleName = RemapVariantName(assetBundleName);
            LoadAssetBundle(assetBundleName, true, isFast);

            var operation = new AssetBundleLoadMultiAssetAsyncOperationFull2(assetBundleName, assetName, type);
            m_InProgressOperations.Add(operation);

            return operation;
        }

        static public AssetBundleLoadAssetOperation2 LoadAsset(string assetBundleName, string assetName, bool isFast, System.Type type)
        {
            //assetBundleName = RemapVariantName(assetBundleName);
            LoadAssetBundle(assetBundleName, false, isFast);

            var operation = new AssetBundleLoadAssetAsyncOperationFull2(assetBundleName, assetName, type);
            m_InProgressOperations.Add(operation);

            return operation;
        }

        // Load level from the given assetBundle.
        static public AssetBundleLoadOperation2 LoadLevelAsync(string assetBundleName, string levelName, bool isAdditive, Action<float> onProgressAction)
        {
            //assetBundleName = RemapVariantName(assetBundleName);
            LoadAssetBundle(assetBundleName, true, true);

            var operation = new AssetBundleLoadAsyncLevelOperation2(assetBundleName, levelName, isAdditive)
            {
                OnProgressAction = onProgressAction
            };

            m_SceneOperatorList.Add(operation);
            m_InProgressOperations.Add(operation);

            return operation;
        }
    }
}