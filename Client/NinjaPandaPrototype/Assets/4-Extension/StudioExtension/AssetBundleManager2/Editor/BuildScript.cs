﻿using System;
using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.IO;
using Studio.BC;

namespace Studio
{
    public class BuildScript
    {
        const string kAssetBundlesOutputPath = "AssetBundles";

        const string TempAssetBundle = "TempAssetBundle";
        const string AssetBundleSource = "/Assets/0-AssetBundleSource";
        const string PaternFolder = "Textures";

        const string lowResource = "Resources - low";
        const string ResourcePath = "Resources";

        private static List<AssetBundleInfo> assetList(bool isUseLowResource)
        {

            var resourcePath = ResourcePath;
            if (isUseLowResource)
                resourcePath = lowResource;

            List<AssetBundleInfo> _assetList = new List<AssetBundleInfo>()
            {
                #region Asset2d
 
                new AssetBundleInfo()
                {
                    Path = "0-AssetBundles/"+resourcePath+"/Asset2d/Avatar",
                    ExtFilter = "*.png;*jpg",
                    AssetName = "asset2d/avatar/{0}.unity3d"
                },
                //new AssetBundleInfo()
                //{
                //    Path = "0-AssetBundles/"+resourcePath+"/Asset2d/Event",
                //    ExtFilter = "*.png;*.jpg",
                //    AssetName = "asset2d/event/{0}.unity3d"
                //},
                new AssetBundleInfo()
                {
                    Path = "0-AssetBundles/"+resourcePath+"/Asset2d/Border",
                    ExtFilter = "*.png;*.jpg",
                    AssetName = "asset2d/border/{0}.unity3d"
                },
                new AssetBundleInfo()
                {
                    Path = "0-AssetBundles/"+resourcePath+"/Asset2d/Inventory",
                    ExtFilter = "*.png;*.jpg",
                    AssetName = "asset2d/inventory/{0}.unity3d"
                },
                new AssetBundleInfo()
                {
                    Path = "0-AssetBundles/"+resourcePath+"/Asset2d/Trophy",
                    ExtFilter = "*.png;*.jpg",
                    AssetName = "asset2d/trophy/{0}.unity3d"
                },
                 new AssetBundleInfo()
                {
                    Path = "0-AssetBundles/"+resourcePath+"/Asset2d/Quest",
                    ExtFilter = "*.png;*.jpg",
                    AssetName = "asset2d/quest/{0}.unity3d"
                },
                new AssetBundleInfo()
				{
					Path = "0-AssetBundles/"+resourcePath+"/Asset2d/Gun",
					ExtFilter = "*.png;*.jpg",
					AssetName = "asset2d/gun/{0}.unity3d"
				},
				new AssetBundleInfo()
				{
					Path = "0-AssetBundles/"+resourcePath+"/Asset2d/Wing",
					ExtFilter = "*.png;*.jpg",
					AssetName = "asset2d/wing/{0}.unity3d"
				},
                new AssetBundleInfo()
                {
                    Path = "0-AssetBundles/"+resourcePath+"/Asset2d/Currency",
                    ExtFilter = "*.png;*.jpg",
                    AssetName = "asset2d/currency/{0}.unity3d"
                },
                new AssetBundleInfo()
				{
					Path = "0-AssetBundles/"+resourcePath+"/Asset2d/Frame",
					ExtFilter = "*.png;*.jpg",
					AssetName = "asset2d/frame/{0}.unity3d"
				},
				new AssetBundleInfo()
				{
					Path = "0-AssetBundles/"+resourcePath+"/Asset2d/Fish",
					ExtFilter = "*.png;*.jpg",
					AssetName = "asset2d/fish/{0}.unity3d"
				},
				new AssetBundleInfo()
				{
					Path = "0-AssetBundles/"+resourcePath+"/Asset2d/Skill",
					ExtFilter = "*.png;*.jpg",
					AssetName = "asset2d/skill/{0}.unity3d"
				},new AssetBundleInfo()
                {
                    Path = "0-AssetBundles/"+resourcePath+"/Asset2d/Tutorial",
                    ExtFilter = "*.png;*.jpg",
                    AssetName = "asset2d/tutorial/{0}.unity3d"
                },new AssetBundleInfo()
                {
                    Path = "0-AssetBundles/"+resourcePath+"/Asset2d/Map",
                    ExtFilter = "*.png;*.jpg",
                    AssetName = "asset2d/map/{0}.unity3d"
                },
                #endregion

                #region Atlas
                new AssetBundleInfo()
                {
                    Path = "0-AssetBundles/"+resourcePath+"/UI/Atlas",
                    ExtFilter = "*.prefab",
                    AssetName = "atlas/{0}.unity3d"
                },

                #endregion

                #region Config
                new AssetBundleInfo()
                {
                    Path = "0-AssetBundles/"+resourcePath+"/Config",
                    ExtFilter = "*.txt",
                    AssetName = "config/{0}.unity3d"
                },
                #endregion

                #region Effect
                new AssetBundleInfo()
                {
                    Path = "0-AssetBundles/"+resourcePath+"/Effect/Prefabs",
                    ExtFilter = "*.prefab",
                    AssetName = "effect/prefabs/{0}.unity3d"
                },
                new AssetBundleInfo()
                {
                    Path = "0-AssetBundles/"+resourcePath+"/Effect/Textures",
                    ExtFilter = "*.png;*.jpg",
                    AssetName = "effect/textures/{0}.unity3d"
                },
                new AssetBundleInfo()
                {
                    Path = "0-AssetBundles/"+resourcePath+"/Effect/Animation",
                    ExtFilter = "*.controller;*.anim",
                    AssetName = "effect/animation/{0}.unity3d"
                },

                #endregion

                #region Bullet
                new AssetBundleInfo()
                {
                    Path = "0-AssetBundles/"+resourcePath+"/Bullet",
                    ExtFilter = "*.prefab",
                    AssetName = "bullet/{0}.unity3d"
                },

                #endregion	

			    #region Buttons
                new AssetBundleInfo()
                {
                    Path = "0-AssetBundles/"+resourcePath+"/Buttons/Prefabs",
                    ExtFilter = "*.prefab",
                    AssetName = "buttons/prefabs/{0}.unity3d"
                },

                #endregion	

                #region Gun
                new AssetBundleInfo()
                {
                    Path = "0-AssetBundles/"+resourcePath+"/Gun/Prefabs",
                    ExtFilter = "*.prefab",
                    AssetName = "gun/prefabs/{0}.unity3d"
                },
                new AssetBundleInfo()
                {
                    Path = "0-AssetBundles/"+resourcePath+"/Gun/Textures",
                    ExtFilter = "*.png;*.jpg",
                    AssetName = "gun/textures/{0}.unity3d"
                },
                new AssetBundleInfo()
                {
                    Path = "0-AssetBundles/"+resourcePath+"/Gun/Animation",
                    ExtFilter = "*.controller;*.anim",
                    AssetName = "gun/animation/{0}.unity3d"
                },

                #endregion

                #region Frame
                new AssetBundleInfo()
                {
                    Path = "0-AssetBundles/"+resourcePath+"/Frame/Prefabs",
                    ExtFilter = "*.prefab",
                    AssetName = "frame/prefabs/{0}.unity3d"
                },
                new AssetBundleInfo()
                {
                    Path = "0-AssetBundles/"+resourcePath+"/Frame/Textures",
                    ExtFilter = "*.png;*.jpg",
                    AssetName = "frame/textures/{0}.unity3d"
                },
                #endregion

                #region Fish
                new AssetBundleInfo()
                {
                    Path = "0-AssetBundles/"+resourcePath+"/Fish/Prefabs",
                    ExtFilter = "*.prefab",
                    AssetName = "fish/prefabs/{0}.unity3d",
                    IsLocal = 1
                },
                new AssetBundleInfo()
                {
                    Path = "0-AssetBundles/"+resourcePath+"/Fish/Textures",
                    ExtFilter = "*.png;*.jpg",
                    AssetName = "fish/textures/{0}.unity3d",
                    IsLocal = 1
                },

                #endregion

                #region Fish Path
                new AssetBundleInfo()
                {
                    Path = "0-AssetBundles/"+resourcePath+"/FishPath/Prefabs",
                    ExtFilter = "*.prefab",
                    AssetName = "fishpath/prefabs/{0}.unity3d"
                },

                #endregion

                #region Sound
                new AssetBundleInfo()
                {
                    Path = "0-AssetBundles/"+resourcePath+"/Sound",
                    ExtFilter = "*.mp3",
                    AssetName = "sound/{0}.unity3d"
                },
                new AssetBundleInfo()
                {
                    Path = "0-AssetBundles/"+resourcePath+"/Sound",
                    ExtFilter = "*.wav",
                    AssetName = "sound/{0}.unity3d"
                },

                #endregion
			
                //#region Localize
                //new AssetBundleInfo()
                //{
                //    Path = "0-AssetBundles/"+resourcePath+"/Localize",
                //    ExtFilter = "*.txt",
                //    AssetName = "localize/{0}.unity3d"
                //},

                //#endregion

				#region Web
                new AssetBundleInfo()
				{
					Path = "0-AssetBundles/"+resourcePath+"/Web",
					ExtFilter = "*.prefab",
					AssetName = "web/{0}.unity3d"
				},
                #endregion

                #region Skill
                new AssetBundleInfo()
                {
                    Path = "0-AssetBundles/"+resourcePath+"/Skill",
                    ExtFilter = "*.prefab",
                    AssetName = "skill/{0}.unity3d"
                },

                #endregion
				
                #region Wing
                new AssetBundleInfo()
				{
					Path = "0-AssetBundles/"+resourcePath+"/Wing/Prefabs",
					ExtFilter = "*.prefab",
					AssetName = "wing/prefabs/{0}.unity3d"
				},
				new AssetBundleInfo()
				{
					Path = "0-AssetBundles/"+resourcePath+"/Wing/Textures",
					ExtFilter = "*.png;*.jpg",
					AssetName = "wing/textures/{0}.unity3d"
				},
				new AssetBundleInfo()
				{
					Path = "0-AssetBundles/"+resourcePath+"/Wing/Animation",
					ExtFilter = "*.controller;*.anim",
					AssetName = "wing/animation/{0}.unity3d"
				},
                #endregion

			#region MiniGame
			 new AssetBundleInfo()
			{
				Path = "0-AssetBundles/"+resourcePath+"/minigame/prefabs/",
				ExtFilter = "*.prefab",
				AssetName = "minigame/prefabs/{0}.unity3d"
			},
				new AssetBundleInfo()
			{
				Path = "0-AssetBundles/"+resourcePath+"/minigame/",
				ExtFilter = "*.bytes",
				AssetName = "minigame/{0}.unity3d"
			},
				new AssetBundleInfo()
			{
				Path = "0-AssetBundles/"+resourcePath+"/minigame/assets/",
				ExtFilter = "*.png;*.jpg",
				AssetName = "minigame/assets/{0}.unity3d"
			},
			#endregion
			
			//#region PM
			// new AssetBundleInfo()
   //         {
   //             Path = "0-AssetBundles/"+resourcePath+"/pm/assets/",
   //             ExtFilter = "*.png;*.jpg",
			//	AssetName = "pm/assets/{0}.unity3d"
   //         }, new AssetBundleInfo()
   //         {
   //             Path = "0-AssetBundles/"+resourcePath+"/pm/final/",
   //             ExtFilter = "*.bytes;*.prefab",
			//	AssetName = "pm/final/{0}.unity3d"
   //         },
			//#endregion
			
			#region Emoticons
			 new AssetBundleInfo()
			{
				Path = "0-AssetBundles/"+resourcePath+"/Emotion/Prefabs/",
				ExtFilter = "*.prefab",
				AssetName = "emotion/prefabs/{0}.unity3d"
			}, new AssetBundleInfo()
			{
				Path = "0-AssetBundles/"+resourcePath+"/Emotion/Texture/",
				ExtFilter = "*.jpg;*.png",
				AssetName = "emotion/texture/{0}.unity3d"
			},new AssetBundleInfo()
			{
				Path = "0-AssetBundles/"+resourcePath+"/Emotion/Animation/",
				ExtFilter = "*.anim",
				AssetName = "emotion/animation/{0}.unity3d"
			}
			#endregion
			
			};

            #region FishRaw
            var info = new DirectoryInfo(Application.dataPath + "/0-AssetBundles/"+resourcePath+"/FishRaw");
            var fileInfo = info.GetFiles();
            foreach (var file in fileInfo)
            {
				if (file.Name.Contains(".meta"))
					continue;
                string dirName = file.Name.Replace(".meta", "");
                var bundleInfo = new AssetBundleInfo()
                {
                    Path = "0-AssetBundles/"+resourcePath+"/FishRaw/" + dirName,
                    ExtFilter = "*.prefab,*.controller;*.anim;*.png;*.jpg",
                    AssetName = "fishraw/" + dirName.ToLower() + "/{0}.unity3d"
                };
                _assetList.Add(bundleInfo);
            }

			#endregion

			return _assetList;
        }

        public static void BuildAssetBundles(string path, List<BuildTarget> listBuildTargets, BuildAssetBundleOptions option, bool encyptAssetBundleName, bool isUseLowResource)
        {
            var assetBuildList = GetAssetBuildList(encyptAssetBundleName, isUseLowResource).ToArray();

            BCToolEditor.RemoveAtlasUIEffectPrefab();
            AssetDatabase.Refresh();

            foreach (var buildTarget in listBuildTargets)
            {
                var outputPath = Path.Combine(path, BaseLoader2.GetPlatformFolderForAssetBundles(buildTarget));
                if (!Directory.Exists(outputPath)) Directory.CreateDirectory(outputPath);

                Debug.Log("START BUILD " + BaseLoader2.GetPlatformFolderForAssetBundles(buildTarget));
                BuildPipeline.BuildAssetBundles(outputPath, assetBuildList, option, buildTarget);
                Debug.Log("BUILD SUCCESSED: " + BaseLoader2.GetPlatformFolderForAssetBundles(buildTarget));
            }

            BCToolEditor.RevertAtlasUIEffectPrefab();
            AssetDatabase.Refresh();
            Application.OpenURL(path);
        }

        public static void MoveTempTextureAssetBundle()
        {
            var basePath = Environment.CurrentDirectory;
			#if !UNITY_IOS
            var from = (basePath + AssetBundleSource).Replace("/", "\\");
            var to = (basePath + "/" + TempAssetBundle).Replace("/", "\\");
			#else
			var from = (basePath + AssetBundleSource);
			var to = (basePath + "/" + TempAssetBundle);
			#endif
            //remove to new temp
            MoveFolderLikePatern(from, to, PaternFolder, true);
            AssetDatabase.Refresh();
        }

        public static void MoveNormalTextureAssetBundle()
        {
            var basePath = Environment.CurrentDirectory;
			#if !UNITY_IOS
            var from = (basePath + "/" + TempAssetBundle).Replace("/", "\\");
            var to = (basePath + AssetBundleSource).Replace("/", "\\");
			#else
			var from = (basePath + "/" + TempAssetBundle);
			var to = (basePath + AssetBundleSource);
			#endif
            //remove to new temp
            MoveFolderLikePatern(from, to, PaternFolder);
            AssetDatabase.Refresh();
        }

        public static void BuildPlayer()
        {
            var outputPath = EditorUtility.SaveFolderPanel("Choose Location of the Built Game", "", "");
            if (outputPath.Length == 0)
                return;

            string[] levels = GetLevelsFromBuildSettings();
            if (levels.Length == 0)
            {
                Debug.Log("Nothing to build.");
                return;
            }

            string targetName = GetBuildTargetName(EditorUserBuildSettings.activeBuildTarget);
            if (targetName == null)
                return;

            BuildScript.CopyAssetBundlesTo(Path.Combine(Application.streamingAssetsPath, kAssetBundlesOutputPath));

            BuildOptions option = EditorUserBuildSettings.development ? BuildOptions.Development : BuildOptions.None;
            BuildPipeline.BuildPlayer(levels, outputPath + targetName, EditorUserBuildSettings.activeBuildTarget, option);
        }

        public static string GetBuildTargetName(BuildTarget target)
        {
            switch (target)
            {
                case BuildTarget.Android:
                    return "/test.apk";
                case BuildTarget.StandaloneWindows:
                case BuildTarget.StandaloneWindows64:
                    return "/test.exe";
                case BuildTarget.StandaloneOSXIntel:
                case BuildTarget.StandaloneOSXIntel64:
                case BuildTarget.StandaloneOSX:
                    return "/test.app";
                default:
                    Debug.Log("Target not implemented.");
                    return null;
            }
        }

        static void CopyAssetBundlesTo(string outputPath)
        {
            // Clear streaming assets folder.
            FileUtil.DeleteFileOrDirectory(Application.streamingAssetsPath);
            Directory.CreateDirectory(outputPath);

            string outputFolder = BaseLoader2.GetPlatformFolderForAssetBundles(EditorUserBuildSettings.activeBuildTarget);

            // Setup the source folder for assetbundles.
            var source = Path.Combine(Path.Combine(System.Environment.CurrentDirectory, kAssetBundlesOutputPath), outputFolder);
            if (!System.IO.Directory.Exists(source))
                Debug.Log("No assetBundle output folder, try to build the assetBundles first.");

            // Setup the destination folder for assetbundles.
            var destination = System.IO.Path.Combine(outputPath, outputFolder);
            if (System.IO.Directory.Exists(destination))
                FileUtil.DeleteFileOrDirectory(destination);

            FileUtil.CopyFileOrDirectory(source, destination);
        }

        static string[] GetLevelsFromBuildSettings()
        {
            List<string> levels = new List<string>();
            for (int i = 0; i < EditorBuildSettings.scenes.Length; ++i)
            {
                if (EditorBuildSettings.scenes[i].enabled)
                    levels.Add(EditorBuildSettings.scenes[i].path);
            }

            return levels.ToArray();
        }

        static void MoveFolderLikePatern(string from, string to, string paternFolderName, bool isDeleteFromFolder = false)
        {
            if (!Directory.Exists(to)) Directory.CreateDirectory(to);
            var directorys = Directory.GetDirectories(from, paternFolderName, SearchOption.AllDirectories);

            foreach (var directory in directorys)
            {
                var childPath = to + directory.Replace(from, "");
                if (!Directory.Exists(childPath))
                    Directory.CreateDirectory(childPath);

                MoveFiles(directory, childPath);

                //delete folder
                if (isDeleteFromFolder)
                    Directory.Delete(directory, true);
            }
        }

        static void MoveFiles(string from, string to)
        {
            if (!Directory.Exists(from)) return;
            if (!Directory.Exists(to))
            {
                Directory.CreateDirectory(to);
            }

            var files = Directory.GetFiles(from, "*.*", SearchOption.AllDirectories);
            foreach (var file in files)
            {
                var path = to + file.Replace(from, "");
                File.Copy(file, path, true);
            }
        }

        public static List<AssetBundleBuild> GetAssetBuildList(bool encyptAssetBundleName, bool isUseLowResource)
        {
            return GetAssetBuildList(assetList(isUseLowResource), encyptAssetBundleName);
        }

        public static List<AssetBundleBuild> GetAssetBuildList(List<AssetBundleInfo> assetList, bool encyptAssetBundleName)
        {
            var list = new List<AssetBundleBuild>();
            foreach (var assetBundleInfo in assetList)
            {
				#if !UNITY_IOS
                var path = (Application.dataPath + "/" + assetBundleInfo.Path).Replace("/", "\\");
				#else
				var path = (Application.dataPath + "/" + assetBundleInfo.Path);
				#endif
                var extension = string.IsNullOrEmpty(assetBundleInfo.ExtFilter) ? "*.*" : assetBundleInfo.ExtFilter;
                var searchOption = assetBundleInfo.AllDirectories
                    ? SearchOption.AllDirectories
                    : SearchOption.TopDirectoryOnly;

                foreach (var ext in extension.Split(';'))
                {
                    if (string.IsNullOrEmpty(ext)) continue;

                    var files = Directory.GetFiles(path, ext, searchOption);
                    foreach (var filePath in files)
                    {
                        var fileName = Path.GetFileNameWithoutExtension(filePath);
						#if !UNITY_IOS
                        var assetPath = "Assets" + (filePath.Replace("\\", "/").Replace(Application.dataPath, ""));
						#else
						var assetPath = "Assets" + (filePath.Replace(Application.dataPath, ""));
						#endif
                        if (!string.IsNullOrEmpty(fileName))
                        {
                            fileName = fileName.Replace(" ", "").ToLower();
                        }

                        var assetBundleName = "";
                        if (assetBundleInfo.HasParentParam)
                        {
                            var fullPath = filePath;
                            var parentName = "";
                            var parentLevel = assetBundleInfo.ParentLevel;

                            while (parentLevel > 0)
                            {
                                var directoryInfo = Directory.GetParent(fullPath);
                                if (directoryInfo.Parent != null)
                                {
                                    parentName = directoryInfo.Name;
                                    fullPath = directoryInfo.FullName;
                                }
                                parentLevel -= 1;
                            }

                            assetBundleName = string.Format(assetBundleInfo.AssetName, parentName, fileName);
                        }
                        else
                        {
                            assetBundleName = string.Format(assetBundleInfo.AssetName, fileName);
                        }

                        if (encyptAssetBundleName) {
                            assetBundleName = assetBundleName.Replace(".unity3d", "").EncryptMd5() + ".unity3d";
                        }

                        list.Add(new AssetBundleBuild()
                        {
                            assetBundleName = assetBundleName,
                            assetNames = new[] { assetPath },
                            assetBundleVariant = assetBundleInfo.AssetBundleVariant
                        });
                    }
                }
            }

            return list;
        }

        static void DeleteFileMandifest(string url)
        {
            var filePaths = Directory.GetFiles(url, "*.manifest", SearchOption.AllDirectories);
            foreach (var filePath in filePaths)
            {
                File.Delete(filePath);
            }
        }

        public static void EncryptFile(string url, string outPath, List<BuildTarget> listBuild)
        {
            foreach (var buildTarget in listBuild)
            {
                var buildTargetName = BaseLoader2.GetPlatformFolderForAssetBundles(buildTarget);
                var fullUrl = url + "/" + buildTargetName;
                var fullOutPath = outPath + "/" + buildTargetName;

                if (Directory.Exists(fullOutPath)) {
                    ClearFolder(fullOutPath);
                }

                Debug.Log("START ENCRYPT FILE...\nFrom: " + fullUrl + "\nTo: " + fullOutPath);

                var filePaths = Directory.GetFiles(fullUrl, "*.unity3d", SearchOption.AllDirectories);
                foreach (var filePath in filePaths)
                {
                    var outFileName = fullOutPath + filePath.Replace(fullUrl, "");
                    var outFileDir = outFileName.GetDirectory();

                    if (!Directory.Exists(outFileDir))
                    {
                        Directory.CreateDirectory(outFileDir);
                    }

                    var data = File.ReadAllBytes(filePath);
                    var dataEncrypt = data.EncryptFile();
                    File.WriteAllBytes(outFileName, dataEncrypt);
                }
            }

            Debug.Log("ENCRYPT FILE DONE !!");
        }

        public static void ExtractStreamingAsset(string url)
        {
            var streamingAssetUrl = Application.streamingAssetsPath;

            if (Directory.Exists(streamingAssetUrl)) {
                ClearFolder(streamingAssetUrl);
            }

            if (!Directory.Exists(streamingAssetUrl)) {
                Directory.CreateDirectory(streamingAssetUrl);
            }

            var encryptUrl = url + "/" + BaseLoader2.GetPlatformFolderForAssetBundles();

            Debug.Log("START EXTRACT STREAMINGASSET:: " + encryptUrl);

            var allFiles = Directory.GetFiles(encryptUrl, "*.*", SearchOption.AllDirectories);

            foreach (var filePath in allFiles)
            {
                if (File.Exists(filePath))
                {
                    var newFilePath = streamingAssetUrl + filePath.Replace(encryptUrl, "");
                    var fileFolder = newFilePath.GetDirectory();

                    if (!Directory.Exists(fileFolder)) {
                        Directory.CreateDirectory(fileFolder);
                    }

                    File.Copy(filePath, newFilePath);
                }
            }

            Debug.Log("EXTRACT DONE !");
            AssetDatabase.Refresh();
        }

        static List<AssetBundleBuild> GetAssetBundleBuild(string path, string assetName, Type type)
        {
            var list = new List<AssetBundleBuild>();
            var allAssets = type != null ? Resources.LoadAll(path, type) : Resources.LoadAll(path);
            Debug.Log("path:: " + path + " :: type: " + type);
            foreach (var o in allAssets)
            {
                if (o == null || string.IsNullOrEmpty(o.name)) continue;

                var assetPath = AssetDatabase.GetAssetPath(o);
                list.Add(new AssetBundleBuild()
                {
                    assetBundleName = string.Format(assetName.Replace(" ", ""), o.name),
                    assetNames = new[] { assetPath }
                });
            }

            return list;
        }

        static void ClearFolder(string folderPath)
        {
            var dir = new DirectoryInfo(folderPath);
            foreach (var fi in dir.GetFiles())
                fi.Delete();

            foreach (var di in dir.GetDirectories())
            {
                ClearFolder(di.FullName);
                di.Delete();
            }
        }

        public static void ExportAssetBundleManifest(string path, BuildTarget buildTarget)
        {
			#if !UNITY_IOS
            var outputPath = Path.Combine(path, BaseLoader2.GetPlatformFolderForAssetBundles(buildTarget)).Replace("\\", "/");
			#else
			var outputPath = Path.Combine(path, BaseLoader2.GetPlatformFolderForAssetBundles(buildTarget));
			#endif
            var folders = Directory.GetDirectories(outputPath, "*.*", SearchOption.AllDirectories);
            var listData = new List<object>();
            var listAssetBundleName = new List<string>();

            long totalSize = 0;

            foreach (var folderPath in folders)
            {
                var files = Directory.GetFiles(folderPath, "*.unity3d");
                foreach (var filePath in files)
                {
					#if !UNITY_IOS
                    var assetbundleName = filePath.Replace(outputPath + "\\", "").Replace("\\", "/");
					#else
					var assetbundleName = filePath.Replace(outputPath + "\\", "");
					#endif
                    if (listAssetBundleName.Contains(assetbundleName)) continue;

                    if (!File.Exists(filePath)) continue;
                    var fileInfo = new FileInfo(filePath);
                    var size = fileInfo.Length;

                    listData.Add(new Dictionary<string, object>()
                    {
                        {"assetbundle_name", assetbundleName},
                        {"size", size}
                    });

                    totalSize += size;

                    listAssetBundleName.Add(assetbundleName);
                }
            }
        }

        public const string FromPathExportSd = "0-AssetBundleSource/Resources";
        public const string ToPathExportSd = "0-AssetBundleSource/SD";

        public static void ExportSd()
        {
			#if !UNITY_IOS
            var from = (Application.dataPath + "/" + FromPathExportSd).Replace("/", "\\");
            var to = (Application.dataPath + "/" + ToPathExportSd).Replace("/", "\\");
            var logPath = to + "\\" + "logsd.txt";
			#else
			var from = (Application.dataPath + "/" + FromPathExportSd);
			var to = (Application.dataPath + "/" + ToPathExportSd);
			var logPath = to + "/" + "logsd.txt";
			#endif
            if (!Directory.Exists(to)) Directory.CreateDirectory(to);

            var lastLogText = File.Exists(logPath) ? File.ReadAllText(logPath) : "";
            var lastLogDic = new Dictionary<string, string[]>();

            if (!string.IsNullOrEmpty(lastLogText))
            {
                var logSplit = lastLogText.Split('\n');
                for (var i = 0; i < logSplit.Length; i++)
                {
                    if (i == 0) continue;
                    var logItems = logSplit[i].Split(':');
                    if (lastLogDic.ContainsKey(logItems[1]))
                    {
                        Debug.Log("id exists:: " + logItems[1]);
                        continue;
                    }

                    lastLogDic.Add(logItems[1], logItems);
                }
            }

            var logText = CopyFolderAndFile(from, to, from, ref lastLogDic);
            File.WriteAllText(logPath, logText);

            foreach (var lastLog in lastLogDic)
            {
                var type = lastLog.Value[0].ToInt();
                var lastLogPath = to + lastLog.Key;

                switch (type)
                {
                    case 0:
                        {
                            if (Directory.Exists(lastLogPath))
                                Directory.Delete(lastLogPath, true);
                            break;
                        }

                    case 1:
                        {
                            if (File.Exists(lastLogPath))
                                File.Delete(lastLogPath);
                            break;
                        }
                }
            }

            Debug.Log("EXPORT SD COMPLETED !!");
            AssetDatabase.Refresh();
        }

        private static string CopyFolderAndFile(string rootFrom, string rootTo, string from, ref Dictionary<string, string[]> lastLogDic, bool replaceMetaFile = false)
        {
            var logText = "";
            var ext = new string[] { "*.png", "*.jpg", "*.psd" };

            foreach (var searchPattern in ext)
            {
                var files = Directory.GetFiles(from, searchPattern);
                foreach (var file in files)
                {
                    var idFolder = from.Replace(rootFrom, "");
                    var folderPath = rootTo + idFolder;
                    if (!Directory.Exists(folderPath))
                    {
                        Directory.CreateDirectory(folderPath);

                        logText += "\n";
                        logText += "0:" + idFolder;
                    }

                    var id = file.Replace(rootFrom, "");
                    var signature = CreateSignatureFile(file);
                    var lastSignature = "";

                    logText += "\n";
                    logText += "1:" + id + ":" + signature;

                    if (lastLogDic.ContainsKey(id))
                    {
                        lastSignature = lastLogDic[id][2];
                        lastLogDic.Remove(id);
                    }

                    var filePath = rootTo + id;
                    if (lastSignature.Equals(signature)) continue;

                    File.Copy(file, filePath, true);

                    var isCopyMetaFile = (replaceMetaFile || !File.Exists(filePath));
                    if (isCopyMetaFile && File.Exists(file + ".meta"))
                    {
                        File.Copy(file + ".meta", filePath + ".meta", true);
                    }
                }
            }

            var folders = Directory.GetDirectories(from);
            foreach (var folder in folders)
            {
                var id = folder.Replace(rootFrom, "");
                if (lastLogDic.ContainsKey(id))
                    lastLogDic.Remove(id);

                logText += CopyFolderAndFile(rootFrom, rootTo, folder, ref lastLogDic);
            }

            return logText;
        }

        static string CreateSignatureFile(string path)
        {
            var fileInfo = new FileInfo(path);
            return fileInfo.LastWriteTime.ToString("ddMMyyyyhhmmss");
        }
    }

    public class AssetBundleInfo
    {
        public string Path;
        public string ExtFilter;
        public bool AllDirectories;
        public bool HasParentParam;
        public int ParentLevel = 1;

        public string AssetName;
        public string AssetBundleVariant;
        public int IsLocal = 0;
    }
}

