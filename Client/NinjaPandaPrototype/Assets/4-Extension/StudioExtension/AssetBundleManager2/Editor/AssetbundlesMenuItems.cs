﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEditor;

namespace Studio
{
    public class AssetbundlesMenuItems : EditorWindow
    {
        private const string kSimulateAssetBundlesMenu = "AssetBundles/Simulate AssetBundles";

        private static AssetbundlesMenuItems _myWindow;
        private static string _pathBuildBundle;

        private static bool _isBuildAndroid;
        private static bool _isBuildIos;
        private static bool _isBuildWindow;
        private static bool _isBuildWindowStore;
        private static bool _isBuildWebGL;
        private static BuildAssetBundleOptions _buildOption;
        private static bool _encyptAssetBundleName;
        private static bool _autoEncrypt;
        private static bool _autoExtractStreamingAsset;
        private static bool _isUseLowResource;

        private static string _outPathBuildBundle;
        private static bool _isEncryptBuildAndroid;
        private static bool _isEncryptBuildIos;
        private static bool _isEncryptBuildWindow;
        private static bool _isEncryptBuildWindowStore;
        private static bool _isEncryptBuildWebGL;

        private int _toolBarIndexSelected = 0;
        [MenuItem("HVL/Delete All Playerprefs")]
        public static void DeleteAllPlayerPrefs()
        {
            PlayerPrefs.DeleteAll();
            PlayerPrefs.Save();
        }

        [MenuItem("HVL/AssetBundles/Build All", false, 1)]
        public static void BuildAllAssetBundles()
        {
            ToolDataconfigEditor.AddAllPrefabResource();
            _pathBuildBundle = EditorPrefs.GetString("ab_path", "");
            _outPathBuildBundle = EditorPrefs.GetString("db_outPathBuildBundle", "");
			_isBuildAndroid = EditorPrefs.GetBool("ab_android", false);
            _isBuildIos = EditorPrefs.GetBool("ab_ios", false);
            _isBuildWindow = EditorPrefs.GetBool("ab_window", false);
            _isBuildWindowStore = EditorPrefs.GetBool("ab_windowstore", false);
            _isBuildWebGL = EditorPrefs.GetBool("ab_webGL", false);
            _buildOption = (BuildAssetBundleOptions)EditorPrefs.GetInt("ab_build_option", 0);
            _encyptAssetBundleName = EditorPrefs.GetBool("ab_encyptAssetBundleName", false);
            _autoEncrypt = EditorPrefs.GetBool("ab_autoEncrypt", false);
            _autoExtractStreamingAsset = EditorPrefs.GetBool("ab_autoExtractStreamingAsset", false);

            _myWindow = (AssetbundlesMenuItems) EditorWindow.GetWindow(typeof (AssetbundlesMenuItems));
            _myWindow.Show();
        }

        void OnGUI()
        {
            GUILayout.Space(10);

            _toolBarIndexSelected = GUILayout.Toolbar(_toolBarIndexSelected, new string[] {"Build", "Encrypt", "StreamingAsset"});
            switch (_toolBarIndexSelected)
            {
                case 0:
                {
                    DrawBuildGroup();
                    break;
                }

                case 1:
                {
                    DrawEncryptGroup();
                    break;
                }

                case 2:
                {
                    DrawStreamingAssetGroup();
                    break;
                }
            }
        }

        void DrawBuildGroup()
        {
            GUILayout.Space(10);
            EditorGUILayout.BeginHorizontal();
            _pathBuildBundle = EditorGUILayout.TextField("PathResource", _pathBuildBundle);
            if (GUILayout.Button("Brower", GUILayout.Width(100)))
            {
                _pathBuildBundle = EditorUtility.OpenFolderPanel("Chose folder", _pathBuildBundle, "");
            }
            EditorGUILayout.EndHorizontal();

			GUILayout.Space(10);
            _isBuildAndroid = EditorGUILayout.Toggle("Android", _isBuildAndroid);

            GUILayout.Space(10);
            _isBuildIos = EditorGUILayout.Toggle("IOS", _isBuildIos);

            GUILayout.Space(10);
            _isBuildWindow = EditorGUILayout.Toggle("Window", _isBuildWindow);

            GUILayout.Space(10);
            _isBuildWindowStore = EditorGUILayout.Toggle("Window Store", _isBuildWindowStore);

            GUILayout.Space(10);
            _isBuildWebGL = EditorGUILayout.Toggle("WebGL", _isBuildWebGL);

            GUILayout.Space(10);
            _buildOption = (BuildAssetBundleOptions)EditorGUILayout.EnumPopup("Build OPtion", _buildOption);

            GUILayout.Space(10);
            EditorGUILayout.BeginHorizontal();
            _encyptAssetBundleName = EditorGUILayout.Toggle("Encrypt AssetBundleName", _encyptAssetBundleName);
            _autoEncrypt = EditorGUILayout.Toggle("Auto Encrypt", _autoEncrypt);
            _autoExtractStreamingAsset = EditorGUILayout.Toggle("Auto ExtractStreamingAsset", _autoExtractStreamingAsset);
            EditorGUILayout.EndHorizontal();

            GUILayout.Space(10);
            _isUseLowResource = EditorGUILayout.Toggle("Use Low-Quality Resource", _isUseLowResource);

            GUILayout.Space(20);
            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("BUILD"))
            {
                if (string.IsNullOrEmpty(_pathBuildBundle))
                {
                    Debug.LogError("BUILD ASSETBUNDLE please input Build Path!!");
                    return;
                }

                var listBuild = new List<BuildTarget>();
                if (_isBuildAndroid) listBuild.Add(BuildTarget.Android);
                if (_isBuildIos) listBuild.Add(BuildTarget.iOS);
                if (_isBuildWindow) listBuild.Add(BuildTarget.StandaloneWindows);
                if (_isBuildWindowStore) listBuild.Add(BuildTarget.WSAPlayer);
                if (_isBuildWebGL) listBuild.Add(BuildTarget.WebGL);

                CacheEditorPref();

                BuildScript.BuildAssetBundles(_pathBuildBundle, listBuild, _buildOption, _encyptAssetBundleName,_isUseLowResource);

                if (_autoEncrypt) {
                    BuildScript.EncryptFile(_pathBuildBundle, _outPathBuildBundle, listBuild);
                }

                if (_autoExtractStreamingAsset) {
                    BuildScript.ExtractStreamingAsset(_outPathBuildBundle);
                }
            }

            if (GUILayout.Button("SAVE CONFIG"))
            {
                CacheEditorPref();
            }
           
            EditorGUILayout.EndHorizontal();
        }

        void DrawEncryptGroup()
        {
            GUILayout.Space(10);

            EditorGUILayout.BeginHorizontal();
            _pathBuildBundle = EditorGUILayout.TextField("PathResource", _pathBuildBundle);
            if (GUILayout.Button("Brower", GUILayout.Width(100)))
            {
                _pathBuildBundle = EditorUtility.OpenFolderPanel("Chose folder", _pathBuildBundle, "");
            }
            EditorGUILayout.EndHorizontal();

            GUILayout.Space(10);
            EditorGUILayout.BeginHorizontal();
            _outPathBuildBundle = EditorGUILayout.TextField("Encrypt PathResource", _outPathBuildBundle);
            if (GUILayout.Button("Brower", GUILayout.Width(100)))
            {
                _outPathBuildBundle = EditorUtility.OpenFolderPanel("Chose folder", _outPathBuildBundle, "");
            }
            EditorGUILayout.EndHorizontal();

            GUILayout.Space(10);
            _isEncryptBuildAndroid = EditorGUILayout.Toggle("Android", _isEncryptBuildAndroid);

            GUILayout.Space(10);
            _isEncryptBuildIos = EditorGUILayout.Toggle("IOS", _isEncryptBuildIos);

            GUILayout.Space(10);
            _isEncryptBuildWindow = EditorGUILayout.Toggle("Window", _isEncryptBuildWindow);

            GUILayout.Space(10);
            _isEncryptBuildWindowStore = EditorGUILayout.Toggle("Window Store", _isEncryptBuildWindowStore);

            GUILayout.Space(10);
            _isEncryptBuildWebGL = EditorGUILayout.Toggle("WebGL", _isEncryptBuildWebGL);

            GUILayout.Space(20);
            EditorGUILayout.BeginHorizontal();

            if (GUILayout.Button("ENCRYPT"))
            {
                CacheEditorPref();

                var listBuild = new List<BuildTarget>();
                if (_isEncryptBuildAndroid) listBuild.Add(BuildTarget.Android);
                if (_isEncryptBuildIos) listBuild.Add(BuildTarget.iOS);
                if (_isEncryptBuildWindow) listBuild.Add(BuildTarget.StandaloneWindows);
                if (_isEncryptBuildWindowStore) listBuild.Add(BuildTarget.WSAPlayer);
                if (_isEncryptBuildWebGL) listBuild.Add(BuildTarget.WebGL);

                BuildScript.EncryptFile(_pathBuildBundle, _outPathBuildBundle, listBuild);
                Application.OpenURL(_outPathBuildBundle);
            }

            EditorGUILayout.EndHorizontal();
        }

        private void DrawStreamingAssetGroup()
        {
            GUILayout.Space(10);
            EditorGUILayout.BeginHorizontal();
            _outPathBuildBundle = EditorGUILayout.TextField("Encrypt PathResource", _outPathBuildBundle);
            if (GUILayout.Button("Brower", GUILayout.Width(100)))
            {
                _outPathBuildBundle = EditorUtility.OpenFolderPanel("Chose folder", _outPathBuildBundle, "");
            }

            EditorGUILayout.EndHorizontal();

            GUILayout.Space(20);
            if (GUILayout.Button("EXTRACT CURRENT PLATFORM"))
            {
                CacheEditorPref();

                BuildScript.ExtractStreamingAsset(_outPathBuildBundle);
            }
        }

        void CacheEditorPref()
        {
            Debug.Log("EditorPref Saved !!");

            EditorPrefs.SetString("ab_path", _pathBuildBundle);
            EditorPrefs.SetString("db_outPathBuildBundle", _outPathBuildBundle);
            EditorPrefs.SetBool("ab_android", _isBuildAndroid);
            EditorPrefs.SetBool("ab_ios", _isBuildIos);
            EditorPrefs.SetBool("ab_window", _isBuildWindow);
            EditorPrefs.SetBool("ab_windowstore", _isBuildWindowStore);
            EditorPrefs.SetBool("ab_webGL", _isBuildWebGL);
            EditorPrefs.SetInt("ab_build_option", (int)_buildOption);
            EditorPrefs.SetBool("ab_encyptAssetBundleName", _encyptAssetBundleName);
            EditorPrefs.SetBool("ab_autoEncrypt", _autoEncrypt);
            EditorPrefs.SetBool("ab_autoExtractStreamingAsset", _autoExtractStreamingAsset);
        }

        [MenuItem("HVL/AssetBundles/GetAllAssetBundleBuild", false, 3)]
        public static void GetAllAssetBundleBuild()
        {
            //BuildScript.GetResourceList();
        }
    }
}
