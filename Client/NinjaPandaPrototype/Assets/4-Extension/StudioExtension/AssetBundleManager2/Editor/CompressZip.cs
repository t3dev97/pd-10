﻿using System.Linq;
using System.Text.RegularExpressions;
#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.IO;
using System.Collections;
using System.Collections.Generic;

public class CompressZip : ScriptableWizard
{
    public string AssetBundleOriginPath = "";
    public long FileZipSize = 10;

    [MenuItem("HVL/CompressAssetbundle")]
    static void CompressData()
    {
        ScriptableWizard.DisplayWizard("Compress Assetbundle", typeof(CompressZip), "Compress","Choose folder Assetbundle OS");
    }

    private void OnWizardOtherButton()
    {
        AssetBundleOriginPath = EditorUtility.OpenFolderPanel("Chose AssetBundle Origin Folder", Application.dataPath, "");
    }

    private void OnWizardCreate()
    {
        if (string.IsNullOrEmpty(AssetBundleOriginPath))
        {
            Debug.LogError("Need Choose folder before compress");
            return;
        }

        List<Sprite> lsSprite = new List<Sprite>();

        DirectoryInfo dirInfo = new DirectoryInfo(AssetBundleOriginPath);
        
        long limitFileSize = FileZipSize * 1024 * 1024;

        int errorCode = 1;

        long totalSize = 0;

        int childCount;

        string pathZipFile;

        string fileZipName;

        bool isFirstTime;

        int split;

        Dictionary<string, object> metadatazip = new Dictionary<string, object>();

        var listDataZip = new List<object>();

        string nameMetaFile = (dirInfo.FullName + "/" + dirInfo.Name + ".txt").Replace("\\", "/");

        if (File.Exists(nameMetaFile)) File.Delete(nameMetaFile);

        foreach (var folder in dirInfo.GetDirectories())
        {
            childCount = 0;

            pathZipFile = (folder.FullName + ".zip").Replace("\\", "/");

            fileZipName = folder.Name + ".zip";

            isFirstTime = false;

            split = 1;

            foreach (var fileInfo in folder.GetFiles("*.*", SearchOption.AllDirectories))
            {
                ++childCount;

                string pathFile = fileInfo.FullName.Replace("\\","/");

                string pathFileSave = (fileInfo.FullName.Substring(fileInfo.FullName.IndexOf(folder.Name))).Replace("\\", "/");
                
                if (!isFirstTime)
                {
                    isFirstTime = true;

                   // errorCode = lzip.compress_File(9, pathZipFile, pathFile, false, pathFileSave);
                }
                else
                {
                    //errorCode = lzip.compress_File(9, pathZipFile, pathFile, true, pathFileSave);
                }

                //if (errorCode < 0) Debug.Log("errorCode " + errorCode);

                FileInfo[] fileZipInfo = dirInfo.GetFiles(fileZipName, SearchOption.TopDirectoryOnly);

                if (fileZipInfo[0].Length > limitFileSize)
                {
                    //log data
                    listDataZip.Add(AddZipData(fileZipName, fileZipInfo[0].Length, childCount));

                    totalSize += fileZipInfo[0].Length;

                    isFirstTime = false;

                    ++split;

                    childCount = 0;

                    fileZipName = folder.Name + split + ".zip";

                    pathZipFile = (folder.FullName + split + ".zip").Replace("\\", "/");
                }
            }

            //log data
            FileInfo[] fileZipInfo2 = dirInfo.GetFiles(fileZipName, SearchOption.TopDirectoryOnly);
            if (fileZipInfo2.Length > 0)
            {
                listDataZip.Add(AddZipData(fileZipName, fileZipInfo2[0].Length, childCount));

                totalSize += fileZipInfo2[0].Length;
            }
        }

        childCount = 0;

        pathZipFile = (dirInfo.FullName + "/" + dirInfo.Name + ".zip").Replace("\\", "/");

        fileZipName = dirInfo.Name + ".zip";

        isFirstTime = false;

        foreach (var fileInfo in dirInfo.GetFiles("*.*", SearchOption.TopDirectoryOnly).Where(file => Regex.IsMatch(file.Name, @"^(.(?!zip))*$")))
        {
            ++childCount;

            string pathFile = fileInfo.FullName.Replace("\\", "/");

            string pathFileSave = (fileInfo.FullName.Substring(fileInfo.FullName.IndexOf(dirInfo.Name) + dirInfo.Name.Length + 1)).Replace("\\", "/");

            if (!isFirstTime)
            {
                isFirstTime = true;

                //errorCode = lzip.compress_File(9, pathZipFile, pathFile, false, pathFileSave);
            }
            else
            {
               // errorCode = lzip.compress_File(9, pathZipFile, pathFile, true, pathFileSave);
            }

            //if (errorCode < 0) Debug.Log("errorCode " + errorCode);
        }

        //log data
        FileInfo[] fileZipInfo3 = dirInfo.GetFiles(fileZipName, SearchOption.TopDirectoryOnly);
        if (fileZipInfo3.Length > 0)
        {
            listDataZip.Add(AddZipData(fileZipName, fileZipInfo3[0].Length, childCount));

            totalSize += fileZipInfo3[0].Length;
        }
        
        //log total size
        metadatazip.Add("data",listDataZip);

        metadatazip.Add("total_size", totalSize);

       // var data = MiniJSON.Json.Serialize(metadatazip);

       // File.WriteAllText(nameMetaFile, data);
    }

    public static Dictionary<string, object> AddZipData(string name, long size, int childCount)
    {
        Dictionary<string, object> data = new Dictionary<string, object>();

        data.Add("assetbundle_name", name);

        data.Add("size", size);

        data.Add("child_count", childCount);

        return data;
    }
}
#endif
