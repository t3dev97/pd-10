﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Studio
{
    public static class Utils
    {
		public static string GetGameOS()
		{
#if UNITY_ANDROID
			return "android";
#endif

#if UNITY_IOS
                return "ios";
#endif

#if UNITY_WEBPLAYER
                return "webplayer";
#endif

#if UNITY_WEBGL
                return "webgl";
#endif

#if UNITY_WP8 || UNITY_WP8_1 || UNITY_WSA
                return "wsa";
#endif
#if UNITY_STANDALONE
        return "execute"; // Mac, Windows or Linux
#endif
		}

        public static T Get<T>(this Dictionary<string, object> h, string key, bool useDefaultIfNotExist = true)
        {
            return Get<T, object>(h, key, useDefaultIfNotExist);
        }

        public static T Get<T, T1>(this Dictionary<string, T1> h, string key, bool useDefaultIfNotExist = true)
        {
            if (h == null)
            {
                return default(T);
            }

            if (!h.ContainsKey(key))
            {
                // DebugXX.Warn("Can't find key <" + key + ">");
                return default(T);
            }


            object value = h[key];

            if (value == null) return default(T);

            Type valueT = value.GetType();

            if (valueT == typeof(T)) return (T)value;

            if (CanCast(valueT, typeof(T)))
            {
                try
                {
#if (UNITY_WSA) && !UNITY_EDITOR
                return (T)Convert.ChangeType(value, typeof(T));
#else
                    return (T)Convert.ChangeType(value, Type.GetTypeCode(typeof(T)));
#endif
                }
                catch (Exception e)
                {
                    DebugX.LogWarning("cast value error with key <" + key + "> error:: " + e.Message);
                    return default(T);
                }
            }

            return default(T);
        }

        public static bool CanCast(this Type p_from, Type p_to)
        {
#if (UNITY_WSA) && !UNITY_EDITOR
            if (p_to.GetTypeInfo().IsAssignableFrom(p_from.GetTypeInfo())) { return true; }
            return (p_from.GetTypeInfo().IsPrimitive || p_from.GetTypeInfo().IsEnum) && (p_to.GetTypeInfo().IsPrimitive || p_to.GetTypeInfo().IsEnum);
#else
            if (p_to.IsAssignableFrom(p_from)) { return true; }
            return (p_from.IsPrimitive || p_from.IsEnum) && (p_to.IsPrimitive || p_to.IsEnum);
#endif
        }
        public static string GetDeviceUniqueIdentifier()
        {
            var id = "";

            //#if !UNITY_EDITOR
#if UNITY_IPHONE
#if UNITY_4_3 || UNITY_4_5 || UNITY_4_6 || UNITY_4_7
			id = iPhone.vendorIdentifier;
#else
			id = UnityEngine.iOS.Device.vendorIdentifier;
#endif
#elif UNITY_WEBGL || UNITY_EDITOR
            id = PlayerPrefs.GetString("deviceId");
#endif
            //#endif
            if (string.IsNullOrEmpty(id))
            {
#if UNITY_WEBGL
                id = Guid.NewGuid() + "_" + GenerateFakeVersionForWeb();
                PlayerPrefs.SetString("deviceId", id);
                PlayerPrefs.Save();
#else
                id = SystemInfo.deviceUniqueIdentifier;
#endif
            }
            return id;
        }

        public static bool IsDicNullOrEmpty(this Dictionary<string, object> dic, string exceptKey = "")
        {
            return (dic == null || dic.Count == 0 ||
                    (!string.IsNullOrEmpty(exceptKey) && dic.ContainsKey(exceptKey) && dic.Count == 1));
        }

        public static string DictionaryToJson(this Dictionary<string, object> data)
        {
            return MiniJSON.Json.Serialize(data);
        }

        public static void SetLayer(this GameObject go, string layerName)
        {
            if (go == null) return;

            go.layer = LayerMask.NameToLayer(layerName);

            var childCount = go.transform.childCount;
            for (var i = 0; i < childCount; i++)
            {
                var child = go.transform.GetChild(i);
                if (child != null)
                {
                    SetLayer(child.gameObject, layerName);
                }
            }
        }

        public static Dictionary<string, object> JsonToDictionary(this string json)
        {
            return MiniJSON.Json.Deserialize(json) as Dictionary<string, object>;
        }

        public static bool ToBool(this object value, bool defaultValue = false)
        {
            bool outValue;
            if (bool.TryParse(value.ToString(), out outValue))
            {
                return outValue;
            }

            return defaultValue;
        }

        public static bool ToBool(this int value)
        {
            if (value == 0) { return false; }

            return true;
        }

        public static Vector2 ToVector2(this string v)
        {
            var splits = v.Split(':');
            if (splits.Length < 2) return Vector3.zero;

            return new Vector2(splits[0].ToFloat(), splits[1].ToFloat());
        }

        public static Vector3 ToVector3(this string v)
        {
			if (string.IsNullOrEmpty(v))
				return Vector3.zero;
            var splits = v.Split(':');
            if (splits.Length < 3) return Vector3.zero;

            return new Vector3(splits[0].ToFloat(), splits[1].ToFloat(), splits[2].ToFloat());
        }

		public static Quaternion ToQuaternion(this string v)
		{
			if (string.IsNullOrEmpty(v))
				return Quaternion.Euler(0,0,0);
			var splits = v.Split(':');
			if (splits.Length < 3) return Quaternion.Euler(0,0,0);

			return new Quaternion(splits[0].ToFloat(), splits[1].ToFloat(), splits[2].ToFloat(), 0);
		}

		public static string Vector2ToString(this Vector2 v)
        {
            return v.x + ":" + v.y;
        }

        public static string Vector3ToString(this Vector3 v)
        {
            return v.x + ":" + v.y + ":" + v.z;
        }

        public static int ToInt(this object value, int defaultValue = 0)
        {
            int outValue;
            if (int.TryParse(value.ToString(), out outValue))
            {
                return outValue;
            }

            return defaultValue;
        }

        public static long ToLong(this object value, int defaultValue = 0)
        {
            int outValue;
            if (int.TryParse(value.ToString(), out outValue))
            {
                return outValue;
            }

            return defaultValue;
        }

        public static float ToFloat(this object value, float defaultValue = 0.0f)
        {
            float outValue;
            if (float.TryParse(value.ToString(), out outValue))
            {
                return outValue;
            }

            return defaultValue;
        }

        public static double ToDouble(this object value, float defaultValue = 0.0f)
        {
            float outValue;
            if (float.TryParse(value.ToString(), out outValue))
            {
                return outValue;
            }

            return defaultValue;
        }

        public static string GetTimeFormatHHmm(this int time)
        {
            var timeInt = Mathf.FloorToInt(time);
            if (time <= 0) return "00:00";

            var day = timeInt / 3600 / 24;
            var hour = timeInt / 3600 % 24 + day * 24;
            var minute = timeInt % 3600 / 60;

            return (hour <= 0 ? "00" : (hour < 10 ? "0" + hour : hour + "")) + ":" +
                   (minute <= 0 ? "00" : (minute < 10 ? "0" + minute : minute + ""));
        }

        public static int[] GetTimeFormatArr(this int time)
        {
            var timeInt = Mathf.FloorToInt(time);
            if (time <= 0) return new[] {0, 0, 0, 0};

            var day = timeInt/3600/24;
            var hour = timeInt/3600%24;
            var minute = timeInt%3600/60;
            var second = timeInt%60;

            return new[] {day, hour, minute, second};
        }
        public static int[] GetTimeFormatArr(this long time)
        {
            var timeInt = Mathf.FloorToInt(time);
            if (time <= 0) return new[] { 0, 0, 0, 0 };

            var day = timeInt / 3600 / 24;
            var hour = timeInt / 3600 % 24;
            var minute = timeInt % 3600 / 60;
            var second = timeInt % 60;

            return new[] { day, hour, minute, second };
        }
        public static string GetTimeFormatString(this int expValue)
        {
            var timeArr = expValue.GetTimeFormatArr();

            var day = timeArr[0];
            if (day > 0) return LanguageManager.api.GetKey(Studio.BC.BCLocalize.ID_GUI_ITEM_DAY_DURATION, new[] { day + "" });

            var hour = timeArr[1];
            if (hour > 0) return LanguageManager.api.GetKey(Studio.BC.BCLocalize.ID_GUI_ITEM_HOUR_DURATION, new[] { hour + "" });

            var minute = timeArr[2];
            if (minute > 0) return LanguageManager.api.GetKey(Studio.BC.BCLocalize.ID_GUI_ITEM_MINUTE_DURATION, new[] { minute + "" });

            var second = timeArr[3];
            if (second > 0) return LanguageManager.api.GetKey(Studio.BC.BCLocalize.ID_GUI_ITEM_SECOND_DURATION, new[] { second + "" });

            return "";
        }
        public static string GetTimeFormatString(this long expValue)
        {
            var timeArr = expValue.GetTimeFormatArr();

            var day = timeArr[0];
            if (day > 0) return LanguageManager.api.GetKey(Studio.BC.BCLocalize.ID_GUI_ITEM_DAY_DURATION, new[] { day + "" });

            var hour = timeArr[1];
            if (hour > 0) return LanguageManager.api.GetKey(Studio.BC.BCLocalize.ID_GUI_ITEM_HOUR_DURATION, new[] { hour + "" });

            var minute = timeArr[2];
            if (minute > 0) return LanguageManager.api.GetKey(Studio.BC.BCLocalize.ID_GUI_ITEM_MINUTE_DURATION, new[] { minute + "" });

            var second = timeArr[3];
            if (second > 0) return LanguageManager.api.GetKey(Studio.BC.BCLocalize.ID_GUI_ITEM_SECOND_DURATION, new[] { second + "" });

            return "";
        }
        public static string GenerateFakeVersionForWeb()
        {
            return "rnd=" + DateTime.Now.ToString("yyyyMMddHHmmsstt");
        }

        public static string ToMoneyFormat(this long val)
        {
            long value = val;
            bool isNegative = false;
            if (value < 0)
            {
                isNegative = true;
                value *= -1;
            }

            var result = value.ToString();
            var counter = result.Length;
            while (counter > 3)
            {
                counter -= 3;
                if (value < 0 && counter == 1)
                    return result;
                result = result.Insert(counter, ".");
            }
            if (isNegative)
            {
                result = "-" + result;
            }

            return result;
        }

        public static string ToMoneyFormat(this int val)
        {
            long value = val;
            bool isNegative = false;
            if (value < 0)
            {
                isNegative = true;
                value *= -1;
            }

            var result = value.ToString();
            var counter = result.Length;
            while (counter > 3)
            {
                counter -= 3;
                if (value < 0 && counter == 1)
                    return result;
                result = result.Insert(counter, ".");
            }
            if (isNegative)
            {
                result = "-" + result;
            }

            return result;
        }

        public static string ToQuantityFormat(this int val, bool isShowMark = false)
        {
            var result = "";
            if (isShowMark)
            {
                if (val >= 1000000)
                {
                    var tempquantity = (int)val / 1000000;
                    result = "x" + tempquantity.ToMoneyFormat() + "m";
                }
                else if (val >= 1000)
                {
                    var tempquantity = val / 1000;
                    result = "x" + tempquantity.ToMoneyFormat() + "k";
                }
                else
                    result = "x" + val.ToMoneyFormat();
            }
            else
            {
                if (val >= 1000000)
                {
                    var tempquantity = val / 1000000;
                    result = tempquantity.ToMoneyFormat() + "m";
                }
                else if (val >= 1000)
                {
                    var tempquantity = val / 1000;
                    result = tempquantity.ToMoneyFormat() + "k";
                }
                else
                    result = val.ToMoneyFormat();
            }
            return result;
        }
        public static string ToQuantityFormat(this long val, bool isShowMark = false)
        {
            var result = "";
            if (isShowMark)
            {
                if (val >= 1000000)
                {
                    var tempquantity = (long)val / 1000000;
                    result = "x" + tempquantity.ToMoneyFormat() + "m";
                }
                else if (val >= 1000)
                {
                    var tempquantity = val / 1000;
                    result = "x" + tempquantity.ToMoneyFormat() + "k";
                }
                else
                    result = "x" + val.ToMoneyFormat();
            }
            else
            {
                if (val >= 1000000)
                {
                    var tempquantity = val / 1000000;
                    result = tempquantity.ToMoneyFormat() + "m";
                }
                else if (val >= 1000)
                {
                    var tempquantity = val / 1000;
                    result = tempquantity.ToMoneyFormat() + "k";
                }
                else
                    result = val.ToMoneyFormat();
            }
            return result;
        }
        public static string GetMoneyFormatWithString(this int value, string signDefault = ".")
        {
            var result = value.ToString();

            if (result.Length < 4) { return result; }

            result = result.Substring(0, result.Length - 3);

            var counter = result.Length;
            while (counter > 3)
            {
                counter -= 3;
                result = result.Insert(counter, signDefault);
            }

            if(result.EndsWith(signDefault + "000"))
            {
                return result.Substring(0, result.Length - signDefault.Length - 3 ) + "M";
            }
            else
            {
                return result + "K";
            }
            
        }

        public static string GetMoneyFormatWithString(this long value, string signDefault = ".")
        {
            var result = value.ToString();

            if (result.Length < 4) { return result; }

            result = result.Substring(0, result.Length - 3);

            var counter = result.Length;
            while (counter > 3)
            {
                counter -= 3;
                result = result.Insert(counter, signDefault);
            }
            return result + "K";
        }

        public static int FromMoneyToInt(this string val)
        {
            string[] splited = val.Split('.');
            string str = "";
            int result;
            foreach (string s in splited)
            {
                str += s;
            }
            result = str.ToInt();
            return result;

        }

        public static Color ColorFromHex(this string hex)
        {
            if (string.IsNullOrEmpty(hex)) return Color.white;
            Color color;

            if (!ColorUtility.TryParseHtmlString(hex, out color))
            {
                color = Color.white;
            }

            return color;
        }
        //public static void ResetShaderAllChild(this GameObject go)
        //{
        //    if (go == null) return;
        //    ResetShaderAllChild(go.transform);
        //}

        //public static void ResetShaderAllChild(this Transform tran)
        //{
        //    ResetShader(tran);

        //    var tranCount = tran.childCount;
        //    if (tranCount == 0) return;

        //    for (var i = 0; i < tranCount; i++)
        //    {
        //        var child = tran.GetChild(i);
        //        if (child == null) continue;

        //        ResetShaderAllChild(child);
        //    }
        //}

        //public static void ResetShader(this Transform tran)
        //{
        //    var rd = tran.AddToggleToList<Renderer>();
        //    if (rd != null && rd.sharedMaterials != null)
        //    {
        //        foreach (var sharedMaterial in rd.sharedMaterials)
        //        {
        //            if (sharedMaterial != null)
        //            {
        //                sharedMaterial.shader = Shader.Find(sharedMaterial.shader.name);
        //            }
        //        }
        //    }

        //    var img = tran.AddToggleToList<Image>();
        //    if (img != null && img.material != null)
        //    {
        //        img.material.shader = Shader.Find(img.material.shader.name);
        //    }

        //    var spinGraphic = tran.AddToggleToList<Spine.Unity.SkeletonGraphic>();
        //    if (spinGraphic != null && spinGraphic.material != null)
        //    {
        //        spinGraphic.material.shader = Shader.Find(spinGraphic.material.shader.name);
        //    }
        //}

        public static void ClearAllChild(this Transform tfParent)
        {
            List<Transform> lstChild = new List<Transform>();
            if (tfParent == null)
                return;
            foreach (Transform tf in tfParent)
            {                
                lstChild.Add(tf);
            }

            for (int i = 0; i < lstChild.Count; i++)
            {
                GameObject.Destroy(lstChild[i].gameObject);
            }
        }

        public static void ClearAllChild(Transform tfParent, string childName)
        {
            foreach (Transform tf in tfParent)
            {
                if (tf.gameObject.name == childName)
                {
                    GameObject.Destroy(tf.gameObject);
                }
            }
        }

        public static void HideAllChild(Transform tfParent)
        {            
            foreach (Transform tf in tfParent)
            {
                tf.gameObject.SetActive(false);
            }
        }

        public static List<T> GetChildsWidthType<T>(this GameObject go) where T:Component
        {
            var listT = new List<T>();
            if (go == null) return listT;

             var t = go.GetComponent<T>();
            if(t != null) listT.Add(t);

            var tran = go.transform;
            var childCount = tran.childCount;
            
            for (var i = 0; i < childCount; i++)
            {
                listT.AddRange(GetChildsWidthType<T>(tran.GetChild(i).gameObject));
            }

            return listT;
        }

        public static void SetGroupSizeVertical(RectTransform rt)
        {
            GridLayoutGroup gridLayoutGroup = rt.GetComponent<GridLayoutGroup>();

            int totalData = rt.childCount;
            int totalRow = totalData / gridLayoutGroup.constraintCount;
            if (totalData % gridLayoutGroup.constraintCount != 0)
            {
                totalRow++;
            }

            float totalHeight = gridLayoutGroup.cellSize.y * totalRow + gridLayoutGroup.spacing.y * totalRow + 16f;

            rt.sizeDelta = new Vector2(rt.sizeDelta.x, totalHeight);

            rt.anchoredPosition = new Vector2(rt.anchoredPosition.x, 0);

        }

        public static void SetGroupSizeHorizal(RectTransform rt)
        {
            GridLayoutGroup gridLayoutGroup = rt.GetComponent<GridLayoutGroup>();

            int totalData = rt.childCount;
            int totalCol = totalData / gridLayoutGroup.constraintCount;
            if (gridLayoutGroup.constraintCount > 1)
            {
                if (totalData % gridLayoutGroup.constraintCount != 0)
                {
                    // totalCol++;
                }
            }


            float totalWidth = gridLayoutGroup.cellSize.x * totalCol + gridLayoutGroup.spacing.x * totalCol;

            if(totalCol > 1)
            {
                totalWidth -= gridLayoutGroup.spacing.x;
            }

            rt.sizeDelta = new Vector2(totalWidth, rt.sizeDelta.y);

            rt.anchoredPosition = new Vector2(0, rt.anchoredPosition.y);

        }

        public static void SetWebGLInput(bool isActive)
        {
#if !UNITY_EDITOR && UNITY_WEBGL
            WebGLInput.captureAllKeyboardInput = !isActive;
#endif
        }

        public static void OpenURL(string url)
        {
#if UNITY_WEBGL && !UNITY_EDITOR
            Application.ExternalEval(string.Format("window.open(\"{0}\",\"_blank\")", url));
#else
            Application.OpenURL(url);
#endif

        }

		public static string DeepTrace(this object p_obj, int p_level = 0)
		{
			string baseStr = Duplicate("\t", p_level);

			if (p_obj == null) { return "null"; }

			if (p_obj.GetType().IsPrimitive) { return p_obj.ToString(); }

			var list1 = p_obj as IList;
			if (list1 != null)
			{
				IList list = list1;
				string str = baseStr + "[";
				for (int i = 0; i < list.Count; i++)
				{
					str += (i > 0 ? ",\n" : "\n") + baseStr + "\t" + DeepTrace(list[i], p_level + 1);
				}
				return str + "]";
			}

			var dictionary = p_obj as IDictionary;
			if (dictionary != null)
			{
				IDictionary dict = dictionary;
				string str = baseStr + "{";
				bool first = true;

				foreach (DictionaryEntry item in dict)
				{
					str += (first ? "\n" : ",\n") + baseStr + "\t"
						   + (item.Key + ":" + DeepTrace(item.Value, p_level + 1));
					first = false;
				}

				return str + "}";
			}

			return baseStr + p_obj;
		}

		public static string Duplicate(this string p_str, int p_nTimes)
		{
			string result = "";
			for (int i = 0; i < p_nTimes; i++) { result += p_str; }
			return result;
		}

		public class SimpleEncryptDecrypt
		{
			static int tempTextLength = 0;
			// static string tempTextList = "0123456789abcdefghijklmnopqABCDEFGHIJKLMNOPQRSTUVWXYZ";
			static string tempTextBegin = "poklhbmnqweopk";
			static int deep = 0;

			public static string Encrypt(string text)
			{
				byte[] bytesToEncode = Encoding.UTF8.GetBytes(text);
				string encodedText = Convert.ToBase64String(bytesToEncode);
				string tempText = "";
				for (int i = 0; i < tempTextLength; i++)
				{
					// tempText += tempTextList[PKUtils.randomInstance.Next(0, tempTextList.Length - 1)];
				}

				// encodedText = encodedText.Substring(0, encodedText.Length - 1);

				encodedText = encodedText + tempText;

				encodedText = SimpleEncryptDecrypt.Encrypt2(encodedText);

				encodedText = tempTextBegin + encodedText;

				return encodedText;

			}

			static string Encrypt2(string encodedText)
			{
				for (int i = 0; i < deep; i++)
				{
					byte[] bytesToEncode = Encoding.UTF8.GetBytes(encodedText);
					encodedText = Convert.ToBase64String(bytesToEncode);
				}


				return encodedText;

			}

			public static string Decrypt(string encodedText)
			{
				if (encodedText.StartsWith(tempTextBegin) == false)
				{
					return encodedText;
				}

				encodedText = encodedText.Substring(tempTextBegin.Length, encodedText.Length - tempTextBegin.Length);

				encodedText = SimpleEncryptDecrypt.Decrypt2(encodedText);

				// encodedText = encodedText.Substring(tempTextBegin.Length, encodedText.Length - tempTextBegin.Length);
				encodedText = encodedText.Substring(0, encodedText.Length - tempTextLength);
				// encodedText += "=";

				byte[] decodedBytes = Convert.FromBase64String(encodedText);
				string decodedText = Encoding.UTF8.GetString(decodedBytes, 0, decodedBytes.Length);

				return decodedText;
			}

			static string Decrypt2(string decodedText)
			{
				for (int i = 0; i < deep; i++)
				{
					byte[] decodedBytes = Convert.FromBase64String(decodedText);
					decodedText = Encoding.UTF8.GetString(decodedBytes, 0, decodedBytes.Length);
				}

				return decodedText;

			}

			public static bool HasEncrypt(string encodedText)
			{
				return encodedText.StartsWith(tempTextBegin);
			}
		}
	}
}

