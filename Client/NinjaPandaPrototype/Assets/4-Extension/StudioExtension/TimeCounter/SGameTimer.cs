﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Studio.Timer
{
    public delegate void STimeCounterCallback(TimerResult timerResult);

    public class Async
    {
        public static void Call(Action callback)
        {
            (new TimeCounter().Init(0, 1, callback, true)).Start();
        }

        public static void Call(Action callback, float timeDelay)
        {
            (new TimeCounter().Init(timeDelay, 1, callback)).Start();
        }
    }

    public class SGameTimer : MonoBehaviour
    {
        private static SGameTimer _api;
        public static SGameTimer Api
        {
            get
            {
                if (_api == null)
                {
                    var go = new GameObject("_SGameTimer");
                    _api = go.AddComponent<SGameTimer>();
                    DontDestroyOnLoad(go);
                }

                return _api;
            }
        }

        private readonly List<TimeCounter> _callbackList = new List<TimeCounter>();

        void Update()
        {
            foreach (var timeCounter in _callbackList)
            {
                var distance = Time.realtimeSinceStartup - timeCounter.StartTime;
                var time = Mathf.FloorToInt(distance / timeCounter.TimeDelay);
                
                if (!timeCounter.IsPause) {

                    if (timeCounter.IsCallFirst) {
                        timeCounter.IsCallFirst = false;
                        timeCounter.Call();
                        continue;
                    }

                    for (var i = 0; i < time; i++) {
                        timeCounter.Call();
                    }
                }

                if (time > 0) {
                    timeCounter.ResetStartTime();
                }
            }

            CheckStop();
        }

        public void Call(TimeCounter timeCounter)
        {
            _callbackList.Add(timeCounter);
        }

        void CheckStop()
        {
            for (var i = 0; i < _callbackList.Count;) {
                if (!_callbackList[i].IsAlive) {
                    _callbackList.RemoveAt(i);
                }
                else i++;
            }
        }
    }

    public class TimeCounter
    {
        public STimeCounterCallback Callback
        {
            get; private set;
        }

        public Action CallbackSingle;

        public float TimeDelay
        {
            get; private set;
        }

        public bool IsAlive
        {
            get; private set;
        }

        public bool IsPause
        {
            get; private set;
        }

        public float StartTime
        {
            get; private set;
        }

        public bool IsRunning
        {
            get; private set;
        }

        public TimerResult Result
        {
            get; private set;
        }

        public bool IsCallFirst { get; internal set; }

        public TimeCounter Init(float timeDelay, STimeCounterCallback callback, bool isCallFirst = false)
        {
            if (timeDelay <= 0) {
                DebugX.LogError("TimeCounter: timeDelay must be larger than 0");
                return this;
            }

            TimeDelay   = timeDelay;
            Callback    = callback;
            IsAlive     = true;
            Result      = new TimerResult();
            IsCallFirst = isCallFirst;

            return this;
        }
        
        public TimeCounter Init(float timeDelay, int maxTimeCount, STimeCounterCallback callback, bool isCallFirst = false)
        {
            var init = Init(timeDelay, callback, isCallFirst);
            if (init != null) {
                Result.MaxTimeCount = maxTimeCount;
            }

            return init;
        }

        public TimeCounter Init(float timeDelay, int maxTimeCount, Action callback, bool isCallFirst = false)
        {
            if (timeDelay <= 0)
            {
                DebugX.LogError("TimeCounter: timeDelay must be larger than 0");
                return this;
            }

            TimeDelay = timeDelay;
            CallbackSingle = callback;
            IsAlive = true;
            Result = new TimerResult();
            IsCallFirst = isCallFirst;
            Result.MaxTimeCount = maxTimeCount;

            return this;
        }

        public TimeCounter Init(MonoBehaviour monoBehaviour, float timeDelay, STimeCounterCallback callback, bool isCallFirst = false)
        {
            var init = Init(timeDelay, callback, isCallFirst);
            if (init != null) {
                Result.SetMonoBehaviour(monoBehaviour);
            }

            return init;
        }

        public TimeCounter Init(MonoBehaviour monoBehaviour, float timeDelay, int maxTimeCount, STimeCounterCallback callback, bool isCallFirst = false)
        {
            var init = Init(timeDelay, callback, isCallFirst);
            if (init != null)
            {
                Result.MaxTimeCount = maxTimeCount;
                Result.SetMonoBehaviour(monoBehaviour);
            }

            return init;
        }

        public TimeCounter SetData(TimerResult data)
        {
            Result = data;
            Result.TimeDelay = TimeDelay;
            return this;
        }

        public void Start()
        {
            StartTime           = Time.realtimeSinceStartup;
            Result.TimeDelay    = TimeDelay;
            Result.StartTime    = StartTime;
            Result.TimeCount    = 0;
            IsRunning           = true;
            IsAlive             = true;

            SGameTimer.Api.Call(this);
        }

        public void Stop()
        {
            IsAlive     = false;
            IsRunning   = false;
        }

        public void Pause()
        {
            IsPause = true;
        }

        public void Resume()
        {
            IsPause = false;
        }

        bool CanCallback()
        {
            if (Callback == null) return false;
            if (Result == null) return false;
            if (Result.BehaviourIsNull()) return false;

            return IsAlive;
        }

        internal void Call()
        {
            Result.TimeCount += 1;

            if (CanCallback())
            {
                if (Callback != null) Callback(Result);
                if (CallbackSingle != null) CallbackSingle();
            }

            //check max time count
            if (Result.TimeCount >= Result.MaxTimeCount && Result.MaxTimeCount > 0) {
                IsAlive = false;
            }
        }

        internal void ResetStartTime()
        {
            StartTime = Time.realtimeSinceStartup;
        }
    }

    public class TimerResult
    {
        public float TimeDelay { get; internal set; }
        public int TimeCount { get; internal set; }

        public int MaxTimeCount { get; set; }

        public MonoBehaviour Behaviour { get; internal set; }

        private bool _hasMono;

        public TimerResult()
        {
            TimeCount = 0;
            _hasMono = false;
        }

        public TimerResult(float timeDelay, MonoBehaviour behaviour)
        {
            TimeDelay = timeDelay;
            Behaviour = behaviour;
            TimeCount = 0;

            _hasMono = true;
        }

        public void SetMonoBehaviour(MonoBehaviour behaviour)
        {
            Behaviour = behaviour;
            _hasMono = true;
        }

        public float Duration
        {
            get { return Time.realtimeSinceStartup - StartTime; }
        }

        internal bool BehaviourIsNull()
        {
            if (_hasMono)
            {
                if (Behaviour == null) return true;
                if (Behaviour.gameObject == null) return true;
            }

            return false;
        }

        internal float StartTime;
    }

    public static class STimerUtil
    {
        public static void StartTimeCounter(this MonoBehaviour monoBehaviour, TimeCounter timeCounter)
        {
            if (timeCounter == null) {
                //DebugX.LogError(monoBehaviour.name + ": TimeCounter is null !!");
                return;
            }

            timeCounter.Result.SetMonoBehaviour(monoBehaviour);
            timeCounter.Start();
        }

        public static void StopTimeCounter(this MonoBehaviour monoBehaviour, TimeCounter timeCounter)
        {
            if (timeCounter == null) {
                //DebugX.LogError(monoBehaviour.name + ": TimeCounter is null !!");
                return;
            }

            timeCounter.Stop();
        }
    }
}

