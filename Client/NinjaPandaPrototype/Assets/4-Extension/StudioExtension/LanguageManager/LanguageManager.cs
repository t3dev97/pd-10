﻿#region

using System;
using System.Collections.Generic;
using UnityEngine;

#endregion

namespace Studio {
    public class LanguageManager {
        static LanguageManager _api;
        public static LanguageManager api { get { return _api ?? (_api = new LanguageManager()); } }

        public Action OnLanguageChanged;

        private Dictionary<string, Dictionary<string, string>> m_dict;
        private Dictionary<string, string> m_token;
        //todo: test
        private string m_locale = "vn";

        private LanguageManager(){}

        public bool hasAnyLocale {
            get { return m_dict != null && m_dict.Count > 0; }
        }

        public string GetLocale() { return m_locale;}

        public bool ContainsKeyLocale(string key) {
            return m_dict.ContainsKey(key);
        }

        public LanguageManager AddLocale(string p_locale, string p_data, bool p_setAsCurrent = false)
        {
            //if (m_dict == null)
            //{
                m_dict = new Dictionary<string, Dictionary<string, string>>();
            //}
            if (m_dict.ContainsKey(p_locale))
            {
                DebugX.LogWarning("LanguageManager.AddLocale() ErrorString - locale <" + p_locale
                                 + "> already added - skipping");
                return this;
            }
            if (m_token == null)
                m_token = new Dictionary<string, string>();
            var localeDict = new Dictionary<string, string>();
            int idx = 0;
            int l1 = p_data.Length;
            var checkN = true;
            var checkR = true;
            var nIdx = -1;
            var rIdx = -1;
            while (idx < l1) {
                if (checkN && nIdx ==-1)
                {
                    nIdx = p_data.IndexOf("\n", idx + 1, StringComparison.Ordinal);
                    checkN = nIdx != -1;
                }
                if (checkR && rIdx ==-1)
                {
                    rIdx = p_data.IndexOf("\r", idx + 1, StringComparison.Ordinal);
                    checkR = rIdx != -1;
                }
                int eol = checkN ? checkR ? Mathf.Min(rIdx, nIdx) : nIdx : rIdx;
                if (eol == rIdx)
                { //useR
                    rIdx = -1;
                }
                else
                { //useN
                    nIdx = -1;
                }
                if (eol == -1)
                    eol = l1;

                string str = p_data.Substring(idx, eol - idx).Trim();
                //string str = p_data.Substring(idx, eol - idx);
                idx = eol + 1;
                if ((!string.IsNullOrEmpty(str)) && //ignore empty tokens
                    (str.Length > 2) && (str.Substring(0, 2) != "//")) //ignore line comments
                {
                    var isToken = str.IndexOf("TOKEN==>") == 0;
                    if (isToken) str = str.Replace("TOKEN==>","");
                    int eqIdx = str.IndexOf("==");
                    if (eqIdx != -1) {
                        string key = str.Substring(0, eqIdx).Trim();
                        string value = ReplaceSpecialToken(str.Substring(eqIdx + 2, str.Length - (eqIdx + 2)).Trim());
                        if (isToken) {
                            if (m_token.ContainsKey(key)) {
                                DebugX.LogWarning("Duplicated token<" + key + "> found with value <" + value + "> in locale <" + p_locale + ">");
                                continue;
                            }
                            m_token.Add(key, value);
                        } else {
                            if (localeDict.ContainsKey(key)) {
                                DebugX.LogWarning("Duplicated key<" + key + "> found with value <" + value + "> in locale <" + p_locale + ">");
                                continue;
                            }
                            localeDict.Add(key, value);
                        }
                    }
                } else {
                    //DebugX.DebugX.Warn("skipped ... eol=" + eol + " : idx=" + idx + " str=" + str);
                }
            }

            //DebugX.DebugX.Log("End at : "+ Turn.realtimeSinceStartup);
            if (p_setAsCurrent) { m_locale = p_locale; }
            m_dict.Add(p_locale, localeDict);
            return this;
        }

        private string ReplaceSpecialToken(string value)
        {
            var v = value;
            if (value.IndexOf(@"{\") != -1) {
                v = value.Replace(@"{\n}", "\n");
            }
            return v.Replace("\\n", "\n");
        }
        
        public string[] GetKeysWithPrefix(string prefix, string p_locale = null) {
            var result = new List<string>();
            if (m_dict == null) return result.ToArray();

            var keys = m_dict.Keys.GetEnumerator();
            keys.MoveNext();
            string firstLocal = keys.Current;

            string key = (string.IsNullOrEmpty(p_locale)) ? firstLocal : p_locale;
            Dictionary<string, string> locContent = m_dict[key];
            foreach (var item in locContent) {
                if(item.Key.StartsWith(prefix))
                    result.Add(item.Key);
            }
            return result.ToArray();
        }
       
        public string GetKey(string p_key) { return GetKeyLocale(m_locale, p_key); }

        public string GetKey(string p_key, List<string> tokens)
        {
            string[] strTokens = new string[tokens.Count];
            for (int i = 0; i < tokens.Count; i++)
            {
                strTokens[i] = tokens[i].ToString();
            }

            return GetKey(p_key, strTokens, null);
        }

        public string GetKey(string p_key, List<object> tokens)
        {
            if (tokens == null) { return GetKey(p_key); } //{ tokens = new List<object>(); }

            if (tokens.Count == 0) { return GetKey(p_key); }

            string[] strTokens = new string[tokens.Count];
            for(int i= 0;i<tokens.Count;i++)
            {
                strTokens[i] = tokens[i].ToString();
            }

            return GetKey(p_key, strTokens, null);
        }

        public string GetKey(string p_key, string[] tokens, Dictionary<string, object> vars = null) {

            if (m_token == null) m_token = new Dictionary<string, string>();

            var str = GetKey(p_key.Trim());
            var fIdx = str.IndexOf('{');

            if (tokens == null) tokens = new string[0];
            if (vars == null) vars = new Dictionary<string, object>();

            var eIdx = -1;
            var cnt = 0;
            var tmp = "";

            while (fIdx >= 0) {
                tmp += str.Substring(eIdx+1, fIdx-(eIdx+1));
                eIdx = str.IndexOf('}', fIdx + 1);
                if (eIdx == -1) {
                    DebugX.LogWarning("ErrorString - Open token { found but no corresponding close token } being found in with key=" + p_key.Trim()+ " <" + str +">");
                    break;
                }

                var tk = str.Substring(fIdx+1, eIdx - fIdx-1);

                //DebugX.DebugX.Log("Token ---> " + tk);

                if (vars.ContainsKey(tk)) 
                {
                    tmp += vars[tk];
                } 
                else if (tokens.Length > cnt) 
                {
                    if (m_token.ContainsKey(tk)) 
                    { //Token defined in localize file
                        tmp += GetKey(m_token[tk] + tokens[cnt]);
                    } 
                    else 
                    { // Token replace in position
                        if (tk == "TP") tk = "T|m";
                        if (tk == "TG") tk = "T|h";

                        var tmp2 = tokens[cnt];
                        /*
                        var vals = tk.Split('|');
                        switch (vals[0]) { //special variables
                            //SetData time
                            case "T"    : tmp2 = int.SetData(tmp2).ToTimeFormat(vals[1]); break;
                        }*/

                        tmp += tmp2;
                    }

                    cnt++;
                }

                fIdx = str.IndexOf('{', eIdx + 1);
            }

            tmp += str.Substring(eIdx+1, str.Length - eIdx -1);

            return tmp;
        }

        public string GetKeyLocale(string p_locale, string p_key) 
        {
            if (string.IsNullOrEmpty(p_locale)) {
                DebugX.LogWarning("LanguageManager.GetKey() ErrorString : p_locale should not be null, please set a default language (locale)");
                return p_key;
            }

            if (string.IsNullOrEmpty(p_key)) {
                DebugX.LogWarning("LanguageManager.GetKey() ErrorString : p_key should not be null");
                return p_key;
            }

            if(m_dict == null)
            {
                DebugX.LogWarning("LanguageManager.GetKey() ErrorString : m_dict should not be null, you must init it before use this feature");
                return p_key;
            }

            p_key = p_key.Trim();

            Dictionary<string, string> locale = m_dict[p_locale];
            if (locale != null && locale.ContainsKey(p_key)) { return locale[p_key]; }

            DebugX.LogWarning("LanguageManager.GetKey() ErrorString : key<" + p_key + "> is missing in locale <" + m_locale + ">");
            return p_key;
        }
       
        public void SetLocale(string p_locale) {
            if (m_dict != null && m_dict.ContainsKey(p_locale)) {
                m_locale = p_locale;
                if (OnLanguageChanged != null) OnLanguageChanged();
                return;
            }

            DebugX.LogWarning("LanguageManager.SetLocale() ErrorString - locale<" + p_locale + "> not found");
        }
    }
}
