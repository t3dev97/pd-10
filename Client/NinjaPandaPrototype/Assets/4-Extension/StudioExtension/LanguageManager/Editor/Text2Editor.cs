﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEditor.UI;
using System.Collections;

namespace Studio
{
    [CustomEditor(typeof (Text2), true)]
    [CanEditMultipleObjects]
    public class Text2Editor : UnityEditor.UI.TextEditor
    {
        private string _tokenTemp;

        public override void OnInspectorGUI()
        {
            var targetObj = target as Text2;
            if (targetObj != null)
            {
                GUILayout.Space(10);

                targetObj.AutoChangeLocalize = EditorGUILayout.Toggle("AutoChangeLocalize", targetObj.AutoChangeLocalize);
                if (targetObj.AutoChangeLocalize)
                {
                    GUILayout.Space(1);

                    targetObj.LocalizeId = EditorGUILayout.TextField("LocalizeId", targetObj.LocalizeId);

                    GUILayout.Space(1);

                    var index = -1;
                    if (targetObj.Tokens == null) targetObj.Tokens = new List<string>();
                    for (var i = 0; i < targetObj.Tokens.Count; i++)
                    {
                        EditorGUILayout.BeginHorizontal();
                        targetObj.Tokens[i] = EditorGUILayout.TextField("Token " + i, targetObj.Tokens[i]);

                        var indexTemp = i;
                        if (GUILayout.Button("-", GUILayout.Width(50)))
                        {
                            targetObj.Tokens.RemoveAt(indexTemp);
                        }
                        EditorGUILayout.EndHorizontal();
                        index = i;

                        GUILayout.Space(1);
                    }

                    GUILayout.Space(2);
                    EditorGUILayout.BeginHorizontal();

                    _tokenTemp = EditorGUILayout.TextField("Token " + (index + 1), _tokenTemp);
                    if (GUILayout.Button("+", GUILayout.Width(50)))
                    {
                        targetObj.Tokens.Add(_tokenTemp);
                        _tokenTemp = "";
                    }
                    EditorGUILayout.EndHorizontal();

                    GUILayout.Space(1);

                    targetObj.subfixText = EditorGUILayout.TextField("subfixText", targetObj.subfixText);
                }

                GUILayout.Space(5);
            }

            base.OnInspectorGUI();
        }
    }
}
