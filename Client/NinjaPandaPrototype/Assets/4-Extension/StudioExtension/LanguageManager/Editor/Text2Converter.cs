﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace Studio
{
    public static class Text2Converter
    {
        [MenuItem("GameObject/UI/Text2", false, 1)]
        static void AddText2()
        {
            const string name = "Text2";
            var goSelected = Selection.gameObjects;
            if (goSelected != null && goSelected.Length > 0)
            {
                var parent = goSelected[0].transform;
                var go = CreateGo<Text2>(parent, name);
                var goText = go.transform.GetComponent<Text2>();
                goText.text = "New Text2";
            }
            else
            {
                var canvas = GameObject.FindObjectOfType<Canvas>();
                if (canvas != null)
                {
                    var go = CreateGo<Text2>(canvas.transform, name);
                    var goText = go.transform.GetComponent<Text2>();
                    goText.text = "New Text2";
                }
            }
        }

        [MenuItem("GameObject/UI/Convert/ToText2", false, 3)]
        static void ConvertToText2()
        {
            var goSelected = Selection.gameObjects;
            if (goSelected != null && goSelected.Length > 0)
            {
                foreach (var gameObject in goSelected)
                {
                    ConvertTextToText2Action(gameObject.transform);
                }
            }
        }

        [MenuItem("GameObject/UI/Convert/ToText", false, 5)]
        static void ConvertText22Text()
        {
            var goSelected = Selection.gameObjects;
            if (goSelected != null && goSelected.Length > 0)
            {
                foreach (var gameObject in goSelected)
                {
                    ConvertText2ToTextAction(gameObject.transform);
                }
            }
        }

        static string pathToObject = "";

        [MenuItem("GameObject/GetPath", false, 4)]
        static void GetPathObject()
        {
            pathToObject = "";
            var goSelected = Selection.gameObjects;
            if (goSelected != null)
            {
                if (goSelected.Length == 1)
                {
                    GetParent(goSelected[0]);
                    Debug.Log(pathToObject);
                    pathToObject = "";
                }
                else
                {
                    Debug.Log("You must select one object");
                }
            }
        }

        static void GetParent(GameObject gObj)
        {
            if (gObj != null)
            {
                if (gObj.transform.parent)
                    GetParent(gObj.transform.parent.gameObject);
                pathToObject = pathToObject + "/" + gObj.name;
            }
            else
            {
                return;
            }
        }

        static void ConvertTextToText2Action(Transform child)
        {
            if (child == null) return;

            var text = child.GetComponent<Text>();
            var text2Temp = child.GetComponent<Text2>();

            if (text2Temp != null || text == null) return;

            var t = text.text;
            var f = text.font;
            var fstyle = text.fontStyle;
            var fsize = text.fontSize;
            var lineSpacing = text.lineSpacing;
            var richText = text.supportRichText;
            var alligment = text.alignment;
            var horiz = text.horizontalOverflow;
            var verti = text.verticalOverflow;
            var bestFit = text.resizeTextForBestFit;
            var minSize = text.resizeTextMinSize;
            var maxSize = text.resizeTextMaxSize;
            var color = text.color;
            var material = text.material;

            GameObject.DestroyImmediate(text);

            var text2 = child.gameObject.AddComponent<Text2>();

            text2.text = t;
            text2.font = f;
            text2.fontStyle = fstyle;
            text2.fontSize = fsize;
            text2.lineSpacing = lineSpacing;
            text2.supportRichText = richText;
            text2.alignment = alligment;
            text2.horizontalOverflow = horiz;
            text2.verticalOverflow = verti;
            text2.resizeTextForBestFit = bestFit;
            text2.resizeTextMinSize = minSize;
            text2.resizeTextMaxSize = maxSize;
            text2.color = color;
            text2.material = material;
        }

        static void ConvertText2ToTextAction(Transform child)
        {
            if (child == null) return;

            var text2 = child.GetComponent<Text2>();
            if (text2 == null) return;

            var t = text2.text;
            var f = text2.font;
            var fstyle = text2.fontStyle;
            var fsize = text2.fontSize;
            var lineSpacing = text2.lineSpacing;
            var richText = text2.supportRichText;
            var alligment = text2.alignment;
            var horiz = text2.horizontalOverflow;
            var verti = text2.verticalOverflow;
            var bestFit = text2.resizeTextForBestFit;
            var minSize = text2.resizeTextMinSize;
            var maxSize = text2.resizeTextMaxSize;
            var color = text2.color;
            var material = text2.material;

            GameObject.DestroyImmediate(text2);

            var text = child.gameObject.AddComponent<Text>();

            text.text = t;
            text.font = f;
            text.fontStyle = fstyle;
            text.fontSize = fsize;
            text.lineSpacing = lineSpacing;
            text.supportRichText = richText;
            text.alignment = alligment;
            text.horizontalOverflow = horiz;
            text.verticalOverflow = verti;
            text.resizeTextForBestFit = bestFit;
            text.resizeTextMinSize = minSize;
            text.resizeTextMaxSize = maxSize;
            text.color = color;
            text.material = material;
        }

        static GameObject CreateGo<T>(Transform parent, string name) where T : Component
        {
            var newGo = new GameObject(name);
            newGo.transform.SetParent(parent);
            newGo.transform.localPosition = Vector3.zero;
            newGo.transform.localScale = Vector3.one;
            newGo.AddComponent<T>();
            return newGo;
        }
    }
}

