﻿using Studio;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

[RequireComponent(typeof(TextMeshProUGUI))]
public class TextMeshPro2 : MonoBehaviour
{
    public string LocalizeId;

    [Header("Setup Shadow")]
    public bool ShadowDrop = false;
    public Vector3 OffsetPosition;

    public bool AutoChangeLocalize = true;
    [HideInInspector] public List<string> Tokens;
    [HideInInspector] public string subfixText = "";
    [HideInInspector] public string alternativeText = "";

    protected void OnEnable()
    {
        if (Application.isPlaying)
        {

            if (AutoChangeLocalize)
            {
                LanguageManager.api.OnLanguageChanged += OnLanguageChanged;
                OnLanguageChanged();
            }
        }
    }
    protected void OnDisable()
    {
        if (Application.isPlaying)
        {
            if (AutoChangeLocalize) LanguageManager.api.OnLanguageChanged -= OnLanguageChanged;
        }
    }
    private void Start()
    {
        if (ShadowDrop)
        {
            ShadowDrop = false;

            // shadow
            var shadow = Instantiate(this.gameObject);
            shadow.transform.SetParent(this.transform);
            shadow.transform.localScale = new Vector3(1, 1, 1);
            shadow.AddComponent<TextMeshPro2>();
            shadow.GetComponent<TextMeshPro2>().SetLocalize(LocalizeId);
            shadow.GetComponent<TextMeshProUGUI>().color = Color.black;
            shadow.name = this.name + "_shadow";
            shadow.transform.position += OffsetPosition;

            shadow.GetComponent<RectTransform>().anchorMin = new Vector2(0, 0);
            shadow.GetComponent<RectTransform>().anchorMax = new Vector2(1, 1);
            shadow.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0.5f);
            shadow.GetComponent<RectTransform>().right = Vector3.zero;
            shadow.GetComponent<RectTransform>().offsetMin = new Vector2(0, -OffsetPosition.y); // top 
            shadow.GetComponent<RectTransform>().offsetMax = new Vector2(0, OffsetPosition.y);  // bottom

            // main
            var main = Instantiate(this.gameObject);
            main.transform.SetParent(this.transform);
            main.transform.localScale = new Vector3(1, 1, 1);
            main.AddComponent<TextMeshPro2>();
            main.GetComponent<TextMeshPro2>().SetLocalize(LocalizeId);
            main.GetComponent<TextMeshProUGUI>().color = Color.white;
            main.name = this.name + "_main";

            main.GetComponent<RectTransform>().anchorMin = new Vector2(0, 0);
            main.GetComponent<RectTransform>().anchorMax = new Vector2(1, 1);
            main.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0.5f);
            main.GetComponent<RectTransform>().right = Vector3.zero;
            main.GetComponent<RectTransform>().offsetMin = new Vector2(0, 0);
            main.GetComponent<RectTransform>().offsetMax = new Vector2(0, 0);

            GetComponent<TextMeshProUGUI>().enabled = false;
        }
    }
    public void OnLanguageChanged()
    {
        if (string.IsNullOrEmpty(LocalizeId)) return;
        GetComponent<TextMeshProUGUI>().text = (Tokens == null || Tokens.Count == 0)
            ? LanguageManager.api.GetKey(LocalizeId)
            : LanguageManager.api.GetKey(LocalizeId, Tokens.ToArray(), null);

        GetComponent<TextMeshProUGUI>().text += subfixText;
    }

    public void SetLocalize(string key, string[] tokens = null)
    {
        LocalizeId = key;
        Tokens = (tokens == null) ? Tokens : tokens.ToList();
        //Debug.Log(GetComponent<TextMeshProUGUI>() == null);
        if (GetComponent<TextMeshProUGUI>() != null)
            GetComponent<TextMeshProUGUI>().text = (Tokens == null || Tokens.Count == 0)
                ? LanguageManager.api.GetKey(key)
                : LanguageManager.api.GetKey(key, Tokens.ToArray(), null);

        GetComponent<TextMeshProUGUI>().text += subfixText;
    }

    //public void SetLeft(this RectTransform rt, float left)
    //{
    //    rt.offsetMin = new Vector2(left, rt.offsetMin.y);
    //}

    //public void SetRight(this RectTransform rt, float right)
    //{
    //    rt.offsetMax = new Vector2(-right, rt.offsetMax.y);
    //}

    //public void SetTop(this RectTransform rt, float top)
    //{
    //    rt.offsetMax = new Vector2(rt.offsetMax.x, -top);
    //}

    //public void SetBottom(this RectTransform rt, float bottom)
    //{
    //    rt.offsetMin = new Vector2(rt.offsetMin.x, bottom);
    //}
}
