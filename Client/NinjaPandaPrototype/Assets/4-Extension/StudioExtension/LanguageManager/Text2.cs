﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Studio
{
    public class Text2 : Text
    {
        /// <summary>
        /// Thường là dấu : cuối ký tự
        /// </summary>
        [HideInInspector] public string subfixText = "";

        [HideInInspector] public bool AutoChangeLocalize = true;
        [HideInInspector] public string LocalizeId;
        [HideInInspector] public List<string> Tokens;

        /// <summary>
        /// Text dùng cho trường hợp không đủ cấp để click vào button
        /// </summary>
        [HideInInspector] public string alternativeText = "";

        protected override void OnEnable()
        {
            if (Application.isPlaying)
            {

                if (AutoChangeLocalize)
                {
                    LanguageManager.api.OnLanguageChanged += OnLanguageChanged;
                    OnLanguageChanged();
                }
            }

            base.OnEnable();
        }

        protected override void OnDisable()
        {
            if (Application.isPlaying)
            {
                if (AutoChangeLocalize) LanguageManager.api.OnLanguageChanged -= OnLanguageChanged;
            }

            base.OnDisable();
        }

        public void OnLanguageChanged()
        {
            if (alternativeText != "")
            {
                text = alternativeText;
                return;
            }
            if (string.IsNullOrEmpty(LocalizeId)) return;
            text = (Tokens == null || Tokens.Count == 0)
                ? LanguageManager.api.GetKey(LocalizeId)
                : LanguageManager.api.GetKey(LocalizeId, Tokens.ToArray(), null);

            text += subfixText;
        }

        public void SetLocalize(string key, string[] tokens = null)
        {
            LocalizeId = key;
            Tokens = (tokens == null) ? Tokens : tokens.ToList();

            text = (Tokens == null || Tokens.Count == 0)
                ? LanguageManager.api.GetKey(key)
                : LanguageManager.api.GetKey(key, Tokens.ToArray(), null);

            text += subfixText;
        }

        public void SetTokens(string[] tokens)
        {
            Tokens = (tokens == null) ? new List<string>() : tokens.ToList();
            text = (tokens == null || tokens.Length == 0)
                ? LanguageManager.api.GetKey(LocalizeId)
                : LanguageManager.api.GetKey(LocalizeId, tokens, null);

            text += subfixText;
        }
    }

    public static class Text2Utils
    {
        public static void SetLocalize(this Text2 text2, string localizeId, string[] tokens = null)
        {
            if (text2 == null)
            {
                DebugX.LogWarning("Text2Utils:SetLocalize: text2 is null");
                return;
            }

            text2.SetLocalize(localizeId, tokens);
        }

        public static void SetLocalize(this Button btn, string localizeId, string[] tokens = null)
        {
            if (btn == null)
            {
                DebugX.LogWarning("Text2Utils:SetLocalize: Button is null");
                return;
            }

            var text2 = btn.transform.GetComponent<Text2>();
            text2.SetLocalize(localizeId, tokens);
        }

        public static void SetLocalize(this Button btn, string text2Name, string localizeId, string[] tokens = null)
        {
            if (btn == null)
            {
                DebugX.LogWarning("Text2Utils:SetLocalize: Button is null");
                return;
            }

            var text = btn.transform.Find(text2Name);
            if (text == null)
            {
                DebugX.LogWarning("Text2Utils:SetLocalize: Button.Text is null");
                return;
            }

            var text2 = text.GetComponent<Text2>();
            text2.SetLocalize(localizeId, tokens);
        }

        public static void SetLocalize(this InputField inputField, string localizeId, string[] tokens = null)
        {
            if (inputField == null)
            {
                DebugX.LogWarning("Text2Utils:SetLocalize: inputField is null");
                return;
            }

            var placeHolder = inputField.transform.Find("Placeholder");
            if (placeHolder == null)
            {
                DebugX.LogWarning("Text2Utils:SetLocalize: inputField.placeHolder is null");
                return;
            }

            var text2 = placeHolder.GetComponent<Text2>();
            text2.SetLocalize(localizeId, tokens);
        }
    }
}
