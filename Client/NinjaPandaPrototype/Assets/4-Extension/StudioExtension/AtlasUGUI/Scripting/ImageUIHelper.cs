﻿//#define TEST_EDIT_MODE //su dung khi test download asset bundle tren editor

//using com.vietlabs.DebugX;
using UnityEngine;
using UnityEngine.UI;
using Studio;
using Studio.BC;

#if UNITY_EDITOR
using UnityEditor;
#endif

[ExecuteInEditMode]
public class ImageUIHelper : MonoBehaviour
{
    public string nameTexUI = "";
    public GameObject refManageUI = null;

    private GameObject manageSpriteUI = null;
    private bool _isWaitDowload = false;
    private bool _isloaded;
    private GameObject _loadingObject;

#if UNITY_EDITOR && !TEST_EDIT_MODE
    public Sprite spriteSelected = null;
    public Sprite oldSpriteSelected = null;
#endif

    bool IsSetSpriteWhenDownloadComplete = false;

    void Awake()
    {
        if (refManageUI == null)
        {
#if UNITY_EDITOR
            refManageUI = Resources.Load<GameObject>("RefManageUI");
#else
            refManageUI = BCResource.Api.RefManageUI;
#endif
        }

        if (!string.IsNullOrEmpty(nameTexUI))
            SetSprite(nameTexUI);
        else
            try
            {
                if (gameObject.GetComponent<Image>().sprite != null)
                    nameTexUI = gameObject.GetComponent<Image>().sprite.name;
            }
            catch (System.Exception)
            {

                var sprite = gameObject.GetComponent<SpriteRenderer>();
                if (sprite != null)
                {
                    nameTexUI = sprite.sprite.name;
                }
            }


#if UNITY_EDITOR && !TEST_EDIT_MODE
        if (gameObject.GetComponent<Image>() != null)
        {
            spriteSelected = gameObject.GetComponent<Image>().sprite;
            oldSpriteSelected = spriteSelected;
        }
#endif
    }

    void Update()
    {
        if (_isWaitDowload)
        {
            SetSprite(nameTexUI);
        }

#if UNITY_EDITOR && !TEST_EDIT_MODE
        if (oldSpriteSelected != spriteSelected)
        {
            SetSprite(spriteSelected.name);
            oldSpriteSelected = spriteSelected;
        }
#endif

#if !UNITY_EDITOR || TEST_EDIT_MODE
        //if (IsSetSpriteWhenDownloadComplete == false)
            //SetSpriteInAssetBundleWhenDownloadComplete();
#endif
    }

    public void SetSprite(string nameTexture, bool setNativeSize = false)
    {
        nameTexUI = nameTexture;
        if (refManageUI == null)
        {
            DebugX.LogWarning(gameObject.name + " _ " + nameTexture + " can't find RefPrefabManageUI");
            return;
        }

        manageSpriteUI = refManageUI.GetComponent<RefPrefabManageUI>().objectReference;

        if (Application.isPlaying)
        {
            if (manageSpriteUI == null)
            {
                manageSpriteUI = BCResource.Api.AtlasUI;
                refManageUI.GetComponent<RefPrefabManageUI>().objectReference = manageSpriteUI;
            }
        }

        if (manageSpriteUI == null)
        {
            _isWaitDowload = true;
            DebugX.LogWarning(gameObject.name + ":: can't find objectReference");
            return;
        }

        var managetSpriteUiView = manageSpriteUI.GetComponent<ManageSpriteUI>();
        if (managetSpriteUiView == null || managetSpriteUiView.listUI == null)
        {
            DebugX.LogWarning(gameObject.name + ":: can't find ManageSpriteUI script");
            return;
        }

        var image = gameObject.GetComponent<Image>();
        if (image != null)
        {

#if UNITY_EDITOR
            Undo.RecordObject(image, "Image Change");
            EditorUtility.SetDirty(image);
#endif

            image.sprite = managetSpriteUiView.GetSprite(nameTexture);
            if (setNativeSize) image.SetNativeSize();

            //AddDebugTextOnMissingImage(image, nameTexture);
        }
        else
        {
            var sprite = gameObject.GetComponent<SpriteRenderer>();
            if (sprite != null)
            {
                sprite.sprite = managetSpriteUiView.GetSprite(nameTexture);
            }
        }

    }

    void ClearDebugTextOnMissingImage(Image image)
    {
        // disable tính năng này
        return;

        // only for debug in editor
        if (Application.platform != RuntimePlatform.WindowsEditor) { return; }

        try // không để tính năng debug ảnh hưởng game
        {
            if (image == null) { return; }

            string goDebugTextName = "____txtDebugMissingIcon";

            // xóa object debug cũ
            Utils.ClearAllChild(image.transform, goDebugTextName);
        }
        catch (System.Exception exp)
        {
            Debug.LogError(exp);
        }
    }
    /*
    void AddDebugTextOnMissingImage(Image image, string nameTexture)
    {
        // disable tính năng này
        return;

        // only for debug in editor
        if (Application.platform != RuntimePlatform.WindowsEditor) { return; }

        try // không để tính năng debug ảnh hưởng game
        {
            if (image == null) { return; }

            string goDebugTextName = "____txtDebugMissingIcon";

            // xóa object debug cũ
            ClearDebugTextOnMissingImage(image);

            // có image
            if (image.sprite != null) { return; }

            GameObject goDebug = null;
            try
            {
                GameObject goDebugPrefab = ResourcesManager.inst.LoadPrefab("Prefabs/txtDebugMissingIcon") as GameObject;
                goDebug = GameObject.Instantiate(goDebugPrefab);
                goDebug.name = goDebugTextName;
                goDebug.SetActive(true);
                Text txtDebug = goDebug.transform.Find("text").AddToggleToList<Text>();
                txtDebug.text = nameTexture;
                //txtDebug.transform.localPosition = Vector3.zero;
                //txtDebug.horizontalOverflow = HorizontalWrapMode.Overflow;
                //txtDebug.verticalOverflow = VerticalWrapMode.Overflow;

                goDebug.transform.localPosition = Vector3.zero;
                goDebug.AddToggleToList<Transform>().SetParent(image.transform, false);
            }
            catch (System.Exception exp)
            {
                Debug.LogError(image.name + "==>" + exp);
                if (goDebug != null)
                {
                    GameObject.Destroy(goDebug);
                }
            }
        }
        catch(System.Exception exp)
        {
            Debug.LogError(exp);
        }
    }
	*/
    public void SetSpriteInAssetBundleWhenDownloadComplete()
    {
        SetSprite(nameTexUI);
        if (gameObject.GetComponent<Image>().sprite != null)
            IsSetSpriteWhenDownloadComplete = true;
    }
    public void Revert()
    {
        Awake();
    }
}
