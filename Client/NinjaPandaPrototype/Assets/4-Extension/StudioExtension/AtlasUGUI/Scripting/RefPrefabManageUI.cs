﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using Studio;
using Studio.BC;

//[ExecuteInEditMode]
public class RefPrefabManageUI : MonoBehaviour {

	public GameObject objectReference;
    public string nameAtlasRef;

    private GameObject _atlasGo;

    void Awake()
    {
        StartCoroutine(LoadAtlas());
    }

#if UNITY_EDITOR
    private void Start()
    {
       //FindMissing(transform, gameObject.name);
    }

    void FindMissing(Transform objTransform, string assetPath)
    {
        Image img = objTransform.GetComponent<Image>();

        if (img != null)
        {
            //check sprite default
            if (img.sprite != null)
            {
                if (img.sprite.name == "Background" ||
                    img.sprite.name == "Checkmark" ||
                    img.sprite.name == "DropdownArrow" ||
                    img.sprite.name == "InputFieldBackground" ||
                    img.sprite.name == "Knob" ||
                    img.sprite.name == "UIMask" ||
                    img.sprite.name == "UISprite")
                    DebugX.LogError("Image must not be default Unity's sprite: " + assetPath);
            }
            else
            {
                DebugX.LogError("Image missing: " + assetPath);
            }            
        }

        int count = objTransform.childCount;
        for (int i = 0; i < count; ++i)
        {
            Transform objTransformChild = objTransform.GetChild(i);
            FindMissing(objTransformChild, assetPath + "/" + objTransformChild.gameObject.name);
        }
    }
#endif

    IEnumerator LoadAtlas()
    {
        GameObject atlasPrefab = (GameObject)null;
		yield return StartCoroutine(BCResource.Api.LoadAtlas(nameAtlasRef, (prefab) =>
        {
            atlasPrefab = prefab;

            _atlasGo = Instantiate(atlasPrefab);
            _atlasGo.name = "atlas_" + gameObject.name;
            _atlasGo.transform.localPosition = new Vector3(0.0f, 500.0f, 0.0f);
            _atlasGo.transform.localScale = Vector3.one;

            this.objectReference = _atlasGo;

        }));

        if (atlasPrefab == null)
        {
            //ShowDownloadFail();
            yield break;
        }
    }

    void OnDestroy()
    {
//#if UNITY_EDITOR
//        DestroyImmediate(_atlasGo,true);
//#else
        Destroy(_atlasGo);
//#endif
    }
}
