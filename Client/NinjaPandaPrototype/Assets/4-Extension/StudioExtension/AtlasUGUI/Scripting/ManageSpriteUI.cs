﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Studio;
//using com.vietlabs.DebugX;

public class ManageSpriteUI : MonoBehaviour
{
	public List<Sprite> listUI = new List<Sprite>();
    public Dictionary<string, Sprite> dicUI;

    private void Start()
    {
        dicUI = new Dictionary<string, Sprite>();
        foreach (var spriteUI in listUI)
        {
            if (spriteUI == null)
            {
                continue;
            }
            if(!dicUI.ContainsKey(spriteUI.name))
                dicUI.Add(spriteUI.name, spriteUI);
            else
                DebugX.LogWarning("same sprite name in atlas: " + spriteUI.name);
        }
    }

    /// <summary>
    /// sprite name có thể lấy từ các constain trong file : ConstantAtlasSprite.cs
    /// </summary>
    /// <param name="spriteName"></param>
    /// <returns></returns>
    public Sprite GetSprite(string spriteName)
    {
        if (dicUI == null)
        {
            dicUI = new Dictionary<string, Sprite>();
            foreach (var spriteUI in listUI)
            {
                if (!dicUI.ContainsKey(spriteUI.name))
                    dicUI.Add(spriteUI.name, spriteUI);
                else
                    DebugX.LogWarning("same sprite name in atlas: " + spriteUI.name);
            }
        }

        if (string.IsNullOrEmpty(spriteName)) return null;
        if (listUI.Count == 0) return null;

#if !UNITY_EDITOR
        Sprite spr = null;
        if(dicUI.ContainsKey(spriteName))
            spr = dicUI[spriteName];
        else
            DebugX.Log("not find sprite: " + spriteName);
#else
        Sprite spr = listUI.Find(x => (x != null && x.name == spriteName));
#endif

        if(spr == null)
        {
            DebugX.LogWarning("Invalid sprite name: " + spriteName);
        }

        return spr;
    }

#if UNITY_EDITOR

    private GameObject _objectReferenceTemp;
    private RefPrefabManageUI _pefPrefabManageUi;

    public void SetObjectPreference(RefPrefabManageUI obj)
    {
        _objectReferenceTemp = obj.objectReference;
        _pefPrefabManageUi = obj;
    }

    void OnDestroy()
    {
        if (_pefPrefabManageUi != null) {
            _pefPrefabManageUi.objectReference = _objectReferenceTemp;
        }
    }

#endif
}