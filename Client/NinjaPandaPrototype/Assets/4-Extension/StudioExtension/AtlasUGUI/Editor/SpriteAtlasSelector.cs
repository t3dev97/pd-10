﻿
using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpriteAtlasSelector : ScriptableWizard
{
    public delegate void Callback(Sprite sprite);
    
    Sprite mSprite;
    Callback mCallback;

    static public SpriteAtlasSelector instance;
    private GameObject prefabAtlas;
    private List<Sprite> ListSprite;

    void OnEnable() { instance = this; }
    void OnDisable() { instance = null; }

    Vector2 mPos = Vector2.zero;

    float mClickTime = 0f;

    private string selectedSprite = "";
    string filterString = "";
    void OnGUI()
    {
        EditorGUIUtility.labelWidth = 80.0f;

        if (prefabAtlas == null)
        {
            GUILayout.Label("No RefManageUI selected.", "LODLevelNotifyText");
        }
        else
        {
            GameObject manageSpriteUI = prefabAtlas.GetComponent<RefPrefabManageUI>().objectReference;
            if (manageSpriteUI && ListSprite == null)
                ListSprite = manageSpriteUI.GetComponent<ManageSpriteUI>().listUI;

            bool close = false;
            //GUILayout.Label(ListSprite[0].texture.name, "LODLevelNotifyText");

            DrawSeparator();

            GUILayout.BeginHorizontal();
            GUILayout.Space(84f);

            
            filterString = EditorGUILayout.TextField("", filterString, "SearchTextField");           

            if (GUILayout.Button("", "SearchCancelButton", GUILayout.Width(18f)))
            {
                filterString = "";
                GUIUtility.keyboardControl = 0;
            }
            GUILayout.Space(84f);
            GUILayout.EndHorizontal();
            
            if (ListSprite == null /*&& ListSprite.Count < 1*/)
            {
                GUILayout.Label("The atlas doesn't have a texture to work with");
                return;
            }

            List<Sprite> sprites = GetListOfSprites(filterString);
            if (filterString == "")
                sprites = ListSprite;
            float size = 80f;
            float padded = size + 10f;
            int columns = Mathf.FloorToInt(Screen.width / padded);
            if (columns < 1) columns = 1;

            int offset = 0;
            Rect rect = new Rect(10f, 0, size, size);

            GUILayout.Space(10f);
            mPos = GUILayout.BeginScrollView(mPos);
            int rows = 1;

            while (offset < sprites.Count)
            {
                GUILayout.BeginHorizontal();
                {
                    int col = 0;
                    rect.x = 10f;

                    for (; offset < sprites.Count; ++offset)
                    {
                        Sprite sprite = sprites[offset];
                        if (sprite == null) continue;

                        // Button comes first
                        if (GUI.Button(rect, ""))
                        {
                            if (Event.current.button == 0)
                            {
                                float delta = Time.realtimeSinceStartup - mClickTime;
                                mClickTime = Time.realtimeSinceStartup;

                                if (selectedSprite != sprite.name)
                                {
                                    if (mSprite != null)
                                    {
                                        EditorUtility.SetDirty(mSprite);
                                    }

                                    selectedSprite = sprite.name;
                                    SpriteAtlasSelector.instance.Repaint();
                                    if (mCallback != null) mCallback(sprite);
                                }
                                else if (delta < 0.5f) close = true;
                            }
                        }

                        if (Event.current.type == EventType.Repaint)
                        {
                            // On top of the button we have a checkboard grid
                            Rect uv = sprite.rect;
                            uv = ConvertToTexCoords(uv, sprite.texture.width, sprite.texture.height);

                            // Calculate the texture's scale that's needed to display the sprite in the clipped area
                            float scaleX = rect.width / uv.width;
                            float scaleY = rect.height / uv.height;

                            // Stretch the sprite so that it will appear proper
                            float aspect = (scaleY / scaleX) / ((float)sprite.texture.height / sprite.texture.width);
                            Rect clipRect = rect;

                            if (aspect != 1f)
                            {
                                if (aspect < 1f)
                                {
                                    // The sprite is taller than it is wider
                                    float padding = size * (1f - aspect) * 0.5f;
                                    clipRect.xMin += padding;
                                    clipRect.xMax -= padding;
                                }
                                else
                                {
                                    // The sprite is wider than it is taller
                                    float padding = size * (1f - 1f / aspect) * 0.5f;
                                    clipRect.yMin += padding;
                                    clipRect.yMax -= padding;
                                }
                            }

                            GUI.DrawTextureWithTexCoords(clipRect, sprite.texture, uv);
                            
                            // Draw the selection
                            if (selectedSprite == sprite.name)
                            {
                                DrawOutline(rect, new Color(0.4f, 1f, 0f, 1f));
                            }
                        }

                        GUI.backgroundColor = new Color(1f, 1f, 1f, 0.5f);
                        GUI.contentColor = new Color(1f, 1f, 1f, 0.7f);
                        GUI.Label(new Rect(rect.x, rect.y + rect.height, rect.width, 32f), sprite.name, "ProgressBarBack");
                        GUI.contentColor = Color.white;
                        GUI.backgroundColor = Color.white;

                        if (++col >= columns)
                        {
                            ++offset;
                            break;
                        }
                        rect.x += padded;
                    }
                }
                GUILayout.EndHorizontal();
                GUILayout.Space(padded);
                rect.y += padded + 26;
                ++rows;
            }
            GUILayout.Space(rows * 26);
            GUILayout.EndScrollView();

            if (close) Close();
        }
    }

    static public void Show(Object refPrefabAtlas, Callback callback)
    {
        if (instance != null)
        {
            instance.Close();
            instance = null;
        }

        SpriteAtlasSelector comp = ScriptableWizard.DisplayWizard<SpriteAtlasSelector>("Select a Sprite");

        comp.mSprite = null;
        comp.mCallback = callback;
        comp.prefabAtlas = refPrefabAtlas as GameObject;
    }

    private void DrawSeparator()
    {
        GUILayout.Space(12f);

        if (Event.current.type == EventType.Repaint)
        {
            Texture2D tex = EditorGUIUtility.whiteTexture;
            Rect rect1 = GUILayoutUtility.GetLastRect();
            GUI.color = new Color(0f, 0f, 0f, 0.25f);
            GUI.DrawTexture(new Rect(0f, rect1.yMin + 6f, Screen.width, 4f), tex);
            GUI.DrawTexture(new Rect(0f, rect1.yMin + 6f, Screen.width, 1f), tex);
            GUI.DrawTexture(new Rect(0f, rect1.yMin + 9f, Screen.width, 1f), tex);
            GUI.color = Color.white;
        }
    }

    public Rect ConvertToTexCoords(Rect rect, int width, int height)
    {
        Rect final = rect;

        if (width != 0f && height != 0f)
        {
            final.xMin = rect.xMin / width;
            final.xMax = rect.xMax / width;
            final.yMax = rect.yMax / height;
            final.yMin = rect.yMin / height;
        }
        return final;
    }

    public List<Sprite> GetListOfSprites(string match)
    {
        List<Sprite> tempList = new List<Sprite>();
        for (int i = 0; i < ListSprite.Count; i++)
        {
            Sprite sprite = ListSprite[i];
            if (sprite == null)
            {
                Debug.Log("sprite is null [match:" + match + "] - Build lại atlas sẽ hết bug này");
                continue;
                // int x = 1;
            }
            
            if (sprite.name.ToLower().Contains(match))
            {
                tempList.Add(sprite);
            }
        }
        return tempList;
    }

    void DrawOutline(Rect rect, Color color)
    {
        if (Event.current.type == EventType.Repaint)
        {
            Texture2D tex = EditorGUIUtility.whiteTexture;
            GUI.color = color;
            GUI.DrawTexture(new Rect(rect.xMin, rect.yMin, 1f, rect.height), tex);
            GUI.DrawTexture(new Rect(rect.xMax, rect.yMin, 1f, rect.height), tex);
            GUI.DrawTexture(new Rect(rect.xMin, rect.yMin, rect.width, 1f), tex);
            GUI.DrawTexture(new Rect(rect.xMin, rect.yMax, rect.width, 1f), tex);
            GUI.color = Color.white;
        }
    }
}
