﻿using System.Collections.Generic;
using System.IO;
//using com.vietlabs;
//using com.vietlabs.DebugX;
using MiniJSON;
using UnityEditor;
using UnityEngine;
using System.Collections;
using Studio;

public class ToolTexturePacker : ScriptableWizard  {

    public string AtlasGUIDir = "";
    public string BorderDataPath = "";
    public string SpriteDirPath = "";

    [HideInInspector]
    public static string prefabPath = "";

    [MenuItem("HVL/Atlas UI/Tool support Texture Packer")]
    static void SyncTexturePacker()
    {
        ScriptableWizard.DisplayWizard("Tool Texture Packer", typeof(ToolTexturePacker), "Add list sprite to AtlasUI prefab", "Sync border");
    }

    void OnWizardCreate()
    {
        //Add list sprite to AtlasUI prefab

        if (AtlasGUIDir == "")
            AtlasGUIDir = EditorUtility.OpenFolderPanel("Chose Atlas UGui folder", Application.dataPath, "");
        
        if (string.IsNullOrEmpty(AtlasGUIDir))
        {
            //DebugX.LogError("Add list sprite again!!!!!!!!!!!");
            return;
        }

        List<Sprite> lsSprite = new List<Sprite>();

        DirectoryInfo dirInfo = new DirectoryInfo(AtlasGUIDir);

        foreach (FileInfo pngFile in dirInfo.GetFiles("*.png", SearchOption.AllDirectories))
        {
            string allPath = pngFile.FullName;
            string assetPath = allPath.Substring(allPath.IndexOf("Assets"));
            Object[] sprites = AssetDatabase.LoadAllAssetRepresentationsAtPath(assetPath);

            for (int i = 0; i < sprites.Length; i++)
            {
                lsSprite.Add(sprites[i] as Sprite);
            }
        }

        prefabPath = AtlasGUIDir.Substring(AtlasGUIDir.IndexOf("Assets")) + "/AtlasUI.prefab";
        GameObject oldPrefab = LoadAsset<GameObject>(prefabPath);
        if (oldPrefab)
        {
            AssetDatabase.StartAssetEditing();
            oldPrefab.GetComponent<ManageSpriteUI>().listUI = lsSprite;
            AssetDatabase.StopAssetEditing();
            EditorUtility.SetDirty(oldPrefab);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }
        else
        {
            GameObject temp = new GameObject();
            temp.AddComponent<ManageSpriteUI>().listUI = lsSprite;

            PrefabUtility.CreatePrefab(prefabPath, temp);
            Object.DestroyImmediate(temp, false);
        }
    }

    void OnWizardOtherButton()
    {
        //sync border

        if (AtlasGUIDir == "")
            AtlasGUIDir = EditorUtility.OpenFolderPanel("Chose Atlas UGui folder", Application.dataPath, "");

        if (BorderDataPath == "")
            BorderDataPath = EditorUtility.OpenFilePanel("Open Border Data file", Application.dataPath, "txt");

        if (SpriteDirPath == "")
            SpriteDirPath = EditorUtility.OpenFolderPanel("Chose Sprite folder", Application.dataPath, "");

        if (string.IsNullOrEmpty(BorderDataPath) || string.IsNullOrEmpty(AtlasGUIDir))
        {
            //DebugX.LogError("Sync Again!!!!!!!!!!!");
            return;
        }

        //read
        SyncBorder();

        //write
        SyncBorder();

        SyncToSprite();
    }

    public void SyncBorder()
    {
        var data = File.ReadAllText(BorderDataPath);
        Dictionary<string, object> dataBorder = data.JsonToDictionary();

        DirectoryInfo dirInfo = new DirectoryInfo(AtlasGUIDir);

        foreach (FileInfo pngFile in dirInfo.GetFiles("*.png", SearchOption.AllDirectories))
        {
            string allPath = pngFile.FullName;
            string assetPath = allPath.Substring(allPath.IndexOf("Assets"));

            Texture2D texture = AssetDatabase.LoadAssetAtPath(assetPath, typeof (Texture2D)) as Texture2D;

            string path = AssetDatabase.GetAssetPath(texture);
            TextureImporter texImporter = AssetImporter.GetAtPath(path) as TextureImporter;

            texImporter.isReadable = true;

            if (texImporter.spriteImportMode == SpriteImportMode.Multiple)
            {
                //format sprite
                texImporter.spritePixelsPerUnit = 1;
                texImporter.mipmapEnabled = false;
                texImporter.textureFormat = TextureImporterFormat.AutomaticTruecolor;

                List<SpriteMetaData> newData = new List<SpriteMetaData>();
                for (int i = 0; i < texImporter.spritesheet.Length; i++)
                {
                    SpriteMetaData d = texImporter.spritesheet[i];
                    //DebugX.Log("name sprite: " + d.name);
                    if (d.border == Vector4.zero)
                    {
                        //read data
                        if (dataBorder.ContainsKey(d.name))
                        {
                            var border = dataBorder.Get<Dictionary<string, object>>(d.name);
                            //Dictionary<string, object> dic = border as Dictionary<string, object>;

                            d.border.x = border.Get<float>("x");
                            d.border.y = border.Get<float>("y");
                            d.border.z = border.Get<float>("z");
                            d.border.w = border.Get<float>("w");
                        }
                    }
                    else
                    {
                        //write data
                        Dictionary<string, object> border = new Dictionary<string, object>();

                        border.Add("x", d.border.x);
                        border.Add("y", d.border.y);
                        border.Add("z", d.border.z);
                        border.Add("w", d.border.w);

                        dataBorder[d.name] = border;

                        //d.border = Vector4.zero;
                    }

                    //DebugX.Log(d.border);

                    newData.Add(d);
                }
                texImporter.spritesheet = newData.ToArray();
            }

            if (texImporter.spriteImportMode == SpriteImportMode.Single)
            {
                //format sprite
                texImporter.spritePixelsPerUnit = 1;
                texImporter.mipmapEnabled = false;
                texImporter.textureFormat = TextureImporterFormat.AutomaticTruecolor;

                if (texImporter.spriteBorder == Vector4.zero)
                {
                    //read data
                    if (dataBorder.ContainsKey(texture.name))
                    {
                        var border = dataBorder.Get<Dictionary<string, object>>(texture.name);
                        //Dictionary<string, object> dic = border as Dictionary<string, object>;

                        Vector4 readBorder = new Vector4(0.0f,0.0f,0.0f,0.0f);
                        readBorder.x = border.Get<float>("x");
                        readBorder.y = border.Get<float>("y");
                        readBorder.z = border.Get<float>("z");
                        readBorder.w = border.Get<float>("w");
                        texImporter.spriteBorder = readBorder;
                    }
                }
                else
                {
                    //write data
                    Dictionary<string, object> border = new Dictionary<string, object>();

                    border.Add("x", texImporter.spriteBorder.x);
                    border.Add("y", texImporter.spriteBorder.y);
                    border.Add("z", texImporter.spriteBorder.z);
                    border.Add("w", texImporter.spriteBorder.w);

                    dataBorder[texture.name] = border;

                    //d.border = Vector4.zero;
                }
            }

            File.WriteAllText(BorderDataPath, dataBorder.DictionaryToJson());

            //DebugX.Log("forceupdate: " + path);
            AssetDatabase.ImportAsset(path, ImportAssetOptions.ForceUpdate);
			EditorUtility.SetDirty(texture);
			EditorUtility.SetDirty(texImporter);
			AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }
    }

    void SyncToSprite()
    {
        var data = File.ReadAllText(BorderDataPath);
        Dictionary<string, object> dataBorder = data.JsonToDictionary();

        DirectoryInfo dirInfo = new DirectoryInfo(SpriteDirPath);

        foreach (FileInfo pngFile in dirInfo.GetFiles("*.png", SearchOption.AllDirectories))
        {
            string allPath = pngFile.FullName;
            string assetPath = allPath.Substring(allPath.IndexOf("Assets"));

            Texture2D texture = AssetDatabase.LoadAssetAtPath(assetPath, typeof(Texture2D)) as Texture2D;

            string path = AssetDatabase.GetAssetPath(texture);
            TextureImporter texImporter = AssetImporter.GetAtPath(path) as TextureImporter;

            if(texImporter.isReadable != true)
                texImporter.isReadable = true;
            
            if (texImporter.spriteImportMode == SpriteImportMode.Single)
            {
                //format sprite
                if (texImporter.spritePixelsPerUnit != 1)
                    texImporter.spritePixelsPerUnit = 1;
                if (texImporter.mipmapEnabled != false)
                    texImporter.mipmapEnabled = false;
                if (texImporter.textureFormat != TextureImporterFormat.AutomaticTruecolor)
                    texImporter.textureFormat = TextureImporterFormat.AutomaticTruecolor;

                //if (texImporter.spriteBorder == Vector4.zero)
                //{
                    //read data
                    if (dataBorder.ContainsKey(texture.name))
                    {
                        var border = dataBorder.Get<Dictionary<string, object>>(texture.name);
                        //Dictionary<string, object> dic = border as Dictionary<string, object>;

                        Vector4 readBorder = new Vector4(0.0f, 0.0f, 0.0f, 0.0f);
                        readBorder.x = border.Get<float>("x");
                        readBorder.y = border.Get<float>("y");
                        readBorder.z = border.Get<float>("z");
                        readBorder.w = border.Get<float>("w");
                        if(texImporter.spriteBorder != readBorder)
                            texImporter.spriteBorder = readBorder;
                    }
                //}
                //else
                //{
                //    //write data
                //    Dictionary<string, object> border = new Dictionary<string, object>();

                //    border.Add("x", texImporter.spriteBorder.x);
                //    border.Add("y", texImporter.spriteBorder.y);
                //    border.Add("z", texImporter.spriteBorder.z);
                //    border.Add("w", texImporter.spriteBorder.w);

                //    dataBorder[texture.name] = border;

                //    //d.border = Vector4.zero;
                //}
            }

            //File.WriteAllText(BorderDataPath, dataBorder.DictionaryToJson());

            //DebugX.Log("forceupdate: " + path);
            AssetDatabase.ImportAsset(path, ImportAssetOptions.ForceUpdate);
			EditorUtility.SetDirty(texture);
			EditorUtility.SetDirty(texImporter);
			AssetDatabase.SaveAssets();
			AssetDatabase.Refresh();
        }
    }

    private static T LoadAsset<T>(string path) where T : Object
    {
        return AssetDatabase.LoadAssetAtPath(path, typeof(T)) as T;
    }
}
