﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;
using UnityEngine.UI;

[CustomEditor(typeof(RefPrefabManageUI))]
public class CustomEditorRefPrefabManageUI : Editor {

    private SerializedProperty component = null;
    private ReorderableList reorderableList;

    private Object rootObject;
    private GameObject rootGameObject;
    private RefPrefabManageUI rootRefPrefabManageUI;
    private bool isKeepCurrentSpriteInAtlas = true;
    private string atlasGUIDirRelative = "/0-AssetBundleSource/Resources/UI/Atlas";
    private string SpriteDirRelative = "/0-AssetBundleSource/Resources/UI/Sprite";
    private string atlasGUIDir = "";
    private string prefabPath = "";
    private List<Texture2D> textures = new List<Texture2D>();
    private Texture2D textureAtlas = null;
    private Rect[] rects;
    private string pathAtlasTex = "";
    private int textureSize = 1024;

    private RefPrefabManageUI refPrefabManageUI
    {
        get
        {
            return target as RefPrefabManageUI;
        }
    }

    private void OnEnable()
    {
        reorderableList = new ReorderableList(textures, typeof(Texture), true, true, true, true);

        // This could be used aswell, but I only advise this your class inherrits from UnityEngine.Object or has a CustomPropertyDrawer
        // Since you'll find your item using: serializedObject.FindProperty("list").GetArrayElementAtIndex(index).objectReferenceValue
        // which is a UnityEngine.Object
        // reorderableList = new ReorderableList(serializedObject, serializedObject.FindProperty("list"), true, true, true, true);

        // Add listeners to draw events
        reorderableList.drawHeaderCallback += DrawHeader;
        reorderableList.drawElementCallback += DrawElement;

        reorderableList.onAddCallback += AddItem;
        reorderableList.onRemoveCallback += RemoveItem;
    }

    private void OnDisable()
    {
        // Make sure we don't get memory leaks etc.
        reorderableList.drawHeaderCallback -= DrawHeader;
        reorderableList.drawElementCallback -= DrawElement;

        reorderableList.onAddCallback -= AddItem;
        reorderableList.onRemoveCallback -= RemoveItem;
    }

    private void DrawHeader(Rect rect)
    {
        GUI.Label(rect, "List texture");
    }

    private void DrawElement(Rect rect, int index, bool active, bool focused)
    {
        EditorGUI.BeginChangeCheck();
        //item.boolValue = EditorGUI.Toggle(new Rect(rect.x, rect.y, 18, rect.height), item.boolValue);
        //item.stringvalue = EditorGUI.TextField(new Rect(rect.x + 18, rect.y, rect.width - 18, rect.height), item.stringvalue);
        //item = (Texture2D)EditorGUILayout.ObjectField(new Rect(rect.x, rect.y, 18, rect.height), "Image", item, typeof(Texture2D), false);

        //EditorGUI.DrawPreviewTexture(new Rect(rect.x, rect.y, 24, rect.height), textures[index] as Texture);// "Image", item, typeof(Texture2D), false);

        EditorGUI.DrawTextureTransparent(new Rect(rect.x, rect.y, 24, rect.height), textures[index] as Texture);

        textures[index] = (Texture2D)EditorGUILayout.ObjectField(textures[index].name, textures[index], typeof(Texture2D), true);

        if (EditorGUI.EndChangeCheck())
        {
            EditorUtility.SetDirty(target);
        }

        // If you are using a custom PropertyDrawer, this is probably better
        // EditorGUI.PropertyField(rect, serializedObject.FindProperty("list").GetArrayElementAtIndex(index));
        // Although it is probably smart to cach the list as a private variable ;)
    }

    private void AddItem(ReorderableList list)
    {
        textures.Add(new Texture2D(32,32));

        EditorUtility.SetDirty(target);
    }

    private void RemoveItem(ReorderableList list)
    {
        textures.RemoveAt(list.index);

        EditorUtility.SetDirty(target);
    }

    public override void OnInspectorGUI()
    {
        //base.OnInspectorGUI();

        serializedObject.Update();
        DrawForProperty();
        serializedObject.ApplyModifiedProperties();

        reorderableList.DoLayoutList();
    }

    void DrawForProperty()
    {
        component = serializedObject.FindProperty("objectReference");
        component.objectReferenceValue = EditorGUILayout.ObjectField("GUI Ref", component.objectReferenceValue, typeof(GameObject), true);
             
        component = serializedObject.FindProperty("nameAtlasRef");
        component.stringValue = EditorGUILayout.TextField("Name Atlas", component.stringValue);

        rootObject = target;
        rootObject = EditorGUILayout.ObjectField("Root RefPrefabManageUI", rootObject, typeof(RefPrefabManageUI), true);

        isKeepCurrentSpriteInAtlas = EditorGUILayout.Toggle("Is keep current sprite in atlas", isKeepCurrentSpriteInAtlas);

        atlasGUIDir = Application.dataPath + atlasGUIDirRelative;
        atlasGUIDir = EditorGUILayout.TextField("Atlas GUI Dir", atlasGUIDir);
        
        if (GUILayout.Button("Add Sprites From Root"))
        {
            AddSprite();
        }

        if (GUILayout.Button("Pack Atlas"))
        {
            PackAtlas();
        }

        if (GUILayout.Button("Reset Image"))
        {
            ResetImage();
        }

        if (GUILayout.Button("Reset Image Without Remove ImageUIHelper"))
        {
            ResetImageWithoutRemImgUIHelper();
        }
    }

    void AddSprite()
    {
        textures.Clear();

        rootRefPrefabManageUI = rootObject as RefPrefabManageUI;
        rootGameObject = rootRefPrefabManageUI.gameObject;

        if (rootGameObject != null)
        {

            List<string> listNameSprite = new List<string>();

            FindSprite(rootGameObject.transform, rootGameObject.name, listNameSprite);

            if (isKeepCurrentSpriteInAtlas)
            {
                while (atlasGUIDir == "")
                {
                    atlasGUIDir = EditorUtility.OpenFolderPanel("Chose Atlas UGui folder", Application.dataPath, "");
                }

                if(rootRefPrefabManageUI.nameAtlasRef == "")
                    prefabPath = atlasGUIDir.Substring(atlasGUIDir.IndexOf("Assets")) + "/" + rootGameObject.name + "_atlas.prefab";
                else
                    prefabPath = atlasGUIDir.Substring(atlasGUIDir.IndexOf("Assets")) + "/" + rootRefPrefabManageUI.nameAtlasRef + ".prefab";

                GameObject oldPrefab = LoadAsset<GameObject>(prefabPath);

                if (oldPrefab)
                {
                    foreach (var sprt in oldPrefab.GetComponent<ManageSpriteUI>().listUI)
                    {
                        listNameSprite.Add(sprt.name);
                    }
                }
            }

            List<Texture2D> lstTexUI = new List<Texture2D>();

            DirectoryInfo dirInfo = new DirectoryInfo(Application.dataPath);
            foreach (FileInfo pngFile in dirInfo.GetFiles("*.png", SearchOption.AllDirectories))
            {
                string allPath = pngFile.FullName;
                string assetPath = allPath.Substring(allPath.IndexOf("Assets"));
                lstTexUI.Add(LoadAsset<Texture2D>(assetPath));
            }


            foreach (Texture2D texUI in lstTexUI)
            {
                if (texUI == null || texUI.name == "")
                    continue;
                TextureImporter texImporter = AssetImporter.GetAtPath(AssetDatabase.GetAssetPath(texUI)) as TextureImporter;
                if (texImporter == null)
                    continue;

                if (listNameSprite.Contains(texUI.name))
                    textures.Add(texUI);
            }
        }
    }

    public void FindSprite(Transform objTransform, string assetPath, List<string> listNameSprite)
    {
        Image img = objTransform.GetComponent<Image>();

        if (img != null)
        {
            if (img.sprite != null)
            {
                listNameSprite.Add(img.sprite.name);
            }
        }

        int count = objTransform.childCount;
        for (int i = 0; i < count; ++i)
        {
            Transform objTransformChild = objTransform.GetChild(i);
            FindSprite(objTransformChild, assetPath + "/" + objTransformChild.gameObject.name, listNameSprite);
        }
    }

    private static T LoadAsset<T>(string path) where T : Object
    {
        return AssetDatabase.LoadAssetAtPath(path, typeof(T)) as T;
    }

    void PackAtlas()
    {
        if (textures.Count == 0)
            return;

        if (atlasGUIDir == "")
            atlasGUIDir = EditorUtility.OpenFolderPanel("Chose Atlas UGui folder", Application.dataPath, "");

        textureAtlas = new Texture2D(1024, 1024, TextureFormat.RGBA32, false);
        
        //feature convert format of list texture
        foreach (Texture2D tex in textures)
        {
            if (tex == null)
                continue;

            ConfigureForAtlas(AssetDatabase.GetAssetPath(tex));
        }

        rects = textureAtlas.PackTextures(textures.ToArray(), 2);

        byte[] bytes = textureAtlas.EncodeToPNG();

        if (!Directory.Exists(atlasGUIDir))
        {
            Directory.CreateDirectory(atlasGUIDir);
        }

        if (rootRefPrefabManageUI.nameAtlasRef == "")
            pathAtlasTex = atlasGUIDir + "/" + rootGameObject.name + "_atlas.png";
        else
            pathAtlasTex = atlasGUIDir + "/" + rootRefPrefabManageUI.nameAtlasRef + ".png";
        
        pathAtlasTex = pathAtlasTex.Substring(pathAtlasTex.IndexOf("Assets"));
        File.WriteAllBytes(pathAtlasTex, bytes);

        UnityEngine.Object.DestroyImmediate(textureAtlas);

        AssetDatabase.ImportAsset(pathAtlasTex, ImportAssetOptions.ForceUpdate);
        textureAtlas = AssetDatabase.LoadAssetAtPath(pathAtlasTex, typeof(Texture2D)) as Texture2D;

        ConfigureSpriteAtlas(textureAtlas, rects, textures.ToArray());

        //update prefabs atlas
        UpdatePrefabManageSpriteGui();

        if (textureAtlas.width > textureSize || textureAtlas.height > textureSize)
            Debug.LogError("Atlas is large!!!!!!!!!!!!!!!!!!!!!!!!!!");

        //set nameAtlasRef
        if (rootRefPrefabManageUI.nameAtlasRef == "")
            rootRefPrefabManageUI.nameAtlasRef = rootGameObject.name + "_atlas";

        GameObject refUI = LoadAsset<GameObject>(prefabPath);
        component = serializedObject.FindProperty("objectReference");
        component.objectReferenceValue = EditorGUILayout.ObjectField("GUI Ref", refUI, typeof(GameObject), true);

        FindSpriteSetRef(rootGameObject.transform, rootGameObject.name);
    }

    void ConfigureForAtlas(string texturePath)
    {
        TextureImporter texImporter = AssetImporter.GetAtPath(texturePath) as TextureImporter;

        if (texImporter.textureType != TextureImporterType.Sprite)
            texImporter.textureType = TextureImporterType.Sprite;
        if (texImporter.spritePixelsPerUnit != 1.0f)
            texImporter.spritePixelsPerUnit = 1.0f;

        TextureImporterSettings texImporterSettings = new TextureImporterSettings();
        texImporter.ReadTextureSettings(texImporterSettings);
        if (texImporterSettings.readable != true)
        {
            texImporterSettings.readable = true;
            texImporter.SetTextureSettings(texImporterSettings);
        }

        AssetDatabase.ImportAsset(texturePath, ImportAssetOptions.ForceUpdate);
        AssetDatabase.Refresh();
    }

    void ConfigureSpriteAtlas(Texture2D texture, Rect[] uvs, Texture2D[] spriteTex)
    {
        string path = AssetDatabase.GetAssetPath(texture);
        TextureImporter texImporter = AssetImporter.GetAtPath(path) as TextureImporter;

        texImporter.textureType = TextureImporterType.Sprite;
        texImporter.spriteImportMode = SpriteImportMode.Multiple;

        SpriteMetaData[] spritesheetMeta = new SpriteMetaData[uvs.Length];
        for (int i = 0; i < uvs.Length; i++)
        {
            SpriteMetaData currentMeta = new SpriteMetaData();
            Rect currentRect = uvs[i];
            currentRect.x *= texture.width;
            currentRect.width *= texture.width;
            currentRect.y *= texture.height;
            currentRect.height *= texture.height;
            currentMeta.rect = currentRect;
            if (textures[i] != null)
                currentMeta.name = textures[i].name;

            TextureImporter texImporter2 = AssetImporter.GetAtPath(AssetDatabase.GetAssetPath(spriteTex[i])) as TextureImporter;
            if (texImporter2 == null)
                continue;

            //modify sprites
            if (texImporter2.spritePixelsPerUnit != 1.0f)
                texImporter2.spritePixelsPerUnit = 1.0f;
            if (texImporter2.textureFormat != TextureImporterFormat.AutomaticTruecolor)
                texImporter2.textureFormat = TextureImporterFormat.AutomaticTruecolor;
            if (texImporter2.mipmapEnabled != false)
                texImporter2.mipmapEnabled = false;

            AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(spriteTex[i]), ImportAssetOptions.ForceUpdate);
            AssetDatabase.Refresh();
            //end modify sprites

            currentMeta.border = texImporter2.spriteBorder;
            currentMeta.alignment = (int)SpriteAlignment.Center;
            currentMeta.pivot = texImporter2.spritePivot;

            spritesheetMeta[i] = currentMeta;
        }
        texImporter.spritesheet = spritesheetMeta;
        texImporter.spritePixelsPerUnit = 1.0f;
        //texImporter.textureFormat = TextureImporterFormat.AutomaticTruecolor;
        texImporter.alphaIsTransparency = true;
        texImporter.textureCompression = TextureImporterCompression.Uncompressed;
        texImporter.mipmapEnabled = false;

        AssetDatabase.ImportAsset(path, ImportAssetOptions.ForceUpdate);
        AssetDatabase.Refresh();
    }

    void UpdatePrefabManageSpriteGui()
    {
        List<Sprite> lsSprite = new List<Sprite>();

        DirectoryInfo dirInfo = new DirectoryInfo(atlasGUIDir);

        string nameSearch = "";
        if (rootRefPrefabManageUI.nameAtlasRef == "")
            nameSearch = rootGameObject.name + "_atlas*";
        else
            nameSearch = rootRefPrefabManageUI.nameAtlasRef + "*";

        foreach (FileInfo pngFile in dirInfo.GetFiles(nameSearch, SearchOption.AllDirectories))
        {
            string allPath = pngFile.FullName;
            string assetPath = allPath.Substring(allPath.IndexOf("Assets"));
            Object[] sprites = AssetDatabase.LoadAllAssetRepresentationsAtPath(assetPath);

            for (int i = 0; i < sprites.Length; i++)
            {
                lsSprite.Add(sprites[i] as Sprite);
            }
        }


        if (rootRefPrefabManageUI.nameAtlasRef == "")
            prefabPath = atlasGUIDir.Substring(atlasGUIDir.IndexOf("Assets")) + "/" + rootGameObject.name + "_atlas.prefab";
        else
            prefabPath = atlasGUIDir.Substring(atlasGUIDir.IndexOf("Assets")) + "/" + rootRefPrefabManageUI.nameAtlasRef + ".prefab";

        GameObject oldPrefab = LoadAsset<GameObject>(prefabPath);
        if (oldPrefab)
        {
            AssetDatabase.StartAssetEditing();
            oldPrefab.GetComponent<ManageSpriteUI>().listUI = lsSprite;
            AssetDatabase.StopAssetEditing();
            EditorUtility.SetDirty(oldPrefab);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }
        else
        {
            GameObject temp = new GameObject();
            temp.AddComponent<ManageSpriteUI>().listUI = lsSprite;

            PrefabUtility.CreatePrefab(prefabPath, temp);
            Object.DestroyImmediate(temp, false);
        }

    }

    void FindSpriteSetRef(Transform objTransform, string assetPath)
    {
        Image img = objTransform.GetComponent<Image>();

        if (img != null)
        {
            //check sprite default
            if (img.sprite != null)
            {
                if (img.sprite.name == "Background" ||
                    img.sprite.name == "Checkmark" ||
                    img.sprite.name == "DropdownArrow" ||
                    img.sprite.name == "InputFieldBackground" ||
                    img.sprite.name == "Knob" ||
                    img.sprite.name == "UIMask" ||
                    img.sprite.name == "UISprite")
                    Debug.LogError("Image must not be default Unity's sprite: " + assetPath);
            }
            else
            {
                Debug.LogError("Image not set sprite: " + assetPath);
            }
            
            //check pack
            bool isPack = false;
            foreach (Texture2D tex in textures)
            {
                if (tex == null)
                    continue;

                if (img.sprite != null && tex.name == img.sprite.name)
                {
                    isPack = true;
                }
            }

            //add image ui helper
            if (isPack)
            {
                ImageUIHelper imgHelper = img.gameObject.GetComponent<ImageUIHelper>();
                if (imgHelper == null)
                {
                    //Debug.Log(objTransform + " " + assetPath);
                    if (img.sprite != null)
                        imgHelper = img.gameObject.AddComponent<ImageUIHelper>();
                }

                if (imgHelper != null)
                {
                    imgHelper.refManageUI = rootGameObject;
                    imgHelper.oldSpriteSelected = null;
                }
            }
        }

        int count = objTransform.childCount;
        for (int i = 0; i < count; ++i)
        {
            Transform objTransformChild = objTransform.GetChild(i);
            FindSpriteSetRef(objTransformChild, assetPath + "/" + objTransformChild.gameObject.name);
        }
    }

    void ResetImage()
    {
        rootRefPrefabManageUI = rootObject as RefPrefabManageUI;
        rootGameObject = rootRefPrefabManageUI.gameObject;

        List<Sprite> lsSprite = new List<Sprite>();

        string path = Application.dataPath + SpriteDirRelative;
        DirectoryInfo dirInfo = new DirectoryInfo(path);

        foreach (FileInfo pngFile in dirInfo.GetFiles("*.png", SearchOption.AllDirectories))
        {
            string allPath = pngFile.FullName;
            string assetPath = allPath.Substring(allPath.IndexOf("Assets"));
            Object[] sprites = AssetDatabase.LoadAllAssetRepresentationsAtPath(assetPath);

            for (int i = 0; i < sprites.Length; i++)
            {
                lsSprite.Add(sprites[i] as Sprite);
            }
        }

        FindSpriteReset(rootGameObject.transform, lsSprite);
        
        //DestroyImmediate(rootRefPrefabManageUI);
    }    

    void FindSpriteReset(Transform objTransform, List<Sprite> lsSprite)
    {
        ImageUIHelper imgHelper = objTransform.gameObject.GetComponent<ImageUIHelper>();
        if (imgHelper != null)
        {
            string nameTex = imgHelper.nameTexUI;
            
            foreach (Sprite sprite in lsSprite)
            {
                if (sprite.name == nameTex)
                {
                    objTransform.gameObject.GetComponent<Image>().sprite = sprite;
                    DestroyImmediate(imgHelper);
                }
            }
        }

        int count = objTransform.childCount;
        for (int i = 0; i < count; ++i)
        {
            Transform objTransformChild = objTransform.GetChild(i);
            FindSpriteReset(objTransformChild, lsSprite);
        }
    }

    void ResetImageWithoutRemImgUIHelper()
    {
        rootRefPrefabManageUI = rootObject as RefPrefabManageUI;
        rootGameObject = rootRefPrefabManageUI.gameObject;

        List<Sprite> lsSprite = new List<Sprite>();

        string path = Application.dataPath + SpriteDirRelative;
        DirectoryInfo dirInfo = new DirectoryInfo(path);

        foreach (FileInfo pngFile in dirInfo.GetFiles("*.png", SearchOption.AllDirectories))
        {
            string allPath = pngFile.FullName;
            string assetPath = allPath.Substring(allPath.IndexOf("Assets"));
            Object[] sprites = AssetDatabase.LoadAllAssetRepresentationsAtPath(assetPath);

            for (int i = 0; i < sprites.Length; i++)
            {
                lsSprite.Add(sprites[i] as Sprite);
            }
        }

        FindSpriteResetWithoutRemImgUIHelper(rootGameObject.transform, lsSprite);
    }

    void FindSpriteResetWithoutRemImgUIHelper(Transform objTransform, List<Sprite> lsSprite)
    {
        ImageUIHelper imgHelper = objTransform.gameObject.GetComponent<ImageUIHelper>();
        if (imgHelper != null)
        {
            string nameTex = imgHelper.nameTexUI;

            foreach (Sprite sprite in lsSprite)
            {
                if (sprite.name == nameTex)
                {
                    objTransform.gameObject.GetComponent<Image>().sprite = sprite;
                }
            }
        }

        int count = objTransform.childCount;
        for (int i = 0; i < count; ++i)
        {
            Transform objTransformChild = objTransform.GetChild(i);
            FindSpriteResetWithoutRemImgUIHelper(objTransformChild, lsSprite);
        }
    }
}
#endif