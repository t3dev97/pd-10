﻿#if UNITY_EDITOR
using UnityEngine;
using System.Collections;
using UnityEditor;
using System.IO;
using System.Collections.Generic;

public class GenerateAtlasUI : ScriptableWizard 
{
	public Texture2D textureAtlas = null;
    //private string namePrefabAtlas = "AtlasUI";
	public string TagName = "";
	public string AtlasGUIDir = "";//Application.dataPath +"/AtlasGUI";
	//public string pathPrefabManageUI = Application.dataPath +"/Prefabs/ManageUISprite.prefab";
	[HideInInspector]
	public static string prefabPath = "";

	public List<Texture2D> textures;
	

	private int textureSize = 1024;
	//private Texture2D textureAtlas;
	private Rect[] rects;
	private string pathAtlasTex;
	private Texture2D oldTextureAtlas = null;

	void OnWizardUpdate()
	{
		if (oldTextureAtlas != textureAtlas) {
			//folder
			AtlasGUIDir = AssetDatabase.GetAssetPath(textureAtlas);
			AtlasGUIDir =  AtlasGUIDir.Substring (0, AtlasGUIDir.LastIndexOf ("/"));
			//name
			//namePrefabAtlas = textureAtlas.name.Substring(0,textureAtlas.name.LastIndexOf('_'));
            TagName = textureAtlas.name;//.Substring(textureAtlas.name.LastIndexOf('_') + 1);
			//list sprite
			textures.Clear();
//			List<Texture2D> lstTexUI = new List<Texture2D>();
//			DirectoryInfo dirInfo = new DirectoryInfo (Application.dataPath);
//			foreach (FileInfo pngFile in dirInfo.GetFiles("*.png",SearchOption.AllDirectories)) 
//			{
//				string allPath = pngFile.FullName;
//				string assetPath = allPath.Substring (allPath.IndexOf ("Assets"));
//				lstTexUI.AddValue(LoadAsset<Texture2D>(assetPath));
//			}
//			Object[] sprites = AssetDatabase.LoadAllAssetRepresentationsAtPath (AtlasGUIDir + "/" + nameAtlasTex + ".png");
//			for(int i = 0; i < sprites.Length; i++)
//			{
//				Texture2D texUI = lstTexUI.Find (x=>(x.name == sprites[i].name));
//				if(texUI != null)
//					textures.AddValue(texUI);
//			}
		}
		helpString = "Generate Atlas texture";//+"\nChoose name or texture atlas";
		isValid = true;
	}

    [MenuItem("HVL/Atlas UI/PackAtlas")]
	static void PackAtlasTexture()
	{
		ScriptableWizard.DisplayWizard ("Pack Atlas", typeof(GenerateAtlasUI),"Pack!","AddValue Sprite With Tag Key");
	}
	
	void ConfigureForAtlas(string texturePath)
	{
		TextureImporter texImporter = AssetImporter.GetAtPath(texturePath) as TextureImporter;
		TextureImporterSettings texImporterSettings = new TextureImporterSettings();
		
		texImporter.textureType = TextureImporterType.Sprite;
		texImporter.spritePixelsPerUnit = 1.0f;
		texImporter.ReadTextureSettings(texImporterSettings);
		texImporterSettings.readable = true;
		
		texImporter.SetTextureSettings(texImporterSettings);
		AssetDatabase.ImportAsset(texturePath, ImportAssetOptions.ForceUpdate);
		AssetDatabase.Refresh();
	}
	
	void ConfigureSpriteAtlas(Texture2D texture, Rect[] uvs, Texture2D[] spriteTex)
	{
		string path = AssetDatabase.GetAssetPath(texture);
		TextureImporter texImporter = AssetImporter.GetAtPath(path) as TextureImporter;
		
		texImporter.textureType = TextureImporterType.Sprite;
		texImporter.spriteImportMode = SpriteImportMode.Multiple;
		
		SpriteMetaData[] spritesheetMeta = new SpriteMetaData[uvs.Length];
		for(int i = 0; i < uvs.Length; i++)
		{
			SpriteMetaData currentMeta = new SpriteMetaData();
			Rect currentRect = uvs[i];
			currentRect.x *= texture.width;
			currentRect.width *= texture.width; 
			currentRect.y *= texture.height;
			currentRect.height *= texture.height;
			currentMeta.rect = currentRect;
			currentMeta.name = textures[i].name;

			TextureImporter texImporter2 = AssetImporter.GetAtPath(AssetDatabase.GetAssetPath(spriteTex[i])) as TextureImporter;
			if(texImporter2 == null)
				continue;

			//modify sprites
			texImporter2.spritePixelsPerUnit = 1.0f;
			texImporter2.textureFormat = TextureImporterFormat.AutomaticTruecolor;
			texImporter2.mipmapEnabled = false;
			AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(spriteTex[i]), ImportAssetOptions.ForceUpdate);
			AssetDatabase.Refresh();
			//end modify sprites

			currentMeta.border = texImporter2.spriteBorder;
			currentMeta.alignment = (int)SpriteAlignment.Center;
			currentMeta.pivot = texImporter2.spritePivot;
						
			//currentMeta.pivot = new Vector2(currentRect.width / 2, currentRect.height / 2);
			spritesheetMeta[i] = currentMeta;
		}
		texImporter.spritesheet = spritesheetMeta;
		texImporter.spritePixelsPerUnit = 1.0f;
		texImporter.textureFormat = TextureImporterFormat.AutomaticTruecolor;
		texImporter.mipmapEnabled = false;
		
		AssetDatabase.ImportAsset(path, ImportAssetOptions.ForceUpdate);
		AssetDatabase.Refresh();
	}
	
	void OnWizardCreate () {
		if(AtlasGUIDir == "")
			AtlasGUIDir = EditorUtility.OpenFolderPanel( "Chose Atlas UGui folder", Application.dataPath,"");

		textureAtlas = new Texture2D(1024, 1024, TextureFormat.RGBA32, false);

		//feature convert format of list texture
		foreach (Texture2D tex in textures) {
			ConfigureForAtlas(AssetDatabase.GetAssetPath(tex));
		}
		
		rects = textureAtlas.PackTextures(textures.ToArray(),2);
		
		byte[] bytes = textureAtlas.EncodeToPNG();

		if(!Directory.Exists(AtlasGUIDir)){
			Directory.CreateDirectory(AtlasGUIDir);
		}
		//if (namePrefabAtlas == "")
		//	namePrefabAtlas = TagName;
		pathAtlasTex = AtlasGUIDir + "/" + TagName + ".png";//moify affter
		pathAtlasTex = pathAtlasTex.Substring(pathAtlasTex.IndexOf("Assets"));
		File.WriteAllBytes(pathAtlasTex, bytes);
		
		UnityEngine.Object.DestroyImmediate(textureAtlas);
		
		AssetDatabase.ImportAsset(pathAtlasTex,ImportAssetOptions.ForceUpdate);
		textureAtlas = AssetDatabase.LoadAssetAtPath(pathAtlasTex, typeof(Texture2D)) as Texture2D;
		
		ConfigureSpriteAtlas(textureAtlas, rects, textures.ToArray());

		//update prefabs atlas
		UpdatePrefabManageSpriteGui ();

		//update list sprite in editor
		//ProxyDelegate.Instance.SetPrefabAtlas();

		if (textureAtlas.width > textureSize || textureAtlas.height > textureSize)
			Debug.LogError("Atlas is large!!!!!!!!!!!!!!!!!!!!!!!!!!");
	}

	void OnWizardOtherButton()
	{
		if (TagName != "") {


			List<Texture2D> lstTexUI = new List<Texture2D>();// = Resources.FindObjectsOfTypeAll<Texture2D>();// FindObjectsOfType<Texture2D>();

			DirectoryInfo dirInfo = new DirectoryInfo (Application.dataPath);
			foreach (FileInfo pngFile in dirInfo.GetFiles("*.png",SearchOption.AllDirectories)) 
			{
				string allPath = pngFile.FullName;
				string assetPath = allPath.Substring (allPath.IndexOf ("Assets"));
				lstTexUI.Add(LoadAsset<Texture2D>(assetPath));
			}


			foreach(Texture2D texUI in lstTexUI)
			{
				if(texUI.name == "")
					continue;
				TextureImporter texImporter = AssetImporter.GetAtPath(AssetDatabase.GetAssetPath(texUI)) as TextureImporter;
				if(texImporter == null)
					continue;

				if(texImporter.spritePackingTag == TagName)
				{
					textures.Add(texUI);
				}
			}
		}
	}


	void UpdatePrefabManageSpriteGui()
	{
		List<Sprite> lsSprite = new List<Sprite>();
		
		DirectoryInfo dirInfo = new DirectoryInfo (AtlasGUIDir);
        
		foreach (FileInfo pngFile in dirInfo.GetFiles("*.png",SearchOption.AllDirectories)) 
		{
			string allPath = pngFile.FullName;
			string assetPath = allPath.Substring (allPath.IndexOf ("Assets"));
			Object[] sprites = AssetDatabase.LoadAllAssetRepresentationsAtPath (assetPath);
			
			for(int i = 0; i < sprites.Length; i++)
			{
				lsSprite.Add(sprites[i] as Sprite);
			}
		}
//		string directoryPrefab = AtlasGUIDir.Substring (0,AtlasGUIDir.LastIndexOf ("/"));
//		Debug.Log (directoryPrefab);
//		if(!Directory.Exists(directoryPrefab)){
//			Directory.CreateDirectory(directoryPrefab);
//		}
        prefabPath = AtlasGUIDir.Substring(AtlasGUIDir.IndexOf("Assets")) + "/AtlasUI.prefab"; ;
		GameObject oldPrefab = LoadAsset<GameObject>(prefabPath);
		if (oldPrefab)
		{
			AssetDatabase.StartAssetEditing();
			oldPrefab.GetComponent<ManageSpriteUI>().listUI = lsSprite;
			AssetDatabase.StopAssetEditing();
			EditorUtility.SetDirty(oldPrefab);
			AssetDatabase.SaveAssets();
			AssetDatabase.Refresh();
		}
		else
		{
			GameObject temp = new GameObject();
			temp.AddComponent<ManageSpriteUI>().listUI = lsSprite;

			PrefabUtility.CreatePrefab(prefabPath, temp);
			Object.DestroyImmediate(temp, false);
		}

	}

	private static T LoadAsset<T> (string path) where T : Object
	{
		return AssetDatabase.LoadAssetAtPath(path, typeof (T)) as T;
	}
}
#endif