﻿//#define TEST_EDIT_MODE //su dung khi test download asset bundle tren editor
#if UNITY_EDITOR && !TEST_EDIT_MODE
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;
using UnityEngine.UI;

[CustomEditor(typeof(ImageUIHelper))]
public class CustomEditorOverrideUI : Editor
{

    private SerializedProperty component = null;
    //private SerializedProperty component2 = null;
    ReorderableList list;
    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        DrawForProperty();
        serializedObject.ApplyModifiedProperties();
    }

    void DrawForProperty()
    {
        component = serializedObject.FindProperty("refManageUI");
        component.objectReferenceValue = EditorGUILayout.ObjectField("Prefab GUI Ref", component.objectReferenceValue, typeof(GameObject), true);

        if (component.objectReferenceValue == null)
        {
            if (GUILayout.Button("Find"))
            {
                component.objectReferenceValue = Resources.Load<GameObject>("RefManageUI");
            }
        }
        else if (GUILayout.Button("Choose Sprite"))
        {
            SpriteAtlasSelector.Show(component.objectReferenceValue, SelectSprite);
        }

        component = serializedObject.FindProperty("nameTexUI");
        EditorGUILayout.TextField("Texture Key", component.stringValue);
    }

    void SelectSprite(Sprite spriteSelect)
    {
        serializedObject.Update();
        SerializedProperty sp = serializedObject.FindProperty("spriteSelected");

        ImageUIHelper imgUIHelper = target as ImageUIHelper;
        var img = imgUIHelper.gameObject.GetComponent<Image>();
        if (img != null)
        {
            img.sprite = spriteSelect;
            Undo.RecordObject(img, "Image Change");
            EditorUtility.SetDirty(img);
        }
        else
        {
            var img2 = imgUIHelper.gameObject.GetComponent<SpriteRenderer>();
            img2.sprite = spriteSelect;
            Undo.RecordObject(img2, "Image Change");
            EditorUtility.SetDirty(img2);
        }



        sp.objectReferenceValue = spriteSelect;
        SerializedProperty name = serializedObject.FindProperty("nameTexUI");
        name.stringValue = spriteSelect.name;
        serializedObject.ApplyModifiedProperties();
        EditorUtility.SetDirty(serializedObject.targetObject);
    }
}
#endif