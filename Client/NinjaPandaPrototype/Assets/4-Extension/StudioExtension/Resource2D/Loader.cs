﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Studio.BC;

namespace Studio.Resource2D
{
    public class Loader : MonoBehaviour
    {
        private static int _requestCount;
        private static List<LoaderItem> _queue;

        // -----------------------	LIMIT THREAD --------------------
        public const float SecondPerFram = 0.02f;
        public static int MaxConnection = 5; //maximum 10 items loading at the same time
        public static float MaxTimeRequest = 15f;
        private static List<LoaderItem> _pending; //threads in queue
        private static Dictionary<int, int> _timeLoader;
        private static Loader _loader;

        public static void Init()
        {
            if (_loader == null)
            {
                var go = new GameObject("_Resource2DLoader");
                _loader = go.AddComponent<Loader>();
                DontDestroyOnLoad(go);
            }
        }

        public static void Destroy()
        {
            if (_loader != null)
            {
                Destroy(_loader.gameObject);
                _loader = null;
            }
        }

        public static int totalPending { get { return _queue.Count + _pending.Count; } }

        private static bool skippingUpdate
        {
            get { return (_queue == null || _queue.Count == 0) && (_pending == null || _pending.Count == 0); }
        }

        private static void AddPending(LoaderItem item)
        {
            if (_pending == null) { _pending = new List<LoaderItem>(); }
            _pending.Add(item);
        }

        private static void ExecPending()
        {
            if (_queue.Count >= MaxConnection || _pending.Count <= 0) { return; }

            LoaderItem item = _pending[0];
            _pending.RemoveAt(0);
            item.construct();

            _queue.Add(item);

            if (!_timeLoader.ContainsKey(item.index))
            {
                _timeLoader.Add(item.index, 0);
            }
        }


        //-------------------------  POST ---------------------------
        public static LoaderItem<string> Post(string p_url, params object[] p_list)
        {
            return p_list.Length == 0 ? _post(null, p_url) : Post(p_url, new WWWForm(), p_list);
        }

        public static LoaderItem<string> Post(string p_url, WWWForm p_form, params object[] p_list)
        {
            for (int i = 0; i < p_list.Length; i += 2)
            {
                var key = p_list[i];
                var val = p_list[i + 1];

                if (key == null)
                {
                    DebugX.LogWarning("key should not be null or empty, skipping ... ");
                    continue;
                }

                if (val == null)
                {
                    DebugX.LogWarning("value should not be null, sending empty instead ... ");
                    val = "";
                }

                p_form.AddField(key.ToString(), val.ToString());
            }
            return _post(p_form, p_url);
        }

        public static LoaderItem<string> Post(string p_url, Hashtable p_h)
        {
            var form = new WWWForm();
            foreach (DictionaryEntry field in p_h)
            {
                form.AddField(field.Key.ToString(), field.Value != null ? field.Value.ToString() : "");
            }

            return _post(form, p_url);
        }

        private static LoaderItem<string> _post(WWWForm p_form, string p_url)
        {
            return _enqueue<string>(p_url, p_form, LoaderType.Text, null);
        }

        //-----------------------  TEXTURE  -------------------------

        public static Texture2D GetTexture(string p_url, int p_w, int p_h)
        {
            var texture = new Texture2D(p_w, p_h, TextureFormat.ARGB32, false);
            _enqueue<Texture2D>(p_url, texture, LoaderType.Texture2D);
            return texture;
        }

        public static LoaderItem<Texture2D> LoadTexture(string p_url)
        {
            return _enqueue<Texture2D>(p_url, null, LoaderType.Texture2D);
        }

        #region text
        public static LoaderItem<string> LoadText(string p_url)
        {
            return _enqueue<string>(p_url, null, LoaderType.Text);
        }
        #endregion

        //-----------------------  AUDIOCLIP ------------------------
        //-------------------------  BINARY -------------------------
        //----------------------  ASSETBUNDLE -----------------------

        public static LoaderItem<AudioClip> LoadAudio(string p_url)
        {
            return _enqueue<AudioClip>(p_url, null, LoaderType.AudioClip);
        }

        public static LoaderItem<byte[]> LoadBinary(string p_url)
        {
            return _enqueue<byte[]>(p_url, null, LoaderType.Binary);
        }

        public static LoaderItem<AssetBundle> LoadAssetBundle(string p_url, int p_version = 0, uint p_crc = 0)
        {
            LoaderItem<AssetBundle> item = _enqueue<AssetBundle>(p_url,
                                                                 new Dictionary<string, object> {
                                                                     {"version", p_version},
                                                                     {"crc", p_crc}
                                                                 },
                                                                 LoaderType.AssetBundle);
            return item;
        }

        //----------------------  MOVIETEXTURE ----------------------
        //MovieTexture won't work on mobile
        //static public LoaderItem<MovieTexture> LoadMovieTexture(string url)
        //{
        //	return _enqueue<MovieTexture>(url, LoaderType.MovieTexture);
        //}

        //-------------------------  QUEUE ------------------------

        private static LoaderItem<T> _enqueue<T>(string p_url, object p_data, LoaderType p_type, string p_debug = null)
        {
            if (_timeLoader == null) _timeLoader = new Dictionary<int, int>();
            if (_queue == null) { _queue = new List<LoaderItem>(); }

            if (skippingUpdate) { BehaviourX.AddEveryFrameListener(Update); }

            var requestCount = _requestCount++;
            if (requestCount >= int.MaxValue) requestCount = 1;

            var item = new LoaderItem<T> { type = p_type, index = requestCount, _wwwURL = p_url, _wwwData = p_data };

            AddPending(item);

            DebugX.LogWarning(string.Format("LoadRequest[Id={0} BossType={1} URL={2}]{3}",
                                            _requestCount,
                                            p_type,
                                            p_url,
                                            p_debug ?? ""));
            return item;
        }

        private static void OnLoadComplete(LoaderItem p_item)
        {
            if (string.IsNullOrEmpty(p_item.www.error))
            {
                p_item.DispatchProgress(); //force dispatch progress 1

                DebugX.LogWarning(string.Format("LoadComplete[Id={0} BossType={1} URL={2}]---> {3} bytes",
                                                p_item.index,
                                                p_item.type,
                                                p_item.www.url,
                                                p_item.www.bytesDownloaded));
                p_item.DispatchComplete();
            }
            else
            {
                DebugX.LogError(string.Format("LoadError[Id={0} BossType={1} URL={2}]--->{3}",
                                               p_item.index,
                                               p_item.type,
                                               p_item.www.url,
                                               p_item.www.error));
                p_item.DispatchError(p_item.www.error);
            }

            //Keep RAM down
            //DebugX.DebugX.Log("Unload resource Loader");
            //Resources.UnloadUnusedAssets();
            //p_item.www.Dispose();
        }

        public static void Update()
        {
            //DebugX.DebugX.Log("Loader :: Update :: " + _queue.Count + ":" + ":" + "pending :: " + _pending.Count);
            //no pending, no running, why should we call updates ?
            if (skippingUpdate) {
                BehaviourX.RemoveEveryFrameListener(Update);
                return;
            }

            //execute as many pending as possible
            while (_queue.Count < MaxConnection && _pending.Count > 0)
            {
                ExecPending();
            }

            //var str = "Loading ... " + _queue.Count + ":" + _pending.Count;

            for (int i = 0; i < _queue.Count; i++)
            {
                LoaderItem item = _queue[i];

                //str += "\n" + item._wwwURL + ":" + item.progress + ":" + item.bytesLoaded + ":" + item.bytesTotal;
                if (item.www.isDone)
                {
                    if (_timeLoader.ContainsKey(item.index))
                    {
                        _timeLoader.Remove(item.index);
                    }

                    try
                    {
                        OnLoadComplete(item);
                    }
                    catch (Exception e)
                    {
                        item.DispatchError(e);
                        DebugX.LogError(string.Format("Exception[Id={0} BossType={1} URL={2}]--->{3}",
                                                        item.index,
                                                        item.type,
                                                        item.www.url,
                                                        "{" + e + "}"));
                    }
                    _queue[i] = null;
                }
                else if (item.www.progress > 0)
                {
                    if (_timeLoader.ContainsKey(item.index))
                    {
                        _timeLoader.Remove(item.index);
                    }

                    item.DispatchProgress();
                }
                else
                {
                    //check time request, and reload
                    if (item != null && _timeLoader.ContainsKey(item.index))
                    {
                        _timeLoader[item.index] += 1;

                        if ((_timeLoader[item.index] * SecondPerFram > MaxTimeRequest))
                        {
                            item.DispatchError("Request timeout > " + MaxTimeRequest);
                            _timeLoader.Remove(item.index);
                            _queue[i] = null;

                        }
                    }
                }
            }

            //DebugX.DebugX.Log(str);

            _queue.RemoveAll(p_item => p_item == null);
        }
    }

    public enum LoaderType
    {
        Text,
        Texture2D,
        AudioClip,
        AssetBundle,
        MovieTexture,
        Binary,
        Unknown
    }

    public class LoaderGroup
    {
        private static Dictionary<string, LoaderGroup> _map;
        internal int _completeCount;
        internal List<object> _data;

        internal string _id;
        internal List<LoaderItem> _items;
        internal Action<object[], LoaderGroup> _onComplete;
        //internal Action<object, LoaderItem, LoaderGroup> _onError;
        internal Action<float, LoaderGroup> _onProgress;
        internal float _progress;

        public LoaderGroup(string pId = null)
        {
            if (pId != null)
            {
                if (_map == null) { _map = new Dictionary<string, LoaderGroup>(); }
                if (_map.ContainsKey(pId))
                {
                    DebugX.LogWarning("Please Get instead, LoaderGroup with id <" + pId + "> aldready existed");
                }
                _id = pId;
                _map.Add(pId, this);
            }
        }

        public int completeCount { get { return _completeCount; } }
        public List<LoaderItem> items { get { return _items; } }

        public static LoaderGroup Get(string id, bool autoNew = true)
        {
            if (_map != null && _map.ContainsKey(id)) { return _map[id]; }
            return autoNew ? new LoaderGroup(id) : null;
        }

        public LoaderGroup Add(params LoaderItem[] pItems)
        {
            if (_items == null) { _items = new List<LoaderItem>(); }
            if (_data == null) { _data = new List<object>(); }

            for (int i = 0; i < pItems.Length; i++)
            {
                LoaderItem itm = pItems[i];
                if (_items.Contains(itm)) { continue; }

                _items.Add(itm);
                itm._onProgress -= _onItemProgress;
                itm._onProgress += _onItemProgress;

                itm._onError -= _onItemError;
                itm._onError += _onItemError;

                itm._onCompleteInternal -= _onItemComplete;
                itm._onCompleteInternal += _onItemComplete;
            }

            return this;
        }

        void calculateProgress()
        {
            int bytesLoaded = 0;
            int bytesTotal = 0;

            for (var i = 0; i < _items.Count; i++)
            {
                bytesLoaded += _items[i]._bytesLoaded;
                bytesTotal += _items[i]._bytesTotal;
            }

            _progress = bytesLoaded / (float)bytesTotal;
        }
        void _onItemProgress(float progress, LoaderItem item)
        {
            //TODO : only call onProgress once when there are updates
            calculateProgress();
            if (_onProgress != null) _onProgress(_progress, this);
        }

        private void updateGroup()
        {
            if (_completeCount == _items.Count)
            {
                if (_onComplete != null) { _onComplete(_data.ToArray(), this); }
            }
            else
            {
                if (_onProgress != null) { _onProgress(_completeCount / (float)_items.Count, this); }
            }
        }

        private void _onItemComplete(object obj, LoaderItem item)
        {
            _data[completeCount] = item.rawData;
            _completeCount++;
            updateGroup();
        }

        private void _onItemError(object error, LoaderItem item)
        {
            //if (_onError != null) { _onError(error, item, this); }
            _completeCount++;
            updateGroup();
        }

        //TODO : AddValue Remove / Stop / StopAll
    }

    public class LoaderItem
    {
        internal int _bytesLoaded;
        internal int _bytesTotal;
        internal int _fakeSize = 0; // if size not available, assume that the file to be download is 1 KB
        internal Action<object, LoaderItem> _onCompleteInternal; //internal use
        internal Action<object, LoaderItem> _onError;
        internal Action<float, LoaderItem> _onProgress;

        internal object _rawData;
        internal object _wwwData;
        internal string _wwwURL;
        //internal float _progress;

        public int bytesLoaded { get { return _bytesLoaded; } }
        public int bytesTotal { get { return _bytesTotal; } }

        public WWW www { get; internal set; }
        public LoaderType type { get; internal set; }
        public int index { get; internal set; }

        public string id { get; private set; }
        public object userData { get; private set; }
        public float progress { get { return _bytesTotal == 0 ? 0 : _bytesLoaded / (float)_bytesTotal; } }

        public object rawData
        {
            get
            {
                return _rawData
                       ?? (_rawData =
                           (type == LoaderType.Text)
                           ? www.text
                           : (type == LoaderType.Texture2D)
                             ? www.textureNonReadable
                             : (object)www.bytes);
            }
        }

        public void RenewConstruct()
        {
            www = null;
            construct();
        }

        internal void construct()
        {
            if (www != null) { return; }

            string url = _wwwURL;
            object data = _wwwData;

            //_wwwURL = null;
            _wwwData = null;

            if (data == null)
            {
                www = new WWW(url);
                return;
            }

            if (data is Texture2D)
            {
                www = new WWW(url);
                www.LoadImageIntoTexture((Texture2D)data);
                return;
            }

            if (data is WWWForm)
            {
                www = new WWW(url, (WWWForm)data);
                return;
            }

            if (data is Dictionary<string, object>)
            {
                if (type == LoaderType.AssetBundle)
                {
                    var dict = (Dictionary<string, object>)data;
                    www = WWW.LoadFromCacheOrDownload(url, (int)dict["version"], (uint)dict["crc"]);
                    return;
                }
            }

            DebugX.LogWarning("Unsupported wwwData " + _wwwURL + ":" + _wwwData + ":" + this + ":" + type);
        }

        public override string ToString()
        {
            return null; //_rawData.ToString();
        }

        //----------------------  API -----------------------------

        public LoaderItem SetId(string pid)
        {
            id = pid;
            return this;
        }

        public LoaderItem SetData(object p_data)
        {
            userData = p_data;
            return this;
        }

        public LoaderItem SetFakeSize(int psize)
        {
            _fakeSize = psize;
            return this;
        }

        internal void DispatchProgress()
        {
            if (_onProgress != null)
            {
                if (!www.isDone)
                {
                    if (_fakeSize > 0)
                    { //can not access bytesLoaded / size from a cached WWW
                        _bytesLoaded = Mathf.RoundToInt(www.progress * _fakeSize);
                        _bytesTotal = _fakeSize;
                    }
                    else
                    {
                        try
                        {
                            _bytesLoaded = www.bytesDownloaded;
                            _bytesTotal = www.size;
                        }
                        catch
                        {
                            _bytesLoaded = Mathf.RoundToInt(www.progress * _fakeSize);
                            _bytesTotal = _fakeSize;
                        }
                    }

                    _onProgress(progress, this);
                }
            }
        }

        internal void DispatchError(object p_obj)
        {
            if (_onError != null)
            {
                _onError(p_obj, this);
                www.Dispose();
            }
            else
            {
                DebugX.LogError(string.Format("Finish[{0}:{1}]<{2}>::{3}", index, type, www.url, p_obj));
                www.Dispose();
            }
        }

        internal void DispatchErrorTimeout(object p_obj)
        {
            if (_onError != null)
            {
                _onError(p_obj, this);
            }
            else
            {
                DebugX.LogError(string.Format("Finish[{0}:{1}]<{2}>::{3}", index, type, www.url, p_obj));
            }
        }

        internal virtual void DispatchComplete()
        {
            if (_onCompleteInternal != null) { _onCompleteInternal(rawData, this); }
        }
    }

    public class LoaderItem<T> : LoaderItem
    {
        internal Action<T, LoaderItem> _onComplete;

        public T loadedData
        {
            get
            {
                if (rawData != null) { return (T)rawData; }
                return (T)rawData;
            }
        }

        public LoaderItem<T> OnComplete(Action<T, LoaderItem> p_cb)
        {
            _onComplete -= p_cb;
            _onComplete += p_cb;
            return this;
        }

        public LoaderItem<T> OnError(Action<object, LoaderItem> p_cb)
        {
            _onError -= p_cb;
            _onError += p_cb;
            return this;
        }

        public LoaderItem<T> OnProgress(Action<float, LoaderItem> p_cb)
        {
            _onProgress -= p_cb;
            _onProgress += p_cb;
            return this;
        }

        internal override void DispatchComplete()
        {
            if (_onComplete != null)
            {
                _onComplete(loadedData, this);
                www.Dispose();
            }
            else
            {
                www.Dispose();
            }

            base.DispatchComplete(); //dispatch complete internal
        }
    }
}

