﻿using System.Collections;
using System.Runtime.CompilerServices;
using Studio.Timer;
using Studio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

#if (UNITY_WSA_8_1 || UNITY_WP_8_1 || UNITY_WINRT_8_1) && !UNITY_EDITOR
using UnityEngine.Windows;
#else
using System.IO;
#endif

namespace Studio.Resource2D
{
    public class GameResource2
    {
        public bool NeedDecrypt = true;
        public string ParentFolder;

        public GRStatus Status { get; internal set; }
        public Action<string> OnLoadConfigErrorAction;

        private GRConfig _config;
        private GRQueue _queue;
        private Dictionary<string, AGameResource> _localConfig;
        private byte[] _newConfigData;

        private string _mapUrl;
        private Action _onReady;

        private string _versionFileName;
        private int _retryLoadConfig = 1;

        private const bool hasStreamingAsset = true;
        private List<string> _resourcesHasUpdate = new List<string>();

        public static string LocalUrl
        {
#if UNITY_EDITOR || UNITY_STANDALONE
            get
            {
                return GRUtils2.GetCachePath() + "/";
            }
#else
            get { return GRUtils2.GetCachePath() + "/"; }
#endif
        }

        public static string StreamingAssetsUrl
        {
            get
            {
                return Application.streamingAssetsPath + "/";
            }
        }

        // INITIALIZE
        public GameResource2 Initialize(string mapUrl, string signature, string resourceURL, Action onReady, bool reInit = false)
        {
            if (!reInit && Status != GRStatus.NOT_INIT)
            {
                DebugX.LogWarning("GameResource.Init should be call only once, current status <" + Status + ">");
                if (onReady != null) onReady();
                return this;
            }

            Loader.Init();

            this._versionFileName = signature;
            this.Status = GRStatus.NOT_INIT;
            this._mapUrl = mapUrl;
            this._onReady = onReady;

            if (hasStreamingAsset) {
                _resourcesHasUpdate = GetLocalUpdateList();
            }

            _config = new GRConfig();
            _cacher = new Dictionary<string, GRCacheGroup>();
            _queue = new GRQueue(Cache, OnPreloadReady, OnLoadConfigErrorAction, NeedDecrypt, ParentFolder)
            {
                BaseURLs = resourceURL
            };

            LoadConfig();
            return this;
        }

        public void LoadConfig()
        {
            Status = GRStatus.LOAD_CONFIG;

            var url = _mapUrl + "?" + Utils.GenerateFakeVersionForWeb();

            DebugX.LogError("LoadConfig:: " + url);

            Loader.LoadBinary(url)
                .OnComplete(OnConfigComplete)
                .OnError(OnConfigError);
        }

        public void LocalLocalConfig()
        {
            if (_resourcesHasUpdate.Contains(_versionFileName) && !File.Exists(LocalUrl + _versionFileName)) {
                _resourcesHasUpdate.Remove(_versionFileName);
            }

            var urlLocal = GetResourcePath(_versionFileName);

            DebugX.LogError("LocalLocalConfig:: " + urlLocal);

#if UNITY_EDITOR
            OnLocalConfigComplete(urlLocal.LoadFile(), null);
            return;
#endif

            Loader.LoadBinary(urlLocal)
                .OnComplete(OnLocalConfigComplete)
                .OnError(OnLocalConfigError);
        }

        public void CheckUpdate()
        {
            var updateList = new List<AGameResource>();
            foreach (var dict in _config.dict)
            {
                if (!_localConfig.ContainsKey(dict.Key)) {
                    updateList.Add(dict.Value);
                    continue;
                }

                var oldDict = _localConfig[dict.Key];
                if (oldDict.signature.Equals(dict.Value.signature) == false) {
                    updateList.Add(dict.Value);
                }
            }

            DebugX.LogError("=========> CheckUpdate");

            if (updateList.Count == 0)
            {
                _queue.Finished();
                return;
            }

            foreach (var ar in updateList) {

                AddLocalUpdate(ar.id);

                DebugX.LogError("=========> load disk: " + ar.id);
                _queue.Add(ar);
            }
        }

        #region HANDLERS

        protected virtual void OnConfigError(object error, LoaderItem ldi)
        {
            Status = GRStatus.LOAD_CONFIG_ERROR;

            DebugX.Log("Will try again in 1 seconds ... ");

            _retryLoadConfig += 1;
            Async.Call(LoadConfig, 2);
        }

        protected virtual void OnConfigComplete(byte[] content, LoaderItem ldi)
        {
            _newConfigData = content.CopyBytes();

            var decoded = content.DecryptText(NeedDecrypt);
            if (string.IsNullOrEmpty(decoded)) {
                DebugX.LogWarning("Invalid configuration (should not be null or empty) ... ");
                return;
            }

            _config.dict = decoded.ParseConfig();
            LocalLocalConfig();
        }

        protected virtual void OnLocalConfigError(object error, LoaderItem ldi)
        {
            Status = GRStatus.LOAD_CONFIG_ERROR;

            DebugX.Log("Will try again in 1 seconds ... ");
            _retryLoadConfig += 1;
            Async.Call(LocalLocalConfig, 2);
        }

        protected virtual void OnLocalConfigComplete(byte[] content, LoaderItem ldi)
        {
            var decoded = content.DecryptText(NeedDecrypt);
            if (string.IsNullOrEmpty(decoded))
            {
                DebugX.LogWarning("Invalid configuration (should not be null or empty) ... ");
                return;
            }

            _localConfig = decoded.ParseConfig();

            Status = GRStatus.LOAD_RESOURCE;
            CheckUpdate();
        }

        #endregion

        public GameResource2 SetRamLimit(int bytes)
        {
            _queue.maxLoadRam = bytes;
            return this;
        }

        public GameResource2 SetThreadLimit(int max)
        {
            _queue.maxThread = max;
            return this;
        }

        public GameResource2 SetMaxRetry(int max)
        {
            _queue.maxRetry = max;
            return this;
        }

        private List<string> GetLocalUpdateList()
        {
            var url = LocalUrl + "Updater.txt";
            DebugX.Log("Read Url: " + url);
            var lst = new List<string>();
            DebugX.Log("lst: "+ lst);
            if (File.Exists(url))
            {
                DebugX.Log("Read...");
                lst = File.ReadAllText(url).Split('\n').ToList();
            }
            DebugX.Log("lst after read: "+lst);
            return lst;
        }

        private void SaveLocalUpdateList(List<string> lst)
        {
            var url = LocalUrl + "Updater.txt";
            var str = "";
            for (var i = 0; i < lst.Count; i++)
            {
                if (i > 0) str += "\n";
                str += lst[i];
            }
            DebugX.Log("LocalUrl: " + url);
            DebugX.Log("Str: " + str);
            File.WriteAllText(url, str);
        }

        private void AddLocalUpdate(string id)
        {
            if (!_resourcesHasUpdate.Contains(id)) {
                _resourcesHasUpdate.Add(id);
            }
        }

        private void OnPreloadReady()
        {
            DebugX.Log("1");
            Status = GRStatus.READY;
            DebugX.Log("2");
            if (_onReady == null) return;
            DebugX.Log("3");
            AddLocalUpdate(_versionFileName);
            DebugX.Log("4");
            SaveLocalUpdateList(_resourcesHasUpdate);
            DebugX.Log("5");
            //save new config
            if (_newConfigData != null)
            {
                DebugX.Log("6");
                var urlLocal = LocalUrl + _versionFileName;
                DebugX.Log("7");
                _newConfigData.WriteFile(urlLocal, true);
                DebugX.Log("8");
                _newConfigData = null;
                DebugX.Log("9");
            }
            DebugX.Log("10");
            //prevent accidental clear of callbacks
            var tmp = _onReady;
            DebugX.Log("11");
            _onReady = null;
            DebugX.Log("12");
            tmp();
            DebugX.Log("13");
        }

        //PUBLIC APIs
        public string GetText(string id)
        {
            var grText = GetAResource<GRText>(id);
            if (grText == null) return null;

            var text = grText.Text;

            if (string.IsNullOrEmpty(text))
            {
                DebugX.LogWarning(
                    "Text is null, might not have been loaded, should call preloadResource & wait for complete :: " + id);
            }

            return text;
        }

        public Texture2D GetTexture(string id, bool createTemp = true)
        {
            var image = GetAResource<GRImage>(id);
            if (image == null) return null;

            var tex = image.Texture;
            if (tex != null) return tex;

            //DebugX.Log("Cache missed ---> " + image.id + "--- trying to load ... ");
            if (createTemp) tex = image.CreateTempTexture();

            //Trigger load if not yet in queue
            var ext = id.GetExtension();
            var url = GetResourcePath(id).Replace(ext, "unity3d");
            var isLocal = !_resourcesHasUpdate.Contains(id) && !File.Exists(LocalUrl + url);

            image.Url = GetResourcePath(id);
            image.IsLocal = isLocal;

            _queue.Add(image);
            return tex;
        }

        public byte[] GetByteTexure(string id)
        {
            var image = GetAResource<GRImage>(id);
            if (image == null) return null;

            var tex = image.ByteDatas;
            if (tex != null) return tex;

            _queue.Add(image);
            return tex;
        }

        public GameResource2 LoadText(string id, Action<string> onComplete)
        {
            var vid = getValidId(id, GRType.Text);
            if (string.IsNullOrEmpty(vid))
            {
                if (onComplete != null) onComplete(null);
                return null;
            }

            var text = GetAResource<GRText>(vid);
            if (text == null)
            {
                if (onComplete != null) onComplete(null);
                return null;
            }

            if (text.status == GRIStatus.Error)
            {
                DebugX.LogWarning("LoadText <" + id + "> 's status is <ErrorString> so no more efforts have been made");
                if (onComplete != null) onComplete(null);
                return this;
            }

            if ((text.status == GRIStatus.Complete) || (text.status == GRIStatus.TempRam))
            {
                if (onComplete != null) onComplete(text.Text);
                return this;
            }

            //Trigger load if not yet in queue
            if (text.cacheType == GRCacheType.Disk)
            {
                text.cacheType = GRCacheType.RamCache;
                text.status = GRIStatus.Free;
            }

            if (onComplete != null)
            {
                text.onComplete -= onComplete;
                text.onComplete += onComplete;
            }

            var ext = vid.GetExtension();
            var url = GetResourcePath(vid).Replace(ext, "unity3d");
            var isLocal = !_resourcesHasUpdate.Contains(vid) && !File.Exists(LocalUrl + url);

            text.Url = GetResourcePath(vid);
            text.IsLocal = isLocal;

            _queue.Add(text);
            return this;
        }

        public Texture2D LoadTexture(string id, Action<Texture2D> onComplete)
        {
            var vid = getValidId(id, GRType.Image);
            if (String.IsNullOrEmpty(vid)) return null;

            var image = GetAResource<GRImage>(vid);
            if (image == null)
            {
                if (onComplete != null) onComplete(null);
                return null;
            }

            if (image.status == GRIStatus.Error)
            {
                DebugX.LogWarning("LoadTexture <" + id + "> 's status is <ErrorString> so no more efforts have been made");

                if (onComplete != null) onComplete(image.Texture);
                return image.Texture;
            }

            var tex = image.Texture;
            if (tex != null && ((image.status == GRIStatus.Complete) || (image.status == GRIStatus.TempRam)))
            {
                //DebugX.Warn("Early complete by status <" + image.status + ">");
                if (onComplete != null) onComplete(tex);
                return tex;
            }

            //Trigger load if not yet in queue
            if (image.cacheType == GRCacheType.Disk)
            {
                image.cacheType = GRCacheType.RamCache;
                image.status = GRIStatus.Free;
            }

            if (tex == null) tex = image.CreateTempTexture();

            if (onComplete != null)
            {
                image.onComplete -= onComplete;
                image.onComplete += onComplete;
            }

            //DebugX.Log("Loading ... " + image.id + "::: " + image.status);
            var ext = vid.GetExtension();
            var url = GetResourcePath(vid).Replace(ext, "unity3d");
            var isLocal = !_resourcesHasUpdate.Contains(vid) && !File.Exists(LocalUrl + url);

            image.Url = GetResourcePath(vid);
            image.IsLocal = isLocal;

            _queue.Add(image);

            return tex;
        }

        public GameResource2 Preload(Action onComplete, params string[] ids)
        {
            for (var i = 0; i < ids.Length; i++)
            {
                var vid = getValidId(ids[i]);
                if (String.IsNullOrEmpty(vid)) continue;

                var ar = _config.dict[vid];
                if (ar.cacheType == GRCacheType.Disk)
                {
                    ar.cacheType = GRCacheType.RamCache;
                    ar.status = GRIStatus.Free;
                }

                if (ar.status == GRIStatus.Error)
                {
                    DebugX.LogWarning("Preload <" + vid +
                                      "> 's status is <ErrorString> so no more efforts have been made");
                    continue;
                }

                if (ar.status == GRIStatus.Free)
                {
                    ar.Url = vid;
                    ar.IsLocal = true;

                    _queue.Add(ar);
                }
            }

            if (_queue.IsFinished)
            {
                DebugX.LogWarning("Early Complete from status ... status " + Status + " should be <" + GRStatus.READY +
                                  ">");
                if (onComplete != null) onComplete();
            }
            else
            {
                Status = GRStatus.LOAD_RESOURCE;
                if (onComplete != null)
                {
                    _onReady -= onComplete;
                    _onReady += onComplete;
                }
            }
            return this;
        }

        protected List<AGameResource> Find(Func<AGameResource, bool> func)
        {
            var result = new List<AGameResource>();
            foreach (var item in _config.dict)
            {
                if (func(item.Value)) result.Add(item.Value);
            }
            return result;
        }

        public GameResource2 SetCachePreloadType(GRCacheType? cacheType, GRPreloadType? preloadType, params string[] ids)
        {
            for (var i = 0; i < ids.Length; i++)
            {
                var vid = getValidId(ids[i]);
                if (string.IsNullOrEmpty(vid)) continue;

                var ar = _config.dict[vid];
                if (cacheType != null) ar.cacheType = cacheType.Value;
                if (preloadType != null) ar.preloadType = preloadType.Value;
            }

            return this;
        }

        public void CheckPreload()
        {
            var items = _config.preloadList;
            if (items.Count == 0)
            {
                _queue.Finished();
                return;
            }

            for (var i = 0; i < items.Count; i++)
            {
                var ar = items[i];
                if (ar.status == GRIStatus.Free)
                {
                    _queue.Add(ar);
                    //DebugX.LogError("==add queue:: " + ar.id);
                }
            }

            //DebugX.Log("Preload count ---> " + items.Count);
        }

        public void CancelPreload(params string[] ids)
        {
            DebugX.LogWarning("Not yet implemented");
        }

        //CACHER
        private Dictionary<string, GRCacheGroup> _cacher;

        protected GRCacheGroup GetCacheGroup(string id)
        {
            if (_cacher.ContainsKey(id)) return _cacher[id];
            var c = new GRCacheGroup {id = id};
            _cacher.Add(c.id, c);
            return c;
        }

        private void Cache(AGameResource ar)
        {
            GRCacheGroup grcg;
            if (ar == null)
            {
                DebugX.LogWarning("ar should not be null");
                return;
            }
            if (String.IsNullOrEmpty(ar.group) || ar.cacheType == GRCacheType.RamPersistent)
                return; //ignore Ram-persistent

            //DebugX.Log("Cache ---> " + ar.id + ":::" + ar.group);

            if (_cacher.ContainsKey(ar.group))
            {
                grcg = _cacher[ar.group];
            }
            else
            {
                grcg = new GRCacheGroup {id = ar.group};
                _cacher.Add(grcg.id, grcg);
            }

            grcg.Add(ar);
        }

        // LOADING INFORMATION
        public int nLoading
        {
            get { return _queue != null ? _queue.loadingCount : 0; }
        }

        public int nLoaded
        {
            get { return _queue != null ? _queue.completeCount : 0; }
        }

        public int nPending
        {
            get { return _queue != null ? _queue.pendingCount : 0; }
        }

        public int nTotal
        {
            get { return _queue != null ? _queue.pendingCount + _queue.completeCount + _queue.loadingCount : 0; }
        }

        public int bytesLoaded
        {
            get { return _queue != null ? _queue.bytesLoaded : 0; }
        }

        public int bytesTotal
        {
            get { return _queue != null ? _queue.bytesTotal : 0; }
        }

        public int ramUsage
        {
            get { return _queue != null ? _queue.bytesRam : 0; }
        }

        public float progress
        {
            get
            {
                return _queue != null ? (_queue.bytesTotal == 0 ? 1 : (_queue.bytesLoaded/(float) _queue.bytesTotal)) : 0;
            }
        }

        // INTERNAL UTILS
        public string getValidId(string id, GRType type = GRType.Unknown)
        {
            if (string.IsNullOrEmpty(id))
            {
                DebugX.LogWarning("getValidId ErrorString - id should not be null or empty");
                return null;
            }

            if (_config == null || _config.dict == null)
            {
                DebugX.LogWarning(
                    "getValidId ErrorString - map not yet created, please wait for map.csv to loaded & parse");
                return null;
            }

            id = id.Replace("\\", "/"); //.ToLowerInvariant();//.Replace("/", "_")

            //use simplified id if available
            if (_config.dict.ContainsKey(id)) return id;

            //searching for the id
            var exts = type.Extensions();
            for (var i = 0; i < exts.Length; i++)
            {
                //check localize id first
                var vid = id + "." + exts[i];
                if (_config.dict.ContainsKey(vid)) return vid;

                /*//check id
                vid = id + "." + exts[i];

                //DebugX.Log("======> vid:: " + vid);
                if (_config.dict.ContainsKey(vid)) return vid;*/
            }

            DebugX.LogWarning("vId not found for <" + id + "> with extensions=" + exts.Join());
            return null;
        }

        private T GetAResource<T>(string id) where T : AGameResource
        {
            var vid = getValidId(id);
            if (String.IsNullOrEmpty(vid)) return null;

            var ar = _config.dict[vid];
            if (ar.GetType() == typeof (T)) return (T) ar;

            DebugX.LogWarning("GetAResource ErrorString <" + id + "> ---> vId=<" + vid + "> is " + ar.type + " not a " +
                              typeof (T));
            return null;
        }

        public string GetResourcePath(string relativePath)
        {
            if (!hasStreamingAsset || _resourcesHasUpdate.Contains(relativePath))
            {
#if UNITY_EDITOR
                return LocalUrl + relativePath;
#elif UNITY_WEBGL
                return AssetBundleManager2.BaseDownloadingURL + relativePath;
#else
                return "file://" + LocalUrl + relativePath;
#endif
            }
            else
            {
#if UNITY_EDITOR
                return StreamingAssetsUrl + relativePath;
#else
#if UNITY_IOS
                return "file://" + StreamingAssetsUrl + relativePath;
#else
                return StreamingAssetsUrl + relativePath;
#endif
#endif
            }
        }
    }

    public enum GRStatus
    {
        NOT_INIT, // init not yet called
        LOAD_CONFIG, // loading configuration file
        LOAD_CONFIG_ERROR, // loading error, retrying ...


        //CACHE_RESOURCE,     // downloading for new / updated resource to local storage

        READY, // local cache is ready, wait for LoadResource requests
        LOAD_RESOURCE // downloading some resource from user request
    }

    public enum GRType
    {
        Unknown,
        Text,
        Image,
        Sound,
        Bytes
    }

    public enum GRPreloadType
    {
        OnDemand, // only load when being request
        Queue, // queue up and load while the game is running
        Preload, // always preload
    }

    public enum GRCacheType
    {
        Disk, // cache to disk only, do not decode or put in RAM - useful for disk eagerly preload
        RamPersistent, // keep in ram forever once being loaded, never destroy
        None, // always reload when being request, do not cache - useful for checking version / update

        RamCache, // save to temporary Ram - useful for in-use resource
    }

    public enum GRIStatus
    {
        Free, // not yet in queue
        Queue, // in queue, wait to be load
        Loading, // loading
        Complete, // load completed, have been cache to persistent RAM
        Error, // load error, wait for retry

        //TODO      : consider should we keep a reference for loaded assets to prevent null-ref when retrieving asset in onQueueComplete 
        //it's always work by using larger cache to prevent premature memory collection

        TempRam // unload from persistent, in dynamic RAM, might have been collected --> Free
    }


    internal class GRConfig
    {
        public Dictionary<string, AGameResource> dict;

        public GRConfig()
        {
            dict = new Dictionary<string, AGameResource>();
        }

        public List<AGameResource> preloadList
        {
            get { return dict.Values.Where(item => item.preloadType == GRPreloadType.Preload).ToList(); }
        }

        public List<string> getDictKeys()
        {
            return dict.Keys.ToList();
        }

    }

    internal class GRQueueItem
    {
        //public GRIStatus status;
        public int retry;
        public int bytesLoaded;
    }

    internal class GRQueue
    {
        //Load Queue
        public int maxRetry = 5; //DO NOT ALLOW RETRY - Will check later
        public int maxLoadRam = 1024*1024*10; //Max : 1MB used to store temporary download
        public int maxThread = 10;
        public bool needDecrypt;
        public string parentFolder;

        public string BaseURLs;

        private Action onComplete;
        private Action<string> onError;

        private Action<AGameResource> onGRDecode;
        private List<AGameResource> _pending;
        private List<AGameResource> _loading;
        private List<AGameResource> _finished;

        public int pendingCount
        {
            get { return _pending.Count; }
        }

        public int loadingCount
        {
            get { return _loading.Count; }
        }

        public int completeCount
        {
            get { return _finished.Count; }
        }

        public int bytesRam;
        public int bytesLoaded;
        public int bytesTotal;

        public GRQueue(Action<AGameResource> onGRDecode, Action onComplete, Action<string> onError, bool needDecrypt, string parentFolder)
        {
            _pending = new List<AGameResource>();
            _loading = new List<AGameResource>();
            _finished = new List<AGameResource>();

            this.onError = onError;
            this.onComplete = onComplete;
            this.onGRDecode = onGRDecode;
            this.needDecrypt = needDecrypt;
            this.parentFolder = parentFolder;
        }

        public bool IsFinished
        {
            get { return _loading.Count == 0 && _pending.Count == 0; }
        }

        public void Add(AGameResource ar)
        {
            if (ar.status == GRIStatus.Queue && _pending.Contains(ar))
            {
                //prioritize
                _pending.Remove(ar);
                _pending.Add(ar);
                return;
            }

            if (ar.status != GRIStatus.Free && ar.status != GRIStatus.TempRam) {
                return;
            }
            
            ar.status = GRIStatus.Queue;
            ar.retry = 0;
            ar.bytesLoaded = 0;
            _pending.Add(ar);

            bytesTotal += ar.bytesTotal;

            CheckQueue();
        }

        public void Finished()
        {
            bytesTotal = 0;
            _finished.Clear();

            if (onComplete != null) onComplete();
        }

        private void CheckQueue()
        {
            if (IsFinished)
            {
                Finished();
                return;
            }

            if (_pending.Count == 0 || bytesRam >= maxLoadRam || loadingCount >= maxThread) return;

            var item = _pending.RemoveLast();
            _loading.Add(item);
            LoadGR(item);
        }

        private void LoadGR(AGameResource ar)
        {
            //DebugX.Log("LoadGR ----> " + ar.id + ":" + bytesLoaded + ":" + bytesTotal);

            var ext = ar.id.GetExtension();
            var url = "";
            if (ar.IsLocal)
            {
                url = ar.Url;
                
#if UNITY_EDITOR
                url = url.Replace(ext, "unity3d");
                UpdateGRComplete(ar, url.LoadFile());
                return;
#endif
            }
            else
            {
              
                url = BaseURLs + "/" + ar.id + "?sign=" + ar.signature;
            }

            url = url.Replace(ext, "unity3d");

            ar.bytesLoaded = 0;
            bytesRam += ar.bytesTotal;
            ar.status = GRIStatus.Loading;

            Loader.LoadBinary(url)
                .OnProgress(OnGRProgress)
                .OnComplete(OnGRComplete)
                .OnError(OnGRError)
                .SetData(ar)
                .SetFakeSize(ar.bytesTotal);
        }

        private void OnGRProgress(float pct, LoaderItem li)
        {
            var ar = (AGameResource) li.userData;
            bytesLoaded += li.bytesLoaded - ar.bytesLoaded;
            ar.bytesLoaded = li.bytesLoaded;
        }

        private void UpdateGRComplete2(AGameResource ar)
        {
            bytesLoaded += ar.bytesTotal - ar.bytesLoaded;

            ar.status = GRIStatus.Complete;

            _loading.Remove(ar);
            _finished.Add(ar);

            CheckQueue();
        }

        private void UpdateGRComplete(AGameResource ar, byte[] ba)
        {
            bytesLoaded += ar.bytesTotal - ar.bytesLoaded;

            ar.status = GRIStatus.Complete;

            _loading.Remove(ar);
            _finished.Add(ar);

//#if !UNITY_WEBPLAYER && !UNITY_WEBGL
            if (!ar.IsLocal && ar.writeToDisk)
            {
                ba.WriteFile(ar.id.GetFileURI(ar.signature, parentFolder), true);
            }
//#endif

            if (ar.decodeToRam)
            {
                ar.Decode(ba.Decrypt(ar.type != GRType.Text, needDecrypt));
                onGRDecode(ar);
            }

            CheckQueue();
        }

        private void OnGRComplete(byte[] ba, LoaderItem li)
        {
            var ar = (AGameResource) li.userData;

            if (li.bytesTotal != ar.bytesTotal)
            {
                DebugX.LogWarning("Invalid <" + ar.id + "> filesize, originaly <" + ar.bytesTotal + "> get <" +
                                  li.bytesTotal + "> " + li.bytesTotal);
            }

            if (ar.type == GRType.Text)
            {
                var baDecrypted = new byte[ba.Length];
                ba.CopyTo(baDecrypted, 0);
                baDecrypted.Decrypt(false, needDecrypt);
            }

            bytesRam -= ar.bytesTotal;
            UpdateGRComplete(ar, ba);
        }

        private void OnGRCompleteText(string ba, LoaderItem li)
        {
            OnGRComplete(Encoding.UTF8.GetBytes(ba), li);
        }

        private void OnGRError(object error, LoaderItem li)
        {
            var ar = (AGameResource) li.userData;
            //var qi = dict[ar];

            ar.retry++;
            //loadingCount--;
            _loading.Remove(ar);
            bytesRam -= ar.bytesTotal;
            bytesLoaded -= ar.bytesLoaded;

            //DebugX.Warn("[Retry : " + ar.retry + "]" + li._wwwURL + "\n --->" + error);

            if (ar.retry <= maxRetry)
            {
                ar.status = GRIStatus.Queue;
                _pending.Add(ar);
            }
            else
            {
                //update as if it's complete
                bytesLoaded += ar.bytesTotal;
                ar.status = GRIStatus.Error;
                ar.OnLoadErrorAction();

                _finished.Add(ar);

                if (onError != null) onError(ar.id + " :: " + error);
            }

            CheckQueue();
        }
    }

    public class GRCacheGroup
    {
        public string id;
        public int ramCostLimit = 1*1024*1024; //1 MB
        public int countLimit;
        public List<AGameResource> list;

        public void Add(params AGameResource[] arList)
        {
            for (var i = 0; i < arList.Length; i++)
            {
                var ar = arList[i];

                if (ar == null)
                {
                    DebugX.LogWarning("GRCacheGroup<" + id +
                                      ">.AddValue() ErrorString - GameResource should not be null");
                    return;
                }

                if (ar.group != id)
                {
                    DebugX.LogWarning("GRCacheGroup<" + id + ">.AddValue() ErrorString - Group does not matched <" +
                                      ar.group + ">");
                    return;
                }

                if (ar.LoadedContent == null)
                {
                    DebugX.LogWarning("GRCacheGroup<" + id +
                                      ">.AddValue() ErrorString - Loaded content should not be null <" + ar.id + ">");
                    return;
                }

                if (list == null) list = new List<AGameResource>();
                if (list.Contains(ar)) list.Remove(ar);
                list.Add(ar);

                //DebugX.Log("Cache ---> " + ar.id + ":" + ar.group + ":::" + ar.LoadedContent);
            }

            Check();
        }

        private void Check()
        {
            var cnt = list.Count;
            var cost = 0;
            var hasNull = false;

            for (var i = cnt - 1; i >= 0; i--)
            {
                if (list[i] != null) cost += list[i].RamCost;
                if (((countLimit > 0) && i < (cnt - countLimit)) || (cost > ramCostLimit))
                {
                    list[i].ReleaseCacheHR();
                    list[i] = null;
                    hasNull = true;
                }
            }

            //DebugX.Log(id + "Count=" + cnt + ":RamCost=" + (cost/1024) + "kb /" + (ramCostLimit/1024) + "kb" );
            if (hasNull) list.RemoveAll(item => item == null);
        }
    }

    public class AGameResource
    {
        public string id;
        public string group;
        public string signature;
        public bool needDownload;

        public int bytesTotal;

        //temp variables - for download only
        public int retry;
        public int bytesLoaded;

        public GRIStatus status;
        public GRCacheType cacheType;
        public GRPreloadType preloadType;
        public WeakReference cache;

        public string Url;
        public bool IsLocal;

        public bool writeToDisk
        {
            get { return cacheType != GRCacheType.None; }
        }

        public bool decodeToRam
        {
            get { return cacheType != GRCacheType.Disk; }
        }

        public virtual GRType type
        {
            get { return GRType.Unknown; }
        }

        public virtual void Decode(byte[] decryptedBA)
        {
        }

        public virtual void OnLoadErrorAction()
        {

        }

        public virtual void ReleaseCacheHR()
        {
        }

        public virtual int RamCost
        {
            get { return bytesTotal; }
        }

        public virtual object LoadedContent
        {
            get { return null; }
        }
    }

    internal class GRText : AGameResource
    {
        internal string _text;
        public Action<string> onComplete;

        public override GRType type
        {
            get { return GRType.Text; }
        }

        public override int RamCost
        {
            get { return 20 + (_text.Length << 1); }
        }

        public override object LoadedContent
        {
            get { return Text; }
        }

        public string Text
        {
            get { return _text ?? ((cache != null) ? (string) cache.Target : null); }
        }

        //Decode byteArray to local variable (hard reference)
        public override void Decode(byte[] decryptedBA)
        {

            if (!string.IsNullOrEmpty(_text))
                DebugX.LogWarning("RamCache problem ... multiple calls to ramCache <" + id + ">");
            if (decryptedBA == null || decryptedBA.Length == 0)
            {
                _text = "";
            }
            else
            {
                _text = Encoding.UTF8.GetString(decryptedBA, 0, decryptedBA.Length);
                if (string.IsNullOrEmpty(_text)) return;
            }

            var temp = onComplete;
            onComplete = null;
            if (temp != null) temp(_text);
        }

        public override void OnLoadErrorAction()
        {
            base.OnLoadErrorAction();

            var temp = onComplete;
            onComplete = null;
            if (temp != null) temp("");
        }

        //Clear hard reference, save to RamCache
        public override void ReleaseCacheHR()
        {
            if (cacheType == GRCacheType.RamPersistent) return;
            status = GRIStatus.TempRam;
            cache = new WeakReference(_text);
            _text = null;
        }
    }

    internal class GRImage : AGameResource
    {
        internal Texture2D _texture;

        public Texture2D Texture
        {
            get { return _texture ?? ((cache != null) ? (Texture2D) cache.Target : null); }
        }

        internal byte[] _buyteDatas;

        public byte[] ByteDatas
        {
            get { return _buyteDatas; }
        }

        //other properties
        public int width;
        public int height;
        public bool transparent;
        public Action<Texture2D> onComplete;

        public override GRType type
        {
            get { return GRType.Image; }
        }

        public override void Decode(byte[] decryptedBA)
        {
            _buyteDatas = decryptedBA;

            if (_texture == null)
            {
                _texture = new Texture2D(4, 4, transparent ? TextureFormat.ARGB32 : TextureFormat.RGB24, false)
                {
                    name = id,
                    wrapMode = TextureWrapMode.Clamp,
                    filterMode = FilterMode.Bilinear
                };
                //DebugX.Log(id + " A ---->" + _texture + ":" + _texture.GetInstanceID());
            }
            if (!_texture.LoadImage(decryptedBA))
            {
                DebugX.LogWarning("Texture Decode ErrorString <" + id + ">" + decryptedBA);
            }


            //Make Texture no longer readable
            _texture.Apply(false, true);

            //DebugX.Log("Decode ----> " + id + ":::" + _texture + ":" + _texture.GetInstanceID() + ":" + status);

            var temp = onComplete;
            onComplete = null;
            if (temp != null) temp(_texture);
        }

        public Texture2D CreateTempTexture()
        {
            if (_texture != null)
            {
                DebugX.LogWarning(
                    "CreateTempTexture ErrorString - texture should be null, maybe duplicated calls to createTempTexture");
                return _texture;
            }

            if (cacheType == GRCacheType.Disk) cacheType = GRCacheType.RamCache;
            _texture = new Texture2D(width, height, transparent ? TextureFormat.ARGB32 : TextureFormat.RGB24, false)
            {
                name = id,
                //wrapMode = TextureWrapMode.Clamp,
                //filterMode = FilterMode.Bilinear
            };

            //DebugX.Log(id + " B ---->" + _texture + ":" + _texture.GetInstanceID() + ":" + status);

            //if (!transparent) return _texture; // clear colors

            /*var colors = new Color32[width * height];
        var c = new Color32(255, 255, 255, 0);

        for (var i = 0; i < width * height; i++) colors[i] = c;
        _texture.SetPixels32(colors);
        _texture.Apply();*/

            return _texture;
        }

        public override int RamCost
        {
            get { return (transparent ? 4 : 3)*width*height; }
        }

        public override void OnLoadErrorAction()
        {
            base.OnLoadErrorAction();

            var temp = onComplete;
            onComplete = null;
            if (temp != null) temp(null);
        }

        public override object LoadedContent
        {
            get { return _texture; }
        }

        public override void ReleaseCacheHR()
        {
            if (cacheType == GRCacheType.RamPersistent)
            {
                //DebugX.Log("Persitent won't be clear ---> " + id + ":" + _texture);
                return;
            }
            status = GRIStatus.TempRam;
            cache = new WeakReference(_texture);
            //DebugX.Warn("Clear Cache ---> " + id);
            _texture = null;
            _buyteDatas = null;
        }
    }

    internal static class GRUtils2
    {
        public static string GetCachePath()
        {
#if UNITY_EDITOR || UNITY_STANDALONE
            var path = Application.temporaryCachePath;
            var utf8Bytes = Encoding.UTF8.GetBytes(path);
            var unicodeBytes = Encoding.Convert(Encoding.UTF8, Encoding.ASCII, utf8Bytes);
            return Encoding.ASCII.GetString(unicodeBytes).ReplaceSpace().Replace('?', '_');

#elif UNITY_IOS
        return Application.temporaryCachePath;

#elif (UNITY_WSA_8_1 || UNITY_WP_8_1 || UNITY_WINRT_8_1 || UNITY_WSA)
		return UnityEngine.Windows.Directory.localFolder;

#else
        return Application.persistentDataPath;

#endif
        }

        public static string GetExtension(this string path)
        {
            var s = new StringBuilder();
            var hasExtension = false;

            for (var i = path.Length - 1; i >= 0; i--)
            {

                if (path[i] == '.')
                {
                    hasExtension = true;
                    break;
                }

                s.Insert(0, path[i]);
            }

            return hasExtension ? s.ToString() : "";
        }

        public static string[] Extensions(this GRType type)
        {
            return type == GRType.Text
                ? new[] {"txt", "csv", "json", "unity3d"}
                : type == GRType.Image
                    ? new[] {"png", "jpg", "jpeg", "dds",}
                    : type == GRType.Bytes
                        ? new[] {"bytes"}
                        : new[] {"txt", "csv", "json", "png", "jpg", "jpeg", "bytes", "dds", "unity3d"};
        }

        public static string GetFileURI(this string fileId, string signature, string parentFolder)
        {
            return GetFolderUri(parentFolder) + "/" + fileId.Replace("/", "_") +
                   (string.IsNullOrEmpty(signature) ? "" : signature);
        }

        public static string GetFolderUri(string parentFolder)
        {
            var basePath = GetCachePath();
            return basePath + (string.IsNullOrEmpty(parentFolder) ? "" : ("/" + parentFolder));
        }

        public static void DeleteFile(this string fileURI)
        {
            var url = fileURI.ConvertToPath();
            if (File.Exists(url))
            {
                File.Delete(url);
            }
        }

        public static byte[] LoadFile(this string fileUri)
        {
            byte[] data = null;
            try
            {
                var url = fileUri.ConvertToPath();
#if UNITY_WP8 || UNITY_WSA || UNITY_WINRT_8_1
            data = File.ReadAllBytes(url);
#else
                var file = new FileStream(url, FileMode.Open);
                var len = (int) file.Length;
                data = new byte[len];
                file.Read(data, 0, len);
                file.Close();
#endif
            }
            catch (System.Exception e)
            {
                Debug.LogError(e.Message);
            }
            return data ?? new byte[0];
        }

        public static void WriteFile(this string content, string fileUri, bool overwrite)
        {
//#if !UNITY_WEBGL
            var url = fileUri.ConvertToPath();

            var folderPath = url.GetDirectory().ConvertToPath();
            if (!Directory.Exists(folderPath)) CreateDirectory(folderPath);

            try
            {
#if UNITY_WP8 || UNITY_WSA || UNITY_WINRT_8_1
        File.WriteAllBytes(url, Encoding.UTF8.GetBytes(content));
#else
                File.WriteAllText(url, content);
#if UNITY_IOS
    //prevent this file to be backup to iCloud (thus, resulted in a rejection)
		UnityEngine.iOS.Device.SetNoBackupFlag(url);
#endif
#endif
            }
            catch (Exception ex)
            {
                Debug.LogError(ex.Message);
            }
//#endif
        }

        public static void WriteFile(this byte[] content, string fileURI, bool overwrite)
        {
//#if !UNITY_WEBGL
            var url = fileURI.ConvertToPath();
            var folderPath = url.GetDirectory().ConvertToPath();
            if (!Directory.Exists(folderPath)) CreateDirectory(folderPath);

#if UNITY_WP8 || UNITY_WSA || UNITY_WINRT_8_1
        File.WriteAllBytes(url, content);
#else
            try
            {
                var write = new FileStream(url, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                write.Write(content, 0, content.Length);
                write.Close();

#if UNITY_IOS
    //prevent this file to be backup to iCloud (thus, resulted in a rejection)
        UnityEngine.iOS.Device.SetNoBackupFlag(url);
#endif
            }
            catch (Exception ex)
            {
                Debug.LogError(ex.Message);
            }
#endif
//#endif
        }

        public static void CreateDirectory(string directory)
        {
#if (UNITY_WP8 || UNITY_WSA || UNITY_WINRT_8_1) && !UNITY_EDITOR
        var path = directory;
        var listPathCreate = new List<string>();
        while (!Directory.Exists(path))
        {
            listPathCreate.Insert(0, path);
            path = path.GetDirectory();
        }

        foreach (var p in listPathCreate) {
            Directory.CreateDirectory(p);
        }
#else
            if (!Directory.Exists(directory)) Directory.CreateDirectory(directory);
#endif
        }

        public static byte[] Decrypt(this byte[] data, bool firstBitOnly, bool needDecrypt)
        {
            if (!needDecrypt) return data;
            if (data == null || data.Length <= 1)
            {
                DebugX.LogWarning("GameResource ErrorString - Decrypt data should not be null");
                return null;
            }

            //DebugX.Log("Decrypting ... ");
            var encryptTotal = firstBitOnly ? 1 : data.Length;
            for (var i = 0; i < encryptTotal; i++)
            {
                data[i] = (byte) ~data[i];
            }
            return data;
        }

        public static byte[] CopyBytes(this byte[] data)
        {
            if (data == null) return null;

            var count = data.Length;
            var result = new byte[count];
            for (int i = 0; i < count; i++)
            {
                result[i] = data[i];
            }

            return result;
        }

        public static string DecryptText(this byte[] data, bool needDecrypt)
        {
            //DebugX.Log("Decrypt ----> " + data);
            byte[] decryptBytes = Decrypt(data, false, needDecrypt);
//#if WP8
//      return Encoding.UTF8.GetString(decryptBytes, 0, decryptBytes.Length);
//#else
            //return Encoding.UTF8.GetString(decryptBytes); 
//#endif

            return Encoding.UTF8.GetString(decryptBytes, 0, decryptBytes.Length);
        }

        public static string Join<T>(this T[] p_source, string p_joinDelimiter = ",")
        {
            string result = "";
            for (int i = 0; i < p_source.Length; i++) { result += (i == 0 ? "" : p_joinDelimiter) + p_source[i]; }
            return result;
        }

        public static List<T> Clone<T>(this List<T> p_source)
        {
            var list = new List<T>();
            list.AddRange(p_source);
            return list;
        }

        public static T Shift<T>(this List<T> p_source)
        {
            T value = p_source[0];
            p_source.RemoveAt(0);
            return value;
        }

        public static T RemoveLast<T>(this List<T> source)
        {
            if (source == null) return default(T);

            var idx = source.Count - 1;
            var item = source[idx];
            source.RemoveAt(idx);
            return item;
        }

        public static T[] Swap<T>(this T[] p_list, int p_idx1, int p_idx2)
        {
            T tmp = p_list[p_idx1];
            p_list[p_idx1] = p_list[p_idx2];
            p_list[p_idx2] = tmp;
            return p_list;
        }

        public static Dictionary<string, object> KeyValueToDictionary(this object[] keyValueList)
        {
            if (keyValueList == null || keyValueList.Length % 2 == 1)
            {
                DebugX.LogWarning("Invalid keyValueList, number of items should be even " + keyValueList);
                return null;
            }
            var dict = new Dictionary<string, object>();
            for (var i = 0; i < keyValueList.Length; i += 2)
            {
                dict.Add((string)keyValueList[i], keyValueList[i + 1]);
            }
            return dict;
        }

        private static Texture2D _transparent;

        public static Texture2D TransparentTexture
        {
            get { return _transparent ?? (_transparent = Resources.Load<Texture2D>("Textures/transparent")); }
        }

        //v1 :  {Id};{BossType}:{+/-};{FileSize}[;{width};{height}]
        //
        //      Bg\7cities;png:+;v1;2569782;1280;1600
        //      localize_vn;txt:+;v1;137084
        internal static AGameResource ParseGRLine_v1(string[] arr)
        {
            if (arr.Length < 3) return null;

            string pname = arr[0];
            string ptype = arr[1];

            /*DebugX.Log("raw ---> " + arr.Join(";") +":"+ arr.Length);
        for (var i = 0; i < arr.Length; i++) {
            DebugX.Log(i + "::" + arr[i]);
        }*/


            //backward compatible
            var szIndex = 2;
            var fileSize = arr[szIndex].ToInt();

            //DebugX.Log(0);
            if (ptype.IndexOf(":") != -1)
            {
                //new format png:+ or png:-
                var tarr = ptype.Split(':');
                ptype = tarr[0];
                //if (tarr[1] == "+") preload = GRPreloadType.Queue;
            }

            //DebugX.Log(1);
            string pid = (pname + "." + ptype).Replace("\\", "/"); //.ToLowerInvariant();//.Replace("/", "_")
            //DebugX.Log(2);

            switch (ptype)
            {
                case "txt":
                case "json": //  STRING-BASED RESOURCE
                case "xml": //  localize_vn;txt;v1;137084
                    //DebugX.Log("--------------------------> " + arr.Join(";"));
                    return new GRText
                    {
                        id = pid,
                        preloadType = GRPreloadType.Queue,
                        bytesTotal = fileSize,
                        signature = "" + fileSize
                    };

                case "jpg": //  IMAGE-BASED RESOURCE
                case "png": //  Bg\7cities;png;v1;2569782;1280;1600
                    //DebugX.Log(5);

                    // DebugX.Log("----------> transparent File:: " + ptype == "png");
                    return new GRImage
                    {
                        id = pid,
                        preloadType = GRPreloadType.Queue,
                        bytesTotal = fileSize,
                        signature = "" + fileSize,

                        transparent = ptype == "png",
                        width = arr[szIndex + 1].ToInt(),
                        height = arr[szIndex + 2].ToInt()
                    };

                default:
                    DebugX.LogWarning("Unsupported type <" + ptype + ">");
                    break;
            }

            return null;
        }

        //v2 :  {Id};{BossType};{Sign};{PreloadType};{CacheType};{FileSize}[;{width};{height}]
        //
        //      Bg\7cities;png;03June14101125;0;1;2569782;1280;1600
        //      localize_vn;txt;03June14101125;0;1;137084
        internal static AGameResource ParseGRLine_v2(string[] arr)
        {
            if (arr.Length < 6)
            {
                Debug.LogWarning("Invalid line, too short ... " + arr.Length);
                return null;
            }
            string ptype = arr[1].ToLower();
            arr[0] = arr[0].Replace("\\", "/");

            if (Extensions(GRType.Image).Contains(ptype))
            {
                //Debug.Log("Image ---> " + arr[0]);
                return new GRImage
                {
                    transparent = true,
                    id = arr[0] + "." + arr[1].ToLower(),
                    signature = arr[2],
                    preloadType = (GRPreloadType) arr[4].ToInt(),
                    cacheType = (GRCacheType) arr[5].ToInt(),

                    bytesTotal = arr[3].ToInt(),
                    width = arr[6].ToInt(),
                    height = arr[7].ToInt()
                };
            }

            if (Extensions(GRType.Text).Contains(ptype))
            {
                //Debug.Log("Text ---> " + arr[0]);
                return new GRText
                {
                    id = arr[0] + "." + arr[1].ToLower(),
                    signature = arr[2],
                    preloadType = (GRPreloadType) arr[4].ToInt(),
                    cacheType = (GRCacheType) arr[5].ToInt(),
                    bytesTotal = arr[3].ToInt()
                };
            }

            //Debug.LogWarning("Skipped line ... " + arr.Join(","));
            return null;
        }

        internal static Dictionary<string, AGameResource> ParseConfig(this string source)
        {
            var lines = source.Split('\n');
            var line0 = lines[0];
            var startIndex = line0.Equals("v2") ? 1 : 0;
            var dict = new Dictionary<string, AGameResource>();

            for (var i = startIndex; i < lines.Length; i++)
            {
                var line = lines[i];

                //DebugX.Log("line " + i + " :: " + line);

                //skip empty lines
                if (string.IsNullOrEmpty(line)) continue;

                line = line.Trim();

                var ar = ParseGRLine_v2(line.Split(';'));
                if (ar == null)
                {
                    DebugX.LogWarning("Invalid line <" + line + ">");
                    continue;
                }

                if (dict.ContainsKey(ar.id))
                {
                    DebugX.LogWarning("Duplicated key <" + ar.id + "> in Resource Map, skipping ...");
                }
                else
                {
                    //DebugX.Log("key <" + ar.id + "> ... " + ar.type);
                    dict.Add(ar.id, ar);
                }
            }

            return dict;
        }
    }
}