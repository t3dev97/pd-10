﻿using System.Collections;
using System.Collections.Generic;
using Studio;
using Studio.Resource2D;
using UnityEngine;
using System;

public class DynamicResource : GameResource2 {

#if UNITY_WEBGL

    private const int MAX_LOAD_RAM = 4; //4MB RAM used to load asset
    private const int RAM_CACHE_SMAL = 16; //Cache small assets
    private const int RAM_CACHE_MEDIUM = 8; //RAM used to cache small assets
    private const int RAM_CACHE_BIG = 8; //10MB RAM used to cache small assets

#else

    private const int MAX_LOAD_RAM = 1; //4MB RAM used to load asset
    private const int RAM_CACHE_SMAL = 4; //Cache small assets
    private const int RAM_CACHE_MEDIUM = 2; //RAM used to cache small assets
    private const int RAM_CACHE_BIG = 2; //10MB RAM used to cache small assets

#endif

    private static DynamicResource _api;

    public static DynamicResource Api
    {
        get { return _api ?? (_api = new DynamicResource()); }
    }

    public static void Init(string baseURL, string[] preloadIds, Action onComplete)
    {
        if (preloadIds != null) PreloadIds.AddRange(preloadIds);

        const int maxRam = 1024 * 1024 * MAX_LOAD_RAM;
        var signature = "map.unity3d";

        Api.ParentFolder = "";
        Api.Initialize(baseURL + "/map.unity3d", signature, baseURL, onComplete)
            .SetRamLimit((isSD ? 1 : 4) * maxRam)
            .SetThreadLimit(1);
    }

    protected override void OnConfigComplete(byte[] content, LoaderItem ldi)
    {
        base.OnConfigComplete(content, ldi);
        DebugX.Log("-----------> onConfigComplete");
        //PreloadData();
    }

    public void LoadFirstSuccess()
    {
        PlayerPrefs.SetInt(FirstAssetBundleKey.LOAD_ASSER2D_SUCCESS, 1);
        PlayerPrefs.Save();
    }

    void PreloadData()
    {
        var factor = Mathf.RoundToInt((isSD ? 0.25f : 1f) * 1024 * 1024);
        var smallCost = 0.1f * factor; // ~ 200 kb
        var mediumCost = 0.5f * factor; // 2MB

        //preload all texture
        var cacheDisk = new List<string>();

        Find(item =>
        {
            if (item.type == GRType.Image)
            {
                var image = (GRImage)item;
                var cost = image.RamCost;
                image.group = cost < smallCost ? "small" : cost < mediumCost ? "medium" : "big";
            }
            return false;
        });

        GetCacheGroup("small").ramCostLimit = RAM_CACHE_SMAL * factor;
        GetCacheGroup("medium").ramCostLimit = RAM_CACHE_MEDIUM * factor;
        GetCacheGroup("big").ramCostLimit = RAM_CACHE_BIG * factor;

        SetCachePreloadType(GRCacheType.Disk, GRPreloadType.Preload, cacheDisk.ToArray());
        SetCachePreloadType(GRCacheType.RamCache, GRPreloadType.Preload, PreloadIds.ToArray());
        CheckPreload();
    }

    public void OnPreloadStatusDiskToRamCache()
    {
    }

    public static bool isSD
    {
        get
        {
            return false;
            // return Screen.height/320f <= GameConfig.Asset2DThreshold;
        }
    }

    public static bool isHD
    {
        get
        {
            return true;
        }
    }

    public static List<string> PreloadIds = new List<string>()
    {
        "world_map_data",
        "phu_ban_map_data"
    };

}
