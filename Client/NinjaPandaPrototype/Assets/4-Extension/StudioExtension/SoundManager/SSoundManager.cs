﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Studio.Sound
{
    public class SSoundManager : MonoBehaviour
    {
        private AudioListener _listener;
        private AudioSource _musicSource;
        private string _key;

        private float? _musicVolume;
        public float MusicVolume
        {
            get
            {
                if (!string.IsNullOrEmpty(_key)) {
                    if (_musicVolume == null) {
                        _musicVolume = PlayerPrefs.GetFloat(_key + "_music", 1);
                    }
                }

                if (_musicVolume == null) _musicVolume = 1;

                return _musicVolume.Value;
            }
            set
            {
                _musicVolume = value;

                if (!string.IsNullOrEmpty(_key))
                {
                    PlayerPrefs.SetFloat(_key + "_music", value);
                    PlayerPrefs.Save();
                }
            }
        }

        private float? _soundVolume;
        public float SoundVolume
        {
            get
            {
                if (!string.IsNullOrEmpty(_key))
                {
                    if (_soundVolume == null)
                    {
                        _soundVolume = PlayerPrefs.GetFloat(_key + "_sound", 1);
                    }
                }

                if (_soundVolume == null) _soundVolume = 1;

                return _soundVolume.Value;
            }
            set
            {
                _soundVolume = value;

                if (!string.IsNullOrEmpty(_key))
                {
                    PlayerPrefs.SetFloat(_key + "_sound", value);
                    PlayerPrefs.Save();
                }
            }
        }

        private Dictionary<string, AudioClip> _audioClipManager;
        private List<AudioSourceInfo> _soundList;

        public bool IsSoundMute { get; private set; }
        public bool IsMusicMute { get; private set; }

        protected virtual void Update()
        {
            if(_soundList == null) return;
            for (var i = 0; i < _soundList.Count;)
            {
                if (!_soundList[i].CheckTime(Time.deltaTime))
                {
                    Destroy(_soundList[i].Source);
                    _soundList.RemoveAt(i);
                }
                else
                {
                    i ++;
                }
            }
        }

        public SSoundManager Init()
        {
            _audioClipManager = new Dictionary<string, AudioClip>();
            _soundList = new List<AudioSourceInfo>();

            _listener = gameObject.AddComponent<AudioListener>();
            _musicSource = gameObject.AddComponent<AudioSource>();

            IsSoundMute = false;
            IsMusicMute = false;

            return this;
        }

        public void SetKey(string key)
        {
            _key = key;
        }

        public void AddAudioClip(AudioClip clip)
        {
            if (clip == null) return;
            if (!_audioClipManager.ContainsKey(clip.name)) {
                _audioClipManager.Add(clip.name, clip);
            }
        }

        public void SetVolume(TypeSoundX type, float volume)
        {
            switch (type)
            {
                case TypeSoundX.Music:
                {
                    MusicVolume = volume;
                    if (_musicSource != null) _musicSource.volume = volume;
                    break;
                }

                case TypeSoundX.Sound:
                {
                    SoundVolume = volume;
                    foreach (var audioSourceInfo in _soundList) {
                        if (audioSourceInfo.Source != null) {
                            audioSourceInfo.Source.volume = volume;
                        }
                    }

                    break;
                }

            }
        }

        public float GetVolume(TypeSoundX type)
        {
            switch (type)
            {
                case TypeSoundX.Sound:
                    return SoundVolume;
                case TypeSoundX.Music:
                    return MusicVolume;
            }

            return 0;
        }

        public void SetMute(TypeSoundX type, bool isMute)
        {
            switch (type)
            {
                case TypeSoundX.Sound:
                {
                    IsSoundMute = isMute;
                    foreach (var audioSourceInfo in _soundList) {
                        if (audioSourceInfo.Source != null) {
                            audioSourceInfo.Source.mute = isMute;
                        }
                    }
                    break;
                }

                case TypeSoundX.Music:
                {
                    IsMusicMute = isMute;
                    _musicSource.mute = isMute;
                    break;
                }
            }
            
        }

        public void PlaySound(string clipName)
        {
            if (_audioClipManager == null) return;
            if (_audioClipManager.ContainsKey(clipName)) {
                PlaySound(_audioClipManager[clipName], TypeSoundX.Sound, SoundVolume, false);
            }            
        }

        public void PlayMusic(string clipName)
        {
            if (_audioClipManager == null) return;
            if (_audioClipManager.ContainsKey(clipName)) {
                PlaySound(_audioClipManager[clipName], TypeSoundX.Music, MusicVolume, true);
            }
        }

        public void StopMusic()
        {
            if (_musicSource != null) _musicSource.Stop();
        }

        private AudioSource PlaySound(AudioClip clip, TypeSoundX type) { return PlaySound(clip, type, 1f, false, 1f); }

        private AudioSource PlaySound(AudioClip clip, TypeSoundX type, float volume) { return PlaySound(clip, type, volume, false, 1f); }

        private AudioSource PlaySound(AudioClip clip, TypeSoundX type, float volume, bool loop) { return PlaySound(clip, type, volume, loop, 1f); }

        private AudioSource PlaySound(AudioClip clip, TypeSoundX type, float volume, bool loop, float pitch)
        {
            if (clip != null)// && volume > 0.01f
            {
                if (_listener != null && _listener.enabled)
                {
                    var source = type == TypeSoundX.Music ? _musicSource : CreateNewAudioSource();
                    source.pitch = pitch;
                    source.volume = volume;
                    source.loop = loop;

                    if (type == TypeSoundX.Sound)
                    {
                        _soundList.Add(new AudioSourceInfo(source, clip.length));

                        source.mute = IsSoundMute;
                        source.clip = clip;
                        source.PlayOneShot(source.clip, source.volume);
                        return source;
                    }

                    const float duration = 0.5f;
                    if (source.clip != clip)
                    {
                        source.mute = IsMusicMute;
                        LeanTween.cancel(source.gameObject);
                        LeanTween.value(source.gameObject, (a) => {
                            source.volume = a;
                            if (a <= 0)
                            {
                                source.clip = clip;
                                source.Play();
                                LeanTween.value(source.gameObject, (f) => {
                                    source.volume = f;
                                }, 0, volume, duration);
                            }

                        }, volume, 0, duration);
                    }
                    else
                    {
                        if (!source.isPlaying) source.Play();
                    }

                    return source;
                }
            }
            return null;
        }

        private AudioSource CreateNewAudioSource()
        {
            return  gameObject.AddComponent<AudioSource>();
        }
    }

    public class AudioSourceInfo
    {
        public float CurrentTime;
        public AudioSource Source;

        public AudioSourceInfo(AudioSource source, float length)
        {
            Source = source;
            CurrentTime = length;
        }

        public bool CheckTime(float time)
        {
            CurrentTime -= time;
            return CurrentTime > 0;
        }
    }

    public enum TypeSoundX
    {
        Music, Sound
    }
}

