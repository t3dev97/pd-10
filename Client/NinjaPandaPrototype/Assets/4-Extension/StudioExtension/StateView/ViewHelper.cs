﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Studio.BC
{
    public class ViewHelper : MonoBehaviour
    {
        public StateView Main;
        public StateView Popup;
		public StateView PopupNotify;
		public StateView System;
        public StateView Battle;

        protected virtual void Awake() { }

        protected virtual void Start()
        {
        }

        public void ChangeSceneAsync(string sceneName, Action onCompeleteAction = null)
        {
            StartCoroutine(IChangeScene(sceneName, onCompeleteAction));
        }

        public void ChangeScene(string sceneName)
        {
            SceneManager.LoadScene(sceneName);
        }

        private IEnumerator IChangeScene(string sceneName, Action onCompeleteAction)
        {
            var request = SceneManager.LoadSceneAsync(sceneName);
            while (!request.isDone)
            {
                yield return null;
            }

            if (onCompeleteAction != null) onCompeleteAction();
        }

		public virtual void Reset()
		{
			Main.HideAll(true);
			Popup.HideAll(true);
			PopupNotify.HideAll(true);
			System.HideAll(true);
		}
	}
}

