﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using UnityEngine;

namespace Studio.BC
{
	public class StateView : MonoBehaviour
	{
		private UnityEngine.UI.Image _fader;
		public UnityEngine.UI.Image fader
		{
			get
			{
				if (_fader == null)
				{
					var trans = transform.Find("fader");
					if (trans != null)
					{
						_fader = trans.gameObject.GetComponent<UnityEngine.UI.Image>();
						_fader.gameObject.SetActive(false);
					}
				}
				return _fader;
			}
		}
		public Action<string> OnStateChangeAction;

		public GameObject CurrentStateGo { get; private set; }

		public string CurrentState { get; private set; }

		private readonly List<string> _listStateQueue = new List<string>();

		public void SetMainState(string currentState, GameObject currentStateGo)
		{
			CurrentState = currentState;
			CurrentStateGo = CurrentStateGo;
		}

		 void EnableFader(bool isEnable, bool isForceHide = false)
		{
			if (fader != null)
			{
				var color = fader.color;
				Action hide = ()=> {
					var lt = LeanTween.value(color.a, 0, 0.9f).setOnUpdate((float value) =>
					{
						color.a = value;
						fader.color = color;
					});
					LeantweenController.Api.Run(lt);
				};
				if (isForceHide)
				{
					fader.gameObject.SetActive(false);
					return;
				}
				if (!isEnable)
				{
					hide();
					bool isShouldBack = true;
					var childCount = transform.childCount;
					for (int i = 0; i < childCount; i++)
					{
						var child = transform.GetChild(i);
						if (child != fader && child.gameObject.activeSelf)
						{
							isShouldBack = false;
							break;
						}
					}
					if (isShouldBack)
						return;
				}
				fader.gameObject.SetActive(true);
				color.a = isEnable ? 0.9f : 0;
				fader.color = color;
			}
		}

		public void Show(string stateName, Action<GameObject> onCompleteAction = null, bool isHideCurrentState = true, GameObject statePrefab = null, bool isEnableFader = true, bool isPlayAnim = true,bool isForceHide = false)
		{

            if (stateName == CurrentState)
			{
				if (onCompleteAction != null) onCompleteAction(CurrentStateGo);
				return;
			}

			var tran = GetTran(stateName);
			if (tran != null)
			{
				ShowStateTran(tran, isHideCurrentState, isPlayAnim: isPlayAnim);
				ShowOnCompleteAction(stateName, tran.gameObject, onCompleteAction);
			}
			else
			{
				var prefab = statePrefab;
				if (prefab != null)
				{
					var go = Instantiate(prefab);
					go.name = stateName;
					tran = go.transform;
					tran.SetParent(transform, false);
					tran.localScale = Vector3.one;
					tran.localPosition = Vector3.zero;
					ShowStateTran(tran, isHideCurrentState, isPlayAnim:isPlayAnim);
					ShowOnCompleteAction(stateName, go, onCompleteAction);
				}
				else
				{
					var isHideCurrentStateTemp = isHideCurrentState;
                    //if (isEffectResource)
                    //{
                    //BCCache.Api.GetUIPrefabAsync
                     StartCoroutine(BCResource.Api.LoadButton(stateName, (prefabGo) =>
                        {
                            var go = Instantiate(prefabGo);
                            go.name = stateName;
                            tran = go.transform;
                            tran.SetParent(transform, false);
                            tran.localScale = Vector3.one;
                            tran.localPosition = Vector3.zero;
                            ShowStateTran(tran, isHideCurrentStateTemp, isPlayAnim: isPlayAnim);
                            ShowOnCompleteAction(stateName, go, onCompleteAction);
                        }));
                    //}
                    //else
                    //{
                    //    StartCoroutine(LoadStatePrefab(stateName, (prefabGo) =>
                    //    {
                    //        var go = Instantiate(prefabGo);
                    //        go.name = stateName;
                    //        tran = go.transform;
                    //        tran.SetParent(transform, false);
                    //        tran.localScale = Vector3.one;
                    //        tran.localPosition = Vector3.zero;
                    //        ShowStateTran(tran, isHideCurrentStateTemp, isPlayAnim: isPlayAnim);
                    //        ShowOnCompleteAction(stateName, go, onCompleteAction);
                    //    }));
                    //}
                }
			}
			EnableFader(isEnableFader,isForceHide);
		}


        public void ShowMessage(string stateName, Action<GameObject> onCompleteAction = null, bool isHideCurrentState = true, GameObject statePrefab = null, bool isEnableFader = true, bool isPlayAnim = true, bool isForceHide = false)
        {

            if (stateName == CurrentState)
            {
                if (onCompleteAction != null) onCompleteAction(CurrentStateGo);
                return;
            }

            var tran = GetTran(stateName);
            if (tran != null)
            {
                ShowStateTran(tran, isHideCurrentState, isPlayAnim: isPlayAnim);
                ShowOnCompleteAction(stateName, tran.gameObject, onCompleteAction);
            }
            else
            {
                var prefab = statePrefab;
                if (prefab != null)
                {
                    var go = Instantiate(prefab);
                    go.name = stateName;
                    tran = go.transform;
                    tran.SetParent(transform, false);
                    tran.localScale = Vector3.one;
                    tran.localPosition = Vector3.zero;
                    ShowStateTran(tran, isHideCurrentState, isPlayAnim: isPlayAnim);
                    ShowOnCompleteAction(stateName, go, onCompleteAction);
                }
                else
                {
                    var isHideCurrentStateTemp = isHideCurrentState;
                    //if (isEffectResource)
                    //{
                    //BCCache.Api.GetUIPrefabAsync
                    StartCoroutine(BCResource.Api.LoadMessage(stateName, (prefabGo) =>
                    {
                        var go = Instantiate(prefabGo);
                        go.name = stateName;
                        tran = go.transform;
                        tran.SetParent(transform, false);
                        tran.localScale = Vector3.one;
                        tran.localPosition = Vector3.zero;
                        ShowStateTran(tran, isHideCurrentStateTemp, isPlayAnim: isPlayAnim);
                        ShowOnCompleteAction(stateName, go, onCompleteAction);
                    }));
                    //}
                    //else
                    //{
                    //    StartCoroutine(LoadStatePrefab(stateName, (prefabGo) =>
                    //    {
                    //        var go = Instantiate(prefabGo);
                    //        go.name = stateName;
                    //        tran = go.transform;
                    //        tran.SetParent(transform, false);
                    //        tran.localScale = Vector3.one;
                    //        tran.localPosition = Vector3.zero;
                    //        ShowStateTran(tran, isHideCurrentStateTemp, isPlayAnim: isPlayAnim);
                    //        ShowOnCompleteAction(stateName, go, onCompleteAction);
                    //    }));
                    //}
                }
            }
            EnableFader(isEnableFader, isForceHide);
        }
        public void Hide(bool showPrevState = false, bool destroy = false)
		{
			var newCurrentState = "";
			var newCurrentStateGo = (GameObject)null;

			if (!showPrevState) _listStateQueue.Clear();
			else if (_listStateQueue.Count > 0)
			{
				var prevTran = GetTran(_listStateQueue[0]);
				if (prevTran != null)
				{
				    var isPlayAnim = false;
				    var baseView = prevTran.GetComponent<BaseView>();
				    if (baseView != null) isPlayAnim = baseView.IsPlayAnim;

				    ShowStateTran(prevTran, false, false, isPlayAnim: isPlayAnim);
					newCurrentStateGo = prevTran.gameObject;
				}

				newCurrentState = _listStateQueue[0];
				_listStateQueue.RemoveAt(0);
			}

			if (CurrentStateGo != null)
			{
				var baseView = CurrentStateGo.GetComponent<BaseView>();
			    if (baseView != null)
			    {
			        baseView.IsDestroy = destroy;
                    baseView.OnStartTransitionOut();
			    }
				else
				{
				    if (destroy)
				    {
				        Destroy(CurrentStateGo);
				    }
				    else
				    {
				        CurrentStateGo.SetActive(false);
				    }
				}
			}

			ShowOnCompleteAction(newCurrentState, newCurrentStateGo, null);

		    CurrentState = "";
		    CurrentStateGo = null;

			EnableFader(false, true);
		}

		public void Hide(string stateName, bool showPrevState = false, bool destroy = false)
		{
			if (stateName.Equals(CurrentState))
			{
				Hide(showPrevState, destroy);
				return;
			}

			var tran = GetTran(stateName);
			if (tran != null)
			{
			    if (destroy)
			    {
			        Destroy(transform.gameObject);
			    }
			    else
			    {
                    tran.gameObject.SetActive(false);
                }

				if (_listStateQueue.Contains(stateName))
				{
					_listStateQueue.Remove(stateName);
				}
			}
			EnableFader(false, true);
		}

		public void DestroyState(string stateName, bool isShowTransition = false)
		{
		    if (stateName == CurrentState) {
		        CurrentState = "";
		    }

			var tran = GetTran(stateName);
			if (tran != null)
			{
				if (isShowTransition)
				{
					var baseView = tran.GetComponent<BaseView>();
					if (baseView != null) baseView.OnStartTransitionOut();
					else Destroy(tran.gameObject);
				}
				else Destroy(tran.gameObject);
			}
		}

		private void ShowOnCompleteAction(string stateName, GameObject stateGo, Action<GameObject> onCompleteAction)
		{
			CurrentState = stateName;
			CurrentStateGo = stateGo;
#if !BUILD_EGT
            if (!string.IsNullOrEmpty(stateName) && stateGo != null)
            {
                var trackView = stateGo.GetComponent<FirebaseViewTracking>();
                if (trackView == null)
                {
                    trackView = stateGo.AddComponent<FirebaseViewTracking>();
                    trackView.Type = TrackType.Normal;
                    trackView.TrackPrefix = stateName;
                    trackView.ForceTrack();
                }
            }
#endif
            if (onCompleteAction != null)
			{
				onCompleteAction(stateGo);
			}

			if (OnStateChangeAction != null)
			{
				OnStateChangeAction(stateName);
			}
		}

		private void ShowStateTran(Transform tran, bool isHideCurrentState, bool addQueue = true, bool isPlayAnim = false)
		{
			if (addQueue && !string.IsNullOrEmpty(CurrentState))
			{
				_listStateQueue.Insert(0, CurrentState);
			};

			if (!tran.gameObject.activeSelf)
			{
				tran.gameObject.SetActive(true);
			}

			tran.SetAsLastSibling();

			var baseView = tran.GetComponent<BaseView>();
			if (baseView != null)
			{
				baseView.LastView = null;
				baseView.ParentView = this;
                baseView.IsPlayAnim = isPlayAnim;
                baseView.Init();

				if (!baseView.IsPreload)
				{
					baseView.OnStartTransitionIn();
				}
			}

			if (CurrentStateGo != null)
			{
				if (isHideCurrentState)
				{
					var currentbaseView = CurrentStateGo.GetComponent<BaseView>();
					if (currentbaseView != null)
					{
						if (baseView != null) baseView.LastView = currentbaseView;
						if (baseView == null || !baseView.IsPreload)
						{
							currentbaseView.OnStartTransitionOut();
						}
					}

					else CurrentStateGo.SetActive(false);
				}
			}
		}

		private Transform GetTran(string stateName)
		{
			var tran = transform.Find(stateName);
			if (tran != null) return tran;
			return null;
		}

		private IEnumerator LoadStatePrefab(string stateName, Action<GameObject> onCompleteAction)
		{
			var request = Resources.LoadAsync<GameObject>(stateName);
			while (!request.isDone)
			{
				yield return null;
			}

			onCompleteAction(request.asset as GameObject);
		}

		public void HideAll(bool isDestroy = false)
		{
			var childCount = transform.childCount;
			for (int i = 0; i < childCount; i++)
			{
				var chil = transform.GetChild(i);
				chil.gameObject.SetActive(false);
				if (isDestroy)
				{
					if (fader == null || !chil.Equals(fader.transform))
					{
						chil.name += "trash";
						Destroy(chil.gameObject);
					}
				}
			}
			CurrentState = "";
		}

		public void Show(GameObject go, Action<GameObject> onCompleteAction = null, bool isHideCurrentState = true, GameObject statePrefab = null, bool isEnableFader = true, bool isPlayAnim = false)
		{
			var stateName = go.name;
			if (go.name == CurrentState)
			{
				if (onCompleteAction != null) onCompleteAction(CurrentStateGo);
				return;
			}

			go.transform.SetParent(transform, false);
		    go.transform.localScale = Vector3.one;
		    go.transform.localPosition = Vector3.zero;

            ShowStateTran(go.transform, isHideCurrentState, isPlayAnim: isPlayAnim);
			ShowOnCompleteAction(stateName, go, onCompleteAction);
			EnableFader(isEnableFader);
		}
	}
}
