﻿using System;
using System.Collections;
using System.Collections.Generic;
using TTBT.DynamicScroller;
using UnityEngine;

namespace Studio.BC
{
    public class BaseView : MonoBehaviour 
    {
        public bool IsPreload;
        public bool TransitionEffect;

        internal CanvasGroup CanvGroup;

        internal float TimeFade = 1f;
        internal float CurrentFade;

        internal bool IsFadeIn;
		internal bool IsFadeCompleted = true;
		internal bool IsPlayAnim = false;
        internal bool IsDestroy;
		internal float TimeAnimIn = 0.3f;
		internal float TimeAnimOut = 0.2f;
		internal int AnimStatus; //0 == new 1 == started, 2 == end

        internal BaseView LastView;
        internal StateView ParentView;

        private List<DynamicScroller> _listScrollers;

        protected virtual void Awake()
        {
            Init();
        }

        protected virtual void Start()
        {
        }

        protected virtual void Update()
        {
            if (CanvGroup != null && !IsFadeCompleted)
            {
				ProgressAnim();

                if (IsPlayAnim && AnimStatus < 2)
				{
                    
					CanvGroup.alpha = 1;
					return;
				}

				if (IsFadeIn)
                {
                    CurrentFade += TimeFade * Time.deltaTime;
                    if (!TransitionEffect|| CurrentFade > 1)
                    {
                        CurrentFade = 1;
                        IsFadeCompleted = true;
                        OnTransitionInComplete();
                    }
                }
                else
                {
                    CurrentFade -= TimeFade * Time.deltaTime;
                    if (!TransitionEffect || CurrentFade < 0)
                    {
                        CurrentFade = 0;
                        IsFadeCompleted = true;
                        OnTransitionOutComplete();
                    }
                }

                CanvGroup.alpha = CurrentFade;
			}
		}

        protected virtual void OnEnable() { }

        protected virtual void OnDisable() { }

        internal void Init()
        {
            CanvGroup = GetComponent<CanvasGroup>();
            if (CanvGroup == null) {
                CanvGroup = gameObject.AddComponent<CanvasGroup>();
            }

            CanvGroup.alpha = 0;
            CanvGroup.interactable = true;
            CanvGroup.blocksRaycasts = true;

            _listScrollers = gameObject.GetChildsWidthType<DynamicScroller>();
        }

        public void OnPreloadComplete()
        {
            OnStartTransitionIn();

            if (LastView != null) {
                LastView.OnStartTransitionOut();
            }
        }

        public virtual void OnStartTransitionIn()
        {
            IsFadeIn = true;
            IsFadeCompleted = false;
			AnimStatus = 0;

			if (IsPlayAnim)
				TransitionEffect = false;
		}

        public virtual void OnTransitionInComplete()
        {
            
        }

        public virtual void OnStartTransitionOut()
        {
            IsFadeIn = false;
            IsFadeCompleted = false;
			AnimStatus = 0;

			if (IsPlayAnim)
				TransitionEffect = false;
            StartCoroutine(TransitionOutDelayDestroy());
		}

        public virtual void OnTransitionOutComplete()
        {
            if (this == null) return;
            if (gameObject == null) return;

            if (IsDestroy)
            {
                Destroy(gameObject);
            }
            else
            {
                gameObject.SetActive(false);
            }
        }
        IEnumerator TransitionOutDelayDestroy()
        {
            yield return new WaitForSeconds(TimeFade + 0.5f);

            if (this != null && gameObject != null)
            {
                if (IsDestroy)
                    Destroy(gameObject);
                else
                    gameObject.SetActive(false);
            }
        }

        public void Close(bool showPrevState = false, bool destroy = false)
        {
            if (ParentView != null) ParentView.Hide(showPrevState, destroy);
		}

		public virtual void OnBackFunction() { }

		void ProgressAnim()
		{
		    if (!IsPlayAnim || AnimStatus > 0) {
		        return;
		    }

		    SetEnableScrollRect(false);

            if (IsFadeIn)
			{
				transform.localScale = Vector3.zero;
				var lt = LeanTween.scale(gameObject, Vector3.one, TimeAnimIn).setEaseOutBack();
				LeantweenController.Api.Run(lt, ()=>
				{
                    SetEnableScrollRect(true);
                    AnimStatus = 2;
				});
			}
			else
			{
				transform.localScale = Vector3.one;
				var lt = LeanTween.scale(gameObject, Vector3.zero, TimeAnimOut).setEaseInBack();
				LeantweenController.Api.Run(lt, ()=>
				{
                    SetEnableScrollRect(true);
                    transform.localScale = Vector3.zero;
					AnimStatus = 2;
				});
			}
			AnimStatus = 1;
			//CurrentTimeScale += Time.deltaTime;
			//if (IsFadeIn)
			//{
			//	if (CurrentTimeScale < TimeBack)
			//	{
			//		DeltaScale = 1.2f/TimeBack;
			//	}
			//	else if (CurrentTimeScale < TimeAnimIn)
			//	{
			//		DeltaScale = -0.2f/(TimeAnimIn - TimeBack);
			//	}
			//	else
			//	{
			//		DeltaScale = 0;
			//		IsPlayAnim = false;
			//		transform.localScale = Vector3.one;
			//	}
			//	DeltaScale *= Time.deltaTime;
			//	transform.localScale = new Vector3(transform.localScale.x + DeltaScale, transform.localScale.y + DeltaScale, transform.localScale.y + DeltaScale);
			//	DebugX.Log("Delta " + DeltaScale);
			//	DebugX.Log(transform.localScale.ToString());
			//	return;
			//}
		}

        void SetEnableScrollRect(bool enable)
        {
            if (_listScrollers != null) {
                foreach (var dynamicScroller in _listScrollers) {
                    dynamicScroller.EnableScrollRect(enable);
                }
            }
        }
	}
}
