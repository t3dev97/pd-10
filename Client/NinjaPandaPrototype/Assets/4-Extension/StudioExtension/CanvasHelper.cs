﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Studio
{
    public class CanvasHelper : MonoBehaviour
    {
#if UNITY_EDITOR || UNITY_WEBGL
        int _preWidth, _preHeight;
#endif

        void Awake()
        {
            var size3_2 = 3 / 2f;
            var sizeScene = (float)Screen.width / Screen.height;
            var isMatchHeight = sizeScene >= size3_2;
            var canvas = GetComponent<CanvasScaler>();
            canvas.matchWidthOrHeight = isMatchHeight ? 1 : 0;
        }

#if UNITY_EDITOR || UNITY_WEBGL
        void Update()
        {
            if (_preWidth != Screen.width || _preHeight != Screen.height)
            {
                _preWidth = Screen.width;
                _preHeight = Screen.height;
                Awake();
            }
        }
#endif
    }
}

