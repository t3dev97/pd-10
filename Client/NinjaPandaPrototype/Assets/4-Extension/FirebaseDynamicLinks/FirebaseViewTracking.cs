﻿#if !BUILD_EGT
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Studio;

public class FirebaseViewTracking : FirebaseTracking
{
    private float _startTime;
    private bool _isTrack;
    private string _trackName;

    void OnEnable ()
    {
        if (!_isTrack)
        {
            _trackName = TrackingName;

            if (!string.IsNullOrEmpty(_trackName))
            {
                _isTrack = true;
                _startTime = Time.realtimeSinceStartup;

                DebugX.Log("FirebaseViewTracking: " + FirebaseEvent.UserLogin + " : " + FirebaseParam.UserStateView + " : " + _trackName);
                FirebaseDynamicLinks.Api.SetLogEvent(FirebaseEvent.UserLogin, FirebaseParam.UserStateView, _trackName);
            }
        }
    }

    void OnDisable()
    {
        _isTrack = false;

        var duration = (int)(Time.realtimeSinceStartup - _startTime);

        if (!string.IsNullOrEmpty(_trackName)) {
            DebugX.Log("FirebaseViewTracking Time: " + FirebaseEvent.UserView + " : " + _trackName + " : " + duration);
            FirebaseDynamicLinks.Api.SetLogEvent(FirebaseEvent.UserView, _trackName, duration);
        }
    }

    public void ForceTrack()
    {
        OnEnable();
    }
}
#endif