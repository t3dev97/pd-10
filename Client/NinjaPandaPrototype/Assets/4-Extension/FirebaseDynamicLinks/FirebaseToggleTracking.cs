﻿#if !BUILD_EGT
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Studio;

public class FirebaseToggleTracking : FirebaseTracking, IPointerDownHandler
{
    private Toggle _btn;
    private string _trackName;

    void Start()
    {
        if (_btn == null) {
            _btn = transform.GetComponent<Toggle>();
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (_btn != null && _btn.interactable)
        {
            RecordTracking();
        }
    }

    public void RecordTracking()
    {
        _trackName = TrackingName;

        if (!string.IsNullOrEmpty(_trackName))
        {
            DebugX.Log("FirebaseToggleTracking: " + FirebaseEvent.UserLogin + " : " + FirebaseParam.UserFunction + " : " + _trackName);
            FirebaseDynamicLinks.Api.SetLogEvent(FirebaseEvent.UserLogin, FirebaseParam.UserFunction, _trackName);
        }
    }
}
#endif