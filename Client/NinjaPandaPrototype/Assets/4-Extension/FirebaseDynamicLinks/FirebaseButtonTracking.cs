﻿#if !BUILD_EGT
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Studio;

public class FirebaseButtonTracking : FirebaseTracking, IPointerDownHandler
{
    private Button _btn;
    private string _trackName;

    void Start()
    {
        if (_btn == null) {
            _btn = transform.GetComponent<Button>();
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (_btn != null && _btn.interactable)
        {
            RecordTracking();
        }
    }
   
    public void RecordTracking()
    {
        _trackName = TrackingName;
        if (!string.IsNullOrEmpty(_trackName))
        {
            //DebugX.Log("FirebaseButtonTracking: " + FirebaseEvent.UserLogin + " : " + FirebaseParam.UserFunction + " : " + _trackName);

            FirebaseDynamicLinks.Api.SetLogEvent(FirebaseEvent.UserLogin, FirebaseParam.UserFunction, _trackName);
        }
    }
}
#endif