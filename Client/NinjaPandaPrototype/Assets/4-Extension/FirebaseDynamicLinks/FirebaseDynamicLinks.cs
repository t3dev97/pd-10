﻿#if !BUILD_EGT
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Studio;
using Studio.BC;
using Facebook.Unity;

public static class FirebaseEvent
{
    public static string UserLogin = "user_login";
    public static string UserView = "user_view";
}

public static class FirebaseParam
{
    public static string LoginOs = "login_os";
    public static string UserLevel = "user_level";
    public static string UserStateView = "user_view";
    public static string UserFunction = "user_function";
    public static string UserViewTimeSpent = "user_view_time_spent";
}

public class FirebaseFunctionName
{
    public static string NANG_CAP_DOI_HINH = "nang_cap_hoi_hinh";
}

public class FirebaseDynamicLinks : MonoBehaviour
{
    private static FirebaseDynamicLinks _api;

    public static FirebaseDynamicLinks Api
    {
        get
        {
            if (_api == null)
            {
                var go = new GameObject("_FirebaseDynamicLinks");
                _api = go.AddComponent<FirebaseDynamicLinks>();
                DontDestroyOnLoad(go);
            }

            return _api;
        }
    }

    public bool firebaseInitialized = false;
    public string refLinkUrl = "";
    public string deepLinkUrl = "";

    private static bool isInit;
    private Action _onCompleteInit;

#if !UNITY_WEBGL
#endif
    // When the app starts, check to make sure that we have
    // the required dependencies to use Firebase, and if not,
    // add them if possible.
    public void Init(Action OnCompleteInit = null)
    {
        isInit = false;
        _onCompleteInit = OnCompleteInit;
#if !UNITY_WEBGL
        StartCoroutine(DelayStart());

        if (!FB.IsInitialized)
            FB.Init(InitDeepLink, OnHideUnity);
        else
            InitDeepLink();
#else
        if (_onCompleteInit!=null)
            _onCompleteInit();
#endif
    }
    private IEnumerator DelayStart()
    {
        yield return new WaitForSeconds(3f);
        if (!isInit)
        {
            isInit = true;
            _onCompleteInit?.Invoke();
        }
    }
    public void InitDeepLink()
    {
        if (FB.IsInitialized)
        {
            FB.GetAppLink((appLinkResult) =>
            {
                deepLinkUrl = appLinkResult.Url;
                DebugX.LogError("FB.GetAppLink: " + deepLinkUrl);
                CallDeepLink();
            });
        }
    }

    void OnHideUnity(bool isGameShown)
    {
        if (!isGameShown)
        {
            // Pause the game - we will need to hide
            Time.timeScale = 0;
        }
        else
        {
            // Resume the game - we're getting focus again
            Time.timeScale = 1;
        }
    }

    // Handle initialization of the necessary firebase modules:
#if !UNITY_WEBGL
    void InitializeFirebase()
    {
        firebaseInitialized = true;
    }
#endif

    public void CallDeepLink()
    {
#if !UNITY_WEBGL
        DebugX.LogError("CallDeepLink: " + deepLinkUrl);

        if (!string.IsNullOrEmpty(deepLinkUrl))
        {
            //BCGameService.Api.SendWebRequest
            //(
            //    (dataComplete) =>
            //    {
            //        DebugX.Log("CallDeepLink: " + dataComplete.DeepTrace());
            //    },
            //    (dataError) =>
            //    {
            //        DebugX.LogError("CallDeepLink: " + dataError.DeepTrace());
            //    },
            //    WebKey.UPDATE_DEEPLINK,
            //    WebKey.url, deepLinkUrl,
            //    WebKey.deviceId, Utils.GetDeviceUniqueIdentifier()
            //);
        }
#endif
    }

    public void UpdateDeepLink()
    {
#if !UNITY_WEBGL
        DebugX.LogError("UpdateDeepLink: " + deepLinkUrl);

        if (!string.IsNullOrEmpty(deepLinkUrl))
        {
            //BCGameService.Api.SendWebRequest
            //(
            //    (dataComplete) =>
            //    {
            //        DebugX.Log("CallDeepLink: " + dataComplete.DeepTrace());
            //    },
            //    (dataError) =>
            //    {
            //        DebugX.LogError("CallDeepLink: " + dataError.DeepTrace());
            //    },
            //    WebKey.UPDATE_DEEPLINK,
            //    WebKey.url, deepLinkUrl,
            //    WebKey.deviceId, Utils.GetDeviceUniqueIdentifier()
            //);
        }
#endif
    }

    public void CallRefLink()
    {
#if !UNITY_WEBGL
        //DebugX.LogError("CallDeepLink: " + refLinkUrl);

        if (!string.IsNullOrEmpty(refLinkUrl))
        {
            //BCGameService.Api.SendWebRequest
            //(
            //    (dataComplete) =>
            //    {
            //        DynamicLinks.DynamicLinkReceived -= OnDynamicLink;
            //        DebugX.Log("CallDeepLink: " + dataComplete.DeepTrace());
            //    },
            //    (dataError) =>
            //    {
            //        DebugX.LogError("CallDeepLink: " + dataError.DeepTrace());
            //    },
            //    WebKey.UPDATE_REFLINK,
            //    WebKey.url, refLinkUrl,
            //    WebKey.deviceId, Utils.GetDeviceUniqueIdentifier()
            //);
        }
#endif
    }

    public void UpdateRefLink()
    {
#if !UNITY_WEBGL
        DebugX.LogError("UpdateRefLink: " + refLinkUrl);

        if (!string.IsNullOrEmpty(refLinkUrl))
        {
            //BCGameService.Api.SendWebRequest
            //(
            //    (dataComplete) =>
            //    {
            //        DebugX.Log("CallRefLink: " + dataComplete.DeepTrace());
            //    },
            //    (dataError) =>
            //    {
            //        DebugX.LogError("CallRefLink: " + dataError.DeepTrace());
            //    },
            //    WebKey.UPDATE_REFLINK,
            //    WebKey.url, refLinkUrl,
            //    WebKey.deviceId, Utils.GetDeviceUniqueIdentifier()
            //);
        }
#endif
    }

    public void TrackingUserOS(string OS)
    {
        DebugX.Log("FirebaseOSTracking: " + FirebaseEvent.UserLogin + " : " + FirebaseParam.LoginOs + " : " + OS);
        SetLogEvent(FirebaseEvent.UserLogin, FirebaseParam.LoginOs, OS);
    }

    public void SetUserName(string userName)
    {
#if !UNITY_WEBGL
#endif
    }

    public void SetUserProperty(string propertyName, string propertyValue)
    {
#if !UNITY_WEBGL
#endif
    }

#region LOG EVENT

    public void SetLogEvent(string evtName, string parmsName, string parmsValue)
    {
#if !UNITY_WEBGL
#endif
    }

    public void SetLogEvent(string evtName, string parmsName, int parmsValue)
    {
#if !UNITY_WEBGL
#endif
    }

    public void SetLogEvent(string evtName, string parmsName, float parmsValue)
    {
#if !UNITY_WEBGL
#endif
    }

    public void SetLogEvent(string evtName, string parmsName, long parmsValue)
    {
#if !UNITY_WEBGL
#endif
    }

#endregion

    public void SetCurrentScreen(string currentScreen, string className)
    {
#if !UNITY_WEBGL
#endif
    }

    // Display the dynamic link received by the application.
#if !UNITY_WEBGL
    void OnDynamicLink(object sender, EventArgs args)
    {
        if (!isInit)
        {
            isInit = true;
        }
    }
#endif
}
#endif