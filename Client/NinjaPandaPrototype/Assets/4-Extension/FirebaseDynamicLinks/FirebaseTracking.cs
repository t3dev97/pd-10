﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FirebaseTracking : MonoBehaviour
{
    public TrackType Type;
    public string TrackPrefix;
    public Text TextInput;

    public string TrackingName
    {
        get
        {
            if (Type == TrackType.Normal)
            {
                return TrackPrefix;
            }

            return TrackPrefix + (TextInput != null ? TextInput.text : "");
        }   
    }
}

public enum TrackType
{
    Normal,
    TextInput,
}
