using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Studio.BC
{
    public class BehaviourX : MonoBehaviour
    {
        private static BehaviourX _api;
        private static bool _inited;
        private int m_lastSecond;
        private Action m_onEveryFrame;
        private Action m_onEverySecond;
        private Action m_onScreenResize;
        private int m_screenH;
        private int m_screenW;
        public static float time { get; private set; }
        public static float realTime { get; private set; }

        public static BehaviourX Api
        {
            get { return _api; }
        }

        internal static void Initialize()
        {
            if (_inited) return;

            time = 0;
            realTime = 0;

            #region Delete UnityX Before

            DebugX.LogWarning("==> start FindObjectsOfType<BehaviourX>()");
            var unityXBefores = GameObject.FindObjectsOfType<BehaviourX>();
            DebugX.LogWarning("==> end FindObjectsOfType<BehaviourX>()");

            if (unityXBefores != null)
            {
                foreach (var unityXBefore in unityXBefores)
                {
                    if (unityXBefore != null) Destroy(unityXBefore.gameObject);
                }
            }
            #endregion

            new GameObject { name = "UnityX" }.AddComponent<BehaviourX>();
            _inited = true;
        }

        public IEnumerator UnloadResource()
        {
            yield break;
            //            if (collectCount < 3000)
            //            {
            //                yield break;
            //            }
            //
            //            collectCount = 0;
            //            var unloadAsset = Resources.UnloadUnusedAssets();
            //            while (!unloadAsset.isDone)
            //            {
            //                yield return null;
            //            }
        }

#if UNITY_WEBPLAYER
        public void j2u_vars(string vars) {
            UnityX.ParseUnityVars(vars);
        }
#endif

        //------------------------------------ EVENT LISTENERS ---------------------------------------

        public static void AddEveryFrameListener(Action p_onEveryFrame)
        {
            if (!_inited) { Initialize(); }
            _api.m_onEveryFrame -= p_onEveryFrame;
            _api.m_onEveryFrame += p_onEveryFrame;
        }

        public static void AddEverySecondListener(Action p_onEverySecond)
        {
            if (!_inited) { Initialize(); }
            _api.m_onEverySecond -= p_onEverySecond;
            _api.m_onEverySecond += p_onEverySecond;
        }

        public static void AddScreenResizeListener(Action p_onResize)
        {
            if (!_inited) { Initialize(); }
            _api.m_onScreenResize -= p_onResize;
            _api.m_onScreenResize += p_onResize;
        }

        public static void RemoveEveryFrameListener(Action p_action)
        {
            if (_inited && _api.m_onEveryFrame != null) { _api.m_onEveryFrame -= p_action; }
        }

        public static void RemoveEverySecondListener(Action p_action)
        {
            if (_inited && _api.m_onEverySecond != null) { _api.m_onEverySecond -= p_action; }
        }

        public static void RemoveScreenResizeListener(Action p_onResize)
        {
            if (_inited && _api.m_onScreenResize != null) { _api.m_onScreenResize -= p_onResize; }
        }

        //--------------------------------------- Unity callbacks --------------------------------------------

        public static Coroutine StartRoutine(IEnumerator routine)
        {
            return _api.StartCoroutine(routine);
        }

        private void Awake()
        {
            if (_inited)
            {
                DebugX.LogWarning("There should be only 1 instance of UnityX - Destroying the new one");
                //Destroy(gameObject); //don't destroy gameObject
                //return;
            }

            DontDestroyOnLoad(this);
            _api = this;
        }

        private int collectCount;

        private void Update()
        {
            if (_inited && _api != this)
            {
                Destroy(gameObject);
                return;
            }

            time = Time.time;
            realTime = Time.realtimeSinceStartup;

            collectCount++;
            //Auto collect unused resources every 60 second
            /* if (collectCount > 3000*3)
             { 
                 Resources.UnloadUnusedAssets();
                 collectCount = 0;
             }*/

            //update onEverySecond
            if (time - m_lastSecond > 1f)
            {
                m_lastSecond += 1;
                if (m_onEverySecond != null) { m_onEverySecond(); }
            }

            //update onEveryFrame
            if (m_onEveryFrame != null) { m_onEveryFrame(); }

            //update onScreenResize
            if (Screen.width != m_screenW || Screen.height != m_screenH)
            {
                m_screenW = Screen.width;
                m_screenH = Screen.height;
                if (m_onScreenResize != null) { m_onScreenResize(); }
            }
        }
    }
}


