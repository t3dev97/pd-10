﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;

namespace Studio.BC
{
    public class IapManager : MonoBehaviour, IStoreListener
    {
        public bool IsPurchasing;

        private Dictionary<int, string> _androidPurchaseDic;
        private List<string> _androidPurchaseList;

        private Dictionary<int, string> _iOSPurchaseDic;
        private List<string> _iOSPurchaseList;

        private static IapManager api;
        public static IapManager Api
        {
            get
            {
                if (api == null)
                {
                    var go = new GameObject("IAPController");
                    DontDestroyOnLoad(go);

                    api = go.AddComponent<IapManager>();
                }

                return api;
            }
        }
        // public static IAPController api { private set; get; }
        public static List<string> ConsumablePurchaseList;
        public static List<string> NonConsumablePurchaseList;

        private static IStoreController m_StoreController;          // The Unity Purchasing system.
        private static IExtensionProvider m_StoreExtensionProvider; // The store-specific Purchasing subsystems.

        private enum ProcessResult { InProcess, Fail, Success };

        private Action<bool, string> _onCompleteAction;
        private Action<bool, string> _onCompleteInit;

        public void InitializePurchasing(Action<bool, string> onCompleteInit, List<string> consumableDataList = null, List<string> nonConsumableDataList = null)
        {
            IsPurchasing = false;

            if (m_StoreController == null)
            {
                _onCompleteInit = onCompleteInit;

                if (IsInitialized())
                {
                    _onCompleteInit(true, "Already Initialized!");
                    return;
                }
                var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

                // Consumable Items
                if (consumableDataList != null)
                {
                    ConsumablePurchaseList = consumableDataList;
                    foreach (var item in ConsumablePurchaseList)
                        builder.AddProduct(item.ToLower(), ProductType.Consumable);
                }

                // NonConsumable Items.
                if (nonConsumableDataList != null)
                {
                    NonConsumablePurchaseList = nonConsumableDataList;
                    foreach (var item in NonConsumablePurchaseList)
                        builder.AddProduct(item.ToLower(), ProductType.NonConsumable);
                }
                UnityPurchasing.Initialize(this, builder);
            }
            else if (IsInitialized())
            {
                _onCompleteInit(true, "Already Initialized!");
                return;
            }
        }
    
        private bool IsInitialized()
        {
            // Only say we are initialized if both the Purchasing references are set.
            return m_StoreController != null && m_StoreExtensionProvider != null;
        }


        public void BuyProductID(string packageName, Action<bool, string> onCompleteAction)
        {
            if (IsInitialized())
            {
                _onCompleteAction = onCompleteAction;
                Product product = m_StoreController.products.WithID(packageName.ToLower());
                if (product != null && product.availableToPurchase)
                {
                    //DebugX.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
                    m_StoreController.InitiatePurchase(product);
                }
                else
                {
                    DebugX.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
                    if (_onCompleteAction != null)
                        _onCompleteAction(false, BCLocalize.ID_GUI_IAP_FAILED);
                }

            }
            else
            {
                DebugX.Log("BuyProductID: FAIL. Not initialized.");
                if (onCompleteAction != null)
                {
                    onCompleteAction(false, BCLocalize.ID_GUI_IAP_FAILED_INIT);
                }
            }

        }

        // Restore purchases previously made by this customer. Some platforms automatically restore purchases, like Google. 
        // Apple currently requires explicit purchase restoration for IAP, conditionally displaying a password prompt.
        public void RestorePurchases()
        {
            // If Purchasing has not yet been set up ...
            if (!IsInitialized())
            {
                // ... report the situation and stop restoring. Consider either waiting longer, or retrying initialization.
                Debug.Log("RestorePurchases FAIL. Not initialized.");
                return;
            }

            // If we are running on an Apple device ... 
            if (Application.platform == RuntimePlatform.IPhonePlayer ||
                Application.platform == RuntimePlatform.OSXPlayer)
            {
                // ... begin restoring purchases
                Debug.Log("RestorePurchases started ...");

                // Fetch the Apple store-specific subsystem.
                var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
                // Begin the asynchronous process of restoring purchases. Expect a confirmation response in 
                // the Action<bool> below, and ProcessPurchase if there are previously purchased products to restore.
                apple.RestoreTransactions((result) => {
                    // The first phase of restoration. If no more responses are received on ProcessPurchase then 
                    // no purchases are available to be restored.
                    Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
                });
            }
            // Otherwise ...
            else
            {
                // We are not running on an Apple device. No work is necessary to restore purchases.
                Debug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
            }
        }

        public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
        {
            // Purchasing has succeeded initializing. Collect our Purchasing references.
            DebugX.Log("OnInitialized: PASS");
            // Overall Purchasing system, configured with products for this application.
            m_StoreController = controller;
            // Store specific subsystem, for accessing device-specific store features.
            m_StoreExtensionProvider = extensions;
            _onCompleteInit(true, "Initialize Completed!");
        }

        public void OnInitializeFailed(InitializationFailureReason error)
        {
            // Purchasing set-up has not succeeded. Check error for reason. Consider sharing this reason with the user.
            DebugX.Log("OnInitializeFailed InitializationFailureReason:" + error);
            _onCompleteInit(false, "Initialize Failed: " + error);
        }

        public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
        {
            if (ConsumablePurchaseList != null)
            {
                foreach (var item in ConsumablePurchaseList)
                {
                    if (string.Equals(args.purchasedProduct.definition.id, item.ToLower(), StringComparison.Ordinal))
                    {
                        if (_onCompleteAction != null)
                        {
#if UNITY_ANDROID
                            DebugX.Log("Receipt: " + args.purchasedProduct.receipt);
                            _onCompleteAction(true, args.purchasedProduct.receipt);
#endif
#if UNITY_IOS
                             DebugX.Log("Receipt: " + args.purchasedProduct.receipt);
                            _onCompleteAction(true, args.purchasedProduct.receipt);
#endif
                        }

                        return PurchaseProcessingResult.Complete;
                    }
                }
            }
            if (NonConsumablePurchaseList != null)
            {
                foreach (var item in NonConsumablePurchaseList)
                {
                    if (string.Equals(args.purchasedProduct.definition.id, item.ToLower(), StringComparison.Ordinal))
                    {
                        if (_onCompleteAction != null)
                        {
#if UNITY_ANDROID
                            DebugX.Log("Receipt: " + args.purchasedProduct.receipt);
                            _onCompleteAction(true, args.purchasedProduct.receipt);
#endif
#if UNITY_IOS
                             DebugX.Log("Receipt: " + args.purchasedProduct.receipt);
                            _onCompleteAction(true, args.purchasedProduct.receipt);
#endif
                        }

                        return PurchaseProcessingResult.Complete;
                    }
                }
            }

            DebugX.Log(string.Format("ProcessPurchase: FAIL. Unrecognized product: '{0}'", args.purchasedProduct.definition.id));
            // Return a flag indicating whether this product has completely been received, or if the application needs 
            // to be reminded of this purchase at next app launch. Use PurchaseProcessingResult.Pending when still 
            // saving purchased products to the cloud, and when that save is delayed. 

            if (_onCompleteAction != null)
                _onCompleteAction(false, BCLocalize.ID_GUI_IAP_FAILED);

            return PurchaseProcessingResult.Complete;
        }

        public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
        {
            DebugX.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
            switch (failureReason)
            {
                case PurchaseFailureReason.UserCancelled:
                    {
                        if (_onCompleteAction != null)
                            _onCompleteAction(false, BCLocalize.ID_GUI_IAP_USER_CANCEL);
                        break;
                    }
                default:
                    {
                        if (_onCompleteAction != null)
                            _onCompleteAction(false, BCLocalize.ID_GUI_IAP_FAILED);
                        break;
                    }
            }

        }
    }
}
