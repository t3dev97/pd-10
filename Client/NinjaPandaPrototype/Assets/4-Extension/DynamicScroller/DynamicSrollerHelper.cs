﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TTBT.DynamicScroller;
using System;

class DynamicScrollerHelperData {
	public DynamicScroller Scroller;
	public int ItemCount;
	public float CellSize;
	public DynamicScrollerCellView PrefabItem;
}

public class DynamicScrollerHelper : MonoBehaviour,  IDynamicScrollerDelegate{
	public Action<DynamicScroller, DynamicScrollerCellView> OnFocusCellAction;
	Dictionary<DynamicScroller, DynamicScrollerHelperData> _ref;
	Dictionary<DynamicScroller, DynamicScrollerHelperData> Ref
	{
		get
		{
			if (_ref == null)
				_ref = new Dictionary<DynamicScroller, DynamicScrollerHelperData>();
			return _ref;
		}
	}
	public void Reload(DynamicScroller scroller, int itemCount, float cellSize, DynamicScrollerCellView prefabItem)
	{
		if (!Ref.ContainsKey(scroller))
		{
			Ref.Add(scroller, new DynamicScrollerHelperData());
		}
		Ref[scroller].ItemCount = itemCount;
		Ref[scroller].CellSize = cellSize;
		Ref[scroller].PrefabItem = prefabItem;
		Ref[scroller].Scroller = scroller;
	}
	public int GetCellCount(DynamicScroller scroller)
	{
		if (!Ref.ContainsKey(scroller))
			return 0;
		return Ref[scroller].ItemCount;
	}

	public float GetCellSize(DynamicScroller scroller, int dataIndex)
	{
		if (!Ref.ContainsKey(scroller))
			return 0;
		return Ref[scroller].CellSize;
	}

	public DynamicScrollerCellView GetCellView(DynamicScroller scroller, int dataIndex)
	{
		var cell = scroller.GetCellView(Ref[scroller].PrefabItem, dataIndex);
		if (OnFocusCellAction != null)
			OnFocusCellAction(scroller, cell);
		return cell;
	}
}
