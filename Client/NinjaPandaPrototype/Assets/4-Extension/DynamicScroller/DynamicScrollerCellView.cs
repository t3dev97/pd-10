﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace TTBT.DynamicScroller
{
	public class DynamicScrollerCellView : MonoBehaviour
	{
		[HideInInspector]
		public int DataIndex;
		public string CellIdentifier
		{
			get
			{
				return GetType().ToString();
			}
		}
		private UnityEngine.UI.LayoutElement _layoutElement;
		public UnityEngine.UI.LayoutElement LayoutElement
		{
			get
			{
				if (_layoutElement == null)
				{
					_layoutElement = gameObject.GetComponent<UnityEngine.UI.LayoutElement>();
					if (_layoutElement == null)
						_layoutElement = gameObject.AddComponent<UnityEngine.UI.LayoutElement>();
				}
				return _layoutElement;
			}
			set { _layoutElement = value; }
		}
	}
}
