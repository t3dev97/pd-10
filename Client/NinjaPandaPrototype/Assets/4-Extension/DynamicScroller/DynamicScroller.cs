﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace TTBT.DynamicScroller
{
	public enum DynamicScrollerCellAlightment
	{
		Normal = 1,
		Center = 2
	}
	public enum DynamicScrollerType
	{
		Horizontal = 1,
		Vertical = 2
	}

	public enum DynamicScrollerDirection
	{
		Left = 1,
		Right = 2,
		Top = 3,
		Bottom = 4
	}

	public interface IDynamicScrollerDelegate
	{
		int GetCellCount(DynamicScroller scroller);
		float GetCellSize(DynamicScroller scroller, int dataIndex);
		DynamicScrollerCellView GetCellView(DynamicScroller scroller, int dataIndex);
	}

	[RequireComponent(typeof(ScrollRect))]
	[RequireComponent(typeof(Image))]
	[RequireComponent(typeof(Mask))]
	public class DynamicScroller : MonoBehaviour
	{
		/// <summary>
		/// ScrollerType: horizontal or vertical
		/// </summary>
		public DynamicScrollerType ScrollerType = DynamicScrollerType.Horizontal;
		/// <summary>
		/// Padding of Scroller
		/// </summary>
		public RectOffset Padding;
		/// <summary>
		/// Spacing between 2 cells
		/// </summary>
		public float Spacing;
		/// <summary>
		/// Number of cell in one line. Default is one cell. In this case, scroller will take first dataIndex cellSize
		/// </summary>
		#region MiniCellOptions
		public int CellPerLine = 3;
		/// <summary>
		/// In cas CellPerLine > 1, if SquareMiniCell = true. MiniCell in line will be forced squared shape
		/// </summary>
		[HideInInspector]
		public bool SquareMiniCell = true;
		/// <summary>
		/// Alight miniCell in line
		/// </summary>
		[HideInInspector]
		public DynamicScrollerCellAlightment MiniCellAligtment = DynamicScrollerCellAlightment.Center;
		/// <summary>
		/// The size of the is use custom mini cell.
		/// </summary>
		[HideInInspector]
		public bool IsUseCustomMiniCellSize = false;
		/// <summary>
		/// The size of minicell. With limit range.
		/// </summary>
		[HideInInspector]
		public float CustomMiniCellSize;
		/// <summary>
		/// This use in case mutli cell per line. (CellPerLine > 1)
		/// </summary>
		private int totalLine;
		/// <summary>
		/// This use in case multi cell per line. This is size of minicell
		/// </summary>
		private Vector2 miniCellSize;
		/// <summary>
		/// This use in case multi cell per line. This is spacing between 2 minicells
		/// </summary>
		private Vector2 miniCellSpacing;
		/// <summary>
		/// The mini cell in view port.
		/// </summary>
		private int miniCellCountInViewPort;
		/// <summary>
		/// Size of current active cells
		/// </summary>
		private float activeCellSize;
		/// <summary>
		/// offset calculate after scroll
		/// </summary>
		private float offsetScroll;
		#endregion
		/// <summary>
		/// Delegate
		/// </summary>
		private IDynamicScrollerDelegate _delegate;
		public IDynamicScrollerDelegate Delegate
		{
			get
			{
				return _delegate;
			}
			set { _delegate = value; }
		}

		private ScrollRect _scroller;
		private ScrollRect scroller
		{
			get
			{
				if (_scroller == null)
				{
					_scroller = GetComponent<ScrollRect>();
				}
				return GetComponent<ScrollRect>();
			}
		}
		private Image _scrollerBG;
		private Image scrollerBG
		{
			get
			{
				if (_scrollerBG == null)
				{
					_scrollerBG = GetComponent<Image>();
					var color = _scrollerBG.color;
					color.a = 0;
					_scrollerBG.color = color;
				}
				return _scrollerBG;
			}
		}

		private Mask _scrollerMask;
		private Mask scrollerMask
		{
			get
			{
				if (_scrollerMask == null)
					_scrollerMask = GetComponent<Mask>();
				_scrollerMask.showMaskGraphic = false;
				return _scrollerMask;
			}
		}
		private RectTransform _container;
		private RectTransform container
		{
			get
			{
				if (_container == null)
				{
					var goContainer = scroller.transform.Find("_Container");
					if (goContainer == null)
						goContainer = new GameObject("_Container").transform;
					goContainer.SetParent(scroller.transform, false);
					_container = goContainer.gameObject.AddComponent<RectTransform>();
				}
				return _container;
			}
		}

		private List<DynamicScrollerCellView> _tempCells;
		private List<DynamicScrollerCellView> TempCells
		{
			get
			{
				if (_tempCells == null)
					_tempCells = new List<DynamicScrollerCellView>();
				return _tempCells;
			}
			set { _tempCells = value; }
		}

		private List<DynamicScrollerLineView> _tempLines;
		private List<DynamicScrollerLineView> TempLines
		{
			get
			{
				if (_tempLines == null)
				{
					_tempLines = new List<DynamicScrollerLineView>();
				}
				return _tempLines;
			}
			set
			{
				_tempLines = value;
			}
		}

		private List<DynamicScrollerCellView> _activeCells;
		public List<DynamicScrollerCellView> ActiveCells
		{
			get
			{
				if (_activeCells == null)
				{
					_activeCells = new List<DynamicScrollerCellView>();
				}
				_activeCells.Clear();
				_activeCells.AddRange(TempCells.FindAll(x => x.gameObject.activeSelf));
				return _activeCells;
			}
			set { _activeCells = value; }
		}

		private List<DynamicScrollerCellView> _reuseCells;
		public List<DynamicScrollerCellView> ReuseCells
		{
			get
			{
				if (_reuseCells == null)
					_reuseCells = new List<DynamicScrollerCellView>();
				return _reuseCells;
			}
			set { _reuseCells = value; }
		}

		private List<DynamicScrollerLineView> _reuseLines;
		public List<DynamicScrollerLineView> ReuseLines
		{
			get
			{
				if (_reuseLines == null)
					_reuseLines = new List<DynamicScrollerLineView>();
				return _reuseLines;
			}
			set { _reuseLines = value; }
		}
		public float ContainerPos
		{
			get
			{
				return ScrollerType == DynamicScrollerType.Horizontal ? -container.anchoredPosition.x : container.anchoredPosition.y; ;
			}
		}
		/// <summary>
		/// First padder of Container, this one control cell position in viewport
		/// </summary>
		private LayoutElement firstPadder;
		/// <summary>
		/// Second padder
		/// </summary>
		private LayoutElement secondPadder;

		/// <summary>
		/// Begin dataIndex in viewPort
		/// </summary>
		[HideInInspector]
		public int BeginIndex;
		/// <summary>
		/// End dataIndex in viewPort
		/// </summary>
		[HideInInspector]
		public int EndIndex;

		private bool reloadData;
		private float viewPort;
		/// <summary>
		/// cell addition for protect view from invisible
		/// </summary>
		private int additionCell = 2;
		/// <summary>
		/// Direction of current Scroll action
		/// </summary>
		private DynamicScrollerDirection scrollDir = DynamicScrollerDirection.Top;
		/// <summary>
		/// Total size of container
		/// </summary>
		private float totalSize;

		private void Awake()
		{
			Init();
		}

		void Init()
		{
			scroller.horizontal = ScrollerType == DynamicScrollerType.Horizontal;
			scroller.vertical = ScrollerType == DynamicScrollerType.Vertical;

			if (ScrollerType == DynamicScrollerType.Horizontal)
			{
				var layout = container.gameObject.AddComponent<HorizontalLayoutGroup>();
				layout.spacing = Spacing;
				layout.padding = Padding;
				layout.childForceExpandHeight = true;
				layout.childForceExpandWidth = false;
				layout.childControlHeight = true;
				layout.childControlWidth = true;
				container.anchorMin = new Vector2(0, 0);
				container.anchorMax = new Vector2(0, 1);
				container.offsetMin = new Vector2(container.offsetMin.x, 0);
				container.offsetMax = new Vector2(container.offsetMax.x, 0);
				container.pivot = Vector2.zero;
			}
			else
			{
				var layout = container.gameObject.AddComponent<VerticalLayoutGroup>();
				layout.childForceExpandHeight = false;
				layout.childForceExpandWidth = true;
				layout.childControlHeight = true;
				layout.childControlWidth = true;
				layout.spacing = Spacing;
				layout.padding = Padding;
				container.anchorMin = new Vector2(0, 1);
				container.anchorMax = new Vector2(1, 1);
				container.offsetMin = new Vector2(0, container.offsetMin.y);
				container.offsetMax = new Vector2(0, container.offsetMax.y);
				container.pivot = Vector2.one;
			}

			_scroller.onValueChanged.RemoveAllListeners();
            _scroller.onValueChanged.AddListener(OnScrollerValueChange);
            _scroller.content = container;

			if (firstPadder == null)
			{
				var goPadder = new GameObject("FirstPadder");
				goPadder.transform.SetParent(container, false);
				firstPadder = goPadder.AddComponent<LayoutElement>();
				firstPadder.transform.SetAsFirstSibling();
			}
			if (secondPadder == null)
			{
				var goPadder = new GameObject("SecondPadder");
				goPadder.transform.SetParent(container, false);
				secondPadder = goPadder.AddComponent<LayoutElement>();
				secondPadder.transform.SetAsLastSibling();
			}
			//ReloadData();
		}

		public DynamicScrollerCellView GetCellView(DynamicScrollerCellView prefab, int dataIndex)
		{
			var foundCell = ReuseCells.Find(x => x.DataIndex == dataIndex);
			if (foundCell == null)
				foundCell = TempCells.Find(x => !x.gameObject.activeSelf && x.CellIdentifier == prefab.CellIdentifier);
			else
				ReuseCells.Remove(foundCell);
			if (foundCell == null)
			{
				foundCell = Instantiate(prefab);
				foundCell.transform.SetParent(container, false);
				TempCells.Add(foundCell);
			}
			foundCell.DataIndex = dataIndex;
			foundCell.gameObject.SetActive(true);
			return foundCell;
		}
		private DynamicScrollerLineView GetLineView(int lineIndex)
		{
			var foundLine = ReuseLines.Find(x => x.DataIndex == lineIndex);
			if (foundLine == null)
				foundLine = TempLines.Find(x => !x.gameObject.activeSelf);
			else
				ReuseLines.Remove(foundLine);
			if (foundLine == null)
			{
				var newLine = new GameObject("_Line" + TempLines.Count);
				newLine.transform.SetParent(container, false);
				foundLine = newLine.AddComponent<DynamicScrollerLineView>();
				TempLines.Add(foundLine);
			}
			int beginIndex = lineIndex * CellPerLine;
			int endIndex = beginIndex + CellPerLine;
			endIndex = endIndex > Delegate.GetCellCount(this) ? Delegate.GetCellCount(this) : endIndex;
			foundLine.Init(this, beginIndex, endIndex - beginIndex, miniCellSize, miniCellSpacing);
			foundLine.gameObject.SetActive(true);
			foundLine.DataIndex = lineIndex;
			return foundLine;
		}
		void OnScrollerValueChange(Vector2 vec)
		{
			if (Delegate == null)
				return;
			if (ScrollerType == DynamicScrollerType.Horizontal)
				scrollDir = scroller.velocity.x > 0 ? DynamicScrollerDirection.Right : DynamicScrollerDirection.Left;
			else
				scrollDir = scroller.velocity.y > 0 ? DynamicScrollerDirection.Top : DynamicScrollerDirection.Bottom;
			float cellCount = Delegate.GetCellCount(this);
			if (CellPerLine > 1)
			{
                if (CellPerLine == 0)
                    CellPerLine = 1;
                var totalLine = cellCount / CellPerLine;
				if (cellCount % CellPerLine > 0)
					totalLine++;
				cellCount = totalLine;
			}
			var pos = ContainerPos;
			var currentBeginIndex = 0;
			float currentTotalSize = 0;
			for (int i = 0; i < cellCount; i++)
			{
				float cellSize = Delegate.GetCellSize(this, i);
				currentTotalSize += cellSize + Spacing;
				if (currentTotalSize > pos)
				{
					currentBeginIndex = i;
					break;
				}
			}
			CalScrollOffset();
			if (currentBeginIndex != BeginIndex)
				reloadData = true;
		}
		public void ReloadData(bool isResetPosition = true)
		{
			if (Delegate == null)
				return;
			if(isResetPosition)
				container.anchoredPosition = Vector2.zero;
			RectTransform rectTrans = GetComponent<RectTransform>();
			int cellCount = Delegate.GetCellCount(this);
			float cellSize = Delegate.GetCellSize(this, 0);
			if (CellPerLine == 1)
			{
				totalSize = 0;
				for (int i = 0; i < cellCount; i++)
				{
					cellSize = Delegate.GetCellSize(this, i);
					totalSize += cellSize + Spacing;
				}
			}
			else
			{
				//If cellperLine > 1, that every-line's size need to be equal;
				totalLine = cellCount / CellPerLine;
				if (cellCount % CellPerLine > 0)
					totalLine++;
				totalSize = totalLine * (cellSize + Spacing);
				float _miniSize = CustomMiniCellSize;
				if (ScrollerType == DynamicScrollerType.Horizontal)
				{
					if (!IsUseCustomMiniCellSize)
						_miniSize = (int)(rectTrans.rect.height - Spacing * (CellPerLine + 1)) / CellPerLine;
					if (SquareMiniCell)
					{
						_miniSize = _miniSize < cellSize ? _miniSize : cellSize;
						miniCellSize = new Vector2(_miniSize, _miniSize);
					}
					else
						miniCellSize = new Vector2(cellSize, _miniSize);
					miniCellSpacing.x = 0;
					miniCellSpacing.y = Spacing;
					if (MiniCellAligtment == DynamicScrollerCellAlightment.Center)
						miniCellSpacing.y = (rectTrans.rect.height - (CellPerLine * _miniSize)) / (CellPerLine - 1);
				}
				else
				{
					if (!IsUseCustomMiniCellSize)
						_miniSize = (int)(rectTrans.rect.width - Spacing * (CellPerLine + 1)) / CellPerLine;
					if (SquareMiniCell)
					{
						_miniSize = _miniSize < cellSize ? _miniSize : cellSize;
						miniCellSize = new Vector2(_miniSize, _miniSize);
					}
					else
						miniCellSize = new Vector2(_miniSize, cellSize);
					miniCellSpacing.x = Spacing;
					if (MiniCellAligtment == DynamicScrollerCellAlightment.Center)
						miniCellSpacing.x = (rectTrans.rect.width - (CellPerLine * _miniSize)) / (CellPerLine - 1);
					miniCellSpacing.y = 0;
				}
			}
			if (ScrollerType == DynamicScrollerType.Vertical)
			{
				container.sizeDelta = new Vector2(container.sizeDelta.x, totalSize);
				viewPort = Mathf.Abs(rectTrans.rect.height);
			}
			else
			{
				container.sizeDelta = new Vector2(totalSize, container.sizeDelta.y);
				viewPort = Mathf.Abs(rectTrans.rect.width);
			}
			if (CellPerLine > 1)
			{
				miniCellCountInViewPort = Mathf.CeilToInt(viewPort / (cellSize + Spacing)) + additionCell;
				activeCellSize = miniCellCountInViewPort * (cellSize + Spacing);
			}
			CalScrollOffset();

            reloadData = true;
		}

		private void Update()
		{
		    if (!reloadData || Delegate == null)
		    {
		        return;
		    }

			if (CellPerLine > 1)
			{
				UpdateMultiCell();
				return;
			}

			int cellCount = Delegate.GetCellCount(this);
			var pos = ContainerPos;
			float currentTotalSize = 0;
			float firstPadderSize = pos;
			BeginIndex = -1;
			activeCellSize = 0;
			EndIndex = cellCount;
			for (int i = 0; i < cellCount; i++)
			{
				float cellSize = Delegate.GetCellSize(this, i);
				currentTotalSize += cellSize + Spacing;
				if (BeginIndex < 0 && (currentTotalSize > (pos + offsetScroll)))
				{
					BeginIndex = i;
					firstPadderSize = currentTotalSize - (cellSize + Spacing);
					currentTotalSize = 0;
					activeCellSize += (cellSize + Spacing);
				}
				else if (BeginIndex >= 0 && currentTotalSize > viewPort)
				{
					EndIndex = i + additionCell;
					activeCellSize += (cellSize + Spacing);
					for (int j = 0; j < additionCell; j++)
					{
						if (j < cellCount)
							activeCellSize += Delegate.GetCellSize(this, j) + Spacing;
					}
					break;
				}
				else if (BeginIndex >= 0)
					activeCellSize += (cellSize + Spacing);
			}
			BeginIndex = BeginIndex < 0 ? 0 : BeginIndex;
			EndIndex = EndIndex >= cellCount ? cellCount : EndIndex;
			var cellInActive = EndIndex - BeginIndex;
			var cells = ActiveCells;
			ReuseCells.Clear();
			foreach (var cell in cells)
			{
				if (cell.DataIndex >= BeginIndex && cell.DataIndex < EndIndex)
					ReuseCells.Add(cell);
				else
				{
					cell.gameObject.SetActive(false);
					cellInActive--;
					if (cellInActive <= 0)
					{
						DestroyObject(cell.gameObject);
						cell.name = "";
					}
				}
			}
			cells.RemoveAll(x => x == null || string.IsNullOrEmpty(x.name));
			TempCells.RemoveAll(x => x == null || string.IsNullOrEmpty(x.name));
			for (int i = BeginIndex; i < EndIndex; i++)
			{
				var cellView = Delegate.GetCellView(this, i);
				cellView.DataIndex = i;
				if (ScrollerType == DynamicScrollerType.Horizontal)
				{
					cellView.LayoutElement.minHeight = 0;
					cellView.LayoutElement.minWidth = Delegate.GetCellSize(this, i);
					cellView.LayoutElement.preferredWidth = cellView.LayoutElement.minWidth;
					cellView.LayoutElement.preferredHeight = 0;
				}
				else
				{
					cellView.LayoutElement.minWidth = 0;
					cellView.LayoutElement.minHeight = Delegate.GetCellSize(this, i);
					cellView.LayoutElement.preferredHeight = cellView.LayoutElement.minHeight;
					cellView.LayoutElement.preferredWidth = 0;
				}
				cellView.transform.SetAsLastSibling();
				cellView.name = "_Cell" + i;
			}
			//if (firstPadderSize > pos)
			//	firstPadderSize = pos;
			//if (firstPadderSize < pos - (activeCellSize - viewPort))
			//	firstPadderSize = pos - (activeCellSize - viewPort);
			if (ScrollerType == DynamicScrollerType.Vertical)
			{
				firstPadder.preferredWidth = 0;
				firstPadder.preferredHeight = firstPadderSize;
				secondPadder.preferredWidth = 0;
				secondPadder.preferredHeight = container.rect.size.y - activeCellSize - firstPadderSize;
			}
			else
			{

				firstPadder.preferredWidth = firstPadderSize;
				firstPadder.preferredHeight = 0;
				secondPadder.preferredWidth = container.rect.size.y - activeCellSize - firstPadderSize;
				secondPadder.preferredHeight = 0;
			}
			firstPadder.transform.SetAsFirstSibling();
			secondPadder.transform.SetAsLastSibling();
			reloadData = false;
		}

		void UpdateMultiCell()
		{
			int cellCount = Delegate.GetCellCount(this);
			float cellSize = Delegate.GetCellSize(this, 0);
			var pos = ContainerPos + offsetScroll;
			BeginIndex = (int)(pos / (cellSize + Spacing));
			BeginIndex = BeginIndex < 0 ? 0 : BeginIndex;
			EndIndex = BeginIndex + miniCellCountInViewPort;
			EndIndex = EndIndex >= totalLine ? totalLine : EndIndex;
			var cells = ActiveCells;
			ReuseCells.Clear();
			int realBeginIndex = BeginIndex * CellPerLine;
			int realEndIndex = EndIndex * CellPerLine;
			realEndIndex = realEndIndex < cellCount ? realEndIndex : cellCount - 1;
			realBeginIndex = realBeginIndex > cellCount ? cellCount : realBeginIndex;
			foreach (var cell in cells)
			{
				if (cell.DataIndex >= realBeginIndex && cell.DataIndex < realEndIndex)
					ReuseCells.Add(cell);
				else
					cell.gameObject.SetActive(false);
			}
			ReuseLines.Clear();
			foreach (var line in TempLines)
			{
				if (line.DataIndex >= BeginIndex && line.DataIndex < EndIndex && line.gameObject.activeSelf)
					ReuseLines.Add(line);
				else
					line.gameObject.SetActive(false);
			}
			for (int i = BeginIndex; i < EndIndex; i++)
			{
				var line = GetLineView(i);
				line.DataIndex = i;
				if (ScrollerType == DynamicScrollerType.Horizontal)
				{
					line.LayoutElement.minHeight = 0;
					line.LayoutElement.minWidth = Delegate.GetCellSize(this, i);
					line.LayoutElement.preferredWidth = line.LayoutElement.minWidth;
					line.LayoutElement.preferredHeight = 0;
				}
				else
				{
					line.LayoutElement.minWidth = 0;
					line.LayoutElement.minHeight = Delegate.GetCellSize(this, i);
					line.LayoutElement.preferredHeight = line.LayoutElement.minHeight;
					line.LayoutElement.preferredWidth = 0;
				}
				line.transform.SetAsLastSibling();
				line.name = "_Line" + i;
			}
			float firstPadderSize = (BeginIndex) * (cellSize + Spacing);
			//var limitRange = new Vector2(ContainerPos - (activeCellViewPort - viewPort), ContainerPos);
			//if (firstPadderSize > limitRange.y)
			//	firstPadderSize = limitRange.y;
			//if (firstPadderSize < limitRange.x)
			//	firstPadderSize = limitRange.x;
			if (ScrollerType == DynamicScrollerType.Vertical)
			{
				firstPadder.preferredWidth = 0;
				firstPadder.preferredHeight = firstPadderSize;
				secondPadder.preferredWidth = 0;
				secondPadder.preferredHeight = container.rect.size.y - activeCellSize - firstPadderSize;
			}
			else
			{

				firstPadder.preferredWidth = firstPadderSize;
				firstPadder.preferredHeight = 0;
				secondPadder.preferredWidth = container.rect.size.y - activeCellSize - firstPadderSize;
				secondPadder.preferredHeight = 0;
			}
			firstPadder.transform.SetAsFirstSibling();
			secondPadder.transform.SetAsLastSibling();
			reloadData = false;
		}

		public void JumpToDataIndex(int dataIndex, float offsetFromBegin = 0.5f, Action onCompleteAction = null, float jumpDuration = 0.5f)
		{
			if (Delegate == null)
				return;
			if (totalSize <= 0)
				ReloadData();
			//if (_container.rect.size.y < viewPort)
			//{
			//	if (onCompleteAction != null)
			//		onCompleteAction();
			//	return;
			//}
			int cellCount = Delegate.GetCellCount(this);
			if (dataIndex > cellCount || dataIndex < 0)
				return;
			float cellSize = Delegate.GetCellSize(this, dataIndex);
			float jumpPos = 0;
			if (CellPerLine == 1)
			{
				for (int i = 0; i < dataIndex; i++)
				{
					jumpPos += Delegate.GetCellSize(this, i) + Spacing;
				}
			}
			else
			{
				int line = dataIndex / CellPerLine;
				jumpPos = line * (cellSize + Spacing);
			}
			float offsetDistance = offsetFromBegin * viewPort;
			if (jumpPos > offsetDistance)
				jumpPos -= offsetDistance;
			var pos = ScrollerType == DynamicScrollerType.Horizontal ? container.anchoredPosition.x : container.anchoredPosition.y;
			if (ScrollerType == DynamicScrollerType.Horizontal)
				jumpPos *= -1;
			LeanTweenType tweenType = scrollDir == DynamicScrollerDirection.Top || scrollDir == DynamicScrollerDirection.Right ? LeanTweenType.easeInBack : LeanTweenType.easeOutBack;
			var farDistance = 3 * viewPort;
			if (jumpPos > pos && jumpPos - pos > farDistance)
				pos = jumpPos - farDistance;
			else if (jumpPos < pos && pos - jumpPos > farDistance)
				pos = jumpPos + farDistance;
			SetContainerAnchorPos(pos);
			CalScrollOffset();
			if (pos == jumpPos)
			{
				if (onCompleteAction != null)
					onCompleteAction();
				return;
			}
			if (jumpDuration == 0)
			{
				SetContainerAnchorPos(jumpPos);
				if (onCompleteAction != null)
					onCompleteAction();
				return;
			} 
			LeanTween.value(pos, jumpPos, jumpDuration).setEase(tweenType).setDestroyOnComplete(true).setOnComplete(onCompleteAction).setOnUpdate((float value) =>
			{
				SetContainerAnchorPos(value);
				reloadData = true;
			});
		}

		void SetContainerLocalPos(float value)
		{
			var containerPos = container.localPosition;
			if (ScrollerType == DynamicScrollerType.Horizontal)
				containerPos.x = value;
			else
				containerPos.y = value;
			container.localPosition = containerPos;
		}

		void SetContainerAnchorPos(float value)
		{
			var containerPos = container.anchoredPosition;
			if (ScrollerType == DynamicScrollerType.Horizontal)
				containerPos.x = value;
			else
				containerPos.y = value;
			container.anchoredPosition = containerPos;
		}

		public DynamicScrollerCellView GetCellView(int dataIndex)
		{
			return ActiveCells.Find(x => x.DataIndex == dataIndex);
		}

		void CalScrollOffset()
		{
			if (CellPerLine > 1)
				offsetScroll = (scrollDir == DynamicScrollerDirection.Top || scrollDir == DynamicScrollerDirection.Right) ? 0 : -(activeCellSize - viewPort) / 2;
			else
				offsetScroll = (scrollDir == DynamicScrollerDirection.Top || scrollDir == DynamicScrollerDirection.Right) ? 0 : -(Delegate.GetCellSize(this, 0)) / 2;
		}

		public void JumpToDataIndex2(int dataIndex, float jumpDuration, LeanTweenType type, Action onCompleteAction = null)
		{
			if (Delegate == null)
				return;
			if (totalSize <= 0)
				ReloadData();
			if (_container.rect.size.y < viewPort)
			{
				if (onCompleteAction != null)
					onCompleteAction();
				return;
			}
			int cellCount = Delegate.GetCellCount(this);
			if (dataIndex > cellCount || dataIndex < 0)
				return;
			float cellSize = Delegate.GetCellSize(this, dataIndex);
			float jumpPos = 0;
			if (CellPerLine == 1)
			{
				for (int i = 0; i < dataIndex; i++)
				{
					jumpPos += Delegate.GetCellSize(this, i) + Spacing;
				}
			}
			else
			{
				int line = dataIndex / CellPerLine;
				jumpPos = line * (cellSize + Spacing);
			}
			if (ScrollerType == DynamicScrollerType.Horizontal)
				jumpPos *= -1;
			var pos = ScrollerType == DynamicScrollerType.Horizontal ? container.anchoredPosition.x : container.anchoredPosition.y;
			LeanTween.value(pos, jumpPos, jumpDuration).setEase(type).setOnComplete(onCompleteAction).setOnUpdate((float value) =>
			{
				SetContainerAnchorPos(value);
				reloadData = true;
			});
		}

        public void EnableScrollRect(bool enable)
        {
            scroller.enabled = enable;
        }
        public ScrollRect GetScrollRect()
        {
            return scroller;
        }
    }
}