﻿using System;
using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

namespace TTBT.DynamicScroller
{
#if UNITY_EDITOR
    [CustomEditor(typeof(DynamicScroller))]
	public class DynamicScrollerEditor : Editor
	{
		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();
			var myScript = target as DynamicScroller;

			if (myScript.CellPerLine > 1)
			{
				UnityEditor.EditorGUILayout.Space();
				UnityEditor.EditorGUILayout.LabelField("MultiCell Options: ", UnityEditor.EditorStyles.boldLabel);
				myScript.MiniCellAligtment = (DynamicScrollerCellAlightment)UnityEditor.EditorGUILayout.EnumPopup(new GUIContent("MiniCellAligtment"), (Enum)myScript.MiniCellAligtment);
				myScript.IsUseCustomMiniCellSize = UnityEditor.EditorGUILayout.Toggle(new GUIContent("IsUseCustomMiniCellSize"), myScript.IsUseCustomMiniCellSize);
				if (myScript.IsUseCustomMiniCellSize)
				{
					int customMaxSize = CalMaxMiniCellSize(myScript);
					myScript.CustomMiniCellSize = UnityEditor.EditorGUILayout.IntSlider(new GUIContent("CustomMiniCellSize"), (int)myScript.CustomMiniCellSize, 1, customMaxSize);
					myScript.SquareMiniCell = false;
				} else
					myScript.SquareMiniCell = UnityEditor.EditorGUILayout.Toggle(new GUIContent("SquareMiniCell"), myScript.SquareMiniCell);
			}
		}
		int CalMaxMiniCellSize(DynamicScroller scroller)
		{
			int result = 0;
			var rectT = scroller.GetComponent<RectTransform>();
			if (scroller.ScrollerType == DynamicScrollerType.Horizontal)
				result = (int)rectT.rect.height / scroller.CellPerLine;
			else
				result = (int)rectT.rect.width / scroller.CellPerLine;
			return result;
		}
	}
#endif
}
