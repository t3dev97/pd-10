﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace TTBT.DynamicScroller
{
	public class DynamicScrollerLineView : DynamicScrollerCellView
	{
		private List<DynamicScrollerCellView> _cells;
		private List<DynamicScrollerCellView> cells
		{
			get
			{
				if (_cells == null)
				{
					_cells = new List<DynamicScrollerCellView>();
				}
				return _cells;
			}
			set { _cells = value; }
		}

		private GridLayoutGroup _gridLayout;
		private GridLayoutGroup gridLayout
		{
			get
			{
				if (_gridLayout == null)
				{
					_gridLayout = gameObject.GetComponent<GridLayoutGroup>();
					if (_gridLayout == null)
						_gridLayout = gameObject.AddComponent<GridLayoutGroup>();
				}
				return _gridLayout;
			}
			set { _gridLayout = value; }
		}
		public void Init(DynamicScroller scroller, int beginIndex, int cellCount, Vector2 cellSize)
		{
			Init(scroller, beginIndex, cellCount, cellSize, Vector2.zero);
		}
		public void Init(DynamicScroller scroller, int beginIndex, int cellCount, Vector2 cellSize, Vector2 spacing)
		{
			if (scroller == null)
				return;
			gridLayout.cellSize = cellSize;
			gridLayout.spacing = spacing;
			gridLayout.constraint = scroller.ScrollerType == DynamicScrollerType.Horizontal ? GridLayoutGroup.Constraint.FixedColumnCount : GridLayoutGroup.Constraint.FixedRowCount;
			gridLayout.constraintCount = 1;
			//foreach (var c in cells)
			//	c.gameObject.SetActive(false);
			cells.Clear();
			int endIndex = beginIndex + cellCount;
			for (int i = beginIndex; i < endIndex; i++)
			{
				var foundCell = scroller.Delegate.GetCellView(scroller, i);
				foundCell.transform.SetParent(transform, false);
				cells.Add(foundCell);
				foundCell.name = "_Cell" + i;
				foundCell.gameObject.SetActive(true);
				foundCell.DataIndex = i;
				foundCell.transform.SetAsLastSibling();
			}
		}

		public DynamicScrollerCellView GetCellView(int dataIndex)
		{
			var foundCellView = cells.Find(x => x.DataIndex == dataIndex);
			return foundCellView;
		}
	}
}
