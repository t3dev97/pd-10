﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "HVL/ParticleTransparentUVSpeed" {
Properties {
	_TintColor ("Tint Color", Color) = (1.0,1.0,1.0,1.0)
	_MainTex ("Particle Texture", 2D) = "white" {}
//	_InvFade ("Soft Particles Factor", Range(0.01,3.0)) = 1.0//soft particle
	_ScrollXSpeed ("X Scroll Speed", Range(-1000, 1000)) = 0
	_ScrollYSpeed ("Y Scroll Speed", Range(-1000, 1000)) = 0
	_TileOffset ("Tile Offset", Vector) = (1.0,1.0,0.0,0.0)
}

Category {
	Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" }
	Blend SrcAlpha One
//	Blend SrcAlpha OneMinusSrcAlpha
//	Blend One One
//	AlphaTest Greater .01
	ColorMask RGB
	Cull Off Lighting Off ZWrite Off

	SubShader {
		Pass {
		
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_particles
			
			#include "UnityCG.cginc"
			#include "UnityShaderVariables.cginc"

			sampler2D _MainTex;
			fixed4 _TintColor;
			fixed _ScrollXSpeed;				//float
			fixed _ScrollYSpeed;				//float
			
			struct appdata_t {
				fixed4 vertex : POSITION;		//float4
				fixed4 color : COLOR;
				fixed2 texcoord : TEXCOORD0;	//float2
			};

			struct v2f {
				fixed4 vertex : SV_POSITION;	//float4
				fixed4 color : COLOR;
				fixed4 texcoord : TEXCOORD0;	//float2
//				#ifdef SOFTPARTICLES_ON
//				float4 projPos : TEXCOORD1;
//				#endif
			};
			
			fixed4 _MainTex_ST;		//float4
			fixed4 _TileOffset;

			v2f vert (appdata_t v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
//				#ifdef SOFTPARTICLES_ON
//				o.projPos = ComputeScreenPos (o.vertex);
//				COMPUTE_EYEDEPTH(o.projPos.z);
//				#endif
				o.color = v.color;
				
				fixed2 scrolledUV = v.texcoord;
				
				fixed xScrollValue = _ScrollXSpeed * _Time;
				fixed yScrollValue = _ScrollYSpeed * _Time;
				scrolledUV += fixed2(xScrollValue, yScrollValue);
				
				o.texcoord = fixed4(0,0,0,0);
				o.texcoord.xy = TRANSFORM_TEX(scrolledUV + _TileOffset.zw,_MainTex);
				return o;
			}

			sampler2D_float _CameraDepthTexture;
			//float _InvFade;
			
			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 color = tex2D(_MainTex, i.texcoord.xy) * _TintColor;
				color.rgb *= i.color.rgb;
				color.a = color.a * i.color.a;
				//color.a = color.a - abs(1 - i.color.a);
				
//				#ifdef SOFTPARTICLES_ON
//				float sceneZ = LinearEyeDepth (SAMPLE_DEPTH_TEXTURE_PROJ(_CameraDepthTexture, UNITY_PROJ_COORD(i.projPos)));
//				float partZ = i.projPos.z;
//				float fade = saturate (_InvFade * (sceneZ-partZ));
//				color.a *= fade;
//				#endif
				
				//if(color.a < 0.1)
				//	discard;

				return color;
//				#ifdef SOFTPARTICLES_ON
//				return 2.0* color;
//				#endif
			}
			ENDCG 
		}
	}	
}
}
