﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BCDataGameTest : MonoBehaviour
{
    public static BCDataGameTest api;
    public int LevelPlayer;
    public int energy;
    public int coin;
    public int gem;
    public void Awake()
    {
        if (api == null) api = this;
        else if (api != this) Destroy(this);
    }

}
