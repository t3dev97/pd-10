﻿using Studio.BC;
using UnityEngine;

public class ClearObj : MonoBehaviour
{
    public float TimeCoolDown = 1.5f;
    public bool Auto;
    private void Start()
    {
        if (Auto)
            Destroy(gameObject, TimeCoolDown);
    }
    public void Clear(float time = 0)
    {
        Destroy(gameObject, time);
    }
    public void EndDie()
    {
        BCCache.Api.GetEffectPrefabAsync(EffectKey.EnemyDieFX, (go) =>
        {
            GameObject _dieEffect = Instantiate(go);
            _dieEffect.SetActive(false);
            _dieEffect.transform.position = new Vector3(transform.position.x, transform.position.y + 1f, transform.position.z);
            _dieEffect.SetActive(true);
            Destroy(_dieEffect, 2f);
        });
    }
}
