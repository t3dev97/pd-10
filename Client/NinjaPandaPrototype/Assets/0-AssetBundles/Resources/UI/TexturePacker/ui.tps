<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.3.1</string>
        <key>fileName</key>
        <string>//tsclient/D/Projects/pd-10/Client/NinjaPandaPrototype/Assets/0-AssetBundles/Resources/UI/TexturePacker/ui.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>1024</int>
            <key>height</key>
            <int>1024</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../Atlas/ui{n}.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <true/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../Sprites/Btn_fade.png</key>
            <key type="filename">../Sprites/bg_add.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>16,16,32,32</rect>
                <key>scale9Paddings</key>
                <rect>16,16,32,32</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/Devil_UI.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>88,53,175,106</rect>
                <key>scale9Paddings</key>
                <rect>88,53,175,106</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/Kunaichamtuong.png</key>
            <key type="filename">../Sprites/Kunaichamtuong1.png</key>
            <key type="filename">../Sprites/PrizeArrow.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>32,32,64,64</rect>
                <key>scale9Paddings</key>
                <rect>32,32,64,64</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/Lock.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>17,22,34,43</rect>
                <key>scale9Paddings</key>
                <rect>17,22,34,43</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/Merchant_UI.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>88,64,176,129</rect>
                <key>scale9Paddings</key>
                <rect>88,64,176,129</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/SpinWheel.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>128,128,256,256</rect>
                <key>scale9Paddings</key>
                <rect>128,128,256,256</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/TalentButton.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>33,30,66,60</rect>
                <key>scale9Paddings</key>
                <rect>33,30,66,60</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/TopBar.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>9,8,19,16</rect>
                <key>scale9Paddings</key>
                <rect>9,8,19,16</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/TopBarBlue.png</key>
            <key type="filename">../Sprites/TopBarRed.png</key>
            <key type="filename">../Sprites/TopBarWhite.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,6,14,11</rect>
                <key>scale9Paddings</key>
                <rect>7,6,14,11</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/TopBarBoss.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>15,13,30,25</rect>
                <key>scale9Paddings</key>
                <rect>15,13,30,25</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/atk.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>25,30,49,60</rect>
                <key>scale9Paddings</key>
                <rect>25,30,49,60</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/bar_1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,3,12,6</rect>
                <key>scale9Paddings</key>
                <rect>6,3,12,6</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/bar_2.png</key>
            <key type="filename">../Sprites/bar_3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,2,10,4</rect>
                <key>scale9Paddings</key>
                <rect>5,2,10,4</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/bar_boss_1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,11,21,21</rect>
                <key>scale9Paddings</key>
                <rect>10,11,21,21</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/bar_boss_2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,3,12,7</rect>
                <key>scale9Paddings</key>
                <rect>6,3,12,7</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/bar_boss_3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,7,20,15</rect>
                <key>scale9Paddings</key>
                <rect>10,7,20,15</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/bg2.png</key>
            <key type="filename">../Sprites/bg_1.jpg</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>160,130,320,260</rect>
                <key>scale9Paddings</key>
                <rect>160,130,320,260</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/bg5_t130_b106.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>131,95,262,191</rect>
                <key>scale9Paddings</key>
                <rect>131,95,262,191</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/bg_2.jpg</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>160,65,320,130</rect>
                <key>scale9Paddings</key>
                <rect>160,65,320,130</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/bg_3.jpg</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>160,143,320,286</rect>
                <key>scale9Paddings</key>
                <rect>160,143,320,286</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/bg_4.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>46,109,91,219</rect>
                <key>scale9Paddings</key>
                <rect>46,109,91,219</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/bg_5.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>131,78,262,157</rect>
                <key>scale9Paddings</key>
                <rect>131,78,262,157</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/bg_avatar.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>28,28,56,56</rect>
                <key>scale9Paddings</key>
                <rect>28,28,56,56</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/bg_avatar_1.png</key>
            <key type="filename">../Sprites/bg_avatar_2.png</key>
            <key type="filename">../Sprites/bg_avatar_3.png</key>
            <key type="filename">../Sprites/bg_avatar_4.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>23,23,46,46</rect>
                <key>scale9Paddings</key>
                <rect>23,23,46,46</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/bg_bt.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>41,36,82,72</rect>
                <key>scale9Paddings</key>
                <rect>41,36,82,72</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/bg_item.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>50,69,100,137</rect>
                <key>scale9Paddings</key>
                <rect>50,69,100,137</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/bg_item_in.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>42,42,83,83</rect>
                <key>scale9Paddings</key>
                <rect>42,42,83,83</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/bg_title.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>48,11,96,22</rect>
                <key>scale9Paddings</key>
                <rect>48,11,96,22</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/bg_title_2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>42,12,84,24</rect>
                <key>scale9Paddings</key>
                <rect>42,12,84,24</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/bg_title_3.png</key>
            <key type="filename">../Sprites/bg_title_4.png</key>
            <key type="filename">../Sprites/bg_title_5.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>160,18,320,35</rect>
                <key>scale9Paddings</key>
                <rect>160,18,320,35</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/bg_title_6.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>160,29,320,57</rect>
                <key>scale9Paddings</key>
                <rect>160,29,320,57</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/bg_title_7.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>59,17,118,35</rect>
                <key>scale9Paddings</key>
                <rect>59,17,118,35</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/bg_top.jpg</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>160,21,320,42</rect>
                <key>scale9Paddings</key>
                <rect>160,21,320,42</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/box_1.png</key>
            <key type="filename">../Sprites/icon_lv3.png</key>
            <key type="filename">../Sprites/lineHealt.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>34,10,69,21</rect>
                <key>scale9Paddings</key>
                <rect>34,10,69,21</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/box_2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,38,11,77</rect>
                <key>scale9Paddings</key>
                <rect>6,38,11,77</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/box_3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,18,20,36</rect>
                <key>scale9Paddings</key>
                <rect>10,18,20,36</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/bt_1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>39,39,78,78</rect>
                <key>scale9Paddings</key>
                <rect>39,39,78,78</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/bt_2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>25,25,50,49</rect>
                <key>scale9Paddings</key>
                <rect>25,25,50,49</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/bt_3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>26,25,51,51</rect>
                <key>scale9Paddings</key>
                <rect>26,25,51,51</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/bt_home.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,27,14,54</rect>
                <key>scale9Paddings</key>
                <rect>7,27,14,54</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/bt_home_2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>53,27,105,54</rect>
                <key>scale9Paddings</key>
                <rect>53,27,105,54</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/bt_home_3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>15,13,29,27</rect>
                <key>scale9Paddings</key>
                <rect>15,13,29,27</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/bt_home_line.jpg</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>1,26,3,53</rect>
                <key>scale9Paddings</key>
                <rect>1,26,3,53</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/bt_mui_ten.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>15,11,30,23</rect>
                <key>scale9Paddings</key>
                <rect>15,11,30,23</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/bt_nomal.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,22,17,43</rect>
                <key>scale9Paddings</key>
                <rect>8,22,17,43</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/bt_pause.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>18,21,37,42</rect>
                <key>scale9Paddings</key>
                <rect>18,21,37,42</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/bt_play.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>14,21,29,42</rect>
                <key>scale9Paddings</key>
                <rect>14,21,29,42</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/bt_small 1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>12,19,25,38</rect>
                <key>scale9Paddings</key>
                <rect>12,19,25,38</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/bt_small.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>12,17,24,34</rect>
                <key>scale9Paddings</key>
                <rect>12,17,24,34</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/bt_sound.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>11,14,22,28</rect>
                <key>scale9Paddings</key>
                <rect>11,14,22,28</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/button.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>21,34,42,68</rect>
                <key>scale9Paddings</key>
                <rect>21,34,42,68</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/heal.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>23,24,46,48</rect>
                <key>scale9Paddings</key>
                <rect>23,24,46,48</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/hp_back.png</key>
            <key type="filename">../Sprites/hp_front.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>21,3,41,6</rect>
                <key>scale9Paddings</key>
                <rect>21,3,41,6</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/hp_bar.png</key>
            <key type="filename">../Sprites/hp_red.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>20,2,39,4</rect>
                <key>scale9Paddings</key>
                <rect>20,2,39,4</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/ico_agile.png</key>
            <key type="filename">../Sprites/ico_enhance.png</key>
            <key type="filename">../Sprites/ico_glory.png</key>
            <key type="filename">../Sprites/ico_iron_bulwark.png</key>
            <key type="filename">../Sprites/ico_isnpire.png</key>
            <key type="filename">../Sprites/ico_power.png</key>
            <key type="filename">../Sprites/ico_recover.png</key>
            <key type="filename">../Sprites/ico_strenght.png</key>
            <key type="filename">../Sprites/ico_time_reward.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>27,23,54,45</rect>
                <key>scale9Paddings</key>
                <rect>27,23,54,45</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/icon_ tick.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>11,9,23,18</rect>
                <key>scale9Paddings</key>
                <rect>11,9,23,18</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/icon_Equipment.png</key>
            <key type="filename">../Sprites/icon_shop.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>19,19,39,39</rect>
                <key>scale9Paddings</key>
                <rect>19,19,39,39</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/icon_Equipment_big.png</key>
            <key type="filename">../Sprites/icon_shop_big.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>24,24,48,48</rect>
                <key>scale9Paddings</key>
                <rect>24,24,48,48</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/icon_add.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,5,11,11</rect>
                <key>scale9Paddings</key>
                <rect>6,5,11,11</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/icon_angle.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>72,50,144,101</rect>
                <key>scale9Paddings</key>
                <rect>72,50,144,101</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/icon_coin.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>9,12,18,24</rect>
                <key>scale9Paddings</key>
                <rect>9,12,18,24</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/icon_lucchien.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>9,10,17,21</rect>
                <key>scale9Paddings</key>
                <rect>9,10,17,21</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/icon_lv.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>14,14,29,29</rect>
                <key>scale9Paddings</key>
                <rect>14,14,29,29</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/icon_lv2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>25,5,50,10</rect>
                <key>scale9Paddings</key>
                <rect>25,5,50,10</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/icon_lv_big.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>61,61,122,122</rect>
                <key>scale9Paddings</key>
                <rect>61,61,122,122</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/icon_mang.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>12,10,24,20</rect>
                <key>scale9Paddings</key>
                <rect>12,10,24,20</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/icon_rubi.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>13,12,26,25</rect>
                <key>scale9Paddings</key>
                <rect>13,12,26,25</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/icon_ruong.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>15,15,30,29</rect>
                <key>scale9Paddings</key>
                <rect>15,15,30,29</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/icon_setting.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>19,19,38,39</rect>
                <key>scale9Paddings</key>
                <rect>19,19,38,39</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/icon_setting_big.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>28,29,57,58</rect>
                <key>scale9Paddings</key>
                <rect>28,29,57,58</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/icon_small_1.png</key>
            <key type="filename">../Sprites/icon_small_2.png</key>
            <key type="filename">../Sprites/icon_small_3.png</key>
            <key type="filename">../Sprites/icon_small_4.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,8,17,17</rect>
                <key>scale9Paddings</key>
                <rect>8,8,17,17</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/icon_sucmanh.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,13,16,26</rect>
                <key>scale9Paddings</key>
                <rect>8,13,16,26</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/icon_talent.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>20,19,39,39</rect>
                <key>scale9Paddings</key>
                <rect>20,19,39,39</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/icon_talent_big.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>24,24,49,48</rect>
                <key>scale9Paddings</key>
                <rect>24,24,49,48</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/icon_video.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>15,12,30,25</rect>
                <key>scale9Paddings</key>
                <rect>15,12,30,25</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/icon_world_big.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>22,17,43,34</rect>
                <key>scale9Paddings</key>
                <rect>22,17,43,34</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/item_chest_1.png</key>
            <key type="filename">../Sprites/item_chest_1b.png</key>
            <key type="filename">../Sprites/item_chest_2.png</key>
            <key type="filename">../Sprites/item_chest_2b.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>64,64,128,128</rect>
                <key>scale9Paddings</key>
                <rect>64,64,128,128</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/khung _popup.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>99,29,197,57</rect>
                <key>scale9Paddings</key>
                <rect>99,29,197,57</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/line.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>16,5,32,10</rect>
                <key>scale9Paddings</key>
                <rect>16,5,32,10</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/loading_1.png</key>
            <key type="filename">../Sprites/loading_2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>40,7,79,14</rect>
                <key>scale9Paddings</key>
                <rect>40,7,79,14</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/lv_big_2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>145,68,291,137</rect>
                <key>scale9Paddings</key>
                <rect>145,68,291,137</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/lv_big_3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>61,56,123,112</rect>
                <key>scale9Paddings</key>
                <rect>61,56,123,112</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/mui _ten _khung_popup.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,8,19,15</rect>
                <key>scale9Paddings</key>
                <rect>10,8,19,15</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/skill_bg.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>38,38,77,77</rect>
                <key>scale9Paddings</key>
                <rect>38,38,77,77</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/skill_border.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>40,40,81,81</rect>
                <key>scale9Paddings</key>
                <rect>40,40,81,81</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/spin_1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>116,119,232,237</rect>
                <key>scale9Paddings</key>
                <rect>116,119,232,237</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/spin_2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>87,87,175,174</rect>
                <key>scale9Paddings</key>
                <rect>87,87,175,174</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/spin_3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>43,42,86,84</rect>
                <key>scale9Paddings</key>
                <rect>43,42,86,84</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/spin_4.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>42,41,85,81</rect>
                <key>scale9Paddings</key>
                <rect>42,41,85,81</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/spin_5.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>16,21,32,41</rect>
                <key>scale9Paddings</key>
                <rect>16,21,32,41</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../Sprites</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
